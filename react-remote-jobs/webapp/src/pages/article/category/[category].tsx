import {GetStaticPaths, GetStaticProps} from 'next';
import {
    ArticleImportedModel
} from "../../../components/react-shared-module/base-projects/articles-digest/data/model/ArticleImported.model";
import {FirebaseClientFirestoreUtils} from "../../../components/react-shared-module/logic/shared-data/firebase.utils";
import {
    SharedFirestoreCollectionDB
} from "../../../components/react-shared-module/logic/shared-data/datamodel/shared-firestore-collections";
import {UIHelper} from "../../../components/ui-helper/UIHelper";
import CustomRootLayoutComponent from "../../../components/app-components/root/CustomRootLayout.component";
import ArticlesCategorySelectorComponent
    from "../../../components/react-shared-module/base-projects/articles-digest/ui-components/articles-category-selector.component";
import NewsletterSignupListBannerComponent
    from "../../../components/react-shared-module/ui-components/newsletter/shared/newsletter-signup-list-banner.component";
import NewsletterPopupComponent
    from "../../../components/react-shared-module/ui-components/newsletter/shared/newsletter-popup.component";
import ArticlesListContainerComponent
    from "../../../components/react-shared-module/base-projects/articles-digest/ui-components/articles-list-container.component";
import {PageHeadProps} from "../../../components/react-shared-module/ui-components/root/RootLayout.component";
import RootConfigs from "../../../configs";
import SpinnerComponent from "../../../components/react-shared-module/ui-components/shared/Spinner.component";

const MAX_LATEST_ARTICLES = 30;

export const getStaticPaths: GetStaticPaths = async () => {
    const paths = UIHelper.getArticlesCategories().map((category) => ({
        params: {category: encodeURIComponent(category)},
    }));

    return {
        paths,
        fallback: 'blocking',
    };
};

export const getStaticProps: GetStaticProps = async (context) => {
    const category = decodeURIComponent(context.params?.category as string);

    try {
        const listRecentArticles = await FirebaseClientFirestoreUtils.getDocumentsFilteredWithPagination({
            collectionPath: SharedFirestoreCollectionDB.ArticlesDigestProject.ArticlePublished,
            pageSize: MAX_LATEST_ARTICLES,
            fieldOrderBy: "createdAt",
            filter: {
                field: "aiGeneratedContent.category",
                value: category,
            }
        });

        return {
            props: {
                latestArticles: listRecentArticles || [],
                category,
            },
            revalidate: 60, // Revalidate every 60 seconds
        };
    } catch (error) {
        console.error("Error fetching data for category:", category, error);
        return {
            props: {
                latestArticles: [],
                category,
            },
            revalidate: 60,
        };
    }
};

interface ArticlesCategoryPageProps {
    latestArticles: ArticleImportedModel[];
    category: string;
}

export default function ArticlesCategoryPage({latestArticles, category}: ArticlesCategoryPageProps) {
    if (!category) {
        return (
            <CustomRootLayoutComponent>
                <SpinnerComponent/>
            </CustomRootLayoutComponent>
        )
    }

    const pageHeadParams: PageHeadProps = {
        title: `${RootConfigs.SITE_TITLE} - Latest News About ${category}`,
        description: RootConfigs.SITE_DESCRIPTION,
        publishedTime: latestArticles.length > 0
            ? new Date(latestArticles[0].createdAt!).toISOString()
            : new Date().toISOString(),
    };

    return (
        <CustomRootLayoutComponent
            pageHeadProps={pageHeadParams}
            headerBanner={
                <ArticlesCategorySelectorComponent
                    categories={UIHelper.getArticlesCategories()}
                    articlesUrl="/"
                    selectedCategory={category}
                />
            }
        >
            <div className='flex flex-col items-center w-full'>
                <NewsletterSignupListBannerComponent title="Subscribe to our Newsletter"/>
            </div>
            <ArticlesListContainerComponent
                listArticles={latestArticles}
                title="Latest News About"
                titleHighlight={category}
            />
            <NewsletterPopupComponent/>
        </CustomRootLayoutComponent>
    );
}