import {
    DailyPublishedJobs,
    JobDetailsModel, JobListModel, JobsDiscoverDataUtils
} from "../../components/react-shared-module/base-projects/jobs-discover/jobs-discover-data.model";
import CustomRootLayoutComponent from "../../components/app-components/root/CustomRootLayout.component";
import {
    FirebaseClientFirestoreUtils,
    getAPIDocument
} from "../../components/react-shared-module/logic/shared-data/firebase.utils";
import {
    SharedFirestoreCollectionDB
} from "../../components/react-shared-module/logic/shared-data/datamodel/shared-firestore-collections";
import JobDetailsPageComponent
    from "../../components/react-shared-module/base-projects/jobs-discover/ui/job-details-page.component";
import NotFoundContentComponent
    from "../../components/react-shared-module/ui-components/shared/NotFoundContent.component";
import {PageHeadProps} from "../../components/react-shared-module/ui-components/root/RootLayout.component";
import RootConfigs from "../../configs";
import { SharedRoutesUtils } from "../../components/react-shared-module/utils/shared-routes.utils";

const MAX_LATEST_ARTICLES = 10;

export async function getStaticPaths() {
    let listRecentJobs: JobDetailsModel[] | undefined = await FirebaseClientFirestoreUtils.getDocumentsWithPagination(SharedFirestoreCollectionDB.JobsDiscoverProject.JobDetails, 15, "createdAt");
    let listPaths: any[] = [];

    if (listRecentJobs) {
        listPaths = listRecentJobs
            .filter((jobItem) => jobItem && jobItem.companyLogo && jobItem.title && jobItem.applicationUrl)
            .map((jobItem) => {
                return {
                    params: {
                        jobId: jobItem.uid!,
                    },
                }
            });
    }

    return {
        paths: listPaths,
        fallback: 'blocking',
    }
}

export async function getStaticProps(context: any) {
    const jobId: string = context.params.jobId as string;

    const jobData: JobDetailsModel | undefined = await getAPIDocument(SharedFirestoreCollectionDB.JobsDiscoverProject.JobDetails, jobId);


    const listPublishedDays: DailyPublishedJobs[] = await FirebaseClientFirestoreUtils
        .getDocumentsWithPagination(SharedFirestoreCollectionDB.JobsDiscoverProject.DailyPublishedJobs, 2, "publishedDayId");

    let latestJobs: JobListModel[] = [];
    listPublishedDays.map((dailyItem) => {
        latestJobs = latestJobs
            .concat(Object.values(dailyItem.listJobs)
                .filter((item) => item.uid !== jobId)).sort((j1, j2) => j2.createdAt! - j1.createdAt!);
    });



    if (jobData) {
        return {
            props: {
                jobData: jobData,
                latestJobs: latestJobs.slice(0, MAX_LATEST_ARTICLES),
            },
            revalidate: 60, // each 60 seconds
        };
    } else {
        return {
            props: {
            },
            revalidate: 60, // each 60 seconds
        }
    }

}

export default function JobDetailsPage(props: {jobData?: JobDetailsModel, latestJobs: JobListModel[]}) {
    if (props.jobData) {
        const pageHeadParams: PageHeadProps = {
            title: `${props.jobData?.title!} [Remote Job] @${props.jobData.company}`,
            description: JobsDiscoverDataUtils.generateSEODescription(props.jobData),
            imgUrl: props.jobData.companyThumbnailUrl ? props.jobData.companyThumbnailUrl : props.jobData?.companyLogo!,
            publishedTime: new Date(props.jobData?.createdAt!).toString(),
            canonicalUrl: `${RootConfigs.BASE_URL}${SharedRoutesUtils.getJobDetailsPageUrl(props.jobData.uid!)}`
        };
        return (
            <CustomRootLayoutComponent customBody={true} pageHeadProps={pageHeadParams}>
                <JobDetailsPageComponent jobData={props.jobData} />
            </CustomRootLayoutComponent>
        )
    } else {
        return (
            <CustomRootLayoutComponent isIndexable={false}>
                <NotFoundContentComponent />
            </CustomRootLayoutComponent>
        )
    }

}