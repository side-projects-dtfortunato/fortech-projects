import {
    FirebaseClientFirestoreUtils
} from "../components/react-shared-module/logic/shared-data/firebase.utils";
import {
    SharedFirestoreCollectionDB
} from "../components/react-shared-module/logic/shared-data/datamodel/shared-firestore-collections";
import {
    DailyPublishedArticlesModel
} from "../components/react-shared-module/base-projects/articles-digest/data/model/DailyPublishedArticles.model";
import CustomRootLayoutComponent from "../components/app-components/root/CustomRootLayout.component";
import NewsletterPopupComponent
    from "../components/react-shared-module/ui-components/newsletter/shared/newsletter-popup.component";
import NewsletterSignupListBannerComponent
    from "../components/react-shared-module/ui-components/newsletter/shared/newsletter-signup-list-banner.component";
import JobListItemComponent
    from "../components/react-shared-module/base-projects/jobs-discover/ui/job-list-item.component";
import {
    DailyPublishedJobs,
    JobListModel,
    JobsDiscoverDataUtils
} from "../components/react-shared-module/base-projects/jobs-discover/jobs-discover-data.model";
import JobListPageComponent
    from "../components/react-shared-module/base-projects/jobs-discover/ui/job-list-page.component";


const ITEMS_PER_PAGE = 3;

export async function getStaticProps() {
    const listPublishedDays: DailyPublishedJobs[] = await FirebaseClientFirestoreUtils
        .getDocumentsWithPagination(SharedFirestoreCollectionDB.JobsDiscoverProject.DailyPublishedJobs, ITEMS_PER_PAGE, "publishedDayId");

    let filterListSkills: string[] = await JobsDiscoverDataUtils.getFiltersListSkills();


    let listTopJobs: JobListModel[] = [];

    // Get Featured Jobs
    const listFeaturedJobs: JobListModel[] = await FirebaseClientFirestoreUtils
        .getDocumentsFilteredWithPagination({
            collectionPath: SharedFirestoreCollectionDB.JobsDiscoverProject.JobDetails,
            pageSize: 10,
            fieldOrderBy: "createdAt",
            filter: { field: "publishPlanType", value: "FEATURED" },
        });
    if (listFeaturedJobs.length > 0) {
        listTopJobs = listFeaturedJobs;
    }

    // Get Popular Jobs
    const popularJobsCount = 5 - listFeaturedJobs.length;
    if (popularJobsCount > 0) {
        let listPopularJobs: JobListModel[] = await FirebaseClientFirestoreUtils
            .getDocumentsWithPagination(SharedFirestoreCollectionDB.JobsDiscoverProject.JobDetails, popularJobsCount, "stats.applyCounter", );
        listPopularJobs = listPopularJobs.map((job) => ({ ...job, isPopular: true }));
        listTopJobs = [...listTopJobs, ...listPopularJobs];
    }

    
    return {
        props: {
            listPublishedDays,
            filterListSkills,
            popularJobs: listTopJobs,
        },
        revalidate: 60,
    }
}

export default function Home(props: {listPublishedDays: DailyPublishedJobs[]}) {

    return (
        <CustomRootLayoutComponent>

            <JobListPageComponent preloadedData={props.listPublishedDays} itemsPerPage={ITEMS_PER_PAGE} />

            <div className='flex flex-col items-center w-full'>
                <NewsletterSignupListBannerComponent title={"Subscribe our Newsletter"} />
            </div>
            {/* Popups */}
            <NewsletterPopupComponent />
        </CustomRootLayoutComponent>
    )
}
