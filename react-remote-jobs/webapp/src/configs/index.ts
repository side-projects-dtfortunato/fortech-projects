
export default class RootConfigs {
    static ENVIRONMENT_DEV = false;
    static BASE_URL = "https://www.reactremotejobs.com";
    static SITE_NAME = "ReactRemoteJobs.com";
    static SITE_TITLE = "Remote React Jobs | Find Top React Developer Positions Worldwide";
    static SITE_DESCRIPTION = "Discover the best remote React developer jobs from top companies globally. Our curated list features exciting opportunities for React professionals seeking flexible, work-from-anywhere positions. Stay ahead in your career with our regularly updated job board tailored for React experts.";
    static CURRENT_NICHE_SITE = RootConfigs.SITE_NAME;
    static CONTACT_EMAIL = "contact@reactremotejobs.com";
    static SITE_TOPIC = "React Remote Jobs";
    static THEME = {
        PRIMARY_COLOR: "blue",
    }
    static META_CONFIGS: any = {
        updateNicheBackend: false,
        disableGoogleSignin: true,
        disableCookies: true,
        disableAppStoreBanner: false,
        enableJobSearch: true,
    }
    static APP_STORE_LINKS = {
        androidAppId: "https://play.google.com/store/apps/details?id=com.web2app.reactjobs&pli=1",
        iosAppId: "https://apps.apple.com/us/app/react-remote-jobs/id6470238140",
    }
    static SOCIAL_NETWORKS: {
        [socialId: string]: {
            iconUrl: string,
            label: string,
            link: string,
        },
    } = {
 /*       medium: {
            label: "Medium",
            link: "https://medium.com/@journeypreneur",
            iconUrl: "https://miro.medium.com/fit/c/152/152/1*sHhtYhaCe2Uc3IU0IgKwIQ.png",
        },*/
    }
    static FIREBASE_CONFIG = this.ENVIRONMENT_DEV ?

        // DEV Config - // TODO Change it
        {
            apiKey: "AIzaSyCB2yrNkEHbpShTH_xwHOzcl2mXJT5i1K8",
            authDomain: "fortuly-qa-env.firebaseapp.com",
            projectId: "fortuly-qa-env",
            storageBucket: "fortuly-qa-env.appspot.com",
            messagingSenderId: "650128691290",
            appId: "1:650128691290:web:69f8a283090d23e0454863",
            measurementId: "G-V6XN6K51VZ"
        } :
        // PROD
        {
            apiKey: "AIzaSyC4pEUbabBF-z53w67gkp2PnueOSaP6wvE",
            authDomain: "react-remote-jobs.firebaseapp.com",
            databaseURL: "https://react-remote-jobs-default-rtdb.firebaseio.com",
            projectId: "react-remote-jobs",
            storageBucket: "react-remote-jobs.appspot.com",
            messagingSenderId: "97897443194",
            appId: "1:97897443194:web:560cc690afa2c7b80bb0c3",
            measurementId: "G-HM665MW1D3"
        };


    static IMAGEKIT_API = {
        publicKey : "public_OurCtnmj0jWqjVslk5YDfKE4aHQ=",
        privateKey : "private_vRUtPf6HGOj68f5rZZN8u9RHfiQ=",
        urlEndpoint : "https://ik.imagekit.io/fortulyapps",
    }

    static ONE_SIGNAL_API = "0c01787c-8fbd-4153-a608-c5d102cbc1b0";

}
