import {
    JobDetailsModel,
    LinkedinSourceModel
} from "../../shareable-firebase-backend/base-projects/jobs-discover/shared-jobs-discover.data";
import axios from "axios";


/**
 * Regions ids:
 * - UK: 101165590
 * - Australia: 101452733
 * - USA: 103644278
 * - EU: 91000002
 */
export const LINKEDIN_SOURCES: LinkedinSourceModel[] = [
    {
        id: "1",
        category: "React Developer",
        region: "USA",
        url: "https://www.linkedin.com/jobs/search?keywords=React%20Developer&geoId=103644278&f_TPR=&f_WT=2&f_E=2%2C3%2C4%2C5&trk=public_jobs_jobs-search-bar_search-submit&position=1&pageNum=0",
    },
    {
        id: "2",
        category: "React Developer",
        region: "EU",
        url: "https://www.linkedin.com/jobs/search?keywords=React%20Developer&geoId=91000002&f_TPR=&f_WT=2&f_E=2%2C3%2C4%2C5&trk=public_jobs_jobs-search-bar_search-submit&position=1&pageNum=0",
    },
    {
        id: "3",
        category: "React Developer",
        region: "Australia",
        url: "https://www.linkedin.com/jobs/search?keywords=React%20Developer&geoId=101452733&f_TPR=&f_WT=2&f_E=2%2C3%2C4%2C5&trk=public_jobs_jobs-search-bar_search-submit&position=1&pageNum=0",
    },
    {
        id: "4",
        category: "React Developer",
        region: "UK",
        url: "https://www.linkedin.com/jobs/search?keywords=React%20Developer&geoId=101165590&f_TPR=&f_WT=2&f_E=2%2C3%2C4%2C5&position=1&pageNum=0",
    },
]

export const JOB_CATEGORIES = [
    {
        jobCategoryLabel: "React Developer",
        jobCategoryQuery: "React Developer"
    },
    {
        jobCategoryLabel: "Fullstack React",
        jobCategoryQuery: "Fullstack React"
    },
    {
        jobCategoryLabel: "React Lead",
        jobCategoryQuery: "React Lead"
    },
    {
        jobCategoryLabel: "React Native",
        jobCategoryQuery: "React Native"
    },
] as {jobCategoryLabel: string, jobCategoryQuery: string}[];

export const JOB_REGIONS = [
    {
        regionLabel: "USA",
        regionId: "103644278"
    },
    {
        regionLabel: "EU",
        regionId: "91000002"
    },
    {
        regionLabel: "Australia",
        regionId: "101452733"
    },
    {
        regionLabel: "UK",
        regionId: "101165590"
    },
] as {regionLabel: string, regionId: string}[];

export const ALLOWED_WORK_MODE = ["REMOTE"];

export class CustomJobsDiscoverUtils {

    static getIsValidRule() {
        return undefined;
    }

    static async importJobsFromRemoteJobsHub() {
        const category = "React%20Developer";
        const listJobs: JobDetailsModel[] = (await axios.get(`https://us-central1-remotejobshub-app.cloudfunctions.net/getLatestJobsFunction?category=${category}`)).data;
        return listJobs;
    }
}