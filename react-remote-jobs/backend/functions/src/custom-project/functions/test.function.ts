import * as functions from "firebase-functions";
import {
    SharedJobsDiscoverData
} from "../../shareable-firebase-backend/base-projects/jobs-discover/shared-jobs-discover.data";

export const testFunction = functions.https.onRequest(
    async (req, resp) => {

        let jobs = await SharedJobsDiscoverData.extractJobListings("https://www.linkedin.com/jobs/search?keywords=React%20Developer&geoId=91000002&f_TPR=&f_WT=2&f_E=2%2C3%2C4%2C5&trk=public_jobs_jobs-search-bar_search-submit&position=1&pageNum=0");
        resp.send({jobs});

    });