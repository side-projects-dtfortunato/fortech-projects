
export const PRODUCTION_GCLOUD_PROJECT = "react-remote-jobs";

export const WEBAPP_CONFIGS = {
    LOGO_URL: "https://www.reactremotejobs.com/images/logo-white-bg.jpg",
    SITE_TAGLINE: "",
    SITE_NAME: "RreactRemoteJobs.com",
    WEBSITE_URL: "https://www.reactremotejobs.com",
    COMPANY_ADDRESS: "3030 Coimbra (Portugal)",
    COMPANY_EMAIL: "contact@reactremotejobs.com",
    POST_PREFIX_URL: "https://www.reactremotejobs.com/post/",
    PROFILE_PREFIX_URL: "https://www.reactremotejobs.com/public-profile/",
    UNSUBSCRIBE_URL: `https://us-central1-${PRODUCTION_GCLOUD_PROJECT}.cloudfunctions.net/unsubscribeEmail?uid=`,

    MAILERSEND_API_KEY: "TODO",

    CUSTOM_CONFIGS: {
        openPushInApp: true,
    } as any,

    // Account: contact@sidehustlify.com - Di.....oc7
    IMAGE_KIT_API: {
        publicKey : "public_OurCtnmj0jWqjVslk5YDfKE4aHQ=",
        privateKey : "private_vRUtPf6HGOj68f5rZZN8u9RHfiQ=",
        urlEndpoint : "https://ik.imagekit.io/fortulyapps",
    },

    STRIPE_API: {
        prod: "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",
        dev: "sk_test_51PueA3HT2hxH9IRDdlrmFSjPbHmjQ1An0PNkz74ZkAOw50c1MSK5nG0MGBWOvbm5RjkBDgG4VXnMDZ2yRBWJGtpA00wlrvUV9J",
        webhook_dev: "whsec_jySvwwZUSe3BLDFdJPYWn6c2vABfZbDd",
        webhook_prod: "whsec_LdhIuERVGsmQP4Xptkwrk0kbIALaCYQV",
    },

    ONESIGNAL_CONFIGS: {
        apiKey: "ODE5NzI1NTAtNDg3MC00MWY3LWFkN2EtODg1MGM0OWQxMTM3",
        appId: "0c01787c-8fbd-4153-a608-c5d102cbc1b0"
    },

    SOCIAL_NETWORK_API_KEY: {
        metaAccessToken: "",
        facebookPageId: "",
        instagramId: "",
        threadsAPI: {
            clientId: "",
            clientSecret: "",
            redirectUri: `https://us-central1-${PRODUCTION_GCLOUD_PROJECT}.cloudfunctions.net/threadsAPIHandleAuthCallback`,
        },
        redditAPI: {
            clientId: "",
            clientSecret: "",
            username: "",
            password: "",
            subReddit: "",
        }, 
        bluesky: {
            username: "",
            password: "",
        },
        telegram: {
            channel: "",
            botToken: "",
        }
    }
}