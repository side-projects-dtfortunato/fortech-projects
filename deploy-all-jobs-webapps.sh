#!/bin/bash

# Check if any custom commands were provided
# ./deploy-all-jobs-webapps.sh -c "npm install lodash" -c "npm audit fix" -c "npm install moment"
custom_commands=()
while [[ "$#" -gt 0 ]]; do
    case $1 in
        -c|--command) custom_commands+=("$2"); shift ;;
        *) echo "Unknown parameter: $1"; exit 1 ;;
    esac
    shift
done

# Function to deploy a single project
deploy_project() {
    local project=$1
    cd "$project/webapp"
    
    # Execute any custom commands provided
    for cmd in "${custom_commands[@]}"; do
        echo "Executing custom command in $project: $cmd"
        eval "$cmd"
    done
    
    # Execute the deployment command
    echo "Starting deployment for $project"
    npm run deploy-prod
    echo "Finished deployment for $project"
    
    cd ../..
}

# Array to store background process PIDs
pids=()
current_parallel=0
max_parallel=3

# Deploy projects with parallel limit
for project in australiajobs-app jobsinuk-app remotejobshub-app customerremotejobs marketingremotejobs-app remoteitjobs-app design-remote-jobs react-remote-jobs juniorremotejobs-com remoteinaustralia remote-work-webapp-v2; do
    # If we've reached the parallel limit, wait for one to finish
    if [ $current_parallel -ge $max_parallel ]; then
        wait -n  # Wait for any background process to finish
        current_parallel=$((current_parallel - 1))
    fi
    
    # Start the deployment in background
    deploy_project "$project" &
    pids+=($!)
    current_parallel=$((current_parallel + 1))
done

# Wait for all remaining processes to complete
wait

echo "All deployments completed!"
