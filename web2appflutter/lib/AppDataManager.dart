import 'dart:convert';
import 'dart:io';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
import 'package:web2appflutter/shared/data/model-data/AppData.model.dart';

class AppDataManager {
  static const String DEFAULT_JSON = '{"bottomNavbar":[{"icon":{"fileUrl":"data:image/svg+xml;base64,PHN2ZyBzdHJva2U9ImN1cnJlbnRDb2xvciIgZmlsbD0iIzAwMDAwMCIgc3Ryb2tlLXdpZHRoPSIwIiB2aWV3Qm94PSIwIDAgNTc2IDUxMiIgaGVpZ2h0PSIxZW0iIHdpZHRoPSIxZW0iIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+PHBhdGggZD0iTTI4MC4zNyAxNDguMjZMOTYgMzAwLjExVjQ2NGExNiAxNiAwIDAgMCAxNiAxNmwxMTIuMDYtLjI5YTE2IDE2IDAgMCAwIDE1LjkyLTE2VjM2OGExNiAxNiAwIDAgMSAxNi0xNmg2NGExNiAxNiAwIDAgMSAxNiAxNnY5NS42NGExNiAxNiAwIDAgMCAxNiAxNi4wNUw0NjQgNDgwYTE2IDE2IDAgMCAwIDE2LTE2VjMwMEwyOTUuNjcgMTQ4LjI2YTEyLjE5IDEyLjE5IDAgMCAwLTE1LjMgMHpNNTcxLjYgMjUxLjQ3TDQ4OCAxODIuNTZWNDQuMDVhMTIgMTIgMCAwIDAtMTItMTJoLTU2YTEyIDEyIDAgMCAwLTEyIDEydjcyLjYxTDMxOC40NyA0M2E0OCA0OCAwIDAgMC02MSAwTDQuMzQgMjUxLjQ3YTEyIDEyIDAgMCAwLTEuNiAxNi45bDI1LjUgMzFBMTIgMTIgMCAwIDAgNDUuMTUgMzAxbDIzNS4yMi0xOTMuNzRhMTIuMTkgMTIuMTkgMCAwIDEgMTUuMyAwTDUzMC45IDMwMWExMiAxMiAwIDAgMCAxNi45LTEuNmwyNS41LTMxYTEyIDEyIDAgMCAwLTEuNy0xNi45M3oiPjwvcGF0aD48L3N2Zz4=","storagePath":""},"label":"Home","url":"https://Footballtips.app"},{"icon":{"fileUrl":"data:image/svg+xml;base64,PHN2ZyBzdHJva2U9ImN1cnJlbnRDb2xvciIgZmlsbD0iIzAwMDAwMCIgc3Ryb2tlLXdpZHRoPSIwIiB2aWV3Qm94PSIwIDAgNTEyIDUxMiIgaGVpZ2h0PSIxZW0iIHdpZHRoPSIxZW0iIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+PHBhdGggZD0iTTUwNSAxNzQuOGwtMzkuNi0zOS42Yy05LjQtOS40LTI0LjYtOS40LTMzLjkgMEwxOTIgMzc0LjcgODAuNiAyNjMuMmMtOS40LTkuNC0yNC42LTkuNC0zMy45IDBMNyAzMDIuOWMtOS40IDkuNC05LjQgMjQuNiAwIDM0TDE3NSA1MDVjOS40IDkuNCAyNC42IDkuNCAzMy45IDBsMjk2LTI5Ni4yYzkuNC05LjUgOS40LTI0LjcuMS0zNHptLTMyNC4zIDEwNmM2LjIgNi4zIDE2LjQgNi4zIDIyLjYgMGwyMDgtMjA4LjJjNi4yLTYuMyA2LjItMTYuNCAwLTIyLjZMMzY2LjEgNC43Yy02LjItNi4zLTE2LjQtNi4zLTIyLjYgMEwxOTIgMTU2LjJsLTU1LjQtNTUuNWMtNi4yLTYuMy0xNi40LTYuMy0yMi42IDBMNjguNyAxNDZjLTYuMiA2LjMtNi4yIDE2LjQgMCAyMi42bDExMiAxMTIuMnoiPjwvcGF0aD48L3N2Zz4=","storagePath":""},"label":"Results","url":"https://www.footballtips.app/previous-results"}],"createdAt":1705435422236,"iconColor":"#1B237E","logo":{"fileUrl":"https://firebasestorage.googleapis.com/v0/b/fortuly-qa-env.appspot.com/o/eLa0AJZMNsdAsZzvKEdMFUviCK82%2Fprojects%2Ffootball-tips-1705435377316%2Flogo?alt=media&token=0342a5c2-12ed-4a53-84ea-e231d882b864","storagePath":"eLa0AJZMNsdAsZzvKEdMFUviCK82/projects/football-tips-1705435377316/logo"},"navbarBgColor":"#FFFFFF","subscriptionPlan":"ANDROID_ONLY","title":"Football Tips","uid":"football-tips-1705435377316","updatedAt":1705436068104,"url":"https://Footballtips.app"}';
  static const String PROJECT_DOMAIN = "web2app-maker-default-rtdb"; //"web2app-maker-default-rtdb"; // "fortuly-qa-env-default-rtdb";
  static const String APP_ID = "football-tips-1705435377316";

  static final AppDataManager _instance = AppDataManager._internal();
  AppData? appData;

  factory AppDataManager() {
    return _instance;
  }

  AppDataManager._internal();

  Future<void> initialize() async {
    await _loadDataFromLocal();
    if (_shouldFetchNewData()) {
      await _fetchDataFromAPI();
    }
  }

  Future<void> _loadDataFromLocal() async {
    // appData = AppData.fromJson(DEFAULT_JSON); // First initialize App Data from the Default JSON

    final prefs = await SharedPreferences.getInstance();
    final jsonString = prefs.getString('appData');
    if (jsonString != null) {
      print("LocalJson:");
      print(jsonString);
      appData = AppData.fromJson(jsonString);
      final lastLoadedTime = DateTime.parse(prefs.getString('lastLoadedTime') ?? DateTime.now().toIso8601String());
      if (DateTime.now().difference(lastLoadedTime).inHours >= 1) {
        await _fetchDataFromAPI();
      }
    } else {
      await _fetchDataFromAPI();
    }
  }

  Future<void> _fetchDataFromAPI() async {
    const url = "https://" + PROJECT_DOMAIN + ".firebaseio.com/PublishedAppStyle/" + APP_ID + ".json";
    try {
      final response = await http.get(Uri.parse(url));
      if (response.statusCode == 200) {
        final prefs = await SharedPreferences.getInstance();
        await prefs.setString('appData', response.body);
        await prefs.setString('lastLoadedTime', DateTime.now().toIso8601String());
        appData = AppData.fromJson(response.body);
      } else {
        print('Failed to fetch data. Status code: ${response.statusCode}');
      }
    } catch (e) {
      print('Error fetching data: $e');
    }
  }

  bool _shouldFetchNewData() {
    // Add logic if needed to determine if new data should be fetched
    return appData == null;
  }

  bool isSubscriptionValid() {
    if (AppDataManager().appData == null) {
      return false;
    } else if (AppDataManager().appData!.subscriptionPlan == null || AppDataManager().appData!.subscriptionPlan == "FREE_PLAN") {
      return false;
    } else if (Platform.isAndroid && appData!.subscriptionPlan != "ANDROID_ONLY" && appData!.subscriptionPlan != "ANDROID_AND_IOS") {
      return false;
    }  else if (Platform.isIOS && appData!.subscriptionPlan != "IOS_ONLY" && appData!.subscriptionPlan != "ANDROID_AND_IOS") {
      return false;
    } else {
      return true;
    }
  }

}