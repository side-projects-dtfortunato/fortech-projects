import 'package:flutter/material.dart';
import 'package:web2appflutter/AppDataManager.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:web2appflutter/shared/InitialAppManager.dart';
import 'package:web2appflutter/shared/data/OneSignal.manager.dart';
import 'package:web2appflutter/shared/pages/root-web-navigation.page.dart';

void main() async {
  await InitialAppManager().initMainApp();

  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});



  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return InitialAppManager().renderInitialBuild();
  }
}
