import 'dart:io';

import 'package:flutter/material.dart';
import 'package:web2appflutter/AppDataManager.dart';
import 'package:web2appflutter/shared/components/shared/TrialPopup.component.dart';
import 'package:web2appflutter/shared/components/web-navigation/WebNavigationAndroid.component.dart';
import 'package:web2appflutter/shared/components/web-navigation/WebNavigationiOS.component.dart';
import 'package:web2appflutter/shared/data/OneSignal.manager.dart';

class RootWebNavigation extends StatefulWidget {
  final String? notificationUrl;

  const RootWebNavigation({Key? key, this.notificationUrl}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _State();
}

class _State extends State<RootWebNavigation> {
  bool isInitializing = true;

  @override
  Widget build(BuildContext context) {
    if (!AppDataManager().isSubscriptionValid()) {
      WidgetsBinding.instance.addPostFrameCallback((_) {
        showDialog(
          context: context,
          builder: (context) => TrialPopup(),
          barrierDismissible: false,
        );
      });
    }

    final String initialUrl = createInitialUrl();

    if (Platform.isAndroid) {
      return WebNavigationAndroidComponent(url: initialUrl);
    } else {
      return WebNavigationiOSComponent(initialUrl: initialUrl);
    }
  }

  String createInitialUrl() {
    // Always start with the default URL
    String initialUrl = AppDataManager().appData!.url;
    String? oneSignalUserId = OneSignalManager().userId;

    if (oneSignalUserId != null && oneSignalUserId.isNotEmpty) {
      final Uri currentUri = Uri.parse(initialUrl);
      final Map<String, dynamic> queryParams = Map.from(currentUri.queryParameters);
      queryParams['oneSignalUserId'] = oneSignalUserId;

      initialUrl = Uri(
        scheme: currentUri.scheme,
        host: currentUri.host,
        path: currentUri.path,
        queryParameters: queryParams,
      ).toString();
    }

    return initialUrl;
  }
}