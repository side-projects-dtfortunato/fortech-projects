import 'dart:io';
import 'dart:async';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:web2appflutter/AppDataManager.dart';
import 'package:web2appflutter/shared/data/OneSignal.manager.dart';
import 'package:web2appflutter/shared/pages/root-web-navigation.page.dart';
import 'package:app_tracking_transparency/app_tracking_transparency.dart';
import 'package:connectivity_plus/connectivity_plus.dart';

class InitialAppManager {
  static final InitialAppManager _instance = InitialAppManager._internal();
  factory InitialAppManager() {
    return _instance;
  }
  InitialAppManager._internal();

  late StreamSubscription<List<ConnectivityResult>> _connectivitySubscription;
  String? _initialNotificationUrl;

  Future<void> initMainApp() async {
    WidgetsFlutterBinding.ensureInitialized();
    await AppDataManager().initialize();

    if (AppDataManager().appData?.askFilesPermission == true) {
      await Permission.camera.request();
      await Permission.storage.request();
    }

    if (Platform.isIOS) {
      final status = await AppTrackingTransparency.requestTrackingAuthorization();
    }

    await OneSignalManager().initialize();
    
    _initialNotificationUrl = OneSignalManager().dataOpenUrl;

    // Initialize connectivity checking
    await _initConnectivity();
  }

  Future<void> _initConnectivity() async {
    final connectivityResults = await Connectivity().checkConnectivity();
    _handleConnectivityChange(connectivityResults);

    _connectivitySubscription = Connectivity().onConnectivityChanged.listen(_handleConnectivityChange);
  }

  void _handleConnectivityChange(List<ConnectivityResult> results) {
    if (results.isEmpty || results.every((result) => result == ConnectivityResult.none)) {
      _showReconnectDialog();
    }
  }

  void _showReconnectDialog() {
    showDialog(
      context: navigatorKey.currentContext!,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('No Internet Connection'),
          content: Text('Please check your internet connection and try again.'),
          actions: <Widget>[
            TextButton(
              child: Text('Retry'),
              onPressed: () async {
                Navigator.of(context).pop();
                final connectivityResults = await Connectivity().checkConnectivity();
                if (connectivityResults.isNotEmpty && connectivityResults.any((result) => result != ConnectivityResult.none)) {
                  // Internet is back, you can perform any necessary actions here
                } else {
                  _showReconnectDialog();
                }
              },
            ),
          ],
        );
      },
    );
  }

  Widget renderInitialBuild() {
    return MaterialApp(
      navigatorKey: navigatorKey,
      title: AppDataManager().appData!.title,
      theme: ThemeData(
        primarySwatch: AppDataManager().appData!.getPrimaryColor(),
      ),
      home: RootWebNavigation(notificationUrl: _initialNotificationUrl),
    );
  }

  final GlobalKey<NavigatorState> navigatorKey = GlobalKey<NavigatorState>();

  void dispose() {
    _connectivitySubscription.cancel();
  }
}