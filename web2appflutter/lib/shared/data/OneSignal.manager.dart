import 'package:onesignal_flutter/onesignal_flutter.dart';
import 'package:web2appflutter/AppDataManager.dart';

class OneSignalManager {
  static final KEY_OPENURL = "openUrl";


  static final OneSignalManager _instance = OneSignalManager._internal();

  factory OneSignalManager() {
    return _instance;
  }

  OneSignalManager._internal() {}

  String? dataOpenUrl;

  Function? onRefreshUrl;

  String? lastUserId;

  String? userId;

  Future<void> initialize() async {
    if (AppDataManager().appData?.externalIntegrations?.oneSignalAppId != null &&
        AppDataManager().appData!.externalIntegrations!.oneSignalAppId!.isNotEmpty) {

      // Initialize OneSignal
      OneSignal.shared.setAppId(AppDataManager().appData!.externalIntegrations!.oneSignalAppId!);

      // Request notification permission
      OneSignal.shared.promptUserForPushNotificationPermission().then((accepted) {
        print("Accepted permission: $accepted");
      });

      // Get OneSignal User ID
      try {
        userId = (await OneSignal.shared.getDeviceState())?.userId;
        print('OneSignal User ID: $userId');

        // Store or use the oneSignalUserId as needed
        // You might want to store this in AppDataManager or another state management solution

      } catch (e) {
        print('Error getting OneSignal User ID: $e');
      }

      // Setting notification opened handler
      OneSignal.shared.setNotificationOpenedHandler((openedResult) {
        var additionalData = openedResult.notification.additionalData;
        if (additionalData != null && additionalData.containsKey(KEY_OPENURL)) {
          this.dataOpenUrl = additionalData[KEY_OPENURL];

          if (onRefreshUrl != null) {
            onRefreshUrl!();
          }
        }
      });
    }
  }

  void loginUser(String userId) {
    if (lastUserId != userId) {
      OneSignal.shared.setExternalUserId(userId);
      lastUserId = userId;
    }
  }

  void addUserTags(Map<String, dynamic> tags) {
    // Convert all values to strings as required by OneSignal
    final Map<String, String> stringTags = tags.map(
            (key, value) => MapEntry(key, value.toString())
    );

    // Add tags to OneSignal
    OneSignal.shared.sendTags(stringTags).then((_) {
      print('Successfully added tags to OneSignal');
    }).catchError((error) {
      print('Error adding tags to OneSignal: $error');
    });
  }
  
  void logout() {
    if (lastUserId != null) {
      OneSignal.shared.removeExternalUserId();
      lastUserId = null;
    }
  }


  void cleanData() {
    dataOpenUrl = null;
  }

}