import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:web2appflutter/shared/data/Utils.dart';
import 'package:web2appflutter/shared/data/model-data/CustomerAppConfigs.model.dart';

class AppData {
  final String title;
  final String uid;
  final String url;
  final String? subscriptionPlan;
  final String navbarBgColor;
  final String iconColor;
  final int createdAt;
  final int updatedAt;
  final String logoFileUrl;
  final List<BottomNavbarItem> bottomNavbar;
  final ExternalIntegrations? externalIntegrations;
  final bool? askFilesPermission;
  final bool? hasOAuthSignin;
  final bool? redirectExternalLinks;

  AppData({
    required this.title,
    required this.uid,
    required this.url,
    this.subscriptionPlan,
    required this.navbarBgColor,
    required this.iconColor,
    required this.createdAt,
    required this.updatedAt,
    required this.logoFileUrl,
    required this.bottomNavbar,
    this.externalIntegrations,
    this.askFilesPermission,
    this.hasOAuthSignin,
    this.redirectExternalLinks,
  });

  static AppData fromJson(String jsonString) {
    final parsedJson = json.decode(jsonString);
    var bottomNavbarList = parsedJson['bottomNavbar'];
    List<BottomNavbarItem> bottomNavbarItems = [];
    if (bottomNavbarList != null) {
      bottomNavbarList.forEach((item) {
        bottomNavbarItems.add(BottomNavbarItem.fromJson(item));
      });
    }
    // List<BottomNavbarItem> bottomNavbarItems = bottomNavbarList != null ? bottomNavbarList.map((item) => BottomNavbarItem.fromJson(item)).toList() as List<BottomNavbarItem> : [];

    return AppData(
      title: parsedJson['title'],
      uid: parsedJson['uid'],
      url: parsedJson['url'],
      subscriptionPlan: parsedJson['subscriptionPlan'],
      navbarBgColor: parsedJson['navbarBgColor'],
      iconColor: parsedJson['iconColor'],
      createdAt: parsedJson['createdAt'],
      updatedAt: parsedJson['updatedAt'],
      logoFileUrl: parsedJson['logo']['fileUrl'],
      bottomNavbar: bottomNavbarItems,
      externalIntegrations: parsedJson['externalIntegrations'] != null
          ? ExternalIntegrations.fromJson(parsedJson['externalIntegrations'])
          : null,
      askFilesPermission: parsedJson['askFilesPermission'] ?? false,
      hasOAuthSignin: parsedJson['hasOAuthSignin'] ?? false,
      redirectExternalLinks: parsedJson['redirectExternalLinks'] ?? true,
    );
  }

  MaterialColor getPrimaryColor() {
    return Utils.convertToMaterialColor(iconColor);
  }

  List<TabPageData> getListTabPageData() {
    List<TabPageData> listTabs = [];

    if (bottomNavbar != null && bottomNavbar.length > 0) {
      listTabs = bottomNavbar.map((item) => TabPageData(item.label, item.icon, item.url)).toList();
    }
    return listTabs;
  }
}

class BottomNavbarItem {
  final String label;
  final String url;
  final String? icon;

  BottomNavbarItem({
    required this.label,
    required this.url,
    this.icon,
  });

  static BottomNavbarItem fromJson(Map<String, dynamic> json) {
    print("BottomNavbarItem.fromJson: " + json.toString());
    return BottomNavbarItem(
      label: json['label'],
      url: json['url'],
      icon: json['icon']?['fileUrl'], // Assuming icon is a nested object with 'fileUrl'
    );
  }
}

class ExternalIntegrations {
  final String oneSignalAppId;

  ExternalIntegrations({
    required this.oneSignalAppId,
  });

  static ExternalIntegrations fromJson(Map<String, dynamic> json) {
    return ExternalIntegrations(
        oneSignalAppId: json['oneSignalAppId']
    );
  }
}