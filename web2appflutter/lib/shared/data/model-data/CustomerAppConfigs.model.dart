import 'package:flutter/cupertino.dart';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:web2appflutter/AppDataManager.dart';

class CustomerAppConfigs {
  final String uid;
  final String appName;

  CustomerAppConfigs(this.uid, this.appName);

  CustomerAppConfigs.fromJson(Map<String, dynamic> json)
      : uid = json['uid'],
        appName = json['appName'];

  Map<String, dynamic> toJson() => {
    'uid': uid,
    'appName': appName,
  };
}

class TabPageData {
  final String label;
  final String url;
  final String? iconBase64;

  TabPageData(this.label, this.iconBase64, this.url);

  Widget getIconImage(bool isActive) {
    if (this.iconBase64 != null) {
      // Remove the data URI prefix (if present)
      final dataPrefix = 'data:image/svg+xml;base64,';
      final cleanedBase64String =
      iconBase64!.startsWith(dataPrefix) ? iconBase64!.substring(dataPrefix.length) : iconBase64;



      // Decode the Base64 string into bytes
      final List<int> bytes = base64Decode(cleanedBase64String!);

      // Convert bytes to a String
      final String svgString = utf8.decode(bytes);

      // Use SvgPicture.string to display the SVG image
      return SvgPicture.string(
        svgString,
        // You can add more properties like width, height, color, etc.
        width: 20,
        height: 20,
        color: isActive ? AppDataManager().appData!.getPrimaryColor().shade500 : AppDataManager().appData!.getPrimaryColor().shade200,
      );
    } else {
      return Container();
    }

  }
}
