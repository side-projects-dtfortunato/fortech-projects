import 'package:flutter/material.dart';

class Utils {
  static MaterialColor convertToMaterialColor(String hexColor) {
    Color color = Color(int.parse(hexColor.substring(1), radix: 16));
    // The primary color is the color itself
    Map<int, Color> colorMap = {
      50: color.withOpacity(0.1),
      100: color.withOpacity(0.2),
      200: color.withOpacity(0.3),
      300: color.withOpacity(0.4),
      400: color.withOpacity(0.5),
      500: color.withOpacity(0.6), // This is the primary color
      600: color.withOpacity(0.7),
      700: color.withOpacity(0.8),
      800: color.withOpacity(0.9),
      900: color.withOpacity(1.0),
    };

    return MaterialColor(color.value, colorMap);
  }

  static Color hexStringToColor(String hexColor, {int alpha = 0xFF}) {
    // Remove the '#' character if present
    hexColor = hexColor.replaceAll('#', '');

    // Parse the hex color string to an integer
    int hexValue = int.tryParse(hexColor, radix: 16)!;

    // Create a Color object from the integer value
    return hexValue != null
        ? Color(hexValue).withAlpha(alpha) // Ensure alpha is set to 255 (opaque)
        : Colors.transparent; // Return transparent color if parsing fails
  }

  static bool isUrlSameDomain(String url1, String url2) {
    Uri uri1 = Uri.parse(url1);
    Uri uri2 = Uri.parse(url2);

    List<String> parts1 = uri1.host.split('.').reversed.toList();
    List<String> parts2 = uri2.host.split('.').reversed.toList();

    // Compare the parts (subdomains) in reverse order
    int minLength = parts1.length < parts2.length ? parts1.length : parts2.length;
    for (int i = 0; i < minLength; i++) {
      if (parts1[i] != parts2[i]) {
        return false;
      }
    }

    return true;
  }

}