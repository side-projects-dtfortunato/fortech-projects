import 'package:flutter/material.dart';
import 'package:flutter_inappwebview/flutter_inappwebview.dart';
import 'package:url_launcher/url_launcher.dart';

class SimpleWebViewComponent extends StatefulWidget {
  final String initialUrl;

  const SimpleWebViewComponent({Key? key, required this.initialUrl}) : super(key: key);

  @override
  _SimpleWebViewComponentState createState() => _SimpleWebViewComponentState();
}

class _SimpleWebViewComponentState extends State<SimpleWebViewComponent> {
  InAppWebViewController? _webViewController;
  String _currentUrl = '';
  bool _isLoading = true;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Stack(
          children: [
            InAppWebView(
              initialUrlRequest: URLRequest(url: WebUri(widget.initialUrl)),
              initialOptions: InAppWebViewGroupOptions(
                crossPlatform: InAppWebViewOptions(
                  useShouldOverrideUrlLoading: true,
                  mediaPlaybackRequiresUserGesture: false,
                ),
                ios: IOSInAppWebViewOptions(
                  allowsInlineMediaPlayback: true,
                ),
              ),
              onWebViewCreated: (controller) {
                _webViewController = controller;
              },
              onLoadStart: (controller, url) {
                setState(() {
                  _isLoading = true;
                  _currentUrl = url.toString();
                });
                print("Page load started: $_currentUrl");
              },
              onLoadStop: (controller, url) {
                setState(() {
                  _isLoading = false;
                  _currentUrl = url.toString();
                });
                print("Page load finished: $_currentUrl");
              },
              shouldOverrideUrlLoading: (controller, navigationAction) async {
                var uri = navigationAction.request.url!;
                print("Intercepted URL: ${uri.toString()}");

                // Only handle main frame navigation
                if (navigationAction.isForMainFrame) {
                  if (!_isSameDomain(uri.toString(), widget.initialUrl)) {
                    print("External navigation detected. Opening in external browser.");
                    await _launchExternalUrl(uri.toString());
                    return NavigationActionPolicy.CANCEL;
                  }
                }
                return NavigationActionPolicy.ALLOW;
              },
              onReceivedError: (controller, request, error) {
                print("WebView Error: $error");
              },
            )
          ],
        ),
      ),
    );
  }

  bool _isSameDomain(String url, String baseUrl) {
    Uri uri = Uri.parse(url);
    Uri baseUri = Uri.parse(baseUrl);

    String domain = _extractDomain(uri.host);
    String baseDomain = _extractDomain(baseUri.host);

    print('Comparing domains: $domain and $baseDomain');

    return domain == baseDomain;
  }

  String _extractDomain(String host) {
    var parts = host.split('.');
    if (parts.length > 2) {
      return parts.sublist(parts.length - 2).join('.');
    }
    return host;
  }

  Future<void> _launchExternalUrl(String url) async {
    print('Launching external URL: $url');
    if (await canLaunch(url)) {
      await launch(url, forceSafariVC: false, forceWebView: false);
    } else {
      print('Could not launch $url');
    }
  }
}