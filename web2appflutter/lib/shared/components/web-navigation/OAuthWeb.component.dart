import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:webview_flutter/webview_flutter.dart';

class OAuthWebComponent extends StatefulWidget {

  final String url;


  const OAuthWebComponent({Key? key, required this.url}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _State();

  static void openPage({context, url}) {
    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => OAuthWebComponent(url: url,)),
    );
  }

}

class _State extends State<OAuthWebComponent> {

  WebViewController? controller;

  @override
  void initState() {
    super.initState();
    controller = WebViewController()
    ..setUserAgent("random")
      ..setJavaScriptMode(JavaScriptMode.unrestricted)
      ..setBackgroundColor(const Color(0x00000000))
      ..setNavigationDelegate(
        NavigationDelegate(
          onProgress: (int progress) {
            // Update loading bar.
          },
          onPageStarted: (String url) {
            print("onPageStarted: " + url);
          },
          onPageFinished: (String url) {
            print("onPageFinished: " + url);
          },
          onWebResourceError: (WebResourceError error) {
          },
          onNavigationRequest: (NavigationRequest request) {
            print("onNavigationRequest: " + request.url);
            /*if (request.url.startsWith('https://www.youtube.com/')) {
              return NavigationDecision.prevent;
            }*/
            return NavigationDecision.navigate;
          },
        ),
      )
      ..loadRequest(Uri.parse(widget.url));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(body: SafeArea(
      child: Expanded(
        child: WebViewWidget(controller: controller!),
      ),
    ));
  }

}