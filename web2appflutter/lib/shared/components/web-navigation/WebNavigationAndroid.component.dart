import 'dart:async';
import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:getwidget/components/button/gf_button.dart';
import 'package:web2appflutter/AppDataManager.dart';
import 'package:web2appflutter/shared/components/shared/bottom-navigation.component.dart';
import 'package:web2appflutter/shared/components/splash-screen/SplashScreen.component.dart';
import 'package:web2appflutter/shared/data/OneSignal.manager.dart';
import 'package:web2appflutter/shared/data/Utils.dart';
import 'package:webview_flutter/webview_flutter.dart';
import 'package:url_launcher/url_launcher.dart';

class WebNavigationAndroidComponent extends StatefulWidget {
  final String url;

  const WebNavigationAndroidComponent({Key? key, required this.url}) : super(key: key);

  @override
  State<WebNavigationAndroidComponent> createState() => _State();
}

class _State extends State<WebNavigationAndroidComponent> {
  bool hasError = false;
  bool hasInitialized = false;
  bool displaySplashScreen = true;
  bool _hasHandledInitialNotification = false;
  final GlobalKey webViewKey = GlobalKey();

  String url = "";
  double progress = 0;

  WebViewController? webViewController;

  String getBaseUrl() {
    // If we have a pending notification URL in OneSignal, use that instead
    return widget.url;
  }

  @override
  void initState() {
    super.initState();

    runTimeoutDisplayingSplashScreen();

    webViewController = WebViewController()
      ..setJavaScriptMode(JavaScriptMode.unrestricted)
      ..setBackgroundColor(const Color(0xFFFFFFFF))
      ..addJavaScriptChannel(
        'FlutterWebView',
        onMessageReceived: (JavaScriptMessage message) {
          handleUserIdMessage(message);
        },
      )
      ..setNavigationDelegate(
        NavigationDelegate(
          onProgress: (int progress) {
            setState(() {
              this.progress = progress / 100;
            });
          },
          onPageStarted: (String url) {
            print('Page started loading: $url');
          },
          onPageFinished: (String url) async {
            print('Page finished loading: $url');
            setState(() {
              hasInitialized = true;
            });
            
            // Only handle notification URL once and only after default page is loaded
            if (hasInitialized && !_hasHandledInitialNotification && OneSignalManager().dataOpenUrl != null) {
              _hasHandledInitialNotification = true;
              // Add a small delay to ensure the default page is fully loaded
              Future.delayed(Duration(milliseconds: 500), () {
                onLoadNotificationUrl();
              });
            }
          },
          onNavigationRequest: (NavigationRequest request) {
            print('Navigation request to: ${request.url}');
            if (shouldLaunchExternally(request.url)) {
              print('Launching externally: ${request.url}');
              launchExternalUrl(request.url);
              return NavigationDecision.prevent;
            }
            print('Navigating internally: ${request.url}');
            return NavigationDecision.navigate;
          },
        ),
      )
      ..loadRequest(Uri.parse(getBaseUrl()));

    if (AppDataManager().appData?.hasOAuthSignin == true) {
      webViewController!.setUserAgent("random");
    }

    // Initialize OneSignal after WebView setup
    onOneSignalInit();
  }

  // Add this method to handle messages from webapp
  void handleUserIdMessage(JavaScriptMessage message) {
    try {
      final data = jsonDecode(message.message);
      if (data['type'] == 'userId') {
        final String? userId = data['userId'];
        print('Received userId from webapp: $userId');
        // Here you can use the userId with OneSignal
        if (userId != null && !userId.isEmpty) {
          OneSignalManager().loginUser(userId);
        } else {
          OneSignalManager().logout();
        }
      } else if (data['type'] == 'userTags') {
        // Get user tags and add them to the user on OneSignal
        final Map<String, dynamic>? tags = data['tags'];
        print('Received user tags from webapp: $tags');

        if (tags != null && tags.isNotEmpty) {
          // Convert all values to strings as required by OneSignal
          OneSignalManager().addUserTags(tags);
        }
      }
    } catch (e) {
      print('Error handling message: $e');
    }
  }

  bool shouldLaunchExternally(String url) {
    Uri baseUri = normalizeUri(Uri.parse(getBaseUrl()));
    Uri requestUri = normalizeUri(Uri.parse(url));

    print('Normalized Base URL: $baseUri');
    print('Normalized Request URL: $requestUri');

    // Check if the requested URL is a subpage of the base URL
    bool isSameHost = requestUri.host == baseUri.host;
    bool isSubpath = requestUri.path.startsWith(baseUri.path);

    print('Is same host: $isSameHost');
    print('Is subpath: $isSubpath');

    return !isSameHost || !isSubpath;
  }

  Uri normalizeUri(Uri uri) {
    String host = uri.host;
    if (host.startsWith('www.')) {
      host = host.substring(4);
    }
    return uri.replace(host: host);
  }

  void launchExternalUrl(String url) async {
    print('Attempting to launch URL: $url');
    if (await canLaunch(url)) {
      await launchUrl(Uri.parse(url), mode: LaunchMode.inAppBrowserView);
    } else {
      print('Could not launch $url');
    }
  }

  void loadWebviewUrl(String? urlToLoad) {
    if (urlToLoad != null) {
      setState(() {
        url = urlToLoad;
      });
      webViewController!.loadRequest(Uri.parse(urlToLoad));
    }
  }

  void onOneSignalInit() {
    OneSignalManager().onRefreshUrl = onLoadNotificationUrl;
    onLoadNotificationUrl();
  }

  void onLoadNotificationUrl() {
    final notificationUrl = OneSignalManager().dataOpenUrl;
    if (notificationUrl != null && 
        notificationUrl.isNotEmpty && 
        notificationUrl != url) {
      // Add a small delay to ensure the default page is fully loaded
      Future.delayed(Duration(milliseconds: 500), () {
        loadWebviewUrl(notificationUrl);
      });
    }
  }

  void runTimeoutDisplayingSplashScreen() {
    Future.delayed(const Duration(seconds: 5), () {
      setState(() {
        displaySplashScreen = false;
      });
    });
  }

  @override
  void dispose() {
    super.dispose();
  }

  Future<bool> _exitApp(BuildContext context) async {
    String? currentUrl = (await webViewController!.currentUrl());

    bool isRoot = currentUrl == widget.url;
    if (webViewController != null && await webViewController!.canGoBack() && !isRoot) {
      webViewController!.goBack();
      return Future.value(false);
    } else {
      return Future.value(true);
    }
  }

  Widget renderWebview() {
    return SafeArea(
        child: Column(
            children: <Widget>[
              Expanded(
                child: Stack(
                  alignment: AlignmentDirectional.bottomCenter,
                  children: [
                    WebViewWidget(controller: webViewController!),
                    progress < 1
                        ? LinearProgressIndicator(
                      value: progress,
                      backgroundColor: AppDataManager().appData!.getPrimaryColor().shade100,
                      color: AppDataManager().appData!.getPrimaryColor().shade400,
                    )
                        : Container(),
                    !hasInitialized || displaySplashScreen ? SplashScreenComponent() : Container(),
                    !hasInitialized || displaySplashScreen
                        ? Container()
                        : BottomNavigationComponent((String url) {
                      loadWebviewUrl(url);
                    }, AppDataManager().appData!.getListTabPageData())
                  ],
                ),
              ),
            ]
        )
    );
  }

  void onReload() async {
    setState(() {
      hasError = false;
    });

    loadWebviewUrl(url);
  }

  Widget renderErrorScreen() {
    return Scaffold(
      body: SafeArea(
        child: Center(
          child: Padding(
            padding: const EdgeInsets.all(10),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                const Text("Some error occurred. Verify if you have internet and restart your app please...", textAlign: TextAlign.center, style: TextStyle(fontSize: 18)),
                GFButton(
                  text: "Retry",
                  onPressed: onReload,
                  color: Utils.hexStringToColor(AppDataManager().appData!.iconColor),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    if (hasError) {
      return renderErrorScreen();
    } else {
      return WillPopScope(
        child: Scaffold(body: renderWebview()),
        onWillPop: () => _exitApp(context),
      );
    }
  }

  String? extractQueryParameter(String url, String queryParameter) {
    Uri uri = Uri.parse(url);
    Map<String, String> queryParameters = uri.queryParameters;
    return queryParameters[queryParameter];
  }
}