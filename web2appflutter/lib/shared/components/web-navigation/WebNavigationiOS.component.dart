import 'package:flutter/material.dart';
import 'package:flutter_inappwebview/flutter_inappwebview.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:web2appflutter/AppDataManager.dart';
import 'dart:convert';

import 'package:web2appflutter/shared/data/OneSignal.manager.dart';
import 'package:web2appflutter/shared/components/splash-screen/SplashScreen.component.dart';
import 'package:web2appflutter/shared/components/shared/bottom-navigation.component.dart';

class WebNavigationiOSComponent extends StatefulWidget {
  final String initialUrl;

  const WebNavigationiOSComponent({Key? key, required this.initialUrl})
      : super(key: key);

  @override
  _WebNavigationiOSComponentState createState() => _WebNavigationiOSComponentState();
}

class _WebNavigationiOSComponentState extends State<WebNavigationiOSComponent> {
  InAppWebViewController? _webViewController;
  String _currentUrl = '';
  bool _isLoading = true;
  bool _hasHandledInitialNotification = false;
  bool displaySplashScreen = true;
  bool hasInitialized = false;
  double progress = 0;

  @override
  void initState() {
    super.initState();
    // Initialize OneSignal after WebView setup
    onOneSignalInit();
    runTimeoutDisplayingSplashScreen();
  }

  void runTimeoutDisplayingSplashScreen() {
    Future.delayed(const Duration(seconds: 5), () {
      setState(() {
        displaySplashScreen = false;
        hasInitialized = true;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Stack(
          alignment: AlignmentDirectional.bottomCenter,
          children: [
            InAppWebView(
              initialUrlRequest: URLRequest(url: WebUri(AppDataManager().appData!.url)),
              initialOptions: InAppWebViewGroupOptions(
                crossPlatform: InAppWebViewOptions(
                  useShouldOverrideUrlLoading: true,
                  mediaPlaybackRequiresUserGesture: false,
                  javaScriptEnabled: true,
                ),
                ios: IOSInAppWebViewOptions(
                  allowsInlineMediaPlayback: true,
                ),
              ),
              onWebViewCreated: (controller) {
                _setupWebView(controller);
              },
              onLoadStart: (controller, url) {
                setState(() {
                  _isLoading = true;
                  _currentUrl = url.toString();
                });
                print("Page load started: $_currentUrl");
              },
              onProgressChanged: (controller, progress) {
                setState(() {
                  this.progress = progress / 100;
                });
              },
              onLoadStop: (controller, url) async {
                setState(() {
                  _isLoading = false;
                  _currentUrl = url.toString();
                  hasInitialized = true;
                });
                
                if (!_isLoading && !_hasHandledInitialNotification && OneSignalManager().dataOpenUrl != null) {
                  _hasHandledInitialNotification = true;
                  Future.delayed(Duration(milliseconds: 500), () {
                    onLoadNotificationUrl();
                  });
                }

                await _injectFlutterWebViewInterface(controller);
              },
              shouldOverrideUrlLoading: (controller, navigationAction) async {
                var uri = navigationAction.request.url!;
                print("Intercepted URL: ${uri.toString()}");

                if (navigationAction.isForMainFrame) {
                  if (!_isSameDomain(uri.toString(), widget.initialUrl)) {
                    print("External navigation detected. Opening in external browser.");
                    await _launchExternalUrl(uri.toString());
                    return NavigationActionPolicy.CANCEL;
                  }
                }
                return NavigationActionPolicy.ALLOW;
              },
              onReceivedError: (controller, request, error) {
                print("WebView Error: $error");
              },
            ),
            progress < 1
                ? LinearProgressIndicator(
                    value: progress,
                    backgroundColor: AppDataManager().appData!.getPrimaryColor().shade100,
                    color: AppDataManager().appData!.getPrimaryColor().shade400,
                  )
                : Container(),
            (!hasInitialized || displaySplashScreen) 
                ? SplashScreenComponent() 
                : Container(),
            (!hasInitialized || displaySplashScreen)
                ? Container()
                : BottomNavigationComponent((String url) {
                    _webViewController?.loadUrl(
                      urlRequest: URLRequest(url: WebUri(url))
                    );
                  }, AppDataManager().appData!.getListTabPageData()),
          ],
        ),
      ),
    );
  }

  void onOneSignalInit() {
    OneSignalManager().onRefreshUrl = onLoadNotificationUrl;
    onLoadNotificationUrl();
  }

  void onLoadNotificationUrl() {
    final notificationUrl = OneSignalManager().dataOpenUrl;
    if (notificationUrl != null && 
        notificationUrl.isNotEmpty && 
        notificationUrl != _currentUrl) {
      _webViewController?.loadUrl(
        urlRequest: URLRequest(url: WebUri(notificationUrl))
      );
    }
  }

  void _setupWebView(InAppWebViewController controller) {
    _webViewController = controller;
    _setupJavaScriptHandler(controller);
  }

  void _setupJavaScriptHandler(InAppWebViewController controller) {
    controller.addJavaScriptHandler(
      handlerName: 'FlutterWebView',
      callback: (args) {
        print("Function _setupJavaScriptHandler called: ${args}");
        if (args.isNotEmpty) {
          try {
            final message = jsonDecode(args[0].toString());
            if (message['type'] == 'userId') {
              final userId = message['userId'];
              print('Received userId from web: $userId');
              // Handle the userId here
              _handleUserId(userId);
            } else if (message['type'] == 'userTags') {
              // Get user tags and add them to the user on OneSignal
              final Map<String, dynamic>? tags = message['tags'];
              print('Received user tags from webapp: $tags');

              if (tags != null && tags.isNotEmpty) {
                // Convert all values to strings as required by OneSignal
                OneSignalManager().addUserTags(tags);
              }
            }
          } catch (e) {
            print('Error processing message: $e');
          }
        }
      },
    );
  }

  Future<void> _injectFlutterWebViewInterface(InAppWebViewController controller) async {
    await controller.evaluateJavascript(source: '''
      window.FlutterWebView = {
        postMessage: function(message) {
          window.flutter_inappwebview.callHandler('FlutterWebView', message);
        }
      };
    ''');
  }

  void _handleUserId(String? userId) {
    if (userId != null && userId.isNotEmpty) {
      OneSignalManager().loginUser(userId);
    } else {
      OneSignalManager().logout();
    }
  }

  bool _isSameDomain(String url, String baseUrl) {
    Uri uri = Uri.parse(url);
    Uri baseUri = Uri.parse(baseUrl);

    String domain = _extractDomain(uri.host);
    String baseDomain = _extractDomain(baseUri.host);

    print('Comparing domains: $domain and $baseDomain');

    return domain == baseDomain;
  }

  String _extractDomain(String host) {
    var parts = host.split('.');
    if (parts.length > 2) {
      return parts.sublist(parts.length - 2).join('.');
    }
    return host;
  }

  Future<void> _launchExternalUrl(String url) async {
    print('Launching external URL: $url');
    if (await canLaunch(url)) {
      await launchUrl(Uri.parse(url), mode: LaunchMode.inAppBrowserView);
    } else {
      print('Could not launch $url');
    }
  }
}