import 'package:flutter/material.dart';
import 'package:flutter_timer_countdown/flutter_timer_countdown.dart';
import 'package:getwidget/components/button/gf_button.dart';
import 'package:url_launcher/url_launcher.dart';

class TrialPopup extends StatefulWidget {
  @override
  _TrialPopupState createState() => _TrialPopupState();
}

class _TrialPopupState extends State<TrialPopup> {
  bool isButtonEnabled = false;

  @override
  Widget build(BuildContext context) {
    return Dialog(
      child: Padding(
        padding: EdgeInsets.all(20.0),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Image.network('https://studio.webtoapp.app/images/logo.png'), // Substitua com a URL do seu logo
            SizedBox(height: 20),
            Text(
              "This app was generated using WebToApp.app and it's in a Free Trial Version, that's why you are seeing this message.",
              textAlign: TextAlign.center,
            ),
            SizedBox(height: 20),
            TimerCountdown(
              format: CountDownTimerFormat.minutesSeconds,
              endTime: DateTime.now().add(Duration(seconds: 15)),
              enableDescriptions: false,
              onEnd: () {
                setState(() {
                  isButtonEnabled = true;
                });
              },
              timeTextStyle: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
            ),
            SizedBox(height: 20),
            GFButton(onPressed: isButtonEnabled ? () => Navigator.of(context).pop() : null,
            text: isButtonEnabled ? "Proceed" : "Wait...",
            color: isButtonEnabled ? Colors.green : Colors.grey,),
            GFButton(
              onPressed: () => _launchURL('https://WebToApp.app'),
              text: "Convert a Website to an App",
              color: Colors.blue
            ),
          ],
        ),
      ),
    );
  }

  void _launchURL(String url) async {
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Not possible to open $url';
    }
  }
}