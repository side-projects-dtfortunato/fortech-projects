import 'package:floating_bottom_navigation_bar/floating_bottom_navigation_bar.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:web2appflutter/AppDataManager.dart';
import 'package:web2appflutter/shared/data/Utils.dart';
import 'package:web2appflutter/shared/data/model-data/CustomerAppConfigs.model.dart';

class BottomNavigationComponent extends StatefulWidget {

  Function onSelectedPage;
  List<TabPageData> tabPagesData;

  BottomNavigationComponent(this.onSelectedPage, this.tabPagesData);

  @override
  State<StatefulWidget> createState() => _State();



}

class _State extends State<BottomNavigationComponent> {
  int _selectedTabIndex = 0;


  @override
  void initState() {
    super.initState();
    this._selectedTabIndex = 0;
  }

  void onSelectedTab(int tabIndex) {
    setState(() {
      this._selectedTabIndex = tabIndex;
      widget.onSelectedPage(widget.tabPagesData[tabIndex].url);
    });
  }

  List<FloatingNavbarItem> generateNavbarItems() {
    List<FloatingNavbarItem> listNavbarItems = [];

    widget.tabPagesData.forEach((tabItem) {
      var itemIndex = widget.tabPagesData.indexOf(tabItem);

      listNavbarItems.add(FloatingNavbarItem(customWidget: tabItem.getIconImage(itemIndex == this._selectedTabIndex), title: tabItem.label));
    });

    return listNavbarItems;
  }


  @override
  Widget build(BuildContext context) {
    if (widget.tabPagesData.length > 0) {
      return FloatingNavbar(
        margin: EdgeInsets.only(top: 0, bottom: 0),
        padding: EdgeInsets.only(top: 2, bottom: 2),
        borderRadius: 10,
        backgroundColor: Utils.hexStringToColor(AppDataManager().appData!.navbarBgColor),
        selectedBackgroundColor: Utils.hexStringToColor(AppDataManager().appData!.navbarBgColor),
        unselectedItemColor: Utils.hexStringToColor(AppDataManager().appData!.iconColor, alpha: 0x66),
        selectedItemColor: Utils.hexStringToColor(AppDataManager().appData!.iconColor),
        onTap: (int val) {
          this.onSelectedTab(val);
        },
        currentIndex: this._selectedTabIndex,
        items: this.generateNavbarItems(),
      );
    } else {
      return Container(height: 1,);
    }

  }



}