import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:web2appflutter/AppDataManager.dart';

class SplashScreenComponent extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(10),
      color: Colors.white,
      child: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Image.asset('assets/images/logo.png', width: 200),
            SizedBox(height: 16),
            Text(
              AppDataManager().appData!.title,
              style: TextStyle(
                fontSize: 24,
                fontWeight: FontWeight.bold,
              ),
            ),
          ],
        ),
      ),
    );
  }
}