/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./src/pages/**/*.{js,ts,jsx,tsx}",
    "./src/components/**/*.{js,ts,jsx,tsx}",
  ],
  theme: {
    extend: {},
  },
  plugins: [require("@tailwindcss/typography"), require("daisyui")],
  daisyui: {
    themes: [
      {
        mytheme: {
          "primary": "#D22730",          // Canadian red

          "secondary": "#0E3A6B",        // Deep blue for professionalism

          "accent": "#2E8BC0",           // Vibrant blue accent 

          "neutral": "#2A3342",          // Dark blue-gray

          "base-100": "#FFFFFF",         // White background

          "info": "#75B9E6",             // Light blue

          "success": "#36B37E",          // Professional green

          "warning": "#FFAB00",          // Warm amber

          "error": "#E5484D",            // Red alert
        }
      }
    ]
    // themes: ["winter"],
  },
  corePlugins: {
    preflight: false,
  },
}
