

export const CustomLanguageLabels: {[labelId: string]: string} = {
    LANDING_PAGE_HEADER_JOIN_COMMUNITY_TITLE: "Join our community of Job Seekers",
    LANDING_PAGE_HEADER_JOIN_COMMUNITY_SUBTITLE: "To discover, share or discuss job opportunities ideas and results. Also, share your own job opportunities with our community of Job Seekers",

    REMOTE_JOBS_EXPLORE_JOBS_TITLE: "Explore Jobs Across Canada",
    REMOTE_JOBS_LATEST_JOBS_IN_TITLE: "Latest Jobs in",
    REMOTE_JOBS_DISCOVER_JOBS_SUBTITLE: "Discover new job opportunities across Canada.",
    REMOTE_JOBS_CATEGORY_SUBTITLE: "Find the best Jobs in #CATEGORY",
    REMOTE_JOBS_POST_JOB_SUBTITLE: "Reach thousands of talent across Canada",
    JOBS_REGION_TITLE: "Recent Jobs in #REGION",
    JOBS_REGION_CATEGORY_TITLE: "Recent Jobs in #REGION and #CATEGORY",
    JOBS_EMPTY_REGION_CATEGORY_MESSAGE: "We don't have any #CATEGORY jobs in #REGION at the moment. Please check back later.",
    JOBS_REGION_CATEGORY_SEO_DESCRIPTION: "Find the latest #CATEGORY jobs in #REGION. Browse available positions updated daily. Start your career today!",
}

export function getCustomLanguageLabel(labelId: string): string | undefined {
    if (Object.keys(CustomLanguageLabels).includes(labelId)) {
        // @ts-ignore
        return CustomLanguageLabels[labelId]! as string;
    } else {
        return undefined;
    }
}