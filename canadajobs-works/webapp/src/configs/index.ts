export default class RootConfigs {
    static ENVIRONMENT_DEV = false;
    static BASE_URL = "https://www.canadajobs.works";
    static SITE_NAME = "CanadaJobs.works";
    static SITE_TITLE = "Canada Jobs | Find Employment Opportunities Across Canada";
    static SITE_DESCRIPTION = "Discover thousands of job opportunities across Canada. Search, apply, and advance your career with the leading Canadian job board. Find full-time, part-time, and remote positions today.";
    static CURRENT_NICHE_SITE = RootConfigs.SITE_NAME;
    static CONTACT_EMAIL = "contact@canadajobs.works";
    static SITE_TOPIC = "Canada Jobs";
    static THEME = {
        PRIMARY_COLOR: "red",
    }
    static META_CONFIGS: any = {
        updateNicheBackend: false,
        disableGoogleSignin: true,
        disableCookies: true,
        disableAppStoreBanner: false,
        disableGoogleAds: false,
        enableJobSearch: true,
    }
    static APP_STORE_LINKS = {
        androidAppId: "TODO",
        iosAppId: "TODO",
    }
    static SOCIAL_NETWORKS: {
        [socialId: string]: {
            iconUrl: string,
            label: string,
            link: string,
        },
    } = {
        /*facebook: {
            label: "Facebook",
            link: "https://www.facebook.com/profile.php?id=61565899179933",
            iconUrl: "/images/ic_social_facebook.png",
        },
        instagram: {
            label: "Instagram",
            link: "https://www.instagram.com/customer_remote_jobs/",
            iconUrl: "/images/ic_social_instagram.png",
        },
        threads: {
            label: "Threads",
            link: "https://www.threads.net/@customer_remote_jobs",
            iconUrl: "/images/ic_social_threads.png",
        },
        bluesky: {
            label: "Bluesky",
            link: "https://marketingremote.bsky.social",
            iconUrl: "/images/ic_social_bluesky.png",
        } */
    }
    static FIREBASE_CONFIG = this.ENVIRONMENT_DEV ?

        // DEV Config - // TODO Change it
        {
            apiKey: "AIzaSyCB2yrNkEHbpShTH_xwHOzcl2mXJT5i1K8",
            authDomain: "fortuly-qa-env.firebaseapp.com",
            projectId: "fortuly-qa-env",
            storageBucket: "fortuly-qa-env.appspot.com",
            messagingSenderId: "650128691290",
            appId: "1:650128691290:web:69f8a283090d23e0454863",
            measurementId: "G-V6XN6K51VZ"
        } :
        // PROD
        {
            apiKey: "AIzaSyCW7dQ3JKHRAnnjmpKcu5vDJRsdKPQoWMo",
            authDomain: "canadajobs-works.firebaseapp.com",
            databaseURL: "https://canadajobs-works-default-rtdb.firebaseio.com",
            projectId: "canadajobs-works",
            storageBucket: "canadajobs-works.firebasestorage.app",
            messagingSenderId: "879388219681",
            appId: "1:879388219681:web:218baea2e954e652f24660",
            measurementId: "G-B15MEFK2YF"
          };


    static IMAGEKIT_API = {
        publicKey : "public_OurCtnmj0jWqjVslk5YDfKE4aHQ=",
        privateKey : "private_vRUtPf6HGOj68f5rZZN8u9RHfiQ=",
        urlEndpoint : "https://ik.imagekit.io/fortulyapps",
    }

    static ONE_SIGNAL_API = "";
}
