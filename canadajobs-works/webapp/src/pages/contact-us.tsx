import CustomRootLayoutComponent from "../components/app-components/root/CustomRootLayout.component";
import InlineLabelSeparatorComponent
    from "../components/react-shared-module/ui-components/shared/inline-label-separator.component";
import {useState} from "react";
import InputTextComponent from "../components/react-shared-module/ui-components/form/InputText.component";
import InputTextAreaComponent from "../components/react-shared-module/ui-components/form/InputTextArea.component";
import CommonUtils from "../components/react-shared-module/logic/commonutils";
import {
    RootGlobalStateActions,
    useAlertMessageGlobalState
} from "../components/react-shared-module/logic/global-hooks/root-global-state";
import {FirebaseClientFirestoreUtils} from "../components/react-shared-module/logic/shared-data/firebase.utils";
import RootConfigs from "../configs";

export async function getStaticProps() {

    return {
        props: {},
    }
}

interface ContactUsFormModel {
    name: string,
    email: string,
    message: string,
}

export default function ContactUsPage() {
    const [formData, setFormData] = useState<ContactUsFormModel>({name: "", message: "", email: ""});
    const [alertMessage, setAlertMessage] = useAlertMessageGlobalState();
    const [isLoading, setIsLoading] = useState(false);

    function onFieldChange(fieldKey: string, value: string) {
        setFormData({
            ...formData,
            [fieldKey]: value,
        });
    }

    function isFormValid() {
        return formData.name.length > 0 && CommonUtils.isValidEmail(formData.email) && formData.message.length > 0;
    }

    async function onSubmit() {
        setIsLoading(true);
        await FirebaseClientFirestoreUtils.updateDocument("ContactUs", Date.now().toString(), formData);
        setIsLoading(false);
        RootGlobalStateActions.displayAlertMessage({
            message: "We received your message. We will reply to your shortly",
            setAlertMessageState: setAlertMessage,
            alertType: "SUCCESS",
        });
        setFormData({message: "", email: "", name: ""});
    }

    return (
        <CustomRootLayoutComponent>
            <div className='flex flex-col mt-10 mb-20'>
                <InlineLabelSeparatorComponent title={"Contact Us"} />
                <div className='flex flex-col gap-2'>
                    <InputTextComponent topLabel={"Name"} hint={"Tell us your name"} value={formData.name} onChanged={(value) => onFieldChange("name", value)} inputType={"text"} />
                    <InputTextComponent topLabel={"Email"} hint={"Tell us your email"} value={formData.email}  onChanged={(value) => onFieldChange("email", value)} inputType={"email"} />
                    <InputTextAreaComponent topLabel={"Your Message"} hint={"Write you message to us here"} value={formData.message}  onChanged={(value) => onFieldChange("message", value)} />
                    <button className={"btn btn-ghost btn-sm " + (isLoading ? "loading" : "")} onClick={onSubmit} disabled={!isFormValid() || isLoading}>Submit message</button>
                    <div className='divider text-slate-400'>or</div>
                    <span className='text-slate-800'>Send us an email: <a className='text-blue-500 underline' href={"mailto: " + RootConfigs.CONTACT_EMAIL}>{RootConfigs.CONTACT_EMAIL}</a></span>
                </div>
            </div>
        </CustomRootLayoutComponent>
    )
}