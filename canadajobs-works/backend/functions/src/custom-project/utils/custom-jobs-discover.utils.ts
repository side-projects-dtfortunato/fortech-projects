import {
    JobDetailsModel, JobWorkModeType
} from "../../shareable-firebase-backend/base-projects/jobs-discover/shared-jobs-discover.data";


/**
 * Regions ids:
 * - UK: 101165590
 * - Australia: 101452733
 * - USA: 103644278
 * - EU: 91000002
 */
/*export const LINKEDIN_SOURCES: LinkedinSourceModel[] = [
    {
        id: "1",
        category: "Software Development",
        region: "USA",
        url: "https://www.linkedin.com/jobs/search?keywords=Software Engineer&geoId=103644278&f_TPR=&f_WT=2&f_E=2%2C3%2C4%2C5&position=1&pageNum=0",
    },
]*/

export const JOB_REGIONS = [
    {
        regionLabel: "Calgary, Alberta",
        regionId: "102199904",
    },
    {
        regionLabel: "Edmonton, Alberta",
        regionId: "106535873",
    },
    {
        regionLabel: "Halifax, Nova Scotia",
        regionId: "103961363",
    },
    {
        regionLabel: "Montreal, Quebec",
        regionId: "101330853",
    },
    {
        regionLabel: "Ottawa, Ontario",
        regionId: "106234700",
    },
    {
        regionLabel: "Quebec City, Quebec",
        regionId: "90009543",
    },
    {
        regionLabel: "Regina, Saskatchewan",
        regionId: "106068238",
    },
    {
        regionLabel: "Saskatoon, Saskatchewan",
        regionId: "101191194",
    },
    {
        regionLabel: "Toronto, Ontario",
        regionId: "100025096"
    },
    {
        regionLabel: "Vancouver, British Columbia",
        regionId: "103366113",
    },
    {
        regionLabel: "Winnipeg, Manitoba",
        regionId: "101213860",
    }
] as {regionLabel: string, regionId: string}[];


export const JOB_CATEGORIES = [
    {
        jobCategoryLabel: "Accounting",
        jobCategoryQuery: "Accounting",
    },
    {
        jobCategoryLabel: "Administrative",
        jobCategoryQuery: "Administrative",
    },
    {
        jobCategoryLabel: "Business Development",
        jobCategoryQuery: "Business Development",
    },
    {
        jobCategoryLabel: "Consulting",
        jobCategoryQuery: "Consulting",
    },
    {
        jobCategoryLabel: "Construction",
        jobCategoryQuery: "Construction",
    },
    {
        jobCategoryLabel: "Customer Service",
        jobCategoryQuery: "Customer Service",
    },
    {
        jobCategoryLabel: "Design",
        jobCategoryQuery: "Design",
    },
    {
        jobCategoryLabel: "Education",
        jobCategoryQuery: "Education",
    },
    {
        jobCategoryLabel: "Engineering",
        jobCategoryQuery: "Engineering",
    },
    {
        jobCategoryLabel: "Finance",
        jobCategoryQuery: "Finance",
    },
    {
        jobCategoryLabel: "Healthcare",
        jobCategoryQuery: "Healthcare",
    },
    {
        jobCategoryLabel: "Human Resources",
        jobCategoryQuery: "Human Resources",
    },
    {
        jobCategoryLabel: "Legal",
        jobCategoryQuery: "Legal",
    },
    {
        jobCategoryLabel: "Manufacturing",
        jobCategoryQuery: "Manufacturing",
    },
    {
        jobCategoryLabel: "Marketing",
        jobCategoryQuery: "Marketing",
    },
    {
        jobCategoryLabel: "Operations",
        jobCategoryQuery: "Operations",
    },
    {
        jobCategoryLabel: "Project Management",
        jobCategoryQuery: "Project Management",
    },
    {
        jobCategoryLabel: "Sales",
        jobCategoryQuery: "Sales",
    },
    {
        jobCategoryLabel: "Software Development",
        jobCategoryQuery: "Software Engineer",
    }
] as {jobCategoryLabel: string, jobCategoryQuery: string}[];


export const ALLOWED_WORK_MODE = ["REMOTE", "ON-SITE", "HYBRID"] as JobWorkModeType[];


export class CustomJobsDiscoverUtils {

    static importJobsFromRemoteJobsHub(): JobDetailsModel[] {
        return [];
    }

    static getIsValidRule() {
        return "";
    }

}