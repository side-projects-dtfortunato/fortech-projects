
export const PRODUCTION_GCLOUD_PROJECT = "canadajobs-works";

export const WEBAPP_CONFIGS = {
    LOGO_URL: "https://www.canadajobs.works/images/logo-white-bg.jpg",
    SITE_TAGLINE: "Find Employment Opportunities Across Canada",
    SITE_NAME: "CanadaJobs.works",
    WEBSITE_URL: "https://canadajobs.works",
    COMPANY_ADDRESS: "3030 Coimbra (Portugal)",
    COMPANY_EMAIL: "contact@canadajobs.works",
    POST_PREFIX_URL: "https://www.canadajobs.works/post/",
    PROFILE_PREFIX_URL: "https://www.canadajobs.works/public-profile/",
    UNSUBSCRIBE_URL: `https://us-central1-${PRODUCTION_GCLOUD_PROJECT}.cloudfunctions.net/unsubscribeEmail?uid=`,

    MAILERSEND_API_KEY: "TODO",


    CUSTOM_CONFIGS: {
        openPushInApp: true,
    } as any,

    // Account: contact@sidehustlify.com - Di.....oc7
    IMAGE_KIT_API: {
        publicKey : "TODO", // "public_QPb3OvUFUKRLd2VLZtV+ExCVDTo=",
        privateKey : "TODO", //"private_z60vq5B2MydsmNQ+c2dMsOG0RBg=",
        urlEndpoint : "TODO", //"https://ik.imagekit.io/sidehustlify",
    },

    STRIPE_API: {
        prod: "",
        dev: "",
        webhook_dev: "",
        webhook_prod: "",
    },

    ONESIGNAL_CONFIGS: {
        apiKey: "",
        appId: ""
    },

    SOCIAL_NETWORK_API_KEY: {
        metaAccessToken: "",
        facebookPageId: "",
        instagramId: "",
        threadsAPI: {
            clientId: "",
            clientSecret: "",
            redirectUri: `https://us-central1-${PRODUCTION_GCLOUD_PROJECT}.cloudfunctions.net/threadsAPIHandleAuthCallback`,
        },
        redditAPI: {
            clientId: "",
            clientSecret: "",
            username: "",
            password: "",
            subReddit: "",
        }, bluesky: {
            username: "",
            password: "",
        }, telegram: {
            channel: "",
            botToken: "",
        }
    }
}