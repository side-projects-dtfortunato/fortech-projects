import * as functions from 'firebase-functions';
import * as admin from 'firebase-admin';

import * as AuthService from './shareable-firebase-backend/functions/auth'
import * as DocumentChangeService from './shareable-firebase-backend/functions/document-change';
import * as PostsDataService from './shareable-firebase-backend/functions/posts-data';
import * as UserProfileService from './shareable-firebase-backend/functions/user-profile';

import * as EmailMarketingService from './shareable-firebase-backend/functions/email-marketing';

import {testFunction} from './custom-project/functions/test.function';

import {fetchRecentArticlesFunction} from './shareable-firebase-backend/base-projects/articles-digest/functions/fetch-recent-articles.function'
import {onArticleImportedChanged} from './shareable-firebase-backend/base-projects/articles-digest/functions/on-article-updated.function'
import {newsletterSubscribe} from './shareable-firebase-backend/functions/newsletter/newsletter-subscribe'

import {onCommunityCommentPostUpdateFunction} from './shareable-firebase-backend/base-projects/community/functions/on-community-comment-update.function'
import {communityPostComment} from './shareable-firebase-backend/base-projects/community/functions/post-community-comment.function'
import {communityCommentsDelete} from './shareable-firebase-backend/base-projects/community/functions/community-comments-delete.function'
import {communityReplyComment} from './shareable-firebase-backend/base-projects/community/functions/reply-community-comment.function'
import {communityDailyCleanupJob} from './shareable-firebase-backend/base-projects/community/functions/community-daily-cleanup-job.function'

import {generateEmailDailyDigest} from './shareable-firebase-backend/base-projects/articles-digest/functions/articles-daily-digest-email.function'
// import {dispatchEmailBulk} from './shareable-firebase-backend/functions/email-marketing/dispatch-email-bulk'
import {routineJobFunction} from './shareable-firebase-backend/functions/routine-jobs/routineJob.function'

import {threadsAPIHandleAuthCallback, threadsAPIGenerateAuthUrl} from './shareable-firebase-backend/base-projects/social-networks-autopost/functions/threadsapi.function'

import {fetchJobsList, digestImportedJobsList} from './shareable-firebase-backend/base-projects/jobs-discover/functions/fetchJobsList.function'
import {submitJobPost, submitJobPostStripeWebhook} from './shareable-firebase-backend/base-projects/jobs-discover/functions/submitJobPost.function'
import {getLatestJobsFunction} from './shareable-firebase-backend/base-projects/jobs-discover/functions/getLatestJobs.function'
import {onJobUpdated} from './shareable-firebase-backend/base-projects/jobs-discover/functions/on-job-updated.function'
import {cleanOlderJobsFunction} from './shareable-firebase-backend/base-projects/jobs-discover/functions/cleanOlderJobs.function'
import {globalEmailDailyDigestGenerator} from './shareable-firebase-backend/functions/email-marketing/global-email-daily-digest-generator.function'


// Init App
admin.initializeApp(functions.config().firebase);
admin.firestore().settings({ ignoreUndefinedProperties: true });

// Auth
export const onCreateUser = AuthService.onCreateUser;

// User profile
export const updateUserProfile = UserProfileService.updateUserProfile;
export const onUpdatePublicUserProfile = UserProfileService.onUpdatePublicUserProfile;
export const checkUsernameValid = UserProfileService.checkUsernameValid;

// Document Change Service
export const requestDocumentChanges = DocumentChangeService.requestDocumentChanges;

export const exportUrlMetadata = PostsDataService.exportUrlMetadata;

// Email Marketing Service
export const validateEmail = EmailMarketingService.validateEmail;
export const unsubscribeEmail = EmailMarketingService.unsubscribeEmail;


// Export functions
export {
    testFunction,
    fetchRecentArticlesFunction,
    onArticleImportedChanged,
    newsletterSubscribe,
    onCommunityCommentPostUpdateFunction, communityPostComment, communityCommentsDelete, communityReplyComment, communityDailyCleanupJob,

    globalEmailDailyDigestGenerator, routineJobFunction,

    // Social Integrations
    threadsAPIHandleAuthCallback, threadsAPIGenerateAuthUrl,

    // Jobs Discovery Functions
    fetchJobsList, digestImportedJobsList, onJobUpdated, cleanOlderJobsFunction, getLatestJobsFunction,
    submitJobPost, submitJobPostStripeWebhook
}