import * as functions from "firebase-functions";
import { getQuickResponse } from "../../shareable-firebase-backend/model-data/api-response.model";
import { OnesignalUtils } from "../../shareable-firebase-backend/utils/onesignal.utils";

export const testFunction = functions.https.onRequest(
    async (req, resp) => {

        /*await OnesignalUtils.sendPushNotificationToUserJobFilters("🚀 New Remote Job Alert!", 
            `💼 Position: Flutter Developer
🏢 Company: SR2 | Socially Responsible Recruitment | Certified B Corporation™
🌍 Location: West Midlands, England, United Kingdom 
📁 Category: Software Development
💰 Salary: £35 000 - £48 000
🛠️ Skills: Flutter Develpment`, "https://www.jobsinuk.app/job/sr2-socially-responsible-recruitment-certified-b-corporation-flutter-developer", {category: "Software Development", remoteRegion: "West Midlands, England, United Kingdom"});*/
        resp.send(getQuickResponse(true, "Push Notification Sent"));
    });