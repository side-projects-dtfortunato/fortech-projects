import {
    JobDetailsModel, JobWorkModeType,
    LinkedinSourceModel
} from "../../shareable-firebase-backend/base-projects/jobs-discover/shared-jobs-discover.data";


/**
 * Regions ids:
 * - UK: 101165590
 * - Australia: 101452733
 * - USA: 103644278
 * - EU: 91000002
 */

export const JOB_REGIONS = [
    {
        regionLabel: "Greater London, England",
        regionId: "102257491"
    },
    {
        regionLabel: "South East England",
        regionId: "119576547"
    },
    {
        regionLabel: "South West England",
        regionId: "119490374"
    },
    {
        regionLabel: "West Midlands, England",
        regionId: "100345145"
    },
    {
        regionLabel: "East Midlands, England",
        regionId: "119593893"
    },
    {
        regionLabel: "Yorkshire and the Humber",
        regionId: "119554449"
    },
    {
        regionLabel: "North West England",
        regionId: "119579933"
    },
    {
        regionLabel: "North East England",
        regionId: "119482315"
    },
    {
        regionLabel: "Manchester, England",
        regionId: "90009497"
    },
    {
        regionLabel: "Birmingham, England",
        regionId: "100356971"
    },
    {
        regionLabel: "Leeds, England",
        regionId: "90009492"
    },
    {
        regionLabel: "Edinburgh, Scotland",
        regionId: "100209086"
    },
    {
        regionLabel: "Glasgow, Scotland",
        regionId: "102681496"
    },
    {
        regionLabel: "Aberdeen, Scotland",
        regionId: "103506580"
    },
    {
        regionLabel: "Cardiff, Wales",
        regionId: "90009478"
    },
    {
        regionLabel: "Swansea, Wales",
        regionId: "90009511"
    },
    {
        regionLabel: "Newport, Wales",
        regionId: "90009499"
    },
    {
        regionLabel: "Belfast, Northern Ireland",
        regionId: "90009469"
    },
    {
        regionLabel: "Derry, Northern Ireland",
        regionId: "101961963"
    },
] as {regionLabel: string, regionId: string}[];

export const JOB_CATEGORIES = [
    {
        jobCategoryLabel: "Software Development",
        jobCategoryQuery: "Software Engineer",
    },
    {
        jobCategoryLabel: "Sales",
        jobCategoryQuery: "Sales",
    },
    {
        jobCategoryLabel: "Construction",
        jobCategoryQuery: "Construction",
    },
    {
        jobCategoryLabel: "Healthcare",
        jobCategoryQuery: "Healthcare",
    },
    {
        jobCategoryLabel: "Business Development",
        jobCategoryQuery: "Business Development",
    },
    {
        jobCategoryLabel: "Operations",
        jobCategoryQuery: "Operations",
    },
    {
        jobCategoryLabel: "Finance",
        jobCategoryQuery: "Finance",
    },
    {
        jobCategoryLabel: "Human Resources",
        jobCategoryQuery: "Human Resources",
    },
    {
        jobCategoryLabel: "Education",
        jobCategoryQuery: "Education",
    },
    {
        jobCategoryLabel: "Customer Service",
        jobCategoryQuery: "Customer Service",
    },
    {
        jobCategoryLabel: "Project Management",
        jobCategoryQuery: "Project Management",
    },
    {
        jobCategoryLabel: "Administrative",
        jobCategoryQuery: "Administrative",
    },
    {
        jobCategoryLabel: "Accounting",
        jobCategoryQuery: "Accounting",
    },
    {
        jobCategoryLabel: "Manufacturing",
        jobCategoryQuery: "Manufacturing",
    },
    {
        jobCategoryLabel: "Consulting",
        jobCategoryQuery: "Consulting",
    },
    {
        jobCategoryLabel: "Design",
        jobCategoryQuery: "Design",
    },
    {
        jobCategoryLabel: "Marketing",
        jobCategoryQuery: "Marketing",
    },
] as {jobCategoryLabel: string, jobCategoryQuery: string}[];


export const ALLOWED_WORK_MODE = ["REMOTE", "ON-SITE", "HYBRID"] as JobWorkModeType[];

export class CustomJobsDiscoverUtils {

    static importJobsFromRemoteJobsHub(): JobDetailsModel[] {
        return [];
    }

    static getIsValidRule() {
        return "";
    }

}