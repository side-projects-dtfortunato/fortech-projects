
export const PRODUCTION_GCLOUD_PROJECT = "jobsinuk-app";

export const WEBAPP_CONFIGS = {
    LOGO_URL: "https://www.jobsinuk.app/images/logo-white-bg.jpg",
    SITE_TAGLINE: "Find your next Job in UK",
    SITE_NAME: "JobsInUK.app",
    WEBSITE_URL: "https://jobsinuk.app",
    COMPANY_ADDRESS: "3030 Coimbra (Portugal)",
    COMPANY_EMAIL: "contact@jobsinuk.app",
    POST_PREFIX_URL: "https://www.jobsinuk.app/post/",
    PROFILE_PREFIX_URL: "https://www.jobsinuk.app/public-profile/",
    UNSUBSCRIBE_URL: `https://us-central1-${PRODUCTION_GCLOUD_PROJECT}.cloudfunctions.net/unsubscribeEmail?uid=`,

    MAILERSEND_API_KEY: "TODO",

    CUSTOM_CONFIGS: {
        openPushInApp: true,
    } as any,

    // Account: contact@sidehustlify.com - Di.....oc7
    IMAGE_KIT_API: {
        publicKey : "TODO", // "public_QPb3OvUFUKRLd2VLZtV+ExCVDTo=",
        privateKey : "TODO", //"private_z60vq5B2MydsmNQ+c2dMsOG0RBg=",
        urlEndpoint : "TODO", //"https://ik.imagekit.io/sidehustlify",
    },

    STRIPE_API: {
        prod: "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",
        dev: "sk_test_51PueA3HT2hxH9IRDdlrmFSjPbHmjQ1An0PNkz74ZkAOw50c1MSK5nG0MGBWOvbm5RjkBDgG4VXnMDZ2yRBWJGtpA00wlrvUV9J",
        webhook_dev: "whsec_jySvwwZUSe3BLDFdJPYWn6c2vABfZbDd",
        webhook_prod: "whsec_GHjAS4Rkp8AbmwRldSqTeRJ7qsRswT4y",
    },

    ONESIGNAL_CONFIGS: {
        apiKey: "os_v2_app_f6kn25yyuzgctmd6d634xdewtfvmoz6frjcucyvxtke5khkejjplo2eydjzq66q3m2ehfs2ttop5k2egjxyrpzactdyqutdknvazzwi",
        appId: "2f94dd77-18a6-4c29-b07e-1fb7cb8c9699"
    },

    SOCIAL_NETWORK_API_KEY: {
        metaAccessToken: "",
        facebookPageId: "",
        instagramId: "",
        threadsAPI: {
            clientId: "",
            clientSecret: "",
            redirectUri: `https://us-central1-${PRODUCTION_GCLOUD_PROJECT}.cloudfunctions.net/threadsAPIHandleAuthCallback`,
        },
        redditAPI: {
            clientId: "",
            clientSecret: "",
            username: "",
            password: "",
            subReddit: "",
        }, bluesky: {
            username: "jobsinuk.bsky.social",
            password: "divadlooc7",
        }, telegram: {
            channel: "@jobsinuk_app",
            botToken: "7391147252:AAGPieQrgPRYRhcjXP2UXGKePOaELoTAWFs",
        }
    }
}