import CustomRootLayoutComponent from "../components/app-components/root/CustomRootLayout.component";
import NotFoundContentComponent from "../components/react-shared-module/ui-components/shared/NotFoundContent.component";

export default function Custom404() {
    return (
        <CustomRootLayoutComponent>
            <NotFoundContentComponent />
        </CustomRootLayoutComponent>
    )
}