
export default class RootConfigs {
    static ENVIRONMENT_DEV = false;
    static BASE_URL = "https://www.jobsinuk.app";
    static SITE_NAME = "JobsInUK.app";
    static SITE_TITLE = "JobsInUK.app | Search Local & Remote UK Careers";
    static SITE_DESCRIPTION = "Find your next career move on JobsInUK.app. Browse thousands of jobs across the UK, from remote tech roles to local positions. Updated daily.";
    static CURRENT_NICHE_SITE = RootConfigs.SITE_NAME;
    static CONTACT_EMAIL = "contact@jobsinuk.app";
    static SITE_TOPIC = "Jobs In UK";
    static THEME = {
        PRIMARY_COLOR: "indigo",
    }
    static META_CONFIGS: any = {
        updateNicheBackend: false,
        disableGoogleSignin: true,
        disableCookies: true,
        disableAppStoreBanner: false,
        disableGoogleAds: false,
        enableJobSearch: true,
    }
    static APP_STORE_LINKS = {
        androidAppId: "jobsinuk.app",
        iosAppId: "6740539542",
    }
    static SOCIAL_NETWORKS: {
        [socialId: string]: {
            iconUrl: string,
            label: string,
            link: string,
        },
    } = {
        /*facebook: {
            label: "Facebook",
            link: "https://www.facebook.com/profile.php?id=61565899179933",
            iconUrl: "/images/ic_social_facebook.png",
        },
        instagram: {
            label: "Instagram",
            link: "https://www.instagram.com/customer_remote_jobs/",
            iconUrl: "/images/ic_social_instagram.png",
        },
        threads: {
            label: "Threads",
            link: "https://www.threads.net/@customer_remote_jobs",
            iconUrl: "/images/ic_social_threads.png",
        },*/
        bluesky: {
            label: "Bluesky",
            link: "https://jobsinuk.bsky.social",
            iconUrl: "/images/ic_social_bluesky.png",
        },
        telegram: {
            label: "Telegram Community",
            link: "https://t.me/jobsinuk_app",
            iconUrl: "/images/ic_social_threads.png",
        },
    }
    static FIREBASE_CONFIG = this.ENVIRONMENT_DEV ?

        // DEV Config - // TODO Change it
        {
            apiKey: "AIzaSyCB2yrNkEHbpShTH_xwHOzcl2mXJT5i1K8",
            authDomain: "fortuly-qa-env.firebaseapp.com",
            projectId: "fortuly-qa-env",
            storageBucket: "fortuly-qa-env.appspot.com",
            messagingSenderId: "650128691290",
            appId: "1:650128691290:web:69f8a283090d23e0454863",
            measurementId: "G-V6XN6K51VZ"
        } :
        // PROD
        {
            apiKey: "AIzaSyBHBC6S0-bqQ5CwXXLBdMCs6-eWXthMRgU",
            authDomain: "jobsinuk-app.firebaseapp.com",
            databaseURL: "https://jobsinuk-app-default-rtdb.firebaseio.com",
            projectId: "jobsinuk-app",
            storageBucket: "jobsinuk-app.firebasestorage.app",
            messagingSenderId: "893765022924",
            appId: "1:893765022924:web:7484486b8a182d0ee73e69",
            measurementId: "G-D9RTTK5E4C"
        };


    static IMAGEKIT_API = {
        publicKey : "public_OurCtnmj0jWqjVslk5YDfKE4aHQ=",
        privateKey : "private_vRUtPf6HGOj68f5rZZN8u9RHfiQ=",
        urlEndpoint : "https://ik.imagekit.io/fortulyapps",
    }

    static ONE_SIGNAL_API = "2f94dd77-18a6-4c29-b07e-1fb7cb8c9699";
}
