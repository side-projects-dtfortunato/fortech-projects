
export const PRODUCTION_GCLOUD_PROJECT = "newyorknews-app";

export const WEBAPP_CONFIGS = {
    LOGO_URL: "https://www.newyorknews.app/images/logo-white-bg.jpg",
    SITE_TAGLINE: "Daily New York City News and Community",
    SITE_NAME: "NewYorkNews.com",
    WEBSITE_URL: "https://newyorknews.app",
    COMPANY_ADDRESS: "3030 Coimbra (Portugal)",
    COMPANY_EMAIL: "contact@newyorknews.app",
    POST_PREFIX_URL: "https://www.newyorknews.app/post/",
    PROFILE_PREFIX_URL: "https://www.newyorknews.app/public-profile/",
    UNSUBSCRIBE_URL: `https://us-central1-${PRODUCTION_GCLOUD_PROJECT}.cloudfunctions.net/unsubscribeEmail?uid=`,

    MAILERSEND_API_KEY: "TODO",

    CUSTOM_CONFIGS: {
        openPushInApp: true,
    } as any,

    // Account: contact@sidehustlify.com - Di.....oc7
    IMAGE_KIT_API: {
        publicKey : "public_OurCtnmj0jWqjVslk5YDfKE4aHQ=",
        privateKey : "private_vRUtPf6HGOj68f5rZZN8u9RHfiQ=",
        urlEndpoint : "https://ik.imagekit.io/fortulyapps",
    },

    STRIPE_API: {
        prod: "",
        dev: "",
        webhook_dev: "",
        webhook_prod: "",
    },

    ONESIGNAL_CONFIGS: {
        apiKey: "ZDc4NTEyYWEtNTkyMy00YTk2LThhNTQtYmQ5MDNmMDgyYTU2",
        appId: "08eb92a9-e1c8-4d01-bd93-b5be4e3ab96f"
    },

    SOCIAL_NETWORK_API_KEY: {
        metaAccessToken: "EAAQ1xcpTscUBOzrkZAkuJrIaPJCAMNvXWvZBols45Oj09zCgMo36V7pVznXGZCVESr3cwBUlw1gidXsT6ShYT62AV1PL1KZAGPhZBr5I4oHeWm4ITwLQc7T1Mrl6ZBJo9vf4P8mMp3OiLURPrwYqkvYkvZAOMHwhxGzRd42ptu7S3ZCLbhjZB8nOqazLsT1OlViwZD",
        facebookPageId: "369123819622255",
        instagramId: "",
        threadsAPI: {
            clientId: "",
            clientSecret: "",
            redirectUri: `https://us-central1-${PRODUCTION_GCLOUD_PROJECT}.cloudfunctions.net/threadsAPIHandleAuthCallback`,
        },
        redditAPI: {
            clientId: "UvKNNzR4TsTElJHfDN6F4w",
            clientSecret: "yZLF0TzGPdKwQuOaJLPW32G6DPMTCQ",
            username: "newyorknews-app",
            password: "D****c7",
            subReddit: "NewYorkNews_app",
        },
        telegram: {
            channel: "",
        },
        bluesky: {
            username: "",
            password: "",
        }
    }
}