import {
    JobDetailsModel,
    LinkedinSourceModel
} from "../../shareable-firebase-backend/base-projects/jobs-discover/shared-jobs-discover.data";


/**
 * Regions ids:
 * - UK: 101165590
 * - Australia: 101452733
 * - USA: 103644278
 * - EU: 91000002
 */
export const LINKEDIN_SOURCES: LinkedinSourceModel[] = []


export class CustomJobsDiscoverUtils {

    static async importJobsFromRemoteJobsHub(): Promise<JobDetailsModel[]> {
        return [];
    }

    static getIsValidRule() {
        return undefined;
    }

}