
export default class RootConfigs {
    static ENVIRONMENT_DEV = false;
    static BASE_URL = "https://www.newyorknews.app";
    static SITE_NAME = "NewYorkNews.app";
    static SITE_TITLE = "Daily New York City News and Community";
    static SITE_DESCRIPTION = "NewYorkNews.app: Your real-time pulse on the Big Apple. Get breaking local news, in-depth coverage, and insightful analysis on politics, business, arts, sports, and more. Stay informed with our curated, up-to-the-minute reporting on everything that matters in New York City. From Wall Street to Broadway, from the boroughs to City Hall, we've got New York covered.";
    static CURRENT_NICHE_SITE = RootConfigs.SITE_NAME;
    static CONTACT_EMAIL = "contact@newyorknews.app";
    static SITE_TOPIC = "New York City";
    static THEME = {
        PRIMARY_COLOR: "slate",
    }
    static META_CONFIGS: any = {
        updateNicheBackend: false,
        disableGoogleSignin: false,
        disableCookies: true,
        disableAppStoreBanner: false,
    }
    static APP_STORE_LINKS = {
        androidAppId: "newyorknews.app",
        iosAppId: "6587580666",
    }
    static SOCIAL_NETWORKS: {
        [socialId: string]: {
            iconUrl: string,
            label: string,
            link: string,
        },
    } = {
 /*       medium: {
            label: "Medium",
            link: "https://medium.com/@journeypreneur",
            iconUrl: "https://miro.medium.com/fit/c/152/152/1*sHhtYhaCe2Uc3IU0IgKwIQ.png",
        },*/
    }
    static FIREBASE_CONFIG = this.ENVIRONMENT_DEV ?

        // DEV Config
        {
            apiKey: "AIzaSyCB2yrNkEHbpShTH_xwHOzcl2mXJT5i1K8",
            authDomain: "fortuly-qa-env.firebaseapp.com",
            projectId: "fortuly-qa-env",
            storageBucket: "fortuly-qa-env.appspot.com",
            messagingSenderId: "650128691290",
            appId: "1:650128691290:web:69f8a283090d23e0454863",
            measurementId: "G-V6XN6K51VZ"
        } :
        // PROD
        {
            apiKey: "AIzaSyDEg_kh6i5OaxSkEjG3GehAaPTt-OwqoLg",
            authDomain: "newyorknews-app.firebaseapp.com",
            projectId: "newyorknews-app",
            storageBucket: "newyorknews-app.appspot.com",
            messagingSenderId: "878104594826",
            appId: "1:878104594826:web:d64d7a93d11854c9bda073",
            measurementId: "G-Q1S2ZD1V73"
        };


    static IMAGEKIT_API = {
        publicKey : "public_OurCtnmj0jWqjVslk5YDfKE4aHQ=",
        privateKey : "private_vRUtPf6HGOj68f5rZZN8u9RHfiQ=",
        urlEndpoint : "https://ik.imagekit.io/fortulyapps",
    }

    static ONE_SIGNAL_API = "08eb92a9-e1c8-4d01-bd93-b5be4e3ab96f";
}
