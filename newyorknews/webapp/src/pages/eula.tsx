import CustomRootLayoutComponent from "../components/app-components/root/CustomRootLayout.component";
import EULAComponent from "../components/react-shared-module/ui-components/shared/EULAComponent";

export default function EulaPage() {
    return (
        <CustomRootLayoutComponent isIndexable={false}>
            <EULAComponent />
        </CustomRootLayoutComponent>
    )
}