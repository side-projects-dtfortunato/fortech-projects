

export const PRODUCTION_GCLOUD_PROJECT = "comaker-ai";

export const WEBAPP_CONFIGS = {
    LOGO_URL: "https://www.CoMaker.ai/images/logo-white-bg.jpg",
    SITE_NAME: "CoMaker.ai",
    WEBSITE_URL: "https://CoMaker.ai",
    COMPANY_ADDRESS: "3030 Coimbra (Portugal)",
    COMPANY_EMAIL: "contact@partnerai.app",
    POST_PREFIX_URL: "https://www.CoMaker.ai/post/",
    PROFILE_PREFIX_URL: "https://www.CoMaker.ai/public-profile/",
    UNSUBSCRIBE_URL: "https://us-central1-comaker-ai.cloudfunctions.net/unsubscribeEmail?uid=",
    SITE_TAGLINE: "Your AI Assistant to build web writing content",

    MAILERSEND_API_KEY: "TODO",

    CUSTOM_CONFIGS: {
        openPushInApp: true,
    } as any,

    // Account: contact@sidehustlify.com - Di.....oc7
    IMAGE_KIT_API: {
        publicKey : "public_n18554rP8SLkf4SuolRqKKMzzyg=", // "public_QPb3OvUFUKRLd2VLZtV+ExCVDTo=",
        privateKey : "private_ER/PWZO4muMSvq4WGOD0Sunbe0g=", //"private_z60vq5B2MydsmNQ+c2dMsOG0RBg=",
        urlEndpoint : "https://ik.imagekit.io/comaker", //"https://ik.imagekit.io/sidehustlify",
    },

    STRIPE_API: {
        dev: "",
        prod: "",
    },

    ONESIGNAL_CONFIGS: {
        appId: "",
        apiKey: "",
    },

    SOCIAL_NETWORK_API_KEY: {
        metaAccessToken: "",
        facebookPageId: "",
        instagramId: "",
        threadsAPI: {
            clientId: "",
            clientSecret: "",
            redirectUri: `https://us-central1-${PRODUCTION_GCLOUD_PROJECT}.cloudfunctions.net/handleAuthCallback`,
        },
        redditAPI: {
            clientId: "",
            clientSecret: "",
            username: "",
            password: "",
            subReddit: "",
        }
    }
}