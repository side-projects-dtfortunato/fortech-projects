import * as functions from 'firebase-functions';
import * as admin from 'firebase-admin';

import * as AuthService from './shareable-firebase-backend/functions/auth'
import * as PushNotifsService from './shareable-firebase-backend/functions/push-notifs';
import * as DocumentChangeService from './shareable-firebase-backend/functions/document-change';
import * as UserProfileService from './shareable-firebase-backend/functions/user-profile';

import * as EmailMarketingService from './shareable-firebase-backend/functions/email-marketing';

/** COMAKER IMPORTS */
import {onCreateUserProjectsDashboard} from "./comaker-backend/functions/on-user-created";
import {onUsersProjectsUpdated, createUserProject} from "./comaker-backend/functions/user-projects";
import {generateAIWritingContent, saveGeneratedContentChanges, deleteGeneratedItem} from "./comaker-backend/functions/user-projects/generate-content";
import {updateGeneratedContent} from "./comaker-backend/functions/user-projects/generate-content/update-generated-content";
import {onUserSubscriptionUpdated, buyMoreCredits} from "./comaker-backend/functions/subscriptions";
import {stripeCreateCustomerPortalSession} from "./comaker-backend/functions/subscriptions/stripe";
import {stripeCreateCheckoutSession} from "./comaker-backend/functions/subscriptions/stripe/create-subscription";
import {stripeWebhook, testCustomerId} from "./comaker-backend/functions/subscriptions/stripe/webhooks";
import {subscriptionsRenewPlanJob} from "./comaker-backend/functions/subscriptions/stripe/renew-plan-job";
import {updateSubscriptionPlan} from "./comaker-backend/functions/subscriptions/stripe/upgrade-plan";
import {openAIGenerateImage} from "./comaker-backend/functions/user-projects/generate-images";
import {onGeneratedItemWritten, onGeneratedImageWritten} from "./comaker-backend/functions/user-projects/on-update";




// Init App
admin.initializeApp(functions.config().firebase);
admin.firestore().settings({ ignoreUndefinedProperties: true });

/***** COMMON FUNCTIONS **/
// Push
export const updateFCMTokens = PushNotifsService.updateFCMTokens;

// Auth
export const onCreateUser = AuthService.onCreateUser;

// User profile
export const updateUserProfile = UserProfileService.updateUserProfile;
export const onUpdatePublicUserProfile = UserProfileService.onUpdatePublicUserProfile;
export const checkUsernameValid = UserProfileService.checkUsernameValid;

// Document Change Service
export const requestDocumentChanges = DocumentChangeService.requestDocumentChanges;


// Email Marketing Service
// export const sendEmailWeeklyDigest = EmailMarketingService.sendEmailWeeklyDigest;
export const importNewsletterSubscribers = EmailMarketingService.importNewsletterSubscribers;
export const generateNewsletterSubscribersQueue = EmailMarketingService.generateNewsletterSubscribersQueue;
export const validateEmail = EmailMarketingService.validateEmail;

/** END COMMON FUNCTIONS */



/** INIT COMAKER.AI FUNCTIONS */
export {
    onCreateUserProjectsDashboard,
    onUsersProjectsUpdated, createUserProject,
    generateAIWritingContent, saveGeneratedContentChanges, deleteGeneratedItem,
    updateGeneratedContent,
    onUserSubscriptionUpdated, buyMoreCredits,
    stripeCreateCheckoutSession, stripeWebhook, testCustomerId, stripeCreateCustomerPortalSession, updateSubscriptionPlan, subscriptionsRenewPlanJob,
    openAIGenerateImage,
    onGeneratedItemWritten, onGeneratedImageWritten
};

/** END COMAKER.AI FUNCTIONS */