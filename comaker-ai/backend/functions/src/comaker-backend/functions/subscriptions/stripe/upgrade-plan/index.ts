import * as functions from "firebase-functions";
import {getQuickResponse, getResponse} from "../../../../../shareable-firebase-backend/model-data/api-response.model";
import {Log} from "../../../../../shareable-firebase-backend/utils/log";
import {StripeConfigs, stripeDev, stripeProd, StripeUtils} from "../../../../managers/stripe/stripe-utils";
import {getDocumentData} from "../../../../../shareable-firebase-backend/utils/utils";
import {
    ComakerCentralUserDataModel,
    CoMakerFirestoreCollectionDB,
    UserSubscriptionPlanModel,
    UserSubscriptionPlanType
} from "../../../../frontend-imports";
import * as admin from "firebase-admin";
import {
    SharedFirestoreCollectionDB
} from "../../../../../shareable-firebase-backend/model-data/shared-firestore-collections";

export const updateSubscriptionPlan = functions.https.onCall(
    (data: { env: "PROD" | "DEV", planType: UserSubscriptionPlanType },
     context) => {
        if (!context.auth || !context.auth.uid) return getResponse(1001, undefined, "User must be logged in to execute this action");
        const userId: string = context && context.auth && context.auth.uid ? context.auth.uid : "";
        Log.onCall(data, userId);

        return new Promise(async (resolve, reject) => {
            let priceId: string | undefined = "";

            if (data.env === "PROD") {
                switch (data.planType) {
                    case UserSubscriptionPlanType.SUBSCRIBED_HOBBY:
                        priceId = StripeConfigs.PROD.plans.SUBSCRIBED_HOBBY.priceId;
                        break;
                    case UserSubscriptionPlanType.SUBSCRIBED_PRO:
                        priceId = StripeConfigs.PROD.plans.SUBSCRIBED_PRO.priceId;
                        break;
                }
            } else {
                switch (data.planType) {
                    case UserSubscriptionPlanType.SUBSCRIBED_HOBBY:
                        priceId = StripeConfigs.DEV.plans.SUBSCRIBED_HOBBY.priceId;
                        break;
                    case UserSubscriptionPlanType.SUBSCRIBED_PRO:
                        priceId = StripeConfigs.DEV.plans.SUBSCRIBED_PRO.priceId;
                        break;
                }
            }

            console.log("PriceId: ", priceId);

            // Check if the user has already subscribed this plan
            const userSubscription: UserSubscriptionPlanModel = await getDocumentData(CoMakerFirestoreCollectionDB.UserSubscriptionPlanCollection, userId);

            if (userSubscription.planType === data.planType) {
                resolve(getQuickResponse(false, null, "You already have subscribed this plan"));
                return;
            }

            if (priceId) {
                const stripeApi = data.env === "PROD" ? stripeProd : stripeDev;

                // First check if the user already have a customerId on stripe
                let customerId: string = "";
                let centralUserDataModel: ComakerCentralUserDataModel = await getDocumentData(SharedFirestoreCollectionDB.CentralUserData, userId);
                if (!centralUserDataModel.subscriptionPlan?.customerId) {
                    // We should create the customer first
                    let customerObj = await stripeApi.customers.create({
                        email: centralUserDataModel.email,
                        metadata: {
                            userId: userId,
                        },
                    });
                    if (customerObj && customerObj.id) {
                        customerId = customerObj.id;

                        // Update UserSubscriptionPlan
                        await admin.firestore()
                            .collection(CoMakerFirestoreCollectionDB.UserSubscriptionPlanCollection)
                            .doc(userId).set({
                                customerId: customerId,
                            }, {merge: true});
                    }
                } else {
                    customerId = centralUserDataModel.subscriptionPlan?.customerId;
                }

                if (!customerId) {
                    resolve(getQuickResponse(false, null, "Some error occurred while creating your subscription profile. Please, try again our contact us: contact@comaker.ai"));
                    return;
                }

                if (!userSubscription.stripeData?.subscription) {
                    resolve(getQuickResponse(false, null, "We didn't found any subscription to upgrade"));
                }

                const subscription = await stripeApi.subscriptions.retrieve(userSubscription.stripeData?.subscription);
                const stripeResponse: any = await stripeApi.subscriptions.update(subscription.id, {
                    cancel_at_period_end: false,
                    proration_behavior: 'create_prorations',
                    items: [{
                        id: subscription.items.data[0].id,
                        price: priceId,
                    }],
                });

                if (stripeResponse && stripeResponse.status === "active") {

                    // Update user plan
                    await admin.firestore()
                        .collection(CoMakerFirestoreCollectionDB.UserSubscriptionPlanCollection)
                        .doc(userId)
                        .set(StripeUtils.getUserSubscriptionUpdateByPlan(data.planType), {merge: true});

                    resolve(getQuickResponse(true, stripeResponse));
                } else {
                    resolve(getQuickResponse(false, stripeResponse));
                }

            } else {
                resolve(getQuickResponse(false, null, "Something failed while subscribing. Try again later or contact contact@comaker.ai"));
            }
        });
    });