import * as functions from "firebase-functions";
import * as admin from "firebase-admin";
import {
    CoMakerFirestoreCollectionDB, SubscriptionPlansStaticData, UserSubscriptionPlanType, UserSubscriptionUtils
} from "../../../../frontend-imports";
import {Duration} from "ts-duration";
import {getQuickResponse} from "../../../../../shareable-firebase-backend/model-data/api-response.model";

export const subscriptionsRenewPlanJob = functions.https.onRequest(async (request,
                                                              response) => {
    // Get UserSubscriptions that already expired (24 hours ago)
    const query = await admin.firestore().collection(CoMakerFirestoreCollectionDB.UserSubscriptionPlanCollection).where("renewalDate", "<", Duration.millisecond(Date.now()).sub(Duration.hour(24)).milliseconds).get();

    const batch = admin.firestore().batch();
    // Migrate all of them to Free Plan and
    if (!query.empty) {
        query.docs.forEach((doc) => {
            batch.set(admin.firestore().collection(CoMakerFirestoreCollectionDB.UserSubscriptionPlanCollection).doc(doc.id), {
                availableCredits: SubscriptionPlansStaticData.free.credits,
                initialCredits: SubscriptionPlansStaticData.free.credits,
                renewalDate: UserSubscriptionUtils.renewalDate(),
                planType: UserSubscriptionPlanType.FREE,
                updatedAt: Date.now(),
            }, {merge: true});
        });

        const dbResponse = batch.commit();
        response.send(getQuickResponse(true, dbResponse, "Subscriptions updated"));
    } else {
        response.send(getQuickResponse(true, null, "Nothing  to update"));
    }

});