import * as functions from "firebase-functions";
import * as admin from "firebase-admin";
import {getQuickResponse, getResponse} from "../../../../shareable-firebase-backend/model-data/api-response.model";
import {Log} from "../../../../shareable-firebase-backend/utils/log";
import {
    ComakerCentralUserDataModel,
    CoMakerFirestoreCollectionDB
} from "../../../frontend-imports";
import {getDocumentData} from "../../../../shareable-firebase-backend/utils/utils";
import {stripeDev, stripeProd} from "../../../managers/stripe/stripe-utils";
import {
    SharedFirestoreCollectionDB
} from "../../../../shareable-firebase-backend/model-data/shared-firestore-collections";


export const stripeCreateCustomerPortalSession = functions.https.onCall(
    (data: { env: "PROD" | "DEV"},
     context) => {
        if (!context.auth || !context.auth.uid) return getResponse(1001, undefined, "User must be logged in to execute this action");
        const userId: string = context && context.auth && context.auth.uid ? context.auth.uid : "";
        Log.onCall(data, userId);

        return new Promise(async (resolve, reject) => {
            const stripeApi = data.env === "PROD" ? stripeProd : stripeDev;

            // First check if the user already have a customerId on stripe
            let customerId: string = "";
            let centralUserDataModel: ComakerCentralUserDataModel = await getDocumentData(SharedFirestoreCollectionDB.CentralUserData, userId);
            if (!centralUserDataModel.subscriptionPlan?.customerId) {
                // We should create the customer first
                let customerObj = await stripeApi.customers.create({
                    email: centralUserDataModel.email,
                    metadata: {
                        userId: userId,
                    },
                });
                if (customerObj && customerObj.id) {
                    customerId = customerObj.id;

                    // Update UserSubscriptionPlan
                    await admin.firestore()
                        .collection(CoMakerFirestoreCollectionDB.UserSubscriptionPlanCollection)
                        .doc(userId).set({
                            customerId: customerId,
                        }, {merge: true});
                }

            } else {
                customerId = centralUserDataModel.subscriptionPlan?.customerId;
            }

            if (!customerId) {
                resolve(getQuickResponse(false, null, "Some error occurred while creating your subscription profile. Please, try again our contact us: contact@comaker.ai"));
                return;
            }

            const returnUrl = 'https://CoMaker.ai/edit-profile/user-profile';

            const portalSession = await stripeApi.billingPortal.sessions.create({
                customer: customerId,
                return_url: returnUrl,
            });

            resolve(getQuickResponse(true, {redirectUrl: portalSession.url}));
        });
    });
