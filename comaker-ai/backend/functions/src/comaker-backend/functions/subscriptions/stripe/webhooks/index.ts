/** WEBHOOKS */
import * as functions from "firebase-functions";
import {StripeConfigs, stripeDev, stripeProd, StripeUtils} from "../../../../managers/stripe/stripe-utils";
import {
    CoMakerFirestoreCollectionDB,
    SubscriptionPlansStaticData,
    UserSubscriptionPlanModel,
    UserSubscriptionPlanType
} from "../../../../frontend-imports";
import * as admin from "firebase-admin";


export const stripeWebhook = functions.https.onRequest(async (request,
                                                              response) => {
    const sig = request.headers['stripe-signature'];

    let event;

    let env: string | undefined = request.query.env as string;


    if (env) {
        const stripeApi = env === "PROD" ? stripeProd : stripeDev;
        const webhookKey: string = env === "PROD" ? StripeConfigs.PROD.webHookKey : StripeConfigs.DEV.webHookKey;


        console.log("Env: ", env, "webhookKey: ", webhookKey, "StripeSignature: ", sig);
        try {
            event = stripeApi.webhooks.constructEvent(request.rawBody.toString(), sig, webhookKey);
        } catch (err: any) {
            response.status(400).send(`Webhook Error: ${err.message}`);
            return;
        }

        // Handle the event
        console.log("Event.type: ", event.type, " Event.data.object: ", event.data.object);

        // CustomerId
        const customerId: string | undefined = event.data.object.customer;

        if (!customerId) {
            response.status(400).send("Didn't found the customerId on the object");
            return;
        }

        const queryResponse = (await admin.firestore()
            .collection(CoMakerFirestoreCollectionDB.UserSubscriptionPlanCollection)
            .where("customerId", "==", customerId.toString())
            .get());

        if (queryResponse.empty && queryResponse.docs.length === 0) {
            response.status(400).send("Didn't found the UserSubscriptionPlan document with the customerId: " + customerId);
            return;
        }

        const userSubscriptonData: UserSubscriptionPlanModel = {
            ...queryResponse.docs[0].data() as UserSubscriptionPlanModel,
        };

        let planType: UserSubscriptionPlanType | string;
        const userId: string = userSubscriptonData.uid!;
        let responseData: any = {}
        switch (event.type) {
            case 'checkout.session.completed':
                planType = event.data.object.metadata.planType;
                if (!planType || planType.length === 0) {
                    response.status(400).send("Didn't found the planType");
                    return;
                }
                if (planType === UserSubscriptionPlanType.BUY_50AI_CREDITS) {
                    responseData = await renewUserSubscription({userId, planType, dataObject: event.data.object});
                }
                break;

            case 'customer.subscription.resumed':
            case 'customer.subscription.created':
                let priceId: string = event.data.object.plan.id;
                planType = StripeUtils.getPlanTypeByPriceId(env, priceId);
                if (!planType || planType.length === 0) {
                    response.status(400).send("Didn't found the planType for the priceId: " + priceId);
                    return;
                }
                responseData = await renewUserSubscription({userId, planType, dataObject: event.data.object});
                break;

            case "customer.subscription.deleted":
            case "customer.subscription.paused":
                priceId = event.data.object.plan.id;
                planType = StripeUtils.getPlanTypeByPriceId(env, priceId);
                if (!planType || planType.length === 0) {
                    response.status(400).send("Didn't found the planType for the priceId: " + priceId);
                    return;
                }
                responseData = await cancelUserSubscription({userId, planType});
                break;

            default:
            // Unhandled event type
        }

        // Return a 200 response to acknowledge receipt of the event
        response.send(JSON.stringify(responseData));
    } else {
        response.send({message: "Env param was not set properly"});
    }
});


/** Utils Functions */
async function renewUserSubscription(params: {userId: string, planType: string, dataObject: any}) {

    let updatedData: any = StripeUtils.getUserSubscriptionUpdateByPlan(params.planType);

    if (params.planType === UserSubscriptionPlanType.SUBSCRIBED_PRO || params.planType === UserSubscriptionPlanType.SUBSCRIBED_HOBBY) {
        updatedData = {
            ...updatedData,
            renewalDate: params.dataObject.current_period_end * 1000,
            stripeData: {
                subscription: params.dataObject.id,
                period_end: params.dataObject.current_period_end,
                period_start: params.dataObject.current_period_start,
                dataObject: params.dataObject,
            },
        };
    }


    await admin.firestore()
        .collection(CoMakerFirestoreCollectionDB.UserSubscriptionPlanCollection)
        .doc(params.userId)
        .set(updatedData, {merge: true});

}

async function cancelUserSubscription(params: {userId: string, planType: string}) {

    if (params.planType === UserSubscriptionPlanType.SUBSCRIBED_PRO || params.planType === UserSubscriptionPlanType.SUBSCRIBED_HOBBY) {
        let updatedData: any = {
            initialCredits: SubscriptionPlansStaticData.free.credits,
            availableCredits: SubscriptionPlansStaticData.free.credits,
            planType: UserSubscriptionPlanType.FREE,
        };

        await admin.firestore()
            .collection(CoMakerFirestoreCollectionDB.UserSubscriptionPlanCollection)
            .doc(params.userId)
            .set(updatedData, {merge: true});
    }


}



export const testCustomerId = functions.https.onRequest(async (request,
                                                              response) => {
    const customerId: string = "cus_NhCV5UbhWgZQvm";
    const queryResponse = (await admin.firestore()
        .collection(CoMakerFirestoreCollectionDB.UserSubscriptionPlanCollection)
        .where("customerId", "==", customerId)
        .get());
    if (queryResponse.empty && queryResponse.docs.length === 0) {
        response.status(400).send("Didn't found the UserSubscriptionPlan document with the customerId: " + customerId);
        return;
    } else {
        response.status(200).send(JSON.stringify(queryResponse.docs[0].data()));
    }
});