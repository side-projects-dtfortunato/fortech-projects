import * as functions from "firebase-functions";
import * as admin from "firebase-admin";
import {
    CoMakerFirestoreCollectionDB,
    UserSubscriptionPlanModel
} from "../../frontend-imports";
import {Log} from "../../../shareable-firebase-backend/utils/log";
import {getQuickResponse, getResponse} from "../../../shareable-firebase-backend/model-data/api-response.model";
import {SharedFirestoreCollectionDB} from "../../../shareable-firebase-backend/model-data/shared-firestore-collections";


export const onUserSubscriptionUpdated = functions
    .firestore
    .document(CoMakerFirestoreCollectionDB.UserSubscriptionPlanCollection + '/{userId}')
    .onWrite(async (data, context) => {
        const userId: string = context.params["userId"];

        Log.onWrite(data);
        let userSubscriptionPlan: UserSubscriptionPlanModel | undefined;


        let isDeleted: boolean = data.after ? false : true;
        let dataId: string = !isDeleted ? data.after.id : data.before.id;

        if (data && !isDeleted && data.after.data()) {
            userSubscriptionPlan = {
                ...(isDeleted ? data.before.data() : data.after.data()) as UserSubscriptionPlanModel,
                uid: dataId,
            };
        }

        // Update Central User Data
        let batch = admin.firestore().batch();

        batch.set(admin.firestore().collection(SharedFirestoreCollectionDB.CentralUserData).doc(userId), {
            subscriptionPlan: userSubscriptionPlan,
        }, {merge: true});

        // Commit changes
        await batch.commit();
    });

export const buyMoreCredits = functions.https.onCall(
    (data: {credits: number},
     context) => {
        if (!context.auth || !context.auth.uid) return getResponse(1001, undefined, "User must be logged in to execute this action");
        const userId: string = context && context.auth && context.auth.uid ? context.auth.uid : "";
        Log.onCall(data, userId);

        return new Promise(async (resolve, reject) => {
            // Add more credits
            let batch = admin.firestore().batch();

            batch.set(admin.firestore().collection(CoMakerFirestoreCollectionDB.UserSubscriptionPlanCollection).doc(userId), {
                extraCredits: admin.firestore.FieldValue.increment(data.credits),
            }, {merge: true});

            const dbResponse = await batch.commit();

            resolve(getQuickResponse(true, {dbResponse}));
        });
    });