import * as functions from "firebase-functions";
import * as admin from "firebase-admin";
import {CoMakerFirestoreCollectionDB, CoMakerFirestoreDocsDB} from "../../../data-model/comaker-firestore-collections";
import {Log} from "../../../../shareable-firebase-backend/utils/log";
import {GeneratedContentBaseModel} from "../../../data-model/generate-text-content/generated-content-base.model";

export const onGeneratedItemWritten = functions
    .firestore
    .document(CoMakerFirestoreCollectionDB.UserProjectsDashboardCollection + '/{userId}/' + CoMakerFirestoreDocsDB.UserProjectsDashboard.GeneratedContentItemsCollection + "/{itemId}")
    .onWrite(async (data, context) => {
        Log.onWrite(data);

        // Update aggregate data
        let itemData: GeneratedContentBaseModel<any, any> = data.after.data() as GeneratedContentBaseModel<any, any>;

        if (itemData) {

            let batch = admin.firestore().batch();

            // Add Doc to aggregator docs
            batch.set(admin.firestore().collection(CoMakerFirestoreCollectionDB.UserProjectsDashboardCollection).doc(context.params.userId)
                .collection(CoMakerFirestoreDocsDB.UserProjectsDashboard.AggregatedDocsCollection)
                .doc(CoMakerFirestoreDocsDB.UserProjectsDashboard.GeneratedContentItemsCollection), {
                [itemData.uid!]: {
                    ...itemData,
                    aiResponseData: null,
                    aiOutputGeneratedData: null,
                    outputEditableData: null,
                }
            }, {merge: true});

            await batch.commit();
        }
    });

export const onGeneratedImageWritten = functions
    .firestore
    .document(CoMakerFirestoreCollectionDB.UserProjectsDashboardCollection + '/{userId}/' + CoMakerFirestoreDocsDB.UserProjectsDashboard.GeneratedImagesCollection + "/{itemId}")
    .onWrite(async (data, context) => {
        Log.onWrite(data);

        // Update aggregate data
        let itemData: GeneratedContentBaseModel<any, any> = data.after.data() as GeneratedContentBaseModel<any, any>;

        if (itemData) {

            let batch = admin.firestore().batch();

            // Add Doc to aggregator docs
            batch.set(admin.firestore().collection(CoMakerFirestoreCollectionDB.UserProjectsDashboardCollection).doc(context.params.userId)
                .collection(CoMakerFirestoreDocsDB.UserProjectsDashboard.AggregatedDocsCollection)
                .doc(CoMakerFirestoreDocsDB.UserProjectsDashboard.GeneratedImagesCollection), {
                [itemData.uid!]: {
                    ...itemData,
                    response: null,
                }
            }, {merge: true});

            await batch.commit();
        }
    });