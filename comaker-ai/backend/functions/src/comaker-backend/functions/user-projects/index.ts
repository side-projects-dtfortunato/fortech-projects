import * as functions from "firebase-functions";
import * as admin from "firebase-admin";
import {CoMakerFirestoreCollectionDB, CoMakerFirestoreDocsDB, ProjectDataModel} from "../../frontend-imports";
import {Log} from "../../../shareable-firebase-backend/utils/log";
import {getQuickResponse, getResponse} from "../../../shareable-firebase-backend/model-data/api-response.model";

export const createUserProject = functions.https.onCall(
    (data: { projectData: ProjectDataModel},
     context) => {

        if (!context.auth || !context.auth.uid) return getResponse(1001, undefined, "User must be logged in to execute this action");
        const userId: string = context && context.auth && context.auth.uid ? context.auth.uid : "";
        Log.onCall(data, userId);


        return new Promise(async (resolve, reject) => {

            // TODO Add validation if user has space to create new projects

            // First generate a unique id
            let projectData: ProjectDataModel = {
                ...data.projectData,
                createdAt: Date.now(),
                updatedAt: Date.now(),
                userCreatorId: userId,
                uid: admin.firestore()
                    .collection(CoMakerFirestoreCollectionDB.UserProjectsDashboardCollection)
                    .doc(userId)
                    .collection(CoMakerFirestoreDocsDB.UserProjectsDashboard.ProjectDetailsCollection)
                    .doc().id,
            };

            await admin.firestore()
                .collection(CoMakerFirestoreCollectionDB.UserProjectsDashboardCollection)
                .doc(userId)
                .collection(CoMakerFirestoreDocsDB.UserProjectsDashboard.ProjectDetailsCollection)
                .doc(projectData.uid!)
                .create(projectData);

            resolve(getQuickResponse(true, projectData));
        });
    });

/**
 * Update User Projects Dashboard with the project updates
 */
export const onUsersProjectsUpdated = functions
    .firestore
    .document(CoMakerFirestoreCollectionDB.UserProjectsDashboardCollection + "/{userId}/" + CoMakerFirestoreDocsDB.UserProjectsDashboard.ProjectDetailsCollection + '/{projectId}')
    .onWrite(async (data, context) => {
        const userId: string = context.params["userId"];
        const projectId: string = context.params["projectId"];

        Log.onWrite(data);
        let projectData: ProjectDataModel | undefined;


        let isDeleted: boolean = data.after ? false : true;
        let dataId: string = !isDeleted ? data.after.id : data.before.id;

        if (data && !isDeleted && data.after.data()) {
            projectData = {
                ...(isDeleted ? data.before.data() : data.after.data()) as ProjectDataModel,
                uid: dataId,
            };
        }

        const batch = admin.firestore().batch();
        if (!isDeleted && projectData) {
            // Update User Projects Dashboard
            batch.set(admin.firestore().collection(CoMakerFirestoreCollectionDB.UserProjectsDashboardCollection).doc(userId), {
                listProjects: {
                    [projectId]: projectData,
                },
            }, {merge: true});
        } else {
            // Delete Project from dashboard
            batch.set(admin.firestore().collection(CoMakerFirestoreCollectionDB.UserProjectsDashboardCollection).doc(userId), {
                listProjects: {
                    [projectId]: admin.firestore.FieldValue.delete(),
                },
            }, {merge: true});
        }

        // Commit changes
        const dbResponse = await batch.commit();

        return getQuickResponse(true, {dbResponse});
    });