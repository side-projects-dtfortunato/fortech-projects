import * as functions from "firebase-functions";
import {
    CoMakerFirestoreCollectionDB, CoMakerFirestoreDocsDB, GenerateContentAIOutputBaseModel,
    GeneratedContentBaseModel,
    UserSubscriptionPlanModel,
    UserSubscriptionUtils,
} from "../../../../frontend-imports";
import {getQuickResponse, getResponse} from "../../../../../shareable-firebase-backend/model-data/api-response.model";
import {Log} from "../../../../../shareable-firebase-backend/utils/log";
import {getDocumentData} from "../../../../../shareable-firebase-backend/utils/utils";
import {OpenAIManager} from "../../../../managers/openai/openai.manager";
import {OpenaiPromptUtils} from "../../../../managers/openai/openai-prompt.utils";
import * as admin from "firebase-admin";
import {UserSubscriptionBackendUtils} from "../../../../managers/user-subscription-backend.utils";

export const updateGeneratedContent = functions.runWith({
    memory: "1GB",
    timeoutSeconds: 300,
}).https.onCall(
    (data: { generateContentData: GeneratedContentBaseModel<any, any>, updatePrompt: string, updateCreditsCost: number },
     context) => {

        if (!context.auth || !context.auth.uid) return getResponse(1001, undefined, "User must be logged in to execute this action");
        const userId: string = context && context.auth && context.auth.uid ? context.auth.uid : "";
        Log.onCall(data, userId);


        return new Promise(async (resolve, reject) => {

            // Check if user has available credits
            let userSubscriptionPlan: UserSubscriptionPlanModel | undefined = await getDocumentData<UserSubscriptionPlanModel>(CoMakerFirestoreCollectionDB.UserSubscriptionPlanCollection, userId);

            if (!userSubscriptionPlan || !UserSubscriptionUtils.hasAvailableCredits(userSubscriptionPlan, data.updateCreditsCost)) {
                // Not enough credits
                resolve(getQuickResponse(false, null, "You don't have enough credits to generate this content. Upgrade your plan for more credits."));
                return;
            }

            // Prompt
            let generatedPrompt: string = "Rewrite and update the full previous generated content based on this user request to update the content: " + data.updatePrompt + ". Don't write any more message besides the expected generated content.";
            const openAIMessages: any = [
                {
                    role: "system",
                    content: data.generateContentData.inputUserData.outputFormat,
                },
                {
                    role: "assistant",
                    content: data.generateContentData.outputEditableData.body,
                },
                {
                    role: "user",
                    content: generatedPrompt,
                },
            ];
            console.log("OpenAI.messages:", openAIMessages);
            const openAIResponse: any = await OpenAIManager.requestChatGPT({
                messages: openAIMessages,
                credits: data.generateContentData.creditsCost,
                userId: userId,
            });

            if (!openAIResponse) {
                resolve(getQuickResponse(false, null, "Something failed while generating your content. Please try again or contact us."));
            }

            // Save the generated data

            const aiOutputGeneratedData: GenerateContentAIOutputBaseModel = OpenaiPromptUtils.generateAIOutput(data.generateContentData, openAIResponse);
            const updatedGeneratedContent: GeneratedContentBaseModel<any, any> = {
                ...data.generateContentData,
                outputEditableData: aiOutputGeneratedData,
                aiOutputGeneratedData: aiOutputGeneratedData,
                prompt: generatedPrompt,
                updatedAt: Date.now(),
            }


            const batch = admin.firestore().batch();
            batch.set(admin.firestore()
                .collection(CoMakerFirestoreCollectionDB.UserProjectsDashboardCollection)
                .doc(userId)
                .collection(CoMakerFirestoreDocsDB.UserProjectsDashboard.GeneratedContentItemsCollection).doc(updatedGeneratedContent.uid!), updatedGeneratedContent);

            // Reduce available credits
            batch.set(admin.firestore().collection(CoMakerFirestoreCollectionDB.UserSubscriptionPlanCollection).doc(userId),
                UserSubscriptionBackendUtils.updateUserSubscriptionCreditsSpent(userSubscriptionPlan, data.generateContentData.creditsCost),
                {merge: true});

            batch.commit().then((res) => {
                resolve(getQuickResponse(true, updatedGeneratedContent));
            }).catch((err) => {
                resolve(getQuickResponse(false, err));
            });

        });

    });