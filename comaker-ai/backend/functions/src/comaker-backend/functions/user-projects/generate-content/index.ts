import * as functions from "firebase-functions";
import * as admin from "firebase-admin";
import {getQuickResponse, getResponse} from "../../../../shareable-firebase-backend/model-data/api-response.model";
import {Log} from "../../../../shareable-firebase-backend/utils/log";
import {
    CoMakerFirestoreCollectionDB,
    CoMakerFirestoreDocsDB,
    GeneratedContentBaseModel, UserSubscriptionPlanModel, UserSubscriptionUtils,

} from "../../../frontend-imports";
import {OpenaiPromptUtils} from "../../../managers/openai/openai-prompt.utils";
import {OpenAIManager} from "../../../managers/openai/openai.manager";
import {getDocumentData} from "../../../../shareable-firebase-backend/utils/utils";
import {UserSubscriptionBackendUtils} from "../../../managers/user-subscription-backend.utils";

export const generateAIWritingContent = functions.runWith({
    memory: "1GB",
    timeoutSeconds: 300,
}).https.onCall(
    (data: { generateContentData: GeneratedContentBaseModel<any, any> },
     context) => {

        if (!context.auth || !context.auth.uid) return getResponse(1001, undefined, "User must be logged in to execute this action");
        const userId: string = context && context.auth && context.auth.uid ? context.auth.uid : "";
        Log.onCall(data, userId);


        return new Promise(async (resolve, reject) => {

            // Check if user has available credits
            let userSubscriptionPlan: UserSubscriptionPlanModel | undefined = await getDocumentData<UserSubscriptionPlanModel>(CoMakerFirestoreCollectionDB.UserSubscriptionPlanCollection, userId);

            if (!userSubscriptionPlan || !UserSubscriptionUtils.hasAvailableCredits(userSubscriptionPlan, data.generateContentData.creditsCost)) {
                // Not enough credits
                resolve(getQuickResponse(false, null, "You don't have enough credits to generate this content. Upgrade your plan for more credits."));
                return;
            }


            // Generate prompt
            let generatedPrompt: string = OpenaiPromptUtils.generatePrompt(data.generateContentData);

            const openAIResponse: any = await OpenAIManager.requestChatGPT({
                messages: [{
                    role: "user",
                    content: generatedPrompt,
                },
                    {
                        role: "system",
                        content: data.generateContentData.inputUserData.outputFormat,
                    },
                ],
                credits: data.generateContentData.creditsCost,
                userId: userId.substring(0, 5),
            });
            if (!openAIResponse) {
                resolve(getQuickResponse(false, null, "Something failed while generating your content. Please try again or contact us."));
            }
            /*let generatedPrompt: OpenAIChatMessages[] = OpenaiPromptUtils.generatePromptChatMessages(data.generateContentData);
            const openAIResponse = await OpenAIManager.requestChatGPT({
                messages: generatedPrompt,
                credits: data.generateContentData.creditsCost,
                userId: userId,
            });*/

            // Generate an UID
            const uid: string = admin.firestore()
                .collection(CoMakerFirestoreCollectionDB.UserProjectsDashboardCollection)
                .doc(userId)
                .collection(CoMakerFirestoreDocsDB.UserProjectsDashboard.GeneratedContentItemsCollection).doc().id;
            const aiOutputGeneratedData = OpenaiPromptUtils.generateAIOutput(data.generateContentData, openAIResponse);
            let generatedContentData: GeneratedContentBaseModel<any, any> = {
                ...data.generateContentData,
                userId: userId,
                prompt: generatedPrompt,
                aiResponseData: openAIResponse,
                aiOutputGeneratedData: aiOutputGeneratedData, // Generate Output data based on the content type
                outputEditableData: aiOutputGeneratedData,
                createdAt: Date.now(),
                updatedAt: Date.now(),
                uid: uid,
                isEditable: false,
            };

            const batch = admin.firestore().batch();
            batch.create(admin.firestore()
                .collection(CoMakerFirestoreCollectionDB.UserProjectsDashboardCollection)
                .doc(userId)
                .collection(CoMakerFirestoreDocsDB.UserProjectsDashboard.GeneratedContentItemsCollection).doc(uid), generatedContentData);

            // Reduce available credits
            batch.set(admin.firestore().collection(CoMakerFirestoreCollectionDB.UserSubscriptionPlanCollection).doc(userId),
                UserSubscriptionBackendUtils.updateUserSubscriptionCreditsSpent(userSubscriptionPlan, data.generateContentData.creditsCost),
                {merge: true});

            // Add Doc to aggregator docs
            batch.set(admin.firestore().collection(CoMakerFirestoreCollectionDB.UserProjectsDashboardCollection).doc(userId)
                .collection(CoMakerFirestoreDocsDB.UserProjectsDashboard.AggregatedDocsCollection)
                .doc(CoMakerFirestoreDocsDB.UserProjectsDashboard.GeneratedContentItemsCollection), {
                [uid]: {
                    ...generatedContentData,
                    aiResponseData: null,
                    aiOutputGeneratedData: null,
                    outputEditableData: null,
                }
            }, {merge: true});

            batch.commit().then((res) => {
                resolve(getQuickResponse(true, generatedContentData));
            }).catch((err) => {
                resolve(getQuickResponse(false, err));
            });
        });
    });

export const saveGeneratedContentChanges = functions.https.onCall(
    (data: { generateContentData: GeneratedContentBaseModel<any, any> },
     context) => {

        if (!context.auth || !context.auth.uid) return getResponse(1001, undefined, "User must be logged in to execute this action");
        const userId: string = context && context.auth && context.auth.uid ? context.auth.uid : "";
        Log.onCall(data, userId);

        if (data.generateContentData.userId !== userId) {
            return getQuickResponse(false, null, "User not authorized to save this changes");
        }

        return new Promise(async (resolve, reject) => {


            const batch = admin.firestore().batch();
            batch.set(admin.firestore()
                .collection(CoMakerFirestoreCollectionDB.UserProjectsDashboardCollection)
                .doc(userId)
                .collection(CoMakerFirestoreDocsDB.UserProjectsDashboard.GeneratedContentItemsCollection)
                .doc(data.generateContentData.uid!), data.generateContentData, {merge: true});


            batch.commit().then((res) => {
                resolve(getQuickResponse(true, data.generateContentData));
            }).catch((err) => {
                resolve(getQuickResponse(false, err));
            });
        });
    });

export const deleteGeneratedItem = functions.https.onCall(
    (data: {itemId: string},
     context) => {

        if (!context.auth || !context.auth.uid) return getResponse(1001, undefined, "User must be logged in to execute this action");
        const userId: string = context && context.auth && context.auth.uid ? context.auth.uid : "";
        Log.onCall(data, userId);


        return new Promise(async (resolve, reject) => {

            admin.firestore()
                .collection(CoMakerFirestoreCollectionDB.UserProjectsDashboardCollection)
                .doc(userId)
                .collection(CoMakerFirestoreDocsDB.UserProjectsDashboard.GeneratedContentItemsCollection)
                .doc(data.itemId!)
                .delete()
                .then((res) => {
                    resolve(getQuickResponse(true, res));
                }).catch((err) => {

                resolve(getQuickResponse(false, err, "Something failed when trying to delete the item"));
            });

        });
    });