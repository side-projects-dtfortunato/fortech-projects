import * as functions from "firebase-functions";
import * as admin from "firebase-admin";
import {getQuickResponse, getResponse} from "../../../../shareable-firebase-backend/model-data/api-response.model";
import {Log} from "../../../../shareable-firebase-backend/utils/log";
import {
    CoMakerFirestoreCollectionDB,
    CoMakerFirestoreDocsDB,
    GenerateImageBasePromptModel, UserSubscriptionPlanModel, UserSubscriptionUtils
} from "../../../frontend-imports";
import {ImagekitHelper} from "../../../../shareable-firebase-backend/utils/imagekit.helper";
import {UserSubscriptionBackendUtils} from "../../../managers/user-subscription-backend.utils";
import {getDocumentData} from "../../../../shareable-firebase-backend/utils/utils";
import {OpenAIManager} from "../../../managers/openai/openai.manager";

export const openAIGenerateImage = functions.https.onCall(
    (data: { generatedImage: GenerateImageBasePromptModel, env?: "PROD" | "DEV", generateImagePrompt: GenerateImageBasePromptModel},
     context) => {
        if (!context.auth || !context.auth.uid) return getResponse(1001, undefined, "User must be logged in to execute this action");
        const userId: string = context && context.auth && context.auth.uid ? context.auth.uid : "";
        Log.onCall(data, userId);


        return new Promise(async (resolve, reject) => {
            // Check if user has available credits
            let userSubscriptionPlan: UserSubscriptionPlanModel | undefined = await getDocumentData<UserSubscriptionPlanModel>(CoMakerFirestoreCollectionDB.UserSubscriptionPlanCollection, userId);

            if (!userSubscriptionPlan || !UserSubscriptionUtils.hasAvailableCredits(userSubscriptionPlan, data.generateImagePrompt.creditsCost)) {
                // Not enough credits
                resolve(getQuickResponse(false, null, "You don't have enough credits to generate this content. Upgrade your plan for more credits."));
                return;
            }

            // Generate image
            const generateImageResponse: GenerateImageBasePromptModel = await OpenAIManager.createImage(data.generateImagePrompt);

            if (generateImageResponse.response?.error) {
                resolve(getQuickResponse(false, generateImageResponse.response?.error, "Something failed while generating the image. Try again later"));
                return;
            }

            // Save the data
            let generatedData: GenerateImageBasePromptModel = {
                ...generateImageResponse,
                userId: userId,
                uid: admin.firestore()
                    .collection(CoMakerFirestoreCollectionDB.UserProjectsDashboardCollection)
                    .doc(userId).collection(CoMakerFirestoreDocsDB.UserProjectsDashboard.GeneratedImagesCollection).doc().id,
                createdAt: Date.now(),
                updatedAt: Date.now(),
                storedImages: [],
            };

            // Save Data
            let batch = admin.firestore().batch();

            const prefixPath: string = data.env === "DEV" ? "dev/" : "";
            // Save Images
            for (let i = 0; i < generatedData.response!.responsesUrls!.length; i++) {
                const imageUrl: string = generatedData.response!.responsesUrls![i];
                const storageFile = await ImagekitHelper.uploadImageFromUrl(imageUrl,
                    generatedData.uid!,
                    prefixPath + userId,
                    [generatedData.aiInput.promptText]);
                if (storageFile) {
                    generatedData.storedImages?.push(storageFile);
                }
            }

            // Save Generated Data
            batch.create(admin.firestore()
                .collection(CoMakerFirestoreCollectionDB.UserProjectsDashboardCollection)
                .doc(userId).collection(CoMakerFirestoreDocsDB.UserProjectsDashboard.GeneratedImagesCollection)
                .doc(generatedData.uid!), generatedData);

            // Reduce the credits from user
            batch.set(admin.firestore().collection(CoMakerFirestoreCollectionDB.UserSubscriptionPlanCollection).doc(userId),
                UserSubscriptionBackendUtils.updateUserSubscriptionCreditsSpent(userSubscriptionPlan, generatedData.creditsCost),
                {merge: true});


            batch.commit().then((res) => {
                resolve(getQuickResponse(true, generatedData));
                return;
            }).catch((err) => {
                resolve(getQuickResponse(false, generatedData, err.toString()));
                return;
            });


        });
    });


export const completeGeneratedImageContent = functions.https.onCall(
    (data: { generatedImage: GenerateImageBasePromptModel, env?: "PROD" | "DEV",},
     context) => {

        if (!context.auth || !context.auth.uid) return getResponse(1001, undefined, "User must be logged in to execute this action");
        const userId: string = context && context.auth && context.auth.uid ? context.auth.uid : "";
        Log.onCall(data, userId);


        return new Promise(async (resolve, reject) => {
            if (!data.generatedImage.response || data.generatedImage.response.error || !data.generatedImage.response.responsesUrls) {
                resolve(getQuickResponse(false, null, "The generated image has an error"));
                return;
            }

            // Get User Subscription Plan
            let userSubscriptionPlan: UserSubscriptionPlanModel = await getDocumentData<UserSubscriptionPlanModel>(CoMakerFirestoreCollectionDB.UserSubscriptionPlanCollection, userId);

            // Check if the user has enough credits
            if (!UserSubscriptionUtils.hasAvailableCredits(userSubscriptionPlan, data.generatedImage.creditsCost)) {
                resolve(getQuickResponse(false, null, "Not enough credits"));
                return;
            }

            // Save the data
            let generatedData: GenerateImageBasePromptModel = {
                ...data.generatedImage,
                userId: userId,
                uid: admin.firestore()
                    .collection(CoMakerFirestoreCollectionDB.UserProjectsDashboardCollection)
                    .doc(userId).collection(CoMakerFirestoreDocsDB.UserProjectsDashboard.GeneratedImagesCollection).doc().id,
                createdAt: Date.now(),
                updatedAt: Date.now(),
                storedImages: [],
            };

            // Save Data
            let batch = admin.firestore().batch();

            const prefixPath: string = data.env === "DEV" ? "dev/" : "";
            // Save Images
            for (let i = 0; i < generatedData.response!.responsesUrls!.length; i++) {
                const imageUrl: string = generatedData.response!.responsesUrls![i];
                const storageFile = await ImagekitHelper.uploadImageFromUrl(imageUrl,
                    generatedData.uid!,
                    prefixPath + userId,
                    [generatedData.aiInput.promptText]);
                if (storageFile) {
                    generatedData.storedImages?.push(storageFile);
                }
            }

            // Save Generated Data
            batch.create(admin.firestore()
                .collection(CoMakerFirestoreCollectionDB.UserProjectsDashboardCollection)
                .doc(userId).collection(CoMakerFirestoreDocsDB.UserProjectsDashboard.GeneratedImagesCollection)
                .doc(generatedData.uid!), generatedData);

            // Reduce the credits from user
            batch.set(admin.firestore().collection(CoMakerFirestoreCollectionDB.UserSubscriptionPlanCollection).doc(userId),
                UserSubscriptionBackendUtils.updateUserSubscriptionCreditsSpent(userSubscriptionPlan, generatedData.creditsCost),
                {merge: true});

            batch.commit().then((res) => {
                resolve(getQuickResponse(true, generatedData));
                return;
            }).catch((err) => {
                resolve(getQuickResponse(false, generatedData, err.toString()));
                return;
            })
        });
    }
);