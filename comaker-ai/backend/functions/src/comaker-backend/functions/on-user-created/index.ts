import * as functions from "firebase-functions";
import * as admin from "firebase-admin";
import {Log} from "../../../shareable-firebase-backend/utils/log";
import {
    ComakerCentralUserDataModel,
    CoMakerFirestoreCollectionDB,
    initUserSubscriptionPlan,
    UserProjectsDashboard, UserSubscriptionPlanModel
} from "../../frontend-imports";
import {SharedFirestoreCollectionDB} from "../../../shareable-firebase-backend/model-data/shared-firestore-collections";
import {
    SharedCentralUserDataModel
} from "../../../shareable-firebase-backend/model-data/frontend-model-data/shared-central-user-data.model";


export const onCreateUserProjectsDashboard = functions
    .firestore
    .document(SharedFirestoreCollectionDB.CentralUserData + '/{userId}')
    .onCreate(async (data, context) => {
        // Update catalog data
        Log.onCreate(data);
        let centralUserData: ComakerCentralUserDataModel | undefined;

        let userId: string = data.id;

        const userSubscriptionPlanData: UserSubscriptionPlanModel = initUserSubscriptionPlan(userId);

        if (data && data.data()) {
            centralUserData = {
                ...data.data() as SharedCentralUserDataModel,
                uid: userId,
                subscriptionPlan: userSubscriptionPlanData,
            } as ComakerCentralUserDataModel;
        }



        let batch = admin.firestore().batch();

        if (centralUserData) {

            // Create UserProjectDashboards doc
            batch.create(admin.firestore().collection(CoMakerFirestoreCollectionDB.UserProjectsDashboardCollection).doc(userId), {
                uid: userId,
                updatedAt: Date.now(),
                createdAt: Date.now(),
                userCreatorId: userId,
                listProjects: {},
                recentGeneratedItems: {},
            } as UserProjectsDashboard);

            // Create User Subscription Plan
            batch.create(admin.firestore().collection(CoMakerFirestoreCollectionDB.UserSubscriptionPlanCollection).doc(userId), userSubscriptionPlanData);

            await batch.commit();
        }
    });
