import {GenerationType} from "../generate-text-content/generated-content-base.model";

export interface StaticCategoryContentModel {
    uid: string; // unique id
    categoryLabel: string; // Label to display in the meny option
    description: string; // Short description of the category
    accentColor: string; // Color used on the icon of the category
    generationType: GenerationType;
    displayMenu?: boolean;
}