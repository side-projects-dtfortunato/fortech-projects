import {StaticCategoryContentModel} from "./static-category-content.model";

export interface StaticGenerateContentTypeModel {
    uid: string;
    label: string;
    description: string;
    category?: StaticCategoryContentModel;
    tags: string[];
    popular: boolean;
}