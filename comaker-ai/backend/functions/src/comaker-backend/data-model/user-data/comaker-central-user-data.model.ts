import {UserSubscriptionPlanModel} from "../subscription/UserSubscriptionPlan.model";
import {
    SharedCentralUserDataModel
} from "../../../shareable-firebase-backend/model-data/frontend-model-data/shared-central-user-data.model";

export interface ComakerCentralUserDataModel extends SharedCentralUserDataModel {
    subscriptionPlan?: UserSubscriptionPlanModel;
    creatorRole?: string;
}

export  const CreatorRoles = [
    "Copywriter",
    "Agency",
    "ECommerceOwner",
    "Marketer",
    "Manager",
    "Others",
]