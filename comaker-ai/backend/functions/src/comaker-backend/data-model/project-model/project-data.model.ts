import {GeneratedContentBaseModel} from "../generate-text-content/generated-content-base.model";
import {BaseModel} from "../../../shareable-firebase-backend/model-data/base.model";
import {StorageFileModel} from "../../../shareable-firebase-backend/model-data/storage-file.model";


export interface UserProjectsDashboard extends BaseModel {
    userCreatorId: string;
    listProjects: { [projectId: string]: ProjectDataModel };
    recentGeneratedItems: { [sectionItemId: string]: GeneratedContentBaseModel<any, any> };
}

export interface ProjectDataModel extends BaseModel {
    userCreatorId: string;
    projectTitle?: string;
    projectDescription?: string;
    projectIcon?: StorageFileModel;
}




