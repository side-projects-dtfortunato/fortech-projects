export interface ComakeDrivenDocumentModel {
    type: string;
    systemPrompt: string;
    inputData: {
        docTitle: string,
        customPrompt: string;
    },
    aiResponse?: {
        status: "SUCCESS" | "ERROR",
        message: string,
    },
}