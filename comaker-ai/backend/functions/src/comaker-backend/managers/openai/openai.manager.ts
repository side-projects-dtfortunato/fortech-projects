import {OpenAI} from "openai";
import {OpenAIChatMessages} from "./openai-prompt.utils";
import {GenerateImageBasePromptModel, GenerationStatus} from "../../frontend-imports";

const API_KEY = "sk-proj-IrFNdERCGulIkcMHjobxT3BlbkFJ0VXvaYuhmT61U3osS4sU"
const openai = new OpenAI({
    apiKey: API_KEY,
});

// [Cheaper to expensive]: Ada -> Babbage -> Curie -> DAVINCI -> gpt-3.5-turbo
export enum OpenAIModelTypes {
    CHATGPT = "gpt-4-turbo-preview",
    DAVINCI = "text-davinci-003",
    CURIE = "text-curie-001",
    BABBAGE = "text-babbage-001",
    ADA = "text-ada-001"
}

export class OpenAIManager {
    static TOKENS_PER_CREDIT = 500; // 1 credit === 500 tokens ~= 100 words
    static TOKENS_CHATGPT_PER_CREDIT = 700; // 1 credit === 500 tokens ~= 100 words

    static async requestChatGPT(params: { messages: OpenAIChatMessages[], credits: number, userId: string }) {
        const response = await openai.chat.completions.create({
            model: "gpt-4o-mini",
            messages: params.messages,
            user: params.userId,
            temperature: 0.7,
        });
        return response;
    }

    static async createImage(generateImagePrompt: GenerateImageBasePromptModel): Promise<GenerateImageBasePromptModel> {
        let response = await openai.images.generate({
            prompt: generateImagePrompt.aiInput.promptText,
            size: generateImagePrompt.aiInput.imageSize,
            user: generateImagePrompt.userId,
            n: generateImagePrompt.aiInput.numberResults,
            response_format: "url",
        });
        console.log(response);

        if (response.data) {
            return {
                ...generateImagePrompt,
                response: {
                    originalResponse: response,
                    responsesUrls: (response.data).map((item: any) => item.url),
                },
                status: GenerationStatus.COMPLETED,
            };
        } else {
            return {
                ...generateImagePrompt,
                response: {
                    error: {
                        message: "Some error occured",
                        code: 100,
                    },
                },
                status: GenerationStatus.COMPLETED,
            };
        }
    }

}


