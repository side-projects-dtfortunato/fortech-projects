import {
    GenerateContentAIOutputBaseModel,
    GenerateContentBaseInputFormModel,
    GeneratedContentBaseModel,
    GenerationTextContentType,
    GenerationPointOfView,
    GenerationToneStyle
} from "../../frontend-imports";
import {OpenAIModelTypes} from "./openai.manager";

export interface OpenAIChatMessages {
    role: "system" | "assistant" | "user",
    content: string,
}

export class OpenaiPromptUtils {

    static generatePrompt(generateContentData: GeneratedContentBaseModel<any, any>) {
        let prompt: string = "";
        // Response format
        //prompt += generateContentData.inputUserData.outputFormat;
        prompt += "Generate the content based on the next Questions and Answers: ";

        const inputData: GenerateContentBaseInputFormModel = generateContentData.inputUserData as GenerateContentBaseInputFormModel;

        if (Object.keys(inputData.inputFields).length) {
            Object.values(inputData.inputFields).filter((item) => {
                return item.formQuestionLabel.length > 3 && item.formUserAnswer && item.formUserAnswer.length > 3;
            }).forEach((item) => {
                prompt += " " + item.formQuestionLabel;
                if (!prompt.endsWith(" ")) {
                    prompt += " ";
                }
                prompt += item.formUserAnswer + ". ";
            });
        }

        if (inputData.pointOfView) {
            prompt += this.getPointOfViewPrompt(inputData.pointOfView) + ". ";
        }

        if (inputData.toneStyle) {
            prompt += this.getStyleTone(inputData.toneStyle) + ". ";
        }

        if (inputData.outputLanguage) {
            prompt += "Write the content in the language " + inputData.outputLanguage + ". ";
        }

        // Length of the article
        /*let numberWords: number = generateContentData.creditsCost * 500;
        prompt += "The generated content should have around " + (numberWords - 500) + " - " + numberWords + " words. ";*/


        return prompt;
    }

    static generatePromptChatMessages(generateContentData: GeneratedContentBaseModel<any, any>): OpenAIChatMessages[] {

        let aiMessages: OpenAIChatMessages[] = [];

        aiMessages.push({
            role: "user",
            content: generateContentData.inputUserData.outputFormat,
        });

        const inputData: GenerateContentBaseInputFormModel = generateContentData.inputUserData as GenerateContentBaseInputFormModel;

        if (Object.keys(inputData.inputFields).length) {
            Object.values(inputData.inputFields).filter((item) => {
                return item.formQuestionLabel.length > 3 && item.formUserAnswer && item.formUserAnswer.length > 3;
            }).forEach((item) => {
                aiMessages.push({
                    role: "user",
                    content: item.formQuestionLabel + ": " + item.formUserAnswer,
                });
            });
        }

        if (inputData.pointOfView) {
            aiMessages.push({
                role: "user",
                content: this.getPointOfViewPrompt(inputData.pointOfView),
            });
        }

        if (inputData.toneStyle) {
            aiMessages.push({
                role: "user",
                content: this.getStyleTone(inputData.toneStyle),
            });
        }

        if (inputData.outputLanguage) {
            aiMessages.push({
                role: "user",
                content: "Write the content in the language " + inputData.outputLanguage + ". ",
            });
        }
        return aiMessages;
    }

    static getPointOfViewPrompt(pov: GenerationPointOfView) {
        switch (pov) {
            case GenerationPointOfView.FIRST_PERSON:
                return "Write in the first person perspective";
            case GenerationPointOfView.SECOND_PERSON:
                return "Write in the second person perspective";
            case GenerationPointOfView.THIRD_PERSON:
            default:
                return "Write in the third person perspective";
        }
    }

    static getStyleTone(styleTone: GenerationToneStyle) {
        switch (styleTone) {
            case GenerationToneStyle.ENTHUSIASTIC:
                return "Write in a enthusiastic tone";
            case GenerationToneStyle.PROFESSIONAL:
                return "Write in a professional tone";
            case GenerationToneStyle.HUMOROUS:
                return "Write in a humoristic tone";
            case GenerationToneStyle.INSPIRATIONAL:
                return "Write in a inspirational tone";
            case GenerationToneStyle.CONVERSATIONAL:
            default:
                return "Write in a conversational tone";
        }
    }

    static getAIModelTypeByContentType(type: GenerationTextContentType): OpenAIModelTypes{

        switch (type) {
            case GenerationTextContentType.BLOG_POST:
                return OpenAIModelTypes.DAVINCI;
        }
        return OpenAIModelTypes.DAVINCI;
    }

    static generateAIOutput(generatedContentData: GeneratedContentBaseModel<GenerateContentBaseInputFormModel, GenerateContentAIOutputBaseModel>, openAIResponse: any): GenerateContentAIOutputBaseModel {
        let generatedText: string = openAIResponse && openAIResponse.choices ? openAIResponse.choices[0].message.content : "";

        return {
            body: generatedText,
            outputContentType: generatedContentData.inputUserData.outputContentType,
        } as GenerateContentAIOutputBaseModel;

    }
}