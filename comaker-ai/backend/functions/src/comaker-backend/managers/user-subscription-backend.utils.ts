import {
    UserSubscriptionPlanModel, UserSubscriptionUtils,
} from "../frontend-imports";
import * as admin from "firebase-admin";

export class UserSubscriptionBackendUtils {

    static updateUserSubscriptionCreditsSpent(userSubscriptionPlan: UserSubscriptionPlanModel, creditsCost: number): any {
        let updateData: any = {};

        if (UserSubscriptionUtils.hasAvailableCredits(userSubscriptionPlan, creditsCost)) {
            if (userSubscriptionPlan.availableCredits >= creditsCost) {
                // Only get credits from the available ones
                updateData = {
                    availableCredits: admin.firestore.FieldValue.increment(-creditsCost),
                };
            } else {
                // Not enough on available credits, has to use also on the extra credits
                let extraCreditsSpent: number = creditsCost - userSubscriptionPlan.availableCredits;
                updateData = {
                    availableCredits: admin.firestore.FieldValue.increment(-userSubscriptionPlan.availableCredits),
                    extraCredits: admin.firestore.FieldValue.increment(-extraCreditsSpent),
                };
            }
        }
        return updateData;
    }
}