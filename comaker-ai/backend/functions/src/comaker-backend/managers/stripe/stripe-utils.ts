import {
    SubscriptionPlansStaticData,
    UserSubscriptionPlanType
} from "../../frontend-imports";
import * as admin from "firebase-admin";

export const stripeDev = require('stripe')('sk_test_51MjqsYEMLa2n90268TJBv5IK9aCgD46bfzQuSvMZeFlditEnb6m0FdUgpTzbMUfeT3tfgGd0YV8XlTwTmqjdOtTZ00ix5u6GNv');
export const stripeProd = require('stripe')('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');

export const StripeConfigs = {
    PROD: {
        webHookKey: "whsec_coOJwnBIT5mIwmEsnbeoG4LUYqvvY6VB",
        plans: {
            "SUBSCRIBED_HOBBY": {
                priceId: "price_1MqjzNEMLa2n9026iG8J8ep3",
            },
            "SUBSCRIBED_PRO": {
                priceId: "price_1MqXL9EMLa2n9026OsN3jT3Z",
            },
            "BUY_50_CREDITS": {
                priceId: "price_1MwvZnEMLa2n9026cDB0VAfu",
            }
        },
    },
    DEV: {
        webHookKey: "whsec_whbt3QQnSrL3kKNc1LCbSvb5FIHXjssS",
        plans: {
            "SUBSCRIBED_HOBBY": {
                priceId: "price_1Mk8aAEMLa2n9026kszzcAj6",
            },
            "SUBSCRIBED_PRO": {
                priceId: "price_1Mlcz8EMLa2n9026OAZCCf2P",
            },
            "BUY_50_CREDITS": {
                priceId: "price_1MvnBVEMLa2n90267c90nduX",
            }
        },

    },
}

export class StripeUtils {

    static getPlanTypeByPriceId(env: string, priceId: string) {
        let envConfigs: any = env === "PROD" ? StripeConfigs.PROD : StripeConfigs.DEV;
        let planType: string = "";

        Object.keys(envConfigs.plans).forEach((itPlanType) => {
            if (envConfigs.plans[itPlanType].priceId === priceId) {
                planType = itPlanType;
            }
        });

        return planType;
    }

    static getUserSubscriptionUpdateByPlan(planType: string) {
        let updatedData: any = {};

        switch (planType as UserSubscriptionPlanType) {
            case UserSubscriptionPlanType.SUBSCRIBED_HOBBY:
                updatedData = {
                    initialCredits: SubscriptionPlansStaticData.hobby.credits,
                    availableCredits: SubscriptionPlansStaticData.hobby.credits,
                    planType: planType,
                };
                break;
            case UserSubscriptionPlanType.SUBSCRIBED_PRO:
                updatedData = {
                    initialCredits: SubscriptionPlansStaticData.pro.credits,
                    availableCredits: SubscriptionPlansStaticData.pro.credits,
                    planType: planType,
                };
                break;
            case UserSubscriptionPlanType.BUY_50AI_CREDITS:
                updatedData = {
                    extraCredits: admin.firestore.FieldValue.increment(SubscriptionPlansStaticData.buy50AICredits.credits),
                };
                break;
        }
        return updatedData;
    }
}