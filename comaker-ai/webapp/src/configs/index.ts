
export default class RootConfigs {
    static BACKEND_ENV: "DEV" | "PROD" = "PROD"; // "PROD"
    static ENVIRONMENT_DEV = false;
    static DISABLE_PAYMENTS = false;
    static BASE_URL = "https://CoMaker.ai";
    static SITE_NAME = "CoMaker.ai";
    static SITE_TITLE = "CoMaker.ai - Your AI-powered content creation partner\n";
    static SITE_DESCRIPTION = "CoMaker.ai is an AI-powered platform that assists entrepreneurs, marketers, and influencers in developing and growing their businesses. Get personalized project management, task tracking, and content creation ideas in one place. Start building your business with CoMaker.ai today.";
    static SOCIAL_NETWORKS: {
        [socialId: string]: {
            iconUrl: string,
            label: string,
            link: string,
        },
    } = {
        medium: {
            label: "Blog",
            link: "https://blog.comaker.ai/",
            iconUrl: "https://miro.medium.com/fit/c/152/152/1*sHhtYhaCe2Uc3IU0IgKwIQ.png",
        },
    }

    static OPEN_AI_API_KEY: string | undefined = this.ENVIRONMENT_DEV ? process.env.DEV_OPENAI_API_KEY : process.env.PROD_OPENAI_API_KEY;

    static FIREBASE_CONFIG = this.ENVIRONMENT_DEV ?

        // DEV Config
        {
            apiKey: "AIzaSyCkl5-SyLs0OKyJTsAzolhIpwLSBd_9fj0",
            authDomain: "comaker-ai-qa.firebaseapp.com",
            projectId: "comaker-ai-qa",
            storageBucket: "comaker-ai-qa.appspot.com",
            messagingSenderId: "100040874383",
            appId: "1:100040874383:web:802cc2948795b99105d3fa",
            measurementId: "G-2HL1XZKBT9"
        } :
        // PROD
        {
            apiKey: "AIzaSyBkWhpXjv2-1Wxrebw15BiWAkm1vPYJp20",
            authDomain: "comaker-ai.firebaseapp.com",
            projectId: "comaker-ai",
            storageBucket: "comaker-ai.appspot.com",
            messagingSenderId: "744231851079",
            appId: "1:744231851079:web:cd63e7d02d6f4b1c5c0d62",
            measurementId: "G-4XTE6RSE1Y"
        };

}
