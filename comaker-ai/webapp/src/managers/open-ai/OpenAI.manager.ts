import {ApiResponse} from "../../components/react-shared-module/logic/shared-data/datamodel/base.model";
import AuthManager from "../../components/react-shared-module/logic/auth/auth.manager";
import {ComakerCentralUserDataModel} from "../../data/data-model/user-data/comaker-central-user-data.model";
import {
    GeneratedImageSize,
    GenerateImageBasePromptModel,
    GenerationStatus,
    InputCreateImageType
} from "../../data/data-model/generate-image/generate-image-base-prompt.model";
import {Configuration, OpenAIApi} from 'openai';
import RootConfigs from "../../configs";
import {createReadStream, PathLike} from "fs";
import {ComakerBackendApi} from "../../data/api/comaker-backend.api";
import {UserSubscriptionUtils} from "../../data/data-model/subscription/UserSubscriptionPlan.model";



const configuration = new Configuration({
    apiKey: RootConfigs.OPEN_AI_API_KEY,
});
const openai = new OpenAIApi(configuration);

export const OpenAIImageCosts = {
    1024: {
        creditsCost: 5,
    },
    512: {
        creditsCost: 4,
    },
    256: {
        creditsCost: 3,
    }
}

export class OpenAIManager {

    static calculateImageGenerationCost(imageSize: GeneratedImageSize, numResults: number) {
        let cost:number = 0;
        switch (imageSize) {
            case "1024x1024":
                cost = OpenAIImageCosts["1024"].creditsCost * numResults;
                break;
            case "512x512":
                cost = OpenAIImageCosts["512"].creditsCost * numResults;
                break;
            case "256x256":
                cost = OpenAIImageCosts["256"].creditsCost * numResults;
                break;
        }
        return cost;
    }

    static async hasUserAvailableCredits(credits: number): Promise<ApiResponse<any>> {
        let apiResponse: ApiResponse<any> = {
            responseCode: 1000,
            isSuccess: false,
            message: "Some error occurred while validating your available credits",
        };

        if (AuthManager.isUserLogged()) {
            let centralData: ComakerCentralUserDataModel | undefined = await AuthManager.getCentralUserData();

            if (centralData && centralData.subscriptionPlan && UserSubscriptionUtils.hasAvailableCredits(centralData.subscriptionPlan, credits)) {
                apiResponse.isSuccess = true;
                apiResponse.responseCode = 2000;
                apiResponse.message = "";

                return apiResponse;
            } else {
                apiResponse.message = "You don't have enough credits to generate this content. Please, upgrade your account";

                return apiResponse;
            }
        } else {
            apiResponse.message = "You must be logged in to execute this action";
        }

        return apiResponse;
    }

    static async generateImage(generateImagePrompt: GenerateImageBasePromptModel): Promise<ApiResponse<GenerateImageBasePromptModel | undefined>> {
        let apiResponse: ApiResponse<any> = {
            responseCode: 1000,
            isSuccess: false,
            message: "Some error occurred while validating your available credits",
        };
        let generateImageResponseData: GenerateImageBasePromptModel = {
            ...generateImagePrompt,
        };

        // Validate if User has enough credits
        apiResponse = await this.hasUserAvailableCredits(generateImagePrompt.creditsCost);
        if (!apiResponse.isSuccess) {
            return apiResponse;
        }

        console.log("Prompt: ", generateImagePrompt);
        // Generate Image
        switch (generateImagePrompt.aiInput.generationType) {

            /** CREATE IMAGE */
            case InputCreateImageType.CREATE_IMAGE:
                // generateImageResponseData = await this.createImage(generateImagePrompt);
                apiResponse = await ComakerBackendApi.openAIGenerateImage({
                    env: RootConfigs.BACKEND_ENV,
                    generateImagePrompt: generateImagePrompt,
                });
                break;

            /** CREATE IMAGE EDIT */
            /*case InputCreateImageType.CREATE_IMAGE_EDIT:
                if (!generateImagePrompt.aiInput.imageFiles || generateImagePrompt.aiInput.imageFiles.length < 2) {
                    apiResponse = {
                        isSuccess: false,
                        message: "Didn't received the expected Image Files",
                        responseCode: 1000,
                    };
                } else {
                    generateImageResponseData = await this.createImageEdit(generateImagePrompt, generateImagePrompt.aiInput.imageFiles![0], generateImagePrompt.aiInput.imageFiles![1]);
                }
                break;*/

            /** CREATE IMAGE VARIATION */
            /*case InputCreateImageType.CREATE_IMAGE_VARIATION:
                if (!generateImagePrompt.aiInput.imageFiles || generateImagePrompt.aiInput.imageFiles.length < 1) {
                    apiResponse = {
                        isSuccess: false,
                        message: "Didn't received the expected Image Files",
                        responseCode: 1000,
                    };
                } else {
                    generateImageResponseData = await this.createImageVariation(generateImagePrompt, generateImagePrompt.aiInput.imageFiles![0]);
                }
                break;*/

        }

        /*
        // Update API Response based on the generated Image Response data
        if (generateImageResponseData.response?.error) {
            apiResponse = {
                isSuccess: false,
                responseCode: 1000,
                message: generateImageResponseData.response.error.message,
                responseData: generateImageResponseData,
            };
        } else {
            // Before the response, save the data in our Backend and reduce the credits from the user
            apiResponse = await ComakerBackendApi.completeGeneratedImageContent({generatedImage: generateImageResponseData, env: RootConfigs.BACKEND_ENV});
        }*/

        return apiResponse;
    }


    static async createImage(generateImagePrompt: GenerateImageBasePromptModel): Promise<GenerateImageBasePromptModel> {
        let response = await openai.createImage({
            prompt: generateImagePrompt.aiInput.promptText,
            size: generateImagePrompt.aiInput.imageSize,
            user: generateImagePrompt.userId,
            n: generateImagePrompt.aiInput.numberResults,
            response_format: "url",
        });
        console.log(response);

        if (response.status === 200) {
            return {
                ...generateImagePrompt,
                response: {
                    originalResponse: response.data,
                    responsesUrls: (response.data.data).map((item: any) => item.url),
                },
                status: GenerationStatus.COMPLETED,
            };
        } else {
            return {
                ...generateImagePrompt,
                response: {
                    error: {
                        message: response.statusText,
                        code: response.status,
                    },
                },
                status: GenerationStatus.COMPLETED,
            };
        }
    }

    static async createImageEdit(generateImagePrompt: GenerateImageBasePromptModel, imageFile: PathLike, maskImageFile: PathLike): Promise<GenerateImageBasePromptModel> {
        let response = await openai.createImageEdit(
            createReadStream(imageFile) as any,
            createReadStream(maskImageFile) as any,
            generateImagePrompt.aiInput.promptText,
            generateImagePrompt.aiInput.numberResults,
            generateImagePrompt.aiInput.imageSize,
            "url",
            generateImagePrompt.userId,
        );
        if (response.status === 200) {
            return {
                ...generateImagePrompt,
                response: {
                    originalResponse: response.data,
                    responsesUrls: (response.data.data).map((item: any) => item.url),
                },
                status: GenerationStatus.COMPLETED,
            };
        } else {
            return {
                ...generateImagePrompt,
                response: {
                    error: {
                        message: response.statusText,
                        code: response.status,
                    },
                },
                status: GenerationStatus.COMPLETED,
            };
        }
    }

    static async createImageVariation(generateImagePrompt: GenerateImageBasePromptModel, originalImage: PathLike): Promise<GenerateImageBasePromptModel> {
        let response = await openai.createImageVariation(
            createReadStream(originalImage) as any,
            generateImagePrompt.aiInput.numberResults,
            generateImagePrompt.aiInput.imageSize,
            "url",
            generateImagePrompt.userId,
        );
        if (response.status === 200) {
            return {
                ...generateImagePrompt,
                response: {
                    originalResponse: response.data,
                    responsesUrls: (response.data.data).map((item: any) => item.url),
                },
                status: GenerationStatus.COMPLETED,
            };
        } else {
            return {
                ...generateImagePrompt,
                response: {
                    error: {
                        message: response.statusText,
                        code: response.status,
                    },
                },
                status: GenerationStatus.COMPLETED,
            };
        }
    }


}