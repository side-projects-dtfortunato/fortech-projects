import CustomRootLayoutComponent from "../../../components/app-components/root/CustomRootLayout.component";
import GenerateContentContainerComponent
    from "../../../components/app-components/generate-content-forms/generate-content-container.component";
import {GenerationTextContentType} from "../../../data/data-model/generate-text-content/generated-content-base.model";
import {
    StaticGenerateContentTypeModel
} from "../../../data/static-data/static-data-model/static-generate-content-type.model";
import {PageHeadProps} from "../../../components/react-shared-module/ui-components/root/RootLayout.component";
import {
    StaticGenerateContentCategoriesList,
    StaticGenerateContentListContent
} from "../../../data/static-data/static-content/static-generate-content-list.content";


export async function getStaticPaths() {
    let contentType: string[] = Object.values(StaticGenerateContentListContent).filter((item) => item.category?.uid !== StaticGenerateContentCategoriesList.imageGenerator.uid).map((item) => item.uid.toLowerCase());
    let listPaths: any[] = [];

    if (contentType) {
        listPaths = Object.values(contentType).map((contentType) => {
            return {
                params: {
                    contentType: contentType,
                },
            }
        });
    }

    return {
        paths: listPaths,
        fallback: false, // can also be true or 'blocking'
    }
}

export function getStaticProps(context: any) {
    const contentType: string = context.params.contentType as string;

    return {
        props: {
            contentType: contentType.toUpperCase(),
        }
    };
}

export default function GenerateContentTypePage(props: {contentType: string}) {
    let contentStaticInfo: StaticGenerateContentTypeModel | undefined;

    Object.values(StaticGenerateContentListContent).forEach((staticItem) => {
        if (staticItem.uid === props.contentType) {
            contentStaticInfo = staticItem;
        }
    });

    let pageHeadProps: PageHeadProps | undefined;
    if (contentStaticInfo) {
        pageHeadProps = {
            title: contentStaticInfo.label,
            description: contentStaticInfo.description,
        };
    }

    return (
        <CustomRootLayoutComponent pageHeadProps={pageHeadProps}>
            <GenerateContentContainerComponent contentType={props.contentType} />
        </CustomRootLayoutComponent>
    )
}

export function getGenerateContentTypePageURL(contentType: string | GenerationTextContentType) {
    return "/generate/" + contentType.toLowerCase();
}