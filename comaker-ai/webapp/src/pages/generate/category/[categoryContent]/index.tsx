import CustomRootLayoutComponent from "../../../../components/app-components/root/CustomRootLayout.component";
import GenerateContentMenuListComponent
    from "../../../../components/app-components/generate-content-menu/generate-content-menu-list.component";
import PageHeaderTitleComponent from "../../../../components/app-components/shared/PageHeaderTitle.component";
import {
    StaticGenerateContentCategoriesList
} from "../../../../data/static-data/static-content/static-generate-content-list.content";
import {PageHeadProps} from "../../../../components/react-shared-module/ui-components/root/RootLayout.component";


export async function getStaticPaths() {
    let categoryTypes: string[] = Object.keys(StaticGenerateContentCategoriesList).map((item) => item);
    let listPaths: any[] = [];

    if (categoryTypes) {
        listPaths = Object.values(categoryTypes).map((categoryType) => {
            return {
                params: {
                    categoryContent: categoryType,
                },
            }
        });
    }

    console.log("getStaticPaths.listPaths:", listPaths);

    return {
        paths: listPaths,
        fallback: false, // can also be true or 'blocking'
    }
}

export function getStaticProps(context: any) {
    const categoryContent: string = context.params.categoryContent as string;

    return {
        props: {
            categoryContent: categoryContent,
        }
    };
}

export default function GenerateCategoryContentPage(props: {categoryContent: string}) {
    console.log("GenerateCategoryContentPage: ", props);
    let categoryLabel: string = StaticGenerateContentCategoriesList[props.categoryContent].categoryLabel;
    let description: string = StaticGenerateContentCategoriesList[props.categoryContent].description;

    const pageHeadProps: PageHeadProps = {
        title: categoryLabel,
        description: description,
    }

    return (
        <CustomRootLayoutComponent pageHeadProps={pageHeadProps}>
            <div className='flex flex-col gap-5'>
                <PageHeaderTitleComponent title={categoryLabel} subtitle={description} displayAvatar={false} />
                <div className='divider text-base text-slate-500'>{categoryLabel}</div>
                <GenerateContentMenuListComponent category={props.categoryContent} />
            </div>

        </CustomRootLayoutComponent>
    )
}

export function getGenerateCategoryContentPageURL(contentCategory: string) {
    return "/generate/category/" + contentCategory;
}