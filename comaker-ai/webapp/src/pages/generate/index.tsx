import CustomRootLayoutComponent from "../../components/app-components/root/CustomRootLayout.component";
import GenerateContentMenuListComponent
    from "../../components/app-components/generate-content-menu/generate-content-menu-list.component";
import PageHeaderTitleComponent from "../../components/app-components/shared/PageHeaderTitle.component";

export function getStaticProps(context: any) {
    return {
        props: {},
    };
}

export default function GenerateContentPage() {

    return (
        <CustomRootLayoutComponent>
            <div className='flex flex-col gap-5'>
                <PageHeaderTitleComponent title={"Here's all the generation tools"} subtitle={"Discover every available tools to help you."} displayAvatar={true} />
                <div className='divider text-base text-slate-500'>All Tools</div>
                <GenerateContentMenuListComponent />
            </div>
        </CustomRootLayoutComponent>
    )
}

export function getGenerateContentAllToolsUrl() {
    return "/generate";
}