// pages/_document.js
import { Html, Head, Main, NextScript } from 'next/document'
import RootConfigs from "../configs";

export default function Document() {
    return (
        <Html>

            <Head>

                <meta charSet="UTF-8" />
                <meta name="application-name" content={RootConfigs.SITE_NAME} />
                <meta name="apple-mobile-web-app-capable" content="yes" />
                <meta name="apple-mobile-web-app-status-bar-style" content="default" />
                <meta name="apple-mobile-web-app-title" content={RootConfigs.SITE_NAME} />
                <meta name="format-detection" content="telephone=no" />
                <meta name="mobile-web-app-capable" content="yes" />
                <meta name="msapplication-config" content="none"/>
                <meta name="msapplication-TileColor" content="#073264" />
                <meta name="msapplication-tap-highlight" content="no" />
                <meta name="theme-color" content="#EEB0BC" />
                <meta name="ahrefs-site-verification" content="5657a2d93c8df92da115eaacbc702b11c20b8b8b377ada59bcfc8c20cd91b82c"/>

                <link rel="apple-touch-icon" href="/images/ic_logo.png" />
                <link rel="icon" type="image/png" sizes="32x32" href="/images/icon-32.png" />
                <link rel="icon" type="image/png" sizes="16x16" href="/images/icon-16.png" />
                <link rel="shortcut icon" href="/favicon.ico" />
                <link rel="manifest" href="/manifest.json" />


                <link rel="preconnect" href="https://fonts.googleapis.com"/>
                <link rel="preconnect" href="https://fonts.gstatic.com" />
                <link
                    href="https://fonts.googleapis.com/css2?family=Raleway:ital,wght@0,100;0,300;0,400;0,700;1,100;1,300;1,400;1,700&family=Roboto:wght@300&display=swap"
                    rel="stylesheet"/>

            </Head>
            <body>
            <Main />
            <NextScript />
            </body>

        </Html>
    )
}