import CustomRootLayoutComponent from "../../../components/app-components/root/CustomRootLayout.component";
import PageHeaderTitleComponent from "../../../components/app-components/shared/PageHeaderTitle.component";
import ListGeneratedItemsContainer
    from "../../../components/app-components/user-dashboard/containers/list-generated-items.container";
import {GenerationType} from "../../../data/data-model/generate-text-content/generated-content-base.model";

export default function MyContentPage() {
    return (
        <CustomRootLayoutComponent authRequired={true}>
            <div className='flex flex-col gap-5'>
                <PageHeaderTitleComponent title={"Here's your generated content"} subtitle={"Edit, use or remove your existing content"} displayAvatar={true} />
                <div className='divider text-base text-slate-500'>MY CONTENT</div>
                <ListGeneratedItemsContainer listType={GenerationType.TEXT} />
            </div>
        </CustomRootLayoutComponent>
    )
}

export function getMyContentPageUrl() {
    return "/my-content/text";
}