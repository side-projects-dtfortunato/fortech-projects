import CustomRootLayoutComponent from "../../../components/app-components/root/CustomRootLayout.component";
import GenerateImageContentContainerComponent
    from "../../../components/app-components/generate-image/generate-image-content-container.component";
import {GenerationImageContentType} from "../../../data/data-model/generate-image/generate-image-base-prompt.model";
import {
    StaticGenerateContentCategoriesList,
    StaticGenerateContentListContent
} from "../../../data/static-data/static-content/static-generate-content-list.content";

export async function getStaticPaths() {
    let contentType: string[] = Object.values(StaticGenerateContentListContent).filter((item) => item.category?.uid === StaticGenerateContentCategoriesList.imageGenerator.uid).map((item) => item.uid);
    let listPaths: any[] = [];

    if (contentType) {
        listPaths = Object.values(contentType).map((contentType) => {
            return {
                params: {
                    contentType: contentType,
                },
            }
        });
    }

    return {
        paths: listPaths,
        fallback: true, // can also be true or 'blocking'
    }
}

export function getStaticProps(context: any) {
    const contentType: string = context.params.contentType as string;

    return {
        props: {
            contentType: contentType.toUpperCase(),
        }
    };
}
export default function GenerateContentTypePage(props: {contentType: string}) {
    return (
        <CustomRootLayoutComponent>
            <GenerateImageContentContainerComponent imageContentType={props.contentType} />
        </CustomRootLayoutComponent>
    )
}

export function getGenerateImageTypePageURL(contentType: string | GenerationImageContentType) {
    return "/generate-image/" + contentType.toLowerCase();
}