import {useAuthState} from "react-firebase-hooks/auth";
import {auth} from "../../../../components/react-shared-module/logic/shared-data/firebase.utils";
import {useState} from "react";
import {GeneratedContentBaseModel} from "../../../../data/data-model/generate-text-content/generated-content-base.model";
import AuthManager from "../../../../components/react-shared-module/logic/auth/auth.manager";
import {ComakerBackendApi} from "../../../../data/api/comaker-backend.api";
import CustomRootLayoutComponent from "../../../../components/app-components/root/CustomRootLayout.component";
import GenerateContentContainerComponent
    from "../../../../components/app-components/generate-content-forms/generate-content-container.component";
import SpinnerComponent from "../../../../components/react-shared-module/ui-components/shared/Spinner.component";
import NotFoundContentComponent
    from "../../../../components/react-shared-module/ui-components/shared/NotFoundContent.component";
import {
    GenerateImageBasePromptModel
} from "../../../../data/data-model/generate-image/generate-image-base-prompt.model";
import GenerateImageContentContainerComponent
    from "../../../../components/app-components/generate-image/generate-image-content-container.component";
import GeneratedImagesViewerComponent
    from "../../../../components/app-components/generate-image/viewer/generated-images.viewer.component";

export default function GenerateImageViewerPage(props: { itemId: string}) {
    const [currentUser, isAuthLoading] = useAuthState(auth);
    const [generatedContentItem, setGeneratedContentItem] = useState<GenerateImageBasePromptModel | undefined>(undefined)
    const [isLoading, setIsLoading] = useState(true);

    if (!generatedContentItem && AuthManager.isUserLogged()) {
        // Load data
        ComakerBackendApi.getGeneratedImageDetails({
            userId: AuthManager.getUserId()!,
            itemId: props.itemId,
        }).then((response) => {
            // @ts-ignore
            setGeneratedContentItem(response);
            setIsLoading(false);
        })
    }

    if (generatedContentItem) {
        return (
            <CustomRootLayoutComponent authRequired={true}>
                <GeneratedImagesViewerComponent item={generatedContentItem} />
            </CustomRootLayoutComponent>
        )
    } else if (isAuthLoading || (isLoading && AuthManager.isUserLogged())) {
        return (
            <CustomRootLayoutComponent>
                <SpinnerComponent />
            </CustomRootLayoutComponent>
        )
    } else if (!isAuthLoading && !isLoading && AuthManager.isUserLogged()) {
        return (
            <CustomRootLayoutComponent>
                <NotFoundContentComponent/>
            </CustomRootLayoutComponent>
        )
    } else {
        return (
            <CustomRootLayoutComponent notAuthorized={true}>
            </CustomRootLayoutComponent>
        )
    }
}



GenerateImageViewerPage.getInitialProps = async (context: any) => {
    const itemId: string = context.query.id as string;

    return {
        itemId: itemId,
    };
}


export function getGeneratedImageViewerPageURL(itemId: string) {
    return "/generate-image/viewer/" + itemId;
}