import CustomRootLayoutComponent from "../../components/app-components/root/CustomRootLayout.component";
import PageHeaderTitleComponent from "../../components/app-components/shared/PageHeaderTitle.component";
import GenerateContentMenuListComponent
    from "../../components/app-components/generate-content-menu/generate-content-menu-list.component";

export default function GenerateImagePage() {
    return (
        <CustomRootLayoutComponent>
            <div className='flex flex-col gap-5'>
                <PageHeaderTitleComponent title={"Generation Image Tools"} subtitle={"Generate an image based on your content"} displayAvatar={true} />
                <div className='divider text-base text-slate-500'>Image Generation</div>
                TODO List of all image generation tools
            </div>
        </CustomRootLayoutComponent>
    )
}