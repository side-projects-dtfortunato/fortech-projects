
import {getLoginPageLink} from "../login";
import {useAuthState} from "react-firebase-hooks/auth";
import {useRouter} from "next/router";
import {useState} from "react";
import RootConfigs from "../../configs";
import {auth} from "../../components/react-shared-module/logic/shared-data/firebase.utils";
import RootLayoutComponent, {PageHeadProps} from "../../components/react-shared-module/ui-components/root/RootLayout.component";
import AuthManager from "../../components/react-shared-module/logic/auth/auth.manager";
import EditProfileFormComponent
    from "../../components/react-shared-module/ui-components/edit-profile/EditProfileForm.component";
import CustomRootLayoutComponent from "../../components/app-components/root/CustomRootLayout.component";
import UserEditSettingsComponent from "../../components/app-components/user-settings/user-edit-settings.component";
import PageHeaderTitleComponent from "../../components/app-components/shared/PageHeaderTitle.component";

export default function UserProfilePage(props: {}) {
    const router = useRouter();
    const [currentUser, isAuthLoading] = useAuthState(auth);
    const [hasDataChanged, setHasDataChanged] = useState<boolean>(false);

    let paramIsNewUser: boolean = false;
    if (router.query["newUser"]) {
        paramIsNewUser = Boolean(router.query["newUser"] as string);
    }


    const pageHeadProps: PageHeadProps = {
        title: "Edit your profile",
        description: RootConfigs.SITE_DESCRIPTION,
    }


    if (isAuthLoading) {
        // Is loading Central User Data
        return (
            <CustomRootLayoutComponent pageHeadProps={pageHeadProps} authRequired={true}>
                <div className='flex h-screen w-full justify-content-center align-items-center'>
                    <div className='loading' />
                </div>
            </CustomRootLayoutComponent>
        )
    } else if (AuthManager.isUserLogged()) {
        return (
            <CustomRootLayoutComponent pageHeadProps={pageHeadProps}>
                <div className='w-full flex flex-col items-center gap-3'>
                    <PageHeaderTitleComponent title={"User Settings"} subtitle={"Configure your personal preferences"} displayAvatar={false} />
                    <UserEditSettingsComponent />
                </div>
            </CustomRootLayoutComponent>
        )
    }
    else {
        router.push(getLoginPageLink());
        return (
            <CustomRootLayoutComponent isLoading={true} isIndexable={false}>
                <div className="h-screen"></div>
            </CustomRootLayoutComponent>
        )
    }
}


export function getUserProfileLink(newUser?: boolean) {
    let urlBuilder: string = "/edit-profile/user-profile";
    if (newUser) {
        urlBuilder += "?newUser=" + true;
    }
    return urlBuilder;
}