import CustomRootLayoutComponent from "../../components/app-components/root/CustomRootLayout.component";
import CoMakerPricingTablePlansComponent
    from "../../components/app-components/pricing-page/CoMakerPricingTablePlans.component";
import PageHeaderTitleComponent from "../../components/app-components/shared/PageHeaderTitle.component";
import ManageSubscriptionBillingButtonComponent
    from "../../components/app-components/subscription/shared/ManageSubscriptionBillingButton.component";
import {getLanguageLabel} from "../../components/react-shared-module/logic/language/language.helper";
import SubscriptionPlanButtonComponent
    from "../../components/app-components/subscription/shared/SubscriptionPlanButton.component";
import {UserSubscriptionPlanType} from "../../data/data-model/subscription/UserSubscriptionPlan.model";
import RootConfigs from "../../configs";

export default function PricingPage() {

    function renderBuyCreditsContainer() {
        return (
            <div className='flex flex-col gap-2 justify-center items-center'>
                <h3 className='custom'>Add More Credits</h3>
                <span className='subtitle'>Buy 50 AI Credits without subscribe to a plan, to let you produce more content powered by AI.</span>
                <SubscriptionPlanButtonComponent planType={UserSubscriptionPlanType.BUY_50AI_CREDITS} />
                <div className='divider text-slate-400'>OR SUBSCRIBE A PLAN</div>
            </div>
        )
    }

    // Logged
    return (
        <CustomRootLayoutComponent>
            <div className='flex flex-col items-stretch gap-5'>
                {
                    RootConfigs.DISABLE_PAYMENTS ? (<div className='flex rounded w-full p-5 bg-red-500 items-center justify-center'>
                        <span className='w-full text-center text-white text-xl'>WE HAVE SUSPENDED OUR PLANS (Sorry for the inconvenience)</span>
                    </div>) : (<></>)
                }
                <PageHeaderTitleComponent title={getLanguageLabel("NAVBAR_PRICING_BTN")}
                                          subtitle={"Manage your billing and payment details."} displayAvatar={true} />
                {renderBuyCreditsContainer()}
                <CoMakerPricingTablePlansComponent />
                <div className='w-full flex items-center justify-center'>
                    <ManageSubscriptionBillingButtonComponent />
                </div>

            </div>
        </CustomRootLayoutComponent>
    )
}

export function getPricingPageUrl() {
    return "/pricing";
}