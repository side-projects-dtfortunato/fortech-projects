import '../styles/globals.css'
import '../styles/landing-page.css'
import Script from 'next/script';
import type {AppProps} from 'next/app'
import RootConfigs from "../configs";

export default function App({Component, pageProps}: AppProps) {
    return (
        <>
            <Script src="https://cdn.tailwindcss.com"></Script>
            {/** Set the proper google tag manager **/}
            <Script
                id="gtm_0"
                src={"https://www.googletagmanager.com/gtag/js?id=" + RootConfigs.FIREBASE_CONFIG.measurementId}
                strategy="afterInteractive"
            />
            <Script id="google-analytics" strategy="afterInteractive">
                {
                    `
              window.dataLayer = window.dataLayer || [];
              function gtag(){window.dataLayer.push(arguments);}
              gtag('js', new Date());
        
              gtag('config', '${RootConfigs.FIREBASE_CONFIG.measurementId}');
            `}
            </Script>


            <Script id="Tawk-chat" type="text/javascript">
                {
                    `
              var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
                    (function(){
                    var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
                    s1.async=true;
                    s1.src='https://embed.tawk.to/641c14ec4247f20fefe7860c/1gs6r3fbo';
                    s1.charset='UTF-8';
                    s1.setAttribute('crossorigin','*');
                    s0.parentNode.insertBefore(s1,s0);
                })();
            `}

            </Script>


            <Component {...pageProps} />
        </>
    )
}
