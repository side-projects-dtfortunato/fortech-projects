import {GeneratedContentBaseModel} from "../../../data/data-model/generate-text-content/generated-content-base.model";
import {useAuthState} from "react-firebase-hooks/auth";
import {auth} from "../../../components/react-shared-module/logic/shared-data/firebase.utils";
import AuthManager from "../../../components/react-shared-module/logic/auth/auth.manager";
import {ComakerBackendApi} from "../../../data/api/comaker-backend.api";
import CustomRootLayoutComponent from "../../../components/app-components/root/CustomRootLayout.component";
import GenerateContentContainerComponent
    from "../../../components/app-components/generate-content-forms/generate-content-container.component";
import NotFoundContentComponent
    from "../../../components/react-shared-module/ui-components/shared/NotFoundContent.component";
import {useState} from "react";
import SpinnerComponent from "../../../components/react-shared-module/ui-components/shared/Spinner.component";


export default function GeneratedItemPage(props: { itemId: string}) {
    const [currentUser, isAuthLoading] = useAuthState(auth);
    const [generatedContentItem, setGeneratedContentItem] = useState<GeneratedContentBaseModel<any, any> | undefined>(undefined)
    const [isLoading, setIsLoading] = useState(true);

    if (!generatedContentItem && AuthManager.isUserLogged()) {
        // Load data
        ComakerBackendApi.getGeneratedItemDetails({
            userId: AuthManager.getUserId()!,
            generatedItemId: props.itemId,
        }).then((response) => {
            // @ts-ignore
            setGeneratedContentItem(response);
            setIsLoading(false);
        })
    }

    if (generatedContentItem) {
        return (
            <CustomRootLayoutComponent authRequired={true}>
                <GenerateContentContainerComponent contentType={generatedContentItem.contentType}
                                                   defaultData={generatedContentItem}/>
            </CustomRootLayoutComponent>
        )
    } else if (isAuthLoading || (isLoading && AuthManager.isUserLogged())) {
        return (
            <CustomRootLayoutComponent>
                <SpinnerComponent />
            </CustomRootLayoutComponent>
        )
    } else if (!isAuthLoading && !isLoading && AuthManager.isUserLogged()) {
        return (
            <CustomRootLayoutComponent>
                <NotFoundContentComponent/>
            </CustomRootLayoutComponent>
        )
    } else {
        return (
            <CustomRootLayoutComponent notAuthorized={true}>
            </CustomRootLayoutComponent>
        )
    }
}

GeneratedItemPage.getInitialProps = async (context: any) => {
    const itemId: string = context.query.id as string;

    return {
        itemId: itemId,
    };
}

export function getGeneratedItemDetailsPageUrl(itemId: string) {
    return "/generated-item/" + itemId;
}