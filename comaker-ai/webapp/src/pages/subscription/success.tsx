import CustomRootLayoutComponent from "../../components/app-components/root/CustomRootLayout.component";
import {CheckCircleIcon} from "@heroicons/react/20/solid";
import Link from "next/link";

export default function SubscriptionSuccessPage() {
    return (
        <CustomRootLayoutComponent>
            <div className="flex flex-col justify-center items-center h-screen">
                <CheckCircleIcon className="text-green-500 w-16 h-16 mb-4" />
                <h1 className="text-3xl font-bold mb-4">Success!</h1>
                <p className="text-lg text-center">
                    You have successfully subscribed to our plan. Thank you for your support!
                </p>
                <Link href={"/"} className='btn btn-primary btn-outline mt-5' >Create content</Link>
            </div>
        </CustomRootLayoutComponent>
    )
}

export function getSubscriptionSuccessPageURL() {
    return "/subscription/success";
}