import {BasePostDataModel} from "../components/react-shared-module/logic/shared-data/datamodel/base-post-data.model";
import {auth, getAPIDocument} from "../components/react-shared-module/logic/shared-data/firebase.utils";
import {
    SharedFirestoreCollectionDB,
    SharedFirestoreDocsDB
} from "../components/react-shared-module/logic/shared-data/datamodel/shared-firestore-collections";
import {
    LandingPageDataUtils,
    SharedLandingPageAggregatorsModel
} from "../components/react-shared-module/logic/shared-data/datamodel/shared-landing-page-aggregators.model";
import CustomRootLayoutComponent from "../components/app-components/root/CustomRootLayout.component";
import GenerateContentMenuListComponent
    from "../components/app-components/generate-content-menu/generate-content-menu-list.component";
import PageHeaderTitleComponent from "../components/app-components/shared/PageHeaderTitle.component";
import {useAuthState} from "react-firebase-hooks/auth";
import AuthManager from "../components/react-shared-module/logic/auth/auth.manager";
import ComakerLandingPageComponent
    from "../components/app-components/comaker-landing-page/comaker-landing-page.component";

export function getStaticProps() {
    return {
        props: {},
    };
}

export default function Home() {
    const [currentUser, isAuthLoading] = useAuthState(auth);

    if (isAuthLoading) {
        return <ComakerLandingPageComponent />
    } else if (AuthManager.isUserLogged()) {
        return (
            <CustomRootLayoutComponent>
                <div className='flex flex-col gap-5'>
                    <PageHeaderTitleComponent title={"Hey. I'm your CoMaker AI"} subtitle={"Let's make new content together"} displayAvatar={true} />
                    <div className='divider text-base text-slate-500'>DASHBOARD</div>
                    <GenerateContentMenuListComponent />
                </div>
            </CustomRootLayoutComponent>
        )
    } else {
        return <ComakerLandingPageComponent />
    }

}
