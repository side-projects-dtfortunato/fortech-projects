import CustomRootLayoutComponent from "../../components/app-components/root/CustomRootLayout.component";
import Head from "next/head";
import FaqAccordionComponent from "../../components/app-components/shared/FaqAccordion.component";

function getStaticProps() {
    return {
        props: {}, // will be passed to the page component as props
    }
}

export default function FaqPage() {
    return (
        <CustomRootLayoutComponent>
            <Head>
                <title>FAQ - CoMaker.ai</title>
            </Head>
            <div className="container mx-auto px-4 py-16">
                <h1 className="text-3xl font-bold mb-8">Frequently Asked Questions</h1>
                <div className="space-y-4">
                    {faqData.map((faq, index) => (
                        <FaqAccordionComponent key={index} question={faq.question} answer={faq.answer} />
                    ))}
                </div>
            </div>
        </CustomRootLayoutComponent>
    );
}

const faqData = [
    {
        question: 'What is CoMaker.ai?',
        answer:
            'CoMaker.ai is an AI-powered content generation platform that helps you create high-quality articles, marketing materials, and more based on your provided keywords or descriptions.',
    },
    {
        question: 'How do I get started with CoMaker.ai?',
        answer:
            'To get started, sign up for an account, choose a pricing plan, and start generating content using our simple and intuitive interface.',
    },
    {
        question: 'What are the available pricing plans?',
        answer:
            'CoMaker.ai offers three pricing plans: Free, Hobby, and Pro. Each plan comes with different usage limits and features, allowing you to choose the one that best suits your needs.',
    },
    {
        question: 'How does the AI generate content?',
        answer:
            'CoMaker.ai uses advanced AI models trained on large datasets to generate content based on the input you provide. The AI analyzes the input and generates relevant and coherent text that matches your requirements.',
    },
    {
        question: 'Can I generate a logo using CoMaker.ai?',
        answer:
            'Yes, CoMaker.ai offers an AI-powered logo generation feature that allows you to create unique and visually appealing logos based on your preferences and requirements.',
    },
];