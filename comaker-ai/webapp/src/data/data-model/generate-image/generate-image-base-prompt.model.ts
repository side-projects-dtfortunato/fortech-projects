import {StorageFileModel} from "../../../components/react-shared-module/logic/shared-data/datamodel/storage-file.model";
import {PathLike} from "fs";
import {BaseModel} from "../../../components/react-shared-module/logic/shared-data/datamodel/base.model";
import {GenerateBaseModel} from "../generate-text-content/generated-content-base.model";



export enum InputCreateImageType {
    CREATE_IMAGE = "CREATE_IMAGE",
    CREATE_IMAGE_EDIT = "CREATE_IMAGE_EDIT",
    CREATE_IMAGE_VARIATION = "CREATE_IMAGE_VARIATION"
}

export enum GenerationImageContentType {
    CREATE_IMAGE = "CREATE_IMAGE"
}

export enum GeneratedImageSize {
    SMALL = "256x256",  MEDIUM = "512x512", LARGE = "1024x1024",
}
export enum GenerationStatus {
    REQUESTING = "REQUESTING", COMPLETED = "COMPLETED",
}


export interface GenerateImageBasePromptModel extends BaseModel, GenerateBaseModel {
    userId?: string;
    creditsCost: number;
    generationType: GenerationImageContentType;
    aiInput: {
        promptText: string;
        numberResults: number;
        generationType: InputCreateImageType;
        imageSize: GeneratedImageSize;
        imageFiles?: PathLike[];
    },
    status: GenerationStatus;
    storedImages?: StorageFileModel[];
    response?: {
        originalResponse?: any;
        responsesUrls?: string[];
        error?: {
            message: string,
            code: number,
        },
    },
}