export const CoMakerFirestoreCollectionDB = {
    UserProjectsDashboardCollection: "UserProjectsDashboard",
    UserSubscriptionPlanCollection: "UserSubscriptionPlan",
}

export const CoMakerFirestoreDocsDB = {
    UserProjectsDashboard: {
        ProjectDetailsCollection: "ProjectDetails",
        GeneratedContentItemsCollection: "GeneratedContentItems",
        GeneratedImagesCollection: "GeneratedImages",
        AggregatedDocsCollection: "AggregatedDocs",
    },
}