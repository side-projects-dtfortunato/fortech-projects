import {BaseModel} from "../../../components/react-shared-module/logic/shared-data/datamodel/base.model";
import {Duration} from "ts-duration";

export enum UserSubscriptionPlanType {
    FREE = "FREE",
    SUBSCRIBED_HOBBY = "SUBSCRIBED_HOBBY",
    SUBSCRIBED_PRO = "SUBSCRIBED_PRO",

    BUY_50AI_CREDITS = "BUY_50AI_CREDITS",
};

export function initUserSubscriptionPlan(userId: string): UserSubscriptionPlanModel {
    return {
        uid: userId,
        availableCredits: SubscriptionPlansStaticData.free.credits,
        initialCredits: SubscriptionPlansStaticData.free.credits,
        extraCredits: 0,
        planType: UserSubscriptionPlanType.FREE,
        renewPeriod: "MONTHLY",
        renewalDate: UserSubscriptionUtils.renewalDate(),
        updatedAt: Date.now(),
        createdAt: Date.now(),
    }
}


export interface UserSubscriptionPlanModel extends BaseModel {
    planType: UserSubscriptionPlanType;
    renewPeriod: "MONTHLY" | "YEARLY",
    availableCredits: number; // Available credits from the plan
    initialCredits: number; // Plan credits
    extraCredits: number; // Credits bought from the user for instance
    renewalDate?: number;
    customerId?: string;
    stripeData?: {
        subscription: string;
        period_end: number;
        period_start: number;
        dataObject: any;
    };
}

export interface SubscriptionPlanParams {
    planType: UserSubscriptionPlanType;
    credits: number;
    stripeId: string;
    stripeTestId?: string;
    label: string;
}

export const SubscriptionPlansStaticData = {
    free: {
        planType: UserSubscriptionPlanType.FREE,
        credits: 10,
        label: "Free Plan",
        price: "$0",
    },
    hobby: {
        planType: UserSubscriptionPlanType.SUBSCRIBED_HOBBY,
        credits: 100,
        label: "Hobby Plan",
        price: "$10",
    },
    pro: {
        planType: UserSubscriptionPlanType.SUBSCRIBED_PRO,
        credits: 500,
        label: "Pro Plan",
        price: "$20",
    },
    buy50AICredits: {
        planType: UserSubscriptionPlanType.BUY_50AI_CREDITS,
        credits: 50,
        label: "Buy 50 AI Credits",
        price: "$6",
    },
}

export class UserSubscriptionUtils {

    static getPlanTypeLabel(planType: UserSubscriptionPlanType) {

        let planData = Object.values(SubscriptionPlansStaticData).find((item) => item.planType === planType);

        if (planData) {
            return planData.label;
        } else {
            return "";
        }
    }


    static hasAvailableCredits(userSubscriptionPlan: UserSubscriptionPlanModel, creditsCost: number) {
        let totalCredits: number = userSubscriptionPlan.availableCredits;
        if (userSubscriptionPlan.extraCredits) {
            totalCredits += userSubscriptionPlan.extraCredits;
        }

        return totalCredits >= creditsCost;
    }

    static renewalDate() {
        return Date.now() + Duration.hour(24*30).milliseconds;
    }

}