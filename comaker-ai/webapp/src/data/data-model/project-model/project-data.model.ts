import {BaseModel} from "../../../components/react-shared-module/logic/shared-data/datamodel/base.model";
import {StorageFileModel} from "../../../components/react-shared-module/logic/shared-data/datamodel/storage-file.model";
import {GeneratedContentBaseModel} from "../generate-text-content/generated-content-base.model";


export interface UserProjectsDashboard extends BaseModel {
    userCreatorId: string;
    listProjects: { [projectId: string]: ProjectDataModel };
    recentGeneratedItems: { [sectionItemId: string]: GeneratedContentBaseModel<any, any> };
}

export interface ProjectDataModel extends BaseModel {
    userCreatorId: string;
    projectTitle?: string;
    projectDescription?: string;
    projectIcon?: StorageFileModel;
}




