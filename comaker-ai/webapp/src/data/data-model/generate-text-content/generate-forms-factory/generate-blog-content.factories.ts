import {
    GenerateContentBaseInputFormModel, GenerateContentInputFormFieldModel,
    GeneratedContentBaseModel, GenerationPointOfView,
    GenerationTextContentType,
    GenerationToneStyle,
    GenerationType
} from "../generated-content-base.model";
import {
    StaticGenerateContentListContent
} from "../../../static-data/static-content/static-generate-content-list.content";
import {GeneratePresetsInputFormsFactory} from "../../../utils/generate-content.utils";

/* BLOG POST */
export const initBlogPostModel = (): GeneratedContentBaseModel<any, any> => {
    return {
        isEditable: true,
        contentType: GenerationTextContentType.BLOG_POST,
        type: GenerationType.TEXT,
        creditsCost: 1, // ~2500tokens
        inputUserData: {
            inputFields: {
                generationTitle: { // SHOULD ALWAYS HAVE THE GENERATION TITLE
                    id: "generationTitle",
                    formQuestionLabel: "Title of the blog post: ",
                    formHint: "e.g: The best side hustle ideas to make money",
                    formType: "text",
                    isRequired: true,
                    formUserAnswer: "",
                },
                description: {
                    id: "description",
                    formHint: "e.g: List the best side hustles to make money online, without any experience and any investment upfront",
                    formQuestionLabel: "Describe the topic of your blog post: ",
                    formType: "textarea",
                    isRequired: true,
                },
                targetAudience: {
                    ...GeneratePresetsInputFormsFactory.targetAudience
                },
                blogKeywords: {
                    ...GeneratePresetsInputFormsFactory.blogKeywords,
                },
            },
            toneStyle: GenerationToneStyle.INSPIRATIONAL,
            pointOfView: GenerationPointOfView.SECOND_PERSON,
            outputLanguage: "English (UK)",
            submitBtnText: "Write a blog post",
            outputContentType: "HTML",
            // outputFormat: "Write a blog post based on the user inputs. Respond the content with EditorJS Blocks format.",
            outputFormat: "You are an assistant that helps the user to write a blog post based on the user inputs." + HTML_OUTPUTFORMAT,
        },
        contentTypeDetails: StaticGenerateContentListContent.blogPost,
    }
}

/** BLOG LIST TITLES */
export const initBlogTitleIdeas = (): GeneratedContentBaseModel<any, any> => {
    return {
        isEditable: true,
        contentType: GenerationTextContentType.BLOG_TITLE_IDEAS,
        type: GenerationType.TEXT,
        creditsCost: 1, // ~1000tokens
        inputUserData: {
            inputFields: {
                generationTitle: {
                    id: "generationTitle",
                    formQuestionLabel: "What is the topic of your blog post?",
                    formType: "text",
                    formHint: "Enter the topic of your blog post",
                    isRequired: true,
                },
                targetAudience: {
                    ...GeneratePresetsInputFormsFactory.targetAudience
                },
                blogType: {
                    ...GeneratePresetsInputFormsFactory.blogType
                },
                blogKeywords: {
                    ...GeneratePresetsInputFormsFactory.blogKeywords,
                },
            },
            toneStyle: GenerationToneStyle.INSPIRATIONAL,
            pointOfView: GenerationPointOfView.SECOND_PERSON,
            outputLanguage: "English (UK)",
            submitBtnText: "Generate List Titles",
            outputContentType: "HTML",
            outputFormat: "You are an assistant that helps the user to write 10 titles suggestions for a blog post based on the user inputs." + HTML_OUTPUTFORMAT,
        },
        contentTypeDetails: StaticGenerateContentListContent.blogTitles,
    }
}

/** BLOG PARAGRAPH */
export const initBlogParagraphFactory = (): GeneratedContentBaseModel<any, any> => {
    return {
        isEditable: true,
        contentType: GenerationTextContentType.BLOG_PARAGRAPH_WRITER,
        contentTypeDetails: StaticGenerateContentListContent.blogParagraph,
        type: GenerationType.TEXT,
        creditsCost: 1, // ~500tokens
        inputUserData: {
            inputFields: {
                generationTitle: {
                    id: "generationTitle",
                    formQuestionLabel: "What is the topic of your blog post?",
                    formType: "text",
                    formHint: "Enter the topic of your blog post",
                    isRequired: true,
                },
                paragraphTitle: {
                    id: "generationTitle",
                    formQuestionLabel: "What is the paragraph title?",
                    formType: "text",
                    formHint: "Enter the title of the paragraph",
                    isRequired: true,
                },
                paragraphType: {
                    id: "paragraphType",
                    formQuestionLabel: "What type of paragraph do you want to create?",
                    formType: "dropdown",
                    formHint: "Select the type of paragraph you want to create",
                    options: [
                        "Introduction",
                        "Body",
                        "Conclusion",
                        "Quote",
                        "Statistic",
                        "Other",
                    ],
                    isRequired: true,
                },
                targetAudience: {
                    ...GeneratePresetsInputFormsFactory.targetAudience
                },
                blogType: {
                    ...GeneratePresetsInputFormsFactory.blogType
                },
                blogKeywords: {
                    ...GeneratePresetsInputFormsFactory.blogKeywords,
                },
            },
            toneStyle: GenerationToneStyle.INSPIRATIONAL,
            pointOfView: GenerationPointOfView.SECOND_PERSON,
            outputLanguage: "English (UK)",
            submitBtnText: "Generate a paragraph",
            outputContentType: "HTML",
            outputFormat: "You are an assistant that helps the user to write a Blog paragraph based on the user inputs." + HTML_OUTPUTFORMAT,
        } as GenerateContentBaseInputFormModel,
    }
}

/** BLOG REWRITE LINK */
export const initBlogRewriteLinkFactory = (): GeneratedContentBaseModel<any, any> => {
    return {
        isEditable: true,
        contentType: GenerationTextContentType.BLOG_REWRITE_POST_LINK,
        contentTypeDetails: StaticGenerateContentListContent.blogRewriteLink,
        type: GenerationType.TEXT,
        creditsCost: 1,
        inputUserData: {
            inputFields: {
                generationTitle: {
                    id: "generationTitle",
                    formQuestionLabel: "What is the topic of your blog post?",
                    formType: "text",
                    formHint: "Enter the topic of your blog post",
                    isRequired: true,
                },
                link: {
                    id: "link",
                    formQuestionLabel: "Link of the post to rewrite?",
                    formType: "text",
                    formHint: "Enter the full and valid URL to be rewritten",
                    isRequired: true,
                },
            },
            toneStyle: GenerationToneStyle.INSPIRATIONAL,
            pointOfView: GenerationPointOfView.SECOND_PERSON,
            outputLanguage: "English (UK)",
            submitBtnText: "Rewrite the Article",
            outputContentType: "HTML",
            outputFormat: "You are an assistant that helps the user to write a Blog post article based on the Link provided by the user." + HTML_OUTPUTFORMAT,
        } as GenerateContentBaseInputFormModel,
    }
}

/** BLOG REWRITE Content */
export const initBlogRewriteContentFactory = (): GeneratedContentBaseModel<any, any> => {
    return {
        isEditable: true,
        contentType: GenerationTextContentType.BLOG_REWRITE_POST_CONTENT,
        contentTypeDetails: StaticGenerateContentListContent.blogRewriteContent,
        type: GenerationType.TEXT,
        creditsCost: 1,
        inputUserData: {
            inputFields: {
                generationTitle: {
                    id: "generationTitle",
                    formQuestionLabel: "What is the topic of your blog post?",
                    formType: "text",
                    formHint: "Enter the topic of your blog post",
                    isRequired: true,
                },
                content: {
                    id: "content",
                    formQuestionLabel: "Blog post content to rewrite: ",
                    formType: "textarea",
                    formHint: "Copy & Paste the content of the post that you would like to rewrite?",
                    isRequired: true,
                },
            },
            toneStyle: GenerationToneStyle.INSPIRATIONAL,
            pointOfView: GenerationPointOfView.SECOND_PERSON,
            outputLanguage: "English (UK)",
            submitBtnText: "Rewrite the Article",
            outputContentType: "HTML",
            outputFormat: "You are an assistant that helps the user to write a Blog post article based on the content provided by the user." + HTML_OUTPUTFORMAT,
        } as GenerateContentBaseInputFormModel,
    }
}

/** JOBS - CV/Resume */
export const initJobsCVResumeFactory = (): GeneratedContentBaseModel<any, any> => {
    return {
        isEditable: true,
        contentType: GenerationTextContentType.JOBS_CV_WRITING,
        contentTypeDetails: StaticGenerateContentListContent.jobsResumeCvWriting,
        type: GenerationType.TEXT,
        creditsCost: 1,
        inputUserData: {
            inputFields: {
                generationTitle: { // SHOULD ALWAYS HAVE THE GENERATION TITLE
                    id: "generationTitle",
                    formQuestionLabel: "Title of the content: ",
                    formHint: "e.g: CV to apply for a Google Position",
                    formType: "text",
                    isRequired: true,
                    formUserAnswer: "",
                },
                cvName: {
                    id: "cvName",
                    formQuestionLabel: "What is your name?",
                    formType: "text",
                    formHint: "e.g: Your first and last name",
                    isRequired: true,
                },
                cvPosition: {
                    id: "cvPosition",
                    formQuestionLabel: "What is your desired job position?",
                    formType: "text",
                    formHint: "eg: Engineering Manager; Frontend Developer; Architect",
                    isRequired: true,
                },
                cvQualifications: {
                    id: "cvQualifications",
                    formQuestionLabel: "What are your qualifications?",
                    formType: "textarea",
                    formHint: "List your educational background, certifications, and relevant experience.",
                    isRequired: true,
                },
                cvSkills: {
                    id: "cvSkills",
                    formQuestionLabel: "What are your skills?",
                    formType: "tags",
                    formHint: "List your top skills that relate to your desired job position.",
                    formValue: [],
                    isRequired: true,
                },
                cvExperience: {
                    id: "cvExperience",
                    formQuestionLabel: "What is your work experience?",
                    formType: "textarea",
                    formHint: "List your work experience in reverse chronological order, starting with your most recent job.",
                    isRequired: true,
                },
                cvAdditionalInfo: {
                    id: "cvAdditionalInfo",
                    formQuestionLabel: "Do you have any additional information to include?",
                    formType: "textarea",
                    formHint: "List any additional information that you would like to include in your resume or CV.",
                    isRequired: false,
                },

            },
            toneStyle: GenerationToneStyle.PROFESSIONAL,
            pointOfView: GenerationPointOfView.FIRST_PERSON,
            outputLanguage: "English (UK)",
            submitBtnText: "Write the Resume/CV",
            outputContentType: "HTML",
            outputFormat: "You are an assistant that helps the user to write a Resume/CV to let him search for a job." + HTML_OUTPUTFORMAT,
        } as GenerateContentBaseInputFormModel,
    }
}

/** JOBS - Cover Letter*/
export const initJobsCoverLetterFactory = (): GeneratedContentBaseModel<any, any> => {
    return {
        isEditable: true,
        contentType: GenerationTextContentType.JOBS_COVERLETTER,
        contentTypeDetails: StaticGenerateContentListContent.jobsCoverLetter,
        type: GenerationType.TEXT,
        creditsCost: 1,
        inputUserData: {
            inputFields: {
                generationTitle: { // SHOULD ALWAYS HAVE THE GENERATION TITLE
                    id: "generationTitle",
                    formQuestionLabel: "Title of the content: ",
                    formHint: "e.g: CV to apply for a Google Position",
                    formType: "text",
                    isRequired: true,
                    formUserAnswer: "",
                },
                letterIntro: {
                    id: "letterIntro",
                    formQuestionLabel: "Provide some information about yourself and the position you are applying for: ",
                    formType: "textarea",
                    formHint: "Max. 1000 characters",
                    isRequired: true,
                },
                skills: {
                    id: "skills",
                    formQuestionLabel: "What are your top 3 skills relevant to the position?",
                    formType: "tags",
                    formHint: "eg: Leadership; Communication; Time Management;",
                    formValue: [],
                    isRequired: true,
                },
                whyYou: {
                    id: "whyYou",
                    formQuestionLabel: "Why are you the perfect candidate for this position?",
                    formType: "textarea",
                    formHint: "Max. 2000 characters",
                    isRequired: true,
                },
                whyCompany: {
                    id: "whyCompany",
                    formQuestionLabel: "Why do you want to work for this company?",
                    formType: "textarea",
                    formHint: "Max. 2000 characters",
                    isRequired: true,
                },
                contact: {
                    id: "contact",
                    formQuestionLabel: "Provide your contact information:",
                    formType: "text",
                    formHint: "Email or phone number",
                    isRequired: true,
                },

            },
            toneStyle: GenerationToneStyle.PROFESSIONAL,
            pointOfView: GenerationPointOfView.FIRST_PERSON,
            outputLanguage: "English (UK)",
            submitBtnText: "Write the Cover Letter",
            outputContentType: "HTML",
            outputFormat: "You are an assistant that helps the user to write a cover letter based on the answer to the following questions." + HTML_OUTPUTFORMAT,
        } as GenerateContentBaseInputFormModel,
    }
}

/** JOBS - Resignation Letter*/
export const initJobsPositionDescriptionFactory = (): GeneratedContentBaseModel<any, any> => {
    return {
        isEditable: true,
        contentType: GenerationTextContentType.JOBS_POSITION_DESCRIPTION,
        contentTypeDetails: StaticGenerateContentListContent.jobsCoverLetter,
        type: GenerationType.TEXT,
        creditsCost: 1,
        inputUserData: {
            inputFields: {
                generationTitle: { // SHOULD ALWAYS HAVE THE GENERATION TITLE
                    id: "generationTitle",
                    formQuestionLabel: "Title of the content: ",
                    formHint: "e.g: Title of the job position",
                    formType: "text",
                    isRequired: true,
                    formUserAnswer: "",
                },
                jobPositionDetails: {
                    id: "jobPositionDetails",
                    formQuestionLabel: "Describe in details the job position: ",
                    formType: "textarea",
                    formHint: "e.g: technologies; experience; skills;",
                    isRequired: true,
                },
                companyDescription: {
                    id: "companyDescription",
                    formQuestionLabel: "Describe your company: ",
                    formType: "textarea",
                    formHint: "eg: Company name; Industry; Vision and Missions of the company; Website;",
                    isRequired: true,
                },
            },
            toneStyle: GenerationToneStyle.PROFESSIONAL,
            pointOfView: GenerationPointOfView.THIRD_PERSON,
            outputLanguage: "English (UK)",
            submitBtnText: "Write the Job Description",
            outputContentType: "HTML",
            outputFormat: "You are an assistant that helps the user to write a job offer description." + HTML_OUTPUTFORMAT,
        } as GenerateContentBaseInputFormModel,
    }
}

/** Write a Document*/
export const initWriteAnyDocumentFactory = (): GeneratedContentBaseModel<any, any> => {
    return {
        isEditable: true,
        contentType: GenerationTextContentType.WRITE_DOCUMENT_ASSIST,
        contentTypeDetails: StaticGenerateContentListContent.writeDocument,
        type: GenerationType.TEXT,
        creditsCost: 1,
        inputUserData: {
            inputFields: {
                generationTitle: { // SHOULD ALWAYS HAVE THE GENERATION TITLE
                    id: "generationTitle",
                    formQuestionLabel: "Title of the Document: ",
                    formHint: "e.g: Article about How to earn money online",
                    formType: "text",
                    isRequired: true,
                    formUserAnswer: "",
                },
                promptText: {
                    id: "promptText",
                    formQuestionLabel: "Description of the document to be written: ",
                    formType: "textarea",
                    formHint: "e.g: Write an article about ways of making money online without any investment.",
                    isRequired: true,
                },
            },
            toneStyle: GenerationToneStyle.PROFESSIONAL,
            pointOfView: GenerationPointOfView.SECOND_PERSON,
            outputLanguage: "English (UK)",
            submitBtnText: "Write the Document",
            outputContentType: "HTML",
            outputFormat: "You are an assistant that helps the user to write any type of document based on the description written by the user. " + HTML_OUTPUTFORMAT,
        } as GenerateContentBaseInputFormModel,
    }
}

/** Social Publication Content*/
export const initSocialWriteAPublicationFactory = (): GeneratedContentBaseModel<any, any> => {
    return {
        isEditable: true,
        contentType: GenerationTextContentType.SOCIAL_WRITE_A_PUBLICATION,
        contentTypeDetails: StaticGenerateContentListContent.socialWritePublication,
        type: GenerationType.TEXT,
        creditsCost: 1,
        inputUserData: {
            inputFields: {
                generationTitle: { // SHOULD ALWAYS HAVE THE GENERATION TITLE
                    id: "generationTitle",
                    formQuestionLabel: "Title of the content: ",
                    formHint: "e.g: Article about how to make money online",
                    formType: "text",
                    isRequired: true,
                    formUserAnswer: "",
                },
                prompt: {
                    id: "prompt",
                    formQuestionLabel: "Description of the publication: ",
                    formType: "textarea",
                    formHint: "e.g: I'm publishing a gallery of photos of my trip to Lisbon.",
                    isRequired: true,
                },
                socialNetwork: {
                    id: "socialNetwork",
                    formQuestionLabel: "Social network to publish the summarized content: ",
                    formType: "dropdown",
                    options: ["Instagram", "LinkedIn", "TikTok", "Twitter", "Facebook", "Snapchat"],
                    formHint: "Select the target social network",
                    isRequired: true,
                },
                hashTags: {
                    id: "hashTags",
                    formQuestionLabel: "Hashtags of the publication: ",
                    formType: "tags",
                    formHint: "e.g: money; business;",
                    formValue: [],
                    isRequired: false,
                },
            },
            toneStyle: GenerationToneStyle.PROFESSIONAL,
            pointOfView: GenerationPointOfView.SECOND_PERSON,
            outputLanguage: "English (UK)",
            submitBtnText: "Write the Publication",
            outputContentType: "HTML",
            outputFormat: "You are an assistant that helps the user to write the description of a publication to the selected social networks. Include and add hashtags to the publication. " + HTML_OUTPUTFORMAT,
        } as GenerateContentBaseInputFormModel,
    }
}

/** Summarize Content*/
export const initSocialSummarizeContentFactory = (): GeneratedContentBaseModel<any, any> => {
    return {
        isEditable: true,
        contentType: GenerationTextContentType.SOCIAL_SUMMARIZE_CONTENT_PUBLICATION,
        contentTypeDetails: StaticGenerateContentListContent.socialSummarizeContent,
        type: GenerationType.TEXT,
        creditsCost: 1,
        inputUserData: {
            inputFields: {
                generationTitle: { // SHOULD ALWAYS HAVE THE GENERATION TITLE
                    id: "generationTitle",
                    formQuestionLabel: "Title of the content: ",
                    formHint: "e.g: Article about how to make money online",
                    formType: "text",
                    isRequired: true,
                    formUserAnswer: "",
                },
                originalContent: {
                    id: "originalContent",
                    formQuestionLabel: "Original content to summarize: ",
                    formType: "textarea",
                    formHint: "e.g: Copy/Paste the original content to be summarized for your publication",
                    isRequired: true,
                },
                socialNetwork: {
                    id: "socialNetwork",
                    formQuestionLabel: "Social network to publish the summarized content: ",
                    formType: "dropdown",
                    options: ["Instagram", "LinkedIn", "TikTok", "Twitter", "Facebook", "Snapchat"],
                    formHint: "Select the target social network",
                    isRequired: true,
                },
                hashTags: {
                    id: "hashTags",
                    formQuestionLabel: "Hashtags of the publication: ",
                    formType: "tags",
                    formHint: "e.g: money; business;",
                    formValue: [],
                    isRequired: false,
                },
            },
            toneStyle: GenerationToneStyle.PROFESSIONAL,
            pointOfView: GenerationPointOfView.SECOND_PERSON,
            outputLanguage: "English (UK)",
            submitBtnText: "Write the summary",
            outputContentType: "HTML",
            outputFormat: "You are an assistant that helps the user to write a short summary of a content to then be published to the selected social networks. Include and add hashtags (and create new hashtags) to the publication. " + HTML_OUTPUTFORMAT,
        } as GenerateContentBaseInputFormModel,
    }
}

const HTML_OUTPUTFORMAT = " Write the response exclusively in HTML format and don't write any other chat message besides the expected generated content."