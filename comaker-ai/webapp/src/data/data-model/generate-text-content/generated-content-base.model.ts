import {BaseModel} from "../../../components/react-shared-module/logic/shared-data/datamodel/base.model";
import {StaticGenerateContentTypeModel} from "../../static-data/static-data-model/static-generate-content-type.model";

export enum GenerationType {TEXT = "TEXT", IMAGE = "IMAGE"}

export type OutputGeneratedContentType = "HTML";

export interface GenerateBaseModel extends BaseModel{
    type?: GenerationType;
}

export enum GenerationTextContentType {
    BLOG_POST = "BLOG_POST",
    BLOG_TITLE_IDEAS = "BLOG_TITLE_IDEAS",
    BLOG_PARAGRAPH_WRITER = "BLOG_PARAGRAPH",
    BLOG_REWRITE_POST_LINK = "BLOG_REWRITE_POST_LINK",
    BLOG_REWRITE_POST_CONTENT = "BLOG_REWRITE_POST_CONTENT",

    /** JOBS */
    JOBS_CV_WRITING = "JOBS_CV_WRITING",
    JOBS_COVERLETTER = "JOBS_COVERLETTER",
    JOBS_RESIGNATIONLETTER = "JOBS_RESIGNATIONLETTER",
    JOBS_POSITION_DESCRIPTION = "JOBS_POSITION_DESCRIPTION",

    SOCIAL_WRITE_A_PUBLICATION = "SOCIAL_WRITE_A_PUBLICATION",
    SOCIAL_SUMMARIZE_CONTENT_PUBLICATION = "SOCIAL_SUMMARIZE_CONTENT_PUBLICATION",

    WRITE_DOCUMENT_ASSIST = "WRITE_DOCUMENT_ASSIST",
}

export enum GenerationToneStyle {
    ENTHUSIASTIC = "ENTHUSIASTIC",
    HUMOROUS = "HUMOROUS",
    PROFESSIONAL = "PROFESSIONAL",
    CONVERSATIONAL = "CONVERSATIONAL",
    INSPIRATIONAL = "INSPIRATIONAL"
}

export enum GenerationPointOfView {
    FIRST_PERSON = "FIRST_PERSON",
    SECOND_PERSON = "SECOND_PERSON",
    THIRD_PERSON = "THIRD_PERSON"
}


export interface GeneratedContentBaseModel<InputUserFormData extends GenerateContentBaseInputFormModel, AIOutputGeneratedData extends GenerateContentAIOutputBaseModel> extends BaseModel, GenerateBaseModel {
    userId?: string;
    inputUserData: InputUserFormData;
    listItemLabel?: string;
    prompt?: any;
    aiResponseData?: any;
    aiOutputGeneratedData?: AIOutputGeneratedData;
    outputEditableData?: AIOutputGeneratedData;
    contentType: GenerationTextContentType;
    creditsCost: number;
    isEditable?: boolean;
    contentTypeDetails: StaticGenerateContentTypeModel;
}


export interface GenerateContentBaseInputFormModel {
    inputFields: { [fieldId: string]: GenerateContentInputFormFieldModel }
    toneStyle?: GenerationToneStyle;
    pointOfView?: GenerationPointOfView;
    outputLanguage: string;
    outputFormat: string;
    outputContentType?: OutputGeneratedContentType; // By default consider HTML
    submitBtnText: string;
}

export interface GenerateContentInputFormFieldModel {
    id: string;
    formQuestionLabel: string; // The label to be displayed
    formUserAnswer?: string;
    formType: "text" | "textarea" | "dropdown" | "tags";
    formHint: string;
    options?: string[];
    formValue?: any; // to be used in cases of radiobtn, tags, etc...
    isRequired?: boolean;
}

export interface GenerateContentAIOutputBaseModel {
    body: any;
    outputContentType?: OutputGeneratedContentType;
}

