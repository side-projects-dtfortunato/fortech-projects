import {
    SharedCentralUserDataModel
} from "../../../components/react-shared-module/logic/shared-data/datamodel/shared-central-user-data.model";
import {UserSubscriptionPlanModel} from "../subscription/UserSubscriptionPlan.model";

export interface ComakerCentralUserDataModel extends SharedCentralUserDataModel {
    subscriptionPlan?: UserSubscriptionPlanModel;
    creatorRole?: string;
}

export  const CreatorRoles = [
    "Copywriter",
    "Agency",
    "ECommerceOwner",
    "Marketer",
    "Manager",
    "Others",
]