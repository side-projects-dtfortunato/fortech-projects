import {
    GenerateContentBaseInputFormModel,
    GeneratedContentBaseModel,
    GenerationTextContentType
} from "../data-model/generate-text-content/generated-content-base.model";
import {
    initBlogParagraphFactory,
    initBlogPostModel,
    initBlogRewriteContentFactory,
    initBlogRewriteLinkFactory,
    initBlogTitleIdeas,
    initJobsCoverLetterFactory,
    initJobsCVResumeFactory,
    initJobsPositionDescriptionFactory,
    initSocialSummarizeContentFactory,
    initSocialWriteAPublicationFactory,
    initWriteAnyDocumentFactory
} from "../data-model/generate-text-content/generate-forms-factory/generate-blog-content.factories";

export class GeneratedContentUtils {


    static validateFormInput(formData: GenerateContentBaseInputFormModel) {
        let isValid: boolean = true;

        Object.values(formData.inputFields).forEach((item) => {
            if (item.isRequired && (!item.formUserAnswer || item.formUserAnswer.length < 4) && !item.formValue) {
                isValid = false;
            }
        });

        return isValid;
    }

    static generateContentBaseDataByType(type: GenerationTextContentType, generatedContent?: GeneratedContentBaseModel<any, any>): GeneratedContentBaseModel<any, any> | undefined {
        if (generatedContent) {
            return generatedContent;
        }

        switch (type) {
            case GenerationTextContentType.BLOG_POST:
                return initBlogPostModel();
            case GenerationTextContentType.BLOG_TITLE_IDEAS:
                return initBlogTitleIdeas();
            case GenerationTextContentType.BLOG_PARAGRAPH_WRITER:
                return initBlogParagraphFactory();
            case GenerationTextContentType.BLOG_REWRITE_POST_LINK:
                return initBlogRewriteLinkFactory();
            case GenerationTextContentType.BLOG_REWRITE_POST_CONTENT:
                return initBlogRewriteContentFactory();
            case GenerationTextContentType.JOBS_CV_WRITING:
                return initJobsCVResumeFactory();
            case GenerationTextContentType.JOBS_COVERLETTER:
                return initJobsCoverLetterFactory();
            case GenerationTextContentType.JOBS_POSITION_DESCRIPTION:
                return initJobsPositionDescriptionFactory();
            case GenerationTextContentType.WRITE_DOCUMENT_ASSIST:
                return initWriteAnyDocumentFactory();
            case GenerationTextContentType.SOCIAL_SUMMARIZE_CONTENT_PUBLICATION:
                return initSocialSummarizeContentFactory();
            case GenerationTextContentType.SOCIAL_WRITE_A_PUBLICATION:
                return initSocialWriteAPublicationFactory();
        }

        return undefined;
    }
}

export const GeneratePresetsInputFormsFactory = {
    blogKeywords: {
        id: "keywords",
        formQuestionLabel: "What are some relevant keywords for your blog post?",
        formType: "tags",
        formHint: "Enter relevant keywords. [Press enter to add new keywords]",
        isRequired: true,
        formValue: [],
    },
    blogType: {
        id: "blogType",
        formQuestionLabel: "What type of blog post do you want to write?",
        formType: "dropdown",
        formHint: "Select the type of blog post you want to create",
        options: ["List post", "How-to post", "Thought leadership post", "Review post"],
        isRequired: true,
    },
    targetAudience: {
        id: "targetAudience",
        formQuestionLabel: "Who is your target audience?",
        formType: "text",
        formHint: "Enter your target audience",
        isRequired: true,
    },
}