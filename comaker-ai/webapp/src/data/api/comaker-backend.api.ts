import {
    callFunction,
    getAPIDocumentSubCol
} from "../../components/react-shared-module/logic/shared-data/firebase.utils";
import {
    GenerateBaseModel,
    GeneratedContentBaseModel,
    GenerationType
} from "../data-model/generate-text-content/generated-content-base.model";
import {CoMakerFirestoreCollectionDB, CoMakerFirestoreDocsDB} from "../data-model/comaker-firestore-collections";
import {
    QueryPaginationDataModel
} from "../../components/react-shared-module/logic/shared-data/datamodel/query-pagination-data.model";
import firebase from 'firebase/compat/app';
import 'firebase/compat/firestore';
import {UserSubscriptionPlanType} from "../data-model/subscription/UserSubscriptionPlan.model";
import RootConfigs from "../../configs";
import {GenerateImageBasePromptModel} from "../data-model/generate-image/generate-image-base-prompt.model";
import {ApiResponse} from "../../components/react-shared-module/logic/shared-data/datamodel/base.model";
import CollectionReference = firebase.firestore.CollectionReference;


export class ComakerBackendApi {

    static async generateAIWritingContent(props: { generateContentData: GeneratedContentBaseModel<any, any>}): Promise<ApiResponse<any>> {
        return new Promise((resolve, reject) => {
            callFunction("generateAIWritingContent", props).then((res) => {
                resolve(res);
            }).catch((err) => {
                resolve({
                    isSuccess: false,
                    responseCode: 1000,
                    responseData: err,
                    message: "Something failed while generating the content. Please, try again...",
                } as ApiResponse<any>);
            });
        });
    }

    static async updateGeneratedContent(props: { generateContentData: GeneratedContentBaseModel<any, any>, updatePrompt: string, updateCreditsCost: number }) {
        return await callFunction("updateGeneratedContent", props);
    }

    static async openAIGenerateImage(props: { generateImagePrompt: GenerateImageBasePromptModel, env: "PROD" | "DEV"}) {
        return await callFunction("openAIGenerateImage", props);
    }

    /**
     *
     * @param props
     * @deprecated
     */
    static async completeGeneratedImageContent(props: { generatedImage: GenerateImageBasePromptModel, env: "PROD" | "DEV"}) {
        return await callFunction("completeGeneratedImageContent", props);
    }

    static async saveGeneratedContentChanges(props: { generateContentData: GeneratedContentBaseModel<any, any>}) {
        return await callFunction("saveGeneratedContentChanges", props);
    }

    static async getGeneratedItemDetails(props: {userId: string, generatedItemId: string}) {

        return getAPIDocumentSubCol([CoMakerFirestoreCollectionDB.UserProjectsDashboardCollection,
            CoMakerFirestoreDocsDB.UserProjectsDashboard.GeneratedContentItemsCollection], [props.userId, props.generatedItemId]);
    }

    static async getGeneratedImageDetails(props: {userId: string, itemId: string}) {

        return getAPIDocumentSubCol([CoMakerFirestoreCollectionDB.UserProjectsDashboardCollection,
            CoMakerFirestoreDocsDB.UserProjectsDashboard.GeneratedImagesCollection], [props.userId, props.itemId]);
    }

    static async getListGeneratedContent(props: {userId: string, listType: GenerationType}): Promise<GenerateBaseModel[]> {
        let aggDocId: string = props.listType === GenerationType.IMAGE ? CoMakerFirestoreDocsDB.UserProjectsDashboard.GeneratedImagesCollection : CoMakerFirestoreDocsDB.UserProjectsDashboard.GeneratedContentItemsCollection;
        let apiResponse = await getAPIDocumentSubCol([CoMakerFirestoreCollectionDB.UserProjectsDashboardCollection, CoMakerFirestoreDocsDB.UserProjectsDashboard.AggregatedDocsCollection], [props.userId, aggDocId]);
        let listItems: GenerateBaseModel[] = [];

        if (apiResponse && Object.keys(apiResponse).length > 0) {
            Object.values(apiResponse).forEach((item) => {
                listItems.push(item);
            });
        }

        return listItems;
    }

    static async deleteGeneratedItem(props: {itemId: string}) {
        return await callFunction("deleteGeneratedItem", props);
    }

    static async buyMoreCredits(props: {credits: number}) {
        return await callFunction("buyMoreCredits", props);
    }

    static async stripeCreateCheckoutSession(props: {planType: UserSubscriptionPlanType}) {
        const env: "PROD" | "DEV" = RootConfigs.BACKEND_ENV;
        return await callFunction("stripeCreateCheckoutSession", {
            ...props,
            env,
        });
    }

    static async updateSubscriptionPlan(props: {planType: UserSubscriptionPlanType}) {
        const env: "PROD" | "DEV" = RootConfigs.BACKEND_ENV;
        return await callFunction("updateSubscriptionPlan", {
            ...props,
            env,
        });
    }

    static async stripeCreateCustomerPortalSession() {
        const env: "PROD" | "DEV" = RootConfigs.BACKEND_ENV;
        return await callFunction("stripeCreateCustomerPortalSession", {
            env,
        });
    }


}

/** EXAMPLE: Pagination
 * static async getListGeneratedImagesPagination(props: {userId: string,
    queryDataPagination: QueryPaginationDataModel<GenerateImageBasePromptModel>}) {

    let collectionRef: CollectionReference = firebase.firestore()
        .collection(CoMakerFirestoreCollectionDB.UserProjectsDashboardCollection)
        .doc(props.userId)
        .collection(CoMakerFirestoreDocsDB.UserProjectsDashboard.GeneratedImagesCollection);

    collectionRef.orderBy('updatedAt', 'desc')
        .limit(props.queryDataPagination.itemsPerPage);


    if (props.queryDataPagination.lastDoc) {
        collectionRef.startAfter(props.queryDataPagination.lastDoc);
    }

    const response = await collectionRef.get({source: "default"});

    if (!props.queryDataPagination.listItems) {
        props.queryDataPagination.listItems = [];
    }
    if (response && response.docs && !response.empty) {
        // It's possible that has more items
        props.queryDataPagination.hasMorePages = response.size === props.queryDataPagination.itemsPerPage;

        props.queryDataPagination.listItems = props.queryDataPagination.listItems.concat(response.docs.map<GenerateImageBasePromptModel>((doc) => {
            return {
                ...doc.data(),
            } as GenerateImageBasePromptModel;
        }));
        props.queryDataPagination.lastDoc = response.docs[response.docs.length - 1];
    } else {
        props.queryDataPagination.hasMorePages = false;
    }

    // Sort them
    props.queryDataPagination.listItems = props.queryDataPagination.listItems.sort((i1, i2) => {
        return i2.updatedAt! - i1.updatedAt!;
    });

    return props.queryDataPagination;
}*/