import {
    PricingTablePlanContentModel
} from "../../../components/react-shared-module/ui-components/pricing/PricingTable.component";
import {SubscriptionPlansStaticData} from "../../data-model/subscription/UserSubscriptionPlan.model";

export const CreditLabel = "AI Credits"

export const StaticPricingPlansContent = {
    freePlan: {
        planName: SubscriptionPlansStaticData.free.label,
        planDescription: "Get started with CoMaker.ai at no cost! Our Free plan provides access to essential features, making it perfect for individual users and small projects.",
        priceLabel: SubscriptionPlansStaticData.free.price,
        listFeatures: [
            {
                label: SubscriptionPlansStaticData.free.credits + " " + CreditLabel + " included per month",
                type: "INCLUDED",
            },
            {
                label: "All creator tools available",
                type: "INCLUDED",
            },
            {
                label: "Generate content in multiple languages",
                type: "INCLUDED",
            },

        ]
    } as PricingTablePlanContentModel,
    hobbyPlan: {
        planName: SubscriptionPlansStaticData.hobby.label,
        planDescription: "Unlock the full potential of CoMaker.ai with our Hobby plan. Ideal for enthusiasts and growing projects, this plan offers advanced features and increased limits to help you scale your creations.",
        priceLabel: SubscriptionPlansStaticData.hobby.price,
        listFeatures: [
            {
                label: SubscriptionPlansStaticData.hobby.credits + " " + CreditLabel + " included per month",
                type: "INCLUDED",
            },
            {
                label: "All creator tools available",
                type: "INCLUDED",
            },
            {
                label: "Generate content in multiple languages",
                type: "INCLUDED",
            },
            {
                label: "Ideal for enthusiasts & growing projects",
                type: "INCLUDED",
            },
        ]
    } as PricingTablePlanContentModel,
    proPlan: {
        planName: SubscriptionPlansStaticData.pro.label,
        planDescription: "Empower your business with the CoMaker.ai Pro plan. Designed for professionals and enterprises, this plan delivers premium features, top-tier performance, and priority support to help you drive growth and innovation.",
        priceLabel: SubscriptionPlansStaticData.pro.price,
        listFeatures: [
            {
                label: SubscriptionPlansStaticData.pro.credits + " " + CreditLabel + " included per month",
                type: "INCLUDED",
            },
            {
                label: "All creator tools available",
                type: "INCLUDED",
            },
            {
                label: "Generate content in multiple languages",
                type: "INCLUDED",
            },
            {
                label: "Premium features & top-tier performance",
                type: "INCLUDED",
            },
            {
                label: "Ideal for professionals & enterprises",
                type: "INCLUDED",
            },
            {
                label: "Priority support",
                type: "INCLUDED",
            },
        ]
    } as PricingTablePlanContentModel,
}