import {SvgIconsUtils} from "../../../components/ui-helper/SvgIcons.utils";

export const StaticContentCategoriesIconsMap = {
    writeDocument: {
        icon: SvgIconsUtils.menu_generate_content,
    },
    blogposts: {
        icon: SvgIconsUtils.content_category_blog,
    },
    imageGenerator: {
        icon: SvgIconsUtils.icon_add_image,
    },
    social_networks: {
        icon: SvgIconsUtils.icon_social_networks,
    },
    jobs: {
        icon: SvgIconsUtils.icon_work,
    },
}

export function getContentCategoryIcon(category: string) {
    // @ts-ignore
    if (Object.keys(StaticContentCategoriesIconsMap).includes(category) && StaticContentCategoriesIconsMap[category] && StaticContentCategoriesIconsMap[category].icon) {
        // @ts-ignore
        return StaticContentCategoriesIconsMap[category].icon;
    }

    // Default
    return SvgIconsUtils.menu_generate_content;
}