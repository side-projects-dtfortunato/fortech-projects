import {StaticCategoryContentModel} from "../static-data-model/static-category-content.model";
import {
    GenerationTextContentType,
    GenerationType
} from "../../data-model/generate-text-content/generated-content-base.model";
import {
    GeneratedImageSize,
    GenerateImageBasePromptModel,
    GenerationImageContentType, GenerationStatus, InputCreateImageType
} from "../../data-model/generate-image/generate-image-base-prompt.model";
import {OpenAIManager} from "../../../managers/open-ai/OpenAI.manager";
import {StaticGenerateContentTypeModel} from "../static-data-model/static-generate-content-type.model";


export function initGenerateImageBaseData(contentType: GenerationImageContentType): GenerateImageBasePromptModel {
    return {
        aiInput: {
            promptText: "",
            imageSize: GeneratedImageSize.SMALL,
            generationType: InputCreateImageType.CREATE_IMAGE,
            numberResults: 2,
        },
        creditsCost: OpenAIManager.calculateImageGenerationCost(GeneratedImageSize.LARGE, 2),
        status: GenerationStatus.REQUESTING,
        generationType: contentType,
        type: GenerationType.IMAGE,
    };
}

export const StaticGenerateContentCategoriesList: {[categoryId: string]: StaticCategoryContentModel} = {
    writeDocument: {
        uid: "writeDocument",
        generationType: GenerationType.TEXT,
        categoryLabel: "Write a Document",
        description: "Write a full document with the help of AI.",
        accentColor: "#1C7ED6",
        displayMenu: false,
    },
    blogposts: {
        uid: "blogposts",
        generationType: GenerationType.TEXT,
        categoryLabel: "Blogging",
        description: "Create blog posts with AI-powered templates and natural language processing technology.",
        accentColor: "#46D29A",
    },
    imageGenerator: {
        uid: "imageGenerator",
        generationType: GenerationType.IMAGE,
        categoryLabel: "Generate Images",
        description: "Generate images targeting your requests",
        accentColor: "#D02872",
        displayMenu: false,
    },
    jobs: {
        uid: "jobs",
        generationType: GenerationType.TEXT,
        categoryLabel: "Work/Jobs",
        description: "Generate professional content such as emails, letters, CVs, etc...",
        accentColor: "#ffd166",
        displayMenu: true,
    },
    social_networks: {
        uid: "social_networks",
        generationType: GenerationType.TEXT,
        categoryLabel: "Social Networks",
        description: "Automatize the content to publish on your social networks",
        accentColor: "#D02872",
        displayMenu: true,
    },
    /*marketing: {
        uid: "marketing",
        categoryLabel: "Marketing/Product",
        description: "Easily promote your products using our guided workflow",
        accentColor: "#FC8534",
    },
    website: {
        uid: "website",
        categoryLabel: "Website/SEO",
        description: "Generate content to use directly on your website",
        accentColor: "#1C7ED6",
    },*/
};

export const StaticGenerateContentListContent = {
    /** WRITE A DOCUMENT */
    writeDocument: {
        uid: GenerationTextContentType.WRITE_DOCUMENT_ASSIST,
        category: StaticGenerateContentCategoriesList.writeDocument,
        description: "Write any type of document, email, article, publication with the assistance of CoMaker.ai",
        label: "Write a Document",
        popular: true,
    } as StaticGenerateContentTypeModel,
    createImage: {
        uid: GenerationImageContentType.CREATE_IMAGE,
        category: StaticGenerateContentCategoriesList.imageGenerator,
        description: "Create an image based on your requirements",
        label: "Create a unique image",
        tags: ["Images", "Create an image"],
        popular: false,
    } as StaticGenerateContentTypeModel,
    /** Blog Posts Content */
    blogPost: {
        uid: GenerationTextContentType.BLOG_POST,
        category: StaticGenerateContentCategoriesList.blogposts,
        description: "Generate a blog post easily by using our guided workflow",
        label: "Generate a blog post",
        tags: ["blog", "article", "seo", "content"],
        popular: true,
    } as StaticGenerateContentTypeModel,
    blogTitles: {
        uid: GenerationTextContentType.BLOG_TITLE_IDEAS,
        category: StaticGenerateContentCategoriesList.blogposts,
        description: "Generate title ideas for a blog post",
        label: "Write Blog Title Ideas",
        tags: ["blog", "article", "seo", "ideas", "titles"],
        popular: true,
    } as StaticGenerateContentTypeModel,
    blogParagraph: {
        uid: GenerationTextContentType.BLOG_PARAGRAPH_WRITER,
        category: StaticGenerateContentCategoriesList.blogposts,
        description: "Generate a paragraph to include to your work in progress post",
        label: "Write Blog Post Paragraph",
        tags: ["blog", "article", "seo", "content", "paragraph"],
        popular: true,
    } as StaticGenerateContentTypeModel,
    blogRewriteLink: {
        uid: GenerationTextContentType.BLOG_REWRITE_POST_LINK,
        category: StaticGenerateContentCategoriesList.blogposts,
        description: "Rewrite the entire content of a blog post (from a URL)",
        label: "Rewrite a Blog Post Link",
        tags: ["blog", "article", "seo", "content", "rewrite"],
        popular: false,
    } as StaticGenerateContentTypeModel,
    blogRewriteContent: {
        uid: GenerationTextContentType.BLOG_REWRITE_POST_CONTENT,
        category: StaticGenerateContentCategoriesList.blogposts,
        description: "Rewrite a blog post content",
        label: "Rewrite a Blog Post Content",
        tags: ["blog", "article", "seo", "content", "rewrite"],
        popular: false,
    } as StaticGenerateContentTypeModel,

    /** Work/Jobs */
    jobsResumeCvWriting: {
        uid: GenerationTextContentType.JOBS_CV_WRITING,
        category: StaticGenerateContentCategoriesList.jobs,
        description: "Get a professional resume or CV written with the help of AI.",
        label: "Resume/CV Writing",
        popular: false,
    } as StaticGenerateContentTypeModel,
    jobsCoverLetter: {
        uid: GenerationTextContentType.JOBS_CV_WRITING,
        category: StaticGenerateContentCategoriesList.jobs,
        description: "Create a compelling cover letter that stands out from the crowd.",
        label: "Cover Letter Writing",
        popular: false,
    } as StaticGenerateContentTypeModel,
    jobsPositionDescription: {
        uid: GenerationTextContentType.JOBS_POSITION_DESCRIPTION,
        category: StaticGenerateContentCategoriesList.jobs,
        description: "Create a detailed job offer description that attracts top talent.",
        label: "Job Offer Description",
        popular: false,
    } as StaticGenerateContentTypeModel,

    /** Social Networks */
    socialWritePublication: {
        uid: GenerationTextContentType.SOCIAL_WRITE_A_PUBLICATION,
        category: StaticGenerateContentCategoriesList.social_networks,
        description: "Generate the content of a publication to use directly into your social networks (Instagram, LinkedIn, TikTok, etc...)",
        label: "Publication Content",
        popular: false,
    } as StaticGenerateContentTypeModel,
    socialSummarizeContent: {
        uid: GenerationTextContentType.SOCIAL_SUMMARIZE_CONTENT_PUBLICATION,
        category: StaticGenerateContentCategoriesList.social_networks,
        description: "Summarize your content to be published on your social networks",
        label: "Summarize Content",
        popular: false,
    } as StaticGenerateContentTypeModel,
};

export const StaticPopularGenerateContentListContent = [
    StaticGenerateContentListContent.writeDocument,
    StaticGenerateContentListContent.blogPost,
    StaticGenerateContentListContent.createImage,
    StaticGenerateContentListContent.jobsCoverLetter,
]