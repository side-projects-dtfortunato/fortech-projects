import {
    GeneratedContentBaseModel,
    GenerationTextContentType
} from "../../../data/data-model/generate-text-content/generated-content-base.model";
import NotFoundContentComponent from "../../react-shared-module/ui-components/shared/NotFoundContent.component";
import {getLanguageLabel} from "../../react-shared-module/logic/language/language.helper";
import {useState} from "react";
import NotFoundSimpleMessageComponent
    from "../../react-shared-module/ui-components/shared/NotFoundSimpleMessage.component";
import GenerateBlogPostOutputComponent from "./shared/output-form/generate-blog-post-output.component";
import GenerateFormCustomInputUserComponent from "./shared/input-form/generate-form-custom-input-user.component";
import {GeneratedContentUtils} from "../../../data/utils/generate-content.utils";
import UpdateGeneratedContentFormComponent from "./shared/input-form/UpdateGeneratedContentForm.component";

export default function GenerateContentContainerComponent(props: {contentType: string | GenerationTextContentType, defaultData?: GeneratedContentBaseModel<any, any>}) {
    const [generateContentData, setGenerateContentData] = useState(GeneratedContentUtils.generateContentBaseDataByType(props.contentType as GenerationTextContentType, props.defaultData));

    function renderInputContainer() {
        if (generateContentData) {
            return (<GenerateFormCustomInputUserComponent generateContentForm={generateContentData} setGenerateContentData={setGenerateContentData} onGeneratedAIContent={onGeneratedAIContent} />)
        }
        return (<NotFoundContentComponent />);
    }

    function onGeneratedAIContent(generatedData: GeneratedContentBaseModel<any, any>) {
        setGenerateContentData(generatedData);
    }

    function renderGeneratorHeader() {
        return (
            <div className='flex flex-col gap-2 justify-center items-center my-5'>
                <h1 className='custom'>{generateContentData?.contentTypeDetails.label}</h1>
                <h4 className='custom'>{generateContentData?.contentTypeDetails.description}</h4>
            </div>
        )
    }

    function renderOutputContent() {
        if (hasOutput()) {
            let outputContent = <NotFoundSimpleMessageComponent message={getLanguageLabel("GENERATE_OUTPUT_EMPTY")}/>

            if (generateContentData && generateContentData.aiOutputGeneratedData && generateContentData.aiOutputGeneratedData.body) {
                switch (generateContentData.contentType) {
                    case GenerationTextContentType.BLOG_POST:
                    default:
                        outputContent = (<GenerateBlogPostOutputComponent generatedOutputData={generateContentData} setGenerateContent={setGenerateContentData}/>);
                        break;
                }
            }
            // className='flex list-item-bg prose justify-center items-center h-full w-full p-5'
            return (
                <div className='flex justify-stretch w-full'>
                    {outputContent}
                </div>
            )
        } else {
            return (<></>);
        }

    }

    function hasOutput() {
        return generateContentData?.aiOutputGeneratedData !== undefined;
    }

    function renderCollapsableInitialParams() {
        return (
            <div tabIndex={0} className="collapse collapse-arrow border border-base-300 bg-base-100 rounded-box w-full">
                <input type="checkbox" className="peer" />
                <div className="collapse-title text-xl font-bold">
                    Initial Params
                </div>
                <div className="collapse-content">
                    {renderInputContainer()}
                </div>
            </div>
        );
    }

    if (!hasOutput()) {
        return (
            <div className='flex flex-col p-3 w-full items-center'>

                {renderGeneratorHeader()}
                <div className='flex flex-col w-max-xl lg:flex-row gap-2 items-start list-item-bg drop-shadow w-full p-5'>
                    {renderInputContainer()}
                </div>
            </div>
        )
    } else {
        return (
            <div className='flex flex-col p-3 w-full items-center'>

                {renderGeneratorHeader()}
                <div className='flex flex-col gap-5 items-center drop-shadow w-full'>
                    <UpdateGeneratedContentFormComponent generatedContent={generateContentData!} onContentChanged={setGenerateContentData} creditsCost={1} />
                    {renderOutputContent()}
                    {renderCollapsableInitialParams()}
                </div>
            </div>
        )
    }

}