import {useState} from "react";
import {
    GenerateContentBaseInputFormModel,
    GenerateContentInputFormFieldModel,
    GeneratedContentBaseModel
} from "../../../../../data/data-model/generate-text-content/generated-content-base.model";
import {
    RootGlobalStateActions,
    useAlertMessageGlobalState
} from "../../../../react-shared-module/logic/global-hooks/root-global-state";
import {ComakerBackendApi} from "../../../../../data/api/comaker-backend.api";
import {APIResponseMessageUtils} from "../../../../react-shared-module/logic/global-hooks/APIResponseMessageUtils";
import InputTextComponent from "../../../../react-shared-module/ui-components/form/InputText.component";
import InputTextAreaComponent from "../../../../react-shared-module/ui-components/form/InputTextArea.component";
import DropdownFormComponent from "../../../../react-shared-module/ui-components/form/DropdownForm.component";
import CommonUtils from "../../../../react-shared-module/logic/commonutils";
import InputTagsFieldComponent from "../../../../react-shared-module/ui-components/form/InputTagsField.component";
import GenerateContentCommonBottomInputComponent from "./generate-content-common-bottom-input.component";
import SpinnerComponent from "../../../../react-shared-module/ui-components/shared/Spinner.component";
import {CreditLabel} from "../../../../../data/static-data/pricing-plans/static-pricing-plans.content";
import {GeneratedContentUtils} from "../../../../../data/utils/generate-content.utils";
import {useAuthState} from "react-firebase-hooks/auth";
import {auth} from "../../../../react-shared-module/logic/shared-data/firebase.utils";
import AuthManager from "../../../../react-shared-module/logic/auth/auth.manager";
import SignupLoginCallToActionContainerComponent
    from "../../../../react-shared-module/ui-components/shared/SignupLoginCallToActionContainer.component";
import {ApiResponse} from "../../../../react-shared-module/logic/shared-data/datamodel/base.model";
import GenerateFormButtonComponent from "./GenerateFormButton.component";


export default function GenerateFormCustomInputUserComponent(props: { generateContentForm: GeneratedContentBaseModel<any, any>,
    onGeneratedAIContent: (generateContentForm: GeneratedContentBaseModel<any, any>) => void,
    setGenerateContentData: (generateContent: GeneratedContentBaseModel<any, any>) => void}) {
    const [isLoading, setIsLoading] = useState(false);
    // const [generateContentData, setGenerateContentData] = useState<GeneratedContentBaseModel<any, any>>(props.generateContentForm);
    const [alertMessageState, setAlertMessage] = useAlertMessageGlobalState();
    const [currentUser, isAuthLoading] = useAuthState(auth);
    const generateContentData = props.generateContentForm;

    function isEditable() {
        return !isLoading && generateContentData.isEditable !== false;
    }

    function isFormValid() {
        return !isLoading
            && generateContentData.listItemLabel && generateContentData.listItemLabel.length > 3
            && !generateContentData.aiResponseData && GeneratedContentUtils.validateFormInput(generateContentData.inputUserData);
    }


    async function onWriteArticleClicked() {
        if (isFormValid()) {
            setIsLoading(true);
            props.setGenerateContentData({
                ...generateContentData,
                isEditable: false,
            });

            // Update Generate Content Data
            const response: ApiResponse<any> = await ComakerBackendApi.generateAIWritingContent({
                generateContentData: generateContentData
            });

            if (response.isSuccess) {
                props.setGenerateContentData(response.responseData);
                props.onGeneratedAIContent(response.responseData);
            } else {
                // Display error message
                RootGlobalStateActions.displayAlertMessage({
                    alertType: "ERROR",
                    message: APIResponseMessageUtils.getAPIResponseMessage(response),
                    setAlertMessageState: setAlertMessage,
                });
            }
            setIsLoading(false);
        }
    }

    function onUpdateFormData(formKey: string, data: any) {
        // Set the list item label
        if (formKey === "generationTitle") {
            generateContentData.listItemLabel = data;
        }

        // Update content form data
        if (Object.keys(generateContentData.inputUserData.inputFields).includes(formKey)) {
            // @ts-ignore
            generateContentData.inputUserData.inputFields[formKey].formUserAnswer = data.toString();
            // @ts-ignore
            generateContentData.inputUserData.inputFields[formKey].formValue = data;
        }

        props.setGenerateContentData({
            ...generateContentData,
        });
    }

    function renderCustomInput() {
        let listFormInputs: any[] = [];


        let inputFieldsKeys: string[] = Object.keys((generateContentData.inputUserData as GenerateContentBaseInputFormModel).inputFields);
        inputFieldsKeys.forEach((key: string) => {
            let item: GenerateContentInputFormFieldModel = (generateContentData.inputUserData as GenerateContentBaseInputFormModel).inputFields[key];

            if (item) {
                switch (item.formType) {
                    case "text":
                        listFormInputs.push(
                            <InputTextComponent disabled={!isEditable()} topLabel={item.formQuestionLabel}
                                                hint={item.formHint} maxLength={300}
                                                defaultValue={item.formUserAnswer}
                                                onChanged={(text) => onUpdateFormData(key, text)}/>
                        )
                        break;
                    case "textarea":
                        listFormInputs.push(
                            <InputTextAreaComponent disabled={!isEditable()} topLabel={item.formQuestionLabel}
                                                    hint={item.formHint} maxLength={2000}
                                                    defaultValue={item.formUserAnswer}
                                                    onChanged={(text) => onUpdateFormData(key, text)}/>
                        )
                        break;
                    case "dropdown":
                        if (item.options) {
                            listFormInputs.push(
                                <DropdownFormComponent disabled={!isEditable()} label={item.formQuestionLabel}
                                                       defaultLabel={item.formHint}
                                                       options={CommonUtils.convertArrayToDropdownOptions(item.options)}
                                                       currentSelectionKey={item.formUserAnswer}
                                                       onOptionSelected={(option) => onUpdateFormData(key, option)}/>
                            )

                        }
                        break;
                    case "tags":
                        listFormInputs.push(
                            <InputTagsFieldComponent disabled={!isEditable()} label={item.formQuestionLabel}
                                                     placeholder={item.formHint}
                                                     onTagsChanged={(tags) => onUpdateFormData(key, tags)}
                                                     defaultTags={item.formValue}/>
                        )
                        break;
                }
            }
        });

        return listFormInputs;
    }

    function renderGenerateButton() {
        return <GenerateFormButtonComponent disabled={!isFormValid()} creditsCost={generateContentData.creditsCost} label={generateContentData.inputUserData.submitBtnText} onClick={onWriteArticleClicked} />
        /*
        if (isAuthLoading) {
            return (<></>)
        } else if (AuthManager.isUserLogged()) {
            return (
                <div className='flex flex-col w-full justify-center items-center gap-1'>
                    <button className='btn btn-primary' disabled={!isFormValid()}
                            onClick={onWriteArticleClicked}>{generateContentData.inputUserData.submitBtnText} ({CreditLabel} {generateContentData.creditsCost})
                    </button>
                    <span className='subtitle'>({generateContentData.creditsCost} {CreditLabel} will be deducted to your available credits wallet)</span>
                </div>
            )
        } else {
            return (<SignupLoginCallToActionContainerComponent />);
        }*/
    }

    return (
        <div className='flex flex-col gap-4 w-full'>

            {renderCustomInput()}

            <GenerateContentCommonBottomInputComponent generateContentData={generateContentData}
                                                       setGenerateContentData={props.setGenerateContentData}/>

            {isLoading ? <SpinnerComponent message={"CoMaker is generating your content, please be patient (can take up to 5 minutes to finish)..."}/> : <></>}

            {renderGenerateButton()}
        </div>
    )
}
