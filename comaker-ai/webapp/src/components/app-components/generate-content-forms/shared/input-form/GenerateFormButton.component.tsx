import {useCentralUserDataState} from "../../../../react-shared-module/logic/global-hooks/root-global-state";
import {CreditLabel} from "../../../../../data/static-data/pricing-plans/static-pricing-plans.content";
import SubscriptionPlanButtonComponent from "../../../subscription/shared/SubscriptionPlanButton.component";
import Link from "next/link";
import {getPricingPageUrl} from "../../../../../pages/pricing";
import {
    UserSubscriptionPlanType,
    UserSubscriptionUtils
} from "../../../../../data/data-model/subscription/UserSubscriptionPlan.model";
import {ComakerCentralUserDataModel} from "../../../../../data/data-model/user-data/comaker-central-user-data.model";
import AuthManager from "../../../../react-shared-module/logic/auth/auth.manager";
import SignupLoginCallToActionContainerComponent
    from "../../../../react-shared-module/ui-components/shared/SignupLoginCallToActionContainer.component";
import NotEnoughCreditsButtonsComponent from "./NotEnoughCreditsButtons.component";

export default function GenerateFormButtonComponent(props:{creditsCost: number, label: string, disabled?: boolean, onClick: () => void}) {
    const [centralUserData, setCentralUserData] = useCentralUserDataState();
    let cmCentralUserData: ComakerCentralUserDataModel = centralUserData as ComakerCentralUserDataModel;

    function renderGenerateButton() {
        return (
            <div className='flex flex-col w-full justify-center items-center gap-1'>
                <button className='btn btn-primary' disabled={props.disabled}
                        onClick={props.onClick}>{props.label} ({CreditLabel} {props.creditsCost})
                </button>
                <span className='subtitle'>({props.creditsCost} {CreditLabel} will be deducted to your available credits wallet)</span>
            </div>
        )
    }

    function renderNotEnoughCredits() {
        return (<NotEnoughCreditsButtonsComponent />)
    }

    if (cmCentralUserData && cmCentralUserData.subscriptionPlan) {
        if (UserSubscriptionUtils.hasAvailableCredits(cmCentralUserData.subscriptionPlan, props.creditsCost)) {
            return renderGenerateButton();
        } else {
            return renderNotEnoughCredits();
        }
    } else if (!AuthManager.isUserLogged()) {
        return (<SignupLoginCallToActionContainerComponent />);
    } else {
        return (<></>)
    }
}