import {
    GeneratedContentBaseModel
} from "../../../../../data/data-model/generate-text-content/generated-content-base.model";
import {
    RootGlobalStateActions,
    useAlertMessageGlobalState,
    useCentralUserDataState,
    useIsLoadingGlobalState
} from "../../../../react-shared-module/logic/global-hooks/root-global-state";
import InputTextAreaComponent from "../../../../react-shared-module/ui-components/form/InputTextArea.component";
import {useState} from "react";
import {ComakerCentralUserDataModel} from "../../../../../data/data-model/user-data/comaker-central-user-data.model";
import {UserSubscriptionUtils} from "../../../../../data/data-model/subscription/UserSubscriptionPlan.model";
import NotEnoughCreditsButtonsComponent from "./NotEnoughCreditsButtons.component";
import {CreditLabel} from "../../../../../data/static-data/pricing-plans/static-pricing-plans.content";
import Image from "next/image";
import {ComakerBackendApi} from "../../../../../data/api/comaker-backend.api";

export default function UpdateGeneratedContentFormComponent(props: { generatedContent: GeneratedContentBaseModel<any, any>, onContentChanged: (updatedContent: GeneratedContentBaseModel<any, any>) => void, creditsCost: number }) {
    const [isLoading, setIsLoading] = useIsLoadingGlobalState();
    const [formPromptValue, setFormPromptValue] = useState("");
    const [centralUserData, setCentralUserData] = useCentralUserDataState();
    let cmCentralUserData: ComakerCentralUserDataModel = centralUserData as ComakerCentralUserDataModel;
    const [alertMessage, setAlertMessage] = useAlertMessageGlobalState();

    function onPromptChanged(text: string) {
        setFormPromptValue(text);
    }

    function isFormValid() {
        return formPromptValue.length > 4;
    }

    function onAskAIUpdate() {
        console.log(formPromptValue);

        setIsLoading(true);
        ComakerBackendApi.updateGeneratedContent({
            generateContentData: props.generatedContent,
            updatePrompt: formPromptValue,
            updateCreditsCost: props.creditsCost,
        }).then((res) => {
            if (res.isSuccess) {
                props.onContentChanged(res.responseData as GeneratedContentBaseModel<any, any>);
                RootGlobalStateActions.displayAlertMessage({
                    alertType: "SUCCESS",
                    message: "Your article was updated!",
                    setAlertMessageState: setAlertMessage,
                });
                setFormPromptValue("");
            } else {
                // Display error message
                RootGlobalStateActions.displayAPIResponseError({
                    apiResponse: res,
                    setAlertMessageState: setAlertMessage,
                });
            }
        }).catch((err) => {
            console.log(err);
            RootGlobalStateActions.displayAlertMessage({
                alertType: "ERROR",
                message: err.message,
                setAlertMessageState: setAlertMessage,
            });
        }).finally(() => {
            setIsLoading(false);
        });
    }

    function renderBottomButtons() {
        if (cmCentralUserData && cmCentralUserData.subscriptionPlan && !UserSubscriptionUtils.hasAvailableCredits(cmCentralUserData.subscriptionPlan, props.creditsCost)) {
            return (<NotEnoughCreditsButtonsComponent/>);
        } else {
            return (
                <div className='flex flex-col'>
                    <button className={"btn btn-primary btn-outline " + (isLoading ? "loading" : "")} disabled={isLoading || !isFormValid()} onClick={onAskAIUpdate}>
                        <Image className='mr-4' src={"/images/icon-logo.png"}
                               alt={"CoMaker.ai Logo"} width={30} height={30} />
                        Ask AI Update ({"Cost: " + props.creditsCost + " " + CreditLabel})
                    </button>
                </div>
            )
        }
    }

    return (
        <div className='flex justify-stretch w-max-2xl'>
            <div className='flex flex-col w-full gap-2 list-item-bg p-4'>
                <InputTextAreaComponent topLabel={"Ask AI to update the content:"}
                                        defaultValue={formPromptValue}
                                        value={formPromptValue}
                                        hint={"e.g: Add more details to the introduction"} onChanged={onPromptChanged}
                                        disabled={isLoading} maxLength={700}/>
                <div className='flex flex-row w-full justify-end items-end'>
                    {renderBottomButtons()}
                </div>
            </div>
        </div>
    )
}