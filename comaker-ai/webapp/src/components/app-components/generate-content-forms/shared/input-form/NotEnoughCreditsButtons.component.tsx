import Link from "next/link";
import {getPricingPageUrl} from "../../../../../pages/pricing";
import SubscriptionPlanButtonComponent from "../../../subscription/shared/SubscriptionPlanButton.component";
import {UserSubscriptionPlanType} from "../../../../../data/data-model/subscription/UserSubscriptionPlan.model";

export default function NotEnoughCreditsButtonsComponent(props: {}) {
    return (
        <div className='flex flex-col items-center justify-center gap-1'>
            <h4 className='custom'>You don&apos;t have enough credits!</h4>
            <span className='subtitle'>Subscribe a plan or purchase more credits to generate your content.</span>
            <div className='flex wrap gap-2 justify-between items-center'>
                <Link className='btn btn-accent btn-outline btn-sm' href={getPricingPageUrl()}>Upgrade Plan</Link>
                <div className='divider text-slate-400'>OR</div>
                <SubscriptionPlanButtonComponent planType={UserSubscriptionPlanType.BUY_50AI_CREDITS} extraStyle={"btn-sm"}/>
            </div>
        </div>
    )
}