import {
    GeneratedContentBaseModel,
    GenerationPointOfView, GenerationToneStyle
} from "../../../../../data/data-model/generate-text-content/generated-content-base.model";
import LanguageSelectorComponent from "../../../../react-shared-module/ui-components/form/LanguageSelector.component";
import {FormOption} from "../../../../react-shared-module/ui-components/form/RadioButton.component";
import {getLanguageLabel} from "../../../../react-shared-module/logic/language/language.helper";
import DropdownFormComponent from "../../../../react-shared-module/ui-components/form/DropdownForm.component";

export default function GenerateContentCommonBottomInputComponent(props: {
    generateContentData: GeneratedContentBaseModel<any, any>, setGenerateContentData: (generateContentData: GeneratedContentBaseModel<any, any>) => void}) {

    function onLanguageSelected(languageSelected: string) {
        props.generateContentData.inputUserData.outputLanguage = languageSelected;
        props.setGenerateContentData(
            {
                ...props.generateContentData,
            }
        );
    }

    function onToneStyleSelected(toneStyle: string) {
        props.generateContentData.inputUserData.toneStyle = toneStyle;
        props.setGenerateContentData(
            {
                ...props.generateContentData,
            }
        );
    }

    function onPointOfViewSelected(pointOfView: string) {
        props.generateContentData.inputUserData.pointOfView = pointOfView;
        props.setGenerateContentData(
            {
                ...props.generateContentData,
            }
        );
    }

    return (
        <div className='flex flex-col gap-4'>
            <LanguageSelectorComponent disabled={props.generateContentData.isEditable === false} defaultLanguage={props.generateContentData.inputUserData.outputLanguage} onOptionSelected={onLanguageSelected} />

            <div className='flex flex-wrap gap-5 w-full'>
                <DropdownFormComponent label={getLanguageLabel("GENERATE_FORM_TONE_STYLE_LABEL")}
                                       defaultLabel={"Select the content tone"}
                                       currentSelectionKey={props.generateContentData.inputUserData.toneStyle}
                                       options={FormToneStyleOptions}
                                       disabled={props.generateContentData.isEditable === false}
                                       onOptionSelected={onToneStyleSelected} />

                <DropdownFormComponent label={getLanguageLabel("GENERATE_FORM_POINT_OF_VIEW_LABEL")}
                                       defaultLabel={"Select the content point of view"}
                                       currentSelectionKey={props.generateContentData.inputUserData.pointOfView}
                                       options={FormPointOfViewOptions}
                                       disabled={props.generateContentData.isEditable === false}
                                       onOptionSelected={onPointOfViewSelected} />
            </div>
        </div>
    )
}

export const FormPointOfViewOptions: {[optionId: string]: FormOption} = {
    firstPerson: {
        optionId: GenerationPointOfView.FIRST_PERSON,
        label: "First Person",
    },
    secondPerson: {
        optionId: GenerationPointOfView.SECOND_PERSON,
        label: "Second Person",
    },
    thirdPerson: {
        optionId: GenerationPointOfView.THIRD_PERSON,
        label: "Third Person",
    },
}

export const FormToneStyleOptions: {[optionId: string]: FormOption} = {
    enthusiastic: {
        optionId: GenerationToneStyle.ENTHUSIASTIC,
        label: "Enthusiastic",
    },
    conversational: {
        optionId: GenerationToneStyle.CONVERSATIONAL,
        label: "Conversational",
    },
    inspirational: {
        optionId: GenerationToneStyle.INSPIRATIONAL,
        label: "Inspirational",
    },
    humorous: {
        optionId: GenerationToneStyle.HUMOROUS,
        label: "Humorous",
    },
    professional: {
        optionId: GenerationToneStyle.PROFESSIONAL,
        label: "Professional",
    },
}