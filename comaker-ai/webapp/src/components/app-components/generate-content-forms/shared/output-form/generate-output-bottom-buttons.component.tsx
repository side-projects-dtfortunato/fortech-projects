import {GeneratedContentBaseModel} from "../../../../../data/data-model/generate-text-content/generated-content-base.model";
import {
    RootGlobalStateActions,
    useAlertMessageGlobalState
} from "../../../../react-shared-module/logic/global-hooks/root-global-state";
import {useState} from "react";
import {ComakerBackendApi} from "../../../../../data/api/comaker-backend.api";
import {APIResponseMessageUtils} from "../../../../react-shared-module/logic/global-hooks/APIResponseMessageUtils";
import SpinnerComponent from "../../../../react-shared-module/ui-components/shared/Spinner.component";
import {getLanguageLabel} from "../../../../react-shared-module/logic/language/language.helper";


export default function GenerateOutputBottomButtonsComponent(props: {
    generatedContentDataState: GeneratedContentBaseModel<any, any>,
    setGeneratedContentDataState: (generatedContentDataState: GeneratedContentBaseModel<any, any>) => void,
    hasOutputChangedState: boolean, setHasOutputChangedState: (hasOutputChangedState: boolean) => void,
}) {
    const [alertMessageState, setAlertMessage] = useAlertMessageGlobalState();
    const [isLoading, setIsLoading] = useState(false);


    function onResetChanges() {
        if (!props.hasOutputChangedState) {
            props.setHasOutputChangedState(true);
        }
        props.setGeneratedContentDataState({
            ...props.generatedContentDataState,
            outputEditableData: {
                ...props.generatedContentDataState.aiOutputGeneratedData,
            },
        });
    }

    function onGenerateAgain() {
        props.setGeneratedContentDataState(
            {
                ...props.generatedContentDataState,
                uid: undefined,
                aiOutputGeneratedData: undefined,
                outputEditableData: undefined,
                aiResponseData: undefined,
                prompt: undefined,
                isEditable: true,
            }
        )
    }

    function onSaveChanges() {
        setIsLoading(true);
        ComakerBackendApi.saveGeneratedContentChanges({generateContentData: props.generatedContentDataState}).then((response) => {
            if (response.isSuccess) {
                props.setHasOutputChangedState(false);
            } else {
                RootGlobalStateActions.displayAlertMessage({
                    alertType: "ERROR",
                    message: APIResponseMessageUtils.getAPIResponseMessage(response),
                    setAlertMessageState: setAlertMessage,
                });
            }
            setIsLoading(false);
        });
    }

    return (
        <div className='flex flex-col items-center gap-2'>
            {isLoading ? <SpinnerComponent /> : <></>}
            <div className='flex flex-wrap gap-4 justify-evenly items-center'>
                {/*<button disabled={isLoading} className='btn btn-ghost btn-sm'
                         onClick={onResetChanges}>{getLanguageLabel("FORM_RESET_CHANGES")}</button>*/}
                <button disabled={isLoading} className='btn btn-accent btn-sm btn-outline' onClick={onGenerateAgain}>Generate Again</button>
                <button disabled={isLoading || !props.hasOutputChangedState} className='btn btn-success btn-sm' onClick={onSaveChanges}>{getLanguageLabel("FORM_SAVE_CHANGES")}</button>
            </div>
        </div>
    )
}