import {
    GeneratedContentBaseModel
} from "../../../../../../data/data-model/generate-text-content/generated-content-base.model";
import dynamic from "next/dynamic";
import {useEffect, useState} from "react";
import EditorJS, {OutputData} from "@editorjs/editorjs";
import {convertHtmlToBlocks} from "html-to-editorjs";
const edjsHTML = require("editorjs-html");
const EditorBlock = dynamic(() => import("../../../../shared/editor-js/EditorBlock"), {
    ssr: false,
});

const edjsParser = edjsHTML();

export default function GenerateOutputEditorBlocksComponent(props: {generatedOutputData: GeneratedContentBaseModel<any, any>, onGeneratedOutputDataChanged: (data: GeneratedContentBaseModel<any, any>) => void}) {
    //state to hold output data. we'll use this for rendering later
    const [data, setData] = useState<OutputData>(getGeneratedOutput(props.generatedOutputData));



    function onDataChanged(editorOutput: OutputData) {
        setData(editorOutput);

        props.onGeneratedOutputDataChanged({
            ...props.generatedOutputData,
            outputEditableData: {
                ...props.generatedOutputData.outputEditableData,
                body: convertBlocksToHtml(editorOutput),
                // outputContentType: "EDITORJS",
            },
        });
    }

    return (
        <div className='list-item-bg p-4 drop-shadow flex flex-col w-full bg-slate-50'>
            <EditorBlock data={data} onChange={onDataChanged} holder="editorjs-container"/>
        </div>
    )
}

function getGeneratedOutput(generatedOutputData: GeneratedContentBaseModel<any, any>) {
    let outputBlocks: any;
    if (generatedOutputData.outputEditableData && generatedOutputData.inputUserData.outputContentType !== "EDITORJS" && generatedOutputData.outputEditableData.outputContentType !== "EDITORJS") {
        console.log("generatedOutputData.outputEditableData.body: ", generatedOutputData.outputEditableData.body);
        const html = new DOMParser().parseFromString(generatedOutputData.outputEditableData.body, "text/html").body;
        outputBlocks = {
            blocks: convertHtmlToBlocks(html),
            time: Date.now(),
        };

    } else {
        if (typeof generatedOutputData.outputEditableData.body === "string") {
            outputBlocks = JSON.parse(generatedOutputData.outputEditableData.body);
        } else {
            outputBlocks = generatedOutputData.outputEditableData.body;
        }

    }
    console.log("getGeneratedOutput:", outputBlocks);

    return outputBlocks;
}

function convertBlocksToHtml(blocks: any) {
    let listHtmlItems: string[] = edjsParser.parse(blocks);
    let fullHtml: string = "";
    listHtmlItems.forEach((item) => fullHtml = fullHtml + " " + item);

    return fullHtml;
}