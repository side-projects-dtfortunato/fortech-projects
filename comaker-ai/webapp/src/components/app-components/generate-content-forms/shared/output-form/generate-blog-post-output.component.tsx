import {GeneratedContentBaseModel} from "../../../../../data/data-model/generate-text-content/generated-content-base.model";
import {getLanguageLabel} from "../../../../react-shared-module/logic/language/language.helper";
import RichTextEditorComponent from "../../../../react-shared-module/ui-components/form/RichTextEditor.component";
import {useState} from "react";

import GenerateOutputBottomButtonsComponent from "./generate-output-bottom-buttons.component";
import {TrashIcon} from "@heroicons/react/20/solid";
import {ComakerBackendApi} from "../../../../../data/api/comaker-backend.api";
import SpinnerComponent from "../../../../react-shared-module/ui-components/shared/Spinner.component";
import {useRouter} from "next/router";
import {getMyContentPageUrl} from "../../../../../pages/my-content/text";
import GenerateOutputEditorBlocksComponent from "./shared/GenerateOutputEditorBlocks.component";
import GenerateOutputRichTextComponent from "./shared/GenerateOutputRichText.component";

export default function GenerateBlogPostOutputComponent(props: {generatedOutputData: GeneratedContentBaseModel<any, any>, setGenerateContent: (generatedContent: any) => void}) {
    const [hasOutputChanged, setHasOutputChanged] = useState(false);
    // const [generateContentData, setGenerateContentData] = useState<GeneratedContentBaseModel<any, any>>(props.generatedOutputData);
    const [isLoading, setIsLoading] = useState(false);
    const router = useRouter();

    let outputStr: string = props.generatedOutputData.aiOutputGeneratedData.body;
    if (props.generatedOutputData.outputEditableData?.body) {
        outputStr = props.generatedOutputData.outputEditableData?.body;
    }

    async function onDeleteItem() {
        setIsLoading(true);
        await ComakerBackendApi.deleteGeneratedItem({
            itemId: props.generatedOutputData.uid!,
        });
        setIsLoading(false);
        router.push(getMyContentPageUrl());
    }

    function onGeneratedDataChanged(updatedData: GeneratedContentBaseModel<any, any>) {
        props.setGenerateContent(updatedData);
        setHasOutputChanged(true);
    }

    return (
        <>
            <div className='flex justify-center items-center h-full w-full'>
                <div className='flex flex-col w-full gap-5 items-center mb-10'>
                    {isLoading ? <SpinnerComponent /> : <></>}
                    <div className='flex flex-row justify-between items-center w-full'>
                        <div className='w-6 h-6'></div>
                        <GenerateOutputBottomButtonsComponent generatedContentDataState={props.generatedOutputData}
                                                              setGeneratedContentDataState={props.setGenerateContent}
                                                              hasOutputChangedState={hasOutputChanged}
                                                              setHasOutputChangedState={setHasOutputChanged}/>
                        <label htmlFor="my-modal" className='btn btn-ghost btn-xs'>
                            <TrashIcon color={"#333333"} width={20} height={20}/>
                        </label>
                    </div>
                    <GenerateOutputRichTextComponent generatedOutputData={props.generatedOutputData}
                                                          onGeneratedOutputDataChanged={onGeneratedDataChanged}/>
                </div>
            </div>
            {renderDeleteItemConfirmation(onDeleteItem)}
        </>
    )
}

function renderDeleteItemConfirmation(onDelete: () => Promise<void>) {
    return (
        <>
            <input type="checkbox" id="my-modal" className="modal-toggle"/>
            <div className="modal">
                <div className="modal-box">
                    <h3 className="font-bold text-lg">Do you really want to delete permanently this generated item?</h3>
                    <div className="modal-action flex items-center gap2">
                        <label htmlFor="my-modal" className="btn btn-ghost btn-error">Cancel</label>
                        <label htmlFor="my-modal" className="btn btn-success" onClick={onDelete}>Yes, delete it</label>
                    </div>
                </div>
            </div>
        </>
    )
}