import {
    GeneratedContentBaseModel
} from "../../../../../../data/data-model/generate-text-content/generated-content-base.model";
import dynamic from "next/dynamic";
import {useEffect, useState} from "react";
import EditorJS, {OutputData} from "@editorjs/editorjs";
import {convertHtmlToBlocks} from "html-to-editorjs";
import RichTextEditorComponent from "../../../../../react-shared-module/ui-components/form/RichTextEditor.component";
const edjsHTML = require("editorjs-html");
const EditorBlock = dynamic(() => import("../../../../shared/editor-js/EditorBlock"), {
    ssr: false,
});

const edjsParser = edjsHTML();

export default function GenerateOutputRichTextComponent(props: {generatedOutputData: GeneratedContentBaseModel<any, any>, onGeneratedOutputDataChanged: (data: GeneratedContentBaseModel<any, any>) => void}) {
    //state to hold output data. we'll use this for rendering later
    const [data, setData] = useState<string>(props.generatedOutputData.outputEditableData.body);

    if (data !== props.generatedOutputData.outputEditableData.body) {
        setData(props.generatedOutputData.outputEditableData.body);
    }

    function onDataChanged(text: string) {
        setData(text);

        props.onGeneratedOutputDataChanged({
            ...props.generatedOutputData,
            outputEditableData: {
                ...props.generatedOutputData.outputEditableData,
                body: text,
            },
        });
    }

    return (
        <div className='flex flex-col w-full'>
            <RichTextEditorComponent onTextChanged={onDataChanged} initialValue={data} height={600} />
        </div>
    )
}