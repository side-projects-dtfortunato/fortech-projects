import {FooterLinkItem} from "../../react-shared-module/ui-components/root/Footer.component";
import RootLayoutComponent, {PageHeadProps} from "../../react-shared-module/ui-components/root/RootLayout.component";
import LoginPage from "../../../pages/login";
import SideLeftMenuComponent from "./menu/side-left-menu.component";
import {useAuthState} from "react-firebase-hooks/auth";
import {auth} from "../../react-shared-module/logic/shared-data/firebase.utils";
import AuthManager from "../../react-shared-module/logic/auth/auth.manager";
import SpinnerComponent from "../../react-shared-module/ui-components/shared/Spinner.component";

export default function CustomRootLayoutComponent(props: {pageHeadProps?: PageHeadProps, children: any, customBody?: boolean,
    listFooterLinks?: FooterLinkItem[],
    isIndexable?: boolean, isLoading?: boolean, customNavbarItems?: any,
    leftChilds?: any, rigthChilds?: any, notAuthorized?: boolean, authRequired?: boolean, backgroundColor?: string}) {
    const [currentUser, isAuthLoading] = useAuthState(auth);

    function renderLeftSideContent() {
        let leftSideContent: any[] = [];

        leftSideContent.push(<SideLeftMenuComponent />);

        if (props.leftChilds) {
            leftSideContent = leftSideContent.concat(props.leftChilds);
        }

        return (
            <div className='flex flex-col gap-3'>
                {leftSideContent}
            </div>
        )
    }

    function renderContent() {
        return (
            <div className='flex flex-row gap-6 w-full m-2 mb-20 justify-center'>
                {props.customBody ? <></> :
                    <div className='hidden md:flex'>{renderLeftSideContent()}</div>}
                <div className='w-full max-w-6xl justify-self-center'>
                    {props.authRequired && isAuthLoading ? <SpinnerComponent/> : props.children}

                </div>
            </div>
        )
    }

    // Override Leftside content
    let customProps: any = {
        ...props,
        customBody: true,
        children: renderContent(),
    }


    if ((!isAuthLoading && !AuthManager.isUserLogged() && props.authRequired) || props.notAuthorized) {
        return <LoginPage />;
    } else {
        return RootLayoutComponent(customProps);
    }

}