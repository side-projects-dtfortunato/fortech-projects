import SideMenuItemComponent, {SideMenuItemParams} from "./shared/side-menu-item.component";
import {SvgIconsUtils} from "../../../ui-helper/SvgIcons.utils";
import {
    StaticGenerateContentCategoriesList, StaticGenerateContentListContent
} from "../../../../data/static-data/static-content/static-generate-content-list.content";
import {getGenerateCategoryContentPageURL} from "../../../../pages/generate/category/[categoryContent]";
import SideMenuAvailableCreditsComponent from "../../subscription/side-menu-available-credits.component";
import {useAuthState} from "react-firebase-hooks/auth";
import {auth} from "../../../react-shared-module/logic/shared-data/firebase.utils";
import AuthManager from "../../../react-shared-module/logic/auth/auth.manager";
import {getGenerateImageTypePageURL} from "../../../../pages/generate-image/[contentType]";
import {GenerationImageContentType} from "../../../../data/data-model/generate-image/generate-image-base-prompt.model";
import {getLanguageLabel} from "../../../react-shared-module/logic/language/language.helper";
import {getContentCategoryIcon} from "../../../../data/static-data/static-content/static-content-categories-icons.map";
import {getGenerateContentTypePageURL} from "../../../../pages/generate/[contentType]";
import {
    GenerationTextContentType
} from "../../../../data/data-model/generate-text-content/generated-content-base.model";

export default function SideLeftMenuComponent(props: {children?: any}) {
    const [currentUser, isAuthLoading] = useAuthState(auth);

    function renderUserAuthRestricted() {
        let listMenuItems: any[] = [];
        if (!isAuthLoading && AuthManager.isUserLogged()) {
            listMenuItems.push((
                <div className='my-5'>
                    <SideMenuAvailableCreditsComponent />
                </div>
            ));

            // Dashboard
            listMenuItems.push(<SideMenuItemComponent {...SideMenuPaths.userDashboard} />);
            listMenuItems.push(<SideMenuItemComponent {...SideMenuPaths.myContent} />);
            listMenuItems.push(<SideMenuItemComponent {...SideMenuPaths.myImages} />);
        }
        return listMenuItems;
    }

    function renderSideMenuItem() {
        let listMenuItems: any[] = [];

        // User Navigation
        listMenuItems = listMenuItems.concat(renderUserAuthRestricted());


        // Divider
        listMenuItems.push((<div className='divider text-slate-400'>Create Content</div>))

        // Write any Document
        listMenuItems.push(SideMenuItemComponent({
            icon: getContentCategoryIcon(StaticGenerateContentCategoriesList.writeDocument.uid),
            autoSelect: true,
            path: getGenerateContentTypePageURL(GenerationTextContentType.WRITE_DOCUMENT_ASSIST),
            label: StaticGenerateContentListContent.writeDocument.label,
        }));


        // Generate an Image
        listMenuItems.push(SideMenuItemComponent({
            icon: getContentCategoryIcon(StaticGenerateContentCategoriesList.imageGenerator.uid),
            autoSelect: true,
            path: getGenerateImageTypePageURL(GenerationImageContentType.CREATE_IMAGE),
            label: getLanguageLabel("GENERATE_PAGE_TITLE_" + GenerationImageContentType.CREATE_IMAGE),
        }));

        // Divider
        listMenuItems.push((<div className='divider text-slate-400'>Content Templates</div>))

        // Generate Content Menu
        //listMenuItems.push(SideMenuItemComponent(SideMenuPaths.generateContent));

        // Generate Content Menu for categories
        listMenuItems = listMenuItems.concat(Object.values(StaticGenerateContentCategoriesList)
            .filter((item) => item.displayMenu !== false)
            .map((categoryItem) => {
            return SideMenuItemComponent({
                icon: getContentCategoryIcon(categoryItem.uid),
                autoSelect: true,
                label: categoryItem.categoryLabel,
                path: getGenerateCategoryContentPageURL(categoryItem.uid),
            });
        }));

        return listMenuItems;
    }

    return (
        <ul className="menu w-56 p-2 menu-font gap-4">
            {props.children}
            {renderSideMenuItem()}
        </ul>
    )
}

export const SideMenuPaths: {[menuId: string]: SideMenuItemParams} = {
    userDashboard: {
        path: "/",
        label: "Dashboard",
        // icon: (<Image className='fill-current' src={"/images/menu/ic_menu_dashboard.svg"} height={128} width={128} alt={"Dashboard"} />),
        icon: SvgIconsUtils.menu_dashboard,
        autoSelect: true,
    },
    myContent: {
        path: "/my-content/text",
        label: "My Content (Text)",
        icon: SvgIconsUtils.menu_my_content,
        autoSelect: true,
    },
    myImages: {
        path: "/my-content/images",
        label: "My Images",
        icon: SvgIconsUtils.icon_image,
        autoSelect: true,
    },
    generateContent: {
        path: "/generate",
        label: "All Tools",
        icon: SvgIconsUtils.menu_generate_content,
        autoSelect: true,
    },

}