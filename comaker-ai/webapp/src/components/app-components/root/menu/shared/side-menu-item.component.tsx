import Link from "next/link";
import {useRouter} from "next/router";
import {useEffect, useState} from "react";

export interface SideMenuItemParams {path: string, label: string, icon?: any, isSelected?: boolean, autoSelect?:boolean}

export default function SideMenuItemComponent(props: SideMenuItemParams) {
    const route = useRouter();
    const [isMenuSelected, setIsMenuSelected] = useState(getIsSelected());

    useEffect(() => {
        setIsMenuSelected(getIsSelected);
    }, [route]);

    function getIsSelected() {
        if (props.autoSelect) {
            return route.asPath === props.path;
        } else if (props.isSelected){
            return props.isSelected;
        } else {
            return true;
        }
    }

    let classSuffix: string = "bg-transparent text-slate-500 fill-slate-500 hover:bg-primary-content bg-opacity-20";
    if (isMenuSelected) {
        classSuffix = "bg-primary-content text-primary bg-opacity-20 fill-primary"
    }


    return (
        <Link href={props.path} className={'flex flex-row px-2 py-2 gap-3 items-center rounded-lg ' + classSuffix}>
            <div className='w-6 h-6'>
                {props.icon}
            </div>
            <span className='text-base font-bold'>{props.label}</span>
        </Link>
    );
}