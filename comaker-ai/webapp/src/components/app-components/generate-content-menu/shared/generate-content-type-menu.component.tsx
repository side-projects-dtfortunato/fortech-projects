import {
    StaticGenerateContentTypeModel
} from "../../../../data/static-data/static-data-model/static-generate-content-type.model";
import Link from "next/link";
import {getGenerateContentTypePageURL} from "../../../../pages/generate/[contentType]";
import {getContentCategoryIcon} from "../../../../data/static-data/static-content/static-content-categories-icons.map";
import {GenerationType} from "../../../../data/data-model/generate-text-content/generated-content-base.model";
import {getGenerateImageTypePageURL} from "../../../../pages/generate-image/[contentType]";

export default function GenerateContentTypeMenuComponent(props: {menuOption: StaticGenerateContentTypeModel}) {

    function renderIcon() {
        let backgroundColor: string = "bg-[" + props.menuOption.category!.accentColor + "]";
        let textColor = " text-[" + props.menuOption.category!.accentColor + "]";
        return (
            <div className={'flex flex-row gap-2 items-center rounded-md p-1 bg-opacity-20 ' + backgroundColor + textColor} >
                <div className='w-6 h-6'>
                    {getContentTypeIcon(props.menuOption)}
                </div>
                <span className='text-xs sm:text-sm font-bold'>
                    {props.menuOption.category!.categoryLabel.toUpperCase()}
                </span>
            </div>
        )
    }

    return (
        <Link href={getGenerationContentUrl(props.menuOption)}
              className={'flex flex-col gap-3 list-item-bg drop-shadow px-3 py-5 w-full max-w-xs sm:w-56 h-48 hover:bg-[' + props.menuOption.category!.accentColor + "] hover:bg-opacity-5"}>
            {renderIcon()}
            <h3 className='custom'>{props.menuOption.label}</h3>
            <span className='subtitle'>{props.menuOption.description}</span>
        </Link>
    )
}

function getGenerationContentUrl(menuOption: StaticGenerateContentTypeModel) {
    if (menuOption.category?.generationType === GenerationType.IMAGE) {
        return getGenerateImageTypePageURL(menuOption.uid);
    } else {
        return getGenerateContentTypePageURL(menuOption.uid)
    }
}

function getContentTypeIcon(menuOption: StaticGenerateContentTypeModel) {
    return getContentCategoryIcon(menuOption.category?.uid!);
}