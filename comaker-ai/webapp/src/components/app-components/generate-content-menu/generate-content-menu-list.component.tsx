import {
    StaticGenerateContentListContent
} from "../../../data/static-data/static-content/static-generate-content-list.content";
import GenerateContentTypeMenuComponent from "./shared/generate-content-type-menu.component";
import {
    StaticGenerateContentTypeModel
} from "../../../data/static-data/static-data-model/static-generate-content-type.model";

export default function GenerateContentMenuListComponent(props: { category?: string }) {

    function renderList() {
        return Object.values(StaticGenerateContentListContent).filter((item) => props.category ? item.category!.uid === props.category : true)
            .map((menuItem) => {
                return <GenerateContentTypeMenuComponent key={menuItem.uid} menuOption={menuItem as StaticGenerateContentTypeModel}/>
            });
    }

    return (
        <div className='flex flex-wrap justify-center items-center gap-3 w-full'>
            {renderList()}
        </div>
    )
}