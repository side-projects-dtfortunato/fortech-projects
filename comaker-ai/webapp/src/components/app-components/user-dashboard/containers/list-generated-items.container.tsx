import {
    GenerateBaseModel,
    GeneratedContentBaseModel, GenerationType
} from "../../../../data/data-model/generate-text-content/generated-content-base.model";
import {
    initQueryPaginationDataModel,
    QueryPaginationDataModel
} from "../../../react-shared-module/logic/shared-data/datamodel/query-pagination-data.model";
import {ComakerBackendApi} from "../../../../data/api/comaker-backend.api";
import ListItemsPaginationComponent
    from "../../../react-shared-module/ui-components/shared/ListItemsPagination.component";
import AuthManager from "../../../react-shared-module/logic/auth/auth.manager";
import GeneratedListItemComponent from "../list-items/generated-list-item.component";
import Skeleton from "react-loading-skeleton";
import {useEffect, useState} from "react";
import {
    GenerateImageBasePromptModel
} from "../../../../data/data-model/generate-image/generate-image-base-prompt.model";
import GeneratedImageListImageComponent from "../list-items/generated-image-list-image.component";
import SpinnerComponent from "../../../react-shared-module/ui-components/shared/Spinner.component";
import Link from "next/link";
import {ComakerRoutesUtils} from "../../../../managers/comaker-routes.utils";
import {SvgIconsUtils} from "../../../ui-helper/SvgIcons.utils";
import NotFoundSimpleMessageComponent
    from "../../../react-shared-module/ui-components/shared/NotFoundSimpleMessage.component";

export default function ListGeneratedItemsContainer(props: {listType: GenerationType}) {
    const [isLoading, setIsLoading] = useState(true);
    const [listData, setListData] = useState<GenerateBaseModel[] | undefined>();

    useEffect(() => {
        if (!listData) {
            ComakerBackendApi.getListGeneratedContent({
                userId: AuthManager.getUserId()!,
                listType: props.listType,
            }).then((listItems: GenerateBaseModel[]) => {
                setListData(listItems);
                setIsLoading(false);
            }).catch((err) => {
               setListData([]);
                setIsLoading(false);
            });
        }
    }, []);

    function renderItem(listItem: GenerateBaseModel) {

        if (listItem.type) {
            switch (listItem.type) {
                case GenerationType.IMAGE:
                    return <GeneratedImageListImageComponent item={listItem as GenerateImageBasePromptModel} />;
                case GenerationType.TEXT:
                default:
                    return <GeneratedListItemComponent generatedContentItem={listItem as GeneratedContentBaseModel<any, any>}/>;
            }
        }

        return <GeneratedListItemComponent generatedContentItem={listItem as GeneratedContentBaseModel<any, any>}/>
    }

    function renderContentLoader() {
        if (isLoading) {
            return (
                <SpinnerComponent />
            )
        } else {
            return <></>
        }
    }

    function renderButton() {
        return (<Link href={ComakerRoutesUtils.getUserDashboardUrl()} className='btn btn-primary text-white gap-2'> <div className='w-6 h-6'>{SvgIconsUtils.menu_generate_content}</div> Add new content</Link> )
    }

    function renderListItems() {
        if (listData && listData.length > 0) {
            return (<div className='flex flex-wrap justify-center gap-3'>
                {isLoading ? renderContentLoader() : <></>}
                {listData.sort((i1: GenerateBaseModel, i2: GenerateBaseModel) => {return i2.updatedAt! - i1.updatedAt!}).map((item) => {
                    return renderItem(item);
                })}
            </div>)
        } else if (isLoading) {
            return renderContentLoader();
        } else {
            return (
                <NotFoundSimpleMessageComponent message={"You didn't generated any content yet\""} />
            )
        }

    }

    return (<div className='flex flex-col gap-2 w-full justify-center items-center h-full'>
        {renderButton()}
        {renderListItems()}
    </div>);
}
