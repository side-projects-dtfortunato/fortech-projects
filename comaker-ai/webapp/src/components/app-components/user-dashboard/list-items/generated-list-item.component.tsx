import {GeneratedContentBaseModel} from "../../../../data/data-model/generate-text-content/generated-content-base.model";
import CommonUtils from "../../../react-shared-module/logic/commonutils";
import {DocumentTextIcon, TrashIcon} from "@heroicons/react/24/outline";
import Link from "next/link";
import {getGeneratedItemDetailsPageUrl} from "../../../../pages/generated-item/[id]";
import {ComakerBackendApi} from "../../../../data/api/comaker-backend.api";
import {
    RootGlobalStateActions,
    useAlertMessageGlobalState,
    useIsLoadingGlobalState
} from "../../../react-shared-module/logic/global-hooks/root-global-state";
import {APIResponseMessageUtils} from "../../../react-shared-module/logic/global-hooks/APIResponseMessageUtils";
import Popup from "reactjs-popup";
import {useState} from "react";
import {getContentCategoryIcon} from "../../../../data/static-data/static-content/static-content-categories-icons.map";

export default function GeneratedListItemComponent(props: { generatedContentItem: GeneratedContentBaseModel<any, any> }) {
    const [displayAlertMessage, setDisplayAlertMessage] = useAlertMessageGlobalState();
    const [isLoading, setIsLoading] = useState(false);
    const [openPopup, setOpenPopup] = useState(false);

    function onCloseRemovePopup() {
        setOpenPopup(false)
    }

    function onRemoveItemConfirmed() {
        setIsLoading(true);
        ComakerBackendApi.deleteGeneratedItem({itemId: props.generatedContentItem.uid!}).then((res) => {
            setIsLoading(false);
            if (!res.isSuccess) {
                RootGlobalStateActions.displayAlertMessage({
                    alertType: "ERROR",
                    message: APIResponseMessageUtils.getAPIResponseMessage(res),
                    setAlertMessageState: setDisplayAlertMessage,
                });
            } else {
                CommonUtils.refreshPage();
            }
        });
    }

    function renderBadge() {
        if (props.generatedContentItem.contentTypeDetails) {
            let backgroundColor: string = "bg-[" + props.generatedContentItem.contentTypeDetails.category!.accentColor + "]";
            let textColor = " text-[" + props.generatedContentItem.contentTypeDetails.category!.accentColor + "]";
            return (
                <div
                    className={'flex flex-row gap-2 items-center rounded-md px-2 py-1 bg-opacity-20 ' + backgroundColor + textColor}>
                    <div className='w-4 h-4'>
                        {
                            getContentCategoryIcon(props.generatedContentItem.contentTypeDetails.category?.uid!)}
                    </div>
                    <span className='text-xs font-bold'>
                        {props.generatedContentItem.contentTypeDetails.label.toUpperCase()}
                    </span>
                </div>
            )
        } else {
            return (<></>);
        }
    }

    return (
        <Link href={getGeneratedItemDetailsPageUrl(props.generatedContentItem.uid!)}
            className='flex flex-col w-full sm:w-80 gap-2 list-item-bg drop-shadow py-5 px-4 hover:bg-slate-50'>
            {renderBadge()}

            <h3 className="custom">{props.generatedContentItem.listItemLabel}</h3>

            <div className='flex grow items-end w-full' onClick={(e) => CommonUtils.preventClickPropagation(e)}>
                <div className='flex flex-row justify-between items-center w-full'>
                    <span
                        className="subtitle">{"Updated " + CommonUtils.getTimeAgo(props.generatedContentItem.updatedAt!)}</span>
                </div>
            </div>

        </Link>
    )
}