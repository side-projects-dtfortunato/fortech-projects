import {
    GenerateImageBasePromptModel
} from "../../../../data/data-model/generate-image/generate-image-base-prompt.model";
import {getLanguageLabel} from "../../../react-shared-module/logic/language/language.helper";
import Image from "next/image";
import CommonUtils from "../../../react-shared-module/logic/commonutils";
import Link from "next/link";
import {getGeneratedImageViewerPageURL} from "../../../../pages/generate-image/viewer/[id]";

export default function GeneratedImageListImageComponent(props: {item: GenerateImageBasePromptModel}) {

    if (props.item.storedImages && props.item.storedImages.length > 0) {
        return (
            <Link href={getGeneratedImageViewerPageURL(props.item.uid!)} className='flex flex-col w-80 gap-2 list-item-bg drop-shadow py-3 px-3 hover:bg-slate-50 hover:scale-95'>
                <div className='badge'>{getLanguageLabel("GENERATE_ITEM_TYPE_LABEL_" + props.item.generationType)}</div>
                <Image src={props.item.storedImages![0].fileUrl} alt={props.item.aiInput.promptText} width={512} height={512}/>
                <span className="subtitle">{"Updated " + CommonUtils.getTimeAgo(props.item.updatedAt!)}</span>
            </Link>
        )
    } else {
        return (<></>);
    }
}