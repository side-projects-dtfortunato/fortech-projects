import {useCentralUserDataState} from "../../react-shared-module/logic/global-hooks/root-global-state";
import {
    ComakerCentralUserDataModel,
    CreatorRoles
} from "../../../data/data-model/user-data/comaker-central-user-data.model";
import InputTextComponent from "../../react-shared-module/ui-components/form/InputText.component";
import NotFoundContentComponent from "../../react-shared-module/ui-components/shared/NotFoundContent.component";
import DropdownFormComponent from "../../react-shared-module/ui-components/form/DropdownForm.component";
import CommonUtils from "../../react-shared-module/logic/commonutils";
import ManageSubscriptionBillingButtonComponent from "../subscription/shared/ManageSubscriptionBillingButton.component";
import {SharedBackendApi} from "../../react-shared-module/logic/shared-data/sharedbackend.api";
import {useState} from "react";
import SideMenuAvailableCreditsComponent from "../subscription/side-menu-available-credits.component";

export default function UserEditSettingsComponent() {
    const [centralUserData, setCentralUserData] = useCentralUserDataState();
    const comakerCentralData: ComakerCentralUserDataModel = centralUserData as ComakerCentralUserDataModel;
    const [isLoading, setIsLoading] = useState(false);

    function onUsernameChanged(text: string) {
        setCentralUserData({
            ...comakerCentralData,
            username: text,
        });
    }

    function onRoleSelected(option: string) {
        setCentralUserData({
            ...comakerCentralData,
            creatorRole: option,
        } as ComakerCentralUserDataModel);
    }

    async function onSaveChanges() {
        setIsLoading(true);
        await SharedBackendApi.updateUserProfile({centralUserData: centralUserData!});
        setIsLoading(false);
    }


    if (centralUserData) {
        return (
            <div className='flex flex-col list-item-bg drop-shadow w-full gap-3 p-5 max-w-2xl'>
                <div className='flex justify-between items-center'>
                    <h3 className='custom'>Basic Information</h3>
                    <button className={"btn btn-primary btn-outline " + (isLoading ? "loading" : "")} onClick={onSaveChanges}>Save changes</button>
                </div>
                <InputTextComponent topLabel={"Username"} hint={"What is your name?"} defaultValue={centralUserData!.username} onChanged={onUsernameChanged} />
                <InputTextComponent topLabel={"Email"} hint={""} defaultValue={centralUserData!.email} onChanged={(text) => {}} disabled={true} />
                <DropdownFormComponent label={"Your role"} defaultLabel={"Select your role"} currentSelectionKey={comakerCentralData.creatorRole}
                                       options={CommonUtils.convertArrayToDropdownOptions(CreatorRoles)}
                                       onOptionSelected={onRoleSelected} />
                <div className='flex flex-col gap-3 justify-center items-center w-full mt-5'>
                    <div className='divider'></div>
                    <h3 className='custom w-full text-start'>Subscription Plan:</h3>
                    <SideMenuAvailableCreditsComponent />
                    <ManageSubscriptionBillingButtonComponent />
                </div>

            </div>
        )
    } else {
        return (<></>);
    }

}