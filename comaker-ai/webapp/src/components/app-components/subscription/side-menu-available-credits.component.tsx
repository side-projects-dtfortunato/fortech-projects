import {useCentralUserDataState} from "../../react-shared-module/logic/global-hooks/root-global-state";
import {ComakerCentralUserDataModel} from "../../../data/data-model/user-data/comaker-central-user-data.model";
import {
    UserSubscriptionPlanType,
    UserSubscriptionUtils
} from "../../../data/data-model/subscription/UserSubscriptionPlan.model";
import {useState} from "react";
import {ComakerBackendApi} from "../../../data/api/comaker-backend.api";
import AuthManager from "../../react-shared-module/logic/auth/auth.manager";
import Link from "next/link";
import {getPricingPageUrl} from "../../../pages/pricing";
import RootConfigs from "../../../configs";
import SubscriptionPlanButtonComponent from "./shared/SubscriptionPlanButton.component";

export default function SideMenuAvailableCreditsComponent() {
    const [centralUserData, setCentralUserData] = useCentralUserDataState();
    const [isLoading, setIsLoading] = useState(false);

    function renderAvailableCreditsLabel() {
        let availableCredits: number = (centralUserData as ComakerCentralUserDataModel).subscriptionPlan ? (centralUserData as ComakerCentralUserDataModel).subscriptionPlan!.availableCredits : 0;
        let extraCredits: number | undefined = (centralUserData as ComakerCentralUserDataModel).subscriptionPlan?.extraCredits;

        if (extraCredits && extraCredits > 0) {
            availableCredits += extraCredits;
        }
        return (
            <div className='flex flex-row justify-center items-center gap-1'>
                <span className='text-base font-bold text-black'>{availableCredits}</span>
                <span className='text-xs font-light text-slate-700'>AI Credits left</span>
            </div>
        )
    }


    function renderChartAvailableCredits() {
        let availableCredits: number = (centralUserData as ComakerCentralUserDataModel).subscriptionPlan ? (centralUserData as ComakerCentralUserDataModel).subscriptionPlan!.availableCredits : 0;
        let extraCredits: number | undefined = (centralUserData as ComakerCentralUserDataModel).subscriptionPlan?.extraCredits;
        let initialCredits: number = (centralUserData as ComakerCentralUserDataModel).subscriptionPlan ? (centralUserData as ComakerCentralUserDataModel).subscriptionPlan!.initialCredits : 0;

        if (extraCredits && extraCredits > 0) {
            availableCredits += extraCredits;
        }

        let percentage: number = availableCredits > initialCredits ? 100 : (availableCredits * 100)/initialCredits;

        return (
            <progress className="progress progress-primary w-full" value={percentage} max="100"></progress>
        )
    }

    async function onBuyCreditsClicked() {
        setIsLoading(true);

        await ComakerBackendApi.buyMoreCredits({credits: 100});
        setTimeout(async () => {
            setCentralUserData(await AuthManager.getCentralUserData(true));
        }, 2000);


        setIsLoading(false);
    }

    function renderBuyCreditsTest() {
        return (
            <button className={"btn btn-sm btn-primary text-white text-center " + (isLoading ? "loading": "")} onClick={onBuyCreditsClicked}>Buy More Credits</button>
        )
    }

    function renderUpgradePlan() {
        if (centralUserData) {
            const coMakerCentralUserData: ComakerCentralUserDataModel = centralUserData;

            if (coMakerCentralUserData.subscriptionPlan && coMakerCentralUserData.subscriptionPlan.planType !== UserSubscriptionPlanType.SUBSCRIBED_PRO) {
                return (
                    <Link href={getPricingPageUrl()} className='btn btn-accent btn-outline btn-xs'>🚀 Upgrade Plan</Link>
                )
            }
        }

        return (<></>);
    }

    if (!centralUserData || !(centralUserData as ComakerCentralUserDataModel).subscriptionPlan) {
        return (<></>);
    } else {
        return (
            <div className='flex flex-col gap-3 justify-center items-center'>
                <span className='text-base font-bold text-secondary p-2 bg-secondary-content bg-opacity-30 rounded'>{UserSubscriptionUtils.getPlanTypeLabel((centralUserData as ComakerCentralUserDataModel).subscriptionPlan!.planType)}</span>
                {renderUpgradePlan()}
                {renderAvailableCreditsLabel()}
                {renderChartAvailableCredits()}
                <SubscriptionPlanButtonComponent planType={UserSubscriptionPlanType.BUY_50AI_CREDITS} extraStyle={"btn-xs"}/>

            </div>
        )
    }
}