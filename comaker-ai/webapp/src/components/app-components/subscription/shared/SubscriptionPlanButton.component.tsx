import {UserSubscriptionPlanType} from "../../../../data/data-model/subscription/UserSubscriptionPlan.model";
import {useAuthState} from "react-firebase-hooks/auth";
import {auth} from "../../../react-shared-module/logic/shared-data/firebase.utils";
import {ComakerBackendApi} from "../../../../data/api/comaker-backend.api";
import {useState} from "react";
import {useRouter} from "next/router";
import AuthManager from "../../../react-shared-module/logic/auth/auth.manager";
import {
    RootGlobalStateActions,
    useAlertMessageGlobalState,
    useCentralUserDataState
} from "../../../react-shared-module/logic/global-hooks/root-global-state";
import {ComakerCentralUserDataModel} from "../../../../data/data-model/user-data/comaker-central-user-data.model";
import {getSubscriptionSuccessPageURL} from "../../../../pages/subscription/success";
import {getLoginPageLink} from "../../../../pages/login";
import RootConfigs from "../../../../configs";

export default function SubscriptionPlanButtonComponent(props: {planType: UserSubscriptionPlanType, extraStyle?: string}) {
    const [currentUser, isAuthLoading] = useAuthState(auth);
    const [centralUserData, setCentralUserData] = useCentralUserDataState();
    let coMakerCentralUserData: ComakerCentralUserDataModel | undefined | null = centralUserData;
    const [isLoading, setIsLoading] = useState(false);
    const route = useRouter();
    const [alertMessage, setAlertMessage] = useAlertMessageGlobalState();

    const isPaymentsDisabled: boolean = RootConfigs.DISABLE_PAYMENTS;

    async function onSubscribeClicked() {
        setIsLoading(true);

        if (!AuthManager.isUserLogged()) {
            // Redirect user to Login page
            route.push(getLoginPageLink());
        } else {
            // Check if user already have a subscription
            if (props.planType !== UserSubscriptionPlanType.BUY_50AI_CREDITS && coMakerCentralUserData?.subscriptionPlan && coMakerCentralUserData?.subscriptionPlan?.planType !== UserSubscriptionPlanType.FREE) {
                // Already have a subscription, so, can use the upgrade function
                const apiResponse = await ComakerBackendApi.updateSubscriptionPlan({planType: props.planType});

                console.log(apiResponse);

                if (apiResponse.isSuccess) {
                    RootGlobalStateActions.displayAlertMessage({alertType: "SUCCESS", message: "Congratulations. You updated successfully your subscription", setAlertMessageState: setAlertMessage});
                    await route.push(getSubscriptionSuccessPageURL());
                } else {
                    RootGlobalStateActions
                        .displayAPIResponseError({apiResponse: apiResponse, setAlertMessageState: setAlertMessage});
                }
            } else {

                // NEW SUBSCRIBER
                const apiResponse = await ComakerBackendApi
                    .stripeCreateCheckoutSession({planType: props.planType});
                if (apiResponse.isSuccess && apiResponse.responseData.redirectUrl) {
                    document.location.href = apiResponse.responseData.redirectUrl;
                } else {
                    RootGlobalStateActions
                        .displayAPIResponseError({apiResponse: apiResponse, setAlertMessageState: setAlertMessage});
                }
            }

            // Update Central User Data
            await AuthManager.getCentralUserData(true);
        }


        setIsLoading(false);
    }

    function getSubscriptionLabel() {
        switch (props.planType) {
            case UserSubscriptionPlanType.FREE:
                return "Try for Free";
            case UserSubscriptionPlanType.SUBSCRIBED_PRO:
                return "Subscribe Pro Plan";
            case UserSubscriptionPlanType.SUBSCRIBED_HOBBY:
                return "Subscribe Hobby Plan";
            case UserSubscriptionPlanType.BUY_50AI_CREDITS:
                return "Add 50 AI Credits";
        }
    }

    function isSubscribed() {
        return props.planType === coMakerCentralUserData?.subscriptionPlan?.planType;
    }

    return (
        <button className={'btn btn-primary text-white ' + (isLoading ? "loading" : "") + " " + props.extraStyle} disabled={isPaymentsDisabled || isAuthLoading || isSubscribed() || isLoading} onClick={onSubscribeClicked}>{getSubscriptionLabel()}</button>
    )
}