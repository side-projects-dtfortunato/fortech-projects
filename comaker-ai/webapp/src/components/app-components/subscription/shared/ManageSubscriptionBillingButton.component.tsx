import {UserSubscriptionPlanType} from "../../../../data/data-model/subscription/UserSubscriptionPlan.model";
import {useAuthState} from "react-firebase-hooks/auth";
import {auth} from "../../../react-shared-module/logic/shared-data/firebase.utils";
import {ComakerBackendApi} from "../../../../data/api/comaker-backend.api";
import RootConfigs from "../../../../configs";
import {useState} from "react";
import {useRouter} from "next/router";
import AuthManager from "../../../react-shared-module/logic/auth/auth.manager";
import {
    RootGlobalStateActions,
    useAlertMessageGlobalState
} from "../../../react-shared-module/logic/global-hooks/root-global-state";
import {getLanguageLabel} from "../../../react-shared-module/logic/language/language.helper";


export default function ManageSubscriptionBillingButtonComponent() {
    const [currentUser, isAuthLoading] = useAuthState(auth);
    const [isLoading, setIsLoading] = useState(false);
    const route = useRouter();
    const [alertMessage, setAlertMessage] = useAlertMessageGlobalState();

    async function onButtonClicked() {
        setIsLoading(true);

        console.log("onButtonClicked");
        ComakerBackendApi.stripeCreateCustomerPortalSession()
            .then((apiResponse) => {
            console.log("ApiResponse:", apiResponse);
            setIsLoading(false);
            if (apiResponse.isSuccess && apiResponse.responseData.redirectUrl) {
                document.location.href = apiResponse.responseData.redirectUrl;
            } else {
                RootGlobalStateActions.displayAPIResponseError({apiResponse, setAlertMessageState: setAlertMessage});
            }
        }).catch((err) => {
            console.log(err);
            RootGlobalStateActions.displayAlertMessage({
                alertType: "ERROR",
                message: "Error:" + err,
                setAlertMessageState: setAlertMessage,
            });
        });

    }

    if (!isAuthLoading && AuthManager.isUserLogged()) {
        return (
            <button className={'btn btn-secondary btn-outline ' + (isLoading ? "loading" : "")} disabled={isAuthLoading || !AuthManager.isUserLogged()} onClick={onButtonClicked}>{getLanguageLabel("BILLING_MANAGE_BTN")}</button>
        )
    } else {
        return (<></>);
    }

}