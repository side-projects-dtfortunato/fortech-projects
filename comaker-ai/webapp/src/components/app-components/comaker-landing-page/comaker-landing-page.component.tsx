import CustomRootLayoutComponent from "../root/CustomRootLayout.component";
import SaasLandingPageTopComponent
    from "../../react-shared-module/ui-components/saas-landing-page/shared/saas-landing-top.component";
import CoMakerLandingPagePopularToolsComponent from "./shared/landing-page-popular-tools.component";
import CoMakerLandingPageHowItWorksComponent from "./shared/landing-page-how-it-works.component";
import CoMakerLandingPagePricingComponent from "./shared/landing-page-pricing.component";

export default function ComakerLandingPageComponent() {

    return (
        <CustomRootLayoutComponent customBody={true} backgroundColor={"landing-page-bg"}>
            <div className='flex flex-col items-center w-full py-10 px-5'>
                <div className='flex flex-col max-w-6xl gap-10'>
                    <SaasLandingPageTopComponent />
                    <CoMakerLandingPagePopularToolsComponent />
                    <CoMakerLandingPageHowItWorksComponent />
                    <CoMakerLandingPagePricingComponent />
                </div>
            </div>
        </CustomRootLayoutComponent>
    )
}