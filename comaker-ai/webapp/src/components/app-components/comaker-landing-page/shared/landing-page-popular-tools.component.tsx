import {
    StaticPopularGenerateContentListContent
} from "../../../../data/static-data/static-content/static-generate-content-list.content";
import GenerateContentTypeMenuComponent from "../../generate-content-menu/shared/generate-content-type-menu.component";
import LandingPageSecondaryTitleComponent from "./landing-page-secondary-title.component";
import Link from "next/link";
import {getGenerateContentAllToolsUrl} from "../../../../pages/generate";

export default function CoMakerLandingPagePopularToolsComponent() {

    function renderListTools() {
        return StaticPopularGenerateContentListContent.map((item) => {
            return (<GenerateContentTypeMenuComponent key={item.uid} menuOption={item} />)
        })
    }

    return (
        <div className='flex flex-col gap-3 items-center justify-center'>
            <LandingPageSecondaryTitleComponent>Some of our AI Tools</LandingPageSecondaryTitleComponent>
            <div className='flex flex-wrap gap-3 items-center justify-center'>
                {renderListTools()}
            </div>
            <div className='flex w-full justify-center'>
                <Link href={getGenerateContentAllToolsUrl()} className='btn btn-primary btn-outline'>And many more...</Link>
            </div>
        </div>
    )
}
