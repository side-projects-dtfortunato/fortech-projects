export default function LandingPageSecondaryTitleComponent(props: {children: string}) {
    return (
        <div className='flex flex-col gap-2 w-full'>
            <div className='divider'></div>
            <h2 className='text-accent text-center font-bold text-4xl'>{props.children}</h2>
        </div>
    )
}