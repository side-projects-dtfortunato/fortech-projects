import CoMakerPricingTablePlansComponent from "../../pricing-page/CoMakerPricingTablePlans.component";
import LandingPageSecondaryTitleComponent from "./landing-page-secondary-title.component";

export default function CoMakerLandingPagePricingComponent() {

    return (
        <div className='flex flex-col gap-4 items-center w-full'>
            <LandingPageSecondaryTitleComponent>CoMaker.ai Pricing & Plans</LandingPageSecondaryTitleComponent>
            <span className='custom text-center'>Try now for free CoMaker.ai and let us facilitate your content creation.</span>
            <CoMakerPricingTablePlansComponent />
        </div>
    )
}