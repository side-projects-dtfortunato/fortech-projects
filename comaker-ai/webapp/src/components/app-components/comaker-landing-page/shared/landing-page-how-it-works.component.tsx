import LandingPageSecondaryTitleComponent from "./landing-page-secondary-title.component";
import Image from "next/image";

export default function CoMakerLandingPageHowItWorksComponent() {


    function renderSteps() {
        let renderListSteps: any[] = [];

        // Step 1
        renderListSteps.push(renderStep({
            step: 1,
            title: "Select the content type",
            subtitle: "Do you want to generate a Blog post, a title, get new ideas?",
            imgSrc: "/images/landing-page/landing_page_how_works_step_1.png",
        }));

        // Step 2
        renderListSteps.push(renderStep({
            step: 2,
            title: "Give CoMaker.ai a bit of context",
            subtitle: "Give the title, a short description or keywords, to improve the generated content from our AI generator.",
            imgSrc: "/images/landing-page/landing_page_how_works_step_2.png",
        }));

        // Step 3
        renderListSteps.push(renderStep({
            step: 3,
            title: "Setup the expected output",
            subtitle: "Select the tone style, language and the writing perspective to adjust to your type of content.",
            imgSrc: "/images/landing-page/landing_page_how_works_step_3.png",
        }));

        // Step 4
        renderListSteps.push(renderStep({
            step: 4,
            title: "Edit, polish, and publish",
            subtitle: "Use CoMaker.ai’s editor to rewrite paragraphs and polish up sentences. Then, just copy and paste the work into your CMS for publishing.",
            imgSrc: "/images/landing-page/landing_page_how_works_step_4.png",
            isCentered: true,
            customImage:  (
                <div className='flex flex-col gap-3 items-center list-item-bg p-3 shadow-lg shadow-rose-400/50'>
                    <Image src={"/images/logo.png"} alt={"CoMaker.ai logo"} width={200} height={100} />
                    <Image src={"/images/landing-page/landing_page_screenshot.jpg"} alt={"CoMaker.ai Screenshot"} width={1000} height={1000} />
                </div>
            )
        }));

        return renderListSteps.map((item) => {
            return (
                <div key={item.step} className='my-8 w-full'>
                    {item}
                </div>
            )
        });
    }

    return (
        <div className='flex flex-col w-full items-stretch gap-5'>
            <LandingPageSecondaryTitleComponent>How it works</LandingPageSecondaryTitleComponent>
            {renderSteps()}
        </div>
    )
}


function renderStep(props: {
    step: number,
    title: string,
    subtitle: string,
    imgSrc?: string,
    customImage?: any,
    isCentered?: boolean
}) {
    let renderText: any = (
        <div className='flex flex-row items-start gap-2'>
            <div className='rounded-full w-10 h-10 bg-primary-content bg-opacity-25 justify-center items-center'>
                <h2 className='custom text-center'>{props.step}</h2>
            </div>
            <div className='flex flex-col'>
                <h2 className='custom'>{props.title}</h2>
                <span className='custom'>{props.subtitle}</span>
            </div>
        </div>
    )

    let renderImage: any = props.imgSrc ? (
        <img className='shadow-lg shadow-rose-400/50 rounded' src={props.imgSrc} alt={props.title} height={500} width={400}/>
    ) : (<></>);
    if (props.customImage) {
        renderImage = props.customImage
    }

    if (props.isCentered) {
        return (
            <div className='flex flex-col justify-between items-center gap-3'>
                {renderText}
                {renderImage}
            </div>
        )
    } else {
        return (
            <div className='flex flex-col sm:flex-row justify-between items-center sm:items-start gap-4'>
                {renderText}
                {renderImage}
            </div>
        )
    }

}