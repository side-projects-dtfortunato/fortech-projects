import {EDITOR_JS_TOOLS} from "./editorjs-tools";
import React, { memo, useEffect, useRef } from "react";
import EditorJS, { OutputData } from "@editorjs/editorjs";

//props
type Props = {
    data?: OutputData;
    onChange(val: OutputData): void;
    holder: string;
};

/** DOCUMENTATION: https://www.npmjs.com/package/react-editor-js */
const EditorBlock = ({ data, onChange, holder}: Props) => {
    //add a reference to editor
    const ref = useRef<EditorJS>();

    console.log("EditorBlock.data: ", data);

    //initialize editorjs
    useEffect(() => {
        //initialize editor if we don't have a reference
        if (!ref.current) {
            const editor = new EditorJS({
                autofocus: true,
                holder: holder,
                tools: EDITOR_JS_TOOLS,
                data,
                async onChange(api, event) {
                    const data = await api.saver.save();
                    onChange(data);
                },
                onReady() {
                    ref.current = editor;
                }
            });
        } else if (ref.current && data) {
            ref.current.clear();
            ref.current.render(data);
        }

        //add a return function handle cleanup
        return () => {
            if (ref.current && ref.current.destroy) {
                ref.current.destroy();
            }
        };
    }, []);


    return <div id={holder} />;
};

export default memo(EditorBlock);