import Image from "next/image";

export default function PageHeaderTitleComponent(props: {title: string, subtitle: string, displayAvatar: boolean}) {

    function renderAvatar() {
        if (props.displayAvatar) {
            return (
                <div className={'w-12 h-12 avatar rounded-full'}>
                    <Image src={"/images/logo-512.png"} alt={"CoMaker Logo"} width={100} height={100} />
                </div>
            )
        } else {
            return (<></>);
        }
    }

    return (
        <div className='flex flex-col justify-center items-center gap-2 mt-5'>
            <div className='flex flex-wrap justify-center items-center gap-4'>
                {renderAvatar()}
                <h1 className='custom'>{props.title}</h1>
            </div>
            <h4 className='custom text-slate-500 text-center'>{props.subtitle}</h4>
        </div>
    )
}