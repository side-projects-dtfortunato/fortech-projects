import {GeneratedImageSize} from "../../../data/data-model/generate-image/generate-image-base-prompt.model";
import Image from "next/image";

export default function ImageListItemViewerComponent(props: {imageUrl: string, imageSize: GeneratedImageSize}) {

    function getImageSize() {
        /*switch (props.imageSize) {
            case "256x256": return 256;
            case "512x512": return 512;
            case "1024x1024": return 1024;
        }*/
        return 1024;
    }

    return (
        <div className='list-item-bg drop-shadow w-72 h-72 rounded p-1'>
            <Image src={props.imageUrl} alt={"Generated Image"} width={getImageSize()} height={getImageSize()} />
        </div>
    )
}