import {useState} from "react";

export default function FaqAccordionComponent(props: { question: string, answer: string }) {
    const [isOpen, setIsOpen] = useState(false);

    return (
        <div className="bg-white shadow rounded">
            <div
                className="p-4 cursor-pointer"
                onClick={() => setIsOpen(!isOpen)}
            >
                <h2 className="text-xl font-semibold flex justify-between items-center">
                    {props.question}
                    <span className={`transform ${isOpen ? 'rotate-180' : ''}`}>
            <svg
                xmlns="http://www.w3.org/2000/svg"
                className="h-4 w-4"
                viewBox="0 0 20 20"
                fill="currentColor"
            >
              <path
                  fillRule="evenodd"
                  d="M4 8a1 1 0 0 1 .293-.707l6-6a1 1 0 0 1 1.414 0l6 6A1 1 0 0 1 16 9H4a1 1 0 0 1-.707-1.707z"
                  clipRule="evenodd"
              />
            </svg>
          </span>
                </h2>
            </div>
            {isOpen && (
                <div className="p-4 border-t border-gray-200">
                    <p className="text-gray-700">{props.answer}</p>
                </div>
            )}
        </div>
    );
};