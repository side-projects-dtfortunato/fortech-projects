import PricingTableComponent, {
    PricingTablePlanContentModel
} from "../../react-shared-module/ui-components/pricing/PricingTable.component";
import {StaticPricingPlansContent} from "../../../data/static-data/pricing-plans/static-pricing-plans.content";
import {
    useAlertMessageGlobalState,
    useCentralUserDataState
} from "../../react-shared-module/logic/global-hooks/root-global-state";
import {useAuthState} from "react-firebase-hooks/auth";
import {auth} from "../../react-shared-module/logic/shared-data/firebase.utils";
import {ComakerCentralUserDataModel} from "../../../data/data-model/user-data/comaker-central-user-data.model";
import {
    UserSubscriptionPlanModel,
    UserSubscriptionPlanType
} from "../../../data/data-model/subscription/UserSubscriptionPlan.model";
import {useRouter} from "next/router";
import {useState} from "react";
import SubscriptionPlanButtonComponent from "../subscription/shared/SubscriptionPlanButton.component";

export default function CoMakerPricingTablePlansComponent() {
    const [currentUser, isAuthLoading] = useAuthState(auth);
    const [centralUserData, setCentralUserData] = useCentralUserDataState();
    const [billingPeriod, setBillingPeriod] = useState<"MONTHLY" | "YEARLY">("MONTHLY");
    const [alertMessage, setAlertMessage] = useAlertMessageGlobalState();

    const route = useRouter();

    let userSubPlan: UserSubscriptionPlanModel | undefined = centralUserData ? (centralUserData as ComakerCentralUserDataModel).subscriptionPlan : undefined;


    function renderFreePlan() {
        const planContent: PricingTablePlanContentModel = StaticPricingPlansContent.freePlan;
        let displayTable: boolean = true;
        let isCurrentPlan: boolean = false;

        if (userSubPlan) {
            if (userSubPlan.planType === UserSubscriptionPlanType.FREE) {
                isCurrentPlan = true;
            } else {
                displayTable = false;
            }
        }


        if (displayTable) {
            return (
                <PricingTableComponent planTableContent={planContent}
                                       isCurrentPlan={isCurrentPlan}
                                       billingPeriod={billingPeriod}
                                       buttonComponent={<SubscriptionPlanButtonComponent planType={UserSubscriptionPlanType.FREE}/>}/>
            );
        } else {
            return (<></>);
        }
    }

    function renderHobbyPlan() {
        const planContent: PricingTablePlanContentModel = StaticPricingPlansContent.hobbyPlan;
        let displayTable: boolean = true;
        let isCurrentPlan: boolean = false;


        // Only display this plan if not logged or is the current plan
        if (userSubPlan) {
            if (userSubPlan.planType === UserSubscriptionPlanType.SUBSCRIBED_HOBBY) {
                isCurrentPlan = true;
            } else if (userSubPlan.planType === UserSubscriptionPlanType.SUBSCRIBED_PRO) {
                displayTable = false;
            }
        }


        if (displayTable) {
            return (
                <PricingTableComponent planTableContent={planContent}
                                       isCurrentPlan={isCurrentPlan}
                                       billingPeriod={billingPeriod}
                                       buttonComponent={<SubscriptionPlanButtonComponent planType={UserSubscriptionPlanType.SUBSCRIBED_HOBBY}/>}/>
            );
        } else {
            return (<></>);
        }
    }

    function renderProPlan() {
        const planContent: PricingTablePlanContentModel = StaticPricingPlansContent.proPlan;
        let displayTable: boolean = true;
        let isCurrentPlan: boolean = false;

        // Only display this plan if not logged or is the current plan
        if (userSubPlan && userSubPlan.planType === UserSubscriptionPlanType.SUBSCRIBED_PRO) {
            isCurrentPlan = true;
        }

        if (displayTable) {
            return (
                <PricingTableComponent planTableContent={planContent}
                                       isCurrentPlan={isCurrentPlan}
                                       billingPeriod={billingPeriod}
                                       buttonComponent={<SubscriptionPlanButtonComponent planType={UserSubscriptionPlanType.SUBSCRIBED_PRO}/>}/>
            );
        } else {
            return (<></>);
        }
    }


    return (
        <div className='flex flex-wrap sm:flex-grow justify-evenly gap-2 w-full'>
            {renderFreePlan()}
            {renderHobbyPlan()}
            {renderProPlan()}
        </div>
    )
}