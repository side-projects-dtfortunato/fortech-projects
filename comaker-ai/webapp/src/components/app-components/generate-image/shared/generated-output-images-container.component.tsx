import {
    GenerateImageBasePromptModel
} from "../../../../data/data-model/generate-image/generate-image-base-prompt.model";
import ImageListItemViewerComponent from "../../shared/ImageListItemViewer.component";
import EmptyListMessageComponent from "../../../react-shared-module/ui-components/shared/EmptyListMessage.component";
import Image from "next/image";
import SpinnerComponent from "../../../react-shared-module/ui-components/shared/Spinner.component";

export default function GeneratedOutputImagesContainerComponent(props: {generatedImageContent?: GenerateImageBasePromptModel, isLoading: boolean}) {

    function renderImages() {
        if (props.generatedImageContent) {
            let renderImageItems = props.generatedImageContent.storedImages?.map((imageItem) => {
                return (<ImageListItemViewerComponent key={imageItem.fileUrl} imageUrl={imageItem.fileUrl} imageSize={props.generatedImageContent!.aiInput.imageSize} />);
            });

            return (
                <div className='flex flex-wrap gap-3'>
                    {renderImageItems}
                </div>
            )
        } else {
            return (<></>);
        }

    }

    if (props.isLoading) {
        return (
            <div className='w-full h-full flex justify-center items-center'>
                <SpinnerComponent message={"We are generating your image. Please, wait a moment..."} />
            </div>
        )
    } else if (!props.generatedImageContent || !props.generatedImageContent.storedImages) {
        // Display an empty screen
        return (
            <div className='w-full h-full flex justify-center items-center'>
                <EmptyListMessageComponent message={"No images yet"}
                                           icon={(<Image src={"/images/ic_illust_waiting.png"} width={300} height={300} alt={"Waiting to generated"} />)} />
            </div>
        )
    } else {
        return (
            <div className='w-full h-full flex flex-col gap-5 justify-start items-center'>
                <h2 className='custom text-center'>Rendered Images</h2>
                {renderImages()}
            </div>
        )
    }
}