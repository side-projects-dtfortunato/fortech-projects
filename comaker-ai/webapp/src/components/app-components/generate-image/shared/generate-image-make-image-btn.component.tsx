import {
    GeneratedImageSize,
    GenerateImageBasePromptModel
} from "../../../../data/data-model/generate-image/generate-image-base-prompt.model";
import DropdownFormComponent from "../../../react-shared-module/ui-components/form/DropdownForm.component";
import CommonUtils from "../../../react-shared-module/logic/commonutils";
import {OpenAIManager} from "../../../../managers/open-ai/OpenAI.manager";
import {useAuthState} from "react-firebase-hooks/auth";
import {auth} from "../../../react-shared-module/logic/shared-data/firebase.utils";
import AuthManager from "../../../react-shared-module/logic/auth/auth.manager";
import SignupLoginCallToActionContainerComponent
    from "../../../react-shared-module/ui-components/shared/SignupLoginCallToActionContainer.component";
import {useState} from "react";
import {CreditLabel} from "../../../../data/static-data/pricing-plans/static-pricing-plans.content";
import SpinnerComponent from "../../../react-shared-module/ui-components/shared/Spinner.component";

const NumberResultsOptions = ["1", "2", "3", "4", "5"];

export default function GenerateImageMakeImageBtnComponent(props: {generateImagePrompt: GenerateImageBasePromptModel, disabled?: boolean, setGeneratedImagePrompt: (generateImagePrompt: GenerateImageBasePromptModel) => void, onGenerateImageClicked: (generateImagePrompt: GenerateImageBasePromptModel) => Promise<void>}) {
    const [currentUser, isAuthLoading] = useAuthState(auth);
    const [isLoading, setIsLoading] = useState(false);

    function onNumberResultsChanged(value: string) {
        let numResults: number = Number(value);
        props.setGeneratedImagePrompt({
            ...props.generateImagePrompt,
            creditsCost: OpenAIManager.calculateImageGenerationCost(props.generateImagePrompt.aiInput.imageSize, numResults),
            aiInput: {
                ...props.generateImagePrompt.aiInput,
                numberResults: numResults,
            },
        });
    }

    function onImageSizeChanged(value: string) {
        props.setGeneratedImagePrompt({
            ...props.generateImagePrompt,
            creditsCost: OpenAIManager.calculateImageGenerationCost(value as GeneratedImageSize, props.generateImagePrompt.aiInput.numberResults),
            aiInput: {
                ...props.generateImagePrompt.aiInput,
                imageSize: value as GeneratedImageSize,
            },
        });
    }

    async function onGenerateImageClicked() {
        setIsLoading(true);
        await props.onGenerateImageClicked(props.generateImagePrompt);
        setIsLoading(false);
    }

    function renderGenerateImageButton() {
        if (isAuthLoading) {
            return (<></>);
        } else if (!AuthManager.isUserLogged()) {
            return (<SignupLoginCallToActionContainerComponent />);
        } else {
            return (
                <div className='flex flex-col w-full mt-5'>
                    <button className="btn btn-primary text-white" disabled={props.disabled || isLoading} onClick={onGenerateImageClicked}>Generate Image ({props.generateImagePrompt.creditsCost} {CreditLabel})</button>
                </div>
            )
        }
    }

    return (
        <div className='flex flex-col gap-3 w-full'>
            <DropdownFormComponent label={"Number of generated images"} defaultLabel={"Select the number of generations"}
                                   options={CommonUtils.convertArrayToDropdownOptions(NumberResultsOptions)}
                                   currentSelectionKey={props.generateImagePrompt.aiInput.numberResults.toString()}
                                   disabled={props.disabled}
                                   onOptionSelected={onNumberResultsChanged} />

            <DropdownFormComponent label={"Image Size"} defaultLabel={"Select the image size"}
                                   options={CommonUtils.convertArrayToDropdownOptions(Object.values(GeneratedImageSize))}
                                   currentSelectionKey={props.generateImagePrompt.aiInput.imageSize}
                                   disabled={props.disabled}
                                   onOptionSelected={onImageSizeChanged} />

            {isLoading ? <SpinnerComponent /> : <></> }

            {renderGenerateImageButton()}
        </div>
    )
}