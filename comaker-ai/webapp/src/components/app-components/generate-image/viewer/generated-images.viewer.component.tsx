import {
    GenerateImageBasePromptModel
} from "../../../../data/data-model/generate-image/generate-image-base-prompt.model";
import GeneratedOutputImagesContainerComponent from "../shared/generated-output-images-container.component";

export default function GeneratedImagesViewerComponent(props: {item: GenerateImageBasePromptModel}) {

    return (
        <div className='flex flex-col w-full list-item-bg drop-shadow p-5 pb-10'>
            <GeneratedOutputImagesContainerComponent isLoading={false} generatedImageContent={props.item} />
        </div>
    )
}