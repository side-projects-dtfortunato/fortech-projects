import NotFoundContentComponent from "../../react-shared-module/ui-components/shared/NotFoundContent.component";
import {getLanguageLabel} from "../../react-shared-module/logic/language/language.helper";
import {
    GenerateImageBasePromptModel,
    GenerationImageContentType
} from "../../../data/data-model/generate-image/generate-image-base-prompt.model";
import GenerateBasicImageContentComponent from "./content-types/GenerateBasicImageContent.component";
import PageHeaderTitleComponent from "../shared/PageHeaderTitle.component";

export default function GenerateImageContentContainerComponent(props: {imageContentType: string, generatedImage?: GenerateImageBasePromptModel}) {

    function renderGenerateContentForm() {
        switch (props.imageContentType as GenerationImageContentType) {
            case GenerationImageContentType.CREATE_IMAGE:
                return (<GenerateBasicImageContentComponent generatedImage={props.generatedImage} />);
        }
        return (<NotFoundContentComponent />);
    }

    function renderGeneratorHeader() {
        return (
            <PageHeaderTitleComponent title={getLanguageLabel("GENERATE_PAGE_TITLE_" + props.imageContentType)} subtitle={getLanguageLabel("GENERATE_PAGE_TAGLINE_" + props.imageContentType)} displayAvatar={true} />
        )
    }

    return (
        <div className='flex flex-col p-3 w-full'>
            {renderGeneratorHeader()}
            <div className='flex list-item-bg drop-shadow w-full mt-5 p-3'>
                {renderGenerateContentForm()}
            </div>

        </div>
    )
}