import {useState} from "react";
import PageHeaderTitleComponent from "../../shared/PageHeaderTitle.component";
import InputTextAreaComponent from "../../../react-shared-module/ui-components/form/InputTextArea.component";
import DropdownFormComponent from "../../../react-shared-module/ui-components/form/DropdownForm.component";
import CommonUtils from "../../../react-shared-module/logic/commonutils";
import {useForm} from "react-hook-form";

const ImageTypes = ["Photograph", "Digital art illustration", "Artwork", "3D Render", "Pattern", "Abstract"];
const StyleTypes = ["Realistic", "Minimalist", "Grunge", "Vintage", "Futuristic", "Geometric", "Organic", "Watercolor", "Sketch", "Pop Art", "Pixel Art", "Neon", "Low Poly", "Isometric", "Hand-drawn", "Graffiti", "Flat", "Collage", "Cartoon", "Art Deco", "Art Nouveau"];


interface FormData {
    description: string,
    imageType?: string,
    imageStyle?: string,
}

export default function GenerateImageBaseFormComponent(props: {onGeneratedPrompt: (prompt: string) => void, disabled?: boolean}) {
    const [formData, setFormData] = useState<FormData>({description: "", imageStyle: StyleTypes[0], imageType: ImageTypes[0]});

    function onFormFieldChanged(formKey: string, value: string) {
        let updatedData: FormData = {
            ...formData,
            [formKey]: value,
        };
        setFormData(updatedData);
        props.onGeneratedPrompt(convertFormDataToPromptText(updatedData));
    }

    return (
        <div className='flex flex-col gap-4 w-full'>
            <PageHeaderTitleComponent title={"Image Generation Form"} subtitle={"Set the infos about the image you want to make."} displayAvatar={false} />
            <div className='divider'></div>
            <InputTextAreaComponent topLabel={"Describe your desired image"} hint={"a green mountain in a sunny day with a car riding along the mountain."}
                                    disabled={props.disabled}
                                    onChanged={(text) => {onFormFieldChanged("description", text)}} />

            <DropdownFormComponent label={"Image Type"} defaultLabel={"Select an image type"}
                                   disabled={props.disabled}
                                   currentSelectionKey={formData.imageType}
                                   options={CommonUtils.convertArrayToDropdownOptions(ImageTypes)} onOptionSelected={(option) => {onFormFieldChanged("imageType", option)}} />

            <DropdownFormComponent label={"Drawing Style"} defaultLabel={"Select the drawing style"}
                                   currentSelectionKey={formData.imageStyle}
                                   disabled={props.disabled}
                                   options={CommonUtils.convertArrayToDropdownOptions(StyleTypes)} onOptionSelected={(option) => {onFormFieldChanged("imageStyle", option)}} />
        </div>
    )
}

function convertFormDataToPromptText(formData: FormData) {
    return "Create a " + formData.imageType + " with a " + formData.imageStyle + " style of a " + formData.description;
}
