import {useState} from "react";
import {
    GenerateImageBasePromptModel,
    GenerationImageContentType
} from "../../../../data/data-model/generate-image/generate-image-base-prompt.model";
import GeneratedOutputImagesContainerComponent from "../shared/generated-output-images-container.component";
import GenerateImageBaseFormComponent from "../forms/generate-image-base-form.component";
import GenerateImageMakeImageBtnComponent from "../shared/generate-image-make-image-btn.component";
import {
    RootGlobalStateActions,
    useAlertMessageGlobalState
} from "../../../react-shared-module/logic/global-hooks/root-global-state";
import {ComakerBackendApi} from "../../../../data/api/comaker-backend.api";
import {OpenAIManager} from "../../../../managers/open-ai/OpenAI.manager";
import {
    initGenerateImageBaseData
} from "../../../../data/static-data/static-content/static-generate-content-list.content";

export default function GenerateBasicImageContentComponent(props: {generatedImage?: GenerateImageBasePromptModel}) {
    const [generateImagePrompt, setGenerateImagePrompt] = useState<GenerateImageBasePromptModel>(props.generatedImage ? props.generatedImage : initGenerateImageBaseData(GenerationImageContentType.CREATE_IMAGE));
    const [isLoading, setIsLoading] = useState(false);
    const [displayAlertMessage, setDisplayAlertMessage] = useAlertMessageGlobalState();

    function hasGeneratedData(): boolean {
        return (generateImagePrompt.response !== undefined && generateImagePrompt.response.responsesUrls != undefined);
    }

    function onPromptChanged(prompt: string) {
        setGenerateImagePrompt({
            ...generateImagePrompt,
            aiInput: {
                ...generateImagePrompt.aiInput,
                promptText: prompt,
            },
        });
    }

    async function onGenerateImageClicked() {
        // handle api request
        setIsLoading(true);
        const apiResponse = await OpenAIManager.generateImage(generateImagePrompt);
        if (apiResponse.isSuccess) {
            setGenerateImagePrompt({
                ...apiResponse.responseData as GenerateImageBasePromptModel,
            });
        } else {
            RootGlobalStateActions.displayAPIResponseError({
                apiResponse: apiResponse,
                setAlertMessageState: setDisplayAlertMessage,
            });
        }
        setIsLoading(false);
    }

    function renderInputContainer() {
        return (
            <div className='flex flex-col basis-1/2 justify-center items-center h-full w-full'>
                <GenerateImageBaseFormComponent onGeneratedPrompt={onPromptChanged} disabled={isLoading || hasGeneratedData()} />
                <GenerateImageMakeImageBtnComponent generateImagePrompt={generateImagePrompt}
                                                    setGeneratedImagePrompt={setGenerateImagePrompt}
                                                    onGenerateImageClicked={onGenerateImageClicked}
                                                    disabled={isLoading || hasGeneratedData()}
                                                    />
            </div>
        );
    }


    return (
        <div className='flex flex-col lg:flex-row gap-2 w-full items-start'>
            {renderInputContainer()}

            <div className='divider lg:divider-horizontal text-slate-400 mx-8'>OUTPUT</div>

            <div className='flex basis-1/2 justify-center items-center h-full w-full'>
                <GeneratedOutputImagesContainerComponent isLoading={isLoading} generatedImageContent={generateImagePrompt} />
            </div>
        </div>
    )
}