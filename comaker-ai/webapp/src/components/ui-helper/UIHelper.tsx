import Link from "next/link";
import NavbarUserLinkComponent from "../react-shared-module/ui-components/shared/NavbarUserLink.component";
import {BasePostDataModel} from "../react-shared-module/logic/shared-data/datamodel/base-post-data.model";
import {FormOption} from "../react-shared-module/ui-components/form/RadioButton.component";
import {NavbarLinkItem} from "../react-shared-module/ui-components/root/Navbar.component";
import AuthManager from "../react-shared-module/logic/auth/auth.manager";
import {getLoginPageLink} from "../../pages/login";
import {getUserProfileLink} from "../../pages/edit-profile/user-profile";
import {FooterLinkItem} from "../react-shared-module/ui-components/root/Footer.component";
import {
    SharedLandingPageAggregatorsModel
} from "../react-shared-module/logic/shared-data/datamodel/shared-landing-page-aggregators.model";
import {
    SharedCentralUserDataModel
} from "../react-shared-module/logic/shared-data/datamodel/shared-central-user-data.model";
import SideLeftMenuComponent from "../app-components/root/menu/side-left-menu.component";
import {getPricingPageUrl} from "../../pages/pricing";
import {getLanguageLabel} from "../react-shared-module/logic/language/language.helper";
import SideMenuItemComponent from "../app-components/root/menu/shared/side-menu-item.component";
import {NextRouter} from "next/router";
import {
    StaticGenerateContentListContent
} from "../../data/static-data/static-content/static-generate-content-list.content";
import {getGenerateContentTypePageURL} from "../../pages/generate/[contentType]";
import {GenerationTextContentType} from "../../data/data-model/generate-text-content/generated-content-base.model";
import {PostTypes} from "../react-shared-module/utils/shared-ui-helper";

export const CATEGORIES_OPTIONS: { [optionId: string]: FormOption } = {
}


export enum LandingPageTabTypes {
    HOME = "HOME",
}


const ADS_LIST_INDEX = [2, 7, 12, 16];

export class UIHelper {
    static renderNavbarCenterItems() {
        return (
            <div className="navbar-center hidden md:flex grow justify-content-center align-items-center">
                <ul className="menu menu-horizontal p-0">
                    {
                        <li><Link href={getPricingPageUrl()}>{getLanguageLabel("NAVBAR_PRICING_BTN")}</Link></li>
                        /*
                        <li><Link href="/">Link #1 <span className="badge badge-xs">Beta</span></Link></li>
                        <li><Link href="/">Link #2 <span className="badge badge-xs badge-outline">Hiring</span></Link></li>
                         */
                    }
                </ul>
            </div>
        )
    }

    static renderNavbarRightItems() {
        // <div className="hidden lg:flex flex-none">
        return (
            <div className="flex flex-none">
                <ul className="hidden sm:flex menu menu-horizontal p-0 mr-2">
                    {/*More Items*/}
                    {this.renderNavbarLink(StaticGenerateContentListContent.writeDocument.label, getGenerateContentTypePageURL(GenerationTextContentType.WRITE_DOCUMENT_ASSIST))}
                </ul>
                <NavbarUserLinkComponent />
            </div>
        );
    }

    static renderNavbarUserLinks() {
        return (
            <>
                <li key="Edit profile"><Link href={getUserProfileLink()}>Edit your profile</Link></li>
                <li key="Logout"><Link href={"/"} onClick={async () => {
                    await AuthManager.signOut();
                }}>Logout</Link></li>
            </>
        )
    }

    static renderNavbarLink(label: string, href: string) {
        return (<li key='label'><Link href={href}>{label}</Link></li>)
    }

    static renderPostListItem(postBaseData: BasePostDataModel, index: number, hideStats?: boolean) {
        return (<></>)
    }

    static renderLeftNavbar(props: {customLinks?: NavbarLinkItem[]}) {
        let listLinks: NavbarLinkItem[] = [];

        // Add menu links
        listLinks = listLinks.concat(this.getMenuLinkItems({customLinks: props.customLinks}));

        function renderLinks() {
            if (listLinks) {
                return listLinks.map((linkItem) => {
                    return (
                        <li key={linkItem.linkUrl}><Link href={linkItem.linkUrl} onClick={linkItem.onClick}>{linkItem.label}</Link></li>
                    )
                });
            }
        }

        return (
            <div key={"side-menu"} className="flex md:hidden dropdown h-full">
                <label tabIndex={0} className="btn btn-ghost btn-circle">
                    <svg xmlns="http://www.w3.org/2000/svg" className="h-5 w-5" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                        <path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M4 6h16M4 12h16M4 18h7"/>
                    </svg>
                </label>
                <ul tabIndex={0} className="menu menu-compact dropdown-content mt-10 p-2 shadow bg-base-100 rounded-box custom-dropdown-menu">
                    <SideLeftMenuComponent>
                        <SideMenuItemComponent path={getPricingPageUrl()} label={getLanguageLabel("NAVBAR_PRICING_BTN")} autoSelect={true} />
                    </SideLeftMenuComponent>
                </ul>
            </div>
        )
    }
    static getMenuLinkItems(props: {customLinks?: NavbarLinkItem[]}): NavbarLinkItem[] {
        let listItems: NavbarLinkItem[] = [];

        if (props.customLinks) {
            listItems = listItems.concat(props.customLinks);
        }

        if (AuthManager.isUserLogged()) {
            listItems.push({linkUrl: "/", label: "Logout", onClick: () => AuthManager.signOut()});
        } else {
            listItems.push({linkUrl: getLoginPageLink(), label: "Login/Signup"});
        }

        return listItems;
    }
    static getPostCategoryLabel(categoryId: string): string {
        return CATEGORIES_OPTIONS[categoryId] ? CATEGORIES_OPTIONS[categoryId].label : "";
    }

    static getFooterLinks(): FooterLinkItem[] {
        let listFooterLinks: FooterLinkItem[] = [];

        // Navigation
        listFooterLinks.push({
            categoryId: "Navigation",
            categoryLabel: "Navigation",
            label: "Plans & Pricing",
            linkUrl: getPricingPageUrl(),
        });
        listFooterLinks.push({
            categoryId: "Navigation",
            categoryLabel: "Navigation",
            label: "Login/Signup",
            linkUrl: getLoginPageLink(),
        });
        listFooterLinks.push({
            categoryId: "Navigation",
            categoryLabel: "Navigation",
            label: "FAQ",
            linkUrl: "/faq-support",
        });

        // Contacts
        listFooterLinks.push({
            categoryId: "Contacts",
            categoryLabel: "Contacts",
            label: "contact@comaker.ai",
            linkUrl: "mailto:contact@comaker.ai",
        });
        listFooterLinks.push({
            categoryId: "Contacts",
            categoryLabel: "Contacts",
            label: "Chat",
            linkUrl: "https://tawk.to/chat/641c14ec4247f20fefe7860c/1gs6r3fbo",
        });

        return listFooterLinks;
    }

    static getLandingPagePostTypes() {
        return [];
    }


    static getLandingPageTabListPosts(tabSelection: LandingPageTabTypes, landingDataAgg: SharedLandingPageAggregatorsModel, centralUserData?: SharedCentralUserDataModel | undefined | null) {
        let listPosts: BasePostDataModel[] = [];
        return listPosts;
    }
    static getFollowingPosts(listPosts?: BasePostDataModel[], centralUserData?: SharedCentralUserDataModel | undefined | null) {
        if (listPosts && centralUserData && centralUserData.usersFollowing) {
            return listPosts.filter((post) => {
                return Object.keys(centralUserData.usersFollowing!).includes(post.userCreatorId);
            });
        } else {
            return [];
        }
    }


    static getPostFormTypeLabel(postType: PostTypes) {
        return "";
    }

    static onUserCreation(params: {router: NextRouter, goAutoBack?: boolean, centralUserData?: SharedCentralUserDataModel}) {
        if (params.goAutoBack) {
            params.router.back();
        }

    }

}