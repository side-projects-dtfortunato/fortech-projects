

export const CustomLanguageLabels: {[labelId: string]: string} = {
    LANDING_PAGE_HEADER_JOIN_COMMUNITY_TITLE: "Join our community of Side Hustlers",
    LANDING_PAGE_HEADER_JOIN_COMMUNITY_SUBTITLE: "To discover, share or discuss side hustles ideas and results. Also, share your own side hustles with our community of Side Hustlers",
    GENERATE_OUTPUT_EMPTY: "Waiting for the generated content",
    GENERATE_FORM_TONE_STYLE_LABEL: "Content tone style",
    GENERATE_FORM_TONE_STYLE_TYPE_ENTHUSIASTIC: "Enthusiastic",
    GENERATE_FORM_TONE_STYLE_TYPE_HUMOROUS: "Humorous",
    GENERATE_FORM_TONE_STYLE_TYPE_PROFESSIONAL: "Professional",
    GENERATE_FORM_TONE_STYLE_TYPE_CONVERSATIONAL: "Conversational",
    GENERATE_FORM_POINT_OF_VIEW_LABEL: "Content written in this point of view",
    GENERATE_AI_OUTPUT_GENERATED_TITLE: "Output:",
    FORM_SAVE_CHANGES: "Save Changes",
    FORM_RESET_CHANGES: "Reset Changes",
    GENERATE_ITEM_TYPE_LABEL_BLOG_POST: "Blog Post",
    GENERATE_ITEM_TYPE_LABEL_INSTAGRAM_POST: "Instagram Post",
    GENERATE_ITEM_TYPE_LABEL_CREATE_IMAGE: "Image",
    NAVBAR_PRICING_BTN: "Plans & Pricing",
    LANDING_PAGE_TOP_TITLE: "Create your web content",
    LANDING_PAGE_TOP_SUBTITLE: "10X faster with CoMaker.AI",
    LANDING_PAGE_TOP_DESCRIPTION: "Simply input a few keywords, and CoMaker.ai's cutting-edge AI, boasting extensive global knowledge, will instantly generate comprehensive articles and compelling marketing content tailored to your needs.",


    /** Generate Pages titles */
    GENERATE_PAGE_TITLE_BLOG_POST: "Write a Blog Post With AI",
    GENERATE_PAGE_TAGLINE_BLOG_POST: "Turn a title and outline into a long and engaging article.",
    GENERATE_PAGE_TITLE_CREATE_IMAGE: "Generate an image",
    GENERATE_PAGE_TAGLINE_CREATE_IMAGE: "Create an image based on your description.",

}

export function getCustomLanguageLabel(labelId: string): string | undefined {
    if (Object.keys(CustomLanguageLabels).includes(labelId)) {
        // @ts-ignore
        return CustomLanguageLabels[labelId]! as string;
    } else {
        return undefined;
    }
}