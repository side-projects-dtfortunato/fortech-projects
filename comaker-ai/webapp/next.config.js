/** @type {import('next').NextConfig} */
const withPWA = require('next-pwa')({
  dest: 'public'
})

module.exports = withPWA({
  // next.js config
  reactStrictMode: true,
  swcMinify: true,
  images: {
    domains: ['firebasestorage.googleapis.com', 'lh3.googleusercontent.com', 'ik.imagekit.io'],
  },
  webpack: (config, { buildId, dev, isServer }) => {
    config.resolve.symlinks = false;
    return config
  },
  env: {
    DEV_OPENAI_API_KEY: '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',
    PROD_OPENAI_API_KEY: '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',
  },
})