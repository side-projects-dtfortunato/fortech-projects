
export default class RootConfigs {
    static ENVIRONMENT_DEV = false;
    static BASE_URL = "https://www.remoteitjobs.app";
    static SITE_NAME = "RemoteITJobs.app";
    static SITE_TITLE = "Remote IT Jobs | Find Remote Tech Jobs Worldwide";
    static SITE_DESCRIPTION = "Discover top remote IT jobs from leading tech companies. Search software development, DevOps, cybersecurity, and tech leadership positions. Apply to work-from-home tech jobs today.";
    static CURRENT_NICHE_SITE = RootConfigs.SITE_NAME;
    static CONTACT_EMAIL = "contact@remoteitjobs.app";
    static SITE_TOPIC = "IT Jobs";
    static THEME = {
        PRIMARY_COLOR: "zinc",
    }
    static META_CONFIGS: any = {
        updateNicheBackend: false,
        disableGoogleSignin: true,
        disableCookies: true,
        disableAppStoreBanner: false,
        disableGoogleAds: false,
        enableJobSearch: true,
    }
    static APP_STORE_LINKS = {
        androidAppId: "remoteitjobs.app",
        iosAppId: "6740095012",
    }
    static SOCIAL_NETWORKS: {
        [socialId: string]: {
            iconUrl: string,
            label: string,
            link: string,
        },
    } = {
        /*facebook: {
            label: "Facebook",
            link: "https://www.facebook.com/profile.php?id=61565899179933",
            iconUrl: "/images/ic_social_facebook.png",
        },
        instagram: {
            label: "Instagram",
            link: "https://www.instagram.com/customer_remote_jobs/",
            iconUrl: "/images/ic_social_instagram.png",
        },
        threads: {
            label: "Threads",
            link: "https://www.threads.net/@customer_remote_jobs",
            iconUrl: "/images/ic_social_threads.png",
        }, */
        bluesky: {
            label: "Bluesky",
            link: "https://remoteitjobs.bsky.social",
            iconUrl: "/images/ic_social_bluesky.png",
        },
        telegram: {
            label: "Telegram Community",
            link: "https://t.me/remoteitjobs_app",
            iconUrl: "/images/ic_social_threads.png",
        },
    }
    static FIREBASE_CONFIG = this.ENVIRONMENT_DEV ?

        // DEV Config - // TODO Change it
        {
            apiKey: "AIzaSyCB2yrNkEHbpShTH_xwHOzcl2mXJT5i1K8",
            authDomain: "fortuly-qa-env.firebaseapp.com",
            projectId: "fortuly-qa-env",
            storageBucket: "fortuly-qa-env.appspot.com",
            messagingSenderId: "650128691290",
            appId: "1:650128691290:web:69f8a283090d23e0454863",
            measurementId: "G-V6XN6K51VZ"
        } :
        // PROD
        {
            apiKey: "AIzaSyD8Q3Rbxkj5GUyiwKE8gfcR67ReYB5Z0EU",
            authDomain: "remoteitjobs-app.firebaseapp.com",
            projectId: "remoteitjobs-app",
            storageBucket: "remoteitjobs-app.firebasestorage.app",
            messagingSenderId: "161070765414",
            appId: "1:161070765414:web:ed9c8ec04a3da7b64764a7",
            measurementId: "G-2V3WFZRVGD"
        };


    static IMAGEKIT_API = {
        publicKey : "public_OurCtnmj0jWqjVslk5YDfKE4aHQ=",
        privateKey : "private_vRUtPf6HGOj68f5rZZN8u9RHfiQ=",
        urlEndpoint : "https://ik.imagekit.io/fortulyapps",
    }

    static ONE_SIGNAL_API = "9115267d-60d4-4236-9437-fc821933cd07";
}
