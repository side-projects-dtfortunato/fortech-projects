/** @type {import('next-sitemap').IConfig} */
module.exports = {
    siteUrl: process.env.SITE_URL || 'https://www.remoteitjobs.app',
    generateRobotsTxt: true,
    exclude: ["/job/job-post-submission", "/login", "/login/recover-password", "/edit-post/*", "/edit-profile/user-profile", "/bookmarks", "/privacy-policy", "/profile/user-profile", "/eula"],
}