import {
    JobDetailsModel,
    LinkedinSourceModel
} from "../../shareable-firebase-backend/base-projects/jobs-discover/shared-jobs-discover.data";


/**
 * Regions ids:
 * - UK: 101165590
 * - Australia: 101452733
 * - USA: 103644278
 * - EU: 91000002
 */
/*export const LINKEDIN_SOURCES: LinkedinSourceModel[] = [

    {
        id: "1",
        category: "Software Engineer",
        region: "USA",
        url: "https://www.linkedin.com/jobs/search?keywords=Software Engineer&geoId=103644278&f_TPR=&f_WT=2&f_E=2%2C3%2C4%2C5&position=1&pageNum=0",
    },
    {
        id: "2",
        category: "Backend Developer",
        region: "USA",
        url: "https://www.linkedin.com/jobs/search?keywords=Backend Developer&geoId=103644278&f_TPR=&f_WT=2&f_E=2%2C3%2C4%2C5&position=1&pageNum=0",
    },
    {
        id: "3",
        category: "Frontend Web Developer",
        region: "USA",
        url: "https://www.linkedin.com/jobs/search?keywords=Frontend Web Developer&geoId=103644278&f_TPR=&f_WT=2&f_E=2%2C3%2C4%2C5&position=1&pageNum=0",
    },
    {
        id: "4",
        category: "Android Developer",
        region: "USA",
        url: "https://www.linkedin.com/jobs/search?keywords=Android Developer&geoId=103644278&f_TPR=&f_WT=2&f_E=2%2C3%2C4%2C5&position=1&pageNum=0",
    },
    {
        id: "5",
        category: "iOS Developer",
        region: "USA",
        url: "https://www.linkedin.com/jobs/search?keywords=iOS Developer&geoId=103644278&f_TPR=&f_WT=2&f_E=2%2C3%2C4%2C5&position=1&pageNum=0",
    },
    {
        id: "6",
        category: "Data Engineer",
        region: "USA",
        url: "https://www.linkedin.com/jobs/search?keywords=Data Engineer&geoId=103644278&f_TPR=&f_WT=2&f_E=2%2C3%2C4%2C5&position=1&pageNum=0",
    },
    {
        id: "7",
        category: "Engineering Manager",
        region: "USA",
        url: "https://www.linkedin.com/jobs/search?keywords=Engineering Manager&geoId=103644278&f_TPR=&f_WT=2&f_E=2%2C3%2C4%2C5&position=1&pageNum=0",
    },
    {
        id: "8",
        category: "Project Manager",
        region: "USA",
        url: "https://www.linkedin.com/jobs/search?keywords=Project Manager&geoId=103644278&f_TPR=&f_WT=2&f_E=2%2C3%2C4%2C5&position=1&pageNum=0",
    },
    {
        id: "9",
        category: "Product Manager",
        region: "USA",
        url: "https://www.linkedin.com/jobs/search?keywords=Product Manager&geoId=103644278&f_TPR=&f_WT=2&f_E=2%2C3%2C4%2C5&position=1&pageNum=0",
    },
    {
        id: "10",
        category: "Product Owner",
        region: "USA",
        url: "https://www.linkedin.com/jobs/search?keywords=Product Owner&geoId=103644278&f_TPR=&f_WT=2&f_E=2%2C3%2C4%2C5&position=1&pageNum=0",
    },

    // UK
    {
        id: "11",
        category: "Software Engineer",
        region: "UK",
        url: "https://www.linkedin.com/jobs/search?keywords=Software Engineer&geoId=101165590&f_TPR=&f_WT=2&f_E=2%2C3%2C4%2C5&position=1&pageNum=0",
    },
    {
        id: "12",
        category: "Backend Developer",
        region: "UK",
        url: "https://www.linkedin.com/jobs/search?keywords=Backend Developer&geoId=101165590&f_TPR=&f_WT=2&f_E=2%2C3%2C4%2C5&position=1&pageNum=0",
    },
    {
        id: "13",
        category: "Frontend Web Developer",
        region: "UK",
        url: "https://www.linkedin.com/jobs/search?keywords=Frontend Web Developer&geoId=101165590&f_TPR=&f_WT=2&f_E=2%2C3%2C4%2C5&position=1&pageNum=0",
    },
    {
        id: "14",
        category: "Android Developer",
        region: "UK",
        url: "https://www.linkedin.com/jobs/search?keywords=Android Developer&geoId=101165590&f_TPR=&f_WT=2&f_E=2%2C3%2C4%2C5&position=1&pageNum=0",
    },
    {
        id: "15",
        category: "iOS Developer",
        region: "UK",
        url: "https://www.linkedin.com/jobs/search?keywords=iOS Developer&geoId=101165590&f_TPR=&f_WT=2&f_E=2%2C3%2C4%2C5&position=1&pageNum=0",
    },
    {
        id: "16",
        category: "Data Engineer",
        region: "UK",
        url: "https://www.linkedin.com/jobs/search?keywords=Data Engineer&geoId=101165590&f_TPR=&f_WT=2&f_E=2%2C3%2C4%2C5&position=1&pageNum=0",
    },
    {
        id: "17",
        category: "Engineering Manager",
        region: "UK",
        url: "https://www.linkedin.com/jobs/search?keywords=Engineering Manager&geoId=101165590&f_TPR=&f_WT=2&f_E=2%2C3%2C4%2C5&position=1&pageNum=0",
    },
    {
        id: "18",
        category: "Project Manager",
        region: "UK",
        url: "https://www.linkedin.com/jobs/search?keywords=Project Manager&geoId=101165590&f_TPR=&f_WT=2&f_E=2%2C3%2C4%2C5&position=1&pageNum=0",
    },
    {
        id: "19",
        category: "Product Manager",
        region: "UK",
        url: "https://www.linkedin.com/jobs/search?keywords=Product Manager&geoId=101165590&f_TPR=&f_WT=2&f_E=2%2C3%2C4%2C5&position=1&pageNum=0",
    },
    {
        id: "20",
        category: "Product Owner",
        region: "UK",
        url: "https://www.linkedin.com/jobs/search?keywords=Product Owner&geoId=101165590&f_TPR=&f_WT=2&f_E=2%2C3%2C4%2C5&position=1&pageNum=0",
    },

    // EU
    {
        id: "21",
        category: "Software Engineer",
        region: "EU",
        url: "https://www.linkedin.com/jobs/search?keywords=Software Engineer&geoId=91000002&f_TPR=&f_WT=2&f_E=2%2C3%2C4%2C5&position=1&pageNum=0",
    },
    {
        id: "22",
        category: "Backend Developer",
        region: "EU",
        url: "https://www.linkedin.com/jobs/search?keywords=Backend Developer&geoId=91000002&f_TPR=&f_WT=2&f_E=2%2C3%2C4%2C5&position=1&pageNum=0",
    },
    {
        id: "23",
        category: "Frontend Web Developer",
        region: "EU",
        url: "https://www.linkedin.com/jobs/search?keywords=Frontend Web Developer&geoId=91000002&f_TPR=&f_WT=2&f_E=2%2C3%2C4%2C5&position=1&pageNum=0",
    },
    {
        id: "24",
        category: "Android Developer",
        region: "EU",
        url: "https://www.linkedin.com/jobs/search?keywords=Android Developer&geoId=91000002&f_TPR=&f_WT=2&f_E=2%2C3%2C4%2C5&position=1&pageNum=0",
    },
    {
        id: "25",
        category: "iOS Developer",
        region: "EU",
        url: "https://www.linkedin.com/jobs/search?keywords=iOS Developer&geoId=91000002&f_TPR=&f_WT=2&f_E=2%2C3%2C4%2C5&position=1&pageNum=0",
    },
    {
        id: "26",
        category: "Data Engineer",
        region: "EU",
        url: "https://www.linkedin.com/jobs/search?keywords=Data Engineer&geoId=91000002&f_TPR=&f_WT=2&f_E=2%2C3%2C4%2C5&position=1&pageNum=0",
    },
    {
        id: "27",
        category: "Engineering Manager",
        region: "EU",
        url: "https://www.linkedin.com/jobs/search?keywords=Engineering Manager&geoId=91000002&f_TPR=&f_WT=2&f_E=2%2C3%2C4%2C5&position=1&pageNum=0",
    },
    {
        id: "28",
        category: "Project Manager",
        region: "EU",
        url: "https://www.linkedin.com/jobs/search?keywords=Project Manager&geoId=91000002&f_TPR=&f_WT=2&f_E=2%2C3%2C4%2C5&position=1&pageNum=0",
    },
    {
        id: "29",
        category: "Product Manager",
        region: "EU",
        url: "https://www.linkedin.com/jobs/search?keywords=Product Manager&geoId=91000002&f_TPR=&f_WT=2&f_E=2%2C3%2C4%2C5&position=1&pageNum=0",
    },
    {
        id: "30",
        category: "Product Owner",
        region: "EU",
        url: "https://www.linkedin.com/jobs/search?keywords=Product Owner&geoId=91000002&f_TPR=&f_WT=2&f_E=2%2C3%2C4%2C5&position=1&pageNum=0",
    },

    // Australia
    {
        id: "31",
        category: "Software Engineer",
        region: "Australia",
        url: "https://www.linkedin.com/jobs/search?keywords=Software Engineer&geoId=101452733&f_TPR=&f_WT=2&f_E=2%2C3%2C4%2C5&position=1&pageNum=0",
    },
    {
        id: "32",
        category: "Backend Developer",
        region: "Australia",
        url: "https://www.linkedin.com/jobs/search?keywords=Backend Developer&geoId=101452733&f_TPR=&f_WT=2&f_E=2%2C3%2C4%2C5&position=1&pageNum=0",
    },
    {
        id: "33",
        category: "Frontend Web Developer",
        region: "Australia",
        url: "https://www.linkedin.com/jobs/search?keywords=Frontend Web Developer&geoId=101452733&f_TPR=&f_WT=2&f_E=2%2C3%2C4%2C5&position=1&pageNum=0",
    },
    {
        id: "34",
        category: "Android Developer",
        region: "Australia",
        url: "https://www.linkedin.com/jobs/search?keywords=Android Developer&geoId=101452733&f_TPR=&f_WT=2&f_E=2%2C3%2C4%2C5&position=1&pageNum=0",
    },
    {
        id: "35",
        category: "iOS Developer",
        region: "Australia",
        url: "https://www.linkedin.com/jobs/search?keywords=iOS Developer&geoId=101452733&f_TPR=&f_WT=2&f_E=2%2C3%2C4%2C5&position=1&pageNum=0",
    },
    {
        id: "36",
        category: "Data Engineer",
        region: "Australia",
        url: "https://www.linkedin.com/jobs/search?keywords=Data Engineer&geoId=101452733&f_TPR=&f_WT=2&f_E=2%2C3%2C4%2C5&position=1&pageNum=0",
    },
    {
        id: "37",
        category: "Engineering Manager",
        region: "Australia",
        url: "https://www.linkedin.com/jobs/search?keywords=Engineering Manager&geoId=101452733&f_TPR=&f_WT=2&f_E=2%2C3%2C4%2C5&position=1&pageNum=0",
    },
    {
        id: "38",
        category: "Project Manager",
        region: "Australia",
        url: "https://www.linkedin.com/jobs/search?keywords=Project Manager&geoId=101452733&f_TPR=&f_WT=2&f_E=2%2C3%2C4%2C5&position=1&pageNum=0",
    },
    {
        id: "39",
        category: "Product Manager",
        region: "Australia",
        url: "https://www.linkedin.com/jobs/search?keywords=Product Manager&geoId=101452733&f_TPR=&f_WT=2&f_E=2%2C3%2C4%2C5&position=1&pageNum=0",
    },
    {
        id: "40",
        category: "Product Owner",
        region: "Australia",
        url: "https://www.linkedin.com/jobs/search?keywords=Product Owner&geoId=101452733&f_TPR=&f_WT=2&f_E=2%2C3%2C4%2C5&position=1&pageNum=0",
    },
]*/

export const JOB_REGIONS = [
    {
        regionLabel: "EU",
        regionId: "91000002"    
    },
    {
        regionLabel: "UK",
        regionId: "101165590"
    },
    {
        regionLabel: "Australia",
        regionId: "101452733"
    },
    {
        regionLabel: "USA",
        regionId: "103644278"
    },
] as {regionLabel: string, regionId: string}[];


export const JOB_CATEGORIES = [
    {
        jobCategoryLabel: "Software Engineer",
        jobCategoryQuery: "Software Engineer",
    }, 
    {
        jobCategoryLabel: "Backend Developer",
        jobCategoryQuery: "Backend Developer",
    }, 
    {
        jobCategoryLabel: "Frontend Web Developer",
        jobCategoryQuery: "Frontend Web Developer",
    }, 
    {
        jobCategoryLabel: "Android Developer",
        jobCategoryQuery: "Android Developer",
    }, 
    {
        jobCategoryLabel: "iOS Developer",
        jobCategoryQuery: "iOS Developer",
    },  
    {
        jobCategoryLabel: "Data Engineer",
        jobCategoryQuery: "Data Engineer",
    }, 
    {
        jobCategoryLabel: "Engineering Manager",
        jobCategoryQuery: "Engineering Manager",
    },  
    {
        jobCategoryLabel: "Project Manager",
        jobCategoryQuery: "Project Manager",
    }, 
    {
        jobCategoryLabel: "Product Manager",
        jobCategoryQuery: "Product Manager",
    },   
    {
        jobCategoryLabel: "Product Owner",
        jobCategoryQuery: "Product Owner",
    }
] as {jobCategoryLabel: string, jobCategoryQuery: string}[];


export const ALLOWED_WORK_MODE = ["REMOTE"];

export class CustomJobsDiscoverUtils {

    static importJobsFromRemoteJobsHub(): JobDetailsModel[] {
        return [];
    }

    static getIsValidRule() {
        return "";
    }

}