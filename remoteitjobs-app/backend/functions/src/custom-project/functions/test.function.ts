import * as functions from "firebase-functions";
import { OnesignalUtils } from "../../shareable-firebase-backend/utils/onesignal.utils";
import { getQuickResponse } from "../../shareable-firebase-backend/model-data/api-response.model";

export const testFunction = functions.https.onRequest(
    async (req, resp) => {

        /*await OnesignalUtils.sendPushNotificationToUserJobFilters("🚀 New Remote Job Alert!", 
            `💼 Position: Android Developer
🏢 Company: Qarbon IT
🌍 Location: EU 
📁 Category: Android Developer
💰 Salary: €40,000 - €60,000
🛠️ Skills: Android Develpment`, "https://www.remoteitjobs.app/job/qarbon-it-android-developer", {category: "Android Developer", remoteRegion: "EU"});*/
        resp.send(getQuickResponse(true, "Push Notification Sent"));
    });
