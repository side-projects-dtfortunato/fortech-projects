
export const PRODUCTION_GCLOUD_PROJECT = "remoteitjobs-app";

export const WEBAPP_CONFIGS = {
    LOGO_URL: "https://www.remoteitjobs.app/images/logo-white-bg.jpg",
    SITE_TAGLINE: "Find Remote Tech Jobs Worldwide",
    SITE_NAME: "RemoteITJobs.app",
    WEBSITE_URL: "https://remoteitjobs.app",
    COMPANY_ADDRESS: "3030 Coimbra (Portugal)",
    COMPANY_EMAIL: "contact@remoteitjobs.app",
    POST_PREFIX_URL: "https://www.remoteitjobs.app/post/",
    PROFILE_PREFIX_URL: "https://www.remoteitjobs.app/public-profile/",
    UNSUBSCRIBE_URL: `https://us-central1-${PRODUCTION_GCLOUD_PROJECT}.cloudfunctions.net/unsubscribeEmail?uid=`,

    MAILERSEND_API_KEY: "TODO",

    CUSTOM_CONFIGS: {
        openPushInApp: true,
    } as any,

    // Account: contact@sidehustlify.com - Di.....oc7
    IMAGE_KIT_API: {
        publicKey : "TODO", // "public_QPb3OvUFUKRLd2VLZtV+ExCVDTo=",
        privateKey : "TODO", //"private_z60vq5B2MydsmNQ+c2dMsOG0RBg=",
        urlEndpoint : "TODO", //"https://ik.imagekit.io/sidehustlify",
    },

    STRIPE_API: {
        prod: "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",
        dev: "sk_test_51PueA3HT2hxH9IRDdlrmFSjPbHmjQ1An0PNkz74ZkAOw50c1MSK5nG0MGBWOvbm5RjkBDgG4VXnMDZ2yRBWJGtpA00wlrvUV9J",
        webhook_dev: "whsec_jySvwwZUSe3BLDFdJPYWn6c2vABfZbDd",
        webhook_prod: "whsec_YixjlN2DVZCFOnC04gm6aCqqiCzMIej4",
    },

    ONESIGNAL_CONFIGS: {
        apiKey: "os_v2_app_seksm7la2rbdnfbx7sbbsm6na6kjvhieo3iedce6duzlc27flohrs37wcdhrjygxs5znhybodlybqisjy6ipars7iwm6wx7b3wlpzuq",
        appId: "9115267d-60d4-4236-9437-fc821933cd07"
    },

    SOCIAL_NETWORK_API_KEY: {
        metaAccessToken: "",
        facebookPageId: "",
        instagramId: "",
        threadsAPI: {
            clientId: "",
            clientSecret: "",
            redirectUri: `https://us-central1-${PRODUCTION_GCLOUD_PROJECT}.cloudfunctions.net/threadsAPIHandleAuthCallback`,
        },
        redditAPI: {
            clientId: "4AipG2HfGPnuOhtXtOYEmA",
            clientSecret: "9swPagUhH079Yq3uzQjyimQssUYhow",
            username: "FortulyApps",
            password: "d****c7",
            subReddit: "",
        }, bluesky: {
            username: "remoteitjobs.bsky.social",
            password: "divadlooc7",
        }, telegram: {
            channel: "@remoteitjobs_app",
            botToken: "7785067124:AAHxGqLN473GzorP4NoHoDcj0shkQMAm8rE",
        }
    }
}