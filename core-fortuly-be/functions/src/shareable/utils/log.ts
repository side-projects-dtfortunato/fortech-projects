import * as admin from "firebase-admin";
import * as functions from 'firebase-functions';
import DocumentSnapshot = admin.firestore.DocumentSnapshot;

export class Log {

    static onCreate(snapshot: DocumentSnapshot)
    {
        this.printLog(snapshot.id, this.getStrFromDocumentSnapshot(snapshot));
    }

    static onDelete(snapshot: DocumentSnapshot)
    {
        this.printLog(snapshot.id, this.getStrFromDocumentSnapshot(snapshot));
    }

    static onCall(data: any, userId: string) {
        this.printLog(userId, data ? JSON.stringify(data) : 'No data received');
    }

    static onWrite(change: functions.Change<DocumentSnapshot>) {
        let message = '';


        // Before:
        message = 'Before: ' + this.getStrFromDocumentSnapshot(change.before);
        message += ' - ';
        message = 'After: ' + this.getStrFromDocumentSnapshot(change.after);

        let docId = 'NoId';
        if (change.before && change.before.id)
        {
            docId = change.before.id;
        } else if (change.after && change.after.id) {
            docId = change.after.id;
        }
        this.printLog(docId, message);
    }

    static getStrFromDocumentSnapshot(snapshot: DocumentSnapshot): string
    {
        return snapshot && snapshot.exists ? 'id: ' + snapshot.id + ', ' + JSON.stringify(snapshot.data()) : 'NOT-EXIST';
    }

    static printLog(tag: string, text: string)
    {
        console.log('FunctionTriggered: ' + tag + ':' + text);
    }

}