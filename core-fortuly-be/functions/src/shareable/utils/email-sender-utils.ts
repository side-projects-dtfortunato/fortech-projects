import {SentMessageInfo} from "nodemailer";

const nodemailer = require('nodemailer');

//const Recipient = require("mailersend").Recipient;
const EmailParams = require("mailersend").EmailParams;
const MailerSend = require("mailersend");
const mailersend = new MailerSend({
    api_key: "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIxIiwianRpIjoiZWUwOTA3ZjIxOGY5MmQ3YWYyOTY5NzFhMzFkODJjM2E1Nzc3M2NiZjIzZDlkNDk1NGY3YTEwMzlhNGQ4YzM1ZjdiODhkNDQwYzVmZGIyYjUiLCJpYXQiOjE2MzczMzc3MTkuNDg0MzcsIm5iZiI6MTYzNzMzNzcxOS40ODQzNzYsImV4cCI6NDc5MzAxMTMxOS40MDA4NDUsInN1YiI6IjE2MDQ4Iiwic2NvcGVzIjpbImVtYWlsX2Z1bGwiLCJkb21haW5zX2Z1bGwiLCJhY3Rpdml0eV9mdWxsIiwiYW5hbHl0aWNzX2Z1bGwiLCJ0b2tlbnNfZnVsbCIsIndlYmhvb2tzX2Z1bGwiLCJ0ZW1wbGF0ZXNfZnVsbCIsInN1cHByZXNzaW9uc19mdWxsIl19.Ao9_ci-KzzZtBS5DraA01c9Yv9sO_QKtUeWM9iGguyFIqjgiKoCmwK32TQRQ_YclBqaIutUKv-irHhF835nbvDhiFQIMXaVWZ9Q4pQ4sM6fE7XXbCHKpKdYry96P9EPuf7LvV80vg2S9ZjxxMJpwct6QAw2JbgNE2oie3BeFytMbi6WRvVR55hiWZRdXosIdlUzmsgLWIP0ciXgZUC4356xp08FZl3QzOdIEDfZZK15AzQz1eoFIaugdk-FjgwNeITEjgRDHYDJijNNWPD-rlJKP9CRwe5AbT7G7rF8vvVcNQC71gfbBeKQhlYvFabVOUxgakefF7NuuzCJABTHs8ufsH1lxc7kJUKjN479lOBEYru6Hx352cXq40Hi6kqPwm-FX7smqEFB-Cz9cDTmGKltu-E1Zf7VcQ5djYSptjPdzELiqFhzB253F-yoCnw3LgvPJQI6rG3xrgyc79joMz6PyI-DvIVEDe-viYwX_MWAzsNpPWgbnqMVjDMGfA0aRCH8eOAb_KHjXAPqPZNvmdfLjOiXvrTYRcv1udqCZzxsvV004uwYArLXRvHFhBIHHpMz1WI2AvLSf_AzWdXZkaJl2zTom-or8Wrltk9-Fzv-0zV2et0g09Elxjq2pPBS16ngDXrxBGcuLwbidJF0Bu30NM5IlNaEzFAgXpiyoKcI",
});



export function sendMailByMailerSend(params: {destinationsEmail: {email: string, name: string}[], templateId: string, subject?: string}) {
    return new Promise(async (resolve, reject) => {

        const recipients = params.destinationsEmail;

        console.log("Recipients: " + JSON.stringify(recipients));

        const emailParams = new EmailParams()
            .setRecipients(recipients)
            .setTemplateId(params.templateId);

        if (params.subject) {
            emailParams.setSubject(params.subject);
        }

        let sendResponse = await mailersend.send(emailParams);
        if(sendResponse) {
            console.log(JSON.stringify(sendResponse));
        }
        resolve(sendResponse);
    });

}



/** DEPRECATED */
let transporter = nodemailer.createTransport({
    service: 'gmail',
    auth: {
        user: 'remoteflexjobs@gmail.com',
        pass: 'divadlooc7',
    },
});

export function sendEmail(params: {destinationEmail: string, title: string, body: string}): Promise<SentMessageInfo> {

    const mailOptions = {
        from: 'Remotely Jobs App <yourgmailaccount@gmail.com>', // Something like: Jane Doe <janedoe@gmail.com>
        to: params.destinationEmail,
        subject: params.title, // email subject
        html: params.body,
    };

    return transporter.sendMail(mailOptions);

}