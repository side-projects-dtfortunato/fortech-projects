import * as admin from "firebase-admin";
import {BaseModel} from "../model-data/base.model";

export async function getDocumentData<T extends BaseModel>(collectionId: string, docId: string):Promise<T> {
    let dataDoc: any = null;
    const docRef = await admin.firestore().collection(collectionId).doc(docId).get();

    if (docRef && docRef.exists)
    {
        dataDoc = {
            uid: docId,
            ...docRef.data() as T,
        };
    }

    return dataDoc;
}

export function shuffleArray(array: any[]) {
    array.sort(() => Math.random() - 0.5);
}

export function getRandomString(length: number): string {
    let randomChars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    let result: string = '';
    for ( var i = 0; i < length; i++ ) {
        result += randomChars.charAt(Math.floor(Math.random() * randomChars.length));
    }
    return result;
}