import * as admin from "firebase-admin";
import {ShareableFirestoreCollectionNames} from "./shareable-firestore-collections-names";

const DEFAULT_MAX_ITEMS_PER_PAGE = 30;

export interface CollectionPageContainerModel {
    pageNum: number;
    mapItems: { [uid: string]: any }
}

/**
 * Handle the update of an item on the page container
 * @param item
 * @param updatePagination
 * @param collectionName
 * @param docId
 * @param fieldToSort
 * @param maxItemsPerPage
 * @param fieldUidName
 */
export function updatePageContainerItem(itemId: string = '',
                                              dataToUpdate: any,
                                              updatePagination: boolean,
                                              collectionName: string,
                                              docId: string,
                                              batchReuse: admin.firestore.WriteBatch | undefined = undefined,
                                              maxItemsPerPage: number = DEFAULT_MAX_ITEMS_PER_PAGE,
                                              fieldToSort: string = 'date',
                                              fieldUidName: string = 'uid')
{
    console.log('Started updatePageContainerItem for collection ' + collectionName + 'ItemId: ' + itemId + ' docId: ' + docId);
    if (itemId)
    {
        // Get the batch
        let batch = batchReuse ? batchReuse : admin.firestore().batch();

        // Update item on the collection
        batch.set(admin.firestore().collection(collectionName).doc(docId), {mapItems: {[`${itemId}`]: dataToUpdate}}, {merge: true});

        if (updatePagination)
        {
            updateCollectionPageContainer(collectionName, docId, fieldToSort, maxItemsPerPage, fieldUidName, batchReuse);
        }

        // Execute changes
        if (!batchReuse) {
            batch.commit().then(res => console.log(res)).catch(err => console.log(err));
        }
    }
}

/**
 * Rules:
 *  if (listMatches > (MAX_ITEMS_PER_PAGE * 2))
 *      Split list into too (the list should come sorted)
 *      create new doc on the pages collection
 *          path: {base-collection}/{doc-id}/Pagination/{page-num}
 *
 * The current page will be always the bigger one
 * @param batch
 * @param collectionName
 * @param docId
 * @param maxItemsPerPage
 * @param fieldToSort
 * @param fieldUidName
 */
export function updateCollectionPageContainer(
    collectionName: string,
    docId: string,
    fieldToSort: string = 'date',
    maxItemsPerPage: number = DEFAULT_MAX_ITEMS_PER_PAGE,
    fieldUidName: string = 'uid',
    batchReuse: admin.firestore.WriteBatch | undefined = undefined) {
    console.log('Started updateCollectionPageContainer for collection ' + collectionName + ' docId: ' + docId);

    // Get Batch
    let batch = batchReuse ? batchReuse : admin.firestore().batch();

    // Get root collection page container
    admin.firestore().collection(collectionName).doc(docId).get().then(docRef => {
        if (!docRef.exists) {
            console.error('Tried to update CollectionPageContainer, but doesnt exist');
            return;
        }
        console.log('updateCollectionPageContainer found docId: ' + docId);
        const rootPageContainer = docRef.data() as  CollectionPageContainerModel;


        // Check if the size of the map is > (than maxItemsPerPage * 2)
        if (rootPageContainer && rootPageContainer.mapItems) {
            const mapKeys = Object.entries(rootPageContainer.mapItems);

            if (mapKeys && mapKeys.length >= (maxItemsPerPage * 2)) {
                // Should split the map and create a new page

                // Sort the list
                let listItems = Object.values(rootPageContainer.mapItems);
                listItems = listItems.sort((item1, item2) => {
                    const val1 = item1[fieldToSort];
                    const val2 = item2[fieldToSort];
                    return Number(val1) - Number(val2);
                });

                // List is sorted. Split into 2 lists
                const newPageList = listItems.slice(0, maxItemsPerPage); // Page to be added into the second collection
                const rootPageList = listItems.slice(maxItemsPerPage - 1); // List to stay on root container

                // Generate the new maps
                const newPageContainer: CollectionPageContainerModel = {
                    mapItems: {},
                    pageNum: rootPageContainer.pageNum ? rootPageContainer.pageNum : 0,
                };
                const rootPageContainerUpdated: CollectionPageContainerModel = {
                    mapItems: {},
                    pageNum: newPageContainer.pageNum + 1, // Root container should have now a new page
                };


                // Setup map items
                newPageList.forEach((item) => {
                    newPageContainer.mapItems[item[fieldUidName]] = item;
                });
                rootPageList.forEach((item) => {
                    rootPageContainerUpdated.mapItems[item[fieldUidName]] = item;
                });

                // Set new page containers on batch
                batch.set(admin.firestore().collection(collectionName).doc(docId), rootPageContainerUpdated);
                batch.set(admin.firestore().collection(collectionName)
                        .doc(docId)
                        .collection(ShareableFirestoreCollectionNames.subCollectionPagination).doc(String(newPageContainer.pageNum)),
                    newPageContainer);

            } else if (!rootPageContainer.pageNum) {
                // If doesn't have yet a page, should set the page 0 (because, maybe it's the first page)
                batch.set(admin.firestore().collection(collectionName).doc(docId), {pageNum: 0}, {merge: true});
            }

            // If doesn't have any batch yet, just use the one that he received
            if (!batchReuse) {
                batch.commit().then(res => console.log('Executed: updateCollectionPageContainer ' + res))
                    .catch(err => console.log(err));
            }

        }
    }).catch(err => console.log(err));

}