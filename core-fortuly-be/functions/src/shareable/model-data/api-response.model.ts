
export const responseMessages: {[index:number]:string} = {
    1000: 'Something failed',
    1001: 'User not logged in',
    1002: 'User data not found',
    1003: 'Seller profile is already approved',
    1004: 'Its missing some documents to review',
    1005: 'Users FCM Token map not found',
    1006: 'User not authorized to execute this action',
    1007: 'Seller approval already answered',
    1008: 'User already have a user type defined',
    1009: 'Invalid params',
    1010: 'Data not found',
    1011: 'Action not allowed',
    1012: 'Professional profile not found',

    1100: 'This user has already an active partner account',
    1101: 'Failed creating a new Partner account',


    2000: 'Request executed with success',
    2001: 'User profile updated with success',

};

export interface ApiResponse<T extends any> {
    responseCode: number; // Internal response code (if < 2000 is an error or if > 2000 is a success response)
    responseData?: T; // Body response,
    errorData?: any;
    message?: string; // Message in the case it needs some extra information
    displayMessage?: boolean; // if the Message sent should be displayed to the User
    isSuccess: boolean; // If the request was success or not
}

export const getResponse = function (responseCode: number, responseData: any = null, message: any = null, displayMessage = false) {
    const isSuccess = responseCode >= 2000;

    console.error("");
    const responseObj = {
        responseCode: responseCode,
        responseData: isSuccess ? responseData : null,
        errorData: !isSuccess ? responseData : null,
        message: message ? message : responseMessages[responseCode],
        displayMessage: displayMessage,
        isSuccess: responseCode >= 2000,
    };

    if  (responseObj.isSuccess) {
        console.log("Response: " + JSON.stringify(responseObj));
    } else {
        console.error("Error response: " + JSON.stringify(responseObj));
    }

    return responseObj;
};

export const getQuickResponse = function (isSuccess: boolean, responseData: any = null, message: any = null) {
    if (!isSuccess) {
        // Print responses
        console.log('Response: ' + JSON.stringify(responseData));
    }
    return getResponse(isSuccess ? 2000 : 1000, responseData, message, false);
};