export interface ValueCurrencyModel {
    value: number;
    currencyLabel: string;
    currencyISOCode: string;
}