import * as admin from "firebase-admin";
import * as functions from "firebase-functions";
import MulticastMessage = admin.messaging.MulticastMessage;
import {ShareableFirestoreCollectionNames} from "../../model-data/shareable-firestore-collections-names";
import {getResponse} from "../../model-data/api-response.model";
import {Log} from "../../utils/log";

/**
 * Function to execute sending a notification
 * @param data
 */
export function sendPushNotificationFunction(data: PushNotificationBody) {
    return new Promise(async (resolve, reject) => {

        // Get User FCMTokens Map
        const mapTokensRef = await admin.firestore().collection(ShareableFirestoreCollectionNames.userFCMTokensMap).doc(ShareableFirestoreCollectionNames.userFCMTokensMap.toUpperCase()).get();

        if (!mapTokensRef || !mapTokensRef.exists) {
            resolve(getResponse(1005));
        }
        let mapFCMTokens: {[userId:string]:string[]} = mapTokensRef.data() as {[userId:string]:string[]};
        let targetFCMTokens: string[] = [];

        // Send the notifications
        if (mapFCMTokens && data.targetUserIds) {

            // Get FCM Tokens
            data.targetUserIds.forEach((userId) => {

                // Get List of FCM Tokens
                let fcmTokens: string[] = mapFCMTokens[userId];

                if (fcmTokens && fcmTokens.length > 0)
                {
                    fcmTokens.forEach((token) => {
                        console.log('Sending message to: ' + userId + ' Token: ' + token);
                        targetFCMTokens.push(token);
                    });
                }
            });
        }

        console.log("Sending Push notification to: " + targetFCMTokens.length + " List Users: " + JSON.stringify(data.targetUserIds));
        if (targetFCMTokens.length > 0)
        {
            const notification: MulticastMessage = {
                tokens: targetFCMTokens,
                data: {
                    ...data.metadata,
                    "click_action": "FLUTTER_NOTIFICATION_CLICK",
                    "id": "052020",
                    "status": "done",
                },
                notification: {
                    body: data.message,
                    title:  data.title ? data.title : 'Remote-Work.app',
                    //imageUrl: "https://bitbucket.org/bizcateapp/bizcate-assets/raw/master/progamer-assets/icons/ic_progamer_notification.png"
                },
            };
            admin.messaging().sendMulticast(notification).then((res => {
                console.log('Push notification sent: ' + JSON.stringify(notification) +  " Res: " + JSON.stringify(res));
                resolve(res);
            })).catch(err => reject(err));
        } else {
            resolve({});
        }
    });
}

export const updateFCMTokens = functions.https.onCall(
    (data: { token: string, isToRemove: boolean },
     context) => {
        if (!context.auth || !context.auth.uid) return getResponse(1001);

        if (!data.token) return getResponse(1000);

        const userId = context.auth.uid;
        Log.onCall(data, userId);

        return new Promise(async (resolve, reject) => {

            let batch = admin.firestore().batch();

            batch.set(admin
                .firestore()
                .collection(ShareableFirestoreCollectionNames.userFCMTokensMap)
                .doc(ShareableFirestoreCollectionNames.userFCMTokensMap.toUpperCase()), { [userId]: !data.isToRemove ? [data.token] : []}, {merge: true});

            // Update user central user data
            batch.set(admin
                .firestore()
                .collection(ShareableFirestoreCollectionNames.centralUserData)
                .doc(userId), {userFCMToken: !data.isToRemove ? data.token : ""}, {merge: true});

            batch.commit()
                .then(res => resolve(getResponse(2000)))
                .catch(err => {
                    console.log("Something failed on updating FCM Tokens");
                    reject(getResponse(1000, err))
                });
        });
    });

export interface PushNotificationBody {
    title?: string,
    message: string,
    targetUserIds: string[],
    metadata: {[key: string]: string},
}
