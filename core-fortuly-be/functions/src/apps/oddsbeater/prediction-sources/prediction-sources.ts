import {OddsBeaterUtils} from "../utils/oddsbeater-utils";

export const PredictionSourceData: PredictionSourcesMap = {
    forebet: {
        id: "forebet",
        label: "forebet.com",
        url: "https://m.forebet.com/",
        urlPerLeague: {
            "1": "https://m.forebet.com/en/predictions-world/world-cup",
            "4": "https://m.forebet.com/en/predictions-europe/euro-championship",
            "39": "https://m.forebet.com/en/football-tips-and-predictions-for-england/premier-league",
            "40": "https://m.forebet.com/en/football-tips-and-predictions-for-england/championship",
            "2": "https://m.forebet.com/en/predictions-europe/uefa-champions-league",
            "3": "https://m.forebet.com/en/predictions-europe/uefa-europa-league",
            "140": "https://m.forebet.com/en/football-tips-and-predictions-for-spain/primera-division",
            "135": "https://m.forebet.com/en/football-tips-and-predictions-for-italy/serie-a",
            "78": "https://m.forebet.com/en/football-tips-and-predictions-for-germany/Bundesliga",
            "61": "https://m.forebet.com/en/football-tips-and-predictions-for-france/ligue1",
            "94": "https://m.forebet.com/en/football-tips-and-predictions-for-portugal-superliga/primeira-liga",
            "71": "https://m.forebet.com/en/football-tips-and-predictions-for-brazil/serie-a",
            "88": "https://m.forebet.com/en/football-tips-and-predictions-for-netherlands/eredivisie",
        },
    },
    olbg: {
        id: "olbg",
        label: "olbg.com",
        url: "https://www.olbg.com",
        urlPerLeague: {
            "1": "https://www.olbg.com/betting-tips/Football/International/World_Cup_Finals/1",
            "4": `https://www.olbg.com/betting-tips/Football/International/Euro_${OddsBeaterUtils.CURRENT_SEASON}/1`,
            "39": "https://www.olbg.com/betting-tips/Football/UK/England_Premier_League/1",
            "40": "https://www.olbg.com/betting-tips/Football/UK/England_Championship/1",
            "2": "https://www.olbg.com/betting-tips/Football/UEFA_Competitions/Champions_League/1",
            "3": "https://www.olbg.com/betting-tips/Football/UEFA_Competitions/Europa_League/1",
            "140": "https://www.olbg.com/betting-tips/Football/European_Competitions/Spain_Primera_Liga/1",
            "135": "https://www.olbg.com/betting-tips/Football/European_Competitions/Italy_Serie_A/1",
            "78": "https://www.olbg.com/betting-tips/Football/European_Competitions/Germany_Bundesliga_I/1",
            "61": "https://www.olbg.com/betting-tips/Football/European_Competitions/France_Ligue_1/1",
            "94": "https://www.olbg.com/betting-tips/Football/European_Competitions/Portugal_Primeira_Liga/1",
            "71": "https://www.olbg.com/betting-tips/Football/World_Competitions/Brazil_Serie_A/1",
            "88": "https://www.olbg.com/betting-tips/Football/European_Competitions/Netherlands_Eredivisie/1",
        },
    },
    apifootball: {
        id: "apifootball",
        label: "apifootball",
        url: "",
        urlPerLeague: {},
    },
    mybets: {
        id: "mybets",
        label: "mybets",
        url: "https://www.mybets.today/",
        urlPerLeague: {
            today: "https://www.mybets.today/soccer-predictions/",
            tomorrow: "https://www.mybets.today/soccer-predictions/tomorrow/",
            aftertomorrow: "https://www.mybets.today/soccer-predictions/after-tomorrow/",
        },
    },
    footballpredictionscom: {
        id: "footballpredictionscom",
        label: "footballpredictions.com",
        url: "footballpredictions.com",
        urlPerLeague: {
            "1": "https://footballpredictions.com/footballpredictions/world-cup-2022-predictions/",
            "4": `https://footballpredictions.com/footballpredictions/euro-${OddsBeaterUtils.CURRENT_SEASON}-predictions/`,
            "39": "https://footballpredictions.com/footballpredictions/premierleaguepredictions/",
            "40": "https://footballpredictions.com/footballpredictions/championshippredictions/",
            "2": "https://footballpredictions.com/footballpredictions/championsleaguepredictions/",
            "3": "https://footballpredictions.com/footballpredictions/europaleaguepredictions/",
            "140": "https://footballpredictions.com/footballpredictions/primeradivisionpredictions/",
            "135": "https://footballpredictions.com/footballpredictions/serieapredictions/",
            "78": "https://footballpredictions.com/footballpredictions/bundesligapredictions/",
            "61": "https://footballpredictions.com/footballpredictions/ligue1predictions/",
            "94": "https://footballpredictions.com/footballpredictions/portugal-primeira-liga-predictions/",
            "88": "https://footballpredictions.com/footballpredictions/netherlands-eredivisie-predictions/",
        },
    },
}


export interface PredictionSourcesMap {
    [sourceId: string]: PredictionSource;
}

export interface PredictionSource {
    id: string;
    label: string;
    url: string;
    urlPerLeague: {
        [leagueId: string]: string;
    };
    results?: {
        numCorrectPredictions: number;
        numWrongPredictions: number;
        numInvested: number; // Total invested
        numProfitOdds: number; // Total Profits (total of odds won)
    }
}
