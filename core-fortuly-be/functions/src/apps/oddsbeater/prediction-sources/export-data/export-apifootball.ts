import {MatchTipData, MatchTipType} from "../../data-model/match-tip";
import {PredictionSourceData} from "../prediction-sources";
import {getFixturePrediction} from "../../api-football/endpoints";

export interface APIFootballMatchPredictionModel {
    predictions: {
        winner: {
            id: number;
            name: string;
            comment: string;
        },
        win_or_draw: boolean;
        percent: {
            home: any;
            draw: any;
            away: any;
        };
    };
}

export class ExportAPIFootballPrediction {

    /**
     * Get the preditction and update the match data
     * @param matchData
     */
    static async updateMatchPrediction(matchData: MatchTipData): Promise<MatchTipData> {
        if (!matchData.tips || !(matchData.tips as any).apifootball) {

            // API Request
            let apiResponse: any = await getFixturePrediction(matchData.id);
            let matchTipType: MatchTipType = MatchTipType.DRAW;

            if (apiResponse && apiResponse.response && apiResponse.response.length > 0) {
                let matchPrediction: APIFootballMatchPredictionModel = apiResponse.response[0];

                // Convert percentages to number
                matchPrediction.predictions.percent.home = Number(matchPrediction.predictions.percent.home.replace("%", ""));
                matchPrediction.predictions.percent.draw = Number(matchPrediction.predictions.percent.draw.replace("%", ""));
                matchPrediction.predictions.percent.away = Number(matchPrediction.predictions.percent.away.replace("%", ""));

                if (matchPrediction.predictions.percent.draw > matchPrediction.predictions.percent.home
                    && matchPrediction.predictions.percent.draw > matchPrediction.predictions.percent.away) {
                    matchTipType = MatchTipType.DRAW;
                } else if (matchPrediction.predictions.percent.home > matchPrediction.predictions.percent.away) {
                    matchTipType = MatchTipType.HOME;
                } else {
                    matchTipType = MatchTipType.AWAY;
                }

                // Update match data object
                if (!matchData.tips) matchData.tips = {};

                matchData.tips[PredictionSourceData.apifootball.id] = {
                    sourceId: PredictionSourceData.apifootball.id,
                    sourceLabel: PredictionSourceData.apifootball.label,
                    tipSuggestion: matchTipType,
                };
            }
        }

        return matchData;
    }

}