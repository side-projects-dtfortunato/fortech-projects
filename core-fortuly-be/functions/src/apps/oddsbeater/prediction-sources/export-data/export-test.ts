import scrapeit from "scrape-it";


export class ExportTest {



    static async exportListMatches(url: string): Promise<string> {
        return new Promise(async (resolve, reject) => {
            // WhoScore
            let response: any = await scrapeit(url, {
                title: "span.title",
            });

            resolve(JSON.stringify(response.data));
            return;
        });
    }

}