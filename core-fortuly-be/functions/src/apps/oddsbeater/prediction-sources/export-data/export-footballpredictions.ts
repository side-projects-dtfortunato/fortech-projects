import scrapeit from "scrape-it";
import {MatchTipData, MatchTipType} from "../../data-model/match-tip";
import {PredictionSourceData} from "../prediction-sources";

interface ExportListMatch {
    matchTitle: string;
    link: string;
    prediction: string;
}


export class ExportFootballPredictions {

    static async calculateMatchPrediction(matchData: MatchTipData): Promise<MatchTipData> {

        // Get link to export matches
        let leagueId: string | undefined = matchData.matchObject?.league.id;
        if (leagueId) {
            let exportUrl: string | undefined = PredictionSourceData.footballpredictionscom.urlPerLeague[leagueId];

            if (exportUrl) {
                // Export list matches
                let listMatches: ExportListMatch[] = await this.exportListMatches(exportUrl);

                if (listMatches) {
                    listMatches.forEach((exportListItem) => {
                        if (exportListItem.matchTitle && exportListItem.prediction) {
                            let predictionTip: MatchTipType = MatchTipType.UNPREDICTABLE;

                            let cleanTitle: string = exportListItem.matchTitle.toLowerCase();
                            let cleanHomeTeam: string | undefined = matchData.matchObject?.teams.home.name.toLowerCase();
                            let cleanAwayTeam: string | undefined = matchData.matchObject?.teams.away.name.toLowerCase();
                            if (cleanTitle.includes(cleanHomeTeam as string) && cleanTitle.includes(cleanAwayTeam as string)) {
                                let resultStr: string[] = exportListItem.prediction.split("-");
                                let homeResult: number = Number(resultStr[0]);
                                let awayResult: number = Number(resultStr[1]);

                                if (homeResult > awayResult) {
                                    // Home team wins
                                    predictionTip = MatchTipType.HOME;
                                } else if (awayResult > homeResult) {
                                    // Away team wins
                                    predictionTip = MatchTipType.AWAY;
                                } else {
                                    // Draw
                                    predictionTip = MatchTipType.DRAW;
                                }

                                if (!matchData.tips) {
                                    matchData.tips = {};
                                }
                                matchData.tips[PredictionSourceData.footballpredictionscom.id] = {
                                    sourceId: PredictionSourceData.footballpredictionscom.id,
                                    sourceLabel: PredictionSourceData.footballpredictionscom.label,
                                    tipSuggestion: predictionTip,
                                };
                            }

                        }
                    })
                }
            }
        }


        return matchData;
    }

    static async exportListMatches(url: string): Promise<ExportListMatch[]> {
        let response: any = await scrapeit(url, {
            listMatches: {
                listItem: "div.blog-post",
                data: {
                    matchTitle: {
                        selector: "div.predtitle",
                    },
                    link: {
                        selector: "div.predictiontxt a",
                        attr: "href",
                    },
                    prediction: {
                        selector: "div.predictionbox strong",
                    },
                },
            },
        });


        return response.data && response.data.listMatches ? response.data.listMatches as ExportListMatch[] : [];
    }
}