// ES6 or TypeScript:
import scrapeit from "scrape-it";
import {MatchTipData, MatchTipType, NextMatchesByLeague} from "../../data-model/match-tip";
import {PredictionSourceData} from "../prediction-sources";
import {LeagueAvailableModel} from "../../data-model/league-model";

export interface ForebetListMatchItem {
    homeTeam: string;
    awayTeam: string;
    link: string;
    prediction: string;
}

export class ExportForebet {

    static async calculateMatchPrediction(matchData: MatchTipData, leagueData: LeagueAvailableModel): Promise<MatchTipData> {
        let listMatches = {
            [matchData.id]: matchData,
        };

        listMatches = this.updateMatchesPredictions(listMatches, await this.exportListMatches(PredictionSourceData.forebet.urlPerLeague[leagueData.id]))

        return listMatches[matchData.id];
    }

    static async executeMatchesPredictions(listMatches: NextMatchesByLeague): Promise<NextMatchesByLeague> {
        let listLeaguesKeys = Object.keys(listMatches);
        for (let leagueIndex = 0; leagueIndex < listLeaguesKeys.length; leagueIndex++) {
            if (listLeaguesKeys[leagueIndex] !== "uid") {
                let leagueData = listMatches[listLeaguesKeys[leagueIndex]];

                if (leagueData && leagueData.listMatches && Object.values(leagueData.listMatches).length > 0) {
                    let hasMatchToPredict: boolean = Object.values(leagueData.listMatches).find((matchData) => !matchData.tips || !matchData.tips[PredictionSourceData.forebet.id]) ? true : false;

                    if (hasMatchToPredict) {
                        leagueData.listMatches = this.updateMatchesPredictions(leagueData.listMatches, await this.exportListMatches(PredictionSourceData.forebet.urlPerLeague[leagueData.leagueData.id]));
                    }
                }
            }
        }

        return listMatches;
    }

    static updateMatchesPredictions(listMatches: { [fixtureId: string]: MatchTipData; }, predictionListMatches: ForebetListMatchItem[]): { [fixtureId: string]: MatchTipData; } {

        Object.values(listMatches).forEach((matchData) => {

            if (matchData.matchObject) {
                predictionListMatches.forEach((predictedMatchData) => {
                    if ((matchData.matchObject?.teams.home.name.includes(predictedMatchData.homeTeam) || predictedMatchData.homeTeam.includes(<string>matchData.matchObject?.teams.home.name))
                        && (matchData.matchObject?.teams.away.name.includes(predictedMatchData.awayTeam) || predictedMatchData.awayTeam.includes(<string>matchData.matchObject?.teams.away.name))) {

                        listMatches[matchData.id].tips = {
                            ...matchData.tips,
                            [PredictionSourceData.forebet.id]: {
                                sourceId: PredictionSourceData.forebet.id,
                                sourceLabel: PredictionSourceData.forebet.label,
                                tipSuggestion: MatchTipType.DRAW,
                            },
                        }

                        if (matchData.tips) {
                            switch (predictedMatchData.prediction) {
                                case "1":
                                    matchData.tips[PredictionSourceData.forebet.id].tipSuggestion = MatchTipType.HOME;
                                    break;
                                case "X":
                                    matchData.tips[PredictionSourceData.forebet.id].tipSuggestion = MatchTipType.DRAW;
                                    break;
                                case "2":
                                    matchData.tips[PredictionSourceData.forebet.id].tipSuggestion = MatchTipType.AWAY;
                                    break;
                            }
                        }
                    }
                });

            }
        });

        return listMatches;
    }

    static async exportListMatches(url: string): Promise<ForebetListMatchItem[]> {
        let response: any = await scrapeit(url, {
            listMatches: {
                listItem: ".rcnt",
                data: {
                    homeTeam: {
                        selector: ".homeTeam",
                    },
                    awayTeam: {
                        selector: ".awayTeam",
                    },
                    link: {
                        selector: "a.tnmscn",
                        attr: "href",
                    },
                    prediction: {
                        selector: ".predict > .forepr",
                    },
                },
            },
        });


        return response.data && response.data.listMatches ? response.data.listMatches as ForebetListMatchItem[] : [];
    }
}