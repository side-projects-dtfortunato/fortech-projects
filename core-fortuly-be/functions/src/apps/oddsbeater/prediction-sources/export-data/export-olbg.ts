// ES6 or TypeScript:
import scrapeit from "scrape-it";
import {MatchTipData, MatchTipType, NextMatchesByLeague} from "../../data-model/match-tip";
import {PredictionSourceData} from "../prediction-sources";
import {LeagueAvailableModel} from "../../data-model/league-model";

export interface OlbgListMatchItem {
    matchTitle: string;
    confidencePercent: string;
    link: string;
    prediction: string;
}

export class ExportOlbg {

    static async calculateMatchPrediction(matchData: MatchTipData, leagueData: LeagueAvailableModel): Promise<MatchTipData> {
        let listMatches = {
            [matchData.id]: matchData,
        };

        listMatches = this.updateMatchesPredictions(listMatches, await this.exportListMatches(PredictionSourceData.olbg.urlPerLeague[leagueData.id]));

        return listMatches[matchData.id];
    }

    static async executeMatchesPredictions(listMatches: NextMatchesByLeague): Promise<NextMatchesByLeague> {
        let listLeaguesKeys = Object.keys(listMatches);
        for (let leagueIndex = 0; leagueIndex < listLeaguesKeys.length; leagueIndex++) {
            if (listLeaguesKeys[leagueIndex] !== "uid") {
                let leagueData = listMatches[listLeaguesKeys[leagueIndex]];

                if (leagueData && leagueData.listMatches && Object.values(leagueData.listMatches).length > 0) {
                    let hasMatchToPredict: boolean = Object.values(leagueData.listMatches).find((matchData) => !matchData.tips || !matchData.tips[PredictionSourceData.olbg.id]) ? true : false;

                    if (hasMatchToPredict) {
                        leagueData.listMatches = this.updateMatchesPredictions(leagueData.listMatches, await this.exportListMatches(PredictionSourceData.olbg.urlPerLeague[leagueData.leagueData.id]));
                    }
                }
            }
        }

        return listMatches;
    }

    static updateMatchesPredictions(listMatches: { [fixtureId: string]: MatchTipData; }, predictionListMatches: OlbgListMatchItem[]): { [fixtureId: string]: MatchTipData; } {

        Object.values(listMatches).forEach((matchData) => {
            if (matchData.matchObject) {
                predictionListMatches.forEach((predictedMatchData) => {
                    if ((matchData.matchObject?.teams.home.name.includes(predictedMatchData.matchTitle) || predictedMatchData.matchTitle.includes(<string>matchData.matchObject?.teams.home.name))
                        && (matchData.matchObject?.teams.away.name.includes(predictedMatchData.matchTitle) || predictedMatchData.matchTitle.includes(<string>matchData.matchObject?.teams.away.name))) {

                        let predictMatchHomeTeam = predictedMatchData.matchTitle.split(" v ")[0];
                        let predictMatchAwayTeam = predictedMatchData.matchTitle.split(" v ")[1];

                        if (predictedMatchData.prediction.toUpperCase() === "DRAW"
                            || predictedMatchData.prediction.toUpperCase() === predictMatchHomeTeam.toUpperCase()
                            || predictedMatchData.prediction.toUpperCase() === predictMatchAwayTeam.toUpperCase()) {

                            // Check if has a valid tip
                            listMatches[matchData.id].tips = {
                                ...matchData.tips,
                                [PredictionSourceData.olbg.id]: {
                                    sourceId: PredictionSourceData.olbg.id,
                                    sourceLabel: PredictionSourceData.olbg.label,
                                    tipSuggestion: MatchTipType.DRAW,
                                },
                            }

                            if (matchData.tips) {
                                if (predictedMatchData.prediction.toUpperCase() === predictMatchHomeTeam.toUpperCase()) {
                                    matchData.tips[PredictionSourceData.olbg.id].tipSuggestion = MatchTipType.HOME;
                                } else if (predictedMatchData.prediction.toUpperCase() === predictMatchAwayTeam.toUpperCase()) {
                                    matchData.tips[PredictionSourceData.olbg.id].tipSuggestion = MatchTipType.AWAY;
                                } else {
                                    matchData.tips[PredictionSourceData.olbg.id].tipSuggestion = MatchTipType.DRAW;
                                }
                            }
                        }
                    }
                });
            }
        });

        return listMatches;
    }

    static async exportListMatches(url: string): Promise<OlbgListMatchItem[]> {
        let response: any = await scrapeit(url, {
            listMatches: {
                listItem: "tbody > tr.tip-row",
                data: {
                    matchTitle: {
                        selector: "h5.event-name > a.tn-trigger",
                        attr: "title",
                    },
                    confidencePercent: {
                        selector: "p.confidence",
                    },
                    link: {
                        selector: "a.tn-trigger",
                        attr: "href",
                    },
                    prediction: {
                        selector: "h4.selection-name",
                    },
                },
            },
        });


        return response.data && response.data.listMatches ? response.data.listMatches as OlbgListMatchItem[] : [];
    }
}