// ES6 or TypeScript:
import scrapeit from "scrape-it";
import {PredictionSourceData} from "../prediction-sources";
import {MatchTipData, MatchTipType, NextMatchesByLeague} from "../../data-model/match-tip";

export interface MybetsListMatchItem {
    homeTeam: string;
    awayTeam: string;
    link: string;
    prediction: string;
    keywords: string;
}

export class ExportMybets {

    static async calculateMatchPrediction(matchData: MatchTipData): Promise<MatchTipData> {
        let listMatches = {
            [matchData.id]: matchData,
        };


        // Load all list matches
        if (listMatches[matchData.id]) {
            let listUrlsKeys: string[] = Object.keys(PredictionSourceData.mybets.urlPerLeague);
            for (let i = 0; i < listUrlsKeys.length; i++) {
                let urlFetch = PredictionSourceData.mybets.urlPerLeague[listUrlsKeys[i]];

                if (!(listMatches[matchData.id].tips)
                    || !(listMatches[matchData.id].tips as any)[PredictionSourceData.mybets.id])  {
                    // Didn't found the match prediction yet
                    listMatches = this.updateMatchesPredictions(listMatches, await this.exportListMatches(urlFetch));
                }
            }
        }


        // Old method, in  this one had to fetch all urls before search the match predictionn
        //let listAggExportedMatches: MybetsListMatchItem[] = await this.exportAllListMatches();
        //listMatches = this.updateMatchesPredictions(listMatches, listAggExportedMatches);

        return listMatches[matchData.id];
    }

    static async executeMatchesPredictions(listMatches: NextMatchesByLeague): Promise<NextMatchesByLeague> {
        // Load all list matches
        let listAggExportedMatches: MybetsListMatchItem[] = await this.exportAllListMatches();


        let listLeaguesKeys = Object.keys(listMatches);
        for (let leagueIndex = 0; leagueIndex < listLeaguesKeys.length; leagueIndex++) {
            if (listLeaguesKeys[leagueIndex] !== "uid") {
                let leagueData = listMatches[listLeaguesKeys[leagueIndex]];

                if (leagueData && leagueData.listMatches && Object.values(leagueData.listMatches).length > 0) {
                    let hasMatchToPredict: boolean = Object.values(leagueData.listMatches).find((matchData) => !matchData.tips || !matchData.tips[PredictionSourceData.mybets.id]) ? true : false;

                    if (hasMatchToPredict) {
                        leagueData.listMatches = this.updateMatchesPredictions(leagueData.listMatches, listAggExportedMatches);
                    }
                }
            }
        }

        return listMatches;
    }

    static updateMatchesPredictions(listMatches: { [fixtureId: string]: MatchTipData; }, predictionListMatches: MybetsListMatchItem[]): { [fixtureId: string]: MatchTipData; } {

        Object.values(listMatches).forEach((matchData) => {

            if (matchData.matchObject) {
                predictionListMatches.forEach((predictedMatchData) => {
                    if ((matchData.matchObject?.teams.home.name.includes(predictedMatchData.homeTeam) || predictedMatchData.homeTeam.includes(<string>matchData.matchObject?.teams.home.name))
                        && (matchData.matchObject?.teams.away.name.includes(predictedMatchData.awayTeam) || predictedMatchData.awayTeam.includes(<string>matchData.matchObject?.teams.away.name))) {

                        listMatches[matchData.id].tips = {
                            ...matchData.tips,
                            [PredictionSourceData.mybets.id]: {
                                sourceId: PredictionSourceData.mybets.id,
                                sourceLabel: PredictionSourceData.mybets.label,
                                tipSuggestion: MatchTipType.DRAW,
                            },
                        }

                        if (matchData.tips) {
                            switch (predictedMatchData.prediction) {
                                case "1":
                                    matchData.tips[PredictionSourceData.mybets.id].tipSuggestion = MatchTipType.HOME;
                                    break;
                                case "X":
                                    matchData.tips[PredictionSourceData.mybets.id].tipSuggestion = MatchTipType.DRAW;
                                    break;
                                case "2":
                                    matchData.tips[PredictionSourceData.mybets.id].tipSuggestion = MatchTipType.AWAY;
                                    break;
                            }
                        }
                    }
                });

            }
        });

        return listMatches;
    }

    static async exportAllListMatches(): Promise<MybetsListMatchItem[]> {
        let listAggExportedMatches: MybetsListMatchItem[] = [];

        listAggExportedMatches = listAggExportedMatches.concat(await this.exportListMatches(PredictionSourceData.mybets.urlPerLeague.today));
        listAggExportedMatches = listAggExportedMatches.concat(await this.exportListMatches(PredictionSourceData.mybets.urlPerLeague.tomorrow));
        listAggExportedMatches = listAggExportedMatches.concat(await this.exportListMatches(PredictionSourceData.mybets.urlPerLeague.aftertomorrow));

        return listAggExportedMatches;
    }

    static async exportListMatches(url: string): Promise<MybetsListMatchItem[]> {
        let response: any = await scrapeit(url, {
            listMatches: {
                listItem: ".listgames > .event-fixtures",
                data: {
                    homeTeam: {
                        selector: ".homediv",
                    },
                    awayTeam: {
                        selector: ".awaydiv",
                    },
                    link: {
                        selector: "a.linkgames",
                        attr: "href",
                    },
                    prediction: {
                        selector: ".tipdiv",
                    },
                },
            },
        });


        return response.data && response.data.listMatches ? response.data.listMatches as MybetsListMatchItem[] : [];
    }
}