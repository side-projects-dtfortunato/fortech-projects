export const OddsBeaterFirestoreCollectionNames = {

    // Profile
    Table_OddsBeaterData: "OddsBeaterData",
    Doc_OddsBeaterDataNextMatches: "NextMatchesToPredict",
    Doc_OddsBeaterDataPredictedMatches: "PredictedMatches",
    Doc_OddsBeaterDataFinishedPredictedMatches: "FinishedPredictedMatches",
    Doc_OddsBeaterDataPredictionsSources: "PredictedSources",
}