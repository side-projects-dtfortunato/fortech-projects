import {LeagueAvailableModel} from "./league-model";

// @ts-ignore
export enum MatchTipType {
    HOME = "HOME", DRAW = "DRAW", AWAY = "AWAY", UNPREDICTABLE = "UNPREDICTABLE"
}
// @ts-ignore
export enum MatchPredictionStatus {
    PENDING = "PENDING", IN_PROGRESS = "IN_PROGRESS", PREDICTION_COMPLETED = "PREDICTION_COMPLETED"
}

export interface FinishedMatches {
    [matchId: string]: MatchTipData;
}

export interface NextMatchesByLeague {
    [leagueId: string]: {
        leagueData: LeagueAvailableModel;
        listMatches: {
            [fixtureId: string]: MatchTipData;
        },
    },
}

export interface MatchTipData {
    id: string;
    matchObject?: MatchDetailsObject;
    oddsDetails?: any;
    oddsAvg?: MatchOddsAvg;
    tips?: {
        [sourceId: string]: SourceMatchTip;
    };
    prediction?: {
        calculatedPrediction: MatchTipType;
        finalResult?: MatchTipType;
        homeValuePrediction?: number;
        drawValuePrediction?: number;
        awayValuePrediction?: number;
    }
    predictionStatus: MatchPredictionStatus;
    metadata?: {};
}

export interface MatchDetailsObject {
    fixture: {
        id: string;
        date: string;
        timestamp: number;
        status?: {
            long: string;
            short: string; // "FT" for finished matches
            elapsed: number;
        }
    },
    league: {
        id: string;
        name: string;
        country: string;
        logo: string;
        flag: string;
        season: number;
        round: string;
    },
    teams: {
        home: {
            id: number;
            name: string;
            logo: string;
            winner: boolean;
        },
        away: {
            id: number;
            name: string;
            logo: string;
            winner: boolean;
        },
    },
}

export interface MatchOddsAvg {
    home: number;
    draw: number;
    away: number;
}


export interface SourceMatchTip {
    tipSuggestion: MatchTipType;
    sourceId: string;
    sourceLabel: string;
}