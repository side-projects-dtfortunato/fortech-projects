export interface LeagueAvailableModel {
    id: string;
    label: string;
    countryCode: string;
}

export const LeaguesAvailable: { [id: string]: LeagueAvailableModel} = {
    // World Cup
    "1": {
        id: "1",
        label: "World Cup",
        countryCode: "",
    },
    // Euro Championship
    "4": {
        id: "4",
        label: "Euro Championship",
        countryCode: "",
    },
    // England
    "39": {
        id: "39",
        label: "Premier League",
        countryCode: "GB",
    },
    // Uefa Champions League
    "2": {
        id: "2",
        label: "UEFA Champions League",
        countryCode: "",
    },
    "3": {
        id: "3",
        label: "UEFA Europa League",
        countryCode: "",
    },
    "140": {
        id: "140",
        label: "La Liga",
        countryCode: "ES",
    },
    "40": {
        id: "40",
        label: "Championship",
        countryCode: "GB",
    },
    // Italy
    "135": {
        id: "135",
        label: "Serie A",
        countryCode: "IT",
    },
    // Germany
    "78": {
        id: "78",
        label: "Bundesliga",
        countryCode: "DE",
    },
    // France
    "61": {
        id: "61",
        label: "Ligue 1",
        countryCode: "FR",
    },
    // Portugal
    "94": {
        id: "94",
        label: "Primeira Liga",
        countryCode: "PT",
    },
    // Brazil
    /*"71": {
        id: "71",
        label: "Brasileirao",
        countryCode: "BR",
    },*/
    // Netherlands
    "88": {
        id: "88",
        label: "Eredivisie",
        countryCode: "NL",
    },
    /*
    // Uefa Europa League


    // Scotland
    "179": {
        id: "179",
        label: "Premiership",
        countryCode: "GB",
    },
    // US
    "253": {
        id: "253",
        label: "Major League Soccer",
        countryCode: "US",
    },
    // Turkey
    "203": {
        id: "203",
        label: "Super Lig",
        countryCode: "TR",
    },
    */
}