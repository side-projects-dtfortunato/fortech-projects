import * as functions from "firebase-functions";
import * as admin from "firebase-admin";
import {NextMatchesByLeague} from "../../../data-model/match-tip";
import {getDocumentData} from "../../../../../shareable/utils/utils";
import {OddsBeaterFirestoreCollectionNames} from "../../../data-model/oddsbeater-firestore-collection-names";
import {ExportForebet} from "../../../prediction-sources/export-data/export-forebet";
import {getQuickResponse} from "../../../../../shareable/model-data/api-response.model";
import {ExportOlbg} from "../../../prediction-sources/export-data/export-olbg";
import {ExportMybets} from "../../../prediction-sources/export-data/export-mybets";

export const calculatePredictionExportForebet = functions.https.onRequest(async (request,
                                                                    response) => {

    // Get next matches
    let nextMatches: NextMatchesByLeague = await getDocumentData(OddsBeaterFirestoreCollectionNames.Table_OddsBeaterData, OddsBeaterFirestoreCollectionNames.Doc_OddsBeaterDataNextMatches);

    if (nextMatches) {

        // Execute predictions from Forebet
        nextMatches = await ExportForebet.executeMatchesPredictions(nextMatches);

        let batch = admin.firestore().batch();

        batch.set(admin.firestore().collection(OddsBeaterFirestoreCollectionNames.Table_OddsBeaterData).doc(OddsBeaterFirestoreCollectionNames.Doc_OddsBeaterDataNextMatches),
            nextMatches, {merge: true});

        // Update Doc of predicted matches
        batch.set(admin.firestore().collection(OddsBeaterFirestoreCollectionNames.Table_OddsBeaterData).doc(OddsBeaterFirestoreCollectionNames.Doc_OddsBeaterDataPredictedMatches),
            nextMatches, {merge: true});

        // Commit changes
        let dbResponse = await batch.commit();


        response.send(getQuickResponse(true, {dbResponse: dbResponse, data: nextMatches}));
        return;
    } else {
        response.send(getQuickResponse(false));
    }
});


export const calculatePredictionExportOlbg = functions.https.onRequest(async (request,
                                                                    response) => {

    // Get next matches
    let nextMatches: NextMatchesByLeague = await getDocumentData(OddsBeaterFirestoreCollectionNames.Table_OddsBeaterData, OddsBeaterFirestoreCollectionNames.Doc_OddsBeaterDataNextMatches);

    if (nextMatches) {

        // Execute predictions from Forebet
        nextMatches = await ExportOlbg.executeMatchesPredictions(nextMatches);

        let batch = admin.firestore().batch();

        batch.set(admin.firestore().collection(OddsBeaterFirestoreCollectionNames.Table_OddsBeaterData).doc(OddsBeaterFirestoreCollectionNames.Doc_OddsBeaterDataNextMatches),
            nextMatches, {merge: true});

        // Update Doc of predicted matches
        batch.set(admin.firestore().collection(OddsBeaterFirestoreCollectionNames.Table_OddsBeaterData).doc(OddsBeaterFirestoreCollectionNames.Doc_OddsBeaterDataPredictedMatches),
            nextMatches, {merge: true});

        // Commit changes
        let dbResponse = await batch.commit();


        response.send(getQuickResponse(true, {dbResponse: dbResponse, data: nextMatches}));
        return;
    } else {
        response.send(getQuickResponse(false));
    }
});


export const calculatePredictionExportMybets = functions.https.onRequest(async (request,
                                                                    response) => {

    // Get next matches
    let nextMatches: NextMatchesByLeague = await getDocumentData(OddsBeaterFirestoreCollectionNames.Table_OddsBeaterData, OddsBeaterFirestoreCollectionNames.Doc_OddsBeaterDataNextMatches);

    if (nextMatches) {

        // Execute predictions from Mybets
        nextMatches = await ExportMybets.executeMatchesPredictions(nextMatches);

        let batch = admin.firestore().batch();

        batch.set(admin.firestore().collection(OddsBeaterFirestoreCollectionNames.Table_OddsBeaterData).doc(OddsBeaterFirestoreCollectionNames.Doc_OddsBeaterDataNextMatches),
            nextMatches, {merge: true});

        // Update Doc of predicted matches
        batch.set(admin.firestore().collection(OddsBeaterFirestoreCollectionNames.Table_OddsBeaterData).doc(OddsBeaterFirestoreCollectionNames.Doc_OddsBeaterDataPredictedMatches),
            nextMatches, {merge: true});

        // Commit changes
        let dbResponse = await batch.commit();


        response.send(getQuickResponse(true, {dbResponse: dbResponse, data: nextMatches}));
        return;
    } else {
        response.send(getQuickResponse(false));
    }
});



