/*import * as functions from "firebase-functions";
import {NextMatchesByLeague} from "../../../data-model/match-tip";
import {getDocumentData} from "../../../../../shareable/utils/utils";
import {OddsBeaterFirestoreCollectionNames} from "../../../data-model/oddsbeater-firestore-collection-names";
import {getQuickResponse} from "../../../../../shareable/model-data/api-response.model";
import {getFixtureData, getFixturesLeague} from "../../../api-football/endpoints";
import {OddsBeaterUtils} from "../../../utils/oddsbeater-utils";
import {ExportAPIFootballPrediction} from "../../../prediction-sources/export-data/export-apifootball";
import * as admin from "firebase-admin";*/


/*export const fillNextMatchesDetails = functions.https.onRequest(async (request,
                                                                       response) => {

    // Get Data
    let listMatches: NextMatchesByLeague = await getDocumentData(OddsBeaterFirestoreCollectionNames.Table_OddsBeaterData, OddsBeaterFirestoreCollectionNames.Doc_OddsBeaterDataNextMatches);

    if (!listMatches) {
        response.send(getQuickResponse(false, {}, "List matches are empty"));
    } else {
        let hasChanges = false;
        // Check for matches that needs details
        let listMatchesKeys = Object.keys(listMatches);
        for (let i = 0; i < listMatchesKeys.length; i++) {
            let leagueData = listMatches[listMatchesKeys[i]];

            // List of matches
            if (leagueData.listMatches
                && Object.keys(leagueData.listMatches).length > 0) {

                let apiResponseLeagueMatches: any = await getFixturesLeague({
                    league: leagueData.leagueData.id,
                    next: 20,
                    season: OddsBeaterUtils.CURRENT_SEASON,
                });

                let listLeagueMatchesIds = Object.keys(leagueData.listMatches);

                for (let matchIndex = 0; matchIndex < listLeagueMatchesIds.length; matchIndex++) {
                    let matchData = leagueData.listMatches[listLeagueMatchesIds[matchIndex]];

                    // Check if the match needs to be completed
                    if (!matchData.matchObject) {
                        // Search for the match data
                        if (apiResponseLeagueMatches && apiResponseLeagueMatches.response && apiResponseLeagueMatches.response.length > 0) {
                            apiResponseLeagueMatches.response.forEach((matchAPIItem: any) => {
                                if (matchAPIItem.fixture && matchAPIItem.fixture.id === matchData.id) {
                                    hasChanges = true;
                                    matchData.matchObject = matchAPIItem;
                                }
                            });
                        }

                        // If still didn't found the match, then get it directly from the API
                        if (!matchData.matchObject) {
                            let apiResponse: any = await getFixtureData(matchData.id);
                            if (apiResponse && apiResponse.response && apiResponse.response.length > 0) {
                                hasChanges = true;
                                matchData.matchObject = apiResponse.response[0];
                            }
                        }

                        // Update predictio data
                        matchData = await ExportAPIFootballPrediction.updateMatchPrediction(matchData);
                    }
                }
            }
        }

        // Update data per league (to avoid to loose loaded data in timeout functions)
        if (hasChanges) {
            hasChanges = false;
            let apiResponse = await admin
                .firestore()
                .collection(OddsBeaterFirestoreCollectionNames.Table_OddsBeaterData)
                .doc(OddsBeaterFirestoreCollectionNames.Doc_OddsBeaterDataNextMatches)
                .set(listMatches, {merge: true});
            console.log(JSON.stringify(apiResponse));
        }

        response.send(listMatches);
    }
});*/