import * as functions from "firebase-functions";
import * as admin from "firebase-admin";
import {MatchTipData, MatchTipType, NextMatchesByLeague} from "../../data-model/match-tip";
import {getDocumentData} from "../../../../shareable/utils/utils";
import {OddsBeaterFirestoreCollectionNames} from "../../data-model/oddsbeater-firestore-collection-names";
import {OddsBeaterUtils} from "../../utils/oddsbeater-utils";
import {LeagueAvailableModel} from "../../data-model/league-model";
import {getFixtureData} from "../../api-football/endpoints";
import {ExportAPIFootballPrediction} from "../../prediction-sources/export-data/export-apifootball";
import {ExportOlbg} from "../../prediction-sources/export-data/export-olbg";
import {ExportForebet} from "../../prediction-sources/export-data/export-forebet";
import {getQuickResponse} from "../../../../shareable/model-data/api-response.model";
import {PredictionSourcesMap} from "../../prediction-sources/prediction-sources";
import {ExportMybets} from "../../prediction-sources/export-data/export-mybets";
import {ExportFootballPredictions} from "../../prediction-sources/export-data/export-footballpredictions";
import {OneSignalUtils} from "../../utils/onesignal.utils";

export const calculateNextMatchPrediction = functions.https.onRequest(async (request,
                                                                             response) => {

    // Get predicted matches
    let nextMatchToPredict: NextMatchesByLeague = await getDocumentData(OddsBeaterFirestoreCollectionNames.Table_OddsBeaterData, OddsBeaterFirestoreCollectionNames.Doc_OddsBeaterDataNextMatches);
    if (!nextMatchToPredict || Object.values(nextMatchToPredict).length === 0) {
        response.send(getQuickResponse(true, {message: "We don't have any match to predict on DB"}));
        return;
    }

    let nextMatchesPredicted: NextMatchesByLeague = await getDocumentData(OddsBeaterFirestoreCollectionNames.Table_OddsBeaterData, OddsBeaterFirestoreCollectionNames.Doc_OddsBeaterDataPredictedMatches);

    // First, search for a match that wasn't predicted yet
    let listMatchesToPredict = OddsBeaterUtils.exportListMatchesFromMatchesByLeague(nextMatchToPredict);
    let listMatchesPredicted = OddsBeaterUtils.exportListMatchesFromMatchesByLeague(nextMatchesPredicted);

    let matchToCalculate: { matchItem: MatchTipData, leagueItem: LeagueAvailableModel } | undefined = listMatchesToPredict.find((matchItemToPredict) => {
        if (!listMatchesPredicted.find((matchItemPredicted) => matchItemPredicted.matchItem.id === matchItemToPredict.matchItem.id)) {
            return matchItemToPredict;
        }
        return null;
    });

    // Found the match to calculate the prediction
    if (matchToCalculate) {
        // Get Prediction Source Data
        let predictionSources: PredictionSourcesMap = await getDocumentData(OddsBeaterFirestoreCollectionNames.Table_OddsBeaterData, OddsBeaterFirestoreCollectionNames.Doc_OddsBeaterDataPredictionsSources);

        // Fill details
        // If still didn't found the match, then get it directly from the API
        if (!matchToCalculate.matchItem.matchObject) {
            let apiResponse: any = await getFixtureData(matchToCalculate.matchItem.id);
            if (apiResponse && apiResponse.response && apiResponse.response.length > 0) {
                matchToCalculate.matchItem.matchObject = apiResponse.response[0];
            }
        }

        // Calculate from external sources
        matchToCalculate.matchItem = await ExportMybets.calculateMatchPrediction(matchToCalculate.matchItem);
        matchToCalculate.matchItem = await ExportOlbg.calculateMatchPrediction(matchToCalculate.matchItem, matchToCalculate.leagueItem);
        matchToCalculate.matchItem = await ExportForebet.calculateMatchPrediction(matchToCalculate.matchItem, matchToCalculate.leagueItem);
        matchToCalculate.matchItem = await ExportFootballPredictions.calculateMatchPrediction(matchToCalculate.matchItem);

        // Calculate prediction from APIFOOTBALL
        matchToCalculate.matchItem = await ExportAPIFootballPrediction.updateMatchPrediction(matchToCalculate.matchItem);

        // Calculate Final predictionn
        matchToCalculate.matchItem = OddsBeaterUtils.updateMatchFinalPrediction(matchToCalculate.matchItem, predictionSources);

        // Save calculated match and remove it from the list of matches to predict
        let batch = admin.firestore().batch();

        // Update Predicted Matches
        // Only update if the prediction is not unpredictable
        if (matchToCalculate.matchItem.prediction?.calculatedPrediction !== MatchTipType.UNPREDICTABLE) {
            batch.set(admin.firestore().collection(OddsBeaterFirestoreCollectionNames.Table_OddsBeaterData).doc(OddsBeaterFirestoreCollectionNames.Doc_OddsBeaterDataPredictedMatches), {
                [matchToCalculate.leagueItem.id]: {
                    leagueData: matchToCalculate.leagueItem,
                    listMatches: {
                        [matchToCalculate.matchItem.id]: matchToCalculate.matchItem,
                    },
                },
            }, {merge: true});

            // Schedule a Notification
            let notifDateSchedule: Date = new Date(matchToCalculate.matchItem.matchObject?.fixture.date!);
            notifDateSchedule.setMinutes(notifDateSchedule.getMinutes() - 5);
            await OneSignalUtils.sendPushNotification("New Football Bet Tip",
                matchToCalculate.matchItem.matchObject?.teams.home.name + " - " + matchToCalculate.matchItem.matchObject?.teams.away.name,
                OddsBeaterUtils.generatePageMatchTipPath(matchToCalculate.matchItem, matchToCalculate.leagueItem.label), notifDateSchedule);
        }

        // Remove match from next matches
        batch.set(admin.firestore().collection(OddsBeaterFirestoreCollectionNames.Table_OddsBeaterData).doc(OddsBeaterFirestoreCollectionNames.Doc_OddsBeaterDataNextMatches), {
            [matchToCalculate.leagueItem.id]: {
                leagueData: matchToCalculate.leagueItem,
                listMatches: {
                    [matchToCalculate.matchItem.id]: admin.firestore.FieldValue.delete(),
                },
            },
        }, {merge: true});

        // Commit changes
        let dbResponse = await batch.commit();

        response.send(getQuickResponse(true, {
            dbResponse,
            matchToCalculate,
        }));
        return;
    } else {
        response.send(getQuickResponse(true, {message: "Didn't found any match to calculate prediction"}));
        return;
    }
});

