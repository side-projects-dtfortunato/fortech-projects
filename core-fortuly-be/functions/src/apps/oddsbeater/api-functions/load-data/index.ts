import * as functions from "firebase-functions";
import * as admin from "firebase-admin";
import {MatchOddsAvg, MatchPredictionStatus, NextMatchesByLeague} from "../../data-model/match-tip";
import {LeaguesAvailable} from "../../data-model/league-model";
import {OddsBeaterFirestoreCollectionNames} from "../../data-model/oddsbeater-firestore-collection-names";
import {getFixtureData, getFixturesLeague, getOddsByDate} from "../../api-football/endpoints";
import moment from 'moment';
import {OddsBeaterUtils} from "../../utils/oddsbeater-utils";
import {PredictionSourceData} from "../../prediction-sources/prediction-sources";
import {getQuickResponse} from "../../../../shareable/model-data/api-response.model";
import {getDocumentData} from "../../../../shareable/utils/utils";


export const findMatchesGoodOdds = functions.https.onRequest(async (request,
                                                                    response) => {
    /*let  listMatchesByLeague: NextMatchesByLeague = await getDocumentData(OddsBeaterFirestoreCollectionNames.Table_OddsBeaterData, OddsBeaterFirestoreCollectionNames.Doc_OddsBeaterDataPredictedMatches);

    if (!listMatchesByLeague) {
        listMatchesByLeague = {};
    }*/


    let date = new Date();
    date.setUTCDate(date.getUTCDate() + 1); // Tomorrow

    // Get odds of tomorrow matches
    let listOddsMatches: any = {};

    // Load Available leagues
    const availableLeagues = {...LeaguesAvailable};

    // Search next matches of each league
    let availableLeaguesKeys = Object.keys(availableLeagues);

    for (let i = 0; i < availableLeaguesKeys.length; i++) {
        let leagueId = availableLeaguesKeys[i];
        listOddsMatches = {
            ...listOddsMatches,
            [leagueId]: {
                leagueData: availableLeagues[leagueId],
                listMatches: {},
            },
        };

        // Download odds
        let apiResponse: any = await getOddsByDate({
            date: moment(date).format("yyyy-MM-DD"),
            leagueId: leagueId,
            season: OddsBeaterUtils.CURRENT_SEASON,
        });

        if (apiResponse && apiResponse.response && apiResponse.response.length > 0) {

            // Iterate matches
            apiResponse.response.forEach((matchData: any) => {

                // Calculate Odds Avg
                const matchOddsAvg: MatchOddsAvg = OddsBeaterUtils.calculateOddsAvgFixture(matchData);

                // Check if the odds are eligible to predict
                if (OddsBeaterUtils.isMatchOddsAvgEligibleToPedict(matchOddsAvg)) {

                    listOddsMatches = {
                        ...listOddsMatches,
                        [leagueId]: {
                            ...listOddsMatches[leagueId],
                            listMatches: {
                                ...listOddsMatches[leagueId].listMatches,
                                [matchData.fixture.id]: {
                                    id: matchData.fixture.id,
                                    oddsDetails: matchData,
                                    oddsAvg: matchOddsAvg,
                                    tips: {},
                                    predictionStatus: MatchPredictionStatus.PENDING,
                                },
                            },
                        },
                    };
                }
            });
        }
    }

    // Fill matches with details
    // listOddsMatches = await fillNextMatchesDetails(listOddsMatches);

    // Save data
    if (Object.keys(listOddsMatches).length > 0) {
        let dbResponse = await admin.firestore()
            .collection(OddsBeaterFirestoreCollectionNames.Table_OddsBeaterData)
            .doc(OddsBeaterFirestoreCollectionNames.Doc_OddsBeaterDataNextMatches)
            .set(listOddsMatches);

        response.send({
            data: listOddsMatches,
            db: dbResponse,
        });
    } else {
        response.send(getQuickResponse(false, {}, "Didnt found any match"));
    }

});


export const initializeSourcePredictions = functions.https.onRequest(async (request,
                                                                            response) => {
    let dbResponse = await admin
        .firestore()
        .collection(OddsBeaterFirestoreCollectionNames.Table_OddsBeaterData)
        .doc(OddsBeaterFirestoreCollectionNames.Doc_OddsBeaterDataPredictionsSources)
        .set(PredictionSourceData, {merge: true});

    response.send(dbResponse);
});


export const fillNextMatchesDetails = functions.https.onRequest(async (request,
                                                                       response) => {

    // Get Data
    let listMatches: NextMatchesByLeague = await getDocumentData(OddsBeaterFirestoreCollectionNames.Table_OddsBeaterData, OddsBeaterFirestoreCollectionNames.Doc_OddsBeaterDataNextMatches);

    if (!listMatches) {
        response.send(getQuickResponse(false, {}, "List matches are empty"));
    } else {
        let hasChanges = false;
        // Check for matches that needs details
        let listMatchesKeys = Object.keys(listMatches);
        for (let i = 0; i < listMatchesKeys.length; i++) {
            let leagueData = listMatches[listMatchesKeys[i]];

            // List of matches
            if (leagueData.listMatches
                && Object.keys(leagueData.listMatches).length > 0) {

                let apiResponseLeagueMatches: any = await getFixturesLeague({
                    league: leagueData.leagueData.id,
                    next: 20,
                    season: OddsBeaterUtils.CURRENT_SEASON,
                });

                let listLeagueMatchesIds = Object.keys(leagueData.listMatches);

                for (let matchIndex = 0; matchIndex < listLeagueMatchesIds.length; matchIndex++) {
                    let matchData = leagueData.listMatches[listLeagueMatchesIds[matchIndex]];

                    // Check if the match needs to be completed
                    if (!matchData.matchObject) {
                        // Search for the match data
                        if (apiResponseLeagueMatches && apiResponseLeagueMatches.response && apiResponseLeagueMatches.response.length > 0) {
                            apiResponseLeagueMatches.response.forEach((matchAPIItem: any) => {
                                if (matchAPIItem.fixture && matchAPIItem.fixture.id === matchData.id) {
                                    hasChanges = true;
                                    matchData.matchObject = matchAPIItem;
                                }
                            });
                        }

                        // If still didn't found the match, then get it directly from the API
                        if (!matchData.matchObject) {
                            let apiResponse: any = await getFixtureData(matchData.id);
                            if (apiResponse && apiResponse.response && apiResponse.response.length > 0) {
                                hasChanges = true;
                                matchData.matchObject = apiResponse.response[0];
                            }
                        }
                    }
                }
            }
        }

        // Update data per league (to avoid to loose loaded data in timeout functions)
        if (hasChanges) {
            hasChanges = false;
            await admin
                .firestore()
                .collection(OddsBeaterFirestoreCollectionNames.Table_OddsBeaterData)
                .doc(OddsBeaterFirestoreCollectionNames.Doc_OddsBeaterDataNextMatches)
                .set(listMatches, {merge: true});
        }

        response.send(listMatches);
    }
});


/*async function fillNextMatchesDetails(listMatches: NextMatchesByLeague): Promise<NextMatchesByLeague> {

    if (listMatches) {
        // Check for matches that needs details
        let listMatchesKeys = Object.keys(listMatches);
        for (let i = 0; i < listMatchesKeys.length; i++) {
            let leagueData = listMatches[listMatchesKeys[i]];

            // List of matches
            if (leagueData.listMatches
                && Object.keys(leagueData.listMatches).length > 0) {

                let apiResponseLeagueMatches: any = await getFixturesLeague({
                    league: leagueData.leagueData.id,
                    next: 20,
                    season: OddsBeaterUtils.CURRENT_SEASON,
                });

                let listLeagueMatchesIds = Object.keys(leagueData.listMatches);

                for (let matchIndex = 0; matchIndex < listLeagueMatchesIds.length; matchIndex++) {
                    let matchData = leagueData.listMatches[listLeagueMatchesIds[matchIndex]];

                    // Check if the match needs to be completed
                    if (!matchData.matchObject) {
                        // Search for the match data
                        if (apiResponseLeagueMatches && apiResponseLeagueMatches.response && apiResponseLeagueMatches.response.length > 0) {
                            apiResponseLeagueMatches.response.forEach((matchAPIItem: any) => {
                                if (matchAPIItem.fixture && matchAPIItem.fixture.id === matchData.id) {
                                    matchData.matchObject = matchAPIItem;
                                }
                            });
                        }

                        // If still didn't found the match, then get it directly from the API
                        if (!matchData.matchObject) {
                            let apiResponse: any = await getFixtureData(matchData.id);
                            if (apiResponse && apiResponse.response && apiResponse.response.length > 0) {
                                matchData.matchObject = apiResponse.response[0];
                            }
                        }

                        // Update predictio data
                        matchData = await ExportAPIFootballPrediction.updateMatchPrediction(matchData);
                    }
                }
            }
        }
    }

    return listMatches;
}*/