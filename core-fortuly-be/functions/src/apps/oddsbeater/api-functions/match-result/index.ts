import * as functions from "firebase-functions";
import {FinishedMatches, MatchDetailsObject, MatchTipData, NextMatchesByLeague} from "../../data-model/match-tip";
import {getDocumentData} from "../../../../shareable/utils/utils";
import {OddsBeaterFirestoreCollectionNames} from "../../data-model/oddsbeater-firestore-collection-names";
import {PredictionSourcesMap} from "../../prediction-sources/prediction-sources";
import {OddsBeaterUtils} from "../../utils/oddsbeater-utils";
import {getFixtureData} from "../../api-football/endpoints";
import * as admin from "firebase-admin";
import {getQuickResponse} from "../../../../shareable/model-data/api-response.model";

export const validatePredictedMatchesResults = functions.https.onRequest(async (request,
                                                                                response) => {

    // Get next predicted matches
    let nextMatches: NextMatchesByLeague = await getDocumentData(OddsBeaterFirestoreCollectionNames.Table_OddsBeaterData, OddsBeaterFirestoreCollectionNames.Doc_OddsBeaterDataPredictedMatches);
    let predictionSources: PredictionSourcesMap = await getDocumentData(OddsBeaterFirestoreCollectionNames.Table_OddsBeaterData, OddsBeaterFirestoreCollectionNames.Doc_OddsBeaterDataPredictionsSources);


    let finishedMatches: FinishedMatches = {};
    let matchesToRemove: any = {};

    let counterMatchesValidated: number = 0;

    let listLeagueKeys = Object.keys(nextMatches);
    for (let itLeagueIndex = 0; itLeagueIndex < listLeagueKeys.length; itLeagueIndex++) {
        let leagueItem = nextMatches[listLeagueKeys[itLeagueIndex]];

        if (leagueItem.listMatches) {
            let listMatchKeys = Object.keys(leagueItem.listMatches);
            for (let itMatchIndex = 0; itMatchIndex < listMatchKeys.length; itMatchIndex++) {
                let matchItem: MatchTipData = leagueItem.listMatches[listMatchKeys[itMatchIndex]];

                // Dummy Data to test an match result
                /*if (matchItem.matchObject) {
                    matchItem.matchObject.fixture.timestamp = Date.now() - (60000 * 120);
                    console.log("Match date: " + matchItem.matchObject?.fixture.date);
                }*/

                let isMatchFinished = OddsBeaterUtils.isMatchDateFinished(matchItem);

                // Get new fixture data from api
                if (isMatchFinished && counterMatchesValidated < 10) {
                    let apiResponse: {response: any[]} = await getFixtureData(matchItem.id) as any;

                    if (apiResponse.response && apiResponse.response.length > 0) {
                        let updatedMatchData: MatchDetailsObject = apiResponse.response[0];

                        // Dummy Data to test the end of a match
                        /*updatedMatchData.fixture.status = {
                            ...updatedMatchData.fixture.status,
                            short: "FT",
                            long: "Full Time",
                            elapsed: 90,
                        };
                        updatedMatchData.teams.away.winner = true; */

                        let matchStatus: string | undefined = updatedMatchData.fixture.status?.short.toUpperCase();
                        // Check if the match has finished
                        if (matchStatus === "FT" || updatedMatchData.fixture.status?.long.toLowerCase() === "match finished") {
                            // Remove match from predicted matches
                            matchesToRemove = {
                                ...matchesToRemove,
                                [leagueItem.leagueData.id]: {
                                    ...matchesToRemove[leagueItem.leagueData.id],
                                    listMatches: {
                                        ...matchesToRemove[leagueItem.leagueData.id]?.listMatches,
                                        [matchItem.id]: admin.firestore.FieldValue.delete(),
                                    },
                                },
                            };

                            // Update match item with result and final prediction results
                            matchItem.matchObject = updatedMatchData;
                            // Lets remove oddsdetails to reduce data consumption
                            matchItem.oddsDetails = {};
                            (matchItem.matchObject as any).score = {};
                            (matchItem.matchObject as any).players = {};
                            (matchItem.matchObject as any).events = {};
                            (matchItem.matchObject as any).statistics = {};
                            (matchItem.matchObject as any).lineups = {};

                            let processMatchResult = OddsBeaterUtils.processMatchResult(matchItem, updatedMatchData, predictionSources);

                            // Update data items
                            finishedMatches = {
                                ...finishedMatches,
                                [matchItem.id]: processMatchResult.matchTipData,
                            };

                            predictionSources = processMatchResult.predictionSources;
                            counterMatchesValidated = counterMatchesValidated + 1;
                        } else if (matchStatus === "PST" || OddsBeaterUtils.isMatchDateExpired(matchItem)) {
                            // Remove match from predicted matches
                            matchesToRemove = {
                                ...matchesToRemove,
                                [leagueItem.leagueData.id]: {
                                    ...matchesToRemove[leagueItem.leagueData.id],
                                    listMatches: {
                                        ...matchesToRemove[leagueItem.leagueData.id]?.listMatches,
                                        [matchItem.id]: admin.firestore.FieldValue.delete(),
                                    },
                                },
                            };
                        }
                    }
                }

            }
        }

    }

    // Update finished matches, predicted sources and remove predicted matches
    let batch = admin.firestore().batch();

    // Update finished matches
    batch.set(admin.firestore().collection(OddsBeaterFirestoreCollectionNames.Table_OddsBeaterData).doc(OddsBeaterFirestoreCollectionNames.Doc_OddsBeaterDataFinishedPredictedMatches), finishedMatches, {merge: true});

    // Update predicted sources
    batch.set(admin.firestore().collection(OddsBeaterFirestoreCollectionNames.Table_OddsBeaterData).doc(OddsBeaterFirestoreCollectionNames.Doc_OddsBeaterDataPredictionsSources), predictionSources, {merge: true});

    // Update predicted matches (with the removed matches)
    batch.set(admin.firestore().collection(OddsBeaterFirestoreCollectionNames.Table_OddsBeaterData).doc(OddsBeaterFirestoreCollectionNames.Doc_OddsBeaterDataPredictedMatches), matchesToRemove, {merge: true});

    // Commit changes
    let dbResponse: any = await batch.commit();

    await response.send(getQuickResponse(true, {
        dbResponse: dbResponse,
        finishedMatches: finishedMatches,
        predictionSources: predictionSources,
        matchesToRemove: matchesToRemove,
    }));


});

export const cleanOldMatchResults = functions.https.onRequest(async (request,
                                                                     response) => {
    const data: FinishedMatches = await getDocumentData(OddsBeaterFirestoreCollectionNames.Table_OddsBeaterData, OddsBeaterFirestoreCollectionNames.Doc_OddsBeaterDataFinishedPredictedMatches);
    const MAX_RESULTS_MATCHES = 150;

    if (Object.keys(data).length > MAX_RESULTS_MATCHES) {
        // Only keep the most 200 recent matches
        let updatedMatches: MatchTipData[] = Object.values(data)
            .sort((m1, m2) => {
            return m2.matchObject?.fixture.timestamp! - m1.matchObject?.fixture.timestamp!;
        }).filter((m1, index) => index <= MAX_RESULTS_MATCHES);

        let mapUpdatedMatches: FinishedMatches = {};
        updatedMatches.forEach((item) => {
            mapUpdatedMatches = {
                ...mapUpdatedMatches,
                [item.id]: item,
            };
        });
        const dbResponse = await admin.firestore().collection(OddsBeaterFirestoreCollectionNames.Table_OddsBeaterData).doc(OddsBeaterFirestoreCollectionNames.Doc_OddsBeaterDataFinishedPredictedMatches).set(mapUpdatedMatches);
        response.send(getQuickResponse(true, {dbResponse}));
    } else {
        response.send(getQuickResponse(true, {counter: Object.keys(data).length}));
    }
});