import * as functions from 'firebase-functions';
import {getFixturesByDate} from "../api-football/endpoints";
import moment from 'moment';



export const getFixtures = functions.https.onRequest(async (request,
                                                                     response) => {

    let date = new Date();
    date.setUTCDate(date.getUTCDate() + 1);
    let apiResponse = await getFixturesByDate(moment(date).format("yyyy-MM-DD"));

    response.send(apiResponse);
});



export const testAPI = functions.https.onRequest(async (request,
                                                                     response) => {



    let notifDateSchedule: Date = new Date("2024-02-24T19:45:00+00:00");
    response.send({
        toISOString: notifDateSchedule.toISOString(),
        toDateString: notifDateSchedule.toDateString(),
        toTimeString: notifDateSchedule.toTimeString(),
        toString: notifDateSchedule.toString(),
        toUTCString: notifDateSchedule.toUTCString(),
    });



    /*let date = new Date();
    date.setMinutes(date.getMinutes() + 10);

    const resp = await OneSignalUtils.sendPushNotification("Football Tip of the Day",
        "Metz x Lyon",
        "https://www.footballtips.app/match-tip/Ligue-1-Metz-Lyon-1045087", date);

    response.send(getQuickResponse(true, resp));*/



    // response.send(await ExportTest.exportListMatches("https://weworkremotely.com/categories/remote-full-stack-programming-jobs#job-listings"));
    // const predictedMatches: NextMatchesByLeague = await getDocumentData(OddsBeaterFirestoreCollectionNames.Table_OddsBeaterData, OddsBeaterFirestoreCollectionNames.Doc_OddsBeaterDataPredictedMatches);

    /*let matchTip: MatchTipData = predictedMatches[40].listMatches[881154];

    response.send(await ExportFootballPredictions.calculateMatchPrediction(matchTip));

    return;*/
    // Test get fixtures of a league
    /*let listLeaguesIds = Object.keys(LeaguesAvailable);

    response.send(apiResponse);*/
    // response.send(await ExportForebet.exportListMatches("https://m.forebet.com/en/football-tips-and-predictions-for-england/premier-league"));
    // response.send(await ExportPredictz.exportListMatches("https://www.predictz.com/predictions/england/premier-league/"));
    //response.send(await ExportOlbg.exportListMatches(PredictionSourceData.olbg.urlPerLeague["39"]));
    // response.send(await ExportMybets.exportListMatches(PredictionSourceData.mybets.urlPerLeague["default"]));

    /*let apiResponse = await getFixturesLeague({
        league: "39",
        next: 20,
        season: "2022",
    });

    response.send(apiResponse); */
});