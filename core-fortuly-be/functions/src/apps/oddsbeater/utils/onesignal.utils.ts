import * as OneSignal from '@onesignal/node-onesignal';

const ONESIGNAL_APP_ID = "f6cddeef-447b-434d-b1fa-2fc90a09f84c";
const configs = OneSignal.createConfiguration({
    userKey: "f6cddeef-447b-434d-b1fa-2fc90a09f84c",
    appKey: "OTQ0YmFjMjMtYWMxMC00MDhiLWIwNjktMjczYmJiN2NmZjgx",
});
const client = new OneSignal.DefaultApi(configs);

export class OneSignalUtils {
    static async sendPushNotification(
        notificationTitle: string,
        notificationMessage: string,
        dataOpenUrl: string,
        date?: Date // Optional parameter for scheduling
    ) {
        if (!client) {
            return;
        }

        // Create a notification object with the required properties
        const notification = new OneSignal.Notification();
        notification.app_id = ONESIGNAL_APP_ID;
        notification.name = notificationTitle;
        notification.headings = { en: notificationTitle };
        notification.contents = { en: notificationMessage };
        notification.data = {
            openUrl: dataOpenUrl,
        };
        notification.url = dataOpenUrl;
        notification.included_segments = ["All"];

        // If a date is provided, set the send_after property
        if (date) {
            notification.send_after = date.toISOString();
        }

        // Send the notification using the client object
        try {
            const response = await client.createNotification(notification);
            console.log('Notification sent successfully:', response);
            return response;
        } catch (error) {
            console.error('Error sending notification:', error);
            return error;
        }
    }
}