import {
    MatchDetailsObject,
    MatchOddsAvg,
    MatchTipData,
    MatchTipType,
    NextMatchesByLeague,
} from "../data-model/match-tip";
import {PredictionSourcesMap, PredictionSource} from "../prediction-sources/prediction-sources";
import {LeagueAvailableModel} from "../data-model/league-model";

export class OddsBeaterUtils {
    static MinOddsToPredict = 1.7;
    static CURRENT_SEASON = "2024";

    static isMatchOddsAvgEligibleToPedict(matchOdds: MatchOddsAvg) {
        return matchOdds.home >= this.MinOddsToPredict
            && matchOdds.draw >= this.MinOddsToPredict
            && matchOdds.away >= this.MinOddsToPredict;
    }

    static calculateOddsAvgFixture(matchData: any): MatchOddsAvg {
        let bookmakersCounter: number = 0;
        let totalHomeOdds: number = 0;
        let totalDrawOdds: number = 0;
        let totalAwayOdds: number = 0;

        if (matchData.bookmakers && matchData.bookmakers.length > 0) {
            matchData.bookmakers.forEach((bookmaker: any) => {
                bookmakersCounter ++;
                // Search for bet = 1
                bookmaker.bets.forEach((betItem: any) => {
                    if (betItem.id === 1) {
                        betItem.values.forEach((betValue: {
                            value: "Home" | "Draw" | "Away",
                            odd: string,
                        }) => {
                            switch (betValue.value) {
                                case "Home": totalHomeOdds += Number(betValue.odd); break;
                                case "Draw": totalDrawOdds += Number(betValue.odd); break;
                                case "Away": totalAwayOdds += Number(betValue.odd); break;
                            }
                        });
                    }
                })
            });
        }
        // Calculate avg
        return {
            home: totalHomeOdds / bookmakersCounter,
            away: totalAwayOdds / bookmakersCounter,
            draw: totalDrawOdds / bookmakersCounter,
        };
    }

    static isMatchDateFinished(matchItem: MatchTipData): boolean {
        let isPossiblyFinished: boolean = false;

        if (matchItem && matchItem.matchObject && matchItem.matchObject.fixture && matchItem.matchObject.fixture.timestamp) {
            isPossiblyFinished = (matchItem.matchObject.fixture.timestamp * 1000) + (60000 * 115) < Date.now(); // Compare with current date + 115minutes
        }

        return isPossiblyFinished;
    }

    static isMatchDateExpired(matchItem: MatchTipData): boolean {
        let isMatchExpired: boolean = false;

        if (matchItem && matchItem.matchObject && matchItem.matchObject.fixture && matchItem.matchObject.fixture.timestamp) {
            isMatchExpired = (matchItem.matchObject.fixture.timestamp * 1000) + (60000 * 60 * 12) > Date.now(); // Compare with current date + 12 hours
        }

        return isMatchExpired;
    }

    static updateMatchFinalPrediction(matchItem: MatchTipData, predictionSources: PredictionSourcesMap): MatchTipData {

        let cunterResultType: {home: number, draw: number, away: number} = {
            home: 0,
            draw: 0,
            away: 0,
        };

        if (matchItem.tips) {
            Object.values(matchItem.tips).forEach((sourceTip) => {
                // Calculate the incremental value
                let predictionSource: PredictionSource = predictionSources[sourceTip.sourceId];
                let incrementalValue: number = 0;

                if (predictionSource.results) {
                    incrementalValue = predictionSource.results.numCorrectPredictions / (predictionSource.results.numCorrectPredictions + predictionSource.results.numWrongPredictions);
                }

                if (incrementalValue) {
                    switch (sourceTip.tipSuggestion) {
                        case MatchTipType.HOME: cunterResultType.home = cunterResultType.home + incrementalValue; break;
                        case MatchTipType.DRAW: cunterResultType.draw = cunterResultType.draw + incrementalValue; break;
                        case MatchTipType.AWAY: cunterResultType.away = cunterResultType.away + incrementalValue; break;
                    }
                }

            });



            if (cunterResultType.home >= (cunterResultType.away + cunterResultType.draw)) {
                matchItem.prediction = {
                    calculatedPrediction: MatchTipType.HOME,
                }
            } else if (cunterResultType.away >= (cunterResultType.home + cunterResultType.draw)) {
                matchItem.prediction = {
                    calculatedPrediction: MatchTipType.AWAY,
                }
            } else if (cunterResultType.draw > (cunterResultType.home + cunterResultType.away)) {
                matchItem.prediction = {
                    calculatedPrediction: MatchTipType.DRAW,
                }
            } else {
                matchItem.prediction = {
                    calculatedPrediction: MatchTipType.UNPREDICTABLE,
                }
            }

            matchItem.prediction.homeValuePrediction = cunterResultType.home;
            matchItem.prediction.drawValuePrediction = cunterResultType.draw;
            matchItem.prediction.awayValuePrediction = cunterResultType.away;
        }


        return matchItem;
    }

    static processMatchResult(matchTipData: MatchTipData, updatedMatchDetails: MatchDetailsObject, predictionSources: PredictionSourcesMap): {
        matchTipData: MatchTipData,
        predictionSources: PredictionSourcesMap,
    } {
        let matchResult: MatchTipType = MatchTipType.DRAW;
        let winningOdds: number = matchTipData.oddsAvg!.draw;

        if (updatedMatchDetails.teams.home.winner) {
            matchResult = MatchTipType.HOME;
            winningOdds = matchTipData.oddsAvg!.home;
        } else if (updatedMatchDetails.teams.away.winner) {
            matchResult = MatchTipType.AWAY;
            winningOdds = matchTipData.oddsAvg!.away;
        }

        if (matchTipData.prediction) matchTipData.prediction.finalResult = matchResult;

        // Get the winning odds


        // Update predictionSources
        Object.keys(predictionSources).forEach((mapKey) => {

            if (mapKey.toLowerCase() !== "uid") {
                let itemSource: PredictionSource = predictionSources[mapKey];

                if (!itemSource.results) {
                    itemSource.results = {
                        numWrongPredictions: 0,
                        numCorrectPredictions: 0,
                        numInvested: 0,
                        numProfitOdds: 0,
                    };
                }

                // Check if needs to initialize profits
                if (!itemSource.results.numInvested) {
                    itemSource.results.numInvested = 0;
                }
                if (!itemSource.results.numProfitOdds) {
                    itemSource.results.numProfitOdds = 0;
                }

                // Check if has predicted the match
                if (matchTipData.tips && matchTipData.tips[itemSource.id]) {
                    // Add investment
                    itemSource.results.numInvested += 1;

                    if (matchTipData.tips[itemSource.id].tipSuggestion === matchResult) {
                        // Was right, increment the counter
                        itemSource.results.numCorrectPredictions ++;
                        itemSource.results.numProfitOdds += winningOdds;
                    } else {
                        // Was wrong, increment the counter
                        itemSource.results.numWrongPredictions ++;
                    }
                }
            }
        });

        return {
            matchTipData: matchTipData,
            predictionSources: predictionSources,
        }
    }

    static exportListMatchesFromMatchesByLeague(nextMatchesByLeague: NextMatchesByLeague): {matchItem: MatchTipData, leagueItem: LeagueAvailableModel}[] {
        let listMatches: {matchItem: MatchTipData, leagueItem: LeagueAvailableModel}[] = [];

        if (nextMatchesByLeague) {
            Object.values(nextMatchesByLeague).forEach((leagueItem) => {
                if (leagueItem.listMatches && Object.values(leagueItem.listMatches).length > 0) {

                    Object.values(leagueItem.listMatches).forEach((matchItem) => {
                        listMatches.push({
                            leagueItem: leagueItem.leagueData,
                            matchItem,
                        });
                    });
                }
            });
        }

        return listMatches;
    }

    static generatePageMatchTipPath(match: MatchTipData, leagueLabel: string) {

        return "https://footballtips.app/match-tip/"
            + leagueLabel.replace(" ", "-")
            + "-" + match.matchObject!.teams.home.name.replace(" ", "-").replace("/", "")
            + "-" + match.matchObject!.teams.away.name.replace(" ", "-").replace("/", "")
            + "-" + match.id;
    }
}
