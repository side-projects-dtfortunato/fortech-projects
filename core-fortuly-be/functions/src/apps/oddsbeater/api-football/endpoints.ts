var unirest = require("unirest");

const BASE_URL = "https://api-football-v1.p.rapidapi.com/v3"
// const API_KEY = "bb41e00368mshca7d24ebd030a85p1ded7djsnfe47c6294f0b"; // fortuly.apps@gmail.com
const API_KEY = "04101e7059msh7c2877f14ae5dd0p14a81bjsnec7e78a5e925"; // footballtipsapp@gmail.com

export function getAPIRequest(pathUrl: string, query: any) {
    return new Promise(async (resolve, reject) => {
        let req: any = unirest("GET", BASE_URL + pathUrl);

        req.query(query);

        req.headers({
            "X-RapidAPI-Key": API_KEY,
            "X-RapidAPI-Host": "api-football-v1.p.rapidapi.com",
            "useQueryString": true,
        });

        req.end(function (res: any) {
            if (res.error) {
                reject(res.error);
            } else {
                //console.log(res.body);
                resolve(res.body);
            }
        });
    });
}


/**
 * Get odds for a specific date
 * @param day
 * @param month
 * @param year
 */
export async function getOddsByDate(params: {date: string, leagueId: string, season: string}) {
    return await getAPIRequest("/odds", {
        "date": params.date,
        "bet": "1",
        "league": params.leagueId,
        "season": params.season,
    });
}


export async function getOddsBets() {
    return await getAPIRequest("/odds/bets", {});
}

export async function getFixturesByDate(date: string) {
    return await getAPIRequest("/fixtures", {
        "date": date,
    });
}

export async function getFixturePrediction(fixtureId: string) {
    return await getAPIRequest("/predictions", {
        "fixture": fixtureId,
    });
}

export async function getFixtureData(fixtureId: string) {
    return await getAPIRequest("/fixtures", {
        "id": fixtureId,
    });
}



export async function getFixturesLeague(params: {
    league?: string, // league ids
    next?: number, //next matches
    season: string,
}) {
    return await getAPIRequest("/fixtures", params);
}
