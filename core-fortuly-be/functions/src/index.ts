import * as functions from 'firebase-functions';
import * as admin from 'firebase-admin';
import * as OddsBeaterLoadData from './apps/oddsbeater/api-functions/load-data';
import * as OddsBeaterMatchResult from './apps/oddsbeater/api-functions/match-result';
import * as OddsBeaterCentralMatchPrediction from './apps/oddsbeater/api-functions/central-match-prediction';
import * as OddsBeaterAPI from './apps/oddsbeater/api-functions';

// Start writing Firebase Functions
// https://firebase.google.com/docs/functions/typescript



// Init App
admin.initializeApp(functions.config().firebase);


// Odds Beater
//export const getOddsToday = OddsBeaterAPI.getOddsToday;
//export const getFixtures = OddsBeaterAPI.getFixtures;
export const testAPI = OddsBeaterAPI.testAPI;

// Odds Beater - Load Data
export const findMatchesGoodOdds = OddsBeaterLoadData.findMatchesGoodOdds;
export const fillNextMatchesDetails = OddsBeaterLoadData.fillNextMatchesDetails;
export const initializeSourcePredictions = OddsBeaterLoadData.initializeSourcePredictions;

// Odds Beater - Calculate Predictionns
export const validatePredictedMatchesResults = OddsBeaterMatchResult.validatePredictedMatchesResults
export const cleanOldMatchResults = OddsBeaterMatchResult.cleanOldMatchResults

// Calculate Prediction
export const calculateNextMatchPrediction = OddsBeaterCentralMatchPrediction.calculateNextMatchPrediction



