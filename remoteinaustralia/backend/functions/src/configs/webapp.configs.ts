
export const PRODUCTION_GCLOUD_PROJECT = "remoteinaustralia";

export const WEBAPP_CONFIGS = {
    LOGO_URL: "https://www.remoteinaustralia.com/images/logo-white-bg.jpg",
    SITE_TAGLINE: "",
    SITE_NAME: "RemoteInAustralia.com",
    WEBSITE_URL: "https://remoteinaustralia.com",
    COMPANY_ADDRESS: "3030 Coimbra (Portugal)",
    COMPANY_EMAIL: "contact@remoteinaustralia.com",
    POST_PREFIX_URL: "https://www.remoteinaustralia.com/post/",
    PROFILE_PREFIX_URL: "https://www.remoteinaustralia.com/public-profile/",
    UNSUBSCRIBE_URL: `https://us-central1-${PRODUCTION_GCLOUD_PROJECT}.cloudfunctions.net/unsubscribeEmail?uid=`,


    CUSTOM_CONFIGS: {
        openPushInApp: true,
    } as any,

    // Account: contact@sidehustlify.com - Di.....oc7
    IMAGE_KIT_API: {
        publicKey : "TODO", // "public_QPb3OvUFUKRLd2VLZtV+ExCVDTo=",
        privateKey : "TODO", //"private_z60vq5B2MydsmNQ+c2dMsOG0RBg=",
        urlEndpoint : "TODO", //"https://ik.imagekit.io/sidehustlify",
    },

    STRIPE_API: {
        prod: "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",
        dev: "sk_test_51PueA3HT2hxH9IRDdlrmFSjPbHmjQ1An0PNkz74ZkAOw50c1MSK5nG0MGBWOvbm5RjkBDgG4VXnMDZ2yRBWJGtpA00wlrvUV9J",
        webhook_dev: "whsec_jySvwwZUSe3BLDFdJPYWn6c2vABfZbDd",
        webhook_prod: "whsec_mzuw5CDnUUrQ0xYnzx57xVYeTVW8AsAS",
    },

    ONESIGNAL_CONFIGS: {
        apiKey: "NTg2ZjIwNWUtNGZiZC00MmRlLTk3NGQtMzVmMGU5N2QyYTc2",
        appId: "2188fcc3-f899-455b-9ae6-07880d32d2fd"
    },

    SOCIAL_NETWORK_API_KEY: {
        metaAccessToken: "EAAIRD7jjkskBO2j05bdLCwhEWvBj3MwIVF6wYDlhIPnATytOc6BYBixgfSWrA5heJ3r6CqQ8tsnmbZAwTBqmMlCkyzUdd47tV2FdFUZBrno7c7beZByzcWGyhEtBGhrkGZA8K2dMwM5vAffeZAORPZAt8RX4tRK0UliWK0NY7h5PQt0umuTzKWKwDNf32qZCgaZAY8B0Teaw",
        facebookPageId: "391981720675478",
        instagramId: "17841437369264473",
        threadsAPI: {
            clientId: "8961865517180954",
            clientSecret: "1a2f11fa3191f0ff5270568528fface4",
            redirectUri: `https://us-central1-${PRODUCTION_GCLOUD_PROJECT}.cloudfunctions.net/threadsAPIHandleAuthCallback`,
        },
        redditAPI: {
            clientId: "4AipG2HfGPnuOhtXtOYEmA",
            clientSecret: "9swPagUhH079Yq3uzQjyimQssUYhow",
            username: "FortulyApps",
            password: "d****c7",
            subReddit: "",
        },
        bluesky: {
            username: "remoteinaustralia.bsky.social",
            password: "divadlooc7",
        },
        telegram: {
            channel: "@remoteinaustralia",
            botToken: "8110422203:AAHDz-MOiE3Q674avdWb2AGX4QG8IbuLv_o",
        }
    }
}