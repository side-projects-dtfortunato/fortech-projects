import * as functions from "firebase-functions";
import {ArticlesDigestUtils} from "../utils/articles-digest.utils";

export const testFunction = functions.https.onRequest(
    async (req, resp) => {

        const fetchedArticles: any = await ArticlesDigestUtils.getMostRecentArticles();
        resp.send({fetchedArticles});

    });