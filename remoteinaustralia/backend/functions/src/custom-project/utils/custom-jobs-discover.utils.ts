import {
    JobDetailsModel,
    LinkedinSourceModel
} from "../../shareable-firebase-backend/base-projects/jobs-discover/shared-jobs-discover.data";


/**
 * Regions ids:
 * - UK: 101165590
 * - Australia: 101452733
 * - USA: 103644278
 * - EU: 91000002
 */
/*export const LINKEDIN_SOURCES: LinkedinSourceModel[] = [
    // Australia
    {
        id: "22",
        category: "Software Development",
        region: "Australia",
        url: "https://linkedin.com/jobs/search?keywords=Software Engineer&geoId=101452733&f_TPR=&f_WT=2&f_E=2%2C3%2C4%2C5&position=1&pageNum=0",
    },
    {
        id: "23",
        category: "Marketing",
        region: "Australia",
        url: "https://linkedin.com/jobs/search?keywords=Marketing&geoId=101452733&f_TPR=&f_WT=2&f_E=2%2C3%2C4%2C5&position=1&pageNum=0",
    },
    {
        id: "24",
        category: "Customer Service",
        region: "Australia",
        url: "https://linkedin.com/jobs/search?keywords=Customer Service&geoId=101452733&f_TPR=&f_WT=2&f_E=2%2C3%2C4%2C5&position=1&pageNum=0",
    },
    {
        id: "25",
        category: "Design",
        region: "Australia",
        url: "https://linkedin.com/jobs/search?keywords=Design&geoId=101452733&f_TPR=&f_WT=2&f_E=2%2C3%2C4%2C5&position=1&pageNum=0",
    },
    {
        id: "26",
        category: "Finance",
        region: "Australia",
        url: "https://linkedin.com/jobs/search?keywords=Finance&geoId=101452733&f_TPR=&f_WT=2&f_E=2%2C3%2C4%2C5&position=1&pageNum=0",
    },
    {
        id: "27",
        category: "HR",
        region: "Australia",
        url: "https://linkedin.com/jobs/search?keywords=HR&geoId=101452733&f_TPR=&f_WT=2&f_E=2%2C3%2C4%2C5&position=1&pageNum=0",
    },
    {
        id: "28",
        category: "Project Management",
        region: "Australia",
        url: "https://linkedin.com/jobs/search?keywords=Project Manager&geoId=101452733&f_TPR=&f_WT=2&f_E=2%2C3%2C4%2C5&position=1&pageNum=0",
    },
    {
        id: "29",
        category: "Data Analysis",
        region: "Australia",
        url: "https://linkedin.com/jobs/search?keywords=Data Analysis&geoId=101452733&f_TPR=&f_WT=2&f_E=2%2C3%2C4%2C5&position=1&pageNum=0",
    },
    {
        id: "30",
        category: "Sales & Business",
        region: "Australia",
        url: "https://linkedin.com/jobs/search?keywords=Sales Business&geoId=101452733&f_TPR=&f_WT=2&f_E=2%2C3%2C4%2C5&position=1&pageNum=0",
    },
    {
        id: "31",
        category: "Administrative",
        region: "Australia",
        url: "https://linkedin.com/jobs/search?keywords=Administrative&geoId=101452733&f_TPR=&f_WT=2&f_E=2%2C3%2C4%2C5&position=1&pageNum=0",
    },

]*/

export const ALLOWED_WORK_MODE = ["REMOTE"];
export const JOB_REGIONS = [
    {
        regionLabel: "Australia",
        regionId: "101452733"
    }
] as {regionLabel: string, regionId: string}[];

export const JOB_CATEGORIES = [
    {
        jobCategoryLabel: "Software Development",
        jobCategoryQuery: "Software Engineer"
    },
    {
        jobCategoryLabel: "Marketing",
        jobCategoryQuery: "Marketing"
    },
    {
        jobCategoryLabel: "Customer Service",
        jobCategoryQuery: "Customer Service"
    },
    {
        jobCategoryLabel: "Design",
        jobCategoryQuery: "Design"
    },
    {
        jobCategoryLabel: "Finance",
        jobCategoryQuery: "Finance"
    },
    {
        jobCategoryLabel: "HR",
        jobCategoryQuery: "HR"
    },
    {
        jobCategoryLabel: "Project Management",
        jobCategoryQuery: "Project Manager"
    },
    {
        jobCategoryLabel: "Data Analysis",
        jobCategoryQuery: "Data Analysis"
    },
    {
        jobCategoryLabel: "Sales & Business",
        jobCategoryQuery: "Sales Business"
    },
    {
        jobCategoryLabel: "Administrative",
        jobCategoryQuery: "Administrative"
    },
] as {jobCategoryLabel: string, jobCategoryQuery: string}[];


export class CustomJobsDiscoverUtils {


    static getIsValidRule(): string | undefined
    {
        return undefined;
    }

    static async importJobsFromRemoteJobsHub(): Promise<JobDetailsModel[]> {
        return [];
    }

}