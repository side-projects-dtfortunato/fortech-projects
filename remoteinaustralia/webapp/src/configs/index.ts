
export default class RootConfigs {
    static ENVIRONMENT_DEV = false;
    static BASE_URL = "https://www.remoteinaustralia.com";
    static SITE_NAME = "RemoteInAustralia.com";
    static SITE_TITLE = "Remote Jobs in Australia | Work From Home Opportunities 2024";
    static SITE_DESCRIPTION = "Find the best remote jobs in Australia. Browse verified work-from-home positions, remote work guides, and expert tips for landing your dream remote role in Australia.";
    static CURRENT_NICHE_SITE = RootConfigs.SITE_NAME;
    static CONTACT_EMAIL = "contact@remoteinaustralia.com";
    static SITE_TOPIC = "Remote In Australia";
    static THEME = {
        PRIMARY_COLOR: "blue",
    }
    static META_CONFIGS: any = {
        updateNicheBackend: false,
        disableGoogleSignin: true,
        disableCookies: true,
        disableAppStoreBanner: false,
        disableGoogleAds: false,
        enableJobSearch: true,
    }
    static APP_STORE_LINKS = {
        androidAppId: "remoteinaustralia.com",
        iosAppId: "6737427023",
    }
    static SOCIAL_NETWORKS: {
        [socialId: string]: {
            iconUrl: string,
            label: string,
            link: string,
        },
    } = {
        facebook: {
            label: "Facebook",
            link: "https://www.facebook.com/profile.php?id=61569097752032",
            iconUrl: "/images/ic_social_facebook.png",
        },
        instagram: {
            label: "Instagram",
            link: "https://www.instagram.com/marketingremotejobs.app/",
            iconUrl: "/images/ic_social_instagram.png",
        },
        threads: {
            label: "Threads",
            link: "https://www.threads.net/@marketingremotejobs.app",
            iconUrl: "/images/ic_social_threads.png",
        },
        bluesky: {
            label: "Bluesky",
            link: "https://remoteinaustralia.bsky.social",
            iconUrl: "/images/ic_social_bluesky.png",
        },
        telegram: {
            label: "Telegram Community",
            link: "https://t.me/remoteinaustralia",
            iconUrl: "/images/ic_social_threads.png",
        }
    }
    static FIREBASE_CONFIG = this.ENVIRONMENT_DEV ?

        // DEV Config - // TODO Change it
        {
            apiKey: "AIzaSyCB2yrNkEHbpShTH_xwHOzcl2mXJT5i1K8",
            authDomain: "fortuly-qa-env.firebaseapp.com",
            projectId: "fortuly-qa-env",
            storageBucket: "fortuly-qa-env.appspot.com",
            messagingSenderId: "650128691290",
            appId: "1:650128691290:web:69f8a283090d23e0454863",
            measurementId: "G-V6XN6K51VZ"
        } :
        // PROD
        {
            apiKey: "AIzaSyAnWwT1Sn73H5GdlSrtm2i6GKNNSJxG-3o",
            authDomain: "remoteinaustralia.firebaseapp.com",
            databaseURL: "https://remoteinaustralia-default-rtdb.firebaseio.com",
            projectId: "remoteinaustralia",
            storageBucket: "remoteinaustralia.appspot.com",
            messagingSenderId: "1067795924909",
            appId: "1:1067795924909:web:3670b83640088baeae4324",
            measurementId: "G-B409NZRZP5"
        };


    static IMAGEKIT_API = {
        publicKey : "public_OurCtnmj0jWqjVslk5YDfKE4aHQ=",
        privateKey : "private_vRUtPf6HGOj68f5rZZN8u9RHfiQ=",
        urlEndpoint : "https://ik.imagekit.io/fortulyapps",
    }

    static ONE_SIGNAL_API = "2188fcc3-f899-455b-9ae6-07880d32d2fd";
}
