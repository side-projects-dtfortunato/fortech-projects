import {JobListModel, JobsDiscoverDataUtils
} from "../../../components/react-shared-module/base-projects/jobs-discover/jobs-discover-data.model";
import CustomRootLayoutComponent from "../../../components/app-components/root/CustomRootLayout.component";
import JobCategoryHeader
    from "../../../components/react-shared-module/base-projects/jobs-discover/ui/job-category-header.component";
import JobListItemComponent
    from "../../../components/react-shared-module/base-projects/jobs-discover/ui/job-list-item.component";
import {PageHeadProps} from "../../../components/react-shared-module/ui-components/root/RootLayout.component";
import RootConfigs from "../../../configs";
import EmptyListMessageComponent
    from "../../../components/react-shared-module/ui-components/shared/EmptyListMessage.component";
import {UIHelper} from "../../../components/ui-helper/UIHelper";

export async function getStaticPaths() {
    let listPaths: any[] = [];

    listPaths = UIHelper.getJobsCategories()
        .map((category) => {
            return {
                params: {
                    category: category,
                },
            }
        });

    return {
        paths: listPaths,
        fallback: true, // can also be true or 'blocking'
    }
}

export async function getStaticProps(context: any) {
    const category: string = context.params.category as string;

    let listJobs: JobListModel[] | undefined = await JobsDiscoverDataUtils.fetchFilteredJobs({
        category: category,
        remoteRegion: "",
    });

    if (!listJobs) {
        listJobs = [];
    }




    return {
        props: {
            listJobs: listJobs,
            category: category,
        },
        revalidate: 60, // each 60 seconds
    };

}


export default function JobByCategory(props: {listJobs: JobListModel[], category: string}) {

    function renderListJobs() {
        if (props.listJobs && props.listJobs.length > 0) {
            return props.listJobs.map((job) => {
                return <JobListItemComponent key={job.uid!} jobData={job}/>
            });
        } else {
            return <EmptyListMessageComponent message={"We don't have any remote job for this category. Please, come back tomorrow."} />
        }

    }

    const pageHeadProps: PageHeadProps = {
        title: `Latest Remote Jobs in ${props.category}`,
        description: generateSEODescription({
            category: props.category,
            jobCount: props.listJobs ? props.listJobs.length : 0,
        }),
        imgUrl: RootConfigs.BASE_URL + "/images/logo-white-bg.png",
        publishedTime: props.listJobs && props.listJobs.length > 0 ? new Date(props.listJobs[0].createdAt!).toString() : new Date().toString(),
    }

    return (
        <CustomRootLayoutComponent pageHeadProps={pageHeadProps}>
            <JobCategoryHeader category={props.category} />

            <div className='flex flex-col'>
                {renderListJobs()}
            </div>
        </CustomRootLayoutComponent>
    )
}


interface SEODescriptionParams {
    category: string;
    jobCount?: number;
    location?: string;
    updateFrequency?: string;
    topCompanies?: string[];
}

function generateSEODescription({
                                    category,
                                    jobCount,
                                    location = 'worldwide',
                                    updateFrequency = 'daily',
                                    topCompanies = [],
                                }: SEODescriptionParams): string {
    let description = `Explore the latest remote ${category} jobs ${location}.`;

    if (jobCount !== undefined) {
        description += ` Find ${jobCount}+ open positions`;
    } else {
        description += ' Discover numerous open positions';
    }

    description += ` updated ${updateFrequency}.`;

    if (topCompanies.length > 0) {
        const companiesList = topCompanies.slice(0, 3).join(', ');
        description += ` Top companies like ${companiesList} are hiring now.`;
    }

    description += ' Start your remote career today!';

    // Ensure the description doesn't exceed a reasonable length for SEO
    return description.slice(0, 160);
}