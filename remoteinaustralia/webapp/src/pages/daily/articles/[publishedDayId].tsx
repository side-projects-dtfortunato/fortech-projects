import {
    FirebaseClientFirestoreUtils,
    getAPIDocument
} from "../../../components/react-shared-module/logic/shared-data/firebase.utils";
import {
    SharedFirestoreCollectionDB
} from "../../../components/react-shared-module/logic/shared-data/datamodel/shared-firestore-collections";
import {
    DailyPublishedArticlesModel
} from "../../../components/react-shared-module/base-projects/articles-digest/data/model/DailyPublishedArticles.model";
import CustomRootLayoutComponent from "../../../components/app-components/root/CustomRootLayout.component";
import RootConfigs from "../../../configs";
import NotFoundContentComponent
    from "../../../components/react-shared-module/ui-components/shared/NotFoundContent.component";
import ArticlesDigestListDailyItemsComponent
    from "../../../components/react-shared-module/base-projects/articles-digest/ui-components/articles-digest-list-daily-items.component";
import NewsletterSignupListBannerComponent
    from "../../../components/react-shared-module/ui-components/newsletter/shared/newsletter-signup-list-banner.component";
import {SortListTypes} from "../../../components/react-shared-module/ui-components/shared/SharedSortListBy.component";

export async function getStaticPaths() {
    let listDailyPublishedArticles: DailyPublishedArticlesModel[] | undefined = await FirebaseClientFirestoreUtils.getDocumentsWithPagination(SharedFirestoreCollectionDB.ArticlesDigestProject.DailyPublishedArticles, 15, "publishedDayId");
    let listPaths: any[] = [];

    if (listDailyPublishedArticles) {
        listPaths = listDailyPublishedArticles
            .map((dailyPublishedArticles) => {
                return {
                    params: {
                        publishedDayId: dailyPublishedArticles.publishedDayId.toString(),
                    },
                }
            });
    }

    return {
        paths: listPaths,
        fallback: true, // can also be true or 'blocking'
    }
}

export async function getStaticProps(context: any) {
    const publishedDayId: string = context.params.publishedDayId as string;

    const dailyPublishedArticles: DailyPublishedArticlesModel | undefined = await getAPIDocument(SharedFirestoreCollectionDB.ArticlesDigestProject.DailyPublishedArticles, publishedDayId);


    if (dailyPublishedArticles) {
        return {
            props: {
                dailyPublishedArticles: dailyPublishedArticles,
            },
            revalidate: 60, // each 60 seconds
        };
    } else {
        return {
            props: {
            },
            revalidate: 60, // each 60 seconds
        }
    }

}


export default function DailyArticlesPage(props: {dailyPublishedArticles?: DailyPublishedArticlesModel}) {

    if (props.dailyPublishedArticles) {
        return (
            <CustomRootLayoutComponent>
                <div className='flex flex-col items-center mb-8 w-full'>
                    <NewsletterSignupListBannerComponent title={"Subscribe our Newsletter"} />
                </div>
                <ArticlesDigestListDailyItemsComponent publishedDayArticles={props.dailyPublishedArticles} linkable={false} sortBy={SortListTypes.MOST_RECENT} />
            </CustomRootLayoutComponent>
        )
    } else {
        return (
            <CustomRootLayoutComponent pageHeadProps={{
                title: RootConfigs.SITE_TITLE + " - Page Not Found",
                description: RootConfigs.SITE_DESCRIPTION,
            }}>
                <NotFoundContentComponent/>
            </CustomRootLayoutComponent>
        )
    }
}