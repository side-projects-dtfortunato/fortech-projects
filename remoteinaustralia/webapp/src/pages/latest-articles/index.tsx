import {
    FirebaseClientFirestoreUtils
} from "../../components/react-shared-module/logic/shared-data/firebase.utils";
import {
    SharedFirestoreCollectionDB
} from "../../components/react-shared-module/logic/shared-data/datamodel/shared-firestore-collections";
import {
    DailyPublishedArticlesModel
} from "../../components/react-shared-module/base-projects/articles-digest/data/model/DailyPublishedArticles.model";
import CustomRootLayoutComponent from "../../components/app-components/root/CustomRootLayout.component";
import ArticlesListPaginationComponent
    from "../../components/react-shared-module/base-projects/articles-digest/ui-components/articles-list-pagination.component";
import NewsletterPopupComponent
    from "../../components/react-shared-module/ui-components/newsletter/shared/newsletter-popup.component";
import NewsletterSignupListBannerComponent
    from "../../components/react-shared-module/ui-components/newsletter/shared/newsletter-signup-list-banner.component";


const ITEMS_PER_PAGE = 2;

export async function getStaticProps() {
    const listPublishedDays: DailyPublishedArticlesModel[] = await FirebaseClientFirestoreUtils
        .getDocumentsWithPagination(SharedFirestoreCollectionDB.ArticlesDigestProject.DailyPublishedArticles, ITEMS_PER_PAGE, "publishedDayId");

    return {
        props: {
            listPublishedDays,
        },
        revalidate: 30,
    }
}

export default function Home(props: {listPublishedDays: DailyPublishedArticlesModel[]}) {

    return (
        <CustomRootLayoutComponent>
            <div className='flex flex-col items-center w-full'>
                <NewsletterSignupListBannerComponent title={"Subscribe our Newsletter"} />
            </div>
            <ArticlesListPaginationComponent preloadedData={props.listPublishedDays} itemsPerPage={ITEMS_PER_PAGE} />

            {/* Popups */}
            <NewsletterPopupComponent />
        </CustomRootLayoutComponent>
    )
}