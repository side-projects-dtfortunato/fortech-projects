import {
    DailyPublishedArticlesModel
} from "../../components/react-shared-module/base-projects/articles-digest/data/model/DailyPublishedArticles.model";
import {FirebaseClientFirestoreUtils} from "../../components/react-shared-module/logic/shared-data/firebase.utils";
import {
    SharedFirestoreCollectionDB
} from "../../components/react-shared-module/logic/shared-data/datamodel/shared-firestore-collections";
import {Feed} from "feed";
import RootConfigs from "../../configs";
import {SharedRoutesUtils} from "../../components/react-shared-module/utils/shared-routes.utils";

const ITEMS_PER_PAGE = 5;

export async function getStaticProps() {
    const listPublishedDays: DailyPublishedArticlesModel[] = await FirebaseClientFirestoreUtils
        .getDocumentsWithPagination(SharedFirestoreCollectionDB.ArticlesDigestProject.DailyPublishedArticles, ITEMS_PER_PAGE, "publishedDayId");

    return {
        props: {
            listPublishedDays,
        },
        revalidate: 30,
    }
}

export default function RssFeedPage(props: {listPublishedDays: DailyPublishedArticlesModel[]}) {
    let author = {
        name: RootConfigs.SITE_NAME,
            email: RootConfigs.CONTACT_EMAIL,
            link: RootConfigs.BASE_URL,
    }
    const feed = new Feed({
        title: RootConfigs.SITE_TITLE,
        description: RootConfigs.SITE_DESCRIPTION,
        id: RootConfigs.BASE_URL,
        link: RootConfigs.BASE_URL,
        language: "en", // optional, used only in RSS 2.0, possible values: http://www.w3.org/TR/REC-html40/struct/dirlang.html#langcodes
        image: RootConfigs.BASE_URL + "/images/logo-512.png",
        favicon: RootConfigs.BASE_URL + "/favicon.ico",
        copyright: "All rights reserved 2024, " + RootConfigs.SITE_NAME,
        updated: new Date(), // optional, default = today
        author: author
    });

    props.listPublishedDays.forEach((dailyPublished) => {
        if (dailyPublished.listArticles) {
            Object.values(dailyPublished.listArticles).forEach((articleItem) => {
                feed.addItem({
                    title: articleItem.title,
                    link: RootConfigs.BASE_URL + SharedRoutesUtils.getArticleDetails(articleItem.uid!),
                    id: articleItem.uid!,
                    image: articleItem.imageUrl,
                    description: articleItem.aiGeneratedContent?.summary!,
                    category: articleItem.aiGeneratedContent?.tags ? articleItem.aiGeneratedContent?.tags.map((tag) => {
                        return {name: tag}
                    }) : [],
                    published: new Date(articleItem.createdAt!),
                    date: new Date(articleItem.createdAt!),
                    author: [author],
                })
            });
        }
    });

    feed.addCategory("Bitcoin News");


    return (
        <>
            {feed.rss2()}
        </>
    )
}