#!/bin/bash

# Create a temporary build directory
mkdir -p .deploy-build

# Copy package files
cp package.json .deploy-build/

# Copy all files resolving symlinks
cp -rL src .deploy-build/
cp -rL public .deploy-build/  # if you have a public folder

# Copy other necessary config files
cp next.config.js .deploy-build/  # if using Next.js
cp .vercel.json .deploy-build/     # if you have one
cp next-sitemap.config.js .deploy-build/     # if you have one


cp tailwind.config.js .deploy-build/     # if you have one
cp tsconfig.json .deploy-build/     # if you have one
cp postcss.config.js .deploy-build/     # if you have one

# Deploy from the build directory
cd .deploy-build
npm run deploy-vercel