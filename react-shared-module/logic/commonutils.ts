import TimeAgo from "javascript-time-ago";
import {MetaData} from "./shared-data/datamodel/posts-model/post-share-link";
import {SharedBackendApi} from "./shared-data/sharedbackend.api";
import RootConfigs from "../../../configs";
// English.
import en from 'javascript-time-ago/locale/en'
import {FormOption} from "../ui-components/form/RadioButton.component";
import {StorageFileModel} from "./shared-data/datamodel/storage-file.model";
import {SharedEditorBlockModel} from "./shared-data/datamodel/shared-editor-block.model";

TimeAgo.addDefaultLocale(en)

export default class CommonUtils {

    static isStringInEnum(value: string, enumType: any): boolean {
        return Object.values(enumType).includes(value);
    }

    static isClientSide() {
        return typeof window !== 'undefined';
    }

    static getTimeAgo(time: Date | number) {
        if (time && time !== undefined) {
            const timeAgo = new TimeAgo('en-US');

            return timeAgo.format(time);
        } else {
            return "";
        }
    }

    static getPublishedDate() {

    }


    static string_to_slug(str: string) {
        let slugStr = str.replace(/^\s+|\s+$/g, ''); // trim
        slugStr = slugStr.toLowerCase();

        // remove accents, swap ñ for n, etc
        var from = "àáäâèéëêìíïîòóöôùúüûñç·/_,:;";
        var to = "aaaaeeeeiiiioooouuuunc------";
        for (var i = 0, l = from.length; i < l; i++) {
            slugStr = slugStr.replace(new RegExp(from.charAt(i), 'g'), to.charAt(i));
        }

        slugStr = slugStr.replace(/[^a-z0-9 -]/g, '') // remove invalid chars
            .replace(/\s+/g, '-') // collapse whitespace and replace by -
            .replace(/-+/g, '-'); // collapse dashes

        return slugStr;
    }

    static slug_to_title(slug: string) {
        var words = slug.split('-');

        for (var i = 0; i < words.length; i++) {
            var word = words[i];
            words[i] = word.charAt(0).toUpperCase() + word.slice(1);
        }

        return words.join(' ');
    }

    static isValidEmail(emailStr?: string) {
        if (emailStr && /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(emailStr)) {
            return true;
        }
        return false;
    }

    static isValidHttpUrl(urlStr?: string) {
        if (!urlStr) {
            return false;
        }
        let url: URL | undefined;
        try {
            url = new URL(urlStr);
        } catch (_) {
            return false;
        }
        return url.protocol === "http:" || url.protocol === "https:";
    }
    static getDomainNameFromUrl(url: string): string {
        const urlObject = new URL(url);
        return urlObject.hostname;
    }


    static async getUrlMetadata(url: string): Promise<MetaData> {
        // let data: urlMetadata.Result = await urlMetadata(url);
        let data: any = await SharedBackendApi.exportUrlMetadata({url});


        return {
            ...data,
        };
    }

    static addLinkSource(originalUrl: string) {
        let updatedUrl: string = CommonUtils.replaceQParamOnURL(originalUrl, "utm_source", RootConfigs.SITE_NAME);
        updatedUrl = CommonUtils.replaceQParamOnURL(updatedUrl, "ref", RootConfigs.SITE_NAME);
        return updatedUrl;
    }

    static replaceQParamOnURL(originalUrl: string, qparam: string, newValue: string) {
        if (!originalUrl.startsWith("http")) {
            originalUrl = "https://" + originalUrl;
        }
        let href: URL = new URL(originalUrl);
        href.searchParams.set(qparam, newValue);
        return href.toString();
    }

    static preventClickPropagation(event: any) {
        event.stopPropagation();
        event.preventDefault();
    }

    static refreshPage() {
        // @ts-ignore
        window.location.reload(true);
    }

    static convertArrayToDropdownOptions(values: string[]): { [optionId: string]: FormOption } {
        let options: { [optionId: string]: FormOption } = {};

        values.forEach((value) => {
            options = {
                ...options,
                [value]: {
                    optionId: value,
                    label: value,
                    displayOption: true,
                },
            };
        });

        return options;
    }

    static insertAt<T>(array: T[], index: number, newItem: T): void {
        if (index < 0 || index > array.length) {
            throw new Error('Index out of bounds');
        }

        array.splice(index, 0, newItem);
    }

    static fileToBase64(file: File): Promise<string> {
        return new Promise((resolve, reject) => {
            const reader = new FileReader();

            reader.onload = () => {
                if (typeof reader.result === "string") {
                    resolve(reader.result);
                } else {
                    reject('Unexpected result: FileReader result is not a string');
                }
            };

            reader.onerror = () => {
                reject(reader.error);
            };

            reader.readAsDataURL(file);
        });
    }

    static resizeBase64Image(data: string, maxSizeBytes: number): Promise<string> {
        return new Promise((resolve, reject) => {
            // First, check if the image is already within the size limit
            if (btoa(unescape(encodeURIComponent(data))).length / 2 <= maxSizeBytes) {
                resolve(data);  // If so, we can resolve immediately with the original data
                return;
            }

            const image = new Image();
            image.onload = () => {
                let canvas = document.createElement('canvas');
                let context = canvas.getContext('2d');

                const startWidth = image.width;
                const startHeight = image.height;

                // We'll try reducing the image size by half each time
                let scale = 0.5;

                canvas.width = startWidth * scale;
                canvas.height = startHeight * scale;

                context!.drawImage(image, 0, 0, canvas.width, canvas.height);

                let output = canvas.toDataURL();

                // If the output size is still larger than our limit
                while (btoa(unescape(encodeURIComponent(output))).length / 2 > maxSizeBytes) {
                    // Reduce scale and try again
                    scale = scale * 0.5;
                    canvas.width = startWidth * scale;
                    canvas.height = startHeight * scale;

                    context!.clearRect(0, 0, canvas.width, canvas.height);
                    context!.drawImage(image, 0, 0, canvas.width, canvas.height);

                    output = canvas.toDataURL();

                    // Add a condition to break the loop if the scale is too small
                    if (scale < 0.01) {
                        reject('Cannot resize image to the desired size');
                        return;
                    }
                }

                resolve(output);
            };

            image.onerror = reject;

            image.src = data;
        });
    }

    static getRandomNumber(max: number): number {
        return Math.floor(Math.random() * max);
    }

    static isArrayIncludesOtherArray(arrayBig: string[], arraySubset: string[]) {
        let isIncluded: boolean = true;

        arraySubset.forEach((label) => {
            if (!arrayBig.includes(label)) {
                isIncluded = false;
            }
        });

        return isIncluded;
    }

    static shortenText(originalText: string, maxLength: number): string {
        if (originalText.length < maxLength) {
            return originalText;
        } else {
            return originalText.substring(0, maxLength);
        }
    }

    static extractHashtags(input: string): string[] {
        // Regular expression to match hashtagged words
        const regex = /#(\w+)/g;

        // Use the match method to find all matching hashtags in the input string
        const matches = input.match(regex);

        if (matches) {
            // Extract the hashtagged words from the matches
            const hashtags = matches.map((match) => match.slice(1)); // Remove the "#" character
            return hashtags;
        }

        return [];
    }

    static renderStorageFileImage(file?: StorageFileModel) {
        if (!file) {
            return "";
        } else if (file.fileUrl.length > 0) {
            return file.fileUrl;
        } else if (file.tempFile) {
            return URL.createObjectURL(file.tempFile);
        } else {
            return "";
        }
    }

    static renderSvgWithColor(base64Svg: string, fillColor: string): string {
        // Remove leading and trailing whitespaces
        const trimmedBase64Svg = base64Svg.trim();

        // Decode the base64 SVG into a string
        const svgString = atob(trimmedBase64Svg);

        // Modify the SVG string to include the fill color
        const modifiedSvgString = svgString.replace('<svg ', `<svg    fill="${fillColor}" `);

        // Encode the modified SVG string back to base64
        const base64ImageWithColor = `data:image/svg+xml;base64,${btoa(modifiedSvgString)}`;

        return base64ImageWithColor;
    };

    static getFileExtension(file: File): string | null {
        const fileName = file.name;
        const dotIndex = fileName.lastIndexOf('.');

        if (dotIndex === -1) {
            return ""; // No file extension found
        }

        const extension = fileName.slice(dotIndex + 1);
        return "." + extension.toLowerCase(); // You may want to convert to lowercase for consistency
    }

    static openExternalUrl(url: string, target?: '_blank' | undefined) {
        document.location.href = url;
    }

    static calculateReadingTimeFromEditorBlocks(listBlocks: SharedEditorBlockModel[]) {
        // The average reading speed for an adult in words per minute
        const readingSpeed = 238;

        // The time spent per image in seconds
        const imageTime = 5;

        // The total word count of the article
        let wordCount = 0;

        // The total image count of the article
        let imageCount = 0;

        // Loop through the blocks and count the words and images
        for (let block of listBlocks) {
            // Get the type and data of the block
            const { type, data } = block;

            // If the block is a header, paragraph, or list, count the words in the text
            if (data!.text && (type === "header" || type === "paragraph" || type === "list")) {
                // Split the text by spaces and punctuation marks
                const words = data!.text.split(/\s+|\W+/);

                // Add the number of words to the word count
                wordCount += words.length;
            }

            // If the block is an image, increment the image count
            if (type === "image") {
                imageCount++;
            }
        }

        // Calculate the reading time in minutes based on the word count and the reading speed
        const readingTime = wordCount / readingSpeed;

        // Calculate the extra time in minutes based on the image count and the image time
        const extraTime = (imageCount * imageTime) / 60;

        // Return the total time in minutes by adding the reading time and the extra time
        let readTimeMinutes = Math.round(readingTime + extraTime);

        if (readTimeMinutes < 1) {
            readTimeMinutes = 1;
        }
        return readTimeMinutes;
    }

    static convertDaytimestampToMillis(dayTimestamp: number) {
        return dayTimestamp * 86400000;
    }

    static getTodayDay() {
        return Math.floor(Date.now() / 86400000)
    }

    static checkArrayIncludesNoCaseSensitive(arr: string[], value: string) {
        const upperCaseArr: string[] = arr.map((str) => str.toUpperCase());
        return upperCaseArr.includes(value.toUpperCase());
    }

    static isOlderThanHours(timestamp: number, hours: number): boolean {
        const currentTime = new Date();
        const timeDifference = currentTime.getTime() - timestamp;
        const differenceInHours = timeDifference / (1000 * 60 * 60);
        return differenceInHours > hours;
    }

    static capitalizeWords(input: string): string {
        return input
            .toLowerCase()
            .split(' ')
            .map(word => word.charAt(0).toUpperCase() + word.slice(1))
            .join(' ');
    }

    static formatUrlToOpenExternalApp(url: string) {
        return this.replaceQParamOnURL(url, "webtoappExternalUrl", "true");
    }

    static isStringNotEmpty(value?: string) {
        return value !== undefined && value !== null && value.length > 0;
    }

    static cleanMarkdown(inputText: string): string {
        let text = inputText;
        // Remove code blocks
        text = text.replace(/```[\s\S]*?```/g, '');
        text = text.replace(/`[^`]*`/g, '');

        // Remove headers
        text = text.replace(/^#+\s+/gm, '');

        // Remove images
        text = text.replace(/!\[.*?\]\(.*?\)/g, '');

        // Remove links
        text = text.replace(/\[([^\]]+)\]\(.*?\)/g, '$1');

        // Remove emphasis (bold, italic, strikethrough)
        text = text.replace(/(\*\*|__)(.*?)\1/g, '$2');
        text = text.replace(/(\*|_)(.*?)\1/g, '$2');
        text = text.replace(/~~(.*?)~~/g, '$1');

        // Remove blockquotes
        text = text.replace(/^>+\s+/gm, '');

        // Remove horizontal rules
        text = text.replace(/^(-\s*?|\*\s*?|_\s*?){3,}$/gm, '');

        // Remove unordered lists
        text = text.replace(/^\s*[-+*]\s+/gm, '');

        // Remove ordered lists
        text = text.replace(/^\s*\d+\.\s+/gm, '');

        // Remove tables
        text = text.replace(/^\|.*?\|$/gm, '');
        text = text.replace(/^\|\s*:?-+:?\s*\|(?:\s*:?-+:?\s*\|)*$/gm, '');

        // Remove HTML tags
        text = text.replace(/<[^>]*>/g, '');

        return text;
    }
}
