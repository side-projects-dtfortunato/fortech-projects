import {ApiResponse} from "../shared-data/datamodel/base.model";

export class APIResponseMessageUtils {

    static getAPIResponseMessage(apiResponse?: ApiResponse<any>) {
        let responseMessage: string = "";
        if (apiResponse) {
            // Get error from
            if (apiResponse.responseCode > 1000 && Object.keys(ResponseCodeMap).includes(String(apiResponse!.responseCode!))) {
                // @ts-ignore
                responseMessage = ResponseCodeMap[apiResponse.responseCode];
            }

            if (!responseMessage && apiResponse.message) {
                responseMessage = apiResponse.message;
            }

            if (!responseMessage) {
                if (!apiResponse.isSuccess) {
                    responseMessage = "Some error just occurred. Please, try again later...";
                } else {
                    responseMessage = "Your request was successfully executed";
                }
            }
        } else {
            responseMessage = "We didn't received any response from our servers. Please, try again...";
        }

        return responseMessage;
    }
}

const ResponseCodeMap = {
    1000: 'Something failed',
    1001: 'User must be logged in',
    1002: 'User data not found',
    1003: 'Profile is already approved',
    1004: 'Its missing some documents to review',
    1005: 'Users FCM Token map not found',
    1006: 'User not authorized to execute this action',
    1007: 'Seller approval already answered',
    1008: 'User already have a user type defined',
    1009: 'Invalid params',
    1010: 'Data not found',
    1011: 'Action not allowed',

    2000: 'Request executed with success',
    2001: 'User profile updated with success',
}