import {useEffect} from "react";
import {useRouter} from "next/router";

export default function useBackButtonRedirectHook() {
    const router = useRouter();
    let redirectToRoot: boolean = false;

    useEffect(() => {
        // Check if there is no page in the back history
        if (window.history.length === 1) {
            // Push a dummy state to the history stack
            history.pushState({ redirected: true }, '', router.asPath);
            redirectToRoot = true;

            const handlePopState = (event: PopStateEvent) => {
                if (redirectToRoot) {
                    // Redirect to the homepage
                    router.replace('/');
                } else {
                    // Allow default back behavior
                    window.removeEventListener('popstate', handlePopState);
                    history.back();
                }
            };

            window.addEventListener('popstate', handlePopState);

            return () => {
                window.removeEventListener('popstate', handlePopState);
            };
        }
    }, [router]);
}