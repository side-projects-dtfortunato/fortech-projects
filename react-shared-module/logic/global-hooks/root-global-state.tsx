import {createGlobalState} from "react-hooks-global-state";
import {SharedCentralUserDataModel} from "../shared-data/datamodel/shared-central-user-data.model";
import {ApiResponse} from "../shared-data/datamodel/base.model";
import {APIResponseMessageUtils} from "./APIResponseMessageUtils";

/** ROOT STATE **/
export interface RootGlobalStateModel {
    alertMessage: RootAlertMessageStateModel,
    isLoading: boolean,
    centralUserData?: SharedCentralUserDataModel | null,
    subscribedNewsletter: boolean,
}
const initialState: RootGlobalStateModel = {
    alertMessage: {
        display: false,
    },
    isLoading: false,
    centralUserData: null,
    subscribedNewsletter: false,
}
/** **/

export type AlertType = "DEFAULT" | "WARNING" | "ERROR" | "INFO" | "SUCCESS";

export interface RootAlertMessageStateModel {
    alertType?: AlertType,
    message?: string,
    display?: boolean,
    errorCode?: string,
}

export interface RootLoadingStateModel {
    displayLoading: false,
}

const {useGlobalState} = createGlobalState<RootGlobalStateModel>(initialState);


/** USE STATES*/
export const useAlertMessageGlobalState = () => {return useGlobalState("alertMessage")}
export const useIsLoadingGlobalState = () => {return useGlobalState("isLoading")}
export const useCentralUserDataState = () => {return useGlobalState("centralUserData")}
export const useSubscribedNewsletterState = () => {return useGlobalState("subscribedNewsletter")}



export class RootGlobalStateActions {
    static centralUserDataListener: any;

    static displayAlertMessage(props: {alertType: AlertType, message: string, setAlertMessageState: (data: any) => void}) {
        props.setAlertMessageState({
            alertType: props.alertType,
            message: props.message,
            display: true,
        })
    }

    static displayAPIResponseError(props: {apiResponse: ApiResponse<any>, setAlertMessageState: (data: any) => void}) {
        this.displayAlertMessage({
            alertType: "ERROR",
            message: APIResponseMessageUtils.getAPIResponseMessage(props.apiResponse),
            setAlertMessageState: props.setAlertMessageState,
        });
    }

}
