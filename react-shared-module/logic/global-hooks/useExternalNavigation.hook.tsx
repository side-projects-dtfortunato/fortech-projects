import {useEffect, useState} from 'react';

export default function  useExternalNavigation() {
    const [isExternalNavigation, setIsExternalNavigation] = useState(false);

    useEffect(() => {
        if (document.referrer && !document.referrer.includes(window.location.hostname)) {
            setIsExternalNavigation(true);
        }
    }, []);

    return isExternalNavigation;
};
