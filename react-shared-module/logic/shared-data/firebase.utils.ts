import firebase from 'firebase/compat/app';
import 'firebase/compat/database';
import {
    doc,
    DocumentData,
    getDocFromCache,
    getDocFromServer,
    getDocs,
    getFirestore,
    limit,
    orderBy,
    Query,
    startAfter,
    WhereFilterOp,
} from "firebase/firestore"
import { getFunctions, HttpsCallable, httpsCallable, HttpsCallableResult } from "@firebase/functions";
import { getAuth } from "@firebase/auth";
import { getDownloadURL, getStorage, ref, uploadBytesResumable } from "@firebase/storage";
import { ApiResponse } from "./datamodel/base.model";
import RootConfigs from "../../../../configs";
import { collection, query, setDoc, where } from "@firebase/firestore";


// Initialize Firebase
const FirebaseApp = firebase.initializeApp(RootConfigs.FIREBASE_CONFIG);
const auth = getAuth(FirebaseApp);
const db = getFirestore(FirebaseApp);
const storage = getStorage(FirebaseApp);
const functions = getFunctions(FirebaseApp);

export { FirebaseApp, db, auth, storage };


export async function getAPIDocument<T>(collectionId: string, docId: string): Promise<T | undefined> {
    const docRef = doc(db, collectionId, docId);
    let docSnap: any;
    try {
        docSnap = await getDocFromCache(docRef);
    } catch (e) {
        docSnap = await getDocFromServer(docRef);
    }

    return new Promise<T | undefined>((resolve, reject) => {
        if (docSnap && docSnap.exists()) {
            resolve({
                ...docSnap.data(),
            } as T);
            return;
        } else {
            resolve(undefined);
            return;
        }
    });
}

export async function getAPIDocumentSubCol<T>(collections: string[], docIds: string[]): Promise<T | undefined> {
    let docRef: any;

    collections.forEach((colId, index) => {
        if (docRef) {
            docRef = doc(docRef, colId, docIds[index]);
        } else {
            docRef = doc(db, colId, docIds[index]);
        }
    });

    let docSnap: any;
    try {
        docSnap = await getDocFromCache(docRef);
    } catch (e) {
        docSnap = await getDocFromServer(docRef);
    }

    return new Promise<T | undefined>((resolve, reject) => {
        if (docSnap && docSnap.exists()) {
            resolve({
                ...docSnap.data(),
            } as T);
            return;
        } else {
            resolve(undefined);
            return;
        }
    });
}

export async function callFunction(functionName: string, body: any): Promise<ApiResponse<any>> {
    const functionCallable: HttpsCallable = httpsCallable(functions, functionName, { timeout: 300000 });
    const response: HttpsCallableResult = await functionCallable(body);

    return response.data as any;
}

export function uploadFileStorage(file: any, filePath: string, onUploadCompleted: (fileUrl: string) => void, onProgressPercent: (percentage: number) => void) {
    if (!file) {
        alert("Please choose a file first!")
    }

    const storageRef = ref(storage, filePath);

    const uploadTask = uploadBytesResumable(storageRef, file);

    uploadTask.on(
        "state_changed",
        (snapshot) => {
            const percent = Math.round(
                (snapshot.bytesTransferred / snapshot.totalBytes) * 100
            );

            // update progress
            onProgressPercent(percent);
        },
        (err) => console.log(err),
        () => {
            // download url
            getDownloadURL(uploadTask.snapshot.ref).then((url) => {
                onUploadCompleted(url);
            });
        }
    );
}


export function uploadFileStoragePromise(file: any, filePath: string, onProgressPercent?: (percentage: number) => void): Promise<string> {
    return new Promise((resolve, reject) => {
        if (!file) {
            alert("Please choose a file first!")
        }

        const storageRef = ref(storage, filePath);

        const uploadTask = uploadBytesResumable(storageRef, file);

        uploadTask.on(
            "state_changed",
            (snapshot) => {
                const percent = Math.round(
                    (snapshot.bytesTransferred / snapshot.totalBytes) * 100
                );

                // update progress
                if (onProgressPercent) {
                    onProgressPercent(percent);
                }
            },
            (err) => console.log(err),
            () => {
                // download url
                getDownloadURL(uploadTask.snapshot.ref).then((url) => {
                    resolve(url);
                });
            }
        );
    });
}

export class FirebaseClientFirestoreUtils {

    static getDB() {
        return db;
    }

    static async updateDocument(
        collectionName: string,
        documentId: string,
        dataToUpdate: Record<string, any>
    ): Promise<boolean> {
        try {
            const docRef = doc(db, collectionName, documentId);
            await setDoc(docRef, dataToUpdate, { merge: true });
            return true;
        } catch (error) {
            console.error('Error updating document:', error);
            // Handle error (e.g., show a notification to the user)
            return false;
        }
    }

    static async updatePathDocument(
        collections: string[],
        docIds: string[],
        dataToUpdate: Record<string, any>
    ): Promise<boolean> {
        try {
            let docRef: any;

            collections.forEach((colId, index) => {
                if (docRef) {
                    docRef = doc(docRef, colId, docIds[index]);
                } else {
                    docRef = doc(db, colId, docIds[index]);
                }
            });
            await setDoc(docRef, dataToUpdate, { merge: true });
            return true;
        } catch (error) {
            console.error('Error updating document:', error);
            // Handle error (e.g., show a notification to the user)
            return false;
        }
    }

    static async getDocumentsWithPagination(
        collectionPath: string,
        pageSize: number,
        fieldOrderBy: string,
        lastDocFieldValue?: any
    ): Promise<any[]> {
        try {
            const constraints = [
                orderBy(fieldOrderBy, "desc"), // Replace 'id' with your actual document ID field
                //lastDocumentId ? startAfter(lastDocumentId) : undefined,
                limit(pageSize)
            ];
            if (lastDocFieldValue) {
                // @ts-ignore
                constraints.push(startAfter(lastDocFieldValue!));
            }
            const q = query(
                collection(db, collectionPath),
                ...constraints
            );

            const querySnapshot = await getDocs(q);
            const documents = querySnapshot.docs.map((doc) => doc.data());

            return documents;
        } catch (error) {
            console.error('Error fetching documents:', error);
            // Handle error (e.g., show a notification to the user)
            return [];
        }
    }

    /**
     * Get documents with pagination and filter
     * @deprecated Use getDocumentsMultipleFilteredWithPagination instead
     * @param props 
     * @returns 
     */
    static async getDocumentsFilteredWithPagination(props: {
        collectionPath: string,
        pageSize: number,
        fieldOrderBy: string,
        lastDocFieldValue?: any,
        filter?: { field: string; value: string, operator?: WhereFilterOp }
    }
    ): Promise<any[]> {
        try {
            let q: Query<DocumentData> = collection(db, props.collectionPath);

            // Apply filter if provided
            if (props.filter) {
                q = query(q, where(props.filter.field, props.filter.operator || '==', props.filter.value));
            }

            // Apply sorting
            q = query(q, orderBy(props.fieldOrderBy, 'desc'));

            // Apply pagination
            if (props.lastDocFieldValue) {
                q = query(q, startAfter(props.lastDocFieldValue));
            }

            // Apply limit
            q = query(q, limit(props.pageSize));

            const querySnapshot = await getDocs(q);
            const documents: any[] = querySnapshot.docs.map((doc) => ({
                uid: doc.id,
                ...doc.data()
            }));

            return documents;
        } catch (error) {
            console.error('Error fetching documents:', error);
            throw error;
        }
    }

    static async getDocumentsMultipleFilteredWithPagination(props: {
        collectionPath: string,
        pageSize: number,
        fieldOrderBy: string,
        lastDocFieldValue?: any,
        filters?: { field: string; value: string | number, operator?: WhereFilterOp, sort?: "asc" | "desc" }[]
    }
    ): Promise<any[]> {
        try {
            let q: Query<DocumentData> = collection(db, props.collectionPath);

            // Apply filter if provided
            if (props.filters) {
                props.filters.forEach((filter) => {
                    q = query(q, where(filter.field, filter.operator || '==', filter.value));
                    if (filter.sort) {
                        q = query(q, orderBy(filter.field, filter.sort));
                    }
                });
            }

            // Apply sorting
            q = query(q, orderBy(props.fieldOrderBy, 'desc'));

            // Apply pagination
            if (props.lastDocFieldValue) {
                q = query(q, startAfter(props.lastDocFieldValue));
            }

            // Apply limit
            q = query(q, limit(props.pageSize));

            const querySnapshot = await getDocs(q);
            const documents: any[] = querySnapshot.docs.map((doc) => ({
                uid: doc.id,
                ...doc.data()
            }));

            return documents;
        } catch (error) {
            console.error('Error fetching documents:', error);
            throw error;
        }
    }


    static async getDocsFromSubCollection(
        path: {
            collection: string,
            docId: string,
            subCollection: string,
        },
        pageSize: number,
        fieldOrderBy: string,
        lastDocumentId?: string
    ): Promise<any[]> {
        try {
            const constraints = [
                orderBy(fieldOrderBy, "desc"), // Replace 'id' with your actual document ID field
                //lastDocumentId ? startAfter(lastDocumentId) : undefined,
                limit(pageSize)
            ];
            if (lastDocumentId) {
                // @ts-ignore
                constraints.push(startAfter(lastDocumentId!));
            }

            const q = query(
                collection(db, path.collection, path.docId, path.subCollection),
                ...constraints
            );

            const querySnapshot = await getDocs(q);
            const documents = querySnapshot.docs.map((doc) => doc.data());

            return documents;
        } catch (error) {
            console.error('Error fetching documents:', error);
            // Handle error (e.g., show a notification to the user)
            return [];
        }
    }

}

export class ClientRealtimeDbUtils {

    static async writeData(params: { collectionName: string, docId?: string, subPaths?: string[], data: any, merge: boolean }): Promise<void> {
        const db = FirebaseApp.database();
        let path = params.collectionName;

        if (params.docId) {
            path += "/" + params.docId
        }

        if (params.subPaths) {
            params.subPaths.forEach((subPath) => {
                path = path + "/" + subPath;
            })
        }

        let finalData = params.data;
        if (params.merge) {
            const existingData: any = await this.getData(params);
            if (existingData) {
                finalData = {
                    ...existingData,
                    ...finalData,
                };
            }
        }

        // Replace all dots in the path
        path = path.replaceAll(".", "");

        return db.ref(path).set(finalData);
    }

    static deleteData(params: { collectionName: string, docId: string, subPaths?: string[] }): Promise<void> {
        const db = FirebaseApp.database();
        let path = params.collectionName + "/" + params.docId;
        if (params.subPaths) {
            params.subPaths.forEach((subPath) => {
                path = path + "/" + subPath;
            })
        }

        // Replace all dots in the path
        path = path.replaceAll(".", "");

        return db.ref(path).remove();
    }

    static async getData(params: { collectionName: string, docId?: string, subPaths?: string[] }): Promise<any> {
        const db = firebase.database();
        let path = params.collectionName;

        if (params.docId) {
            path += "/" + params.docId;
        }
        if (params.subPaths) {
            params.subPaths.forEach((subPath) => {
                path = path + "/" + subPath;
            })
        }

        // Replace all dots in the path
        path = path.replaceAll(".", "");

        return (await db.ref(path).once("value")).val();
    }

    static async getFirstChild(params: { collectionName: string, docId: string, subPaths?: string[] }): Promise<any> {
        const data = await this.getData(params);

        if (data && Object.keys(data).length > 0) {
            return Object.values(data)[0];
        } else {
            return null;
        }
    }
}