export interface StripeProductItemModel {
    productName: string;
    productDescription: string;
    price: number; // in cents
    currency: "usd" | "eur";
    productImageUrl?: string;
}

export enum StripePaymentStatus {
    CONFIRMED = "CONFIRMED", WAITING_PAYMENT = "WAITING_PAYMENT", CANCELLED = "CANCELLED"
}