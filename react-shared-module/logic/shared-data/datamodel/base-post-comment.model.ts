import {BaseModel} from "./base.model";
import {UserShortProfileModel} from "./shared-public-userprofile.model";

export interface PostCommentRootData extends BaseModel {
    postCollectionName: string;
    postDocId: string;
    listComments: {[commentId: string]: BasePostComment};
    usersInfos: {[userId: string]: UserShortProfileModel};
    publishedDayId: number;
}


export interface BasePostComment extends BaseModel {
    userCreatorId: string;
    message: string;
    reactions: {
        [userId: string]: string, // Type of reaction
    },
    replies?: {
        [replyId: string]: BasePostComment,
    },
    pinned?: boolean,
}