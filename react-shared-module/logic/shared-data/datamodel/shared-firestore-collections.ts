export const SharedFirestoreCollectionDB = {
    // Profile
    CentralUserData: "CentralUserData",
    DataCatalog: "DataCatalog",
    UserPostsCreated: "UserPostsCreated",
    PostsDetails: "PostsDetails",
    PublicUserProfile: "PublicUserProfile",
    UserBookmarks: "UserBookmarks",
    UserFollowers: "UserFollowers",
    UserFollowing: "UserFollowing",
    NewsletterCatalog: "NewsletterCatalog",

    PostCommentRoot: "PostCommentRoot",

    DraftUserPosts: "DraftUserPosts",
    ContactUs: "ContactUs",


    // AI Imported Articles Project
    ArticlesDigestProject: {
        ArticlePublished: "ArticlePublished",
        DailyPublishedArticles: "DailyPublishedArticles",
    },
    CommunityCommentPosts: "CommunityCommentPosts",
    CommunityDailyPublishes: "CommunityDailyPublishes",

    // Jobs Discover
    JobsDiscoverProject: {
        SubmitJobWaitingPayment: "SubmitJobWaitingPayment",
        JobDetails: "JobDetails",
        DailyPublishedJobs: "DailyPublishedJobs",
        DigestedJobsIndexRTDB: "PublishedJobsIndexRTDB",
        ImportedJobsListRTDB: "ImportedJobsList", // List of jobs that are waiting to get details
        JobSkillsCounterRTDB: "JobSkillsCounterRTDB"
    },

    AdminRegisteredUsersCounterRTDB: "registeredUsersCounter"
}

export const SharedFirestoreDocsDB = {
    DataCatalog: {
        FilterRecent: "filter:recent",
        FilterPopular: "filter:popular",
        PrefixPostType: "type:",
        PrefixCategoryType: "category:",
        PrefixTagType: "tag:",
        PopularTags: "PopularTags",
        SearchableItems: "SearchableItems",
    },
    NewsletterCatalog: {
        SubscribedUsers: "SubscribedUsers",
        UnsubscribedUsers: "UnsubscribedUsers",
        DigestSenderUsersQueue: "DigestSenderUsersQueue",
        InvalidEmailUsers: "InvalidEmailUsers",
        DailyUserDigest: "DailyUserDigest",
    }
}


export const SharedRealtimeCollectionDB = {
    ArticlesDigestProject: {
        TempRecentPublishedArticles: "TempRecentPublishedArticles",
    },
    StatsEventsName: "StatsEventsName",
    StatsEventsObj: {
        collectionName: "collectionName",
        docId: "docId",
        viewsCounter: "viewsCounter",
    },
    CommunityParentCommentsAggregator: "CommunityParentCommentsAggregator",
    CommunityMostRecentPosts: "CommunityMostRecentPosts",
}