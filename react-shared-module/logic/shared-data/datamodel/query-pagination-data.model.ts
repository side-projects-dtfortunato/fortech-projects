import firebase from "firebase/compat";
import DocumentSnapshot = firebase.firestore.DocumentSnapshot;

export function initQueryPaginationDataModel<ItemsType>(itemsPerPage: number): QueryPaginationDataModel <ItemsType> {
    return {
        currentPage: 0,
        hasMorePages: true,
        itemsPerPage: itemsPerPage,
        listItems: [],
    };
}

export interface QueryPaginationDataModel <ItemsType> {
    listItems: ItemsType[];
    lastDoc?: DocumentSnapshot;
    itemsPerPage: number;
    currentPage: number;
    hasMorePages: boolean;
}