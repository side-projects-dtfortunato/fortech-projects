import {BaseModel} from "./base.model";
import {SharedPublicUserprofileModel} from "./shared-public-userprofile.model";


export enum UserRole {USER = "USER", VERIFIED = "VERIFIED", ADMIN = "ADMIN"}

export interface SharedCentralUserDataModel extends BaseModel {
    username?: string;
    userRole?: UserRole;
    email?: string;
    isEmailValid?: boolean;
    publicProfile?: SharedPublicUserprofileModel;
    localUpdated?: number;
    usersFollowing?: {[userId: string]: boolean},
    disableUsernameChange?: boolean;
    isNewsletterSubscribed?: boolean;
    lastSession?: number;
    nicheSitesLoggedIn?: string[];
}