import {PostDetailsBaseModel} from "../base-post-data.model";
import {StorageFileModel} from "../storage-file.model";

export interface PostFormArticleBlocksDetails extends PostDetailsBaseModel {
    editorJSData?: {
        time: number,
        version: string,
        blocks: any[],
    },

    // listBlocks?: ArticleBlockBase[];
}


export interface ArticleEditorImageFileBlockModel {
    url: string,
    isServerSaved: boolean,
    uid?: string,
}


/*** NOT USED */

export enum ArticleBlockType {TEXT_CONTENT ="TEXT_CONTENT", EMBEDDED = "EMBEDDED"}

export interface ArticleBlockBase {
    type: ArticleBlockType;
}

export interface ArticleBlockTextContent extends ArticleBlockBase {
    title?: string;
    titleLink?: string;
    image?: StorageFileModel;
    text: string;
}

export interface ArticleBlockEmbedded extends ArticleBlockBase{
    title?: string
    htmlCode: string;
    text?: string;
}
