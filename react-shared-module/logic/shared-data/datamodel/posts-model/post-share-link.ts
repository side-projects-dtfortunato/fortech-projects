import {PushPostDataModel} from "../base-post-data.model";
import {FormValidation} from "../shared-model-data";
import CommonUtilsNoDep from "../../CommonUtilsNoDep";

export interface MetaData {
    title?: string;
    description?: string;
    url?: string;
    canonical?: string;
    keywords?: string;
    image?: string;
    icon?: string;
    author?: string;
    language?: string;
    published?: string;
    provider?: string;
}

export interface PostFormShareLinkShortenModel {
    link: string,
    metaData?: MetaData,
    isTheAuthor?: boolean,
}

export class PostFormShareLinksUtils {

    static isFormDataValidToSubmit(postFormData: PushPostDataModel<PostFormShareLinkShortenModel>): FormValidation {
        if (!CommonUtilsNoDep.isValidHttpUrl(postFormData.shortenPostData.link)
            || !postFormData.shortenPostData.metaData
            || !postFormData.shortenPostData.metaData.title) {
            return  {
                isValid: false,
                errorMessage: "Invalid link, please try with a different link",
            };
        }

        return {
            isValid: true,
        };
    }

}