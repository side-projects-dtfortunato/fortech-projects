import {BaseModel} from "./base.model";
import {UserShortProfileModel} from "./shared-public-userprofile.model";
import {ArticleEditorImageFileBlockModel, PostFormArticleBlocksDetails} from "./posts-model/post-article-blocks";
import {ImagekitHelper} from "../../../utils/imagekit.helper";
import {SharedUIHelper} from "../../../utils/shared-ui-helper";

export function initPushDataState<ShortenDataType = any, DetailsPostType = any>(postType: string): PushPostDataModel<ShortenDataType, DetailsPostType> {
    return {
        tags: [],
        shortenPostData: {
        } as any,
        detailsPostData: {
        } as any,
        postType: postType,
        category: "",
        title: "",
    } as PushPostDataModel<ShortenDataType, DetailsPostType>;
}

export interface PushPostDataModel<ShortenDataType = any, DetailsPostType = any> extends BaseModel {
    tags: string[],
    catalogDocs?: string[],
    title?: string,
    summary?: string,
    thumbnailUrl?: string,
    postType: string,
    category: string,
    shortenPostData: ShortenDataType,
    detailsPostData?: DetailsPostType,
    bookmarkedBy?: {[userId: string]: boolean},

    isFeatured?: boolean,
    availableNicheSites?: string[],
}

export interface PostDetailsBaseModel {
    postSummaryListItems?: PostListItemsModel;
}

export interface PostUserInfos extends UserShortProfileModel {}

export interface BasePostDataModel<ShortenDataType = any, DetailsPostType = any> extends PushPostDataModel<ShortenDataType | any, DetailsPostType | any> {
    userCreatorId: string;
    publishedAt: number;
    updatedAt: number;
    listUsersInfos: {
        [userId: string]: PostUserInfos,
    },
    responses: {
        comments: {
            [commentId: string]: BasePostComment,
        },
        reactions: {
            [userId: string]: string,
        },
    }
    openViews: number,
    numComments: number,
    numReactions: number,
    rankPts: number,
}

export interface BasePostComment extends BaseModel {
    userCreatorId: string;
    message: string;
    publishedAt: number;
    reactions: {
        [userId: string]: string, // Type of reaction
    },
    replies?: {
        [replyId: string]: BasePostComment,
    },
}

export interface PostListItemsModel {
    listTitle: string;
    listItems: PostListBulletItemModel[];
}

export interface PostListBulletItemModel {
    title: string;
    summary?: string;
    link?: string;
    imageUrl?: string;
}
export class PostDataUtils {

    static getUserCreatorInfos(basePostData: BasePostDataModel) {
        return this.getUserInfos(basePostData, basePostData.userCreatorId);
    }
    static getUserInfos(basePostData: BasePostDataModel, userId?: string): PostUserInfos | undefined {
        if (userId && basePostData.listUsersInfos && Object.keys(basePostData.listUsersInfos).includes(userId)) {
            return basePostData.listUsersInfos[userId];
        } else {
            return undefined;
        }
    }

    static convertPostToShortData(basePostData: BasePostDataModel) {
        return {
            ...basePostData,
            detailsPostData: {},
            responses: {},
        };
    }

    static generateListSimilarPosts(postDetails: BasePostDataModel, mapListPosts: {[postId: string]: BasePostDataModel}): BasePostDataModel[] {
        let mapRankedPosts: {[postId: string]: {
                ptsSimilarity: number,
                postData: BasePostDataModel,
            }} = {};

        Object.keys(mapListPosts).forEach((postId) => {
            if (postId !== postDetails.uid) {
                let postCompare: BasePostDataModel = mapListPosts[postId];
                let ptsSimilarity: number = 0;

                if (postCompare.category === postDetails.category) {
                    ptsSimilarity += 1;
                }
                postCompare.tags.forEach((postCompareTag) => {
                    if (postDetails.tags.map((tag) => tag.toLowerCase()).includes(postCompareTag)) {
                        ptsSimilarity += 1;
                    }
                });

                if (ptsSimilarity > 1 && postCompare.rankPts > 1) {
                    ptsSimilarity += 1;
                }
                mapRankedPosts = {
                    ...mapRankedPosts,
                    [postId]: {
                        postData: postCompare,
                        ptsSimilarity: ptsSimilarity,
                    },
                };
            }
        });

        // Generate list of similar posts
        return Object
            .values(mapRankedPosts)
            .filter((item) => item.ptsSimilarity > 0 && SharedUIHelper.isPostAvailableForCurrentSite(item.postData))
            .sort((i1, i2) => {
                if (i1.ptsSimilarity !== i2.ptsSimilarity) {
                    return i2.ptsSimilarity - i1.ptsSimilarity;
                } else {
                    return i2.postData.publishedAt - i1.postData.publishedAt;
                }
            }).map((item) => item.postData);

    }


    static async uploadImagesPostArticleBlock(uid: string, postArticleBlocksData: PostFormArticleBlocksDetails) {

        // Check block by block to find an image
        for (let i = 0; i < postArticleBlocksData.editorJSData!.blocks.length; i++) {
            let blockItem = postArticleBlocksData.editorJSData?.blocks[i];

            if (blockItem?.type === "image" && !(blockItem.data.file as ArticleEditorImageFileBlockModel).isServerSaved) {
                // Upload image with ImageKit
                let response = await ImagekitHelper.uploadImageFromUrl((blockItem.data.file as ArticleEditorImageFileBlockModel).url,
                    blockItem.id, uid, []);

                if (response) {
                    (blockItem.data.file as ArticleEditorImageFileBlockModel).isServerSaved = true;
                    (blockItem.data.file as ArticleEditorImageFileBlockModel).url = response.fileUrl;
                    (blockItem.data.file as ArticleEditorImageFileBlockModel).uid = response.storagePath;
                }
            }
        }
        return postArticleBlocksData;
    }

    static getThumbnailUrl(postArticleBlocksData: PostFormArticleBlocksDetails) {
        let blockImage = postArticleBlocksData.editorJSData?.blocks.find((blockItem) => blockItem.type === "image");

        if (blockImage) {
            return (blockImage.data.file as ArticleEditorImageFileBlockModel).url;
        } else {
            return "";
        }

    }
}