export interface SharedEditorBlockModel {
    data?: {
        text?: string;
        level?: number;
        caption?: string;
        file?: {
            isServerSaved: boolean,
            uid: string,
            url: string,
        },
    }

    id: string;
    type: "paragraph" | "image" | "header" | string;
}