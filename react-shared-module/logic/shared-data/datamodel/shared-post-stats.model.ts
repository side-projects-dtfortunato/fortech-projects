export interface SharedPostStatsModel {
    viewsCounter?: number;
    commentsCounter?: number;
}