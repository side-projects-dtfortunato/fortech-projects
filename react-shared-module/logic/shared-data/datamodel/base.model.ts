export interface BaseModel {
    uid?: string;
    createdAt?: number;
    updatedAt?: number;
    stats?: any;
    authorization?: {
        writeUIDs?: string[];
        readUIDs?: string[];
        allRead?: boolean;
        allWrite?: boolean;
    };
}

export interface ApiResponse<T extends any> {
    responseCode: number; // Internal response code (if < 2000 is an error or if > 2000 is a success response)
    responseData?: T; // Body response,
    errorData?: any;
    message?: string; // Message in the case it needs some extra information
    displayMessage?: boolean; // if the Message sent should be displayed to the User
    isSuccess: boolean; // If the request was success or not
}