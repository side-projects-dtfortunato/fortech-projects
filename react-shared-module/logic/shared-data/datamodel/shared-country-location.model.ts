export interface SharedCountryLocationModel {
    countryName: string;
    countryCode: string;
}