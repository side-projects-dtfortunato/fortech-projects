import {BaseModel} from "./base.model";

export interface SearchableItemModel extends BaseModel {
    title: string;
    subtitle?: string;
    rank: number; // More is better
    thumbnail?: string;
    type: string;
    tags: string[];
    similarityResult?: number;
}