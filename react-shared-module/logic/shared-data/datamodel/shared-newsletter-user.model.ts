import {UserShortProfileModel} from "./shared-public-userprofile.model";
import {BasePostDataModel} from "./base-post-data.model";

export interface SharedNewsletterUserModel {
    userId: string;
    name: string;
    email: string;
}

export interface SharedNewsletterDailyUserDigestModel {
    newFollowers?: {
        [userId: string]: UserShortProfileModel,
    },
    newCommentsPosts?: {
        [postId: string]: BasePostDataModel,
    },
    newUpvotes?: {
        [postId: string]: BasePostDataModel,
    },
    followingPosts?: {
        [postId: string]: BasePostDataModel,
    }
}