export interface StorageFileModel {
    fileUrl: string;
    storagePath: string;
    tempFile?: File;
    tempBase64?: string;
}