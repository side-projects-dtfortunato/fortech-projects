import {callFunction, ClientRealtimeDbUtils, db, FirebaseClientFirestoreUtils, getAPIDocument} from "./firebase.utils";
import {SharedCentralUserDataModel} from "./datamodel/shared-central-user-data.model";
import {ApiResponse} from "./datamodel/base.model";
import {BasePostDataModel, PostDataUtils, PushPostDataModel} from "./datamodel/base-post-data.model";
import {
    SharedFirestoreCollectionDB,
    SharedFirestoreDocsDB,
    SharedRealtimeCollectionDB
} from "./datamodel/shared-firestore-collections";
import CommonUtils from "../commonutils";
import AuthManager from "../auth/auth.manager";
import {collection, getDocs, increment, limit, query, setDoc, where} from "@firebase/firestore";
import {SharedPublicUserprofileModel} from "./datamodel/shared-public-userprofile.model";
import {doc} from "firebase/firestore";
import RootConfigs from "../../../../configs";
import {SearchableItemModel} from "./datamodel/searchable-item.model";
import firebase from 'firebase/compat/app';
import 'firebase/compat/firestore';
import {PostFormArticleBlocksDetails} from "./datamodel/posts-model/post-article-blocks";
import {RealtimeDBStatsEventModel} from "./datamodel/realtime-db-models/realtimedb-stats-event.model";
import {CommentAttachedItemModel, CommunityParentRefs} from "../../base-projects/community/data/community-data.model";
import {JobDetailsModel, JobsDiscoverDataUtils} from "../../base-projects/jobs-discover/jobs-discover-data.model";
import ServerValue = firebase.database.ServerValue;
import { SharedUtils } from "../../utils/shared-utils";


export class SharedBackendApi {
    static async getCentralUserData(props: {userId?: string}) {
        if (props.userId) {
            return await getAPIDocument<SharedCentralUserDataModel | undefined>(SharedFirestoreCollectionDB.CentralUserData, props.userId);
        } else {
            return null;
        }
    }

    static async requestDocumentChanges(props: {docData?: any, collectionName: string, docId?: string, methodType: "CREATE" | "MERGE" | "DELETE"}): Promise<ApiResponse<any> | undefined> {
        return await callFunction("requestDocumentChanges", props);
    }

    static async pushPostData(props: {postData: PushPostDataModel, isCreation: boolean, thumbnailBase?: string}) {
        if (props.isCreation) {
            // Generate the UID slug
            if (!props.postData.uid && props.postData.title) {
                let slug: string = props.postData.title;
                if (slug.length > 120) {
                    slug = slug.substring(0, 120);
                }
                slug = CommonUtils.string_to_slug(slug).toLowerCase();

                // Check if the post with this id already exists
                let existingDoc = await getAPIDocument<any>(SharedFirestoreCollectionDB.PostsDetails, slug);
                if (existingDoc && existingDoc.uid === slug) {
                    slug += "-" + Date.now();
                }

                props.postData.uid = CommonUtils.string_to_slug(slug);
            }

            // Add Catalog docIds
            let catalogDocs: string[] = [];
            if (props.postData.category) {
                catalogDocs.push(SharedFirestoreDocsDB.DataCatalog.PrefixCategoryType + props.postData.category);
            }
            catalogDocs.push(SharedFirestoreDocsDB.DataCatalog.PrefixPostType+props.postData.postType);
            catalogDocs.push(SharedFirestoreDocsDB.DataCatalog.FilterRecent);

            props.postData.catalogDocs = catalogDocs;


            if (props.postData.postType === "ARTICLE_BLOCKS") {
                // Upload images first
                props.postData.detailsPostData = await PostDataUtils.uploadImagesPostArticleBlock(props.postData.uid!, props.postData.detailsPostData as PostFormArticleBlocksDetails);
                props.postData.thumbnailUrl = PostDataUtils.getThumbnailUrl(props.postData.detailsPostData);
            }

            // Set available niche sites
            props.postData.availableNicheSites = RootConfigs.META_CONFIGS.defaultPostsAvailableNicheSites;
        }

        return await callFunction("pushPostData", {
            ...props,
            isDev: RootConfigs.ENVIRONMENT_DEV,
        });
    }

    static async pushPostComment(props: {postId: string, replyCommentId?: string, message: string}) {
        return await callFunction("pushPostComment", props);
    }

    static async pushPostReaction(props: {postId: string, reactionType?: string, isToAdd: boolean}) {
        return await callFunction("pushPostReaction", props);
    }

    static async exportUrlMetadata(props: {url: string}) {
        return await callFunction("exportUrlMetadata", props);
    }

    static async updatePostBookmark(props: {postId: string, postData: BasePostDataModel, addBookmark: boolean}) {
        if  (!AuthManager.isUserLogged()) {
            return {
                message: "User is not logged in",
                displayMessage: true,
                isSuccess: false,
                responseCode: 1000,
            };
        }
        return await callFunction("updatePostBookmark", props);
    };

    static async updateFollowingUser(props: {userIdToFollow: string, startFollowing: boolean}) {
        if  (!AuthManager.isUserLogged()) {
            return {
                message: "User is not logged in",
                displayMessage: true,
                isSuccess: false,
                responseCode: 1000,
            };
        }
        return await callFunction("updateFollowingUser", props);
    };

    static async checkUsernameValid(props: {username?: string}) {
        return await callFunction("checkUsernameValid", props);
    }
    static async updateUserProfile(props: {centralUserData: SharedCentralUserDataModel}) {
        return await callFunction("updateUserProfile", props);
    }

    static async getPublicProfileByUsername(props: {username: string}): Promise<SharedPublicUserprofileModel | undefined> {
        let publicProfile: SharedPublicUserprofileModel | undefined;
        const publicProfilesRef = collection(db, SharedFirestoreCollectionDB.PublicUserProfile);
        const q = query(publicProfilesRef, where("username", "==", props.username), limit(1));

        const querySnapshot = await getDocs(q);

        if (!querySnapshot.empty) {
            publicProfile = querySnapshot.docs[0].data();
        }

        return publicProfile;
    }

    static async pushPostPageView(props: {postId: string}): Promise<ApiResponse<any>> {
        await setDoc(doc(db, SharedFirestoreCollectionDB.PostsDetails, props.postId), {
            openViews: increment(1),
            rankPts: increment(0.001),
        }, {merge: true});

        return {
            isSuccess: true,
            responseCode: 2000,
        };
    }

    static async updateCentralUserDataLastSession(): Promise<ApiResponse<any>> {
        if (AuthManager.getUserId()) {
            await setDoc(doc(db, SharedFirestoreCollectionDB.CentralUserData, AuthManager.getUserId()!), {
                lastSession: Date.now(),
            }, {merge: true});
        }

        return {
            isSuccess: true,
            responseCode: 2000,
        };
    }

    static async getSearchableItems(): Promise<{[uid: string]: SearchableItemModel} | undefined> {
        let searchItems: {[uid: string]: SearchableItemModel} | undefined = await getAPIDocument(SharedFirestoreCollectionDB.DataCatalog, SharedFirestoreDocsDB.DataCatalog.SearchableItems);

        return searchItems;
    }


    static listCentralUserDataChanges(props: {setCentralUserData: (data: SharedCentralUserDataModel) => void}) {
        if (AuthManager.isUserLogged()) {
            return firebase.firestore()
                .collection(SharedFirestoreCollectionDB.CentralUserData)
                .doc(AuthManager.getUserId()!).onSnapshot((doc) => {
                    if (doc.data()) {
                        props.setCentralUserData({
                            ...doc.data() as SharedCentralUserDataModel,
                        });
                    }
                });
        }

    }


    static async removePostData(props: {postId: string}) {
        return await callFunction("removePostData", props);
    };

    static async setFeaturePost(props: {postId: string, isFeatured: boolean}) {
        return await callFunction("setFeaturePost", props);
    };

    static async deleteAccount() {
        return await callFunction("deleteAccount", {});
    }

    static async stripeCheckPaymentStatus(props: {sessionId: string}) {
        return await callFunction("stripeCheckPaymentStatus", props);
    }

    static async newsletterSubscribe(props: {email: string}) {
        return await callFunction("newsletterSubscribe", props);
    }

    static async pushCommunityComment(props: {comment: string, replyCommentId?: string, postCollectionName?: string, docId?: string,
        attachedItem?: CommentAttachedItemModel}) {
        return await callFunction("pushCommunityComment", props);
    }

    static async incrementArticleViewEvent(props: {parentCollectionId: string, docId: string}) {
        const data: RealtimeDBStatsEventModel = {
            viewsCounter: ServerValue.increment(1),
            docId: props.docId,
            collectionName: props.parentCollectionId,
        };
        await ClientRealtimeDbUtils.writeData({
            collectionName: SharedRealtimeCollectionDB.StatsEventsName,
            docId: props.docId,
            merge: true,
            data: data,
        });
    }


    static async incrementStatData(props: {parentCollectionId: string, docId: string, dataKey: string}) {
        const data: any = {
            [props.dataKey]: ServerValue.increment(1),
            docId: props.docId,
            collectionName: props.parentCollectionId,
        };
        await ClientRealtimeDbUtils.writeData({
            collectionName: SharedRealtimeCollectionDB.StatsEventsName,
            docId: props.docId,
            merge: true,
            data: data,
        });
    }

    /** Community Endpoints **/
    static async communityPostComment(props: {comment: string, parentRefsId?: CommunityParentRefs,
        attachedItem?: CommentAttachedItemModel}) {
        return await callFunction("communityPostComment", props);
    }
    static async communityReplyComment(props: {comment: string, parentCommentId: string}) {
        return await callFunction("communityReplyComment", props);
    }
    static async communityCommentsDelete(props: {parentCommentId: string, replyCommentId?: string}) {
        return await callFunction("communityCommentsDelete", props);
    }

    static async submitJobPost(props: {jobPost: JobDetailsModel}) {
        // return await callFunction("submitJobPost", props);


        // Migrate the logic from the backend to the frontend
        const finalJobPostData: JobDetailsModel = {
            ...props.jobPost,
            link: "",
            authorization: {
                allRead: true,
                allWrite: true,
            },
            updatedAt: Date.now(),
            createdAt: Date.now(),
            publishedDayId: SharedUtils.getTodayDay(),
            uid: SharedUtils.string_to_slug(props.jobPost.company) + "-" + SharedUtils.string_to_slug(props.jobPost.title),
            description: JobsDiscoverDataUtils.convertHTMLtoMarkdown(props.jobPost.description),
        };

        /*await FirebaseClientFirestoreUtils.updateDocument(SharedFirestoreCollectionDB.JobsDiscoverProject.SubmitJobWaitingPayment,
            finalJobPostData.uid!,
            finalJobPostData);*/
        await ClientRealtimeDbUtils.writeData({
            collectionName: SharedFirestoreCollectionDB.JobsDiscoverProject.SubmitJobWaitingPayment,
            docId: finalJobPostData.uid!,
            merge: false,
            data: finalJobPostData,
        });

        // Stripe params
        const price = props.jobPost.publishPlanType === 'BASIC' ? 1000 : 10000; // in cents
        const productName = `${RootConfigs.SITE_NAME} Job Post`;
        const productDescription = `${props.jobPost.title} - ${props.jobPost.publishPlanType} plan`;
        const successUrl = `${RootConfigs.BASE_URL}/job/job-post-submission?result=success&jobId=${finalJobPostData.uid!}&session_id={CHECKOUT_SESSION_ID}`;
        const cancelUrl = `${RootConfigs.BASE_URL}/job/job-post-submission?result=cancel`;
        const clientReferenceId = finalJobPostData.uid!;

        // Create Stripe checkout session
        const stripeSessionData = {
            price,
            productName,
            productDescription,
            successUrl,
            cancelUrl,
            clientReferenceId
        };

        let response = await fetch('https://fortulyapps.up.railway.app/createJobsStripeCheckoutSession', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(stripeSessionData)
        });
        
        const responseData = await response.json();
    
        return {
            isSuccess: responseData.redirectUrl !== null,
            responseCode: responseData.redirectUrl !== null ? 2000 : 1000,
            responseData: {
                redirectUrl: responseData.redirectUrl,
            },
        };
    }
}
