
export default class CommonUtilsNoDep {


    static isValidHttpUrl(urlStr: string) {
        let url: URL | undefined;
        try {
            url = new URL(urlStr);
        } catch (_) {
            return false;
        }
        return url.protocol === "http:" || url.protocol === "https:";
    }

}