import {getCustomLanguageLabel} from "../../../ui-helper/language/custom-language.helper";
import RootConfigs from "../../../../configs";

const DefaultLanguageLabels: {[label: string]: string} = {
    LANDING_PAGE_HEADER_JOIN_COMMUNITY_TITLE: "Join our community",
    LANDING_PAGE_HEADER_JOIN_COMMUNITY_SUBTITLE: "To discover, share or discuss topics with our community. Share your own ideas with our community.",
    LANDING_PAGE_SUBMIT_POST_TITLE: "Submit a new post",
    LANDING_PAGE_POPULAR_TAGS_TITLE: "Popular Tags",
    LANDING_PAGE_DIVIDER_PUBLICATIONS: "PUBLICATIONS",
    PUBLISH_FORM_ARTICLE_BODY_TITLE: "Write your article here:",
    POST_HEADER_ARTICLE_TOP_LABEL: "📰 Original Article",
    POST_HEADER_LINK_TOP_LABEL: "🔗 Story link shared",
    POST_HEADER_DISCUSSION_TOP_LABEL: "💬 Discussion Topic",
    POST_HEADER_PRODUCT_TOP_LABEL: "🚀 Product Launch",
    POST_HEADER_LIST_OF_TOP_LABEL: "🗒️ List of...",
    BOOKMARKS_PAGE_TITLE_PAGE: "Your Bookmarks",
    BOOKMARKS_EMPTY_LIST: "You don't have any bookmarked posts yet.",
    COOKIES_CONSENT_MESSAGE: "To make " + RootConfigs.SITE_NAME + " work, we log user data. By using " + RootConfigs.SITE_NAME + ", you agree to our Privacy Policy, including cookie policy.",
    EDIT_PROFILE_NEW_USER: "We are preparing your profile. Please, wait a moment...",
    POST_FORM_PRODUCT_LINK_TITLE: "Link for the product",
    POST_FORM_PRODUCT_LINK_HINT: "Share the link to access to your AI Product",
    POST_FORM_PRODUCT_NAME_TITLE: "Name of the product",
    POST_FORM_PRODUCT_NAME_HINT: "What is the commercial name of this product?",
    POST_FORM_PRODUCT_SUMMARY_TITLE: "Short description",
    POST_FORM_PRODUCT_SUMMARY_HINT: "Describe in a few words what this product tries to solve with AI (max: 280 chars)",
    POST_FORM_PRODUCT_CREATOR_TITLE: "You are the creator of this product?",
    POST_FORM_PRODUCT_TAGS_TITLE: "Set some tags to describe this product (e.g: industry, category, etc...)",
    POST_FORM_PRODUCT_TAGS_HINT: "Press enter to add the tag... (max: MAX_TAGS)",
    POST_FORM_PRODUCT_CANCEL: "Cancel submission",
    POST_FORM_PRODUCT_SUBMIT: "Submit Product",
    POST_FORM_LIST_OF_TITLE: "Title",
    POST_FORM_LIST_OF_TITLE_HINT: "e.g: Best 10 things to do...",
    POST_FORM_LIST_OF_INTRO: "Article intro",
    POST_FORM_LIST_OF_INTRO_HINT: "Write a short and descriptive introduction of the list you are presenting in this article (max: 3000 chars)",
    POST_LIST_ITEMS_ADD_BTN: "+ Add List Item",
    POST_LIST_ITEMS_FORM_DIVIDER: "Post List Summary",
    GLOBAL_USER_CREATOR: "AUTHOR",
    FORM_LANGUAGE_SELECTOR_TITLE: "Language",
    ROOT_BREADCRUMB_LABEL: "Home",
    SEARCH_TITLE: "Search Posts and Users",
    PRICING_TRY_IT_FREE: "Try it free",
    PRICING_SUBSCRIBE_IT: "Subscribe it",
    PRICING_SUBSCRIBED: "Subscribed",
    BILLING_MANAGE_BTN: "Manage billing settings",
    LANDING_PAGE_SIGNUP_WITH_EMAIL: "Signup with email",
    LANDING_PAGE_QUICK_POST_PLACEHOLDER: "Publish something for the community...",
    GENERIC_CANCEL_LABEL: "Cancel",
    GENERIC_CANCEL_SUBMIT: "Post",

    // Comments
    COMMENT_TITLE: "Comments",
    COMMENT_SEND_COMMENT: "Send Comment",
    COMMUNITY_SUBTITLE: "Connect with remote workers",

    // Newsletter
    NEWSLETTER_TITLE: "Join To Get Our Daily Newsletter",

    // Articles

    // Jobs
    REMOTE_JOBS_EXPLORE_JOBS_TITLE: "Explore Remote Jobs",
    REMOTE_JOBS_LATEST_JOBS_IN_TITLE: "Latest Remote Jobs in",
    REMOTE_JOBS_DISCOVER_JOBS_SUBTITLE: "Discover new remote opportunities worldwide.",
    REMOTE_JOBS_CATEGORY_SUBTITLE: "Find the best remote positions in #CATEGORY",
    REMOTE_JOBS_POST_JOB_SUBTITLE: "Reach Global Talent",
    REMOTE_JOBS_POST_JOB_TITLE: "Post a New Job",
    REMOTE_JOBS_POST_JOB_SUBTITLE_2: "Reach thousands of qualified remote candidates",
    JOBS_REGION_TITLE: "Recent Remote Jobs in #REGION",
    JOBS_REGION_CATEGORY_TITLE: "Recent Remote Jobs in #REGION and #CATEGORY",
    JOBS_EMPTY_REGION_CATEGORY_MESSAGE: "We don't have any #CATEGORY remote jobs in #REGION at the moment. Please check back later.",
    JOBS_REGION_CATEGORY_SEO_DESCRIPTION: "Find the latest #CATEGORY remote jobs in #REGION. Browse available positions updated daily. Start your remote career today!",
}

export function getLanguageLabel(labelId: string, fallback?: string): string {
    if (getCustomLanguageLabel(labelId)) {
        return getCustomLanguageLabel(labelId)!;
    }
    if (Object.keys(DefaultLanguageLabels).includes(labelId)) {
        // @ts-ignore
        return DefaultLanguageLabels[labelId]! as string;
    } else {
        return fallback ? fallback : labelId;
    }
}
