import {getAPIDocument} from "../shared-data/firebase.utils";

const LOCAL_EXIPRATION_TIME = 1800000; // 30 minutes


let LocalStorageDataMap: {
    [key: string]: {
        localStorageKey: string,
        data?: any,
        timestamp?: number,
    }
} = {};

export default class LocalStorageDataManager {

    static async getLocalData(params: { collection: string, docId: string, forceUpdate?: boolean }): Promise<any> {
        const dataKey: string = params.collection + "." + params.docId;
        // To guarantee that the data instance is initialized
        this.generateDataPath(dataKey);

        let data: any = this.getLocalStorageData(dataKey);

        if (params.forceUpdate || !data || this.isDataExpired(dataKey)) {
            data = await getAPIDocument(params.collection, params.docId);
            if (data) {
                data = await this.saveLocalData(dataKey, data);
            }
        }
        return data;
    }

    private static generateDataPath(dataKey: string) {
        if (!Object.keys(LocalStorageDataMap).includes(dataKey)) {
            LocalStorageDataMap = {
                ...LocalStorageDataMap,
                [dataKey]: {
                    localStorageKey: dataKey,
                },
            };
        }
    }

    private static isDataExpired(dataKey: string) {
        let localData: any = this.getLocalStorageData(dataKey);

        if (localData) {
            let timestamp: number | undefined = LocalStorageDataMap[dataKey]?.timestamp;

            if (!timestamp || (timestamp + LOCAL_EXIPRATION_TIME) < Date.now()) {
                // Has expired, should force update
                return true;
            } else {
                return false;
            }
        }
        return true;
    }

    private static getLocalStorageData(dataKey: string): any {

        if (LocalStorageDataMap[dataKey].data) {
            return LocalStorageDataMap[dataKey].data
        } else if (LocalStorageDataMap[dataKey].data) {
            let jsonData: string | undefined = (localStorage as any).getItem(LocalStorageDataMap[dataKey].localStorageKey);
            if (jsonData) {
                LocalStorageDataMap.PopularTags.data = JSON.parse(jsonData);

                let timestamp: number | undefined = (localStorage as any).getItem(LocalStorageDataMap[dataKey].localStorageKey + ".timestamp");
                LocalStorageDataMap.PopularTags.timestamp = timestamp;
            }
            return LocalStorageDataMap.PopularTags.data;
        } else {
            return undefined;
        }
    }

    private static async saveLocalData(dataKey: string, data: any) {
        LocalStorageDataMap = {
            ...LocalStorageDataMap,
            [dataKey]: {
                ...LocalStorageDataMap[dataKey],
                data: data,
                timestamp: Date.now(),
            },
        };

        localStorage.setItem(dataKey, LocalStorageDataMap as any);

        return data;
    }



    public static saveItem(key: string, data: any) {
        localStorage.setItem(key, JSON.stringify(data));
    }
    public static saveString(key: string, data: any) {
        localStorage.setItem(key, data);
    }

    public static getItem(key: string, defaultValue?: any) {
        let localData: string | null | undefined = this.getLocalStorage()?.getItem(key);
        if (localData) {
            return JSON.parse(localData);
        } else {
            return defaultValue;
        }
    }

    public static getString(key: string, defaultValue?: string) {
        let localData: string | null | undefined = this.getLocalStorage()?.getItem(key);
        if (localData) {
            return localData;
        } else {
            return defaultValue;
        }
    }

    private static getLocalStorage() {
        if (typeof window !== 'undefined') {
            return localStorage;
        } else {
            return undefined;
        }
    }
}