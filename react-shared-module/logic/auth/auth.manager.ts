import {
    createUserWithEmailAndPassword,
    GoogleAuthProvider,
    sendPasswordResetEmail,
    signInWithEmailAndPassword,
    signInWithPopup,
} from "@firebase/auth";
import {auth, getAPIDocument} from "../shared-data/firebase.utils";
import {SharedFirestoreCollectionDB} from "../shared-data/datamodel/shared-firestore-collections";
import {NextRouter} from "next/router";
import {UIHelper} from "../../../ui-helper/UIHelper";
import {Duration} from "ts-duration";
import {SharedBackendApi} from "../shared-data/sharedbackend.api";
import {SharedCentralUserDataModel} from "../shared-data/datamodel/shared-central-user-data.model";
import RootConfigs from "../../../../configs";
import OneSignal from "react-onesignal";
import {SharedUtils} from "../../utils/shared-utils";
import LocalStorageDataManager from "../managers/LocalStorageData.manager";


const LOCAL_EXIPRATION_TIME = 1800000; // 30 minutes

let CentralUserDataInstance: SharedCentralUserDataModel | undefined;

export {CentralUserDataInstance};

export default class AuthManager {

    static getUserId(): string | undefined {
        if (this.isUserLogged()) {
            return auth.currentUser?.uid;
        } else {
            return undefined;
        }
    }

    static setOneSignalUserId(oneSignalUserId?: string) {
        LocalStorageDataManager.saveString("oneSignalUserId", oneSignalUserId);
    }
    static getOneSignalUserId(): string | undefined {
        return LocalStorageDataManager.getString("oneSignalUserId", undefined);
    }

    static isUserLogged() {

        return auth.currentUser ? true : false;
    }

    static async signInUser(email: string, password: string) {
        if (!this.isUserLogged()) {
            let result = await signInWithEmailAndPassword(auth, email, password);

            if (this.isUserLogged()) {
                CentralUserDataInstance = await this.getCentralUserData();

                await OneSignal.login(AuthManager.getUserId()!);
            }
            return result;
        } else {
            return false;
        }
    }

    static async signinWithGoogle(router: NextRouter, goAutoBack?: boolean) {
        const provider = new GoogleAuthProvider();
        // signInWithRedirect(auth, provider)
        signInWithPopup(auth, provider).then(async (result) => {
            if (result.user) {
                if (this.isUserLogged()) {
                    CentralUserDataInstance = await this.getCentralUserData(true);

                    UIHelper.onUserCreation({router: router, goAutoBack: goAutoBack, centralUserData: CentralUserDataInstance});


                    await OneSignal.login(AuthManager.getUserId()!);
                }
            }
        })
    }

    static async createUserAccount(email: string, password: string) {
        if (!this.isUserLogged()) {
            const response = await createUserWithEmailAndPassword(auth, email, password);
            return response;
        } else {
            return false;
        }
    }

    static async recoverPassword(email: string) {
        return await sendPasswordResetEmail(auth, email);
    }

    static async signOut() {
        CentralUserDataInstance = undefined;
        (localStorage as any).clear();

        if (this.isUserLogged()) {
            await auth.signOut();
        }

        await OneSignal.logout();
    }

    static async getCentralUserData(forceUpdate?: boolean): Promise<SharedCentralUserDataModel | undefined>{

        if (this.isUserLogged() && this.getUserId()) {
            if (!CentralUserDataInstance) {
                let jsonData: string | undefined = (localStorage as any).getItem(getLocalCentralUserDataKey(this.getUserId() as string));
                if (jsonData) {
                    CentralUserDataInstance = JSON.parse(jsonData);
                }

                // Check if should Update User Last Session
                if (CentralUserDataInstance && (!CentralUserDataInstance.lastSession || Duration.millisecond(CentralUserDataInstance.lastSession).sub(Duration.millisecond(Date.now())).minutes > 20)) {
                    await SharedBackendApi.updateCentralUserDataLastSession();
                }
            }

            // Check if should force the update due to the validation
            let hasExpired: boolean = false
            if (CentralUserDataInstance && CentralUserDataInstance.localUpdated && (CentralUserDataInstance.localUpdated + LOCAL_EXIPRATION_TIME) < Date.now()) {
                // Has expired, should force update
                hasExpired = true;
            }

            if (!CentralUserDataInstance || forceUpdate || hasExpired) {
                CentralUserDataInstance = await getAPIDocument(SharedFirestoreCollectionDB.CentralUserData, this.getUserId()!);

                if (CentralUserDataInstance) {
                    CentralUserDataInstance.localUpdated = Date.now();
                    (localStorage as any).setItem(getLocalCentralUserDataKey(this.getUserId() as string), JSON.stringify(CentralUserDataInstance));
                }

            }
        }

        return CentralUserDataInstance;
    }
}

function getLocalCentralUserDataKey(userId: string) {
    return "CentralUserData" + userId;
}