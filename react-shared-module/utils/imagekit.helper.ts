import RootConfigs from "../../../configs";
import {StorageFileModel} from "../logic/shared-data/datamodel/storage-file.model";

var ImageKit = require("imagekit");

const imagekit = new ImageKit(RootConfigs.IMAGEKIT_API);

export class ImagekitHelper {


    static async uploadImageFromUrl(data: string, uid: string, folderPath: string, tags: string[]): Promise<StorageFileModel | undefined> {
        const response = await imagekit.upload({
            file : data, //required
            fileName : uid + ".png",   //required
            tags: tags,
            folder: RootConfigs.ENVIRONMENT_DEV ? "dev/" : RootConfigs.SITE_NAME + folderPath,
            overwriteFile: true,
        });

        if (response) {
            return {
                storagePath: response.fileId,
                fileUrl: response.url,
            };
        } else {
            return undefined;
        }
    }

}