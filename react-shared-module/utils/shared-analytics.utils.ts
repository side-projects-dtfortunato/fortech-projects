import { getAnalytics, logEvent, Analytics, setUserProperties } from 'firebase/analytics';
import { User } from 'firebase/auth';

export class SharedAnalyticsManager {
    private static instance: SharedAnalyticsManager;
    private analytics: Analytics | null = null;

    private constructor() {
        // Private constructor to enforce singleton pattern
        if (typeof window !== 'undefined') {
            this.analytics = getAnalytics();
        }
    }

    public static getInstance(): SharedAnalyticsManager {
        if (!SharedAnalyticsManager.instance) {
            SharedAnalyticsManager.instance = new SharedAnalyticsManager();
        }
        return SharedAnalyticsManager.instance;
    }

    /**
     * Log a page view event
     * @param pageName - The name of the page being viewed
     * @param pagePath - The path of the page being viewed
     */
    public logPageView(pageName: string, pagePath: string): void {
        if (!this.analytics) return;

        logEvent(this.analytics, 'page_view', {
            page_title: pageName,
            page_path: pagePath,
            timestamp: new Date().toISOString()
        });
    }

    /**
     * Log a user interaction event
     * @param eventName - The name of the interaction event
     * @param properties - Additional properties for the event
     */
    public logUserInteraction(eventName: string, properties: Record<string, any> = {}): void {
        if (!this.analytics) return;

        logEvent(this.analytics, eventName, {
            ...properties,
            timestamp: new Date().toISOString()
        });
    }

    /**
     * Log user authentication events
     * @param eventType - The type of auth event (login, logout, signup)
     * @param method - The authentication method used
     */
    public logAuthEvent(eventType: 'login' | 'logout' | 'signup', method: string): void {
        if (!this.analytics) return;

        const eventName = `auth_${eventType}`;
        logEvent(this.analytics, eventName, {
            auth_method: method,
            timestamp: new Date().toISOString()
        });
    }

    /**
     * Log content interaction events
     * @param contentType - The type of content being interacted with
     * @param contentId - The unique identifier of the content
     * @param actionType - The type of interaction
     */
    public logContentInteraction(
        contentType: string,
        contentId: string,
        actionType: 'view' | 'share' | 'like' | 'comment'
    ): void {
        if (!this.analytics) return;

        logEvent(this.analytics, 'content_interaction', {
            content_type: contentType,
            content_id: contentId,
            action_type: actionType,
            timestamp: new Date().toISOString()
        });
    }

    /**
     * Log error events
     * @param errorCode - The error code or type
     * @param errorMessage - The error message
     * @param errorContext - Additional context about where the error occurred
     */
    public logError(errorCode: string, errorMessage: string, errorContext: string): void {
        if (!this.analytics) return;

        logEvent(this.analytics, 'error', {
            error_code: errorCode,
            error_message: errorMessage,
            error_context: errorContext,
            timestamp: new Date().toISOString()
        });
    }

    /**
     * Set user properties for analytics
     * @param user - Firebase User object
     * @param additionalProperties - Additional user properties to set
     */
    public setUserProperties(user: User | null, additionalProperties: Record<string, string> = {}): void {
        if (!this.analytics || !user) return;

        const userProperties = {
            user_id: user.uid,
            email_verified: user.emailVerified.toString(),
            ...additionalProperties
        };

        setUserProperties(this.analytics, userProperties);
    }

    /**
     * Log a custom event with arbitrary parameters
     * @param eventName - The name of the custom event
     * @param parameters - Custom parameters for the event
     */
    public logCustomEvent(eventName: string, parameters: Record<string, any> = {}): void {
        if (!this.analytics) return;

        logEvent(this.analytics, eventName, {
            ...parameters,
            timestamp: new Date().toISOString()
        });
    }
}