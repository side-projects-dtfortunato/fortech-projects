import {PostTypes} from "./shared-ui-helper";


export class SharedRoutesUtils {
    static getBookmarksPageUrl() {
        return "/bookmarks";
    }
    static getContactUsUrl() {
        return "/contact-us";
    }

    static getPublishPostPageUrl(postTypeDefault?: PostTypes) {
        let url = "/publish-post";
        if (postTypeDefault) {
            url += "?posttype=" + postTypeDefault;
        }
        return url;
    }

    static getCategoryPostsPageURL(category: string) {
        return "/category/" + category;
    }

    static getPostDetailsPageURL(postId: string) {
        return "/" + postId;
    }

    static getPublicProfilePageURL(userId: string, username?: string) {
        let pageId: string = userId;
        if (username && username.length > 0) {
            pageId = "@" + username;
        }
        return "/publisher/" + pageId;
    }

    static getTaggedPostsPageURL(category: string) {
        return "/tagged-posts/" + category;
    }

    static getSearchPageURL() {
        return "/search";
    }

    static getEditPostUrl(postId: string) {
        return "/edit-post/" + postId;
    }

    static getBlogPageUrl() {
        return "/blog";
    }

    static getBlogPostDetailsPageURL(uid: string) {
        return "/" + uid;
    }

    static getLoginPageUrl() {
        return "/login";
    }

    static getArticleDetails(articleId: string) {
        return "/article/" + articleId;
    }

    static getPublishedArticlesList(publishedDayId: string) {
        return "/daily/articles/" + publishedDayId;
    }

    static getArticlesCategoryUrl(category?: string) {
        return "/article/category/" + category;
    }

    static getAboutUs() {
        return "/about-us";
    }

    static getCommunityPageUrl() {
        return "/community";
    }

    static getCommunityCommentDetailsPageUrl(commentId: string) {
        return "/community/" + commentId;
    }

    static getJobDetailsPageUrl(jobId: string) {
        return "/job/" + jobId;
    }

    static getSubmitJob() {
        return "/job/submit-job";
    }

    static getJobsCategoryUrl(category: string) {
        return "/job/category/" + category;
    }

    static getJobsRegionUrl(region: string) {
        return "/job/region/" + region;
    }

    static getJobsRegionCategoryUrl(region: string, category: string) {
        return "/job/region/" + region + "/" + category;
    }   
}