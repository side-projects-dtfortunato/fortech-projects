export class SharedWebToAppBridgeUtils {


    static sendUserIdToFlutter(userId?: string) {
        try {
            if ((window as any).FlutterWebView) {
                console.log("sendUserIdToFlutter:", userId);
                (window as any).FlutterWebView.postMessage(JSON.stringify({
                    type: 'userId',
                    userId: userId
                }));
            }
        } catch (e) {
            console.log(e);
        }
    }
    static sendUserTags(tags: any) {
        try {
            if ((window as any).FlutterWebView) {
                console.log("sendUserTags:", tags);
                (window as any).FlutterWebView.postMessage(JSON.stringify({
                    type: 'userTags',
                    tags: tags
                }));
            }
        } catch (e) {
            console.log(e);
        }
    }

}