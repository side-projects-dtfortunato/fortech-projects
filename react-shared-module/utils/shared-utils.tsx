import {BasePostDataModel, PostDataUtils} from "../logic/shared-data/datamodel/base-post-data.model";
import {NewsArticle, NewsMediaOrganization, Person, WithContext} from "schema-dts";
import {jsonLdScriptProps} from "react-schemaorg";
import RootConfigs from "../../../configs";
import {PostFormFullArticleDetails} from "../logic/shared-data/datamodel/posts-model/post-full-article";
import {SharedPublicUserprofileModel} from "../logic/shared-data/datamodel/shared-public-userprofile.model";
import {getPrivacyPolicyPage} from "../../../pages/privacy-policy";
import {SharedRoutesUtils} from "./shared-routes.utils";
import {SharedUIHelper} from "./shared-ui-helper";

export class SharedUtils {

    static getSchemaMarkupFromPost(postData: BasePostDataModel) {
        let articleBody: string = postData.summary ? postData.summary : postData.title!;
        if (postData.postType === "WRITE_ARTICLE") {
            articleBody = (postData.detailsPostData as PostFormFullArticleDetails).bodyText;
        }
        let keywords: string[] = [];
        if (postData.category) {
            keywords.push(postData.category);
        }
        if (postData.tags) {
            keywords = keywords.concat(postData.tags);
        }

        return (
            <script
                {...jsonLdScriptProps<NewsArticle>(
                    {
                        "@context": "https://schema.org",
                        "@type": "NewsArticle",
                        "@id": postData.uid,
                        inLanguage: "English",
                        publisher: {
                            "@type": "Organization",
                            url: RootConfigs.BASE_URL,
                            name: RootConfigs.SITE_NAME,
                        },
                        keywords: keywords,
                        description: postData.summary,
                        thumbnailUrl: postData.thumbnailUrl,
                        about: RootConfigs.SITE_DESCRIPTION,
                        url: SharedRoutesUtils.getPostDetailsPageURL(postData.uid!),
                        image: postData.thumbnailUrl ? [
                            postData.thumbnailUrl,
                        ] : [],
                        author: [
                            {
                                "@type": "Person",
                                name: PostDataUtils.getUserInfos(postData, postData.userCreatorId)?.name,
                                url: RootConfigs.BASE_URL + "/" + SharedRoutesUtils.getPublicProfilePageURL(PostDataUtils.getUserInfos(postData, postData.userCreatorId)?.username!),
                            }
                        ],
                        articleBody: articleBody,
                        headline: postData.title,
                        commentCount: postData.numComments,
                        datePublished: new Date(postData.publishedAt).toLocaleDateString(),
                        dateCreated: new Date(postData.publishedAt).toLocaleDateString(),

                    }
                )}
            />
        )
    }
    static getSchemaMarkupFromUser(userProfile: SharedPublicUserprofileModel) {

        return (
            <script
                {...jsonLdScriptProps<Person>(
                    {
                        "@context": "https://schema.org",
                        "@type": "Person",
                        "@id": userProfile.uid!,
                        url: RootConfigs.BASE_URL + "/" + SharedRoutesUtils.getPublicProfilePageURL(userProfile.uid!, userProfile.username),
                        name: userProfile.name,
                        knows: userProfile.publicSubtitle,
                        sameAs: userProfile.links ? Object.values(userProfile.links).map((item) => item.link) : [],
                        image: userProfile.userPicture?.fileUrl ? {
                            "@type": "ImageObject",
                            "@id": userProfile.uid!,
                            url: userProfile.userPicture?.fileUrl,
                        } : undefined,
                        inLanguage: "en-US",
                        caption: userProfile.username,
                    } as  WithContext<Person>
                )}/>
        )
    }
    static getSchemaMarkupSite() {

        return (
            <script
                {...jsonLdScriptProps<NewsMediaOrganization>(
                    {
                        "@context": "https://schema.org",
                        "@type": "NewsMediaOrganization",
                        url: RootConfigs.BASE_URL,
                        name: RootConfigs.SITE_NAME,
                        description: RootConfigs.SITE_DESCRIPTION,
                        publishingPrinciples: getPrivacyPolicyPage(),
                        logo: RootConfigs.BASE_URL + "/images/logo.png"
                    }
                )}/>
        )
    }

    static openExternalUrl(url: string) {
        window.location.href = url;
    }

    static isStringNotEmpty(str?: string) {
        return str && str.length > 0;
    }



    static sendUserIdToFlutter(userId?: string) {
        try {
            console.log("Window Object: ", (window as any));
            if ((window as any).FlutterWebView) {
                console.log("sendUserIdToFlutter:", userId);
                (window as any).FlutterWebView.postMessage(JSON.stringify({
                    type: 'userId',
                    userId: userId
                }));
            }
        } catch (e) {
            console.log(e);
        }
    }

    static getTodayDay() {
        return Math.floor(Date.now() / 86400000)
    }

    static string_to_slug(str: string) {
        let slugStr = str.replace(/^\s+|\s+$/g, ''); // trim
        slugStr = slugStr.toLowerCase();

        // remove accents, swap ñ for n, etc
        var from = "àáäâèéëêìíïîòóöôùúüûñç·/_,:;";
        var to = "aaaaeeeeiiiioooouuuunc------";
        for (var i = 0, l = from.length; i < l; i++) {
            slugStr = slugStr.replace(new RegExp(from.charAt(i), 'g'), to.charAt(i));
        }

        slugStr = slugStr.replace(/[^a-z0-9 -]/g, '') // remove invalid chars
            .replace(/\s+/g, '-') // collapse whitespace and replace by -
            .replace(/-+/g, '-'); // collapse dashes

        return slugStr;
    }

    static slug_to_title(slug: string) {
        var words = slug.split('-');

        for (var i = 0; i < words.length; i++) {
            var word = words[i];
            words[i] = word.charAt(0).toUpperCase() + word.slice(1);
        }

        return words.join(' ');
    }
}