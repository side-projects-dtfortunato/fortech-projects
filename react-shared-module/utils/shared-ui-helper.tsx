import {UIHelper} from "../../ui-helper/UIHelper";
import {BasePostDataModel} from "../logic/shared-data/datamodel/base-post-data.model";
import RootConfigs from "../../../configs";

export enum PostTypes {
    SHARE_LINK = "SHARE_LINK",
    DISCUSSION = "DISCUSSION",
    WRITE_ARTICLE = "WRITE_ARTICLE",
    ARTICLE_BLOCKS = "ARTICLE_BLOCKS",
    LIST_OF = "LIST_OF",
    PRODUCT = "PRODUCT",
    QUICK_ARTICLE = "QUICK_ARTICLE",
}

export class SharedUIHelper {


    static getPostFormTypeLabel(postType: PostTypes) {
        let label: string = postType;
        switch (postType) {
            case PostTypes.SHARE_LINK: label = "🔗 Share a link"; break;
            case PostTypes.LIST_OF: label = "🗒️ List of..."; break;
            case PostTypes.WRITE_ARTICLE:
            case PostTypes.ARTICLE_BLOCKS:
                label = "📰 Write an article"; break;
            case PostTypes.DISCUSSION: label = "💬 Discussion topic"; break;
            case PostTypes.PRODUCT: label = "🚀 Share a product"; break;
        }
        return label;
    }



    static isPostAvailableForCurrentSite(post: BasePostDataModel) {
        if (UIHelper.isPostAvailableForCurrentSite(post)) {
            return true;
        }
        return false;
    }
}