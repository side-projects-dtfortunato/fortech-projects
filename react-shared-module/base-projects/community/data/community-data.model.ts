import {BaseModel} from "../../../logic/shared-data/datamodel/base.model";
import {UserShortProfileModel} from "../../../logic/shared-data/datamodel/shared-public-userprofile.model";

export interface CommunityCommentPostModel extends BaseModel {
    parentRefIds?: CommunityParentRefs, // This will be used in the case where the origin of this Community Comment came from an article or something else
    userCreatorId: string,
    message: string,
    usersInfos: {[userId: string]: UserShortProfileModel},
    reactions: {
        [userId: string]: "UPVOTE" | "SMILE", // Type of reaction
    },
    replies: {
        [replyId: string]: CommunityCommentReplyModel,
    },
    attachedItem?: CommentAttachedItemModel,
    pinned?: boolean,
    publishedDayId?: number,
}

export interface CommunityCommentReplyModel extends BaseModel{
    parentCommentId: string;
    message: string;
    reactions: {
        [userId: string]: string, // Type of reaction
    };
    userCreatorId: string;
}

export interface CommunityParentRefs {
    parentCollectionId: string,
    parentDocId: string,
}

export interface CommentAttachedItemModel {
    title: string;
    thumbnailUrl?: string;
    link: string;
}

export interface DailyCommunityPublishes {
    publishedDayId: number;
    listPosts: {[uid: string]: CommunityCommentPostModel};
}