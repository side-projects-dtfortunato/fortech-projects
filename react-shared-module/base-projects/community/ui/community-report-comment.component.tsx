import React, {useEffect, useState} from 'react';
import {
    useDisclosure,
    Modal,
    ModalBody,
    ModalCloseButton,
    ModalContent,
    ModalHeader,
    ModalOverlay,
    Box,
    Button,
    Checkbox,
    Text,
    VStack
} from "@chakra-ui/react";
import {CommunityUtils} from "../community.utils";

export default function CommunityReportCommentComponent(props: {isPopupOpen: boolean, onPopupClosed: () => void, commentId: string, userId: string }) {
    const { isOpen, onOpen, onClose } = useDisclosure();
    const [blockUser, setBlockUser] = useState(false);


    useEffect(() => {
        if (props.isPopupOpen && !isOpen) {
            onOpen();
        }
    }, [props.isPopupOpen]);

    const handleReport = () => {
        CommunityUtils.reportComment(props.commentId, blockUser ? props.userId : undefined);
        onClosePopup();
    };

    function onClosePopup() {
        onClose();
        props.onPopupClosed();
    }

    return (
        <div className='flex flex-col'>
            <Modal isOpen={isOpen} onClose={onClosePopup} size={"xl"} closeOnEsc={false} closeOnOverlayClick={false} isCentered={true}
                   motionPreset='slideInBottom'>
                <ModalOverlay />
                <ModalContent backgroundColor={"white"} shadow={"lg"} >
                    <ModalHeader>Report Comment</ModalHeader>
                    <ModalCloseButton onClick={onClosePopup} />
                    <ModalBody>
                        <VStack spacing={4} align="stretch">
                            <Text>
                                Are you sure you want to report this comment? This action will flag the comment for review by our moderation team and will be removed from your feed.
                            </Text>
                            <Checkbox isChecked={blockUser} onChange={(e) => setBlockUser(e.target.checked)}>
                                Also block this user
                            </Checkbox>
                            <Box display="flex" justifyContent="space-between">
                                <button className='btn btn-outline' onClick={onClosePopup}>Cancel</button>
                                <Button className="btn btn-error" onClick={handleReport}>Report Comment</Button>
                            </Box>
                        </VStack>
                    </ModalBody>
                </ModalContent>
            </Modal>
        </div>
    )
}