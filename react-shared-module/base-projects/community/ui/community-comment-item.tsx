import Linkify from "react-linkify";
import {CommunityCommentPostModel, CommunityCommentReplyModel} from "../data/community-data.model";
import UserHeaderInfosComponent from "../../../ui-components/shared/UserHeaderInfos.component";
import CommonUtils from "../../../logic/commonutils";
import Link from "next/link";
import Image from "next/image";
import {GoLinkExternal} from "react-icons/go";
import CommunityCommentReplyItemComponent from "./community-comment-reply-item.component";
import {useState} from "react";
import AuthManager from "../../../logic/auth/auth.manager";
import {SharedRoutesUtils} from "../../../utils/shared-routes.utils";
import CommuntyWriteCommentComponent from "./communty-write-comment.component";
import {SharedBackendApi} from "../../../logic/shared-data/sharedbackend.api";
import CommunityPostUpvoteBtnComponent from "./community-post-upvote-btn.component";
import {FaRegCommentDots} from "react-icons/fa";
import {RootGlobalStateActions, useAlertMessageGlobalState} from "../../../logic/global-hooks/root-global-state";
import LoginPopupComponent from "../../../ui-components/login/shared/LoginPopup.component";
import CommunityReportCommentFlagButtonComponent from "./community-report-comment-flag-button.component";
import {CommunityUtils} from "../community.utils";

export default function CommunityCommentItem(props: {communityCommentPost: CommunityCommentPostModel, displayAttachedItem?: boolean, showReplies?: boolean, isLinkableToDetails: boolean}) {
    const [isShowingReplies, setIsShowingReplies] = useState(props.showReplies ? props.showReplies : false);
    const [communityCommentPost, setCommunityCommentPost] = useState<CommunityCommentPostModel>(props.communityCommentPost);
    const [alertMessage, setAlertMessage] = useAlertMessageGlobalState();
    const [displayLoginPopup, setDisplayLoginPopup] = useState(false);


    function renderUpvotesCommentsBtns() {
        const totalUpvotes: number = communityCommentPost.replies ? Object.keys(communityCommentPost.replies).length : 0
        return (
            <div className='flex flex-row gap-2'>
                <CommunityPostUpvoteBtnComponent communityPost={communityCommentPost} />
                <button className={"flex gap-1 btn btn-xs btn-secondary items-center btn-outline"}
                        onClick={() => setIsShowingReplies(true)}>
                    <FaRegCommentDots/> {totalUpvotes + " comments"}
                </button>
            </div>
        )
    }

    async function onSendReplyComment(text: string) {
        if (!AuthManager.isUserLogged()) {
            setDisplayLoginPopup(true);
            return false;
        }

        const apiResponse = await SharedBackendApi.communityReplyComment({
            comment: text,
            parentCommentId: communityCommentPost.uid!
        });
        if (apiResponse.isSuccess) {
            setCommunityCommentPost(apiResponse.responseData)
            RootGlobalStateActions.displayAlertMessage({
                alertType: "SUCCESS",
                message: "Comment published successfuly",
                setAlertMessageState: setAlertMessage,
            });
        } else {
            RootGlobalStateActions.displayAPIResponseError({
                apiResponse: apiResponse,
                setAlertMessageState: setAlertMessage,
            });
        }
        return true;
    }

    function renderAttachedItem() {
        if (props.displayAttachedItem && communityCommentPost.attachedItem) {
            return (
                <Link className='flex flex-row gap-2 p-2 mr-1 rounded bg-slate-50 hover:bg-slate-100 drop-shadow items-center text-slate-700' href={communityCommentPost.attachedItem.link} target={"_blank"}>
                    <Image className='flex h-full rounded min-w-16 w-16 max-h-12 max-h-12 sm:mt-1 object-cover' width={500} height={500} src={communityCommentPost.attachedItem.thumbnailUrl!} alt={communityCommentPost.attachedItem.title.replaceAll("*", "")} />
                    <h3 className='font-bold text-sm grow'>{communityCommentPost.attachedItem.title.replaceAll("*", "")}</h3>
                    <div className='h-full'>
                        <GoLinkExternal size={14}/>
                    </div>
                </Link>
            )
        } else {
            return <></>
        }
    }

    function renderReplies() {
        if (!isShowingReplies) {
            return (<></>);
        } else {
            let listRepliesItems: any[] = [];

            if (Object.keys(communityCommentPost.replies).length > 0) {
                let listReplyComments: CommunityCommentReplyModel[] = Object.values(communityCommentPost.replies).sort((c1, c2) => c1.createdAt! - c2.createdAt!);
                for (let replyComment of listReplyComments) {
                    listRepliesItems.push((
                        <CommunityCommentReplyItemComponent key={replyComment.uid} userInfos={communityCommentPost.usersInfos[replyComment.userCreatorId]} replyData={replyComment} />
                    ));
                }
            }
            return (
                <div className='flex flex-col gap-4 pl-10 pr-5 w-full my-5'>
                    {listRepliesItems}
                    <div className='mt-5'>
                        <CommuntyWriteCommentComponent onSendComment={onSendReplyComment} />
                    </div>
                </div>
            )
        }
    }

    function renderCommentText() {
        const linkProps = {
            target: '_blank',
            rel: 'noopener noreferrer',
            className: 'text-blue-600 hover:underline'
        };

        if (props.isLinkableToDetails) {
            return (
                <Link href={SharedRoutesUtils.getCommunityCommentDetailsPageUrl(communityCommentPost.uid!)}>
                    <Linkify componentDecorator={(decoratedHref, decoratedText, key) => (
                        <a {...linkProps} href={decoratedHref} key={key}>
                            {decoratedText}
                        </a>
                    )}>
                        <span className='text-slate-900 text-base'>{communityCommentPost.message}</span>
                    </Linkify>
                </Link>
            );
        } else {
            return (
                <Linkify componentDecorator={(decoratedHref, decoratedText, key) => (
                    <a {...linkProps} href={decoratedHref} key={key}>
                        {decoratedText}
                    </a>
                )}>
                    <span className='text-slate-900 text-base'>{communityCommentPost.message}</span>
                </Linkify>
            );
        }
    }

    if (CommunityUtils.isCommentBlocked(props.communityCommentPost.uid!, props.communityCommentPost.userCreatorId)) {
        return <></>
    } else {
        return (
            <div className='flex flex-col items-start gap-2 py-2 px-5'>
                <div className='flex flex-row gap-2 items-start w-full'>
                    <div className='flex grow'>
                        <UserHeaderInfosComponent userId={communityCommentPost.userCreatorId} isCreator={false} userInfos={communityCommentPost.usersInfos[communityCommentPost.userCreatorId]} />
                    </div>
                    <CommunityReportCommentFlagButtonComponent commentId={communityCommentPost.uid!} userId={communityCommentPost.userCreatorId} />
                </div>
                {renderCommentText()}
                {renderAttachedItem()}
                <span className='text-xs text-slate-400'>{CommonUtils.getTimeAgo(communityCommentPost.createdAt!)}</span>
                {renderUpvotesCommentsBtns()}
                {renderReplies()}
                <div className='divider'></div>

                <LoginPopupComponent isPopupOpen={displayLoginPopup} onClosed={() => setDisplayLoginPopup(false)} />
            </div>
        )
    }

}