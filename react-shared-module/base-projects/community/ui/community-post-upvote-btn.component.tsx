import {useAuthState} from "react-firebase-hooks/auth";
import {auth, FirebaseClientFirestoreUtils} from "../../../logic/shared-data/firebase.utils";
import {useEffect, useState} from "react";
import AuthManager from "../../../logic/auth/auth.manager";
import {SharedFirestoreCollectionDB} from "../../../logic/shared-data/datamodel/shared-firestore-collections";
import {deleteField} from "@firebase/firestore";
import {BiSolidUpArrow} from "react-icons/bi";
import {CommunityCommentPostModel} from "../data/community-data.model";
import LoginPopupComponent from "../../../ui-components/login/shared/LoginPopup.component";

export default function CommunityPostUpvoteBtnComponent(props: { communityPost: CommunityCommentPostModel}) {
    const [currentUser, isAuthLoading] = useAuthState(auth);
    const [hasUpvoted, setHasUpvoted] = useState(Object.keys(props.communityPost.reactions ? props.communityPost.reactions : {}).includes(AuthManager.getUserId() ? AuthManager.getUserId()! : ""));
    const [displayLoginPopup, setDisplayLoginPopup] = useState(false);

    if (!props.communityPost.reactions) {
        props.communityPost.reactions = {};
    }

    useEffect(() => {
        if (AuthManager.getUserId()) {
            setHasUpvoted(Object.keys(props.communityPost.reactions).includes(AuthManager.getUserId()!))
        }
    }, [currentUser]);

    async function onUpvoteClick() {
        // Backend API
        if (AuthManager.getUserId()) {
            let updateUserVote: boolean = !hasUpvoted;


            if (updateUserVote) {
                props.communityPost.reactions = {
                    ...props.communityPost.reactions,
                    [AuthManager.getUserId()!]: "UPVOTE",
                };
            } else {
                delete props.communityPost.reactions[AuthManager.getUserId()!];
            }
            setHasUpvoted(updateUserVote);

            const result = await FirebaseClientFirestoreUtils.updatePathDocument(
                [SharedFirestoreCollectionDB.CommunityCommentPosts],
                [props.communityPost.uid!], {
                    reactions: {
                        [AuthManager.getUserId()!]: updateUserVote ? "UPVOTE" : deleteField(),
                    },
                });
            if (!result) {
                setHasUpvoted(!updateUserVote);
            }
        } else {
            // If not logged in, then redirect him to the login page
            // router.push(SharedRoutesUtils.getLoginPageUrl());
            setDisplayLoginPopup(true);
        }

    }

    return (<button className={"btn btn-xs btn-secondary items-center " + (hasUpvoted ? "text-white" : "btn-outline")}
                    onClick={onUpvoteClick}>
        <BiSolidUpArrow className="mx-1"/> {Object.keys(props.communityPost.reactions).length + (hasUpvoted ? " Upvoted" : " Upvote")}

        <LoginPopupComponent isPopupOpen={displayLoginPopup} onClosed={() => setDisplayLoginPopup(false)} />
    </button>)

}