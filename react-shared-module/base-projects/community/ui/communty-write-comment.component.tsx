import {useState} from "react";
import {IoSend} from "react-icons/io5";
import {getLanguageLabel} from "../../../logic/language/language.helper";
import InputTextAreaComponent from "../../../ui-components/form/InputTextArea.component";
import AuthManager from "../../../logic/auth/auth.manager";
import CommunityAuthBanner from "./community-auth-banner.component";
import {useAuthState} from "react-firebase-hooks/auth";
import {auth} from "../../../logic/shared-data/firebase.utils";
import UserPictureAvatarComponent from "../../../ui-components/shared/UserPictureAvatar.component";

export default function CommuntyWriteCommentComponent(props: { defaultText?: string, onSendComment: (comment: string) => Promise<boolean> }) {
    const [formValue, setFormValue] = useState<string>(props.defaultText ? props.defaultText : "");
    const [isLoading, setIsLoading] = useState(false);
    const [currentUser, isAuthLoading] = useAuthState(auth);

    function isFormValid() {
        return formValue && formValue.length > 0;
    }

    async function onSendCommentClick() {
        setIsLoading(true);
        const result = await props.onSendComment(formValue);
        if (result) {
            setFormValue("");
        }
        setIsLoading(false);
    }


    if (AuthManager.isUserLogged()) {
        return (
            <div className='flex flex-col gap-2 w-full items-end'>
                <div className='flex flex-row gap-5 w-full'>
                    <div className='mt-10'>
                        <UserPictureAvatarComponent width={"w-12"} />
                    </div>
                    <InputTextAreaComponent customHeight={"h-12"} value={formValue} disabled={isLoading} topLabel={"Publish a comment"} hint={"Write your comment..."} onChanged={setFormValue} maxLength={300} inputType={"text"} />
                </div>
                <button className={"btn btn-secondary btn-ghost btn-sm text-sm sm:mb-8" + (isLoading ? "loading" : "")} disabled={!isFormValid() || isLoading} onClick={onSendCommentClick}>{getLanguageLabel("COMMENT_SEND_COMMENT")} <IoSend className='mx-2'/></button>
            </div>
        );
    } else {
        return <CommunityAuthBanner />
    }

}