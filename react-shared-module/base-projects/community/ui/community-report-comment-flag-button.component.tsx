import {useState} from "react";
import CommunityReportCommentComponent from "./community-report-comment.component";

export default function CommunityReportCommentFlagButtonComponent(props: {commentId: string, userId: string}) {
    const [isPopupOpen, setIsPopupOpen] = useState(false);

    return (
        <>
            <button className='btn btn-xs btn-ghost text-slate-400' onClick={() => setIsPopupOpen(true)}>Report Comment/User</button>
            {/*POPUP*/}
            <CommunityReportCommentComponent isPopupOpen={isPopupOpen} onPopupClosed={() => setIsPopupOpen(false)} commentId={props.commentId} userId={props.userId} />
        </>
    )
}