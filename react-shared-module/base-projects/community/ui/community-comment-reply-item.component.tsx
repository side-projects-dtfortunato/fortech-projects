import {UserShortProfileModel} from "../../../logic/shared-data/datamodel/shared-public-userprofile.model";
import {CommunityCommentReplyModel} from "../data/community-data.model";
import UserHeaderInfosComponent from "../../../ui-components/shared/UserHeaderInfos.component";
import Linkify from "react-linkify";
import CommonUtils from "../../../logic/commonutils";
import {CommunityUtils} from "../community.utils";
import CommunityReportCommentFlagButtonComponent from "./community-report-comment-flag-button.component";

export default function CommunityCommentReplyItemComponent(props: {userInfos: UserShortProfileModel, replyData: CommunityCommentReplyModel}) {

    if (CommunityUtils.isCommentBlocked(props.replyData.uid!, props.replyData.userCreatorId)) {
        return (<></>);
    } else {
        return (
            <div className="chat chat-start">
                <div className='flex flex-col items-start gap-2 py-2 bg-slate-50 rounded-xl p-2 drop-shadow min-w-60'>
                    <div className='flex flex-row w-full gap-2 items-start'>
                        <div className='flex grow'>
                            <UserHeaderInfosComponent userId={props.userInfos.uid!} isCreator={false} userInfos={props.userInfos} />
                        </div>
                        <CommunityReportCommentFlagButtonComponent commentId={props.replyData.uid!} userId={props.replyData.userCreatorId} />
                    </div>
                    <Linkify><span className='text-slate-900 text-base'>{props.replyData.message}</span></Linkify>
                    <span className='text-xs text-slate-400'>{CommonUtils.getTimeAgo(props.replyData.createdAt!)}</span>
                </div>
            </div>
        )
    }

}