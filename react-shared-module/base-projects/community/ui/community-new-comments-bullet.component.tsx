import {useEffect, useState} from "react";
import {ClientRealtimeDbUtils} from "../../../logic/shared-data/firebase.utils";
import {SharedRealtimeCollectionDB} from "../../../logic/shared-data/datamodel/shared-firestore-collections";
import LocalStorageDataManager from "../../../logic/managers/LocalStorageData.manager";
import {SharedLocalStorageKeysMap} from "../../../utils/shared-local-storage-keys.map";
import RootConfigs from "../../../../../configs";

export default function CommunityNewCommentsBulletComponent() {
    const [dataMostRecentComments, setDataMostRecentComments] = useState<{[commentId: string]: number}>({});
    const [communityLastVisit, setCommunityLastVisit] = useState(0);

    useEffect(() => {
        const communityLastVisit: string | undefined = LocalStorageDataManager.getString(SharedLocalStorageKeysMap.COMMUNITY_LAST_VISIT_TIMESTAMP, "0");

        if (communityLastVisit) {
            const lastVisitLocalStorage = LocalStorageDataManager.getString(SharedLocalStorageKeysMap.COMMUNITY_LAST_VISIT_TIMESTAMP, "0");
            if (lastVisitLocalStorage) {
                setCommunityLastVisit(Number(lastVisitLocalStorage));
            }
        }

        // Load Most Recent Comments data
        ClientRealtimeDbUtils.getData({
            collectionName: SharedRealtimeCollectionDB.CommunityMostRecentPosts,
        }).then((data) => {
            setDataMostRecentComments(data);
        })
    }, []);

    function getMostRecentCommunityPostCounter(): number {
        if (!dataMostRecentComments) {
            return 0;
        }

        if (communityLastVisit) {
            return Object.values(dataMostRecentComments).filter((commentTimestamp: number) => {
                return (commentTimestamp - communityLastVisit) > 0;
            }).length;
        } else {
            return Object.keys(dataMostRecentComments).length;
        }
    }

    if (getMostRecentCommunityPostCounter() > 0) {
        const bulletSize: string = getMostRecentCommunityPostCounter() > 9 ? "h-6 w-6" : "h-4 w-4";

        return (
            <div className={`flex bg-${RootConfigs.THEME.PRIMARY_COLOR}-900 rounded-full items-center justify-center ${bulletSize}`}>
                <span className='text-xs text-white text-center'>{getMostRecentCommunityPostCounter()}</span>
            </div>
        )
    } else {
        return (<></>)
    }

}