import React, {useState} from 'react';
import {MessageCircle} from 'lucide-react';


export default function CommunityEmptyStateComponent() {
    return (
        <div className="w-full max-w-2xl mx-auto py-8">
            <div className="flex flex-col items-center gap-4 p-8 text-center">
                {/* Simple icon */}
                <div className="w-12 h-12 flex items-center justify-center">
                    <MessageCircle className="w-full h-full text-gray-300" />
                </div>

                {/* Minimal text */}
                <div className="space-y-2">
                    <p className="text-sm text-gray-500 font-medium">
                        No posts yet
                    </p>
                    <p className="text-xs text-gray-400">
                        The community feed is currently empty, be the first publishing a community post
                    </p>
                </div>
            </div>
        </div>
    );
}