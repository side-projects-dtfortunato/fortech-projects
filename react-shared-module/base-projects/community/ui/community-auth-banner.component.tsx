import React, {useState} from 'react';
import { Users, ArrowRight, Sparkles } from 'lucide-react';
import Link from "next/link";
import {SharedRoutesUtils} from "../../../utils/shared-routes.utils";
import RootConfigs from "../../../../../configs";
import LoginPopupComponent from "../../../ui-components/login/shared/LoginPopup.component";

export default function CommunityAuthBanner() {
    const [displayLoginPopup, setDisplayLoginPopup] = useState(false);

    return (
        <div className="relative overflow-hidden w-full max-w-2xl mx-auto mb-8">
            {/* Background with gradient */}
            <div className="absolute inset-0 bg-gradient-to-r from-blue-600 to-purple-600 opacity-10 rounded-lg" />

            {/* Main banner content */}
            <div className="relative p-6 bg-white rounded-lg border border-gray-200 shadow-sm hover:shadow-md transition-shadow duration-300">
                <div className="flex flex-col sm:flex-row items-center gap-6">
                    {/* Icon section */}
                    <div className="flex-shrink-0">
                        <div className="p-3 bg-blue-50 rounded-full">
                            <Users className={`w-8 h-8 text-${RootConfigs.THEME.PRIMARY_COLOR}-600 `} />
                        </div>
                    </div>

                    {/* Text content */}
                    <div className="flex-grow text-center sm:text-left">
                        <div className="flex items-center justify-center sm:justify-start gap-2 mb-2">
                            <h3 className="text-lg font-semibold text-gray-900 ">
                                Join Our Community
                            </h3>
                            <Sparkles className="w-5 h-5 text-yellow-500 animate-pulse" />
                        </div>
                        <p className="text-sm text-gray-600 mb-4">
                            Create an account to share your thoughts, engage with others, and be part of our growing community.
                        </p>

                        {/* CTA buttons */}
                        <div className="flex flex-col sm:flex-row gap-3 justify-center sm:justify-start">
                            <button
                                className="btn btn-secondary text-white"
                                onClick={() => setDisplayLoginPopup(true)}
                            >
                                Sign up now
                                <ArrowRight className="w-4 h-4 ml-2" />
                            </button>

                        </div>
                    </div>
                </div>

                {/* Optional decorative elements */}
                <div className="absolute top-0 right-0 -mr-3 -mt-3 w-24 h-24 bg-blue-500/10 rounded-full blur-2xl" />
                <div className="absolute bottom-0 left-0 -ml-3 -mb-3 w-24 h-24 bg-purple-500/10 rounded-full blur-2xl" />


                <LoginPopupComponent isPopupOpen={displayLoginPopup} onClosed={() => setDisplayLoginPopup(false)} />
            </div>
        </div>
    );
}