import {CommentAttachedItemModel, CommunityCommentPostModel, CommunityParentRefs} from "../data/community-data.model";
import {useEffect, useState} from "react";
import {ClientRealtimeDbUtils} from "../../../logic/shared-data/firebase.utils";
import {SharedRealtimeCollectionDB} from "../../../logic/shared-data/datamodel/shared-firestore-collections";
import AuthManager from "../../../logic/auth/auth.manager";
import {SharedBackendApi} from "../../../logic/shared-data/sharedbackend.api";
import CommunityCommentItem from "./community-comment-item";
import {getLanguageLabel} from "../../../logic/language/language.helper";
import CommuntyWriteCommentComponent from "./communty-write-comment.component";
import {RootGlobalStateActions, useAlertMessageGlobalState} from "../../../logic/global-hooks/root-global-state";
import LoginPopupComponent from "../../../ui-components/login/shared/LoginPopup.component";

export default function PostCommentsRootComponent(props: {communityParentRefs: CommunityParentRefs, loadedPostCommentRoot?: {[commentId: string]: CommunityCommentPostModel}, attachedItem?: CommentAttachedItemModel}) {
    const [postCommentsListData, setPostCommentsListData] = useState<{[commentId: string]: CommunityCommentPostModel} | undefined>(props.loadedPostCommentRoot);
    const [displayLoginPopup, setDisplayLoginPopup] = useState(false);
    const [alertMessage, setAlertMessage] = useAlertMessageGlobalState();

    useEffect(() => {
        fetchPostComments();
    }, []);


    async function fetchPostComments() {
        let fetchedData: any = await ClientRealtimeDbUtils.getData({
            collectionName: SharedRealtimeCollectionDB.CommunityParentCommentsAggregator,
            subPaths: [props.communityParentRefs.parentDocId],
            docId: props.communityParentRefs.parentCollectionId,
        });
        if (!fetchedData) {
            fetchedData = {};
        }
        setPostCommentsListData(fetchedData);
    }

    async function onSendComment(comment: string, replyCommentId?: string): Promise<boolean> {
        if (AuthManager.isUserLogged()) {

            const apiResponse = await SharedBackendApi.communityPostComment({
                comment: comment,
                attachedItem: props.attachedItem,
                parentRefsId: props.communityParentRefs,
            });


            if (apiResponse.isSuccess) {
                RootGlobalStateActions.displayAlertMessage({
                    alertType: "SUCCESS",
                    message: "Your comment was successfuly published",
                    setAlertMessageState: setAlertMessage,
                });
            } else {
                RootGlobalStateActions.displayAPIResponseError({
                    apiResponse: apiResponse,
                    setAlertMessageState: setAlertMessage,
                });
            }

            if (apiResponse.isSuccess) {
                setPostCommentsListData({
                    ...postCommentsListData,
                    [apiResponse.responseData.uid]: apiResponse.responseData,
                })
            }
            return apiResponse.isSuccess;
        } else {
            setDisplayLoginPopup(true);
            return false;
        }

    }

    function renderListComments() {
        let listItems: any[] = [];
        if (postCommentsListData && Object.keys(postCommentsListData).length > 0) {
            listItems = Object.values(postCommentsListData)
                .sort((c1, c2) => c2.createdAt! - c1.createdAt!)
                .map((commentItem) => <CommunityCommentItem key={commentItem.uid!} communityCommentPost={commentItem} isLinkableToDetails={true} />);
        }

        return (
            <div className='flex flex-col gap-3'>
                {listItems}
            </div>
        )
    }

    function renderTitle() {
        let numComments: number = 0;
        if (postCommentsListData) {
            numComments = Object.keys(postCommentsListData).length;
        }
        return (
            <div className='flex flex-row text-lg font-bold items-center gap-2'>
                <h3 className='font-bold text-slate-700 my-0' style={{margin: 0}}>{getLanguageLabel("COMMENT_TITLE")}</h3>
                <div className='flex items-center justify-center bg-slate-700 text-white rounded-full w-6 h-6'>{numComments}</div>
            </div>
        )
    }

    return (
        <div className='flex flex-col w-full py-2 gap-4'>
            {renderTitle()}
            <CommuntyWriteCommentComponent onSendComment={onSendComment} />
            {renderListComments()}

            <LoginPopupComponent isPopupOpen={displayLoginPopup} onClosed={() => setDisplayLoginPopup(false)} />
        </div>
    )
}