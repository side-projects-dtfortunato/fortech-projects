import LocalStorageDataManager from "../../logic/managers/LocalStorageData.manager";
import CommonUtils from "../../logic/commonutils";

interface LocalReportedCommentsModel {
    blockedCommentIds: string[],
    blockedUserIds: string[],
}
const LOCAL_REPORTED_COMMENTS_KEY = "reported_comments";

export class CommunityUtils {
    static reportedCommentsData?: LocalReportedCommentsModel;

    private static loadReportedComments() {
        this.reportedCommentsData = LocalStorageDataManager.getItem(LOCAL_REPORTED_COMMENTS_KEY);
        if (!this.reportedCommentsData) {
            this.reportedCommentsData = {
                blockedCommentIds: [],
                blockedUserIds: [],
            }
        }
    }

    static reportComment(commentId: string, blockUserId?: string) {
        if (!this.reportedCommentsData) {
            this.loadReportedComments();
        }

        // Add reported comment
        if (!this.reportedCommentsData?.blockedCommentIds.includes(commentId)) {
            this.reportedCommentsData?.blockedCommentIds.push(commentId);
        }

        // Add Blocked User
        if (blockUserId && !this.reportedCommentsData?.blockedUserIds.includes(blockUserId)) {
            this.reportedCommentsData?.blockedUserIds.push(blockUserId);
        }

        // Save Data
        LocalStorageDataManager.saveItem(LOCAL_REPORTED_COMMENTS_KEY, this.reportedCommentsData);

        // Refresh page
        CommonUtils.refreshPage();
    }

    static isCommentBlocked(commentId: string, userId: string) {
        if (!this.reportedCommentsData) {
            this.loadReportedComments();
        }

        return this.reportedCommentsData?.blockedCommentIds.includes(commentId)
            || this.reportedCommentsData?.blockedUserIds.includes(userId);
    }

}