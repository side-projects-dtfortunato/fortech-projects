import {useEffect, useState} from "react";
import {SharedBackendApi} from "../../logic/shared-data/sharedbackend.api";
import CommuntyWriteCommentComponent from "./ui/communty-write-comment.component";
import InfiniteScroll from "react-infinite-scroll-component";
import SpinnerComponent from "../../ui-components/shared/Spinner.component";
import {ClientRealtimeDbUtils, FirebaseClientFirestoreUtils} from "../../logic/shared-data/firebase.utils";
import {SharedFirestoreCollectionDB} from "../../logic/shared-data/datamodel/shared-firestore-collections";
import {CommunityCommentPostModel, DailyCommunityPublishes} from "./data/community-data.model";
import CommunityCommentItem from "./ui/community-comment-item";
import InlineLabelSeparatorComponent from "../../ui-components/shared/inline-label-separator.component";
import CommonUtils from "../../logic/commonutils";
import {RootGlobalStateActions, useAlertMessageGlobalState} from "../../logic/global-hooks/root-global-state";
import AuthManager from "../../logic/auth/auth.manager";
import LocalStorageDataManager from "../../logic/managers/LocalStorageData.manager";
import {SharedLocalStorageKeysMap} from "../../utils/shared-local-storage-keys.map";
import LoginPopupComponent from "../../ui-components/login/shared/LoginPopup.component";
import RootConfigs from "../../../../configs";
import CommunityEmptyStateComponent from "./ui/community-empty-state.component";
import {Users2} from "lucide-react";
import {getLanguageLabel} from "../../logic/language/language.helper";
import JoinTelegramButtonComponent from "../../ui-components/shared/JoinTelegramButton.component";

export default function CommunityPageComponent(props: { preloadedData: DailyCommunityPublishes[], itemsPerPage: number }) {
    const [listDailyPublishes, setListDailyPublishes] = useState(props.preloadedData);
    const [hasMorePages, setHasMorePages] = useState(props.preloadedData.length >= props.itemsPerPage);
    const [alertMessage, setAlertMessage] = useAlertMessageGlobalState();
    const [displayLoginPopup, setDisplayLoginPopup] = useState(false);
    const [totalUsers, setTotalUsers] = useState(-1);

    useEffect(() => {
        // Update LocalStorage with the last visit
        LocalStorageDataManager.saveString(SharedLocalStorageKeysMap.COMMUNITY_LAST_VISIT_TIMESTAMP, Date.now().toString());

        ClientRealtimeDbUtils.getData({
            collectionName: SharedFirestoreCollectionDB.AdminRegisteredUsersCounterRTDB,
        }).then((totalUsers) => {
            if (totalUsers) {
                setTotalUsers(totalUsers);
            }
        });

    }, []);


    async function onSendComment(comment: string) {

        if (AuthManager.isUserLogged()) {
            // Backend API
            const apiResponse = await SharedBackendApi.communityPostComment({
                comment: comment,
            });

            if (apiResponse.isSuccess) {
                RootGlobalStateActions.displayAlertMessage({
                    alertType: "SUCCESS",
                    message: "Your comment was successfuly published",
                    setAlertMessageState: setAlertMessage,
                });
            } else {
                RootGlobalStateActions.displayAPIResponseError({
                    apiResponse: apiResponse,
                    setAlertMessageState: setAlertMessage,
                });
            }
            return apiResponse.isSuccess;
        } else {
            setDisplayLoginPopup(true);
            return false;
        }

    }


    async function loadNextPage() {
        // BackendAPI
        let lastDocId: number | undefined = listDailyPublishes.length > 0 ? listDailyPublishes[listDailyPublishes.length - 1].publishedDayId : undefined;
        const response: any[] = await FirebaseClientFirestoreUtils
            .getDocumentsWithPagination(SharedFirestoreCollectionDB.CommunityDailyPublishes, props.itemsPerPage, "publishedDayId", lastDocId);

        if (response && response.length > 0) {
            setListDailyPublishes(listDailyPublishes.concat(response));
            setHasMorePages(response.length === props.itemsPerPage); // If the response has the same number of items per page, means that has possible more pages
        } else {
            setHasMorePages(false);
        }
    }

    function renderCommentItem(commentPostModel: CommunityCommentPostModel) {
        return (
            <CommunityCommentItem key={commentPostModel.uid!} communityCommentPost={commentPostModel}
                                  isLinkableToDetails={true}
                                  displayAttachedItem={true}/>
        )
    }

    function renderListItems() {
        if (listDailyPublishes.length === 0) {
            return <CommunityEmptyStateComponent/>
        } else {
            let listItems: any[] = listDailyPublishes.map((dailyPublish) => {
                let listDailyComments: any[] = Object.values(dailyPublish.listPosts)
                    .sort((comment1, comment2) => comment2.createdAt! - comment1.createdAt!)
                    .map((comment) => renderCommentItem(comment));
                let title: string = "";

                if (dailyPublish.publishedDayId === CommonUtils.getTodayDay()) {
                    title = "Today"
                } else if (dailyPublish.publishedDayId === (CommonUtils.getTodayDay() - 1)) {
                    title = "Yesterday"
                } else {
                    title = CommonUtils.getTimeAgo(CommonUtils.convertDaytimestampToMillis(dailyPublish.publishedDayId));
                }
                return (
                    <div key={dailyPublish.publishedDayId} className='flex flex-col gap-2'>
                        {
                            RootConfigs.META_CONFIGS["disableCommunitySeparator"] === true ?
                                <></> :
                                <InlineLabelSeparatorComponent title={title} titleSize={"medium"}/>
                        }
                        {listDailyComments}
                    </div>
                )
            });

            return listItems;
        }

    }

    function renderHeader() {
        return (
            <div className='flex flex-col gap-2 items-center'>
                <span className='flex flex-col gap-2 items-center my-2 sm:my-2'>
                    <h1 className={`text-${RootConfigs.THEME.PRIMARY_COLOR}-900 text-3xl font-bold`}>Community Feed</h1>
                    <span className={`text-slate-700 text-md`}>{getLanguageLabel("COMMUNITY_SUBTITLE")}</span>
                </span>
                {totalUsers > 0 ?
                    <div className="flex items-center gap-2">
                        <div className="w-10 h-10 rounded-full bg-green-50 flex items-center justify-center">
                            <Users2 className="w-5 h-5 text-green-600"/>
                        </div>
                        <div className='flex items-center gap-1'>
                            <p className="text-sm font-medium text-gray-900">{totalUsers}</p>
                            <p className="text-xs text-gray-500">Members</p>
                        </div>
                    </div> : <></>}
            </div>
        )
    }

    function renderCommunityHeader() {
        return (
            <div className='flex flex-col gap-2 p-5 sm:rounded-xl drop-shadow-xl bg-white'>
                {renderHeader()}
                <CommuntyWriteCommentComponent onSendComment={onSendComment}/>
                <JoinTelegramButtonComponent />
            </div>
        )
    }

    function renderCommunityPosts() {
        return (
            <div className='flex flex-col gap-2 p-5 sm:rounded-xl drop-shadow-xl bg-white'>
                <InfiniteScroll next={loadNextPage} hasMore={hasMorePages} loader={<SpinnerComponent/>}
                                dataLength={listDailyPublishes.length}>
                    {renderListItems()}
                </InfiniteScroll>
            </div>

        )
    }

    return (
        <div className='flex flex-col gap-2 min-h-screen p-5'>
            {renderCommunityHeader()}
            {renderCommunityPosts()}

            <LoginPopupComponent isPopupOpen={displayLoginPopup} onClosed={() => setDisplayLoginPopup(false)}/>
        </div>
    )
}