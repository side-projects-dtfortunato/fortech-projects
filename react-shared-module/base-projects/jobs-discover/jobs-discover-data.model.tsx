import {BaseModel} from "../../logic/shared-data/datamodel/base.model";
import {SharedFirestoreCollectionDB} from "../../logic/shared-data/datamodel/shared-firestore-collections";
import {collection, getDocs, limit, orderBy, query, Query, where, WhereFilterOp} from 'firebase/firestore';
import {ClientRealtimeDbUtils, FirebaseClientFirestoreUtils} from "../../logic/shared-data/firebase.utils";
import RootConfigs from "../../../../configs";
import {JobPosting, NewsArticle} from "schema-dts";
import {jsonLdScriptProps} from "react-schemaorg";
import LocalStorageDataManager from "../../logic/managers/LocalStorageData.manager";
import {SharedWebToAppBridgeUtils} from "../../utils/shared-web-to-app-bridge.utils";


export type JobWorkModeType = "HYBRID" | "REMOTE" | "ON-SITE";
export type JobRemoteRegion = "USA" | "EU" | "UK" | "Canada" | "Australia" | "WORLDWIDE" | string;
export type JobPublishPlanType = "IMPORTED" | "BASIC" | "FEATURED";

export interface DailyPublishedJobs {
    publishedDayId: number,
    listJobs: {
        [jobId: string]: JobListModel,
    },
}

export interface JobListModel extends BaseModel {
    title: string;
    company: string;
    companyLogo: string;
    companyUrl?: string;
    companyThumbnailUrl?: string;
    location: string;
    link: string;
    workMode?: JobWorkModeType;
    remoteRegion?: JobRemoteRegion;
    category?: string;
    source?: string;
    publishPlanType?: JobPublishPlanType;
    publisherEmail?: string;
    skills?: string[];
    isPopular?: boolean;
}

export interface JobDetailsModel extends JobListModel {
    experienceLevel: string;
    description: string;
    applicationUrl: string;
    publishedDayId: number;
    salaryRange?: JobSalaryRange;
}


export interface JobFiltersSelected {
    category: string;
    remoteRegion: JobRemoteRegion | string;
    skillSelected?: string;
    workMode?: string;
}

export interface JobSalaryRange {
    min?: number;
    max?: number;
    currency?: string;
    isEstimated?: boolean;
}

export class JobsDiscoverDataUtils {
    static JOBS_FILTERS_KEY = "jobs_discovery_filters";

    static getSavedFilters() {
        return LocalStorageDataManager.getItem(this.JOBS_FILTERS_KEY);
    }

    static saveFilters(filters: any) {
        LocalStorageDataManager.saveItem(this.JOBS_FILTERS_KEY, filters);

        // Save the filter selection
        SharedWebToAppBridgeUtils.sendUserTags({
            category: filters?.category,
            remoteRegion: filters?.remoteRegion
        });
    }

    static async fetchFilteredJobs(filters: JobFiltersSelected, searchTokens?: string[], onlyFeatured?: boolean): Promise<JobListModel[]> {
        const jobsRef = collection(FirebaseClientFirestoreUtils.getDB(), SharedFirestoreCollectionDB.JobsDiscoverProject.JobDetails);
        let q: Query = jobsRef;

        // Helper function to add a filter to the query
        const addFilter = (field: string, operator: WhereFilterOp, value: any) => {
            q = query(q, where(field, operator, value));
        };

        // Apply filters
        if (filters.category && filters.category !== 'All Job Categories') {
            addFilter('category', '==', filters.category);
        }

        if (filters.remoteRegion && filters.remoteRegion !== 'All Regions' && filters.remoteRegion !== 'WORLDWIDE') {
            addFilter('remoteRegion', '==', filters.remoteRegion);
        }

        if (filters.skillSelected && filters.skillSelected.length > 0) {
            addFilter('skills', 'array-contains', filters.skillSelected);
        }

        if (filters.workMode && filters.workMode.length > 0) {
            addFilter('workMode', '==', filters.workMode);
        }

        if (searchTokens && searchTokens.length > 0) {
            addFilter('searchableTokens', 'array-contains-any', searchTokens);
        }

        if (onlyFeatured) {
            addFilter('publishPlanType', '==', 'FEATURED');
        }

        // Add sorting by createdAt in descending order (most recent first)
        q = query(q, orderBy('createdAt', 'desc'));

        // Add limit of 30 results
        q = query(q, limit(30));

        try {
            const querySnapshot = await getDocs(q);
            const results = querySnapshot.docs.map(doc => ({
                uid: doc.id,
                ...doc.data()
            } as JobListModel));

            return results;
        } catch (error) {
            console.error('Error fetching jobs:', error);
            return [];
        }
    }



    static generateSEODescription(job: JobListModel): string {
        let description = `Explore ${job.title} opportunity`;

        if (job.company) {
            description += ` at ${job.company}`;
        }

        if (job.location) {
            description += ` in ${job.location}`;
        }

        if (job.workMode) {
            description += ` - ${job.workMode}`;
        }

        if (job.remoteRegion) {
            description += ` for ${job.remoteRegion}`;
        }

        if (job.category) {
            description += ` in the ${job.category} field`;
        }

        description += `. Find remote jobs on ${RootConfigs.SITE_NAME}.`;

        // Ensure the description doesn't exceed a reasonable length for SEO
        return description.slice(0, 160);
    };

    static getExperienceInMonths(job: JobDetailsModel): string {
        switch (job.experienceLevel.toLowerCase()) {
            case "internship": return "0";
            case "entry level": return "12";
            case "associate": return "24";
            case "mid-senior level": return "36";
            case "director": return "72";
            case "executive": return "120";
            default: return "24";
        }
    }

    static generateJobPostingSchema(job: JobDetailsModel) {
        const jsonSchema: JobPosting = {
            // @ts-ignore
            "@context": "https://schema.org",
            "@type": "JobPosting",
            title: job.title,
            description: job.description,
            datePosted: new Date(job.createdAt!).toISOString(),
            validThrough: new Date(new Date(job.createdAt!).setDate(new Date(job.createdAt!).getDate() + 30)).toISOString(), // Adds 30 days to createdAt
            employmentType: job.workMode === "REMOTE" ? "REMOTE" : "FULL_TIME",
            hiringOrganization: {
                "@type": "Organization",
                name: job.company,
                sameAs: job.companyUrl,
                logo: job.companyLogo
            },
            jobLocation: {
                "@type": "Place",
                address: {
                    "@type": "PostalAddress",
                    addressLocality: job.location
                }
            },
            experienceRequirements : {
                "@type" : "OccupationalExperienceRequirements",
                "monthsOfExperience" : this.getExperienceInMonths(job)
            } as any,
            industry: job.category,
            occupationalCategory: job.category,
            url: `${RootConfigs.BASE_URL}/job/${job.uid!}`
        };
        if (job.workMode === "REMOTE") {
            jsonSchema.jobLocationType = "TELECOMMUTE";
        }

        if (job.remoteRegion) {
            jsonSchema.applicantLocationRequirements = {
                "@type": "Country",
                name: job.remoteRegion
            };
        }

        if (job.salaryRange) {
            jsonSchema.baseSalary = {
                "@type": "MonetaryAmount",
                currency: job.salaryRange.currency,
                value: {
                    "@type": "QuantitativeValue",
                    minValue: job.salaryRange.min,
                    maxValue: job.salaryRange.max,
                    unitText: "YEAR"
                }
            };
        }

        return (
            // @ts-ignore
            <script {...jsonLdScriptProps<JobPosting>(jsonSchema)} />
        );
    }

    static getCurrencySymbol = (currency: string): string => {
        switch (currency) {
            case 'AUD': return 'A$';
            case 'USD': return '$';
            case 'EUR': return '€';
            case 'GBP': return '£';
            default: return currency;
        }
    };

    static async getFiltersListSkills() {
        // Function to check if the words in skill1 are a subset of the words in skill2
        function isSkillSubset(skill1: string, skill2: string): boolean {
            const words1 = skill1.toLowerCase().split(/\s+/);
            const words2 = skill2.toLowerCase().split(/\s+/);
            return words1.every(word => words2.includes(word));
        }

        const skillsData: any = await ClientRealtimeDbUtils.getData({
            collectionName: SharedFirestoreCollectionDB.JobsDiscoverProject.JobSkillsCounterRTDB
        });
        let filterListSkills: string[] = [];
        if (skillsData) {
            // Filter out entries where the value is not a valid number
            const validSkills = Object.entries(skillsData).filter(
                ([_, count]) => typeof count === 'number' && !isNaN(count)
            );

            // Sort the valid skills by count in descending order
            const sortedSkills = validSkills.sort(
                ([_, countA], [__, countB]) => (countB as number) - (countA as number)
            );

            // Build the filtered list of top skills without overlaps
            for (const [skill, _] of sortedSkills) {
                // Remove the word 'SKILLS' from the skill name (case-insensitive)
                const cleanedSkill = skill.replace(/\bskills\b/i, '').trim();

                // Skip if the cleaned skill is empty after removal
                if (cleanedSkill === '') {
                    continue;
                }

                let shouldAddSkill = true;

                // Check for overlaps with the existing skills in the filtered list
                for (let i = 0; i < filterListSkills.length; i++) {
                    const existingSkill = filterListSkills[i];

                    if (isSkillSubset(cleanedSkill, existingSkill)) {
                        // Current skill is a subset of an existing skill; skip it
                        shouldAddSkill = false;
                        break;
                    } else if (isSkillSubset(existingSkill, cleanedSkill)) {
                        // Existing skill is a subset of the current skill; replace it
                        filterListSkills.splice(i, 1);
                        i--; // Adjust index after removal
                    }
                }

                if (shouldAddSkill) {
                    filterListSkills.push(cleanedSkill);
                }

                if (filterListSkills.length >= 20) {
                    break;
                }
            }
        }
        return filterListSkills;
    }

    static getOtherRemoteJobLinks() {
        let listFooterLinks = [];
        // Partners
        listFooterLinks.push({
            categoryId: "Partners",
            categoryLabel: "Partners",
            label: "Remote Work App",
            linkUrl: "https://remote-work.app",
            newTab: true,
        });
        listFooterLinks.push({
            categoryId: "Partners",
            categoryLabel: "Partners",
            label: "Remote Jobs Hub",
            linkUrl: "https://remotejobshub.app",
            newTab: true,
        });
        listFooterLinks.push({
            categoryId: "Partners",
            categoryLabel: "Partners",
            label: "Customer Remote Jobs",
            linkUrl: "https://customerremotejobs.com",
            newTab: true,
        });
        listFooterLinks.push({
            categoryId: "Partners",
            categoryLabel: "Partners",
            label: "Junior Remote Jobs",
            linkUrl: "https://juniorremotejobs.com",
            newTab: true,
        });
        listFooterLinks.push({
            categoryId: "Partners",
            categoryLabel: "Partners",
            label: "Remote In Australia",
            linkUrl: "https://remoteinaustralia.com",
            newTab: true,
        });
        listFooterLinks.push({
            categoryId: "Partners",
            categoryLabel: "Partners",
            label: "ReactJS Dev Remote Jobs",
            linkUrl: "https://reactremotejobs.com",
            newTab: true,
        });
        listFooterLinks.push({
            categoryId: "Partners",
            categoryLabel: "Partners",
            label: "Design Remote jobs",
            linkUrl: "https://designremotejobs.com",
            newTab: true,
        });
        listFooterLinks.push({
            categoryId: "Partners",
            categoryLabel: "Partners",
            label: "Marketing Remote Jobs",
            linkUrl: "https://marketingremotejobs.app",
            newTab: true,
        });
        listFooterLinks.push({
            categoryId: "Partners",
            categoryLabel: "Partners",
            label: "Remote IT jobs",
            linkUrl: "https://remoteitjobs.app",
            newTab: true,
        });

        // filter the current link
        listFooterLinks = listFooterLinks.filter((link) => !link.linkUrl.includes(RootConfigs.BASE_URL));

        return listFooterLinks
    }

    static convertHTMLtoMarkdown(html: string): string {
        if (!html) return '';

        // Remove any script tags and their content
        html = html.replace(/<script\b[^<]*(?:(?!<\/script>)<[^<]*)*<\/script>/gi, '');

        // Convert common HTML elements to Markdown
        let markdown = html
            // Replace break tags with newlines
            .replace(/<br\s*\/?>/gi, '\n')
            
            // Convert paragraphs
            .replace(/<p[^>]*>(.*?)<\/p>/gi, '$1\n\n')
            
            // Convert headings
            .replace(/<h1[^>]*>(.*?)<\/h1>/gi, '# $1\n\n')
            .replace(/<h2[^>]*>(.*?)<\/h2>/gi, '## $1\n\n')
            .replace(/<h3[^>]*>(.*?)<\/h3>/gi, '### $1\n\n')
            
            // Convert lists
            .replace(/<ul[^>]*>(.*?)<\/ul>/gi, '$1\n')
            .replace(/<ol[^>]*>(.*?)<\/ol>/gi, '$1\n')
            .replace(/<li[^>]*>(.*?)<\/li>/gi, '* $1\n')
            
            // Convert bold and italic
            .replace(/<strong[^>]*>(.*?)<\/strong>/gi, '**$1**')
            .replace(/<b[^>]*>(.*?)<\/b>/gi, '**$1**')
            .replace(/<em[^>]*>(.*?)<\/em>/gi, '_$1_')
            .replace(/<i[^>]*>(.*?)<\/i>/gi, '_$1_')
            
            // Convert links
            .replace(/<a[^>]*href="([^"]*)"[^>]*>(.*?)<\/a>/gi, '[$2]($1)')
            
            // Convert code blocks
            .replace(/<pre[^>]*>(.*?)<\/pre>/gi, '```\n$1\n```')
            .replace(/<code[^>]*>(.*?)<\/code>/gi, '`$1`')
            
            // Remove other HTML tags
            .replace(/<[^>]*>/g, '');

        // Decode HTML entities
        markdown = markdown
            .replace(/&nbsp;/g, ' ')
            .replace(/&amp;/g, '&')
            .replace(/&lt;/g, '<')
            .replace(/&gt;/g, '>')
            .replace(/&quot;/g, '"')
            .replace(/&#39;/g, "'");

        // Clean up extra whitespace
        markdown = markdown
            .replace(/\n\s*\n\s*\n/g, '\n\n')  // Replace multiple newlines with double newlines
            .replace(/^\s+|\s+$/g, '');         // Trim whitespace from start and end

        return markdown;
    }
}