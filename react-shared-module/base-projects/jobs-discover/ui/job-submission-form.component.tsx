import React, {useEffect, useState} from 'react';
import dynamic from 'next/dynamic';
import {JobDetailsModel, JobPublishPlanType, JobsDiscoverDataUtils} from '../jobs-discover-data.model'; // Adjust the import path as needed
import {toast, ToastContainer} from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import 'react-quill/dist/quill.snow.css';
import RootConfigs from "../../../../../configs";
import {UIHelper} from "../../../../ui-helper/UIHelper";
import {useCentralUserDataState} from "../../../logic/global-hooks/root-global-state";
import PaymentLoadingComponent from "../../../ui-components/shared/PaymentLoading.component";
import {HiOutlineBriefcase, HiOutlineCurrencyDollar, HiOutlineGlobeAlt, HiOutlineStar, HiOutlineBadgeCheck} from 'react-icons/hi';
import {getLanguageLabel} from "../../../logic/language/language.helper";

// Dynamically import ReactQuill to avoid SSR issues
const ReactQuill = dynamic(() => import('react-quill'), {
    ssr: false,
    loading: () => <p>Loading Editor...</p>,
});

// Rich text editor configuration
const QUILL_MODULES = {
    toolbar: [
        [{'header': [1, 2, 3, false]}],
        ['bold', 'italic', 'underline', 'strike'],
        [{'list': 'ordered'}, {'list': 'bullet'}],
        ['link'],
        ['clean']
    ],
};

interface JobSubmissionFormProps {
    onSubmit: (jobDetails: JobDetailsModel) => Promise<boolean>;
}

const JobSubmissionForm: React.FC<JobSubmissionFormProps> = ({onSubmit}) => {
    // Initial form state with default values
    const initialFormState: Partial<JobDetailsModel> = {
        title: '',
        company: '',
        companyLogo: '',
        companyUrl: '',
        location: '',
        workMode: 'REMOTE',
        remoteRegion: "USA",
        category: '',
        experienceLevel: '',
        description: '',
        applicationUrl: '',
        publisherEmail: '',
        publishPlanType: "BASIC",
        salaryRange: {
            min: undefined,
            max: undefined,
            currency: "USD",
        }
    };

    // State management
    const [formData, setFormData] = useState<Partial<JobDetailsModel>>(initialFormState);
    const [categories] = useState<string[]>(UIHelper.getJobsCategories());
    const [isSubmitting, setIsSubmitting] = useState(false);
    const [centralUserData] = useCentralUserDataState();
    const [collapsePercentage, setCollapsePercentage] = useState(0);
    const [selectedPackage, setSelectedPackage] = useState<JobPublishPlanType>(
        formData.publishPlanType ? formData.publishPlanType : "BASIC"
    );

    // Effect to auto-fill publisher email if user is logged in
    useEffect(() => {
        if (centralUserData?.email) {
            setFormData(prevState => ({
                ...prevState,
                publisherEmail: centralUserData.email,
            }));
        }
    }, [centralUserData]);

    // Header collapse effect based on scroll position
    useEffect(() => {
        const handleScroll = () => {
            const scrollPosition = window.scrollY;
            const maxScroll = window.innerHeight * 0.3; // Complete collapse at 30% of viewport
            
            // Calculate percentage based on scroll position
            const percentage = (scrollPosition / maxScroll) * 100;
            setCollapsePercentage(Math.min(100, Math.max(0, percentage)));
        };

        window.addEventListener('scroll', handleScroll);
        return () => window.removeEventListener('scroll', handleScroll);
    }, []);

    // Scroll to top when submitting
    useEffect(() => {
        if (isSubmitting) {
            window.scrollTo({
                top: 0,
                behavior: 'smooth'
            });
        }
    }, [isSubmitting]);

    // Form event handlers
    const handleChange = (e: React.ChangeEvent<HTMLInputElement | HTMLSelectElement>) => {
        const {name, value} = e.target;
        setFormData(prevState => ({
            ...prevState,
            [name]: value
        }));
    };

    const handleSalary = (e: React.ChangeEvent<HTMLInputElement | HTMLSelectElement>) => {
        const {name, value} = e.target;
        setFormData(prevState => ({
            ...prevState,
            salaryRange: {
                ...prevState.salaryRange,
                [name]: value
            }
        }));
    };

    const handleEditorChange = (content: string) => {
        setFormData(prevState => ({
            ...prevState,
            description: content
        }));
    };

    const handleChangePackage = (plan: JobPublishPlanType) => {
        setSelectedPackage(plan);
        setFormData(prevState => ({
            ...prevState,
            publishPlanType: plan
        }));
    };

    const handleClearForm = () => {
        setFormData(initialFormState);
    };

    // Form submission handler
    const handleSubmit = async (e: React.FormEvent<HTMLFormElement>) => {
        e.preventDefault();
        setIsSubmitting(true);

        // Small delay to ensure smooth scroll starts before heavy processing
        await new Promise(resolve => setTimeout(resolve, 100));

        const jobDetails: JobDetailsModel = {
            ...formData,
            publishedDayId: Math.floor(Date.now() / 86400000),
            source: RootConfigs.SITE_NAME,
            title: formData.title || '',
            company: formData.company || '',
            companyLogo: formData.companyLogo || '',
            location: formData.location || '',
            experienceLevel: formData.experienceLevel || '',
            description: formData.description || '',
            applicationUrl: formData.applicationUrl || '',
            link: "",
        };

        try {
            const result = await onSubmit(jobDetails);
            if (result) {
                toast.success('Job post submitted successfully!', {
                    position: "top-center"
                });
                setFormData(initialFormState);  // Reset form
            } else {
                toast.error('Failed to submit job post. Please try again.', {
                    position: "top-center"
                });
            }
        } catch (error) {
            console.error('Error submitting job post:', error);
            toast.error('An error occurred while submitting. Please try again.', {
                position: "top-center"
            });
        } finally {
            setIsSubmitting(false);
        }
    };

    // UI Components
    const renderHeader = () => (
        <div 
            className="relative w-full bg-gradient-to-b from-gray-50 to-white border-b border-gray-200 transition-all duration-300"
            style={{
                transform: `translateY(-${collapsePercentage * 0.4}%)`,
            }}
        >
            <div 
                className="relative transition-all duration-300"
                style={{
                    padding: `${Math.max(0.8, 3 - (collapsePercentage * 0.022))}rem 0`,
                }}
            >
                <div className="w-full">
                    <div className="text-center space-y-4">
                        {/* Title Section */}
                        <div 
                            className="flex flex-col items-center gap-2 transition-all duration-300"
                            style={{
                                transform: `scale(${1 - (collapsePercentage * 0.0015)})`,
                            }}
                        >
                            <h1 className="text-3xl sm:text-4xl font-bold text-gray-900 tracking-tight">
                                <span className="flex flex-col items-center gap-1">
                                    <span>{getLanguageLabel("REMOTE_JOBS_POST_JOB_TITLE")}</span>
                                    <span className={`text-${RootConfigs.THEME.PRIMARY_COLOR}-800 flex items-center gap-2`}>
                                        <HiOutlineBriefcase className="w-8 h-8"/>
                                        {getLanguageLabel("REMOTE_JOBS_POST_JOB_SUBTITLE")}
                                    </span>
                                </span>
                            </h1>
                        </div>

                        {/* Subtitle and Features */}
                        <div 
                            className="transition-all duration-300 overflow-hidden"
                            style={{
                                opacity: Math.max(0, 100 - (collapsePercentage * 1.5)) + '%',
                                maxHeight: `${Math.max(0, 100 - collapsePercentage)}vh`,
                                transform: `translateY(-${collapsePercentage * 0.2}%)`,
                            }}
                        >
                            {/* Subtitle */}
                            <div className="max-w-2xl mx-auto">
                                <p className="text-base text-gray-600 leading-relaxed">
                                    <span className="flex items-center justify-center gap-2">
                                        <HiOutlineGlobeAlt 
                                            className={`h-5 w-5 text-${RootConfigs.THEME.PRIMARY_COLOR}-600`}
                                        />
                                        {getLanguageLabel("REMOTE_JOBS_POST_JOB_SUBTITLE_2")}
                                    </span>
                                </p>
                            </div>

                            {/* Features */}
                            <div className="mt-8 flex flex-wrap justify-center gap-6">
                                <div className="flex items-center gap-2 text-gray-600">
                                    <svg className="w-5 h-5 text-green-500" fill="none" stroke="currentColor" viewBox="0 0 24 24">
                                        <path strokeLinecap="round" strokeLinejoin="round" strokeWidth={2} d="M5 13l4 4L19 7" />
                                    </svg>
                                    <span>30-Day Listing</span>
                                </div>
                                <div className="flex items-center gap-2 text-gray-600">
                                    <svg className="w-5 h-5 text-green-500" fill="none" stroke="currentColor" viewBox="0 0 24 24">
                                        <path strokeLinecap="round" strokeLinejoin="round" strokeWidth={2} d="M5 13l4 4L19 7" />
                                    </svg>
                                    <span>Email Newsletter</span>
                                </div>
                                <div className="flex items-center gap-2 text-gray-600">
                                    <svg className="w-5 h-5 text-green-500" fill="none" stroke="currentColor" viewBox="0 0 24 24">
                                        <path strokeLinecap="round" strokeLinejoin="round" strokeWidth={2} d="M5 13l4 4L19 7" />
                                    </svg>
                                    <span>Social Media Promotion</span>
                                </div>
                            </div>

                            {/* Pricing Badge */}
                            <div className="mt-4 inline-flex items-center gap-2 px-4 py-2 bg-blue-50 rounded-full">
                                <HiOutlineCurrencyDollar className="w-5 h-5 text-blue-600"/>
                                <span className="text-blue-800 font-medium">Starting at $10</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );

    const renderBasicInformationSection = () => (
        <div>
            <div className="px-4 sm:px-8 py-6">
                <h3 className="text-xl font-semibold text-gray-800 flex items-center">
                    <span className="bg-blue-50 p-2 rounded-lg mr-3">
                        <svg className="w-5 h-5 text-blue-500" fill="none" stroke="currentColor" viewBox="0 0 24 24">
                            <path strokeLinecap="round" strokeLinejoin="round" strokeWidth={2} d="M9 12h6m-6 4h6m2 5H7a2 2 0 01-2-2V5a2 2 0 012-2h5.586a1 1 0 01.707.293l5.414 5.414a1 1 0 01.293.707V19a2 2 0 01-2 2z" />
                        </svg>
                    </span>
                    Basic Information
                </h3>
                <div className="mt-4">
                    <div className="grid grid-cols-1 md:grid-cols-2 gap-6">
                        <div className="col-span-1 md:col-span-2">
                            <label htmlFor="title" className="block text-sm font-medium text-gray-700 mb-1">
                                Job Title <span className="text-red-500">*</span>
                            </label>
                            <input
                                type="text"
                                id="title"
                                name="title"
                                value={formData.title}
                                onChange={handleChange}
                                required
                                disabled={isSubmitting}
                                className="w-full px-4 py-3 rounded-lg border border-gray-300 focus:ring-2 focus:ring-blue-500 focus:border-transparent transition duration-200"
                                placeholder="e.g. Senior Frontend Developer"
                            />
                        </div>

                        <div>
                            <label htmlFor="applicationUrl" className="block text-sm font-medium text-gray-700 mb-1">
                                Application URL <span className="text-red-500">*</span>
                            </label>
                            <input
                                type="url"
                                id="applicationUrl"
                                name="applicationUrl"
                                value={formData.applicationUrl}
                                onChange={handleChange}
                                required
                                disabled={isSubmitting}
                                className="w-full px-4 py-3 rounded-lg border border-gray-300 focus:ring-2 focus:ring-blue-500 focus:border-transparent transition duration-200"
                                placeholder="https://example.com/apply"
                            />
                        </div>

                        <div>
                            <label htmlFor="publisherEmail" className="block text-sm font-medium text-gray-700 mb-1">
                                Publisher Email <span className="text-red-500">*</span>
                            </label>
                            <input
                                type="email"
                                id="publisherEmail"
                                name="publisherEmail"
                                value={formData.publisherEmail}
                                onChange={handleChange}
                                required
                                disabled={isSubmitting || !!centralUserData?.email}
                                className="w-full px-4 py-3 rounded-lg border border-gray-300 focus:ring-2 focus:ring-blue-500 focus:border-transparent transition duration-200"
                                placeholder="your@email.com"
                            />
                        </div>

                        <div>
                            <label htmlFor="company" className="block text-sm font-medium text-gray-700 mb-1">
                                Company <span className="text-red-500">*</span>
                            </label>
                            <input
                                type="text"
                                id="company"
                                name="company"
                                value={formData.company}
                                onChange={handleChange}
                                required
                                disabled={isSubmitting}
                                className="w-full px-4 py-3 rounded-lg border border-gray-300 focus:ring-2 focus:ring-blue-500 focus:border-transparent transition duration-200"
                                placeholder="e.g. Tech Innovations Inc."
                            />
                        </div>

                        <div>
                            <label htmlFor="companyLogo" className="block text-sm font-medium text-gray-700 mb-1">
                                Company Logo URL <span className="text-red-500">*</span>
                            </label>
                            <input
                                type="url"
                                id="companyLogo"
                                name="companyLogo"
                                value={formData.companyLogo}
                                onChange={handleChange}
                                required
                                disabled={isSubmitting}
                                className="w-full px-4 py-3 rounded-lg border border-gray-300 focus:ring-2 focus:ring-blue-500 focus:border-transparent transition duration-200"
                                placeholder="https://example.com/logo.png"
                            />
                        </div>

                        <div>
                            <label htmlFor="companyUrl" className="block text-sm font-medium text-gray-700 mb-1">
                                Company Website <span className="text-red-500">*</span>
                            </label>
                            <input
                                type="url"
                                id="companyUrl"
                                name="companyUrl"
                                value={formData.companyUrl}
                                onChange={handleChange}
                                required
                                disabled={isSubmitting}
                                className="w-full px-4 py-3 rounded-lg border border-gray-300 focus:ring-2 focus:ring-blue-500 focus:border-transparent transition duration-200"
                                placeholder="https://example.com"
                            />
                        </div>

                        <div>
                            <label htmlFor="location" className="block text-sm font-medium text-gray-700 mb-1">
                                Location <span className="text-red-500">*</span>
                            </label>
                            <input
                                type="text"
                                id="location"
                                name="location"
                                value={formData.location}
                                onChange={handleChange}
                                required
                                disabled={isSubmitting}
                                className="w-full px-4 py-3 rounded-lg border border-gray-300 focus:ring-2 focus:ring-blue-500 focus:border-transparent transition duration-200"
                                placeholder="e.g. New York, NY"
                            />
                        </div>

                        <div>
                            <label htmlFor="workMode" className="block text-sm font-medium text-gray-700 mb-1">
                                Work Mode <span className="text-red-500">*</span>
                            </label>
                            <select
                                id="workMode"
                                name="workMode"
                                value={formData.workMode}
                                onChange={handleChange}
                                required
                                disabled={isSubmitting}
                                className="w-full px-4 py-3 rounded-lg border border-gray-300 focus:ring-2 focus:ring-blue-500 focus:border-transparent transition duration-200"
                            >
                                <option value="REMOTE">Remote</option>
                                <option value="HYBRID">Hybrid</option>
                                <option value="ON-SITE">On-site</option>
                            </select>
                        </div>

                        <div>
                            <label htmlFor="remoteRegion" className="block text-sm font-medium text-gray-700 mb-1">
                                Remote Region <span className="text-red-500">*</span>
                            </label>
                            <select
                                id="remoteRegion"
                                name="remoteRegion"
                                value={formData.remoteRegion}
                                onChange={handleChange}
                                required
                                disabled={isSubmitting}
                                className="w-full px-4 py-3 rounded-lg border border-gray-300 focus:ring-2 focus:ring-blue-500 focus:border-transparent transition duration-200"
                            >
                                <option value="">Select a region</option>
                                {UIHelper.getJobsFilterRegions().map((region) => <option key={region} value={region}>{region}</option>)}
                            </select>
                        </div>

                        <div>
                            <label htmlFor="category" className="block text-sm font-medium text-gray-700 mb-1">
                                Category <span className="text-red-500">*</span>
                            </label>
                            <select
                                id="category"
                                name="category"
                                value={formData.category}
                                onChange={handleChange}
                                required
                                disabled={isSubmitting}
                                className="w-full px-4 py-3 rounded-lg border border-gray-300 focus:ring-2 focus:ring-blue-500 focus:border-transparent transition duration-200"
                            >
                                <option value="">Select a category</option>
                                {categories.map(category => (
                                    <option key={category} value={category}>{category}</option>
                                ))}
                            </select>
                        </div>

                        <div>
                            <label htmlFor="experienceLevel" className="block text-sm font-medium text-gray-700 mb-1">
                                Experience Level <span className="text-red-500">*</span>
                            </label>
                            <select
                                id="experienceLevel"
                                name="experienceLevel"
                                value={formData.experienceLevel}
                                onChange={handleChange}
                                required
                                disabled={isSubmitting}
                                className="w-full px-4 py-3 rounded-lg border border-gray-300 focus:ring-2 focus:ring-blue-500 focus:border-transparent transition duration-200"
                            >
                                <option value="">Select experience level</option>
                                <option value="Internship">Internship</option>
                                <option value="Entry Level">Entry Level</option>
                                <option value="Mid Level">Mid Level</option>
                                <option value="Senior Level">Senior Level</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div className="border-b border-gray-200 w-full"></div>
        </div>
    );

    const renderJobDetailsSection = () => (
        <div>
            <div className="px-4 sm:px-8 py-6">
                <h3 className="text-xl font-semibold text-gray-800 flex items-center">
                    <span className="bg-blue-50 p-2 rounded-lg mr-3">
                        <svg className="w-5 h-5 text-blue-500" fill="none" stroke="currentColor" viewBox="0 0 24 24">
                            <path strokeLinecap="round" strokeLinejoin="round" strokeWidth={2} d="M19 20H5a2 2 0 01-2-2V6a2 2 0 012-2h10a2 2 0 012 2v1m2 13a2 2 0 01-2-2V7m2 13a2 2 0 002-2V9.5a2.5 2.5 0 00-2.5-2.5H15" />
                        </svg>
                    </span>
                    Job Details
                </h3>
                <div className="mt-4">
                    <div className="space-y-6">
                        <div className="col-span-1 md:col-span-2">
                            <label htmlFor="description" className="block text-sm font-medium text-gray-700 mb-1">
                                Job Description <span className="text-red-500">*</span>
                            </label>
                            <div className="bg-white rounded-lg relative mb-8">
                                <ReactQuill
                                    value={formData.description}
                                    onChange={handleEditorChange}
                                    modules={QUILL_MODULES}
                                    className="h-64"
                                    placeholder="Describe the job role, responsibilities, and qualifications..."
                                />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div className="border-b border-gray-200 w-full relative z-0"></div>
        </div>
    );

    const renderSalarySection = () => (
        <div>
            <div className="px-4 sm:px-8 py-6">
                <h3 className="text-xl font-semibold text-gray-800 flex items-center">
                    <span className="bg-blue-50 p-2 rounded-lg mr-3">
                        <svg className="w-5 h-5 text-blue-500" fill="none" stroke="currentColor" viewBox="0 0 24 24">
                            <path strokeLinecap="round" strokeLinejoin="round" strokeWidth={2} d="M12 8c-1.657 0-3 .895-3 2s1.343 2 3 2 3 .895 3 2-1.343 2-3 2m0-8c1.11 0 2.08.402 2.599 1M12 8V7m0 1v8m0 0v1m0-1c-1.11 0-2.08-.402-2.599-1M21 12a9 9 0 11-18 0 9 9 0 0118 0z" />
                        </svg>
                    </span>
                    Salary Range
                </h3>
                <div className="mt-4">
                    <div className="grid grid-cols-1 md:grid-cols-3 gap-6">
                        <div>
                            <label htmlFor="salaryMin" className="block text-sm font-medium text-gray-700 mb-1">Minimum Salary</label>
                            <input
                                type="number"
                                id="min"
                                name="min"
                                value={formData.salaryRange?.min || ''}
                                onChange={handleSalary}
                                className="w-full px-4 py-3 rounded-lg border border-gray-300 focus:ring-2 focus:ring-blue-500 focus:border-transparent transition duration-200"
                                placeholder="e.g. 50000"
                            />
                        </div>

                        <div>
                            <label htmlFor="salaryMax" className="block text-sm font-medium text-gray-700 mb-1">Maximum Salary</label>
                            <input
                                type="number"
                                id="max"
                                name="max"
                                value={formData.salaryRange?.max || ''}
                                onChange={handleSalary}
                                className="w-full px-4 py-3 rounded-lg border border-gray-300 focus:ring-2 focus:ring-blue-500 focus:border-transparent transition duration-200"
                                placeholder="e.g. 80000"
                            />
                        </div>

                        <div>
                            <label htmlFor="salaryCurrency" className="block text-sm font-medium text-gray-700 mb-1">Salary Currency</label>
                            <select
                                id="currency"
                                name="currency"
                                value={formData.salaryRange?.currency}
                                onChange={handleSalary}
                                className="w-full px-4 py-3 rounded-lg border border-gray-300 focus:ring-2 focus:ring-blue-500 focus:border-transparent transition duration-200"
                            >
                                <option value="USD">USD</option>
                                <option value="EUR">EUR</option>
                                <option value="GBP">GBP</option>
                                <option value="AUD">AUD</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div className="border-b border-gray-200 w-full"></div>
        </div>
    );

    const renderPricingSection = () => (
        <div>
            <div className="px-4 sm:px-8 py-6">
                <h3 className="text-xl font-semibold text-gray-800 flex items-center">
                    <span className="bg-blue-50 p-2 rounded-lg mr-3">
                        <svg className="w-5 h-5 text-blue-500" fill="none" stroke="currentColor" viewBox="0 0 24 24">
                            <path strokeLinecap="round" strokeLinejoin="round" strokeWidth={2} d="M20 7l-8-4-8 4m16 0l-8 4m8-4v10l-8 4m0-10L4 7m8 4v10M4 7v10l8 4" />
                        </svg>
                    </span>
                    Choose Your Package
                </h3>
                <div className="mt-4">
                    <div className="grid grid-cols-1 md:grid-cols-2 gap-6">
                        <div 
                            className={`p-6 rounded-xl transition-all duration-300 cursor-pointer
                                ${selectedPackage === "BASIC" 
                                    ? 'bg-gradient-to-br from-blue-50 to-indigo-50 border-2 border-blue-200 transform scale-100' 
                                    : 'bg-white border-2 border-gray-100 hover:border-blue-100 transform scale-95 hover:scale-97'}`
                            }
                            onClick={() => handleChangePackage("BASIC")}
                        >
                            <div className="flex items-center justify-between mb-4">
                                <span className="flex items-center gap-1 px-2 py-1 text-sm font-semibold text-blue-600 bg-blue-100 rounded-full">
                                    <HiOutlineBadgeCheck className="w-4 h-4" /> Standard
                                </span>
                                <span className="text-2xl font-bold text-blue-600">$10</span>
                            </div>
                            <div className="space-y-4">
                                <div className="space-y-2 text-gray-600">
                                    <p className="flex items-center">
                                        <svg className="w-5 h-5 mr-2 text-blue-500" fill="none" stroke="currentColor" viewBox="0 0 24 24">
                                            <path strokeLinecap="round" strokeLinejoin="round" strokeWidth={2} d="M5 13l4 4L19 7" />
                                        </svg>
                                        Notify all our users about your job
                                    </p>
                                    <p className="flex items-center">
                                        <svg className="w-5 h-5 mr-2 text-blue-500" fill="none" stroke="currentColor" viewBox="0 0 24 24">
                                            <path strokeLinecap="round" strokeLinejoin="round" strokeWidth={2} d="M5 13l4 4L19 7" />
                                        </svg>
                                        List in our Jobs Board for 30 days
                                    </p>
                                    <p className="flex items-center">
                                        <svg className="w-5 h-5 mr-2 text-blue-500" fill="none" stroke="currentColor" viewBox="0 0 24 24">
                                            <path strokeLinecap="round" strokeLinejoin="round" strokeWidth={2} d="M5 13l4 4L19 7" />
                                        </svg>
                                        Include in our next daily digest email
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div 
                            className={`p-6 rounded-xl transition-all duration-300 cursor-pointer
                                ${selectedPackage === "FEATURED" 
                                    ? 'bg-gradient-to-br from-green-50 to-emerald-50 border-2 border-green-200 transform scale-100' 
                                    : 'bg-white border-2 border-gray-100 hover:border-green-100 transform scale-95 hover:scale-97'}`
                            }
                            onClick={() => handleChangePackage("FEATURED")}
                        >
                            <div className="flex items-center justify-between mb-4">
                                <span className="flex items-center gap-1 px-2 py-1 text-sm font-semibold text-green-600 bg-green-100 rounded-full">
                                    <HiOutlineStar className="w-4 h-4" /> Featured
                                </span>
                                <span className="text-2xl font-bold text-green-600">$100</span>
                            </div>
                            <div className="space-y-4">
                                <div className="space-y-2 text-gray-600">
                                    <p className="flex items-center">
                                        <svg className="w-5 h-5 mr-2 text-green-500" fill="none" stroke="currentColor" viewBox="0 0 24 24">
                                            <path strokeLinecap="round" strokeLinejoin="round" strokeWidth={2} d="M5 13l4 4L19 7" />
                                        </svg>
                                        Fixed at the top of our jobs list
                                    </p>
                                    <p className="flex items-center">
                                        <svg className="w-5 h-5 mr-2 text-green-500" fill="none" stroke="currentColor" viewBox="0 0 24 24">
                                            <path strokeLinecap="round" strokeLinejoin="round" strokeWidth={2} d="M5 13l4 4L19 7" />
                                        </svg>
                                        Featured in daily digest email
                                    </p>
                                    <p className="flex items-center">
                                        <svg className="w-5 h-5 mr-2 text-green-500" fill="none" stroke="currentColor" viewBox="0 0 24 24">
                                            <path strokeLinecap="round" strokeLinejoin="round" strokeWidth={2} d="M5 13l4 4L19 7" />
                                        </svg>
                                        Maximum visibility for 30 days
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );

    const renderActionButtons = () => (
        <div className="px-4 sm:px-8 py-6">
            <div className="flex flex-col sm:flex-row justify-center space-y-3 sm:space-y-0 sm:space-x-4">
                <button
                    type="submit"
                    disabled={isSubmitting}
                    className="group w-full sm:w-auto px-8 py-3 bg-blue-600 text-white rounded-lg hover:bg-blue-700 transition duration-200 flex items-center justify-center space-x-2"
                >
                    {isSubmitting ? (
                        <>
                            <svg className="animate-spin h-5 w-5 mr-3" viewBox="0 0 24 24">
                                <circle className="opacity-25" cx="12" cy="12" r="10" stroke="currentColor" strokeWidth="4"/>
                                <path className="opacity-75" fill="currentColor" d="M4 12a8 8 0 018-8V0C5.373 0 0 5.373 0 12h4zm2 5.291A7.962 7.962 0 014 12H0c0 3.042 1.135 5.824 3 7.938l3-2.647z"/>
                            </svg>
                            <span>Processing...</span>
                        </>
                    ) : (
                        <>
                            <span>Submit Job Post</span>
                            <svg 
                                className="w-5 h-5 transition-transform duration-300 animate-[arrow-move_2s_ease-in-out_infinite]" 
                                fill="none" 
                                stroke="currentColor" 
                                viewBox="0 0 24 24"
                            >
                                <path strokeLinecap="round" strokeLinejoin="round" strokeWidth={2} d="M14 5l7 7m0 0l-7 7m7-7H3"/>
                            </svg>
                        </>
                    )}
                </button>
                <button
                    type="button"
                    onClick={handleClearForm}
                    disabled={isSubmitting}
                    className="w-full sm:w-auto px-8 py-3 border border-gray-300 text-gray-700 rounded-lg hover:bg-gray-50 transition duration-200"
                >
                    Clear Form
                </button>
            </div>
        </div>
    );

    // Main render
    return (
        <div className="w-full flex flex-col items-center">
            <ToastContainer/>
            {isSubmitting ? (
                <PaymentLoadingComponent/>
            ) : (
                <div className="min-h-screen w-full">
                    {renderHeader()}
                    <div className="py-4 px-2 sm:px-6 lg:px-8">
                        <form onSubmit={handleSubmit} className="max-w-4xl mx-auto bg-white rounded-xl shadow-lg overflow-hidden">
                            <div className="space-y-8">
                                {renderBasicInformationSection()}
                                {renderJobDetailsSection()}
                                {renderSalarySection()}
                                {renderPricingSection()}
                                {renderActionButtons()}
                            </div>
                        </form>
                    </div>
                </div>
            )}
            <style jsx global>{`
                @keyframes arrow-move {
                    0%, 100% { transform: translateX(0); }
                    15% { transform: translateX(4px); }
                    30% { transform: translateX(0); }
                }
                @keyframes pulse-glow {
                    0% { transform: scale(1); opacity: 0.8; }
                    50% { transform: scale(1.02); opacity: 1; }
                    100% { transform: scale(1); opacity: 0.8; }
                }
                .featured-pulse {
                    animation: pulse-glow 2s ease-in-out infinite;
                    box-shadow: 0 0 20px rgba(72, 187, 120, 0.2);
                }
                
                /* Smooth scrolling for the whole page */
                html {
                    scroll-behavior: smooth;
                }
                
                /* Smooth transitions */
                .transition-all {
                    transition-property: all;
                    transition-timing-function: cubic-bezier(0.4, 0, 0.2, 1);
                    transition-duration: 300ms;
                }
            `}</style>
        </div>
    );
};

export default JobSubmissionForm;
