import React from 'react';
import {HiOutlineBriefcase, HiOutlineGlobeAlt, HiOutlineArrowRight} from 'react-icons/hi';
import RootConfigs from "../../../../../configs";
import {getLanguageLabel} from "../../../logic/language/language.helper";
import Link from 'next/link';
import { SharedRoutesUtils } from '../../../utils/shared-routes.utils';

interface JobRegionCategoryHeaderProps {
    region: string;
    category: string;
}

/**
 * JobRegionCategoryHeader Component
 * Displays a header section for jobs filtered by both region and category
 * 
 * @component
 * @param {JobRegionCategoryHeaderProps} props - Component props
 * @returns {JSX.Element} Rendered component
 */
const JobRegionCategoryHeader: React.FC<JobRegionCategoryHeaderProps> = ({region, category}) => {
    /**
     * Renders the title section with region and category badges
     */
    const renderTitle = () => (
        <div className="flex flex-col items-center gap-2">
            <h1 className="text-3xl sm:text-4xl font-bold text-gray-900 tracking-tight">
                <span className="flex flex-col items-center gap-1">
                    <span>{getLanguageLabel("REMOTE_JOBS_LATEST_JOBS_IN_REGION_AND_CATEGORY", "Latest Remote Jobs in")}</span>
                    <div className="flex items-center gap-2">
                        <span className={`text-${RootConfigs.THEME.PRIMARY_COLOR}-800`}>
                            {region}
                        </span>
                        <span className="text-gray-500">&middot;</span>
                        <span className={`text-${RootConfigs.THEME.PRIMARY_COLOR}-800`}>
                            {category}
                        </span>
                    </div>
                </span>
            </h1>
            
            {/* Region and Category badges */}
            <div className="flex items-center gap-4">
                <div className="flex items-center gap-2">
                    <HiOutlineGlobeAlt 
                        className={`h-5 w-5 text-${RootConfigs.THEME.PRIMARY_COLOR}-600`}
                    />
                    <span className={`inline-flex items-center px-3 py-1 
                        bg-${RootConfigs.THEME.PRIMARY_COLOR}-50 text-${RootConfigs.THEME.PRIMARY_COLOR}-700 
                        text-base font-medium rounded-full`}>
                        {region}
                    </span>
                </div>
                <div className="flex items-center gap-2">
                    <HiOutlineBriefcase 
                        className={`h-5 w-5 text-${RootConfigs.THEME.PRIMARY_COLOR}-600`}
                    />
                    <span className={`inline-flex items-center px-3 py-1 
                        bg-${RootConfigs.THEME.PRIMARY_COLOR}-50 text-${RootConfigs.THEME.PRIMARY_COLOR}-700 
                        text-base font-medium rounded-full`}>
                        {category}
                    </span>
                </div>
            </div>
        </div>
    );

    /**
     * Renders the subtitle section
     */
    const renderSubtitle = () => (
        <div className="max-w-2xl mx-auto">
            <p className="text-base text-gray-600 leading-relaxed">
                {getLanguageLabel("REMOTE_JOBS_REGION_CATEGORY_SUBTITLE", 
                    `Discover the best ${category} remote opportunities in ${region} from top companies`)}
            </p>
        </div>
    );

    /**
     * Renders the CTA section for posting a job
     */
    const renderPostJobCTA = () => (
        <div className="mt-8">
            <Link
                href={SharedRoutesUtils.getSubmitJob()}
                className={`
                    group
                    inline-flex items-center gap-2 px-6 py-3
                    bg-${RootConfigs.THEME.PRIMARY_COLOR}-600 
                    hover:bg-${RootConfigs.THEME.PRIMARY_COLOR}-700
                    text-white font-medium rounded-full
                    transform transition-all duration-200
                    hover:scale-105 hover:shadow-lg
                    focus:outline-none focus:ring-2 
                    focus:ring-${RootConfigs.THEME.PRIMARY_COLOR}-500 focus:ring-offset-2
                `}
            >
                <span>Post a Job</span>
                <HiOutlineArrowRight className="w-5 h-5 transition-transform duration-300 
                    animate-[arrow-move_2s_ease-in-out_infinite]" />
            </Link>
            <p className="mt-3 text-sm text-gray-500">
                {getLanguageLabel("REMOTE_JOBS_POST_JOB_SUBTITLE", "Reach thousands of remote talent worldwide")}
            </p>

            <style jsx global>{`
                @keyframes arrow-move {
                    0%, 100% { transform: translateX(0); }
                    15% { transform: translateX(4px); }
                    30% { transform: translateX(0); }
                }
            `}</style>
        </div>
    );

    return (
        <div className="relative w-full">
            <div className="relative py-8 sm:py-12 px-4 sm:px-6 lg:px-8">
                <div className="max-w-7xl mx-auto">
                    <div className="text-center space-y-4">
                        {renderTitle()}
                        {renderSubtitle()}
                        {renderPostJobCTA()}
                    </div>
                </div>
            </div>
        </div>
    );
};

export default JobRegionCategoryHeader;