import JobFilter from "./job-filter.component";
import { JobFiltersSelected, JobListModel, JobsDiscoverDataUtils } from "../jobs-discover-data.model";
import { useEffect, useState } from "react";
import JobListItemComponent from "./job-list-item.component";
import EmptyListMessageComponent from "../../../ui-components/shared/EmptyListMessage.component";
import JobCategoryHeader from "./job-category-header.component";
import GoogleAdsInFeedComponent from "../../../ui-components/ads/GoogleAdsInFeed.component";
import { UIHelper } from "../../../../ui-helper/UIHelper";
import SpinnerComponent from "../../../ui-components/shared/Spinner.component";
import { JobSearch } from "./job-search.component";
import RootConfigs from "../../../../../configs";

export default function JobListFilteredResultsComponent(props: {
    preloadedJobs: JobListModel[],
    selectedFilters: JobFiltersSelected,
    filterListSkills?: string[],
    searchTokens?: string[]
}) {
    const [listJobs, setListJobs] = useState<JobListModel[]>([]);
    const [filters, setFilters] = useState<JobFiltersSelected>(props.selectedFilters);
    const [isLoading, setIsLoading] = useState(false);
    const [searchTokens, setSearchTokens] = useState<string[]>(props.searchTokens || []);
    // Initialize with preloaded jobs
    useEffect(() => {
        setListJobs(props.preloadedJobs);
    }, [props.preloadedJobs]);

    // Handle filter changes
    useEffect(() => {
        const fetchJobs = async () => {
            setIsLoading(true);
            try {
                if ((filters.category && filters.category.length > 0) ||
                    (filters.remoteRegion && filters.remoteRegion.length > 0) ||
                    filters.skillSelected || filters.workMode || searchTokens.length > 0) {
                    // First fetch Featured Jobs
                    let listFeaturedJobs: JobListModel[] = await JobsDiscoverDataUtils.fetchFilteredJobs(filters, searchTokens, true);
                    // Then fetch the rest of the jobs
                    let listJobs: JobListModel[] = await JobsDiscoverDataUtils.fetchFilteredJobs(filters, searchTokens, false);
                    // Merge the two lists
                    listJobs = [...listFeaturedJobs, ...listJobs];
                    setListJobs(listJobs);
                } else {
                    setListJobs(props.preloadedJobs);
                }
            } catch (error) {
                console.error('Error fetching jobs:', error);
                setListJobs([]);
            } finally {
                setIsLoading(false);
            }
        };

        fetchJobs();
    }, [filters, props.preloadedJobs, searchTokens, props.searchTokens]);


    function onFilterChange(newFilters: JobFiltersSelected) {
        // Save Filters locally
        JobsDiscoverDataUtils.saveFilters(newFilters);

        // Update filters state
        setFilters(newFilters);


    }

    function renderListJobs() {
        if (isLoading) {
            return <div className="w-full text-center py-4"><SpinnerComponent /></div>;
        }

        if (!listJobs || listJobs.length === 0) {
            return <EmptyListMessageComponent message="Didn't find any jobs with the selected filters" />;
        }

        return listJobs.map((listItem) => (
            <JobListItemComponent
                key={`job-${listItem.uid}`}
                jobData={listItem}
                isPopular={listItem.stats?.applyCounter > 10}
            />
        ));
    }

    function onSearch(searchTokens: string[]) {
        console.log("onSearch", searchTokens);
        setSearchTokens(searchTokens);
    }

    
    return (
        <div className="flex flex-col w-full">
            <JobCategoryHeader category={filters.category} />
            <div className="flex flex-col w-full max-w-5xl mx-auto">
                {RootConfigs.META_CONFIGS.enableJobSearch && <JobSearch onSearch={onSearch} initialSearchTokens={searchTokens} />} 
                <JobFilter
                    initialCategory={filters.category}
                    initialRemoteRegion={filters.remoteRegion}
                    initialSkill={filters.skillSelected}
                    initialWorkMode={filters.workMode}
                    filterListSkills={props.filterListSkills}
                    categories={UIHelper.getJobsCategories()}
                    onFilterChange={onFilterChange}
                />
                <GoogleAdsInFeedComponent />
                {renderListJobs()}
            </div>

        </div>
    );
}