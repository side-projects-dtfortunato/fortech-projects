import { DailyPublishedJobs, JobFiltersSelected, JobListModel, JobsDiscoverDataUtils } from "../jobs-discover-data.model";
import { useEffect, useState } from "react";
import { FirebaseClientFirestoreUtils } from "../../../logic/shared-data/firebase.utils";
import { SharedFirestoreCollectionDB } from "../../../logic/shared-data/datamodel/shared-firestore-collections";
import CommonUtils from "../../../logic/commonutils";
import InlineLabelSeparatorComponent from "../../../ui-components/shared/inline-label-separator.component";
import JobListItemComponent from "./job-list-item.component";
import InfiniteScroll from "react-infinite-scroll-component";
import SpinnerComponent from "../../../ui-components/shared/Spinner.component";
import JobListFilteredResultsComponent from "./job-list-filtered-results.component";
import JobFilter from "./job-filter.component";
import JobCategoryHeader from "./job-category-header.component";
import GoogleAdsInFeedComponent from "../../../ui-components/ads/GoogleAdsInFeed.component";
import { UIHelper } from "../../../../ui-helper/UIHelper";
import LocalStorageDataManager from "../../../logic/managers/LocalStorageData.manager";
import { SharedWebToAppBridgeUtils } from "../../../utils/shared-web-to-app-bridge.utils";
import { JobSearch } from "./job-search.component";
import RootConfigs from "../../../../../configs";


export default function JobListPageComponent(props: { preloadedData: DailyPublishedJobs[], itemsPerPage: number, filterListSkills?: string[], popularJobs?: JobListModel[] }) {
    const [listPublishedDays, setListPublishedDays] = useState<DailyPublishedJobs[]>(props.preloadedData);
    const [hasMorePages, setHasMorePages] = useState(props.preloadedData.length >= props.itemsPerPage);
    const [filters, setFilters] = useState<JobFiltersSelected | undefined>();
    const [searchTokens, setSearchTokens] = useState<string[]>([]);

    useEffect(() => {
        if (!filters) {
            setFilters(JobsDiscoverDataUtils.getSavedFilters());
        }

    }, []);

    useEffect(() => {
        // Save the filter selection
        SharedWebToAppBridgeUtils.sendUserTags({
            category: filters?.category,
            remoteRegion: filters?.remoteRegion
        });
    }, [filters])


    async function loadNextPage() {
        // BackendAPI
        let lastDocId: number | undefined = listPublishedDays.length > 0 ? listPublishedDays[listPublishedDays.length - 1].publishedDayId : undefined;
        const response: any[] = await FirebaseClientFirestoreUtils
            .getDocumentsWithPagination(SharedFirestoreCollectionDB.JobsDiscoverProject.DailyPublishedJobs, props.itemsPerPage, "publishedDayId", lastDocId);

        if (response && response.length > 0) {
            setListPublishedDays(listPublishedDays.concat(response));
            setHasMorePages(response.length === props.itemsPerPage); // If the response has the same number of items per page, means that has possible more pages
        } else {
            setHasMorePages(false);
        }
    }

    function renderDailyContainers() {
        return listPublishedDays.filter((listItem) => listItem.listJobs)
            .map((listItem, index) => {
                return (
                    <div key={index} className='flex flex-col gap-5'>
                        <JobListContainer key={listItem.publishedDayId} dailyPublishedJobs={listItem} />
                    </div>
                )
            })
    }


    function onFilterChanged(filters: JobFiltersSelected) {
        // Save Filters locally
        JobsDiscoverDataUtils.saveFilters(filters);
        setFilters(filters);
    }

    function renderPopularJobs() {
        if (!props.popularJobs || props.popularJobs.length === 0) {
            return null;
        }


        return (
            <div className="flex flex-col gap-8 px-5 pb-10 py-5 sm:px-0">
                {props.popularJobs
                    .map((jobItem) => {
                        return <JobListItemComponent key={jobItem.uid!} jobData={jobItem} isPopular={jobItem.isPopular} />
                    })}
            </div>
        )
    }

    function onSearch(searchTokens: string[]) {
        console.log("onSearch", searchTokens);
        setSearchTokens(searchTokens);
    }

    function renderListJobs() {
        if (filters && (filters.category !== "" || filters.remoteRegion !== "" || filters.skillSelected || filters.workMode || searchTokens.length > 0)) {
            let preloadedJobs: JobListModel[] = []
            props.preloadedData
                .filter((dailyJobs) => dailyJobs.listJobs)
                .forEach((dailyJobs) => preloadedJobs = preloadedJobs.concat(Object.values(dailyJobs.listJobs!)));
            return (
                <JobListFilteredResultsComponent preloadedJobs={preloadedJobs} selectedFilters={filters} filterListSkills={props.filterListSkills} searchTokens={searchTokens} />
            )
        } else {
            return (
                <>
                    <JobCategoryHeader />
                    <div className="flex flex-col w-full max-w-5xl mx-auto">
                        {RootConfigs.META_CONFIGS.enableJobSearch && 
                          <JobSearch 
                            onSearch={onSearch} 
                            initialSearchTokens={searchTokens}
                          />
                        }
                        <JobFilter categories={UIHelper.getJobsCategories()} onFilterChange={onFilterChanged} filterListSkills={props.filterListSkills} />

                        <InfiniteScroll next={loadNextPage} hasMore={hasMorePages} loader={<SpinnerComponent />}
                            dataLength={listPublishedDays.length}>
                            {renderPopularJobs()}
                            {renderDailyContainers()}
                        </InfiniteScroll>
                    </div>
                </>
            )
        }
    }

    return (
        <div className='flex flex-col mt-0 pt-2 w-full'>
            {renderListJobs()}
        </div>
    )
}

function JobListContainer(props: { dailyPublishedJobs: DailyPublishedJobs }) {
    let dateStr: string = "";
    let publishedDate: Date = new Date(CommonUtils.convertDaytimestampToMillis(props.dailyPublishedJobs.publishedDayId));
    let titleTextSize: "medium" | "large" = "medium";
    // Is Today
    if (props.dailyPublishedJobs.publishedDayId === CommonUtils.getTodayDay()) {
        dateStr = "Today";
        titleTextSize = "large";
    } else if (props.dailyPublishedJobs.publishedDayId === (CommonUtils.getTodayDay() - 1)) {
        dateStr = "Yesterday";
    } else {
        dateStr = CommonUtils.getTimeAgo(publishedDate.getTime());
    }

    function renderTitle() {
        return (
            <InlineLabelSeparatorComponent title={`${dateStr} Jobs`} titleSize={titleTextSize} />
        )
    }

    function sortListJobs(job1: JobListModel, job2: JobListModel) {
        return job2.createdAt! - job1.createdAt!;
    }


    return (
        <div className='flex flex-col gap-8 px-5 pb-10 sm:py-5 sm:px-0'>
            {renderTitle()}
            <GoogleAdsInFeedComponent />
            {Object.values(props.dailyPublishedJobs.listJobs)
                .sort(sortListJobs)
                .map((listItem, index) => {
                    return (
                        <div key={index} className='flex flex-col gap-2'>
                            {index === 3 || index === 7 ? <GoogleAdsInFeedComponent /> : <></>}
                            <JobListItemComponent key={listItem.uid!} jobData={listItem} isPopular={listItem.stats?.applyCounter > 10} />
                        </div>
                    )
                })}
        </div>
    )
}