import React from 'react';
import { HiOutlineClock, HiOutlineGlobeAlt, HiOutlineLocationMarker, HiOutlineOfficeBuilding, HiOutlineChip, HiOutlineFire, HiOutlineStar } from 'react-icons/hi';
import { JobListModel, JobWorkModeType, JobRemoteRegion } from "../jobs-discover-data.model";
import Link from "next/link";
import { SharedRoutesUtils } from "../../../utils/shared-routes.utils";
import RootConfigs from "../../../../../configs";

// Styles for animations
const animationStyles = `
    /* Shine effect animation for popular jobs */
    .shine-container {
        position: relative;
        overflow: hidden;
    }
    
    .shine-effect {
        position: absolute;
        top: 0;
        left: -100%;
        width: 50%;
        height: 100%;
        background: linear-gradient(
            90deg,
            transparent,
            rgba(255, 255, 255, 0.3),
            transparent
        );
        animation: shine 4s cubic-bezier(0.4, 0, 0.2, 1) infinite;
        animation-delay: ${Math.random() * 1.5}s;
    }
    
    @keyframes shine {
        0% { left: -100%; }
        20% { left: 100%; }
        100% { left: 100%; }
    }
`;

export default function JobListItemComponent({ jobData, isPopular = false }: { jobData: JobListModel, isPopular?: boolean}) {
    // Add featured status check
    const isFeatured = jobData.publishPlanType === 'FEATURED';
    
    // Helper function to format the posting date
    const formatDate = (dateTime: number) => {
        const date = new Date(dateTime);
        const now = new Date();
        const diffTime = Math.abs(now.getTime() - date.getTime());
        const diffDays = Math.ceil(diffTime / (1000 * 60 * 60 * 24));

        if (diffDays <= 1) return "Just now";
        if (diffDays <= 7) return `${diffDays} days ago`;
        return date.toLocaleDateString('en-US', { month: 'short', day: 'numeric', year: 'numeric' });
    };

    // Helper function to format work mode with remote region if applicable
    const formatWorkMode = (workMode?: JobWorkModeType, remoteRegion?: JobRemoteRegion) => {
        if (workMode === 'REMOTE' && remoteRegion) {
            return `Remote (${remoteRegion})`;
        }
        return workMode || 'Not specified';
    };

    // Format the date once for reuse
    const formattedDate = formatDate(jobData.createdAt!);

    // Update container classes to include featured styling
    const containerClasses = `block p-4 rounded-lg drop-shadow-xl mb-4 hover:bg-opacity-70 sm:mx-4 ${
        isPopular 
            ? `bg-${RootConfigs.THEME.PRIMARY_COLOR}-50 border border-blue-100 hover:bg-${RootConfigs.THEME.PRIMARY_COLOR}-100/70 shine-container`
            : isFeatured 
                ? 'bg-green-50 border border-green-100 hover:bg-green-100/70 shine-container'
                : 'bg-white hover:bg-gray-50'
    }`;

    // Component to render popular job animations
    const ShineEffect = () => (
        <div className="shine-effect" />
    );

    return (
        <Link href={SharedRoutesUtils.getJobDetailsPageUrl(jobData.uid!)} className={containerClasses}>
            {/* Render animation effects only for popular jobs */}
            {(isPopular || isFeatured) && <ShineEffect />}
            
            {/* Mobile Layout */}
            <div className="sm:hidden">
                {/* Header section with logo and title aligned */}
                <div className="flex items-center gap-4 mb-3">
                    <div className="w-11 h-11 flex-shrink-0">
                        <img src={jobData.companyLogo} alt={`${jobData.company} logo`} className="w-full h-full rounded object-contain" />
                    </div>
                    <div className="flex-grow">
                        <div className="flex items-center gap-2 flex-wrap">
                            <h3 className="text-lg font-semibold text-gray-800">{jobData.title}</h3>
                            {isPopular && (
                                <span className={`flex items-center gap-1 px-2 py-1 text-xs font-semibold text-${RootConfigs.THEME.PRIMARY_COLOR}-600 bg-${RootConfigs.THEME.PRIMARY_COLOR}-100 rounded-full`}>
                                    <HiOutlineFire className="w-3 h-3" /> Popular
                                </span>
                            )}
                            {isFeatured && (
                                <span className="flex items-center gap-1 px-2 py-1 text-xs font-semibold text-green-600 bg-green-100 rounded-full">
                                    <HiOutlineStar className="w-3 h-3" /> Featured
                                </span>
                            )}
                        </div>
                        <span className="text-slate-400">{jobData.company}</span>
                    </div>
                </div>

                {/* Job details grid */}
                <div className="w-full grid grid-cols-2 gap-2 text-sm text-gray-600">
                    <div className="flex items-center">
                        <HiOutlineClock className="mr-1" />
                        <span>{formattedDate}</span>
                    </div>
                    <div className="flex items-center">
                        <HiOutlineOfficeBuilding className="mr-1" />
                        <span>{jobData.category || 'Not specified'}</span>
                    </div>
                    <div className="flex items-center">
                        <HiOutlineGlobeAlt className="mr-1" />
                        <span>{formatWorkMode(jobData.workMode, jobData.remoteRegion)}</span>
                    </div>
                    <div className="flex items-center">
                        <HiOutlineLocationMarker className="mr-1" />
                        <span>{jobData.location}</span>
                    </div>
                </div>

                {/* Skills section */}
                {jobData.skills && jobData.skills.length > 0 && (
                    <div className="mt-2 flex flex-wrap w-full">
                        <HiOutlineChip className="mr-1 mt-1" />
                        {jobData.skills.slice(0, 3).map((skill, index) => (
                            <span key={index} className="bg-gray-200 rounded-full px-1.5 py-0.5 text-xs text-gray-700 mr-1 mb-1">
                                {skill}
                            </span>
                        ))}
                        {jobData.skills.length > 3 && (
                            <span className="text-xs font-semibold text-gray-700">+{jobData.skills.length - 3} more</span>
                        )}
                    </div>
                )}
            </div>

            {/* Desktop Layout */}
            <div className="hidden sm:block">
                {/* Header section with logo and title aligned */}
                <div className="flex items-center gap-4 mb-4">
                    <div className="w-11 h-11 flex-shrink-0">
                        <img src={jobData.companyLogo} alt={`${jobData.company} logo`} className="w-full h-full rounded object-contain" />
                    </div>
                    <div className="flex-grow">
                        <div className="flex items-center gap-2 flex-wrap">
                            <h3 className="text-lg font-semibold text-gray-800">{jobData.title}</h3>
                            {isPopular && (
                                <span className={`flex items-center gap-1 px-2 py-1 text-xs font-semibold text-${RootConfigs.THEME.PRIMARY_COLOR}-600 bg-blue-100 rounded-full`}>
                                    <HiOutlineFire className="w-3 h-3" /> Popular
                                </span>
                            )}
                            {isFeatured && (
                                <span className="flex items-center gap-1 px-2 py-1 text-xs font-semibold text-green-600 bg-green-100 rounded-full">
                                    <HiOutlineStar className="w-3 h-3" /> Featured
                                </span>
                            )}
                        </div>
                        <span className="text-slate-400">{jobData.company}</span>
                    </div>
                    {/* "Just now" badge aligned to the right */}
                    {formattedDate === "Just now" && (
                        <div className="flex-shrink-0">
                            <span className="px-2 py-1 text-xs font-semibold text-gray-600 bg-gray-200 rounded-full">
                                Just now
                            </span>
                        </div>
                    )}
                </div>

                {/* Job details section - full width */}
                <div className="w-full">
                    <div className="flex flex-wrap items-center text-sm text-gray-600 mb-2">
                        <div className="flex items-center mr-4">
                            <HiOutlineClock className="mr-1" />
                            <span>{formattedDate}</span>
                        </div>
                        <div className="flex items-center mr-4">
                            <HiOutlineOfficeBuilding className="mr-1" />
                            <span>{jobData.category || 'Not specified'}</span>
                        </div>
                        <div className="flex items-center mr-4">
                            <HiOutlineGlobeAlt className="mr-1" />
                            <span>{formatWorkMode(jobData.workMode, jobData.remoteRegion)}</span>
                        </div>
                        <div className="flex items-center">
                            <HiOutlineLocationMarker className="mr-1" />
                            <span>{jobData.location}</span>
                        </div>
                    </div>

                    {/* Skills section - full width */}
                    {jobData.skills && jobData.skills.length > 0 && (
                        <div className="flex flex-wrap items-center w-full">
                            <HiOutlineChip className="mr-1" />
                            {jobData.skills.slice(0, 5).map((skill, index) => (
                                <span key={index} className="bg-gray-200 rounded-full px-1.5 py-0.5 text-xs text-gray-700 mr-1 mb-1">
                                    {skill}
                                </span>
                            ))}
                            {jobData.skills.length > 5 && (
                                <span className="text-xs font-semibold text-gray-700">+{jobData.skills.length - 5} more</span>
                            )}
                        </div>
                    )}
                </div>
            </div>

            {/* Animation styles */}
            <style jsx>{animationStyles}</style>
        </Link>
    );
}