import Link from "next/link";
import { SharedRoutesUtils } from "../../../utils/shared-routes.utils";

export default function JobSubmissionResultPage(props: { isSuccess: boolean, jobId: string }) {
    if (props.isSuccess) {
        return (
            <div className="flex items-center justify-center p-4 py-10">
                <div className="w-full max-w-lg bg-white rounded-2xl shadow-xl border border-emerald-100 overflow-hidden">
                    {/* Status Header */}
                    <div className="bg-gradient-to-br from-emerald-500 to-emerald-600 p-10 text-center relative overflow-hidden">
                        <div className="absolute inset-0 bg-[url('/patterns/success-pattern.svg')] opacity-10"></div>
                        <div className="relative">
                            <div className="mb-6 transform transition-transform hover:scale-105">
                                <div className="w-24 h-24 bg-white/20 backdrop-blur rounded-2xl mx-auto flex items-center justify-center">
                                    {/* Job Post Success Icon */}
                                    <svg className="w-14 h-14 text-white" fill="none" stroke="currentColor" viewBox="0 0 24 24">
                                        <path strokeLinecap="round" strokeLinejoin="round" strokeWidth="1.5" d="M9 12h6m-6 4h6m2 5H7a2 2 0 01-2-2V5a2 2 0 012-2h5.586a1 1 0 01.707.293l5.414 5.414a1 1 0 01.293.707V19a2 2 0 01-2 2z" />
                                        <path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M21 8l-5-5" />
                                        <circle cx="12" cy="8" r="2" strokeWidth="1.5" />
                                    </svg>
                                </div>
                            </div>
                            <h1 className="text-3xl font-bold text-white mb-2">
                                Job Post Published!
                            </h1>
                            <p className="text-emerald-50 text-lg">
                                Your job listing is now being reviewed by our team
                            </p>
                        </div>
                    </div>

                    {/* Status Info */}
                    <div className="px-8 py-6">
                        <div className="flex items-center p-4 bg-emerald-50 rounded-xl mb-6">
                            <div className="flex-shrink-0 w-12 h-12 bg-emerald-100 rounded-xl flex items-center justify-center mr-4">
                                {/* Review Process Icon */}
                                <svg className="w-6 h-6 text-emerald-600" fill="none" stroke="currentColor" viewBox="0 0 24 24">
                                    <path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M15 15l-2 5L9 9l11 4-5 2zm0 0l5 5M7.188 2.239l.777 2.897M5.136 7.965l-2.898-.777M13.95 4.05l-2.122 2.122m-5.657 5.656l-2.12 2.122" />
                                </svg>
                            </div>
                            <div>
                                <h3 className="font-medium text-emerald-900">Review in Progress</h3>
                                <p className="text-emerald-600 text-sm">Your job post will be live after quick review</p>
                            </div>
                        </div>
                    </div>

                    {/* Actions */}
                    <div className="p-6 space-y-4">
                        <Link 
                            href={SharedRoutesUtils.getJobDetailsPageUrl(props.jobId)}
                            className="group block w-full py-3 px-4 text-center bg-gradient-to-r from-emerald-500 to-emerald-600 text-white rounded-xl hover:from-emerald-600 hover:to-emerald-700 transition-all duration-200 shadow-lg shadow-emerald-200"
                        >
                            <span className="flex items-center justify-center">
                                View Job Post
                                <svg className="w-5 h-5 ml-2 transform group-hover:translate-x-1 transition-transform" fill="none" stroke="currentColor" viewBox="0 0 24 24">
                                    <path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M9 5l7 7-7 7" />
                                </svg>
                            </span>
                        </Link>
                        <Link 
                            href="/"
                            className="block w-full py-3 px-4 text-center bg-white text-gray-700 rounded-xl hover:bg-gray-50 transition-colors border border-gray-200"
                        >
                            Return to Dashboard
                        </Link>
                    </div>
                </div>
            </div>
        );
    } else {
        return (
            <div className="flex items-center justify-center p-4 py-10">
                <div className="w-full max-w-lg bg-white rounded-2xl shadow-xl border border-amber-100 overflow-hidden">
                    {/* Status Header */}
                    <div className="bg-gradient-to-br from-amber-500 to-amber-600 p-10 text-center relative overflow-hidden">
                        <div className="absolute inset-0 bg-[url('/patterns/warning-pattern.svg')] opacity-10"></div>
                        <div className="relative">
                            <div className="mb-6 transform transition-transform hover:scale-105">
                                <div className="w-24 h-24 bg-white/20 backdrop-blur rounded-2xl mx-auto flex items-center justify-center">
                                    {/* Cancelled Job Post Icon */}
                                    <svg className="w-14 h-14 text-white" fill="none" stroke="currentColor" viewBox="0 0 24 24">
                                        <path strokeLinecap="round" strokeLinejoin="round" strokeWidth="1.5" d="M9 12h6m-6 4h6m2 5H7a2 2 0 01-2-2V5a2 2 0 012-2h5.586a1 1 0 01.707.293l5.414 5.414a1 1 0 01.293.707V19a2 2 0 01-2 2z" />
                                        <path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M18 6L6 18M6 6l12 12" />
                                    </svg>
                                </div>
                            </div>
                            <h1 className="text-3xl font-bold text-white mb-2">
                                Post Not Submitted
                            </h1>
                            <p className="text-amber-50 text-lg">
                                The job posting process was not completed
                            </p>
                        </div>
                    </div>

                    {/* Help Section */}
                    <div className="p-8">
                        <div className="space-y-4">
                            <div className="flex items-center p-4 bg-amber-50 rounded-xl">
                                <div className="flex-shrink-0 w-10 h-10 bg-amber-100 rounded-xl flex items-center justify-center mr-4">
                                    {/* Edit Document Icon */}
                                    <svg className="w-5 h-5 text-amber-600" fill="none" stroke="currentColor" viewBox="0 0 24 24">
                                        <path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M11 5H6a2 2 0 00-2 2v11a2 2 0 002 2h11a2 2 0 002-2v-5m-1.414-9.414a2 2 0 112.828 2.828L11.828 15H9v-2.828l8.586-8.586z" />
                                    </svg>
                                </div>
                                <div>
                                    <p className="text-amber-900">Review and edit your job post details</p>
                                </div>
                            </div>
                            <div className="flex items-center p-4 bg-amber-50 rounded-xl">
                                <div className="flex-shrink-0 w-10 h-10 bg-amber-100 rounded-xl flex items-center justify-center mr-4">
                                    {/* Support Icon */}
                                    <svg className="w-5 h-5 text-amber-600" fill="none" stroke="currentColor" viewBox="0 0 24 24">
                                        <path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M18.364 5.636l-3.536 3.536m0 5.656l3.536 3.536M9.172 9.172L5.636 5.636m3.536 9.192l-3.536 3.536M21 12a9 9 0 11-18 0 9 9 0 0118 0z" />
                                    </svg>
                                </div>
                                <div>
                                    <p className="text-amber-900">Contact our support team</p>
                                </div>
                            </div>
                        </div>
                    </div>

                    {/* Actions */}
                    <div className="p-6 space-y-4 border-t border-gray-100">
                        <Link 
                            href={SharedRoutesUtils.getSubmitJob()}
                            className="group block w-full py-3 px-4 text-center bg-gradient-to-r from-blue-500 to-blue-600 text-white rounded-xl hover:from-blue-600 hover:to-blue-700 transition-all duration-200 shadow-lg shadow-blue-100"
                        >
                            <span className="flex items-center justify-center">
                                Try Again
                                <svg className="w-5 h-5 ml-2 transform group-hover:translate-x-1 transition-transform" fill="none" stroke="currentColor" viewBox="0 0 24 24">
                                    <path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M13 9l3 3m0 0l-3 3m3-3H8m13 0a9 9 0 11-18 0 9 9 0 0118 0z" />
                                </svg>
                            </span>
                        </Link>
                        <Link 
                            href="/"
                            className="block w-full py-3 px-4 text-center bg-white text-gray-700 rounded-xl hover:bg-gray-50 transition-colors border border-gray-200"
                        >
                            Return to Dashboard
                        </Link>
                    </div>
                </div>
            </div>
        );
    }
}