import {useEffect, useRef, useState} from "react";
import RootConfigs from "../../../../../configs";

// Interface defining the structure of each filter option
export interface JobFilterOptionModel {
    option: string;
    optionLabel?: string;
}

export default function JobFilterFieldListComponent(props: {
    options: JobFilterOptionModel[];
    value?: string;
    onChange: (value: string) => void;
    label: string;
    keepOriginalCase?: boolean;
}) {
    // Refs and state for handling horizontal scroll behavior
    const scrollRef = useRef<HTMLDivElement>(null);
    const [showLeftFade, setShowLeftFade] = useState(false);
    const [showRightFade, setShowRightFade] = useState(false);

    // Function to check if content overflows and show/hide fade effects
    const checkForOverflow = () => {
        if (scrollRef.current) {
            const { scrollWidth, clientWidth, scrollLeft } = scrollRef.current;
            setShowLeftFade(scrollLeft > 0);
            setShowRightFade(scrollLeft < scrollWidth - clientWidth);
        }
    };

    // Set up event listeners for resize and cleanup
    useEffect(() => {
        checkForOverflow();
        window.addEventListener('resize', checkForOverflow);
        return () => window.removeEventListener('resize', checkForOverflow);
    }, []);

    // Auto-scroll to the beginning when a value is selected
    useEffect(() => {
        if (scrollRef.current && props.value) {
            scrollRef.current.scrollTo({
                left: 0,
                behavior: 'smooth'
            });
        }
    }, [props.value]);

    const handleScroll = () => {
        checkForOverflow();
    };

    // Helper function to format option labels with proper capitalization
    function getOptionLabel(optionValue: string) {
        const words = optionValue.split(' ');
        const capitalizedWords = words.map(word => {
            return word.charAt(0).toUpperCase() + word.slice(1).toLowerCase();
        });
        return capitalizedWords.join(' ');
    }

    // Reorganize options array to put selected option first
    const reorganizedOptions = [...props.options].sort((a, b) => {
        if (a.option === props.value) return -1;
        if (b.option === props.value) return 1;
        return 0;
    });

    // Function to render individual filter option buttons
    function renderOptionItem(option: JobFilterOptionModel) {
        const isActive: boolean = props.value === option.option;
        let label: string = option.optionLabel ? option.optionLabel : option.option;

        return (
            <button
                key={option.option}
                className={`
                    whitespace-nowrap px-3 py-1 rounded-full text-sm font-medium
                    ${isActive
                    ? `bg-${RootConfigs.THEME.PRIMARY_COLOR}-900 text-white`
                    : 'bg-gray-200 text-gray-700 hover:bg-gray-300'}
                    transition-colors duration-200
                `}
                onClick={() => props.onChange(option.option === props.value ? "" : option.option)}
            >
                {props.keepOriginalCase ? label : getOptionLabel(label)}
            </button>
        )
    }

    return (
        <div className="mb-4">
            <h3 className="text-lg font-semibold mb-2 text-gray-800">{props.label}</h3>
            <div className="relative">
                {/* Left fade effect for horizontal scroll */}
                <div
                    className={`absolute left-0 top-0 bottom-0 w-8 bg-gradient-to-r from-white to-transparent pointer-events-none transition-opacity duration-300 ${
                        showLeftFade ? 'opacity-100' : 'opacity-0'
                    }`}
                />
                {/* Scrollable container for filter options */}
                <div
                    ref={scrollRef}
                    className="flex overflow-x-auto pb-2 space-x-2 hide-scrollbar relative"
                    style={{ scrollbarWidth: 'none', msOverflowStyle: 'none' }}
                    onScroll={handleScroll}
                >
                    {reorganizedOptions.map((option) => renderOptionItem(option))}
                </div>
                {/* Right fade effect for horizontal scroll */}
                <div
                    className={`absolute right-0 top-0 bottom-0 w-8 bg-gradient-to-l from-white to-transparent pointer-events-none transition-opacity duration-300 ${
                        showRightFade ? 'opacity-100' : 'opacity-0'
                    }`}
                />
            </div>
        </div>
    );
}