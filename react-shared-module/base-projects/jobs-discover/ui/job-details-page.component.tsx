import React, {useEffect, useState} from 'react';
import {
    HiOutlineAcademicCap,
    HiOutlineClock,
    HiOutlineGlobeAlt,
    HiOutlineLocationMarker,
    HiOutlineOfficeBuilding,
    HiOutlineCurrencyDollar, HiOutlineInformationCircle, HiOutlineTag,
    HiOutlineStar
} from 'react-icons/hi';
import {JobDetailsModel, JobListModel, JobSalaryRange, JobsDiscoverDataUtils} from "../jobs-discover-data.model";
import Link from "next/link";
import { SharedRoutesUtils } from "../../../utils/shared-routes.utils";
import ShareButtonsComponent from "../../../ui-components/shared/ShareButtons.component";
import AppStoreBanner from "../../../ui-components/shared/AppStoreBanner.component";
import BaseTextComponent from "../../../ui-components/shared/BaseText.component";
import GoogleAdsComponent, {AdsSlots} from "../../../ui-components/ads/GoogleAds.component";
import {SharedBackendApi} from "../../../logic/shared-data/sharedbackend.api";
import {SharedFirestoreCollectionDB} from "../../../logic/shared-data/datamodel/shared-firestore-collections";
import {SharedAnalyticsManager} from "../../../utils/shared-analytics.utils";
import {Info} from "lucide-react";
import JobListItemComponent from "./job-list-item.component";

const InfoItem: React.FC<{ icon: React.ReactNode; label: string; value: string }> = ({ icon, label, value }) => (
    <div className="flex items-center mr-6 mb-2">
        {icon}
        <span className="text-gray-600 mr-2">{label}:</span>
        <span className="font-semibold">{value}</span>
    </div>
);

const ApplyNowCard: React.FC<{ jobData: JobDetailsModel}> = ({ jobData}) => (
    <div className="bg-white p-6 rounded-lg drop-shadow-xl">
        <h3 className="text-xl font-semibold mb-2">Apply now</h3>
        <p className="text-sm text-gray-600 mb-4">Please let the company know that you found this position on our job board. This is a great way to support us, so we can keep posting cool jobs every day!</p>
        <ApplyNowButton jobData={jobData} />
    </div>
);

function ApplyNowButton(props: {jobData: JobDetailsModel}) {
    const stats: any = (props.jobData as any).stats;
    let statsView: any = <></>
    if (stats && stats.viewsCounter > 10) {
        const viewsCounter: number = stats.viewsCounter ? stats?.viewsCounter : 0;
        const applyCounter: number = stats.applyCounter ? stats?.applyCounter : 0;

        statsView = (
            <div className='flex flex-wrap gap-2'>
                <span>{`👁️ Views: ${viewsCounter}`}</span>
                <span>{`🚀️ Applied: ${applyCounter}`}</span>
            </div>
        )
    }

    return (
        <div className='flex flex-col gap-2 justify-center items-center min-w-64'>
            {statsView}
            <a href={props.jobData.applicationUrl} target="_blank" rel="noopener noreferrer" className="block w-full">
                <button className="w-full btn btn-secondary" onClick={() => {

                    SharedAnalyticsManager.getInstance().logUserInteraction("apply_job_clicked", {
                        "jobUid": props.jobData.uid,
                    });

                    if (props.jobData.uid) {
                        SharedBackendApi.incrementStatData({
                            parentCollectionId: SharedFirestoreCollectionDB.JobsDiscoverProject.JobDetails,
                            docId: props.jobData.uid!!,
                            dataKey: "applyCounter",
                        });
                    }
                }}>
                    🚀 Apply now
                </button>
            </a>
        </div>
    )
}

const SalaryRangeItem: React.FC<{ salaryRange: JobSalaryRange }> = ({ salaryRange }) => {
    const [showTooltip, setShowTooltip] = useState(false);

    const formatSalary = () => {
        const symbol = JobsDiscoverDataUtils.getCurrencySymbol(salaryRange.currency || '');
        if (salaryRange.min && salaryRange.max) {
            return `${symbol}${salaryRange.min.toLocaleString()} - ${symbol}${salaryRange.max.toLocaleString()}`;
        } else if (salaryRange.min) {
            return `From ${symbol}${salaryRange.min.toLocaleString()}`;
        } else if (salaryRange.max) {
            return `Up to ${symbol}${salaryRange.max.toLocaleString()}`;
        }
        return 'Not specified';
    };

    return (
        <div className="flex items-center mr-6 mb-2 relative">
            <HiOutlineCurrencyDollar className="mr-2" />
            <span className="text-gray-600 mr-2">{salaryRange.isEstimated ? "Estimated Salary:" : "Salary:"}</span>
            <span className="font-semibold">
                {formatSalary()}
            </span>
            {salaryRange.isEstimated && (
                <div className="ml-2 relative">
                    <HiOutlineInformationCircle
                        className="text-gray-500 cursor-help"
                        onMouseEnter={() => setShowTooltip(true)}
                        onMouseLeave={() => setShowTooltip(false)}
                    />
                    {showTooltip && (
                        <div className="absolute z-10 w-64 px-4 py-2 text-sm font-light text-white bg-gray-700 rounded-lg shadow-lg bottom-full left-1/2 transform -translate-x-1/2 -translate-y-2">
                            This is an estimated salary range based on similar positions, not officially announced by the company.
                            <div className="absolute w-3 h-3 bg-gray-700 transform rotate-45 left-1/2 -translate-x-1/2 -bottom-1.5"></div>
                        </div>
                    )}
                </div>
            )}
        </div>
    );
};

const SkillsSection: React.FC<{ skills: string[] }> = ({ skills }) => (
    <div className="flex items-center mr-6 mb-2 flex-wrap">
        <HiOutlineTag className="mr-2" />
        <span className="text-gray-600 mr-2">Skills:</span>
        <div className="flex flex-wrap">
            {skills.map((skill, index) => (
                <span key={index} className="bg-gray-200 rounded-full px-2 py-1 text-xs font-semibold text-gray-700 mr-2 mb-2">
                    {skill}
                </span>
            ))}
        </div>
    </div>
);


const JobDetailsPage: React.FC<{ jobData: JobDetailsModel, similarJobs?: JobListModel[] }> = ({ jobData , similarJobs}) => {

    useEffect(() => {
        if (jobData) {
            SharedBackendApi.incrementArticleViewEvent({
                parentCollectionId: SharedFirestoreCollectionDB.JobsDiscoverProject.JobDetails,
                docId: jobData?.uid!,
            });
        }
    }, []);

    const formatPublishedDate = (publishedTime: number) => {
        const date = new Date();
        date.setTime(publishedTime);
        return date.toLocaleDateString('en-US', { year: 'numeric', month: 'long', day: 'numeric' });
    };

    function renderSourceNote() {
        if (jobData.source) {
            return (
                <div className="flex items-center gap-2 text-gray-500 text-xs border-l-4 border-blue-200 pl-3 py-2">
                    <Info className="w-4 h-4" />
                    <span>Originally posted on {jobData.source}</span>
                </div>
            );
        } else {
            return <></>
        }

    }

    function renderSimilarJobs() {
        if (similarJobs) {
            return (
                <div className='flex flex-col gap-2'>
                    <div className='divider'>SIMILAR JOBS</div>
                    {
                        similarJobs.map((jobData) => {
                            return (
                                <JobListItemComponent key={jobData.uid!} jobData={jobData} />
                            )
                        })
                    }
                </div>
            )
        } else {
            return <></>
        }
    }


    return (
        <div className="max-w-7xl mx-auto p-4 md:p-6 flex flex-col gap-2">
            <div className="grid grid-cols-1 md:grid-cols-3 gap-6">
                <div className="md:col-span-2">
                    <div className="bg-white p-4 md:p-6 sm:rounded-lg sm:drop-shadow-xl mb-6">
                        <div className="flex items-center mb-4">
                            <img src={jobData.companyLogo} alt={`${jobData.company} logo`} className="w-16 h-16 rounded mr-4" />
                            <div>
                                <div className="flex items-center gap-2 flex-wrap">
                                    <h1 className="text-2xl font-bold">{jobData.title}</h1>
                                    {jobData.publishPlanType === 'FEATURED' && (
                                        <span className="flex items-center gap-1 px-2 py-1 text-xs font-semibold text-green-600 bg-green-100 rounded-full">
                                            <HiOutlineStar className="w-3 h-3" /> Featured
                                        </span>
                                    )}
                                </div>
                                {
                                    jobData.companyUrl ?
                                        <Link href={jobData.companyUrl} className="text-blue-600 hover:underline" target={"_blank"}>{jobData.company}</Link>
                                        : <span className="text-blue-600 hover:underline">{jobData.company}</span>
                                }
                            </div>
                        </div>
                        <div className="flex flex-wrap mb-4">
                            <Link href={SharedRoutesUtils.getJobsCategoryUrl(jobData.category!)}><InfoItem icon={<HiOutlineOfficeBuilding className="mr-2" />} label="Department" value={jobData.category || 'Not specified'} /></Link>
                            <InfoItem icon={<HiOutlineClock className="mr-2" />} label="Type" value={jobData.workMode!} />
                            <InfoItem icon={<HiOutlineGlobeAlt className="mr-2" />} label="Region" value={jobData.remoteRegion || 'Not specified'} />
                            <InfoItem icon={<HiOutlineLocationMarker className="mr-2" />} label="Location" value={jobData.location} />
                            <InfoItem icon={<HiOutlineAcademicCap className="mr-2" />} label="Experience" value={jobData.experienceLevel} />
                            {jobData.salaryRange && <SalaryRangeItem salaryRange={jobData.salaryRange} />}
                            {jobData.skills && jobData.skills.length > 0 && <SkillsSection skills={jobData.skills} />}
                        </div>
                        <div className="flex w-full justify-center my-4">
                            <ApplyNowButton jobData={jobData} />
                        </div>

                        <div className="flex w-full justify-center my-8">
                            <GoogleAdsComponent slot={AdsSlots.IN_ARTICLE} format={"fluid"} layout={"in-article"} />
                        </div>
                        <div className="flex w-full justify-center my-8">
                            <ShareButtonsComponent customLabel={"Share this job:"} />
                        </div>
                        <div className="mb-6">
                            <h2 className="text-xl font-semibold mb-2">Job Description</h2>
                            <p className="text-gray-700">Posted on: {formatPublishedDate(jobData.createdAt!)}</p>
                            <div className='prose my-8'>
                                <BaseTextComponent text={jobData.description} />
                            </div>
                            {renderSourceNote()}
                        </div>
                    </div>
                </div>
                <div>
                    <ApplyNowCard jobData={jobData} />
                    <div className="bg-white p-6 rounded-lg drop-shadow-xl mt-6">
                        <img src={jobData.companyLogo} alt={`${jobData.company} logo`} className="w-16 h-16 rounded mb-4" />
                        <h3 className="text-xl font-semibold mb-2">{jobData.company}</h3>
                        {
                            jobData.companyUrl ?
                                <Link href={jobData.companyUrl!} target={"_blank"} className="text-blue-600 hover:underline">View company page</Link>
                                : <></>
                        }
                    </div>

                    <div className='w-full my-5'>
                        <AppStoreBanner />
                    </div>


                    {renderSimilarJobs()}
                </div>
            </div>
        </div>
    );
};

export default JobDetailsPage;