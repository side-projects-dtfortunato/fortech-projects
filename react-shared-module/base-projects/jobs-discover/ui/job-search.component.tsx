// Implement job search component, which will be used to search for jobs in the jobs-discover project and will callback the search search tokens array (normalized, lowercase, no special characters, a word per token)
// the search button will be disabled if the search tokens array is empty

import React, { useState, KeyboardEvent, useEffect, useRef } from 'react';
import { FiSearch, FiX, FiTrendingUp, FiTrash2 } from 'react-icons/fi';
import { motion, AnimatePresence } from 'framer-motion';
import LocalStorageDataManager from '../../../logic/managers/LocalStorageData.manager';

// Constants
const RECENT_SEARCHES_KEY = 'recentJobSearches';
const MAX_RECENT_SEARCHES = 5;
const BUTTON_ANIMATION_CONFIG = {
  duration: 0.4,
  ease: [0.4, 0, 0.2, 1], // Custom easing for smooth motion
};

interface JobSearchProps {
  onSearch: (searchTokens: string[]) => void;
  className?: string;
  popularSearches?: string[];
  initialSearchTokens?: string[];
}

export const JobSearch: React.FC<JobSearchProps> = ({ 
  onSearch, 
  className,
  popularSearches = [],
  initialSearchTokens = []
}) => {
  // State management
  const [searchInput, setSearchInput] = useState(initialSearchTokens.join(' ') || '');
  const [showSuggestions, setShowSuggestions] = useState(false);
  const [recentSearches, setRecentSearches] = useState<string[]>([]);

  // Refs for DOM elements
  const inputRef = useRef<HTMLInputElement>(null);
  const containerRef = useRef<HTMLDivElement>(null);

  // Effects
  useEffect(() => {
    // Initialize recent searches from local storage
    const savedSearches = LocalStorageDataManager.getItem(RECENT_SEARCHES_KEY, []);
    setRecentSearches(savedSearches);

    // Handle clicks outside of the component to close suggestions
    const handleClickOutside = (event: MouseEvent) => {
      if (containerRef.current && !containerRef.current.contains(event.target as Node)) {
        setShowSuggestions(false);
      }
    };

    document.addEventListener('mousedown', handleClickOutside);
    return () => document.removeEventListener('mousedown', handleClickOutside);
  }, []);

  // Update search input when initialSearchTokens changes
  useEffect(() => {
    if (initialSearchTokens.length > 0) {
      setSearchInput(initialSearchTokens.join(' '));
    }
  }, [initialSearchTokens]);

  // Utility functions
  const normalizeToken = (token: string): string => {
    return token.toLowerCase().trim();
  };

  const isValidSearch = (): boolean => {
    return searchInput.trim().length > 0;
  };

  // Event handlers
  const handleInputKeyDown = (e: KeyboardEvent<HTMLInputElement>) => {
    if (e.key === 'Enter' && searchInput.trim()) {
      e.preventDefault();
      handleSearch();
    }
    if (e.key === 'Escape') {
      setShowSuggestions(false);
    }
  };

  const updateRecentSearches = (searchQuery: string) => {
    // Add new search to the beginning and remove duplicates
    const newRecentSearches = [
      searchQuery,
      ...recentSearches.filter(search => search !== searchQuery)
    ].slice(0, MAX_RECENT_SEARCHES);

    setRecentSearches(newRecentSearches);
    LocalStorageDataManager.saveItem(RECENT_SEARCHES_KEY, newRecentSearches);
  };

  const handleSearch = () => {
    if (!isValidSearch()) return;

    const searchQuery = searchInput.trim();
    if (searchQuery) {
      updateRecentSearches(searchQuery);
      // Split the search input into words and normalize each
      const allTokens = searchQuery
        .split(/\s+/)
        .map(word => normalizeToken(word))
        .filter(token => token.length > 0);
      
      onSearch(allTokens);
    }
  };

  const handleSuggestionClick = (suggestion: string) => {
    setSearchInput(suggestion);
    setShowSuggestions(false);
  };

  const handleClearSearch = () => {
    setSearchInput('');
    onSearch([]);
  };

  // Render helper functions
  const renderSearchButton = () => (
    <motion.button
      animate={{ 
        // Only expand on md breakpoint and above (768px+)
        width: isValidSearch() ? 'calc(var(--search-button-width, 48px))' : '48px',
        opacity: 1,
      }}
      transition={BUTTON_ANIMATION_CONFIG}
      whileHover={{ scale: isValidSearch() ? 1.02 : 1 }}
      whileTap={{ scale: isValidSearch() ? 0.98 : 1 }}
      onClick={handleSearch}
      disabled={!isValidSearch()}
      className="btn btn-primary h-12 rounded-full overflow-hidden
        whitespace-nowrap flex items-center justify-center disabled:opacity-40
        disabled:cursor-not-allowed min-w-[48px] search-button-container"
    >
      <div className="flex items-center justify-center gap-2 px-1">
        <FiSearch className="w-5 h-5 flex-shrink-0" />
        <AnimatePresence mode="wait">
          {isValidSearch() && (
            <motion.span
              initial={{ opacity: 0, width: 0 }}
              animate={{ opacity: 1, width: 'auto' }}
              exit={{ opacity: 0, width: 0 }}
              transition={{ duration: 0.2, delay: 0.1 }}
              className="overflow-hidden flex-shrink-0 hidden md:flex"
            >
              Search
            </motion.span>
          )}
        </AnimatePresence>
      </div>
    </motion.button>
  );

  const renderSuggestions = () => (
    <AnimatePresence>
      {showSuggestions && (recentSearches.length > 0 || popularSearches.length > 0) && (
        <motion.div
          initial={{ opacity: 0, y: 4 }}
          animate={{ opacity: 1, y: 0 }}
          exit={{ opacity: 0, y: 4 }}
          className="absolute top-full left-0 right-0 mt-2 bg-white rounded-lg shadow-xl 
            border border-base-200 z-50 overflow-hidden"
        >
          {/* Recent Searches Section */}
          {recentSearches.length > 0 && (
            <div className="p-4 border-b">
              <div className="flex justify-between items-center mb-3">
                <h3 className="text-sm font-medium text-base-content/70">Recent Searches</h3>
                <button
                  onClick={() => {
                    setRecentSearches([]);
                    LocalStorageDataManager.saveItem(RECENT_SEARCHES_KEY, []);
                  }}
                  className="text-sm text-error hover:text-error-focus flex items-center gap-1.5 
                    transition-colors duration-200 px-2 py-1 rounded-md hover:bg-error/10"
                >
                  <FiTrash2 size={14} />
                  Clear History
                </button>
              </div>
              <div className="flex flex-wrap gap-2">
                {recentSearches.map((search, index) => (
                  <button
                    key={index}
                    onClick={() => handleSuggestionClick(search)}
                    className="text-sm px-4 py-2 rounded-full bg-base-200 
                      hover:bg-base-300 transition-colors duration-200"
                  >
                    {search}
                  </button>
                ))}
              </div>
            </div>
          )}

          {/* Popular Searches Section */}
          {popularSearches.length > 0 && (
            <div className="p-4">
              <h3 className="text-sm font-medium text-base-content/70 mb-3 flex items-center gap-2">
                <FiTrendingUp className="text-primary" />
                Popular Searches
              </h3>
              <div className="flex flex-wrap gap-2">
                {popularSearches.map((search, index) => (
                  <button
                    key={index}
                    onClick={() => handleSuggestionClick(search)}
                    className="text-sm px-4 py-2 rounded-full bg-base-200 
                      hover:bg-base-300 transition-colors duration-200"
                  >
                    {search}
                  </button>
                ))}
              </div>
            </div>
          )}
        </motion.div>
      )}
    </AnimatePresence>
  );

  // Main render
  return (
    <div className={`w-full max-w-4xl py-5 px-2 mx-auto ${className}`} ref={containerRef}>
      <style jsx global>{`
        @media (min-width: 768px) {
          .search-button-container {
            --search-button-width: 120px;
          }
        }
      `}</style>
      <div className="relative">
        <div className="flex items-center gap-2 bg-white rounded-full shadow-lg pl-6 pr-2 h-16">
          <FiSearch className="text-gray-400 w-6 h-6" />
          <div className="flex-1 flex items-center gap-2">
            <input
              ref={inputRef}
              type="text"
              value={searchInput}
              onChange={(e) => setSearchInput(e.target.value)}
              onKeyDown={handleInputKeyDown}
              onFocus={() => setShowSuggestions(true)}
              className="flex-1 text-lg outline-none bg-transparent"
              placeholder="Job title or keyword"
            />
            {searchInput.trim() && (
              <button
                onClick={handleClearSearch}
                className="p-1.5 hover:bg-base-200 rounded-full transition-colors"
                aria-label="Clear search"
              >
                <FiX className="w-5 h-5 text-gray-400" />
              </button>
            )}
          </div>
          {renderSearchButton()}
        </div>
        {renderSuggestions()}
      </div>
    </div>
  );
};
