import React, { useMemo, useState, useEffect } from 'react';
import {HiOutlineBriefcase, HiOutlineGlobeAlt, HiOutlineArrowRight} from 'react-icons/hi';
import RootConfigs from "../../../../../configs";
import {getLanguageLabel} from "../../../logic/language/language.helper";
import { TypeAnimation } from 'react-type-animation';
import { UIHelper } from '../../../../ui-helper/UIHelper';
import Link from 'next/link';
import { SharedRoutesUtils } from '../../../utils/shared-routes.utils';

/**
 * Props interface for the JobRegionHeader component
 * @property {string} region - Optional region name to display. If not provided, shows homepage view
 */
interface JobHeaderProps {
    region?: string;
}

/**
 * JobRegionHeader Component
 * Displays a header section for the jobs listing page with different views for homepage and region pages
 * 
 * @component
 * @param {JobHeaderProps} props - Component props
 * @returns {JSX.Element} Rendered component
 */
const JobRegionHeader: React.FC<JobHeaderProps> = ({region}) => {
    const isHomepage = !region;
    const [showAnimation, setShowAnimation] = useState(false);
    const [fadeIn, setFadeIn] = useState(false);

    // Get a random initial region
    const initialRegion = useMemo(() => {
        const regions = UIHelper.getJobsFilterRegions();
        const randomIndex = Math.floor(Math.random() * regions.length);
        return regions[randomIndex];
    }, []);

    // Memoize and optimize the typing sequence
    const typingSequence = useMemo(() => {
        const regions = UIHelper.getJobsFilterRegions()
            .slice(0, 5);
        
        return regions.flatMap((reg) => [
            reg,    // Type the region
            2000,   // Wait 2 seconds
            '',     // Clear the text
            400,    // Wait 400ms before next region
        ]);
    }, []);

    useEffect(() => {
        // Start fade out of static text
        const fadeTimer = setTimeout(() => {
            setFadeIn(true);
        }, 3800); // Slightly before showing animation

        // Show animation after fade transition
        const animationTimer = setTimeout(() => {
            setShowAnimation(true);
        }, 4000);

        return () => {
            clearTimeout(fadeTimer);
            clearTimeout(animationTimer);
        };
    }, []);

    const renderRegionText = () => {
        if (showAnimation) {
            return (
                <TypeAnimation
                    sequence={typingSequence}
                    wrapper="span"
                    cursor={true}
                    repeat={Infinity}
                    speed={50}
                    className={`text-${RootConfigs.THEME.PRIMARY_COLOR}-800`}
                />
            );
        }

        return (
            <span 
                className={`
                    text-${RootConfigs.THEME.PRIMARY_COLOR}-800
                    transition-opacity duration-200
                    ${fadeIn ? 'opacity-0' : 'opacity-100'}
                `}
            >
                {initialRegion}
            </span>
        );
    };

    /**
     * Renders the title section with optional region badge
     */
    const renderTitle = () => (
        <div className="flex flex-col items-center gap-2">
            <h1 className="text-3xl sm:text-4xl font-bold text-gray-900 tracking-tight">
                {isHomepage ? (
                    <span className="flex flex-col items-center gap-1">
                        <span>{getLanguageLabel("REMOTE_JOBS_EXPLORE_JOBS_TITLE")}</span>
                        <span className="flex items-center gap-2">
                            {renderRegionText()}
                            <span className={`text-${RootConfigs.THEME.PRIMARY_COLOR}-800`}>
                                Jobs
                            </span>
                        </span>
                    </span>
                ) : (
                    <span className="flex flex-col items-center gap-1">
                        <span>{getLanguageLabel("REMOTE_JOBS_LATEST_JOBS_IN_REGION_TITLE", "Latest Remote Jobs in")}</span>
                        <span className={`text-${RootConfigs.THEME.PRIMARY_COLOR}-800`}>
                            {region}
                        </span>
                    </span>
                )}
            </h1>
            
            {/* Region badge - only shown when region is selected */}
            {!isHomepage && (
                <div className="flex items-center gap-2">
                    <HiOutlineGlobeAlt 
                        className={`h-5 w-5 text-${RootConfigs.THEME.PRIMARY_COLOR}-600`}
                    />
                    <span className={`inline-flex items-center px-3 py-1 
                        bg-${RootConfigs.THEME.PRIMARY_COLOR}-50 text-${RootConfigs.THEME.PRIMARY_COLOR}-700 
                        text-base font-medium rounded-full`}>
                        {region}
                    </span>
                </div>
            )}
        </div>
    );

    /**
     * Renders the subtitle section
     */
    const renderSubtitle = () => (
        <div className="max-w-2xl mx-auto">
            <p className="text-base text-gray-600 leading-relaxed">
                {isHomepage ? (
                    <span className="flex items-center justify-center gap-2">
                        <HiOutlineGlobeAlt 
                            className={`h-5 w-5 text-${RootConfigs.THEME.PRIMARY_COLOR}-600`}
                        />
                        {getLanguageLabel("REMOTE_JOBS_DISCOVER_JOBS_SUBTITLE")}
                    </span>
                ) : (
                    getLanguageLabel("REMOTE_JOBS_REGION_SUBTITLE", `Discover the best remote opportunities in ${region} from top companies`)
                )}
            </p>
        </div>
    );

    /**
     * Renders the CTA section for posting a job
     */
    const renderPostJobCTA = () => (
        <div className="mt-8">
            <Link
                href={SharedRoutesUtils.getSubmitJob()}
                className={`
                    group
                    inline-flex items-center gap-2 px-6 py-3
                    bg-${RootConfigs.THEME.PRIMARY_COLOR}-600 
                    hover:bg-${RootConfigs.THEME.PRIMARY_COLOR}-700
                    text-white font-medium rounded-full
                    transform transition-all duration-200
                    hover:scale-105 hover:shadow-lg
                    focus:outline-none focus:ring-2 
                    focus:ring-${RootConfigs.THEME.PRIMARY_COLOR}-500 focus:ring-offset-2
                `}
            >
                <span>Post a Job</span>
                <HiOutlineArrowRight className="w-5 h-5 transition-transform duration-300 
                    animate-[arrow-move_2s_ease-in-out_infinite]" />
            </Link>
            <p className="mt-3 text-sm text-gray-500">
                {getLanguageLabel("REMOTE_JOBS_POST_JOB_SUBTITLE", "Reach thousands of remote talent worldwide")}
            </p>

            <style>{`
                @keyframes arrow-move {
                    0%, 100% {
                        transform: translateX(0);
                    }
                    15% {
                        transform: translateX(4px);
                    }
                    30% {
                        transform: translateX(0);
                    }
                }
            `}</style>
        </div>
    );

    return (
        <div className="relative w-full">
            {/* Content layer - reduced padding */}
            <div className="relative py-8 sm:py-12 px-4 sm:px-6 lg:px-8">
                <div className="max-w-7xl mx-auto">
                    <div className="text-center space-y-4">
                        {renderTitle()}
                        {renderSubtitle()}
                        {renderPostJobCTA()}
                    </div>
                </div>
            </div>
        </div>
    );
};

export default JobRegionHeader;
