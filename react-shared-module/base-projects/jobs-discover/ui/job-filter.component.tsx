import React, {useState, useEffect, useRef} from 'react';
import {UIHelper} from "../../../../ui-helper/UIHelper";
import JobFilterFieldListComponent, {JobFilterOptionModel} from "./job-filter-field-list.component";
import {JobFiltersSelected} from "../jobs-discover-data.model";
import {SharedAnalyticsManager} from "../../../utils/shared-analytics.utils";
import {SharedUtils} from "../../../utils/shared-utils";
import {
    Bell, 
    Briefcase, 
    Globe2, 
    Code2, 
    Building2,
    ChevronDown, 
    ChevronUp
} from "lucide-react";
import {MdClear} from "react-icons/md";

/**
 * JobFilter Component
 * 
 * A collapsible filter interface for job listings that includes:
 * - Category selection
 * - Region filtering
 * - Work mode options
 * - Skills filtering
 * - Auto-collapse on scroll
 * - Animated transitions
 */
const JobFilter: React.FC<{
    categories: string[];
    filterListSkills?: string[];
    initialCategory?: string;
    initialRemoteRegion?: string;
    initialSkill?: string;
    initialWorkMode?: string
    onFilterChange: (filters: JobFiltersSelected) => void;
}> = ({ 
    categories, 
    filterListSkills, 
    initialCategory = '', 
    initialRemoteRegion = '', 
    initialSkill, 
    initialWorkMode = "", 
    onFilterChange 
}) => {
    // State Management
    const [category, setCategory] = useState<string>(initialCategory);
    const [remoteRegion, setRemoteRegion] = useState<string>(initialRemoteRegion);
    const [workModeSelected, setWorkModeSelected] = useState<string>(initialWorkMode);
    const [skillSelected, setSkillSelected] = useState<string | undefined>(initialSkill);
    const [isExpanded, setIsExpanded] = useState(false);
    const filterRef = useRef<HTMLDivElement>(null);

    // Auto-collapse effect on scroll
    useEffect(() => {
        let lastScrollY = window.scrollY;
        let ticking = false;

        const handleScroll = () => {
            if (!ticking) {
                window.requestAnimationFrame(() => {
                    if (filterRef.current && isExpanded) {
                        const filterRect = filterRef.current.getBoundingClientRect();
                        // Collapse when 50% of component is scrolled out of view
                        if (filterRect.bottom < filterRect.height / 2) {
                            setIsExpanded(false);
                        }
                    }
                    lastScrollY = window.scrollY;
                    ticking = false;
                });
                ticking = true;
            }
        };

        window.addEventListener('scroll', handleScroll, { passive: true });
        return () => window.removeEventListener('scroll', handleScroll);
    }, [isExpanded]);

    // Filter Change Handlers
    const handleFilterChange = (
        type: 'category' | 'region' | 'workMode' | 'skill',
        value: string
    ) => {
        // Log analytics
        SharedAnalyticsManager.getInstance().logUserInteraction("job_filter_selected", {
            [type]: value,
        });

        // Update state based on filter type
        switch (type) {
            case 'category':
                setCategory(value);
                break;
            case 'region':
                setRemoteRegion(value);
                break;
            case 'workMode':
                setWorkModeSelected(value);
                break;
            case 'skill':
                setSkillSelected(value);
                break;
        }

        // Notify parent component of changes
        onFilterChange({
            category: type === 'category' ? value : category,
            remoteRegion: type === 'region' ? value : remoteRegion,
            skillSelected: type === 'skill' ? value : skillSelected,
            workMode: type === 'workMode' ? value : workModeSelected,
        });
    };

    // Helper Functions
    const getActiveFiltersCount = () => 
        [category, remoteRegion, workModeSelected, skillSelected]
            .filter(SharedUtils.isStringNotEmpty).length;

    const getActiveFiltersInlineText = () => {
        const parts = [];
        
        if (SharedUtils.isStringNotEmpty(category)) parts.push(category);
        if (SharedUtils.isStringNotEmpty(remoteRegion)) parts.push(remoteRegion);
        if (SharedUtils.isStringNotEmpty(workModeSelected)) parts.push(workModeSelected);
        if (SharedUtils.isStringNotEmpty(skillSelected)) parts.push(skillSelected);

        return parts.length === 0 ? "All Jobs" : parts.join(" • ");
    };

    const getFilterIcon = (filterType: string) => {
        const iconMap = {
            'Category': <Briefcase className="h-4 w-4" />,
            'Region': <Globe2 className="h-4 w-4" />,
            'Work Mode': <Building2 className="h-4 w-4" />,
            'Skill': <Code2 className="h-4 w-4" />
        };
        return iconMap[filterType as keyof typeof iconMap] || null;
    };

    // Reset Filters Handler
    const handleResetFilters = (e?: React.MouseEvent) => {
        e?.stopPropagation(); // Prevent expanding/collapsing when clearing
        setCategory("");
        setWorkModeSelected("");
        setSkillSelected("");
        setRemoteRegion("");
        onFilterChange({
            category: "",
            remoteRegion: "",
            skillSelected: "",
            workMode: "",
        });
    };

    // Render Functions
    const renderActiveFilters = () => {
        const activeFilters = [
            { type: 'Category', value: category || 'All Job Categories' },
            { type: 'Region', value: remoteRegion || 'All Regions' },
            { type: 'Work Mode', value: workModeSelected || 'Any work mode' }
        ];
        
        if (SharedUtils.isStringNotEmpty(skillSelected)) {
            activeFilters.push({ type: 'Skill', value: skillSelected || '' });
        }

        return (
            <div className="flex flex-wrap gap-2">
                {activeFilters.map((filter, index) => (
                    <span key={index} 
                        className={`badge gap-1 py-2 ${
                            filter.value.startsWith('All ') || filter.value === 'Any work mode' 
                                ? 'badge-ghost text-slate-500' 
                                : 'badge-neutral'
                        }`}
                    >
                        {getFilterIcon(filter.type)}
                        {filter.type}: {filter.value}
                    </span>
                ))}
            </div>
        );
    };

    function renderCategoriesFilter() {
        if (categories.length > 1) {
            const categoriesOptions: JobFilterOptionModel[] = categories.map((cat) => {
                return {
                    option: cat,
                };
            });
            return (
                <JobFilterFieldListComponent
                    options={[{
                        option: "",
                        optionLabel: "All Job Categories",
                    }, ...categoriesOptions]}
                    value={category}
                    onChange={(newCategory) => handleFilterChange('category', newCategory)}
                    label="Job Category"
                />
            )
        } else {
            return (<></>);
        }
    }

    function renderRegionFilter() {
        const remoteRegions = UIHelper.getJobsFilterRegions();
        if (remoteRegions.length > 1) {
            const remoteRegionsOptions: JobFilterOptionModel[] = remoteRegions.map((region) => {
                return {
                    option: region,
                };
            });

            return (
                <JobFilterFieldListComponent
                    keepOriginalCase={true}
                    options={[{
                        option: "",
                        optionLabel: "All Regions",
                    }, ...remoteRegionsOptions]}
                    value={remoteRegion}
                    onChange={(newRegion) => handleFilterChange('region', newRegion)}
                    label="Region"
                />
            )
        } else {
            return <></>
        }
    }

    function renderPopularSkills() {
        if (filterListSkills && filterListSkills.length > 0) {
            return (
                <JobFilterFieldListComponent
                    options={filterListSkills.map((skill) => {
                        return {
                            option: skill,
                        };
                    })}
                    value={skillSelected}
                    onChange={(newSkill) => handleFilterChange('skill', newSkill)}
                    label="Popular Skills"
                />
            )
        } else {
            return <></>
        }
    }

    function renderWorkMode() {
        const workModes = UIHelper.getWorkModeAllowed();
        if (workModes.length > 1) {
            const workModeOptions: JobFilterOptionModel[] = workModes.map((workModeType) => {
                return {
                    option: workModeType,
                };
            });

            return (
                <JobFilterFieldListComponent
                    keepOriginalCase={true}
                    options={[{
                        option: "",
                        optionLabel: "Any work mode",
                    }, ...workModeOptions]}
                    value={workModeSelected}
                    onChange={(newWorkMode) => handleFilterChange('workMode', newWorkMode)}
                    label="Work Mode"
                />
            )
        } else {
            return <></>
        }
    }

    function renderCleanFilters() {
        if (SharedUtils.isStringNotEmpty(category)
            || SharedUtils.isStringNotEmpty(remoteRegion)
            || SharedUtils.isStringNotEmpty(workModeSelected)
            || SharedUtils.isStringNotEmpty(skillSelected)) {
            return (
                <button 
                    className='btn btn-xs btn-ghost text-slate-600 hover:bg-slate-100'
                    onClick={handleResetFilters}
                >
                    <MdClear className="h-4 w-4" /> Clear
                </button>
            )
        }
        return null;
    }

    return (
        <div ref={filterRef} className="rounded-lg border border-slate-200 bg-white shadow-sm mb-5">
            <div className="p-4 flex flex-col gap-2 cursor-pointer hover:bg-slate-50"
                 onClick={() => setIsExpanded(!isExpanded)}>
                <div className="flex items-center justify-between">
                    <h3 className="font-medium text-slate-800 flex items-center gap-2">
                        <Briefcase className="h-4 w-4 text-primary" />
                        Job Search Filters
                    </h3>
                    <div className="flex items-center gap-2">
                        {getActiveFiltersCount() > 0 && !isExpanded && (
                            <button 
                                className="btn btn-xs btn-ghost text-slate-600 hover:bg-slate-100"
                                onClick={handleResetFilters}
                            >
                                <MdClear className="h-3 w-3" />
                            </button>
                        )}
                        {isExpanded ? (
                            <ChevronUp className="h-5 w-5 text-slate-500" />
                        ) : (
                            <ChevronDown className="h-5 w-5 text-slate-500" />
                        )}
                    </div>
                </div>
                {!isExpanded && renderActiveFilters()}
                {isExpanded && renderActiveFilters()}
            </div>

            <div className={`overflow-hidden transition-[max-height] duration-300 ease-in-out ${
                isExpanded ? 'max-h-[1000px]' : 'max-h-0'
            }`}>
                <div className="border-t border-slate-200 p-4 space-y-4">
                    {renderCategoriesFilter()}
                    {renderRegionFilter()}
                    {renderPopularSkills()}
                    {renderWorkMode()}
                    {renderCleanFilters()}
                </div>
                <div className="border-t border-slate-200 bg-slate-50 p-3">
                    <div className="flex items-center gap-2 text-sm text-slate-600">
                        <Bell className="h-4 w-4 text-primary" />
                        <span>
                            We&apos;ll send you personalized notifications for new jobs matching your filters
                        </span>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default JobFilter;