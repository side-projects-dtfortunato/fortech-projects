import {ArticleImportedModel} from "../../data/model/ArticleImported.model";
import InlineLabelSeparatorComponent from "../../../../ui-components/shared/inline-label-separator.component";
import ArticleListMinimizedItemComponent from "../article-list-minimized-item.component";

export default function ArticleDetailsLatestArticlesComponent(props: {latestArticles: ArticleImportedModel[]}) {
    return (
        <div className='flex flex-col gap-5 mt-5'>
            <InlineLabelSeparatorComponent
                title={"Other Latest News"}titleSize={"medium"}/>

            {Object.values(props.latestArticles)
                .map((listArticleItem) => <ArticleListMinimizedItemComponent key={listArticleItem.uid!} articleData={listArticleItem} />)}
        </div>
    )
}