import {useAuthState} from "react-firebase-hooks/auth";
import {useEffect, useState} from "react";
import {BiSolidUpArrow} from "react-icons/bi";
import {deleteField} from "@firebase/firestore";
import {ArticleImportedModel, ArticleImportedUtils} from "../../data/model/ArticleImported.model";
import {SharedFirestoreCollectionDB} from "../../../../logic/shared-data/datamodel/shared-firestore-collections";
import {auth, FirebaseClientFirestoreUtils} from "../../../../logic/shared-data/firebase.utils";
import AuthManager from "../../../../logic/auth/auth.manager";
import LoginPopupComponent from "../../../../ui-components/login/shared/LoginPopup.component";

export default function ArticleUpvoteComponent(props: { articleData: ArticleImportedModel, style: "SQUARE" | "HORIZONTAL", hideVotes?: boolean}) {
    const [currentUser, isAuthLoading] = useAuthState(auth);
    const [hasUpvoted, setHasUpvoted] = useState(Object.keys(props.articleData.upvotes ? props.articleData.upvotes : {}).includes(AuthManager.getUserId() ? AuthManager.getUserId()! : ""));
    const [displayLoginPopup, setDisplayLoginPopup] = useState(false);

    if (!props.articleData.upvotes) {
        props.articleData.upvotes = {};
    }

    useEffect(() => {
        if (AuthManager.getUserId()) {
            setHasUpvoted(Object.keys(props.articleData.upvotes).includes(AuthManager.getUserId()!))
        }
    }, [currentUser]);

    async function onUpvoteClick() {
        // Backend API
        if (AuthManager.getUserId()) {
            let updateUserVote: boolean = !hasUpvoted;


            if (updateUserVote) {
                props.articleData.upvotes = {
                    ...props.articleData.upvotes,
                    [AuthManager.getUserId()!]: Date.now(),
                };
            } else {
                delete props.articleData.upvotes[AuthManager.getUserId()!];
            }
            setHasUpvoted(updateUserVote);

            const result = await FirebaseClientFirestoreUtils.updatePathDocument(
                [SharedFirestoreCollectionDB.ArticlesDigestProject.ArticlePublished],
                [props.articleData.uid!], {
                    upvotes: {
                        [AuthManager.getUserId()!]: updateUserVote ? Date.now() : deleteField(),
                    },
                });
            if (!result) {
                setHasUpvoted(!updateUserVote);
            }
        } else {
            // If not logged in, then redirect him to the login page
            setDisplayLoginPopup(true);
        }

    }

    if (props.style === "SQUARE") {
        return (<button className={"btn btn-secondary w-12 h-12 pt-1 " + (hasUpvoted ? "text-white" : "btn-ghost")}
                        onClick={onUpvoteClick}>
            <div className='flex flex-col gap-1 items-center'>
                <BiSolidUpArrow/> {props.hideVotes ? "-" : ArticleImportedUtils.getTotalUpvotes(props.articleData)}
            </div>


            <LoginPopupComponent isPopupOpen={displayLoginPopup} onClosed={() => setDisplayLoginPopup(false)} />
        </button>)
    } else {
        return (<button className={"btn btn-xs btn-secondary items-center " + (hasUpvoted ? "text-white" : "btn-outline")}
                        onClick={onUpvoteClick}>
            <BiSolidUpArrow className="mx-1"/> {props.hideVotes ? "-" : ArticleImportedUtils.getTotalUpvotes(props.articleData) + (hasUpvoted ? " Upvoted" : " Upvote")}



            <LoginPopupComponent isPopupOpen={displayLoginPopup} onClosed={() => setDisplayLoginPopup(false)} />
        </button>)
    }

}