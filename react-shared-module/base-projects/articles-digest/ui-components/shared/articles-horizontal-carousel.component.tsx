import Slider from "react-slick";

// Import css files
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import {ArticleImportedModel} from "../../data/model/ArticleImported.model";
import ArticlesDigestListItemComponent from "../articles-digest-list-item.component";

export default function ArticlesHorizontalCarouselComponent(props: {listPosts: ArticleImportedModel[]}) {

    function renderSlideItem(content: any, key: string) {
        return (
            <div key={key} className='flex items-center rounded justify-center max-w-56 sm:max-w-96 p-4'>
                {content}
            </div>
        )
    }

    function renderItems() {
        return props.listPosts.map((articleItem) => {
            return renderSlideItem(
                <ArticlesDigestListItemComponent articleData={articleItem} forceSmall={true} />,
                articleItem.uid!
            )
        });
    }

    var settings = {
        className: "slider variable-width",
        dots: false,
        infinite: true,
        centerMode: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        variableWidth: true,
        autoplay: true,
        adaptiveHeight: true,
    };

    if (props.listPosts.length > 0) {
        return (
            <div className='flex flex-col w-full overflow-x-hidden'>
                <Slider {...settings} className={"w-full max-w-fit p-0"}>
                    {renderItems()}
                </Slider>
            </div>
        )
    } else {
        return (<></>);
    }

}