import React from 'react';
import {ArticleImportedModel} from "../data/model/ArticleImported.model";
import ArticlesDigestListItemComponent from "./articles-digest-list-item.component";
import RootConfigs from "../../../../../configs";
import {Newspaper} from 'lucide-react';
import Link from "next/link";

interface ArticlesListContainerProps {
    listArticles?: ArticleImportedModel[];
    title: string;
    titleHighlight: string;
}

export default function ArticlesListContainerComponent({ listArticles, title, titleHighlight }: ArticlesListContainerProps) {
    const renderHeader = () => (
        <span className='flex flex-row text-3xl items-center my-2 sm:my-2'>
            <span className={`p-2 text-${RootConfigs.THEME.PRIMARY_COLOR}-900 font-bold`}>{title}</span>
            <span className={`p-2 bg-${RootConfigs.THEME.PRIMARY_COLOR}-900 rounded-xl text-white font-bold`}>{titleHighlight}</span>
        </span>
    );

    const renderNoArticlesMessage = () => (
        <div className={`flex flex-col items-center justify-center p-8 bg-${RootConfigs.THEME.PRIMARY_COLOR}-50 rounded-lg shadow-sm my-10 border border-${RootConfigs.THEME.PRIMARY_COLOR}-100`}>
            <Newspaper className={`w-16 h-16 text-${RootConfigs.THEME.PRIMARY_COLOR}-300 mb-4`} />
            <h2 className={`text-2xl font-bold text-${RootConfigs.THEME.PRIMARY_COLOR}-700 mb-2`}>Seems a bit quiet here...</h2>
            <p className={`text-${RootConfigs.THEME.PRIMARY_COLOR}-600 text-center max-w-md`}>
                We&apos;re working on bringing you fresh articles in this category.
                In the meantime, why not explore other exciting topics?
            </p>
            <Link href={"/"} className={`mt-6 px-4 py-2 bg-${RootConfigs.THEME.PRIMARY_COLOR}-600 text-white rounded-full hover:bg-${RootConfigs.THEME.PRIMARY_COLOR}-700 transition duration-300 ease-in-out`}>
                Checkout our latest news
            </Link>
        </div>
    );

    return (
        <div className='flex flex-col gap-8 p-5 '>
            {renderHeader()}
            {listArticles && listArticles.length > 0 ? (
                listArticles.map((articleItem) => (
                    <ArticlesDigestListItemComponent key={articleItem.uid!} articleData={articleItem} />
                ))
            ) : (
                renderNoArticlesMessage()
            )}
        </div>
    );
}