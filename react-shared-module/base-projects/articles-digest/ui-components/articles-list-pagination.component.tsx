import {useState} from "react";
import {DailyPublishedArticlesModel} from "../data/model/DailyPublishedArticles.model";
import {FirebaseClientFirestoreUtils} from "../../../logic/shared-data/firebase.utils";
import {SharedFirestoreCollectionDB} from "../../../logic/shared-data/datamodel/shared-firestore-collections";
import ArticlesDigestListDailyItemsComponent from "./articles-digest-list-daily-items.component";
import InfiniteScroll from "react-infinite-scroll-component";
import SpinnerComponent from "../../../ui-components/shared/Spinner.component";
import SharedSortListByComponent, {
    SORTLIST_LOCALSTORAGE_KEY,
    SortListTypes
} from "../../../ui-components/shared/SharedSortListBy.component";
import LocalStorageDataManager from "../../../logic/managers/LocalStorageData.manager";


export default function ArticlesListPaginationComponent(props: {preloadedData: DailyPublishedArticlesModel[], itemsPerPage: number}) {
    const [listPublishedDays, setListPublishedDays] = useState<DailyPublishedArticlesModel[]>(props.preloadedData);
    const [hasMorePages, setHasMorePages] = useState(props.preloadedData.length >= props.itemsPerPage);
    const [selectedSort, setSelectedSort] = useState<SortListTypes>(LocalStorageDataManager.getString(SORTLIST_LOCALSTORAGE_KEY, SortListTypes.MOST_RECENT) as SortListTypes);

    async function loadNextPage() {
        // BackendAPI
        let lastDocId: number | undefined = listPublishedDays.length > 0 ? listPublishedDays[listPublishedDays.length - 1].publishedDayId : undefined;
        const response: any[] = await FirebaseClientFirestoreUtils
            .getDocumentsWithPagination(SharedFirestoreCollectionDB.ArticlesDigestProject.DailyPublishedArticles, props.itemsPerPage, "publishedDayId", lastDocId);

        if (response && response.length > 0) {
            setListPublishedDays(listPublishedDays.concat(response));
            setHasMorePages(response.length === props.itemsPerPage); // If the response has the same number of items per page, means that has possible more pages
        } else {
            setHasMorePages(false);
        }
    }

    function renderDailyPublishedStartupsContainers() {
        return listPublishedDays.filter((listItem) => listItem.listArticles)
            .map((listItem, index) => {
            return (
                <div key={index} className='flex flex-col gap-5'>
                    <ArticlesDigestListDailyItemsComponent key={listItem.publishedDayId} publishedDayArticles={listItem} linkable={true} sortBy={selectedSort} />
                </div>
            )
        })
    }


    return (
        <div className='flex flex-col mt-0 pt-2 min-h-screen'>
            <div className='flex justify-start'>
                <SharedSortListByComponent selectedSortType={selectedSort} onSelectedChanged={setSelectedSort} />
            </div>
            <InfiniteScroll next={loadNextPage} hasMore={hasMorePages} loader={<SpinnerComponent/>} dataLength={listPublishedDays.length}>
                {renderDailyPublishedStartupsContainers()}
            </InfiniteScroll>
        </div>
    )
}