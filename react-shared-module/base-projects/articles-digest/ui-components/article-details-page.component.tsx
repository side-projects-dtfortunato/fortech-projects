import {ArticleImportedModel} from "../data/model/ArticleImported.model";
import CommonUtils from "../../../logic/commonutils";
import BaseTextComponent from "../../../ui-components/shared/BaseText.component";
import Link from "next/link";
import Markdown from "react-markdown";
import {GoLinkExternal} from "react-icons/go";
import ArticleUpvoteComponent from "./shared/article-upvote.component";
import {SharedFirestoreCollectionDB} from "../../../logic/shared-data/datamodel/shared-firestore-collections";
import StatsPostItemComponent from "../../../ui-components/shared/StatsPostItem.component";
import {FaRegCommentDots} from "react-icons/fa";
import NewsletterPopupComponent from "../../../ui-components/newsletter/shared/newsletter-popup.component";
import NewsletterSignupListBannerComponent
    from "../../../ui-components/newsletter/shared/newsletter-signup-list-banner.component";
import PostCommentsRootComponent from "../../community/ui/post-comments-root.component";
import RootConfigs from "../../../../../configs";
import {SharedRoutesUtils} from "../../../utils/shared-routes.utils";
import Image from "next/image";
import GoogleAdsComponent, {AdsSlots} from "../../../ui-components/ads/GoogleAds.component";
import ArticleDetailsLatestArticlesComponent from "./shared/article-details-latest-articles.component";
import ShareButtonsComponent from "../../../ui-components/shared/ShareButtons.component";
import AppStoreBanner from "../../../ui-components/shared/AppStoreBanner.component";
import GoogleAdsInFeedComponent from "../../../ui-components/ads/GoogleAdsInFeed.component";


export default function ArticleDetailsPageComponent(props: {articleData: ArticleImportedModel, latestArticles?: ArticleImportedModel[]}) {

    function renderTopHeader() {
        const sourceLabel: string = CommonUtils.capitalizeWords(props.articleData.source);
        const publishedTimeAgo: string = CommonUtils.getTimeAgo(props.articleData.createdAt!);

        return (
            <div className='flex flex-row items-center gap-2 text-xs text-slate-400'>
                <span>{sourceLabel}</span>
                <span>•</span>
                <span>{publishedTimeAgo}</span>
                <span>•</span>
                <StatsPostItemComponent numComments={props.articleData.stats?.commentsCounter} numViews={props.articleData.stats?.viewsCounter}/>
            </div>
        )
    }

    function renderTitle() {
        return (
            <h1 className='font-bold text-3xl '>
                {props.articleData.title.replaceAll("*", "")}
            </h1>
        )
    }


    function renderCategories() {
        let listTags: any[] = [];
        if (props.articleData.aiGeneratedContent?.category) {
            // Add Category
            listTags.push(<Link href={SharedRoutesUtils.getArticlesCategoryUrl(props.articleData.aiGeneratedContent.category!)} key={"category"} className={`badge badge-ghost bg-${RootConfigs.THEME.PRIMARY_COLOR}-100 text-${RootConfigs.THEME.PRIMARY_COLOR}-700 text-sm hover:bg-${RootConfigs.THEME.PRIMARY_COLOR}-300`}>{props.articleData.aiGeneratedContent.category}</Link>)
        }

        listTags = listTags.concat(props.articleData.aiGeneratedContent?.tags ? props.articleData.aiGeneratedContent?.tags?.map((cat, index) => {
            return (
                <div key={cat} className="badge badge-ghost text-slate-600 text-xs text-slate-600 hover:bg-orange-100">{cat}</div>
            )
        }) : []);



        return (
            <div className='flex flex-wrap gap-2'>
                {listTags}
            </div>
        )
    }

    function renderSourceLink() {
        const openUrl: string = CommonUtils.formatUrlToOpenExternalApp(props.articleData.originalLink);
        return (
            <div className='flex justify-start'>
                <Link className='flex gap-2 justify-start items-center font-light text-slate-400 text-sm no-underline' href={openUrl} target={"_blank"}>{`Source: ${props.articleData.source}`} <GoLinkExternal/></Link>
            </div>
        )
    }

    function renderArticleContent() {
        const listHighlights: any[] = props.articleData.aiGeneratedContent?.highlightsBullets ? props.articleData.aiGeneratedContent?.highlightsBullets
            .map((highlight, index) => {
                return (
                    <li key={index} className='text-slate-700 -my-10' style={{margin: 0}}>
                        <h2>
                            <Markdown className="text-base">{highlight}</Markdown>
                        </h2>
                    </li>
                )
            }) : [];

        // bg-slate-50 rounded-xl drop-shadow-xl
        return (
            <div className={"flex flex-col w-full p-3 gap-2"}>
                <GoogleAdsComponent slot={AdsSlots.HORIZONTAL_ADS} />
                <h3 className='font-bold text-slate-600 text-xl' style={{margin: 0}}>Summary:</h3>
                <ul className='flex flex-col gap-2 list-disc pl-4'>
                    {listHighlights}
                </ul>
                <article className='prose md:prose-lg lg:prose-lg flex flex-col w-full sm:pt-5 gap-2 '>
                    <BaseTextComponent text={props.articleData.aiGeneratedContent?.summary!}
                                   extraClassName={"w-full text-start"}/>
                </article>
                {renderSourceLink()}

            </div>
        )
    }


    function renderHeader() {
        const openUrl: string = CommonUtils.formatUrlToOpenExternalApp(props.articleData.originalLink);
        return (
            <div className='flex flex-col sm:flex-row text-black hover:text-orange-700 gap-5 items-stretch justify-stretch rounded-xl hover:bg-gradient-to-tr from-orange-50 to-transparent max-w-5xl'>
                <div className='-mx-2 -mt-7 sm:mx-0 sm:mt-0'>
                    <Image className='flex h-full sm:rounded w-full sm:min-w-72 sm:w-72 max-h-40 sm:max-h-40 sm:mt-1 object-cover' width={500} height={500} src={props.articleData.imageUrl!} alt={props.articleData.title.replaceAll("*", "")} />
                </div>
                <div className='flex flex-col grow justify-center items-start gap-3'>
                    <div className='flex flex-col gap-1 w-full'>
                        {renderTopHeader()}
                        {renderTitle()}
                    </div>
                    {renderCategories()}
                    <div className={"flex flex-wrap mt-2 gap-2"}>
                        <ArticleUpvoteComponent articleData={props.articleData} style={"HORIZONTAL"} />
                        <Link href={"#comments"} className='btn btn-secondary btn-xs btn-ghost btn-rounded flex gap-2'><FaRegCommentDots/>{(props.articleData.stats?.commentsCounter ? props.articleData.stats?.commentsCounter : "0") + " Comments"}</Link>
                        <Link href={openUrl} className='btn btn-secondary btn-xs btn-ghost btn-rounded flex gap-2' target={"_blank"}><GoLinkExternal/>{"Full Story"}</Link>
                    </div>
                </div>
            </div>
        )
    }

    function renderAdsBanner() {
        return (
            <div className='flex flex-col gap-2 w-full items-center'>
                <div className='hidden sm:flex w-full justify-center max-w-4xl'>
                    <GoogleAdsComponent slot={AdsSlots.RECTANGULAR_ADS} />
                </div>
            </div>
        )
    }

    return (
        <div className='flex flex-col items-center w-full pt-2 sm:pt-5 gap-2 my-5 px-2'>
            {renderHeader()}
            <div className='w-full my-4'>
                <ShareButtonsComponent />
            </div>
            <div className='flex flex-col w-full sm:pt-5 gap-2'>
                <div className='flex flex-col w-full'>
                    {renderArticleContent()}
                </div>
                <div id={"comments"} className='mt-5'>
                    <PostCommentsRootComponent
                        communityParentRefs={{parentCollectionId: SharedFirestoreCollectionDB.ArticlesDigestProject.ArticlePublished, parentDocId: props.articleData.uid!}}
                        attachedItem={{title: props.articleData.title, link: RootConfigs.BASE_URL + SharedRoutesUtils.getArticleDetails(props.articleData.uid!), thumbnailUrl: props.articleData.imageUrl}}/>
                </div>
            </div>
            <div className='flex flex-col items-center my-5 w-full'>
                <NewsletterSignupListBannerComponent title={"Subscribe our newsletter to receive our daily digested news"} />
            </div>
            <GoogleAdsComponent slot={AdsSlots.HORIZONTAL_ADS} />

            <AppStoreBanner />

            <div className='flex sm:hidden w-full'>
                {
                    props.latestArticles ? <ArticleDetailsLatestArticlesComponent latestArticles={props.latestArticles} /> : <></>
                }
            </div>


            {/* Popups */}
            <NewsletterPopupComponent />
        </div>
    )
}