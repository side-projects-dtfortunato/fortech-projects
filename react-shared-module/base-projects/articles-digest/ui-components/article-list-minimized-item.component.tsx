import {ArticleImportedModel} from "../data/model/ArticleImported.model";
import Link from "next/link";
import {SharedRoutesUtils} from "../../../utils/shared-routes.utils";
import CommonUtils from "../../../logic/commonutils";
import StatsPostItemComponent from "../../../ui-components/shared/StatsPostItem.component";
import Image from "next/image";

export default function ArticleListMinimizedItemComponent(props: {articleData: ArticleImportedModel}) {

    function renderInfos() {
        const sourceLabel: string = CommonUtils.capitalizeWords(props.articleData.source);
        const publishedTimeAgo: string = CommonUtils.getTimeAgo(props.articleData.createdAt!);

        return (
            <div className='flex flex-row items-center gap-2 text-xs text-slate-400'>
                <span>{publishedTimeAgo}</span>
                <span>•</span>
                <StatsPostItemComponent numComments={props.articleData.stats?.commentsCounter} numViews={props.articleData.stats?.viewsCounter}/>
            </div>
        )
    }

    function renderCategories() {
        let listTags: any[] = props.articleData.aiGeneratedContent?.tags ? props.articleData.aiGeneratedContent?.tags.map((cat, index) => {
            return (
                <div key={cat} className="badge badge-ghost text-slate-50 text-xs text-slate-600 hover:bg-orange-100">{cat}</div>
            )
        }) : [];

        return (
            <div className='flex flex-wrap gap-2'>
                {listTags}
            </div>
        )
    }

    return (
        <Link href={SharedRoutesUtils.getArticleDetails(props.articleData.uid!)} className={"flex flex-row items-center gap-3 max-w-max text-slate-800 hover:opacity-70"}>
            <Image className='object-cover w-28 h-20 rounded ' src={props.articleData.imageUrl!} alt={props.articleData.title} width={150} height={150} />
            <div className='flex flex-col gap-2'>
                <h3 className='font-bold text-sm'>{props.articleData.title.replaceAll("*", "")}</h3>
                {renderInfos()}
            </div>
        </Link>
    )

    /*return (
        <Link className="flex w-full max-w-sm object-cover h-44 rounded-2xl shadow-xl hover:opacity-70" style={{backgroundImage: `url('${props.articleData.imageUrl}')`}} href={SharedRoutesUtils.getArticleDetails(props.articleData.uid!)}>
            <div className='flex flex-col justify-end w-full h-full bg-black/50 p-2 rounded-2xl gap-2'>
                <h3 className='text-white font-bold text-xl'>{props.articleData.title.replaceAll("*", "")}</h3>
                {renderInfos()}
            </div>
        </Link>
    )*/
}