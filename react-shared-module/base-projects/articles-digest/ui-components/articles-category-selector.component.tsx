import React, {useEffect, useRef, useState} from 'react';
import RootConfigs from "../../../../../configs";
import Link from "next/link";
import {SharedRoutesUtils} from "../../../utils/shared-routes.utils";

export default function ArticlesCategorySelectorComponent(props: {
    categories: string[],
    selectedCategory?: string,
    articlesUrl: string,
}) {
    const scrollRef = useRef<HTMLDivElement>(null);
    const [isDragging, setIsDragging] = useState(false);
    const [startX, setStartX] = useState(0);
    const [scrollLeft, setScrollLeft] = useState(0);

    useEffect(() => {
        const handleWheel = (e: WheelEvent) => {
            if (scrollRef.current) {
                scrollRef.current.scrollLeft += e.deltaY;
            }
        };

        const current = scrollRef.current;
        if (current) {
            current.addEventListener('wheel', handleWheel, { passive: true });
        }

        return () => {
            if (current) {
                current.removeEventListener('wheel', handleWheel);
            }
        };
    }, []);

    const handleMouseDown = (e: React.MouseEvent) => {
        setIsDragging(true);
        setStartX(e.pageX - scrollRef.current!.offsetLeft);
        setScrollLeft(scrollRef.current!.scrollLeft);
    };

    const handleMouseUp = () => {
        setIsDragging(false);
    };

    const handleMouseMove = (e: React.MouseEvent) => {
        if (!isDragging) return;
        e.preventDefault();
        const x = e.pageX - scrollRef.current!.offsetLeft;
        const walk = (x - startX) * 2;
        scrollRef.current!.scrollLeft = scrollLeft - walk;
    };

    const allCategories = ["All", ...props.categories];
    const selectedCategory = props.selectedCategory || "All";

    return (
        <nav className={`w-full bg-${RootConfigs.THEME.PRIMARY_COLOR}-600 text-white`}>
            <div
                ref={scrollRef}
                className="container mx-auto px-4 py-2 overflow-x-auto scrollbar-hide"
                style={{
                    WebkitOverflowScrolling: 'touch',
                    scrollbarWidth: 'none',
                    msOverflowStyle: 'none',
                    cursor: isDragging ? 'grabbing' : 'grab'
                }}
                onMouseDown={handleMouseDown}
                onMouseUp={handleMouseUp}
                onMouseLeave={handleMouseUp}
                onMouseMove={handleMouseMove}
            >
                <ul className="flex space-x-6">
                    {allCategories.map((category) => (
                        <li
                            key={category}
                            className={`cursor-pointer whitespace-nowrap ${selectedCategory === category ? 'font-bold' : ''}`}>
                            <Link href={category === "All" ? props.articlesUrl : SharedRoutesUtils.getArticlesCategoryUrl(category)}>
                                {category}
                            </Link>
                        </li>
                    ))}
                </ul>
            </div>
        </nav>
    );
}