import {ArticleImportedModel} from "../data/model/ArticleImported.model";
import Link from "next/link";
import {SharedRoutesUtils} from "../../../utils/shared-routes.utils";
import LinesEllipsis from "react-lines-ellipsis";
import CommonUtils from "../../../logic/commonutils";
import ArticleUpvoteComponent from "./shared/article-upvote.component";
import StatsPostItemComponent from "../../../ui-components/shared/StatsPostItem.component";
import Image from "next/image";
import RootConfigs from "../../../../../configs";

export default function ArticlesDigestListItemComponent(props: {articleData: ArticleImportedModel, forceSmall?: boolean}) {

    function renderTitle() {
        return (
            <Link href={articleLink} className='text-lg text-start font-bold flex-wrap grow'>{props.articleData.title.replaceAll("*", "")}</Link>
        );
    }

    function renderCategories() {
        let listTags: any[] = [];
        if (props.articleData.aiGeneratedContent?.category) {
            // Add Category
            listTags.push(<Link href={SharedRoutesUtils.getArticlesCategoryUrl(props.articleData.aiGeneratedContent.category!)} key={"category"} className={`badge badge-ghost bg-${RootConfigs.THEME.PRIMARY_COLOR}-100 text-${RootConfigs.THEME.PRIMARY_COLOR}-700 text-sm hover:bg-${RootConfigs.THEME.PRIMARY_COLOR}-300`}>{props.articleData.aiGeneratedContent.category}</Link>)
        }

         listTags = listTags.concat(props.articleData.aiGeneratedContent?.tags ? props.articleData.aiGeneratedContent?.tags?.map((cat, index) => {
            return (
                <div key={cat} className="badge badge-ghost text-slate-600 text-xs text-slate-600 hover:bg-orange-100">{cat}</div>
            )
        }) : []);



        return (
            <div className='flex flex-wrap gap-2'>
                {listTags}
            </div>
        )
    }

    function renderDescription() {
        const descriptionText: string = props.articleData.aiGeneratedContent?.socialNetworkPost ? props.articleData.aiGeneratedContent?.socialNetworkPost : CommonUtils.cleanMarkdown(props.articleData.aiGeneratedContent?.summary!);
        return (
            <span className='font-light text-slate-800 text-sm'>
                <LinesEllipsis text={descriptionText.replaceAll("*", "")} maxLine={2}/>
            </span>
        )
    }

    function renderTopHeader() {
        const sourceLabel: string = CommonUtils.capitalizeWords(props.articleData.source);
        const publishedTimeAgo: string = CommonUtils.getTimeAgo(props.articleData.createdAt!);

        return (
            <div className='flex flex-row items-center gap-2 text-xs text-slate-400'>
                <span>{sourceLabel}</span>
                <span>•</span>
                <span>{publishedTimeAgo}</span>
                <span>•</span>
                <StatsPostItemComponent numComments={props.articleData.stats?.commentsCounter} numViews={props.articleData.stats?.viewsCounter}/>
            </div>
        )
    }

    const articleLink: string = SharedRoutesUtils.getArticleDetails(props.articleData.uid!);


    if (props.forceSmall) {
        return (
            <div className='flex flex-col text-black hover:text-orange-700 gap-5 items-stretch justify-stretch rounded-xl hover:bg-gradient-to-tr from-orange-50 to-transparent'>
                <Link href={articleLink}>
                    <Image className='flex h-full rounded w-full max-h-40 object-cover' width={500} height={500} src={props.articleData.imageUrl!} alt={props.articleData.title.replaceAll("*", "")} loading={"lazy"} />
                </Link>
                <div className='flex flex-col gap-4 grow items-start'>
                    <div className='flex flex-col gap-1'>
                        {renderTopHeader()}
                        <div className='flex flex-row gap-2'>
                            <div className='flex flex-col gap-1 grow'>
                                {renderTitle()}
                                {renderDescription()}
                            </div>
                            <ArticleUpvoteComponent articleData={props.articleData} style={"SQUARE"} />
                        </div>
                    </div>
                    {renderCategories()}
                </div>
            </div>
        )
    } else {
        return (
            <div className='flex flex-col sm:flex-row text-black hover:text-orange-700 gap-5 items-stretch justify-stretch rounded-xl hover:bg-gradient-to-tr from-orange-50 to-transparent'>
                <Link href={articleLink}>
                    <Image className='flex h-full rounded w-full w-48 h-40 sm:min-w-48 sm:w-48 max-h-40 sm:max-h-32 sm:mt-1 object-cover'
                           width={500} height={500} src={props.articleData.imageUrl!} alt={props.articleData.title.replaceAll("*", "")} loading={"lazy"} />
                </Link>
                <div className='flex flex-col gap-4 grow items-start'>
                    <div className='flex flex-col gap-1'>
                        {renderTopHeader()}
                        <div className='flex flex-row gap-2'>
                            <div className='flex flex-col gap-1 grow'>
                                {renderTitle()}
                                {renderDescription()}
                            </div>
                            <ArticleUpvoteComponent articleData={props.articleData} style={"SQUARE"} />
                        </div>
                    </div>
                    {renderCategories()}
                </div>
            </div>
        )
    }

}