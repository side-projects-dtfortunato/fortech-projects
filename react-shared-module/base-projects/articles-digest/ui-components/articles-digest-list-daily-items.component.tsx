import React from 'react';
import {DailyPublishedArticlesModel} from "../data/model/DailyPublishedArticles.model";
import CommonUtils from "../../../logic/commonutils";
import Link from "next/link";
import {SharedRoutesUtils} from "../../../utils/shared-routes.utils";
import InlineLabelSeparatorComponent from "../../../ui-components/shared/inline-label-separator.component";
import ArticlesDigestListItemComponent from "./articles-digest-list-item.component";
import {ArticleImportedModel, ArticleImportedUtils} from "../data/model/ArticleImported.model";
import {SortListTypes} from "../../../ui-components/shared/SharedSortListBy.component";
import GoogleAdsInFeedComponent from "../../../ui-components/ads/GoogleAdsInFeed.component";
import RootConfigs from "../../../../../configs";

const INFEED_AD_INTERVAL = 5; // Show an infeed ad after every 5 articles

export default function ArticlesDigestListDailyItemsComponent(props: {
    publishedDayArticles: DailyPublishedArticlesModel,
    linkable: boolean,
    sortBy?: SortListTypes
}) {
    const { publishedDayArticles, linkable, sortBy } = props;

    const getDateString = (): string => {
        const publishedDate = new Date(CommonUtils.convertDaytimestampToMillis(publishedDayArticles.publishedDayId));
        if (publishedDayArticles.publishedDayId === CommonUtils.getTodayDay()) {
            return "Today";
        } else if (publishedDayArticles.publishedDayId === (CommonUtils.getTodayDay() - 1)) {
            return "Yesterday";
        } else {
            return CommonUtils.getTimeAgo(publishedDate.getTime());
        }
    };

    const dateStr = getDateString();
    const titleTextSize: "medium" | "large" = dateStr === "Today" ? "large" : "medium";

    const renderTitle = () => {
        if (RootConfigs.META_CONFIGS["disableArticlesSeparator"] === true) {
            return <></>
        }
        const title = `${RootConfigs.SITE_TOPIC} Articles Published ${dateStr}`;
        const subtitle = `Top ${RootConfigs.SITE_TOPIC} articles picked and summarized`;

        if (linkable) {
            return (
                <Link href={SharedRoutesUtils.getPublishedArticlesList(publishedDayArticles.publishedDayId.toString())} className='hover:text-orange-400'>
                    <InlineLabelSeparatorComponent title={title} subtitle={subtitle} titleSize={titleTextSize} />
                </Link>
            );
        } else {
            return (
                <InlineLabelSeparatorComponent title={`${dateStr} ${RootConfigs.SITE_TOPIC} Articles Digested`} subtitle={subtitle} titleSize={titleTextSize} />
            );
        }
    };

    const sortListArticles = (article1: ArticleImportedModel, article2: ArticleImportedModel) => {
        if (article1.stats && article2.stats && sortBy) {
            switch (sortBy) {
                case SortListTypes.MOST_VIEWED:
                    return article2.stats.viewsCounter! - article1.stats.viewsCounter!;
                case SortListTypes.TRENDING:
                    return ArticleImportedUtils.getTrendingPts(article2) - ArticleImportedUtils.getTrendingPts(article1);
                case SortListTypes.MOST_RECENT:
                default:
                    return article2.createdAt! - article1.createdAt!;
            }
        } else {
            return article2.createdAt! - article1.createdAt!;
        }
    };

    const renderArticlesWithAds = () => {
        const sortedArticles = Object.values(publishedDayArticles.listArticles).sort(sortListArticles);
        return sortedArticles.reduce((acc: JSX.Element[], article, index) => {
            acc.push(
                <ArticlesDigestListItemComponent key={article.uid!} articleData={article} />
            );

            // Add Infeed ad after every INFEED_AD_INTERVAL articles
            if ((index + 1) % INFEED_AD_INTERVAL === 0 && index !== sortedArticles.length - 1) {
                acc.push(
                    <div key={`infeed-ad-${index}`} className="my-8">
                        <GoogleAdsInFeedComponent />
                    </div>
                );
            }

            return acc;
        }, []);
    };

    return (
        <div className='flex flex-col gap-8 px-5 pb-10 sm:py-5 sm:px-0'>
            {renderTitle()}
            <GoogleAdsInFeedComponent />
            {renderArticlesWithAds()}
        </div>
    );
}