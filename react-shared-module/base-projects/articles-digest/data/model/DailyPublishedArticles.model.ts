import {ArticleImportedModel} from "./ArticleImported.model";

export interface DailyPublishedArticlesModel {
    publishedDayId: number;
    listArticles: {
        [articleId: string]: ArticleImportedModel;
    }
    summary: string;
}