import {BaseModel} from "../../../../logic/shared-data/datamodel/base.model";
import {SharedPostStatsModel} from "../../../../logic/shared-data/datamodel/shared-post-stats.model";

export interface ArticleImportedModel extends BaseModel {
    title: string;
    description: string;
    source: string;
    imageUrl?: string;
    originalLink: string;
    publishedDate: number;
    fullContent?: string; // Extracted content from the article
    aiGeneratedContent?: {
        summary?: string;
        highlightsBullets?: string[];
        relevancy?: number;
        tags?: string[];
        hasSimilarArticles?: boolean;
        socialNetworkPost?: string;
        category?: string;
    }
    publishedDayId?: string;
    customHTMLContentElement?: string;
    upvotes: {
        [userId: string]: number,
    }; // User Ids: timestamp
    stats?: SharedPostStatsModel;
}

export interface ArticleGeneratedContent {
    summary: string;
    bullets: string[];
    relevancy: number;
    tags: string[];
}

export class ArticleImportedUtils {

    static getTotalUpvotes(article: ArticleImportedModel) {
        if (article.upvotes) {
            return Object.keys(article.upvotes).length;
        } else {
            return 0;
        }
    }

    static getTrendingPts(article: ArticleImportedModel) {
        let totalPts: number = 0;
        if (article.upvotes) {
            totalPts += Object.keys(article.upvotes).length * 0.5;
        }
        if (article.stats?.viewsCounter) {
            totalPts += article.stats?.viewsCounter * 0.5;
        }
        return totalPts;
    }

}