import {getLanguageLabel} from "../../../logic/language/language.helper";
import Image from "next/image";
import SignupLoginCallToActionContainerComponent from "../../shared/SignupLoginCallToActionContainer.component";
import RootConfigs from "../../../../../configs";
import {useAuthState} from "react-firebase-hooks/auth";
import {auth} from "../../../logic/shared-data/firebase.utils";
import AuthManager from "../../../logic/auth/auth.manager";

export default function SaasLandingPageTopComponent(props: {heroImgUrl?: string, loggedInComponent?: any, customComponent?: any}) {
    const [currentUser, isAuthLoading] = useAuthState(auth);

    function renderSignupLoginContainer() {
        if (AuthManager.isUserLogged() && props.loggedInComponent) {
            return props.loggedInComponent;
        } else {
            return (<SignupLoginCallToActionContainerComponent />);
        }

    }

    function renderHeroImage() {
        if (!props.heroImgUrl) {
            return <></>;
        } else {
            return <Image src={props.heroImgUrl} alt={RootConfigs.SITE_TITLE} width={500} height={400}/>
        }
    }

    function renderHeadlines() {
        return (
            <div className='flex flex-col max-w-lg'>
                <h1 className='text-accent text-center text-5xl mt-10 mb-1'>{getLanguageLabel("LANDING_PAGE_TOP_TITLE")}</h1>
                <h2 className='text-accent text-center text-5xl font-bold'>{getLanguageLabel("LANDING_PAGE_TOP_SUBTITLE")}</h2>
                <h4 className='text-center text-sm text-slate-500 my-8'>{getLanguageLabel("LANDING_PAGE_TOP_DESCRIPTION")}</h4>
            </div>
        )
    }

    return (
        <div className='flex flex-col justify-center items-center w-full'>
            <Image src={"/images/logo.png"} height={150} width={300} alt={RootConfigs.SITE_NAME} />
            <div className='flex flex-wrap justify-evenly gap-x-10'>
                {renderHeadlines()}
                {renderHeroImage()}
            </div>
            {renderSignupLoginContainer()}
            {props.customComponent ? props.customComponent : <></>}
        </div>
    )
}