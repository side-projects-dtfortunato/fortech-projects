export default function LandingPageSecondaryTitleComponent(props: {children: string, subtitle?: string}) {

    function renderSubtitle() {
        if (!props.subtitle) {
            return <></>
        } else {
            return <h3 className='text-sm sm:text-lg text-slate-400 text-center'>{props.subtitle}</h3>
        }
    }

    return (
        <div className='flex flex-col gap-2 w-full'>
            <div className='divider'></div>
            <h2 className='text-accent text-center font-bold text-4xl sm:text-5xl'>{props.children}</h2>
            {renderSubtitle()}
        </div>
    )
}