import {PostTypes} from "../../../utils/shared-ui-helper";
import {useEffect, useState} from "react";
import {useAlertMessageGlobalState} from "../../../logic/global-hooks/root-global-state";
import {PushPostDataModel} from "../../../logic/shared-data/datamodel/base-post-data.model";
import {SharedBackendApi} from "../../../logic/shared-data/sharedbackend.api";
import {APIResponseMessageUtils} from "../../../logic/global-hooks/APIResponseMessageUtils";
import {CATEGORIES_OPTIONS, UIHelper} from "../../../../ui-helper/UIHelper";
import PostFormShareLinkComponent from "../PostFormShareLink.component";
import PostFormWriteArticleComponent from "../PostFormWriteArticle.component";
import PostFormDiscussionComponent from "../PostFormDiscussion.component";
import {ApiResponse} from "../../../logic/shared-data/datamodel/base.model";
import {useRouter} from "next/router";
import {SharedRoutesUtils} from "../../../utils/shared-routes.utils";
import PostFormArticleBlocksComponent from "../PostFormArticleBlocks.component";
import {getAPIDocument} from "../../../logic/shared-data/firebase.utils";
import {SharedFirestoreCollectionDB} from "../../../logic/shared-data/datamodel/shared-firestore-collections";
import AuthManager from "../../../logic/auth/auth.manager";
import SpinnerComponent from "../../shared/Spinner.component";
import PostFormProductLaunchComponent from "../PostFormProductLaunch.component";


export default function PublishPostFormPageComponent(props: {defaultPostType?: PostTypes, showOnlyForm?: boolean}) {
    const router = useRouter();
    const [postTypeSelection, setPostTypeSelection] = useState<PostTypes>(props.defaultPostType ?props.defaultPostType : PostTypes.SHARE_LINK);
    const [alertMessageState, setAlertMessageState] = useAlertMessageGlobalState();
    const [thumbnailToUploadData, setThumbnailToUploadData] = useState<string|undefined>();
    const [draftDataSaved, setDraftDoc] = useState<any>();
    const [isLoading, setIsLoading] = useState(true);
    const [hasInitialized, setHasInitialized] = useState(false);

    useEffect(() => {

        if (!draftDataSaved && isLoading) {
            // Check if user has some draft document saved
            getAPIDocument(SharedFirestoreCollectionDB.DraftUserPosts, AuthManager.getUserId()!).then((response) => {
                if (response) {
                    setDraftDoc(response);
                }
                setIsLoading(false);
                setHasInitialized(true);
            }).catch((err) => {
                setIsLoading(false);
                setHasInitialized(true);
            });
        }

    }, [draftDataSaved])

    async function onSubmitClicked(postFormData: PushPostDataModel) {
        // API Call
        const apiResponse: ApiResponse<any> = await SharedBackendApi.pushPostData({
            postData: postFormData,
            isCreation: true,
            thumbnailBase: thumbnailToUploadData,
        });

        if (apiResponse && apiResponse.isSuccess) {
            // Redirect user
            await router.push(SharedRoutesUtils.getPostDetailsPageURL(postFormData.uid!));
        } else {
            // Handle errors
            setAlertMessageState(
                {
                    alertType: "ERROR",
                    message: APIResponseMessageUtils.getAPIResponseMessage(apiResponse),
                    display: true,
                }
            );
        }
    }

    function onTabSelected(tabPostType: PostTypes) {
        setPostTypeSelection(tabPostType);
    }

    function renderTab(tabPostType: PostTypes) {
        let tabLabel: string = UIHelper.getPostFormTypeLabel(tabPostType);

        let suffixClass: string = "";
        if (tabPostType === postTypeSelection) {
            suffixClass = "tab-active"
        }
        return (
            <button className={"tab tab-lifted " + suffixClass} onClick={() => onTabSelected(tabPostType)}>{tabLabel}</button>
        )
    }

    function onThumbnailToUpload(thumbnailBase64: string) {
        // Upload in the moment of creating the post
        setThumbnailToUploadData(thumbnailBase64);
    }

    function renderFormContainer() {
        switch (postTypeSelection) {
            case PostTypes.SHARE_LINK:
                return (<PostFormShareLinkComponent postType={postTypeSelection} onSubmitClicked={onSubmitClicked}
                                                    listCategories={CATEGORIES_OPTIONS}/>);
            case PostTypes.WRITE_ARTICLE:
                return (<PostFormWriteArticleComponent postType={postTypeSelection} onSubmitClicked={onSubmitClicked} onThumbnailBase64={onThumbnailToUpload}
                                                    listCategories={CATEGORIES_OPTIONS}/>);
            case PostTypes.ARTICLE_BLOCKS:
                let draftData: any;
                if (draftDataSaved && Object.keys(draftDataSaved).includes(PostTypes.ARTICLE_BLOCKS)) {
                    draftData = draftDataSaved[PostTypes.ARTICLE_BLOCKS];
                }

                return (<PostFormArticleBlocksComponent postType={postTypeSelection} onSubmitClicked={onSubmitClicked}
                                                        defaultArticleData={draftData}
                                                    listCategories={CATEGORIES_OPTIONS}/>);

            case PostTypes.PRODUCT:
                return (<PostFormProductLaunchComponent postType={postTypeSelection} onSubmitClicked={onSubmitClicked}
                                                       listCategories={CATEGORIES_OPTIONS}/>);
            case PostTypes.DISCUSSION:
            default:
                return (<PostFormDiscussionComponent postType={postTypeSelection} onSubmitClicked={onSubmitClicked}
                                                    listCategories={CATEGORIES_OPTIONS}/>);
        }
        return (<></>);
    }

    function renderListTabs() {
        let listTabs: any[] = [];

        return UIHelper.getLandingPagePostTypes().map((postType) => {
            return renderTab(postType);
        });
    }

    if (!hasInitialized) {
        return (<></>);
    } else if (props.showOnlyForm) {
        return (
            <div className='list-item-bg drop-shadow px-4 py-3'>
                {renderFormContainer()}
            </div>
        )
    } else {
        return (
            <div className='flex flex-col'>
                <div className="tabs">
                    {renderListTabs()}
                </div>
                {
                    isLoading ? <SpinnerComponent /> :
                        <div className='list-item-bg drop-shadow px-4 py-3'>
                            {renderFormContainer()}
                        </div>
                }
            </div>
        )
    }
}
