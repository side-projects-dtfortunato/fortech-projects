import {initPushDataState, PushPostDataModel} from "../../logic/shared-data/datamodel/base-post-data.model";
import InputTextComponent from "../form/InputText.component";
import {useState} from "react";
import SpinnerComponent from "../shared/Spinner.component";
import {FormOption} from "../form/RadioButton.component";
import InputTagsFieldComponent from "../form/InputTagsField.component";
import {FormValidation} from "../../logic/shared-data/datamodel/shared-model-data";
import {useRouter} from "next/router";
import DropdownFormComponent from "../form/DropdownForm.component";
import {useIsLoadingGlobalState} from "../../logic/global-hooks/root-global-state";
import InputTextAreaComponent from "../form/InputTextArea.component";
import {PostTopicDiscussionDetailsModel} from "../../logic/shared-data/datamodel/posts-model/post-topic-discussion";

const IS_AUTHOR_RADIO_BTN_OPTIONS: any = {
    YES: {label: "Yes", optionId: "YES"},
    NO: {label: "No", optionId: "NO"},
}
const MAX_TAGS = 5;


export default function PostFormDiscussionComponent(props: {
    postType: string,
    onSubmitClicked: (postData: PushPostDataModel<any, PostTopicDiscussionDetailsModel>) => Promise<void>,
    listCategories?: {[optionId: string]: FormOption}, isEditing?: boolean}) {
    const route = useRouter();
    const [formPostData, setFormPostData] = useState<PushPostDataModel<any, PostTopicDiscussionDetailsModel>>(initPushDataState<any, PostTopicDiscussionDetailsModel>(props.postType));
    const [isLoading, setIsLoadingState] = useIsLoadingGlobalState();
    const [formValidation, setFormValidation] = useState<FormValidation>({isValid: false});

    // Check if the form validation has changed
    if (formValidation.isValid !== isFormDataValidToSubmit(formPostData).isValid) {
        setFormValidation(isFormDataValidToSubmit(formPostData));
    }

    async function onTitleField(text: string) {
        setFormPostData({
            ...formPostData,
            title: text,
        });
    }
    async function onSummaryFieldChanged(text: string) {
        let summary: string = text;
        if (summary.length > 280) {
            summary = summary.substring(0, 280);
        }
        setFormPostData({
            ...formPostData,
            detailsPostData: {
                bodyText: text,
            },
            summary: summary,
        });
    }

    // Tags
    function onTagsChanged(tags: string[]) {
        setFormPostData({
            ...formPostData,
            tags: tags,
        });
    }

    function onCategorySelected(optionId: string) {
        // Option selected
        setFormPostData({
            ...formPostData,
            category: optionId,
        });
    }

    // Category (if any)
    const renderCategoryDropdown = props.listCategories ? (<div>
            <DropdownFormComponent label={"Select a relevant category:"} defaultLabel={"Select a topic"} options={props.listCategories} onOptionSelected={onCategorySelected}  disabled={props.isEditing}  />
        </div>) : (<></>);
    function onCancel() {
        route.back();
    }

    async function onSubmitClicked() {
        setIsLoadingState(true);
        await props.onSubmitClicked(formPostData);
        setIsLoadingState(false);
    }

    return (
        <div className='flex flex-col gap-5'>
            {isLoading ? (<div className='flex justify-center'>
                <SpinnerComponent />
            </div>) : (<></>)}
            <InputTextComponent topLabel={"Title of the topic"} hint={"Enter a suggestive title"} disabled={isLoading} onChanged={onTitleField} inputType={"text"} defaultValue={formPostData.title} value={formPostData.title} maxLength={128}/>
            <InputTextAreaComponent topLabel={"Details of topic discussion"} hint={"Write the body of this discussion (max: 3000 chars)"} onChanged={onSummaryFieldChanged} inputType={"text"} defaultValue={formPostData.summary} disabled={isLoading} maxLength={3000} />
            <InputTagsFieldComponent defaultTags={formPostData.tags} onTagsChanged={onTagsChanged} label={"Set some tags to describe this story (max: " + MAX_TAGS + ")"} placeholder={"Press enter to add the tag..."}  disabled={props.isEditing}  />
            {renderCategoryDropdown}
            <div className='flex justify-end gap-2'>
                <button className='btn btn-ghost' onClick={onCancel} disabled={isLoading}>Cancel post</button>
                <button className={'btn' + (isLoading ? " loading" : "")} onClick={onSubmitClicked} disabled={!formValidation.isValid || isLoading}>Submit</button>
            </div>
        </div>
    )
}


function isFormDataValidToSubmit(postFormData: PushPostDataModel<any, PostTopicDiscussionDetailsModel>): FormValidation {
    if (!postFormData.title || postFormData.title.length < 5) {
        return  {
            isValid: false,
            errorMessage: "Title is too short",
        };
    }

    if (!postFormData.summary || postFormData.summary.length < 5) {
        return  {
            isValid: false,
            errorMessage: "Body is too short",
        };
    }

    if (!postFormData.category) {
        return  {
            isValid: false,
            errorMessage: "You should select a category",
        };
    }

    return {
        isValid: true,
    };
}