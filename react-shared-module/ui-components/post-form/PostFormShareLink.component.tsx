import {
    initPushDataState,
    PostListItemsModel,
    PushPostDataModel
} from "../../logic/shared-data/datamodel/base-post-data.model";
import InputTextComponent from "../form/InputText.component";
import {useState} from "react";
import {
    MetaData,
    PostFormShareLinkShortenModel,
    PostFormShareLinksUtils
} from "../../logic/shared-data/datamodel/posts-model/post-share-link";
import CommonUtils from "../../logic/commonutils";
import {PostLinkPreviewComponent} from "../shared/PostLinkPreview.component";
import SpinnerComponent from "../shared/Spinner.component";
import AlertMessageComponent from "../shared/AlertMessage.component";
import RadioButtonComponent, {FormOption} from "../form/RadioButton.component";
import InputTagsFieldComponent from "../form/InputTagsField.component";
import {FormValidation} from "../../logic/shared-data/datamodel/shared-model-data";
import {useRouter} from "next/router";
import DropdownFormComponent from "../form/DropdownForm.component";
import {useAlertMessageGlobalState, useIsLoadingGlobalState} from "../../logic/global-hooks/root-global-state";
import InputTextAreaComponent from "../form/InputTextArea.component";
import PostFormListItemsComponent from "./shared/PostFormListItems.component";

const IS_AUTHOR_RADIO_BTN_OPTIONS: any = {
    YES: {label: "Yes", optionId: "YES"},
    NO: {label: "No", optionId: "NO"},
}
const MAX_TAGS = 5;


export default function PostFormShareLinkComponent(props: {
    postType: string,
    onSubmitClicked: (postData: PushPostDataModel<PostFormShareLinkShortenModel>) => Promise<void>,
    listCategories?: {[optionId: string]: FormOption}, isEditing?: boolean}) {
    const route = useRouter();
    const [formPostData, setFormPostData] = useState<PushPostDataModel<PostFormShareLinkShortenModel>>(initPushDataState<PostFormShareLinkShortenModel>(props.postType));
    const [isLoading, setIsLoadingState] = useIsLoadingGlobalState();
    const [alertMessageState, setAlertMessageState] = useAlertMessageGlobalState();
    const [timeoutHandler, setTimeoutHandle] = useState<any>();
    const [formValidation, setFormValidation] = useState<FormValidation>({isValid: false});

    // Check if the form validation has changed
    if (formValidation.isValid !== PostFormShareLinksUtils.isFormDataValidToSubmit(formPostData).isValid) {
        setFormValidation(PostFormShareLinksUtils.isFormDataValidToSubmit(formPostData));
    }

    async function onTitleField(text: string) {
        setFormPostData({
            ...formPostData,
            title: text,
        });
    }
    async function onSummaryFieldChanged(text: string) {
        setFormPostData({
            ...formPostData,
            summary: text,
        });
    }

    // Link
    async function onLinkChanged(text: string) {
        let metaData: MetaData | undefined;

        // First clean the current metadata
        setFormPostData({
            ...formPostData,
            shortenPostData: {
                link: text,
                metaData,
            },
        });

        if (timeoutHandler) {
            clearInterval(timeoutHandler);
        }

        if (CommonUtils.isValidHttpUrl(text)) {
            setTimeoutHandle(setTimeout(async () => {
                setIsLoadingState(true);
                metaData = await CommonUtils.getUrlMetadata(text);
                setIsLoadingState(false);
                let title: string | undefined = formPostData.title;

                if (metaData && metaData["title"]) {
                    title = metaData["title"];
                }

                let summary: string | undefined = formPostData.summary;
                if (metaData && metaData["description"]) {
                    summary = metaData["description"];
                }

                let thumbnailUrl: string | undefined = formPostData.thumbnailUrl;
                if (metaData && metaData["image"]) {
                    thumbnailUrl = metaData["image"];
                }

                setFormPostData({
                    ...formPostData,
                    shortenPostData: {
                        link: text,
                        metaData,
                    },
                    title,
                    summary,
                    thumbnailUrl
                });
                clearInterval(timeoutHandler);
            }, 1500, {}));
        }
    }

    // You are the author?
    function onIsTheAuthor(optionId: string) {
        setFormPostData({
            ...formPostData,
            shortenPostData: {
                ...formPostData.shortenPostData,
                isTheAuthor: optionId === IS_AUTHOR_RADIO_BTN_OPTIONS.YES.optionId,
            },
        });
    }

    // Tags
    function onTagsChanged(tags: string[]) {
        setFormPostData({
            ...formPostData,
            tags: tags,
        });
    }

    function onCategorySelected(optionId: string) {
        // Option selected
        setFormPostData({
            ...formPostData,
            category: optionId,
        });
    }

    async function onListSummaryItemsChanged(listItems?: PostListItemsModel) {
        // Save them on post data
        setFormPostData({
            ...formPostData,
            detailsPostData: {
                ...formPostData.detailsPostData,
                postSummaryListItems: listItems,
            },
        });
    }

    // Category (if any)
    const renderCategoryDropdown = props.listCategories ? (<div>
            <DropdownFormComponent label={"Select a relevant category:"} defaultLabel={"Select a topic (optional)"} options={props.listCategories} onOptionSelected={onCategorySelected} disabled={props.isEditing} />
        </div>) : (<></>)

    function renderPreview() {
        if (formPostData && formPostData.shortenPostData.metaData && formPostData.shortenPostData.metaData.title) {
            return (
                <div className='flex flex-col gap-1'>
                    <PostLinkPreviewComponent linkUrl={formPostData.shortenPostData.link} metadata={formPostData.shortenPostData.metaData} />
                </div>
            );
        } else if (!isLoading && formPostData.shortenPostData.metaData && formPostData.shortenPostData.link && CommonUtils.isValidHttpUrl(formPostData.shortenPostData.link)) {
            return (
                <AlertMessageComponent type={"ERROR"} messageText={"Invalid link. Please, copy and paste a valid link to be published..."} />
            )
        } else {
            return (<></>);
        }
    }

    function onCancel() {
        route.back();
    }


    async function onSubmitClicked() {
        setIsLoadingState(true);
        await props.onSubmitClicked(formPostData);
        setIsLoadingState(false);
    }

    return (
        <div className='flex flex-col gap-5'>
            <InputTextComponent topLabel={"Share an article link"} hint={"Enter the link to your story"} disabled={isLoading} onChanged={onLinkChanged} inputType={"url"} />
            {renderPreview()}
            {isLoading ? (<div className='flex justify-center'>
                <SpinnerComponent />
            </div>) : (<></>)}
            <InputTextComponent topLabel={"Title of the article"} hint={"Enter a suggestive title"} disabled={isLoading} onChanged={onTitleField} inputType={"text"} defaultValue={formPostData.title} value={formPostData.title} maxLength={128}/>
            <InputTextAreaComponent topLabel={"Summary of article"} hint={"Enter a short summary to attract readers (Max. length: 280 characters)"} onChanged={onSummaryFieldChanged} inputType={"text"} defaultValue={formPostData.summary} disabled={isLoading} maxLength={280} />
            <PostFormListItemsComponent onListChanged={onListSummaryItemsChanged} />
            <RadioButtonComponent label={"Are you the author?"} options={IS_AUTHOR_RADIO_BTN_OPTIONS} setCurrentSelection={onIsTheAuthor} currentSelection={formPostData.shortenPostData.isTheAuthor ? IS_AUTHOR_RADIO_BTN_OPTIONS.YES.optionId : IS_AUTHOR_RADIO_BTN_OPTIONS.NO.optionId} />
            <InputTagsFieldComponent defaultTags={formPostData.tags} disabled={props.isEditing}  onTagsChanged={onTagsChanged} label={"Set some tags to describe this story (max: " + MAX_TAGS + ")"} placeholder={"Press enter to add the tag..."} />
            {renderCategoryDropdown}
            <div className='flex justify-end gap-2'>
                <button className='btn btn-ghost' onClick={onCancel} disabled={isLoading}>Cancel post</button>
                <button className={'btn' + (isLoading ? " loading" : "")} onClick={onSubmitClicked} disabled={!formValidation.isValid || isLoading}>Submit</button>
            </div>
        </div>
    )
}