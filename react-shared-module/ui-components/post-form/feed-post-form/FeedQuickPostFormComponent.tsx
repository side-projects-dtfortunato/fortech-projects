import AuthManager from "../../../logic/auth/auth.manager";
import LandingLoginSignupComponent from "../../landing-page-content/shared/LandingLoginSignup.component";
import {getLanguageLabel} from "../../../logic/language/language.helper";
import {useAuthState} from "react-firebase-hooks/auth";
import {auth} from "../../../logic/shared-data/firebase.utils";
import {
    RootGlobalStateActions,
    useAlertMessageGlobalState,
    useCentralUserDataState
} from "../../../logic/global-hooks/root-global-state";
import UserPictureAvatarComponent from "../../shared/UserPictureAvatar.component";
import {useState} from "react";
import {PushPostDataModel} from "../../../logic/shared-data/datamodel/base-post-data.model";
import {PostTypes} from "../../../utils/shared-ui-helper";
import CommonUtils from "../../../logic/commonutils";
import {SharedBackendApi} from "../../../logic/shared-data/sharedbackend.api";
import JoinTelegramButtonComponent from "../../shared/JoinTelegramButton.component";

export default function FeedQuickPostFormComponent(props: {bottomComponents?: any}) {
    const [currentUser, isAuthLoading] = useAuthState(auth);
    const [centralUserData, setCentralUserData] = useCentralUserDataState();
    const [alertMessage, setAlertMessage] = useAlertMessageGlobalState();
    const [isFormEditing, setIsFormEditing] = useState(false);
    const [textEditing, setTextEditing] = useState("");
    const [isLoading, setIsLoading] = useState(false);

    function onTextChanged(textChanged: string) {
        setTextEditing(textChanged);
    }

    function onPublishQuickPostClicked() {
        setIsLoading(true);

        // Generate Push Post Data
        const pushPostData: PushPostDataModel<any, {body: string}> = {
            postType: PostTypes.QUICK_ARTICLE,
            shortenPostData: "",
            title: CommonUtils.shortenText(textEditing, 80),
            summary: CommonUtils.shortenText(textEditing, 300),
            tags: CommonUtils.extractHashtags(textEditing),
            detailsPostData: {
                body: textEditing,
            },
            category: "",
        }

        // Backend request
        SharedBackendApi.pushPostData({
            isCreation: true,
            postData: pushPostData,
        }).then((apiResponse) => {
            if (apiResponse.isSuccess) {
                setTextEditing("");
                setIsLoading(false);
                setIsFormEditing(false);
                RootGlobalStateActions.displayAlertMessage({
                    alertType: "SUCCESS",
                    message: "Published successfuly",
                    setAlertMessageState: setAlertMessage,
                });
            } else {
                RootGlobalStateActions.displayAPIResponseError({
                    apiResponse: apiResponse,
                    setAlertMessageState: setAlertMessage,
                });
            }
        }).catch((error) => {
            console.error(error);
            RootGlobalStateActions.displayAlertMessage({
                alertType: "ERROR",
                message: "Something failed. Please, try again...",
                setAlertMessageState: setAlertMessage,
            });
            setIsLoading(false);
        });
    }

    function renderFormNotEditing() {
        return (
            <div className='flex flex-col gap-1'>
                <div className='flex flex-row gap-4 items-start'>
                    <UserPictureAvatarComponent width={"w-8"} pictureUrl={centralUserData?.publicProfile?.userPicture?.fileUrl} />
                    <div className='flex flex-col items-start w-full gap-2'>
                        <span className='input input-bordered w-full py-2 px-4 pb-10 text-slate-400 text-sm hover:bg-slate-100 cursor-pointer' onClick={() => setIsFormEditing(true)}>Publish something...</span>
                    </div>
                </div>
                {props.bottomComponents ? props.bottomComponents : <></>}
            </div>
        )
    }

    function renderFormEditing() {
        return (
            <div className='flex flex-col gap-4'>
                <div className='flex flex-row gap-4 items-start'>
                    <UserPictureAvatarComponent width={"w-8"} pictureUrl={centralUserData?.publicProfile?.userPicture?.fileUrl} />
                    <div className='flex flex-col items-start w-full gap-2'>
                        <textarea
                            autoFocus={true}
                            onChange={(event) => onTextChanged(event.target.value)}
                            placeholder={getLanguageLabel("LANDING_PAGE_QUICK_POST_PLACEHOLDER")}
                            className="textarea textarea-bordered textarea-lg w-full"></textarea>
                    </div>
                </div>
                {renderEditingButtons()}
            </div>
        )
    }

    function renderEditingButtons() {
        const isValidToPublish: boolean = (textEditing !== undefined && textEditing.length > 5);

        return (
            <div className='flex flex-row w-full justify-end gap-3'>
                <button className={'btn btn-ghost'} disabled={isLoading} onClick={() => {setIsFormEditing(false)}}>
                    {getLanguageLabel("GENERIC_CANCEL_LABEL")}
                </button>
                <button className={'btn btn-primary ' + (isLoading ? "loading" : "")} disabled={!isValidToPublish || isLoading} onClick={onPublishQuickPostClicked}>
                    {getLanguageLabel("GENERIC_CANCEL_SUBMIT")}
                </button>
            </div>
        )
    }

    if (!CommonUtils.isClientSide()) {
        return (<></>);
    } else if (isAuthLoading && !AuthManager.isUserLogged() && !centralUserData) {
        return (<></>);
    } else if (!AuthManager.isUserLogged() && !centralUserData) {
        return (
            <div>
                <LandingLoginSignupComponent />
            </div>
        )
    } else {
        return (
            <div className='list-item-bg drop-shadow p-5 flex flex-col gap-2'>
                <h3 className='custom'>{getLanguageLabel("LANDING_PAGE_SUBMIT_POST_TITLE")}</h3>
                {isFormEditing ? renderFormEditing() : renderFormNotEditing()}
                <JoinTelegramButtonComponent/>
            </div>
        )
    }
}