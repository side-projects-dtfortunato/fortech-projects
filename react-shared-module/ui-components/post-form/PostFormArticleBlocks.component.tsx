import {initPushDataState, PushPostDataModel} from "../../logic/shared-data/datamodel/base-post-data.model";
import {FormOption} from "../form/RadioButton.component";
import {PostFormArticleBlocksDetails} from "../../logic/shared-data/datamodel/posts-model/post-article-blocks";
import SpinnerComponent from "../shared/Spinner.component";
import InputTextComponent from "../form/InputText.component";
import InputTagsFieldComponent from "../form/InputTagsField.component";
import {useRouter} from "next/router";
import {useState} from "react";
import {useIsLoadingGlobalState} from "../../logic/global-hooks/root-global-state";
import {FormValidation} from "../../logic/shared-data/datamodel/shared-model-data";
import DropdownFormComponent from "../form/DropdownForm.component";
import {InputRichEditorJSComponent} from "../form/InputRichEditorJS.component";
import PostFormSaveDraftButtonComponent from "./shared/PostFormSaveDraftButton.component";
import {SharedBackendApi} from "../../logic/shared-data/sharedbackend.api";
import {SharedFirestoreCollectionDB} from "../../logic/shared-data/datamodel/shared-firestore-collections";
import AuthManager from "../../logic/auth/auth.manager";


const MAX_TAGS = 5;

export default function PostFormArticleBlocksComponent(props: {
    postType: string,
    defaultArticleData?: PushPostDataModel,
    onSubmitClicked: (postData: PushPostDataModel<any, PostFormArticleBlocksDetails>) => Promise<void>,
    listCategories?: {[optionId: string]: FormOption}, isEditing?: boolean}) {

    const initialFormState = props.defaultArticleData !== undefined ? props.defaultArticleData : initPushDataState<any, PostFormArticleBlocksDetails>(props.postType);

    const route = useRouter();
    const [formPostData, setFormPostData] = useState<PushPostDataModel<any, PostFormArticleBlocksDetails>>(initialFormState);
    const [isLoading, setIsLoadingState] = useIsLoadingGlobalState();
    const [formValidation, setFormValidation] = useState<FormValidation>({isValid: false});
    const [thumbnailTempUrl, setThumbnailTempUrl] = useState<string | null>(null);
    const [editorJSData, setEditorJSData] = useState(props.defaultArticleData ? (props.defaultArticleData.detailsPostData as PostFormArticleBlocksDetails).editorJSData : null);

    async function onTitleField(text: string) {
        setFormPostData({
            ...formPostData,
            title: text,
        });
    }


    // Tags
    function onTagsChanged(tags: string[]) {
        setFormPostData({
            ...formPostData,
            tags: tags,
        });
    }

    // Article content
    function onSummaryChanged(text: string) {
        setFormPostData({
            ...formPostData,
            summary: text,
        });
    }

    function onCategorySelected(optionId: string) {
        // Option selected
        setFormPostData({
            ...formPostData,
            category: optionId,
        });
    }

    // Category (if any)
    const renderCategoryDropdown = props.listCategories && Object.keys(props.listCategories).length > 0 ? (<div>
        <DropdownFormComponent label={"Select a relevant category:"} defaultLabel={"Select a topic"} options={props.listCategories} onOptionSelected={onCategorySelected} disabled={props.isEditing} />
    </div>) : (<></>);
    function onCancel() {
        SharedBackendApi.requestDocumentChanges({
            collectionName: SharedFirestoreCollectionDB.DraftUserPosts,
            docId: AuthManager.getUserId(),
            methodType: "DELETE",
        }).then((response) => {
            route.back();
        });
    }

    async function onSubmitClicked() {
        setIsLoadingState(true);
        await props.onSubmitClicked({
            ...formPostData,
            detailsPostData: {
                ...formPostData.detailsPostData,
                editorJSData: editorJSData!,
            },
        });
        setIsLoadingState(false);
    }

    function onArticleBodyChanged(data: any) {
        setEditorJSData(data);
    }

    // Force to update form with blocks data
    if (editorJSData) {
        formPostData.detailsPostData!.editorJSData = editorJSData;
    }

    function isFormValid(): boolean {
        if (formPostData.title && formPostData.summary && formPostData.detailsPostData?.editorJSData?.blocks && editorJSData && editorJSData.blocks.length > 0) {
            return true;
        } else {
            return false;
        }
    }

    return (
        <div className='flex flex-col gap-5'>
            {isLoading ? (<div className='flex justify-center'>
                <SpinnerComponent />
            </div>) : (<></>)}
            {thumbnailTempUrl && thumbnailTempUrl.length > 0 ? (
                <div className={"flex w-full"} style={{minWidth: 125, height:240}}>
                    <div className='self-stretch rounded drop-shadow border bg-white' style={{backgroundImage: "Url(" + thumbnailTempUrl + ")", backgroundSize: "cover", backgroundPosition: "center", width: "100%", height: "100%"}}/>
                </div>) : (<></>)}
            <InputTextComponent topLabel={"Title of the article"} hint={"Enter a suggestive title"} disabled={isLoading} onChanged={onTitleField} inputType={"text"} defaultValue={formPostData.title} maxLength={128}/>
            <InputTextComponent topLabel={"Short summary about the article"} hint={"Enter a suggestive summary, will be displayed in the post list"} disabled={isLoading} onChanged={onSummaryChanged} inputType={"text"} defaultValue={formPostData.summary} maxLength={280}/>

            <div className='flex w-full justify-center'>
                <article className='prose list-item-bg drop-shadow bg-slate-50 p-3 w-full max-w-none'>
                    <InputRichEditorJSComponent onDataChanged={onArticleBodyChanged} initialData={formPostData.detailsPostData?.editorJSData}/>
                </article>
            </div>

            <InputTagsFieldComponent defaultTags={formPostData.tags} onTagsChanged={onTagsChanged} label={"Set some tags to describe this story (max: " + MAX_TAGS + ")"} placeholder={"Press enter to add the tag..."} disabled={props.isEditing} />
            {renderCategoryDropdown}
            <div className='flex justify-end gap-2'>
                <button className='btn btn-ghost' onClick={onCancel} disabled={isLoading}>Cancel post</button>
                <PostFormSaveDraftButtonComponent postData={formPostData} />
                <button className={'btn btn-success ' + (isLoading ? " loading" : "")} onClick={onSubmitClicked} disabled={!isFormValid() || isLoading}>Submit</button>
            </div>
        </div>
    )
}