import {
    initPushDataState,
    PostListItemsModel,
    PushPostDataModel
} from "../../logic/shared-data/datamodel/base-post-data.model";
import InputTextComponent from "../form/InputText.component";
import {useState} from "react";
import SpinnerComponent from "../shared/Spinner.component";
import {FormOption} from "../form/RadioButton.component";
import InputTagsFieldComponent from "../form/InputTagsField.component";
import {FormValidation} from "../../logic/shared-data/datamodel/shared-model-data";
import {useRouter} from "next/router";
import DropdownFormComponent from "../form/DropdownForm.component";
import {useIsLoadingGlobalState} from "../../logic/global-hooks/root-global-state";
import InputTextAreaComponent from "../form/InputTextArea.component";
import {PostTopicDiscussionDetailsModel} from "../../logic/shared-data/datamodel/posts-model/post-topic-discussion";
import {PostListOfDetailsModel} from "../../logic/shared-data/datamodel/posts-model/post-list-of";
import PostFormListItemsComponent from "./shared/PostFormListItems.component";
import {getLanguageLabel} from "../../logic/language/language.helper";

const IS_AUTHOR_RADIO_BTN_OPTIONS: any = {
    YES: {label: "Yes", optionId: "YES"},
    NO: {label: "No", optionId: "NO"},
}
const MAX_TAGS = 5;


export default function PostFormListOfComponent(props: {
    postType: string,
    onSubmitClicked: (postData: PushPostDataModel<any, PostListOfDetailsModel>) => Promise<void>,
    listCategories?: {[optionId: string]: FormOption}}) {
    const route = useRouter();
    const [formPostData, setFormPostData] = useState<PushPostDataModel<any, PostListOfDetailsModel>>(initPushDataState<any, PostListOfDetailsModel>(props.postType));
    const [isLoading, setIsLoadingState] = useIsLoadingGlobalState();
    const [formValidation, setFormValidation] = useState<FormValidation>({isValid: false});

    // Check if the form validation has changed
    if (formValidation.isValid !== isFormDataValidToSubmit(formPostData).isValid) {
        setFormValidation(isFormDataValidToSubmit(formPostData));
    }

    async function onTitleField(text: string) {
        setFormPostData({
            ...formPostData,
            title: text,
        });
    }
    async function onSummaryFieldChanged(text: string) {
        let summary: string = text;
        if (summary.length > 280) {
            summary = summary.substring(0, 280);
        }
        setFormPostData({
            ...formPostData,
            detailsPostData: {
                bodyText: text,
            },
            summary: summary,
        });
    }

    // Tags
    function onTagsChanged(tags: string[]) {
        setFormPostData({
            ...formPostData,
            tags: tags,
        });
    }

    function onCategorySelected(optionId: string) {
        // Option selected
        setFormPostData({
            ...formPostData,
            category: optionId,
        });
    }

    // Category (if any)
    const renderCategoryDropdown = props.listCategories ? (<div>
            <DropdownFormComponent label={"Select a relevant category:"} defaultLabel={"Select a topic"} options={props.listCategories} onOptionSelected={onCategorySelected} />
        </div>) : (<></>);
    function onCancel() {
        route.back();
    }


    async function onListSummaryItemsChanged(listItems?: PostListItemsModel) {
        // Search for a thumbnail
        let thumbnailUrl: string | undefined;
        if (listItems) {
            listItems.listItems
                .filter((item) => item.imageUrl && item.imageUrl.length > 0)
                .forEach((listItem) => {
                    if (!thumbnailUrl) {
                        thumbnailUrl = listItem.imageUrl;
                    }
                });
        }

        // Save them on post data
        setFormPostData({
            ...formPostData,
            thumbnailUrl: thumbnailUrl,
            detailsPostData: {
                ...formPostData.detailsPostData!,
                postSummaryListItems: listItems,
            },
        });
    }

    async function onSubmitClicked() {
        setIsLoadingState(true);
        await props.onSubmitClicked(formPostData);
        setIsLoadingState(false);
    }

    return (
        <div className='flex flex-col gap-5'>
            {isLoading ? (<div className='flex justify-center'>
                <SpinnerComponent />
            </div>) : (<></>)}
            <InputTextComponent topLabel={getLanguageLabel("POST_FORM_LIST_OF_TITLE")} hint={getLanguageLabel("POST_FORM_LIST_OF_TITLE_HINT")} disabled={isLoading} onChanged={onTitleField} inputType={"text"} defaultValue={formPostData.title} value={formPostData.title} maxLength={128}/>
            <InputTextAreaComponent topLabel={getLanguageLabel("POST_FORM_LIST_OF_INTRO")} hint={getLanguageLabel("POST_FORM_LIST_OF_INTRO_HINT")} onChanged={onSummaryFieldChanged} inputType={"text"} defaultValue={formPostData.summary} disabled={isLoading} maxLength={3000} />
            <PostFormListItemsComponent onListChanged={onListSummaryItemsChanged} />
            <InputTagsFieldComponent defaultTags={formPostData.tags} onTagsChanged={onTagsChanged} label={"Set some tags to describe this story (max: " + MAX_TAGS + ")"} placeholder={"Press enter to add the tag..."} />
            {renderCategoryDropdown}
            <div className='flex justify-end gap-2'>
                <button className='btn btn-ghost' onClick={onCancel} disabled={isLoading}>Cancel post</button>
                <button className={'btn' + (isLoading ? " loading" : "")} onClick={onSubmitClicked} disabled={!formValidation.isValid || isLoading}>Submit</button>
            </div>
        </div>
    )
}


function isFormDataValidToSubmit(postFormData: PushPostDataModel<any, PostTopicDiscussionDetailsModel>): FormValidation {
    if (!postFormData.title || postFormData.title.length < 5) {
        return  {
            isValid: false,
            errorMessage: "Title is too short",
        };
    }

    if (!postFormData.summary || postFormData.summary.length < 5) {
        return  {
            isValid: false,
            errorMessage: "Body is too short",
        };
    }

    if (!postFormData.category) {
        return  {
            isValid: false,
            errorMessage: "You should select a category",
        };
    }

    return {
        isValid: true,
    };
}