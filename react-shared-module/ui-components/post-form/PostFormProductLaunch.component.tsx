import {initPushDataState, PushPostDataModel} from "../../logic/shared-data/datamodel/base-post-data.model";
import InputTextComponent from "../form/InputText.component";
import {useState} from "react";
import {
    MetaData,
    PostFormShareLinkShortenModel,
    PostFormShareLinksUtils
} from "../../logic/shared-data/datamodel/posts-model/post-share-link";
import CommonUtils from "../../logic/commonutils";
import {PostLinkPreviewComponent} from "../shared/PostLinkPreview.component";
import SpinnerComponent from "../shared/Spinner.component";
import AlertMessageComponent from "../shared/AlertMessage.component";
import RadioButtonComponent, {FormOption} from "../form/RadioButton.component";
import InputTagsFieldComponent from "../form/InputTagsField.component";
import {FormValidation} from "../../logic/shared-data/datamodel/shared-model-data";
import {useRouter} from "next/router";
import DropdownFormComponent from "../form/DropdownForm.component";
import {useAlertMessageGlobalState, useIsLoadingGlobalState} from "../../logic/global-hooks/root-global-state";
import InputTextAreaComponent from "../form/InputTextArea.component";
import {getLanguageLabel} from "../../logic/language/language.helper";

const IS_AUTHOR_RADIO_BTN_OPTIONS: any = {
    YES: {label: "Yes", optionId: "YES"},
    NO: {label: "No", optionId: "NO"},
}
const MAX_TAGS = 5;


export default function PostFormProductLaunchComponent(props: {
    postType: string,
    onSubmitClicked: (postData: PushPostDataModel<PostFormShareLinkShortenModel>) => Promise<void>,
    listCategories?: {[optionId: string]: FormOption}}) {
    const route = useRouter();
    const [formPostData, setFormPostData] = useState<PushPostDataModel<PostFormShareLinkShortenModel>>(initPushDataState<PostFormShareLinkShortenModel>(props.postType));
    const [isLoading, setIsLoadingState] = useIsLoadingGlobalState();
    const [alertMessageState, setAlertMessageState] = useAlertMessageGlobalState();
    const [timeoutHandler, setTimeoutHandle] = useState<any>();
    const [formValidation, setFormValidation] = useState<FormValidation>({isValid: false});

    // Select a category by default
    if (!formPostData.category) {
        formPostData.category = props.listCategories![Object.keys(props.listCategories!)[0]].optionId;
    }

    // Check if the form validation has changed
    if (formValidation.isValid !== PostFormShareLinksUtils.isFormDataValidToSubmit(formPostData).isValid) {
        setFormValidation(PostFormShareLinksUtils.isFormDataValidToSubmit(formPostData));
    }

    async function onTitleField(text: string) {
        setFormPostData({
            ...formPostData,
            title: text,
        });
    }
    async function onSummaryFieldChanged(text: string) {
        setFormPostData({
            ...formPostData,
            summary: text,
        });
    }

    // Link
    async function onLinkChanged(text: string) {
        let metaData: MetaData | undefined;

        // First clean the current metadata
        setFormPostData({
            ...formPostData,
            shortenPostData: {
                link: text,
                metaData,
            },
        });

        if (timeoutHandler) {
            clearInterval(timeoutHandler);
        }

        if (CommonUtils.isValidHttpUrl(text)) {
            setTimeoutHandle(setTimeout(async () => {
                setIsLoadingState(true);
                metaData = await CommonUtils.getUrlMetadata(text);
                setIsLoadingState(false);
                let title: string | undefined = formPostData.title;

                if (metaData && metaData["title"]) {
                    title = metaData["title"];
                }

                let summary: string | undefined = formPostData.summary;
                if (metaData && metaData["description"]) {
                    summary = metaData["description"];
                }

                let thumbnailUrl: string | undefined = formPostData.thumbnailUrl;
                if (metaData && metaData["image"]) {
                    thumbnailUrl = metaData["image"];
                }

                setFormPostData({
                    ...formPostData,
                    shortenPostData: {
                        link: text,
                        metaData,
                    },
                    title,
                    summary,
                    thumbnailUrl
                });
                clearInterval(timeoutHandler);
            }, 1500, {}));
        }
    }

    // You are the author?
    function onIsTheAuthor(optionId: string) {
        setFormPostData({
            ...formPostData,
            shortenPostData: {
                ...formPostData.shortenPostData,
                isTheAuthor: optionId === IS_AUTHOR_RADIO_BTN_OPTIONS.YES.optionId,
            },
        });
    }

    // Tags
    function onTagsChanged(tags: string[]) {
        setFormPostData({
            ...formPostData,
            tags: tags,
        });
    }

    function onCategorySelected(optionId: string) {
        // Option selected
        setFormPostData({
            ...formPostData,
            category: optionId,
        });
    }

    // Category (if any)
    const renderCategoryDropdown = props.listCategories ? (<div>
            <DropdownFormComponent label={"Launch type"} defaultLabel={"It's a new launch?"} options={props.listCategories} currentSelectionKey={formPostData.category} onOptionSelected={onCategorySelected} />
        </div>) : (<></>)

    function renderPreview() {
        if (formPostData && formPostData.shortenPostData.metaData && formPostData.shortenPostData.metaData.title) {
            return (
                <div className='flex flex-col gap-1'>
                    <PostLinkPreviewComponent linkUrl={formPostData.shortenPostData.link} metadata={formPostData.shortenPostData.metaData} />
                </div>
            );
        } else if (!isLoading && formPostData.shortenPostData.metaData && formPostData.shortenPostData.link && CommonUtils.isValidHttpUrl(formPostData.shortenPostData.link)) {
            return (
                <AlertMessageComponent type={"ERROR"} messageText={"Invalid link. Please, copy and paste a valid link to be published..."} />
            )
        } else {
            return (<></>);
        }
    }

    function onCancel() {
        route.back();
    }

    async function onSubmitClicked() {
        setIsLoadingState(true);
        await props.onSubmitClicked(formPostData);
        setIsLoadingState(false);
    }

    return (
        <div className='flex flex-col gap-5'>
            <InputTextComponent topLabel={getLanguageLabel("POST_FORM_PRODUCT_LINK_TITLE")} hint={getLanguageLabel("POST_FORM_PRODUCT_LINK_HINT")} disabled={isLoading} onChanged={onLinkChanged} inputType={"url"} />
            {renderPreview()}
            {isLoading ? (<div className='flex justify-center'>
                <SpinnerComponent />
            </div>) : (<></>)}
            <InputTextComponent topLabel={getLanguageLabel("POST_FORM_PRODUCT_NAME_TITLE")} hint={getLanguageLabel("POST_FORM_PRODUCT_NAME_HINT")} disabled={isLoading} onChanged={onTitleField} inputType={"text"} defaultValue={formPostData.title} value={formPostData.title} maxLength={128}/>
            <InputTextAreaComponent topLabel={getLanguageLabel("POST_FORM_PRODUCT_SUMMARY_TITLE")} hint={getLanguageLabel("POST_FORM_PRODUCT_SUMMARY_TITLE")} onChanged={onSummaryFieldChanged} inputType={"text"} defaultValue={formPostData.summary} disabled={isLoading} maxLength={280} />
            <RadioButtonComponent label={getLanguageLabel("POST_FORM_PRODUCT_CREATOR_TITLE")} options={IS_AUTHOR_RADIO_BTN_OPTIONS} setCurrentSelection={onIsTheAuthor} currentSelection={formPostData.shortenPostData.isTheAuthor ? IS_AUTHOR_RADIO_BTN_OPTIONS.YES.optionId : IS_AUTHOR_RADIO_BTN_OPTIONS.NO.optionId} />
            <InputTagsFieldComponent defaultTags={formPostData.tags} onTagsChanged={onTagsChanged} label={getLanguageLabel("POST_FORM_PRODUCT_TAGS_TITLE")} placeholder={getLanguageLabel("POST_FORM_PRODUCT_TAGS_HINT").replace("MAX_TAGS", MAX_TAGS.toString())} />
            {renderCategoryDropdown}
            <div className='flex justify-end gap-2'>
                <button className='btn btn-ghost' onClick={onCancel} disabled={isLoading}>{getLanguageLabel("POST_FORM_PRODUCT_CANCEL")}</button>
                <button className={'btn' + (isLoading ? " loading" : "")} onClick={onSubmitClicked} disabled={!formValidation.isValid || isLoading}>{getLanguageLabel("POST_FORM_PRODUCT_SUBMIT")}</button>
            </div>
        </div>
    )
}