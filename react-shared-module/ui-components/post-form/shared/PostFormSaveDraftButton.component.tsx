import {PushPostDataModel} from "../../../logic/shared-data/datamodel/base-post-data.model";
import {SharedBackendApi} from "../../../logic/shared-data/sharedbackend.api";
import {SharedFirestoreCollectionDB} from "../../../logic/shared-data/datamodel/shared-firestore-collections";
import AuthManager from "../../../logic/auth/auth.manager";
import {useState} from "react";

export default function PostFormSaveDraftButtonComponent(props: {postData: PushPostDataModel}) {
    const [isLoading, setIsLoading] = useState(false);

    function onSaveDraft() {
        setIsLoading(true);

        SharedBackendApi.requestDocumentChanges({
            collectionName: SharedFirestoreCollectionDB.DraftUserPosts,
            docId: AuthManager.getUserId(),
            methodType: "MERGE",
            docData: {
                [props.postData.postType]: props.postData,
            },
        }).then((response) => {
            setIsLoading(false);
        });
    }

    return (
        <button className={'btn btn-outline btn-warning ' + (isLoading ? "loading" : "")} onClick={onSaveDraft}>
            Save Draft
        </button>
    )
}