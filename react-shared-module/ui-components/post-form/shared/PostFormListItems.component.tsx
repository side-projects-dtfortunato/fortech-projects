import {useState} from "react";
import InputTextComponent from "../../form/InputText.component";
import {getLanguageLabel} from "../../../logic/language/language.helper";
import {TrashIcon} from "@heroicons/react/24/solid";
import InputTextAreaComponent from "../../form/InputTextArea.component";
import {PostListBulletItemModel, PostListItemsModel} from "../../../logic/shared-data/datamodel/base-post-data.model";
import {renderListItem} from "../../posts/post-details/shared/PostListSummaryItems.component";
import {PencilSquareIcon} from "@heroicons/react/24/outline";

export default function PostFormListItemsComponent(props: { onListChanged: ((lists?: PostListItemsModel) => Promise<void>) }) {
    const [listItemsState, setListItems] = useState<PostListItemsModel | undefined>();
    const [editingIndex, setEditingIndex] = useState<number>(-1);

    function onDataChanged() {
        props.onListChanged(listItemsState);
    }

    function onTitleChanged(text: string) {
        setListItems(listItemsState ? {
            ...listItemsState,
            listTitle: text,
        } : {listTitle: text, listItems: []});
        onDataChanged();
    }

    function onItemTitle(text: string, postListBulletItem: PostListBulletItemModel, index: number) {
        if (listItemsState) {
            listItemsState!.listItems[index].title = text;
            setListItems({
                ...listItemsState,
            });
            onDataChanged();
        }
    }

    function onItemSummary(text: string, postListBulletItem: PostListBulletItemModel, index: number) {
        if (listItemsState) {
            listItemsState!.listItems[index].summary = text;
            setListItems({
                ...listItemsState,
            });
            onDataChanged();
        }
    }

    function onItemLink(text: string, postListBulletItem: PostListBulletItemModel, index: number) {
        if (listItemsState) {
            listItemsState!.listItems[index].link = text;
            setListItems({
                ...listItemsState,
            });
            onDataChanged();
        }
    }

    function onThumbnailUrl(text: string, postListBulletItem: PostListBulletItemModel, index: number) {
        if (listItemsState) {
            listItemsState!.listItems[index].imageUrl = text;
            setListItems({
                ...listItemsState,
            });
            onDataChanged();
        }
    }

    function onItemAddSummary(postListBulletItem: PostListBulletItemModel, index: number) {
        if (listItemsState) {
            listItemsState!.listItems[index].summary = "";
            setListItems({
                ...listItemsState,
            });
        }
    }

    function onItemAddLink(postListBulletItem: PostListBulletItemModel, index: number) {
        if (listItemsState) {
            listItemsState!.listItems[index].link = "";
            setListItems({
                ...listItemsState,
            });
        }
    }

    function onItemAddThumbnail(postListBulletItem: PostListBulletItemModel, index: number) {
        if (listItemsState) {
            listItemsState!.listItems[index].imageUrl = "";
            setListItems({
                ...listItemsState,
            });
        }
    }

    function onRemoveItem(index: number) {
        if (listItemsState) {
            listItemsState.listItems = listItemsState.listItems.filter((item, itemIndex) => itemIndex !== index);
            setListItems(listItemsState.listItems.length > 0 ? {
                ...listItemsState,
            } : undefined);
            onDataChanged();
        }
    }

    function onEditIndexChanged(index: number) {
        setEditingIndex(index);
    }

    function renderEditorListItem(postListBulletItem: PostListBulletItemModel, index: number) {
        // Input label and input link (and export thumbnail from link)
        let renderSummaryContainer: any = <InputTextAreaComponent topLabel={""}
                                                                  hint={"(Optional) Write a description about this item"}
                                                                  defaultValue={postListBulletItem.summary}
                                                                  maxLength={5000}
                                                                  onChanged={(text) => onItemSummary(text, postListBulletItem, index)}/>;
        if (postListBulletItem.summary === undefined) {
            renderSummaryContainer =
                <button className='btn-xs btn-ghost' onClick={() => onItemAddSummary(postListBulletItem, index)}>+ Add
                    item short description</button>
        }

        let renderLinkContainer: any = <InputTextComponent inputType={"url"} topLabel={""}
                                                           hint={"(Optional) Title redirection link"}
                                                           defaultValue={postListBulletItem.link}
                                                           onChanged={(text) => onItemLink(text, postListBulletItem, index)}/>;
        if (postListBulletItem.link === undefined) {
            renderLinkContainer =
                <button className='btn-xs btn-ghost' onClick={() => onItemAddLink(postListBulletItem, index)}>+ Add item
                    title link</button>
        }

        let renderThumbnailUrlContainer: any = <InputTextComponent inputType={"url"}
                                                                   topLabel={""}
                                                                   hint={"(Optional) Thumbnail Url"}
                                                                   defaultValue={postListBulletItem.imageUrl}
                                                                   onChanged={(text) => onThumbnailUrl(text, postListBulletItem, index)}/>;
        if (postListBulletItem.imageUrl === undefined) {
            renderThumbnailUrlContainer =
                <button className='btn-xs btn-ghost' onClick={() => onItemAddThumbnail(postListBulletItem, index)}>+ Add
                    Thumbnail Url</button>
        }

        return (
            <div className='flex flex-col items-start gap-1 w-full'>
                <InputTextComponent topLabel={"List item title"} hint={"Write the title of the item"}
                                    defaultValue={postListBulletItem.title}
                                    onChanged={(text) => onItemTitle(text, postListBulletItem, index)}/>
                {renderLinkContainer}
                {renderSummaryContainer}
                {renderThumbnailUrlContainer}
            </div>
        )
    }

    function renderBulletListItem(postListBulletItem: PostListBulletItemModel, index: number) {

        let renderContent: any = renderEditorListItem(postListBulletItem, index);

        if (editingIndex !== index) {
            // Add click event
            renderContent = (
                <ul className='list w-full'>
                    {renderListItem(postListBulletItem)}
                </ul>
            )
        }

        return (
            <div className='list-item-bg drop-shadow flex flex-col items-start gap-1 p-2 bg-slate-100'>
                <div>
                    <button className='btn-xs btn-ghost' onClick={() => onRemoveItem(index)}><TrashIcon height={20}/></button>
                    <button className='btn-xs btn-ghost' onClick={() => onEditIndexChanged(index)}><PencilSquareIcon height={20}/></button>
                </div>
                {renderContent}
            </div>
        )
    }

    function renderListItems() {
        if (listItemsState) {
            return (
                <div className='flex flex-col gap-2'>
                    <div className='divider text-slate-400'>{getLanguageLabel("POST_LIST_ITEMS_FORM_DIVIDER")}</div>
                    <InputTextComponent topLabel={"List Title"}
                                        hint={"Summarize here the list of relevant topics on the shared article"}
                                        onChanged={onTitleChanged}/>
                    {listItemsState.listItems.map((item, index) => renderBulletListItem(item, index))}
                </div>
            )
        } else {
            return (<></>);
        }

    }


    function onAddListBtn() {
        if (!listItemsState) {
            setListItems({
                listItems: [{
                    title: "",
                }],
                listTitle: "",
            });
        } else {
            listItemsState.listItems.push({
                title: "",
            });
            setListItems({
                ...listItemsState,
            });
        }

        // Update editing index
        setEditingIndex(listItemsState?.listItems ? listItemsState.listItems.length - 1 : 0);
    }

    function isValidToAddMoreLists() {
        // Check if the last item is valid
        let isValid = true;

        if (listItemsState && listItemsState.listItems.length > 0) {
            isValid = listItemsState.listItems[listItemsState.listItems.length - 1].title.length > 3;
        }
        return isValid;
    }

    return (
        <div className='flex flex-col gap-2'>
            {renderListItems()}
            <button className='btn btn-outline w-full' disabled={!isValidToAddMoreLists()}
                    onClick={onAddListBtn}>{getLanguageLabel("POST_LIST_ITEMS_ADD_BTN")}</button>
        </div>
    )
}