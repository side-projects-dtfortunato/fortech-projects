import {initPushDataState, PushPostDataModel} from "../../logic/shared-data/datamodel/base-post-data.model";
import InputTextComponent from "../form/InputText.component";
import {useState} from "react";
import SpinnerComponent from "../shared/Spinner.component";
import {FormOption} from "../form/RadioButton.component";
import InputTagsFieldComponent from "../form/InputTagsField.component";
import {FormValidation} from "../../logic/shared-data/datamodel/shared-model-data";
import {useRouter} from "next/router";
import DropdownFormComponent from "../form/DropdownForm.component";
import {useIsLoadingGlobalState} from "../../logic/global-hooks/root-global-state";
import RichTextEditorComponent from "../form/RichTextEditor.component";
import {PostFormFullArticleDetails} from "../../logic/shared-data/datamodel/posts-model/post-full-article";
import {getLanguageLabel} from "../../logic/language/language.helper";
import PostFormSaveDraftButtonComponent from "./shared/PostFormSaveDraftButton.component";

var getImgSrc = require('get-img-src')

const IS_AUTHOR_RADIO_BTN_OPTIONS: any = {
    YES: {label: "Yes", optionId: "YES"},
    NO: {label: "No", optionId: "NO"},
}
const MAX_TAGS = 5;


export default function PostFormWriteArticleComponent(props: {
    postType: string,
    defaultArticleData?: PushPostDataModel<any, PostFormFullArticleDetails>,
    onSubmitClicked: (postData: PushPostDataModel<any, PostFormFullArticleDetails>) => Promise<void>,
    onThumbnailBase64: (thumbnailBase64: string) => void,
    listCategories?: {[optionId: string]: FormOption}, isEditing?: boolean}) {
    const route = useRouter();
    const [formPostData, setFormPostData] = useState<PushPostDataModel<any, PostFormFullArticleDetails>>(props.defaultArticleData ? props.defaultArticleData : initPushDataState<any, PostFormFullArticleDetails>(props.postType));
    const [isLoading, setIsLoadingState] = useIsLoadingGlobalState();
    const [formValidation, setFormValidation] = useState<FormValidation>({isValid: false});
    const [thumbnailTempUrl, setThumbnailTempUrl] = useState<string | null>(null);

    // Check if the form validation has changed
    if (formValidation.isValid !== isFormDataValidToSubmit(formPostData).isValid) {
        setFormValidation(isFormDataValidToSubmit(formPostData));
    }

    async function onTitleField(text: string) {
        setFormPostData({
            ...formPostData,
            title: text,
        });
    }
    // Tags
    function onTagsChanged(tags: string[]) {
        setFormPostData({
            ...formPostData,
            tags: tags,
        });
    }

    // Article content
    function onSummaryChanged(text: string) {
        setFormPostData({
            ...formPostData,
            summary: text,
        });
    }

    // Article content
    function onArticleTextChanged(text: string) {
        let imgSrcs: string[] = getImgSrc(text);
        let thumbnailUrl: string | undefined;

        if (imgSrcs && imgSrcs.length > 0) {
            let thumbnailUrlExport: string = imgSrcs[0];
            if (thumbnailUrlExport.startsWith("http")) {
                setThumbnailTempUrl(thumbnailUrlExport);
                thumbnailUrl = thumbnailUrlExport;
            } else {
                setThumbnailTempUrl(thumbnailUrlExport);
                props.onThumbnailBase64(thumbnailUrlExport);
            }
        }

        setFormPostData({
            ...formPostData,
            detailsPostData: {
                bodyText: text,
            },
            thumbnailUrl: thumbnailUrl ? thumbnailUrl : undefined,
        });
    }

    function onCategorySelected(optionId: string) {
        // Option selected
        setFormPostData({
            ...formPostData,
            category: optionId,
        });
    }

    // Category (if any)
    const renderCategoryDropdown = props.listCategories ? (<div>
            <DropdownFormComponent label={"Select a relevant category:"} defaultLabel={"Select a topic"} options={props.listCategories} onOptionSelected={onCategorySelected} disabled={props.isEditing} />
        </div>) : (<></>);
    function onCancel() {
        route.back();
    }

    async function onSubmitClicked() {
        setIsLoadingState(true);
        await props.onSubmitClicked(formPostData);
        setIsLoadingState(false);
    }

    return (
        <div className='flex flex-col gap-5'>
            {isLoading ? (<div className='flex justify-center'>
                <SpinnerComponent />
            </div>) : (<></>)}
            {thumbnailTempUrl && thumbnailTempUrl.length > 0 ? (
                <div className={"flex w-full"} style={{minWidth: 125, height:200}}>
                    <div className='self-stretch rounded drop-shadow border bg-white' style={{backgroundImage: "Url(" + thumbnailTempUrl + ")", backgroundSize: "cover", backgroundPosition: "center", width: "100%", height: "100%"}}/>
                </div>) : (<></>)}
            <InputTextComponent topLabel={"Title of the article"} hint={"Enter a suggestive title"} disabled={isLoading} onChanged={onTitleField} inputType={"text"} defaultValue={formPostData.title} value={formPostData.title} maxLength={128}/>
            <InputTextComponent topLabel={"Short summary about the article"} hint={"Enter a suggestive summary, will be displayed in the post list"} disabled={isLoading} onChanged={onSummaryChanged} inputType={"text"} defaultValue={formPostData.summary} value={formPostData.summary} maxLength={280}/>
            <div className='flex flex-col gap-2 mb-5'>
                <h3 className={"custom"}>{getLanguageLabel("PUBLISH_FORM_ARTICLE_BODY_TITLE")}</h3>
                <RichTextEditorComponent onTextChanged={onArticleTextChanged} value={formPostData.detailsPostData?.bodyText} />
            </div>
            <InputTagsFieldComponent defaultTags={formPostData.tags} onTagsChanged={onTagsChanged} label={"Set some tags to describe this story (max: " + MAX_TAGS + ")"} placeholder={"Press enter to add the tag..."} disabled={props.isEditing} />
            {renderCategoryDropdown}
            <div className='flex justify-end gap-2'>
                <button className='btn btn-ghost' onClick={onCancel} disabled={isLoading}>Cancel post</button>
                <PostFormSaveDraftButtonComponent postData={formPostData} />
                <button className={'btn' + (isLoading ? " loading" : "")} onClick={onSubmitClicked} disabled={!formValidation.isValid || isLoading}>Submit</button>
            </div>
        </div>
    )
}


function isFormDataValidToSubmit(postFormData: PushPostDataModel<any, PostFormFullArticleDetails>): FormValidation {
    if (!postFormData.title || postFormData.title.length < 5) {
        return  {
            isValid: false,
            errorMessage: "Title is too short",
        };
    }

    if (!postFormData.summary || postFormData.summary.length < 10) {
        return  {
            isValid: false,
            errorMessage: "Summary is too short",
        };
    }

    if (!postFormData.detailsPostData?.bodyText || postFormData.detailsPostData?.bodyText.length < 20) {
        return  {
            isValid: false,
            errorMessage: "Article is too short",
        };
    }


    if (!postFormData.category) {
        return  {
            isValid: false,
            errorMessage: "You should select a category",
        };
    }

    return {
        isValid: true,
    };
}