import {ArticleBlockTextContent} from "../../../../logic/shared-data/datamodel/posts-model/post-article-blocks";
import {useState} from "react";
import InputTextComponent from "../../../form/InputText.component";
import RichTextEditorComponent from "../../../form/RichTextEditor.component";

export function ArticleBlockEditorTextContentComponent(props: { blockData: ArticleBlockTextContent, index: number, onRemoved: (index: number) => void, onBlockChanged: (index: number, blockData: ArticleBlockTextContent) => void }) {
    const [blockDataState, setBlockDataState] = useState(props.blockData);

    function onFieldChanged(fieldId: string, value: string) {
        let updatedBlockData: ArticleBlockTextContent = {
            ...blockDataState,
            [fieldId]: value,
        };

        props.onBlockChanged(props.index, updatedBlockData);
        setBlockDataState(updatedBlockData);

    }

    return (
        <div className="list-item-bg drop-shadow p-3 bg-slate-100">
            <div className='flex flex-col justify-center items-stretch gap-3'>
                <h3 className='custom text-align-start'>Content Section:</h3>
                <InputTextComponent topLabel={""} hint={"Title of the section (optional)"}
                                    onChanged={(value) => onFieldChanged("title", value)} maxLength={100}
                                    defaultValue={props.blockData.title} inputType={"text"}/>
                <InputTextComponent topLabel={""} hint={"Title Link (optional)"}
                                    onChanged={(value) => onFieldChanged("titleLink", value)}
                                    defaultValue={props.blockData.titleLink} inputType={"url"}/>
                {/* TODO Upload Image */}
                <span className='subtitle'>Content Body:</span>
                <RichTextEditorComponent onTextChanged={(value) => onFieldChanged("text", value)}
                                         value={props.blockData.text} height={300}/>
                <div className='flex flex-row'>
                    <button className='btn btn-outline btn-error'
                            onClick={() => props.onRemoved(props.index)}>
                        Remove Section
                    </button>
                </div>
            </div>
        </div>
    )
}