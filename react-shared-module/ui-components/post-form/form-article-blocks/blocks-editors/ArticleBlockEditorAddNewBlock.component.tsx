import {
    ArticleBlockBase,
    ArticleBlockEmbedded,
    ArticleBlockTextContent,
    ArticleBlockType
} from "../../../../logic/shared-data/datamodel/posts-model/post-article-blocks";

export function ArticleBlockEditorAddNewBlockComponent(props: {index: number, onAddedBlock: (index: number, blockData: ArticleBlockBase) => void}) {

    function onAddBlock(blockType: ArticleBlockType) {
        let blockData: ArticleBlockBase = {
            type: blockType,
        };

        switch (blockType) {
            case ArticleBlockType.TEXT_CONTENT:
                blockData = {
                    ...blockData,
                    text: "",
                } as ArticleBlockTextContent
                break;
            case ArticleBlockType.EMBEDDED:
                blockData = {
                    ...blockData,
                    htmlCode: "",
                } as ArticleBlockEmbedded
                break;
        }

        props.onAddedBlock(props.index, blockData);
    }

    // For now we only allow to add a text content section
    return (
        <div className='flex flex-row w-full justify-center '>
            <button className="btn btn-circle" onClick={() => onAddBlock(ArticleBlockType.TEXT_CONTENT)}>
                <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth={1.5} stroke="currentColor" className="w-6 h-6">
                    <path strokeLinecap="round" strokeLinejoin="round" d="M12 4.5v15m7.5-7.5h-15" />
                </svg>
            </button>
        </div>
    )

}