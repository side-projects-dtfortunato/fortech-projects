import CustomRootLayoutComponent from "../../../app-components/root/CustomRootLayout.component";
import {useState} from "react";
import LocalStorageDataManager from "../../logic/managers/LocalStorageData.manager";
import {SharedFirestoreCollectionDB} from "../../logic/shared-data/datamodel/shared-firestore-collections";
import {useAuthState} from "react-firebase-hooks/auth";
import {auth} from "../../logic/shared-data/firebase.utils";
import AuthManager from "../../logic/auth/auth.manager";
import {useRouter} from "next/router";
import {getLoginPageLink} from "../../../../pages/login";
import SpinnerComponent from "../shared/Spinner.component";
import {BasePostDataModel} from "../../logic/shared-data/datamodel/base-post-data.model";
import EmptyListMessageComponent from "../shared/EmptyListMessage.component";
import {getLanguageLabel} from "../../logic/language/language.helper";
import {BookmarkIcon} from "@heroicons/react/24/solid";
import ListPostsComponent from "../posts/shared/ListPosts.component";

export default function SharedBookmarksPageContentComponent() {
    const [listBookmarks, setListBookmarks] = useState<{[postId: string]: BasePostDataModel} | undefined>();
    const [currentUser, isAuthLoading] = useAuthState(auth);
    const [isLoading, setIsLoading] = useState(true);
    const router = useRouter();

    if (!isAuthLoading && !AuthManager.isUserLogged()) {
        // Redirect to Login page
        router.push(getLoginPageLink());
        return (
            <CustomRootLayoutComponent isIndexable={false}>
                <SpinnerComponent />
            </CustomRootLayoutComponent>
        )
    } else if (AuthManager.isUserLogged() && !listBookmarks && isLoading) {
        LocalStorageDataManager.getLocalData({collection: SharedFirestoreCollectionDB.UserBookmarks, docId: AuthManager.getUserId()!, forceUpdate: true})
            .then((data) => {
                if (data && data.posts) {
                    setListBookmarks(data.posts);
                }

                setIsLoading(false);
        });
    }

    function renderBookmarksList() {
        if (!listBookmarks || Object.keys(listBookmarks).length === 0) {
            return <EmptyListMessageComponent message={getLanguageLabel("BOOKMARKS_EMPTY_LIST", "You don't have Bookmarks")} icon={<BookmarkIcon height={40} />} />
        } else {
            return <ListPostsComponent key={"Bookmark Posts"} hideStats={true} listPosts={Object.values(listBookmarks).sort((p1, p2) => p2.publishedAt - p1.publishedAt)} />
        }
    }

    function renderContent() {
        if (isLoading || isAuthLoading) {
            return (
                <SpinnerComponent />
            )
        } else {
            return (
                <>
                    {renderBookmarksList()}
                </>
            )
        }
    }

    return (
        <CustomRootLayoutComponent isIndexable={false}>
            <div className='flex flex-col gap-2 items-center'>
                <div className='flex flex-row gap-3 items-center w-full justify-start'>
                    <BookmarkIcon height={25}/>
                    <h1 className='custom'>{getLanguageLabel("BOOKMARKS_PAGE_TITLE_PAGE")}</h1>
                </div>
                {renderContent()}
            </div>
        </CustomRootLayoutComponent>
    )

}

