import RootConfigs from "../../../../../configs";
import {useEffect, useRef} from "react";


export default function AdminThumbnailGeneratorComponent(props: {title: string, bgImageUrl: string, category?: string}) {
    const backgroundImageUrl = props.bgImageUrl;
    const title = props.title;
    const category = props.category ? props.category : "";
    const logoUrl = RootConfigs.BASE_URL + "/images/logo-128.png";

    const canvasRef = useRef<HTMLCanvasElement>(null);

    useEffect(() => {
        const canvas = canvasRef.current;
        const context = canvas?.getContext('2d');
        if (!canvas || !context) return;

        const backgroundImage = new Image();
        const logoImage = new Image();

        backgroundImage.src = backgroundImageUrl;
        logoImage.src = logoUrl;

        backgroundImage.onload = () => {
            const { width, height } = canvas;

            // Center crop the background image
            const imgWidth = backgroundImage.width;
            const imgHeight = backgroundImage.height;
            const aspectRatio = imgWidth / imgHeight;
            let srcX = 0, srcY = 0, srcWidth = imgWidth, srcHeight = imgHeight;

            if (aspectRatio > 1) {
                // Landscape image
                srcWidth = imgHeight;
                srcX = (imgWidth - imgHeight) / 2;
            } else if (aspectRatio < 1) {
                // Portrait image
                srcHeight = imgWidth;
                srcY = (imgHeight - imgWidth) / 2;
            }

            context.drawImage(backgroundImage, srcX, srcY, srcWidth, srcHeight, 0, 0, width, height);

            // Add a gradient overlay from bottom to top
            const gradient = context.createLinearGradient(0, height, 0, 0);
            gradient.addColorStop(0, 'rgba(0, 0, 0, 0.7)');
            gradient.addColorStop(1, 'rgba(0, 0, 0, 0)');
            context.fillStyle = gradient;
            context.fillRect(0, 0, width, height);

            // Set text properties for the title
            context.font = 'bold 60px Arial'; // Title font size
            context.fillStyle = 'white';
            context.textAlign = 'center';

            const maxWidth = width - 40; // Only horizontal margin
            const lineHeight = 64; // Adjusted line height for the larger font
            const x = width / 2;
            let y = height / 2 + lineHeight; // Move title slightly down

            // Function to wrap text
            const wrapText = (context: CanvasRenderingContext2D, text: string, x: number, y: number, maxWidth: number, lineHeight: number): number => {
                const words = text.split(' ');
                let line = '';
                const lines = [];

                for (let n = 0; n < words.length; n++) {
                    const testLine = line + words[n] + ' ';
                    const metrics = context.measureText(testLine);
                    const testWidth = metrics.width;
                    if (testWidth > maxWidth && n > 0) {
                        lines.push(line);
                        line = words[n] + ' ';
                    } else {
                        line = testLine;
                    }
                }
                lines.push(line);

                for (let k = 0; k < lines.length; k++) {
                    context.fillText(lines[k], x, y - (lines.length - 1 - k) * lineHeight);
                }

                return lines.length * lineHeight;
            };

            const titleHeight = wrapText(context, title, x, y, maxWidth, lineHeight);
            y += titleHeight / 2;

            // Set text properties for the category
            context.font = 'lighter 48px Arial'; // Increased font size
            context.fillStyle = 'rgba(255, 255, 255, 0.8)';

            // Draw the category text below the title
            wrapText(context, category, x, y + lineHeight, maxWidth, lineHeight);

            // Draw the logo in the top right corner
            const logoSize = 80; // Increased logo size
            const logoX = width - logoSize - 20; // Adjusted position for top right corner
            const logoY = 20; // Adjusted position for top right corner

            context.drawImage(logoImage, logoX, logoY, logoSize, logoSize);
        };
    }, [backgroundImageUrl, title, category, logoUrl]);

    return (
        <canvas
            ref={canvasRef}
            width={1080}
            height={1080}
            style={{ width: '100%', height: 'auto', maxWidth: '1080px', maxHeight: '1080px' }}
        />
    );
}