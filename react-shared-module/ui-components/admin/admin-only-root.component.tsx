import {useAuthState} from "react-firebase-hooks/auth";
import {auth} from "../../logic/shared-data/firebase.utils";
import {useCentralUserDataState} from "../../logic/global-hooks/root-global-state";
import CustomRootLayoutComponent from "../../../app-components/root/CustomRootLayout.component";
import SpinnerComponent from "../shared/Spinner.component";
import {UserRole} from "../../logic/shared-data/datamodel/shared-central-user-data.model";
import NotFoundSimpleMessageComponent from "../shared/NotFoundSimpleMessage.component";

export default function AdminOnlyRootComponent(props: {children: any}) {
    const [currentUser, isAuthLoading] = useAuthState(auth);
    const [centralUserData, setCentralUserData] = useCentralUserDataState();

    if (isAuthLoading) {
        return (
            <CustomRootLayoutComponent isIndexable={false}>
                <div className='w-full h-screen'>
                    <SpinnerComponent />
                </div>
            </CustomRootLayoutComponent>
        );
    } else if (!centralUserData || centralUserData.userRole !== UserRole.ADMIN) {
        return (
            <CustomRootLayoutComponent isIndexable={false}>
                <NotFoundSimpleMessageComponent message={"You are not authorized."} />
            </CustomRootLayoutComponent>
        );
    } else {
        return props.children;
    }
}