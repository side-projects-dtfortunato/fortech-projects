import {useState} from "react";
import AuthManager from "../../../logic/auth/auth.manager";
import {SharedBackendApi} from "../../../logic/shared-data/sharedbackend.api";
import {useAlertMessageGlobalState, useCentralUserDataState} from "../../../logic/global-hooks/root-global-state";
import {APIResponseMessageUtils} from "../../../logic/global-hooks/APIResponseMessageUtils";
import CommonUtils from "../../../logic/commonutils";
import {getLoginPageLink} from "../../../../../pages/login";
import {useRouter} from "next/router";

export default function UserFollowingButtonComponent(props: {userIdToFollow: string}) {
    const router = useRouter();
    const [centralUserState, setCentralUserState] = useCentralUserDataState();
    const [isFollowing, setIsFollowing] = useState<boolean>(false);
    const [isLoading, setIsLoading] = useState(false);
    const [alertMessageState, setAlertMessageState] = useAlertMessageGlobalState();

    if (centralUserState) {
        let updatedIsFollowingState: boolean = centralUserState.usersFollowing !== undefined && centralUserState.usersFollowing[props.userIdToFollow] === true;

        if (updatedIsFollowingState !== isFollowing) {
            setIsFollowing(updatedIsFollowingState);
        }
    }

    async function onFollowingBtnClicked(e: any) {
        CommonUtils.preventClickPropagation(e);

        if (!AuthManager.isUserLogged()) {
            await router.push(getLoginPageLink());
            return;
        }

        // Toggle current state
        const updatedState: boolean = !isFollowing;
        setIsLoading(true);
        const apiResponse = await SharedBackendApi.updateFollowingUser({userIdToFollow: props.userIdToFollow, startFollowing: updatedState});
        if (apiResponse.isSuccess) {
            setIsFollowing(updatedState);
            // Force to update central data
            setCentralUserState(await AuthManager.getCentralUserData(true));
        } else {
            setAlertMessageState({
                alertType: "ERROR",
                display: true,
                message: APIResponseMessageUtils.getAPIResponseMessage(apiResponse),
            });
        }
        setIsLoading(false);
    }

    if (!AuthManager.getUserId() || props.userIdToFollow === AuthManager.getUserId()) {
        return (<></>);
    } else if (isFollowing) {
        return (
            <button className='btn btn-outline btn-xs' onClick={onFollowingBtnClicked}>Unfollow</button>
        )
    } else if (isLoading) {
        return (
            <button className='btn btn-ghost btn-xs loading' disabled></button>
        )
    } else {
        return (
            <button className='btn btn-xs' onClick={onFollowingBtnClicked}>Follow</button>
        )
    }
}