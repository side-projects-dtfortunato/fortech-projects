import {BasePostDataModel} from "../../logic/shared-data/datamodel/base-post-data.model";
import ListLoadMoreComponent from "../lists/ListLoadMore.component";
import NotFoundSimpleMessageComponent from "../shared/NotFoundSimpleMessage.component";
import {UIHelper} from "../../../ui-helper/UIHelper";

export default function PublicProfileListPostsComponent(props: {listPosts?: {[postId: string]: BasePostDataModel}}) {

    function renderPostListItem(postData: BasePostDataModel, index: number) {
        return UIHelper.renderPostListItem(postData, index);
    }

    if (props.listPosts) {
        let listPostsSorted: BasePostDataModel[] = Object.values(props.listPosts).sort((p1, p2) => p2.publishedAt - p1.publishedAt);

        return (
            <div className='flex flex-col'>
                <ListLoadMoreComponent listItems={listPostsSorted} onRenderItem={renderPostListItem} initialDisplayedItems={12} />
            </div>
        )
    } else {
        return (
            <NotFoundSimpleMessageComponent message={"This user doesn't have any post published yet"} />
        )
    }

}