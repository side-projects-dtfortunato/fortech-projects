import {SharedPublicUserprofileModel} from "../../logic/shared-data/datamodel/shared-public-userprofile.model";
import UserPictureAvatarComponent from "../shared/UserPictureAvatar.component";
import {MapPinIcon} from "@heroicons/react/24/outline";
import CountryFlagIconComponent from "../shared/CountryFlagIcon.component";
import CommonUtils from "../../logic/commonutils";
import AuthManager from "../../logic/auth/auth.manager";
import {getUserProfileLink} from "../../../../pages/edit-profile/user-profile";
import {useAuthState} from "react-firebase-hooks/auth";
import {auth} from "../../logic/shared-data/firebase.utils";
import UserFollowingButtonComponent from "./shared/UserFollowingButton.component";
import Link from "next/link";

export default function PublicProfilePageContent(props: {userProfile: SharedPublicUserprofileModel}) {
    const [currentUser, isAuthLoading] = useAuthState(auth);

    function renderLocation() {
        if (props.userProfile.location) {
            return (
                <div className='flex flex-row items-center gap-2 mt-2'>
                    <MapPinIcon width={18}/>
                    <CountryFlagIconComponent width={20} countryCode={props.userProfile.location.countryCode}/>
                    <span className='subtitle'>{props.userProfile.location.countryName}</span>
                </div>
            )
        } else {
            return (<></>)
        }
    }

    function renderAdditionalLinks() {
        if (props.userProfile.links && Object.keys(props.userProfile.links).length > 0) {
            let renderLinksChilds: any[] = Object.values(props.userProfile.links).map((linkItem) => {
                let link: string = CommonUtils.addLinkSource(linkItem.link);
                return (
                    <Link key={link} href={link} target={"_blank"} rel={"noreferrer"} className="badge badge-outline badge flex items-center gap-1 hover:bg-slate-200">
                        {linkItem.iconUrl ? <img className='rounded' src={linkItem.iconUrl} width={12} height={12} alt={"linkItem.label"}/> : <></>}
                        {linkItem.label}
                    </Link>
                )
            });

            return (
                <div className='flex flex-wrap gap-2 items-center mt-5'>
                    <span className='subtitle'>Links: </span>
                    {renderLinksChilds}
                </div>
            )
        } else {
            return (<></>)
        }
    }

    function renderBio() {
        if (props.userProfile.bioDescription && props.userProfile.bioDescription.length > 0) {
            return (
                <div className='flex flex-col gap-2'>
                    <h3 className='custom'>About</h3>
                    <span className='custom italic'>{props.userProfile.bioDescription}</span>
                </div>
            )
        } else {
            return (<></>);
        }
    }

    const renderEditProfileBtn = props.userProfile.uid === AuthManager.getUserId() ? (<Link href={getUserProfileLink()} className='btn btn-outline btn-xs'>Edit profile</Link>) : (<></>)
    function renderProfileHeader() {
        return (
            <div className='flex flex-row gap-4 items-start'>
                <UserPictureAvatarComponent width={"w-24"} pictureUrl={props.userProfile.userPicture?.fileUrl} />
                <div className='flex flex-col'>
                    <div className='flex flex-wrap items-center gap-3'>
                        <h2 className='custom'>{props.userProfile.name}</h2>
                        {renderEditProfileBtn}
                        <UserFollowingButtonComponent userIdToFollow={props.userProfile.uid!} />
                    </div>
                    <span className='subtitle italic'>{props.userProfile.publicSubtitle}</span>
                    {renderLocation()}
                    {renderAdditionalLinks()}
                </div>
            </div>
        )
    }

    return (
        <div className='list-item-bg drop-shadow p-4 flex flex-col gap-4'>
            {renderProfileHeader()}
            {renderBio()}
        </div>
    )
}