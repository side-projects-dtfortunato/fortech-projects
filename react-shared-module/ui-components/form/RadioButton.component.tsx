export interface FormOption {
    label: string;
    optionId: string;
    displayOption?: boolean;
}

export default function RadioButtonComponent(props: { label: string, options: { [optionId: string]: FormOption}, currentSelection?: string, setCurrentSelection: (optionId: string) => void }, hookForm?: any) {

    function renderOptions() {
        return Object.values(props.options).filter((option) => option.displayOption === true || option.displayOption === undefined)
            .map((option) => {
                let isChecked: boolean = props.currentSelection === option.optionId;
                return (
                    <label className="label cursor-pointer gap-2" key={option.optionId}>
                        <span className="label-text text-base">{option.label}</span>
                        <input type="radio" name={props.label} className="radio" checked={isChecked}
                               onChange={(event) => {
                                   if (event.target.checked) {
                                       props.setCurrentSelection(option.optionId);
                                   }
                               }}/>
                    </label>
                )
            });
    }

    return (
        <div className='flex flex-wrap gap-2 items-center'>
            <span className='font-bold text-base'>{props.label}</span>
            {renderOptions()}
        </div>
    )
}