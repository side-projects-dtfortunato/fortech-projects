import {useState} from "react";
import InputTextComponent from "./InputText.component";
import CommonUtils from "../../logic/commonutils";
import {RiDeleteBackLine} from "react-icons/ri";

export interface ListLinkFormItemModel {
    label: string,
    link: string,
    isValid?: boolean,
    disable?: boolean,
}

export default function InputListLinksFormComponent(props: { defaultLinks: ListLinkFormItemModel[], onListValidLinksChanged: (links: ListLinkFormItemModel[]) => void, maxLinks?: number,}) {
    const [listLinks, setListLinks] = useState<ListLinkFormItemModel[]>(props.defaultLinks ? props.defaultLinks : [{
        label: "",
        link: "",
        isValid: false,
    }]);

    function onLinksChanged() {
        props.onListValidLinksChanged(listLinks.filter((link) => link.isValid));
    }

    function renderListLinkInput(listLinkItem: ListLinkFormItemModel, index: number) {
        return (
            <div className='flex flex-row gap-2 items-end w-full'>
                <InputTextComponent topLabel={index === 0 ? "Url" : ""} hint={"Link"} disabled={listLinkItem.disable} onChanged={(value) => {
                    listLinks[index].link = value.trim();
                    listLinks[index].isValid = CommonUtils.isValidHttpUrl(value);
                    if (CommonUtils.isValidHttpUrl(value)) {
                        listLinks[index].label = CommonUtils.getDomainNameFromUrl(value);
                    } else {
                        listLinks[index].label = "";
                    }

                    setListLinks(listLinks.concat());
                    onLinksChanged();
                }
                } value={listLinkItem.link}/>
                {
                    listLinkItem.disable ? <></> :

                        <button className='btn btn-xs text-lg btn-ghost text-red-500 mb-3' disabled={listLinkItem.disable} onClick={() => {
                            setListLinks(listLinks.filter((link) => link !== listLinkItem));
                        }
                        }><RiDeleteBackLine/></button>
                }
            </div>
        )
    }

    function renderAddButton() {
        if (props.maxLinks && listLinks.length >= props.maxLinks) {
            return (<></>);
        }
        const isDisabled: boolean = listLinks.find((link) => link.isValid === false) !== undefined;

        return (
            <button className='btn btn-xs text-red-400 mt-4' disabled={isDisabled} onClick={() => {
                setListLinks(listLinks.concat([{label: "", link: "", isValid: false}]))
            }
            }>+ Add link</button>
        )
    }

    return (
        <div className='flex flex-col gap-2 w-full items-start'>
            {listLinks.map((listItem, index) => renderListLinkInput(listItem, index))}
            {renderAddButton()}
        </div>
    )
}