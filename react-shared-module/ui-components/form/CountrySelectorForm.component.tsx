import {useMemo, useState} from "react";
import countryList from 'react-select-country-list'
import DropdownFormComponent from "./DropdownForm.component";
import {FormOption} from "./RadioButton.component";


export default function CountrySelectorFormComponent(props: {defaultVal?: string, onCountrySelected: (countryCode: string, countryLabel: string) => void, disabled?: boolean, hookForm?: any}) {
    const [selectedOptionKey, setSelectedOptionKey] = useState(props.defaultVal);
    const options = useMemo(() => countryList().getData(), []);

    let dropdownOptions: {[optionId: string]: FormOption} = {};
    options.forEach((countryData) => {
        dropdownOptions = {
            ...dropdownOptions,
            [countryData.value]: {
                label: countryData.label,
                optionId: countryData.value,
            },
        };
    });

    function onCountrySelected(optionKey: string) {
        setSelectedOptionKey(optionKey);
        if (optionKey && dropdownOptions[optionKey]) {
            props.onCountrySelected(
                optionKey,
                dropdownOptions[optionKey].label,
            );
        }
    }

    return (<DropdownFormComponent hookForm={props.hookForm} disabled={props.disabled} options={dropdownOptions} currentSelectionKey={selectedOptionKey} defaultLabel={"Pick a country"} label={"Where are you from?"} onOptionSelected={onCountrySelected}/>)
}