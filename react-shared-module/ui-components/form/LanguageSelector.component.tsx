import {FormOption} from "./RadioButton.component";
import DropdownFormComponent from "./DropdownForm.component";
import {getLanguageLabel} from "../../logic/language/language.helper";
import {useState} from "react";

export default function LanguageSelectorComponent(props: { onOptionSelected: (optionKey: string) => void, defaultLanguage?: string, disabled?: boolean, hookForm?: any}) {
    const [selectedOption, setSelectedOption] = useState(props.defaultLanguage ? props.defaultLanguage : LanguageSelectorOptions.englishuk.optionId);

    function onOptionSelected(optionId: string) {
        setSelectedOption(optionId);
        props.onOptionSelected(optionId);
    }

    return (
        <div>
            <DropdownFormComponent label={getLanguageLabel("FORM_LANGUAGE_SELECTOR_TITLE")}
                                   defaultLabel={"Select the language"}
                                   options={LanguageSelectorOptions}
                                   currentSelectionKey={selectedOption}
                                   onOptionSelected={onOptionSelected}
                                   disabled={props.disabled} hookForm={props.hookForm}/>
        </div>
    )
}


export const LanguageSelectorOptions: { [optionId: string]: FormOption } = {
    englishuk: {
        optionId: "English (UK)",
        label: "🇬🇧 English (UK)",
    },
    englishus: {
        optionId: "English (US)",
        label: "🇺🇸 English (US)",
    },
    french: {
        optionId: "French",
        label: "🇫🇷 French",
    },
    spanish: {
        optionId: "Spanish",
        label: "🇪🇸 Spanish",
    },
    german: {
        optionId: "German",
        label: "🇩🇪 German",
    },
    italian: {
        optionId: "Italian",
        label: "🇮🇹 Italian",
    },
    dutch: {
        optionId: "Dutch",
        label: "🇳🇱 Dutch",
    },
    portuguese: {
        optionId: "Portuguese (PT)",
        label: "🇵🇹 Portuguese",
    },
    portuguesebr: {
        optionId: "Portuguese (BR)",
        label: "🇧🇷 Portuguese (BR)",
    },
}