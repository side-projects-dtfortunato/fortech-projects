import {HTMLInputTypeAttribute, useEffect, useRef, useState} from "react";

export default function InputTextComponent(props: {topLabel: string, hint: string, disabled?: boolean, onChanged: (text: string) => void, defaultValue?: string, inputType?: HTMLInputTypeAttribute, maxLength?: number, value?: string, hookForm?: any,
    size?: "input-lg" | "input-md" | "input-sm" | "input-xs", onEnterPressed?: () => void, className?: string, autoFocus?: boolean}) {
    let inputType = props.inputType ? props.inputType : "text";
    const [textForm, setTextForm] = useState(props.defaultValue ? props.defaultValue : "");
    const inputRef = useRef(null);

    useEffect(() => {
        // This will make the input get focus immediately when the component mounts
        if (props.autoFocus && inputRef.current) {
            (inputRef.current as any).focus();
        }
    }, []);
    function onTextChanged(text: string) {
        if (text !== props.value) {
            props.onChanged(text);
        }
        setTextForm(text);
    }

    function renderMaxLength() {
        if (props.maxLength) {
            return (
                <span className={"text-xs text-slate-300 font-light"}>{textForm?.length + "/" + props.maxLength}</span>
            )
        } else {
            return <></>
        }
    }

    function handleKeyPress(event: React.KeyboardEvent<HTMLInputElement>) {
        if (event.key === "Enter") {
            // Call your custom callback function here
            if (props.onEnterPressed) {
                props.onEnterPressed();
            }
        }
    }

    const inputClassName: string = props.className ? props.className : "input input-bordered w-full " + (props.size ? props.size : "");

    return (
        <div className="w-full">
            <label className={"label " + (props.topLabel.length > 0 ? "" : "hidden")}>
                <span className="label-text font-bold text-xs md:text-base">{props.topLabel}</span>
            </label>
            <input ref={inputRef} type={inputType} {...props.hookForm} maxLength={props.maxLength} placeholder={props.hint}
                   disabled={props.disabled} className={inputClassName}
                   defaultValue={props.defaultValue} value={props.value}
                   onChange={(htmlEl) => onTextChanged(htmlEl.target.value)}
                   onKeyPress={(event) => handleKeyPress(event)}/>
            {renderMaxLength()}
        </div>
    )
}