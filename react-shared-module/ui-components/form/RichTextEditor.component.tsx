import {useEffect, useState} from "react";
import 'react-quill/dist/quill.snow.css';
import CommonUtils from "../../logic/commonutils";

export default function RichTextEditorComponent(props: { value?: string,  onTextChanged: (text: string) => void, height?: number, toolBarType?: "MINIMAL" | "NORMAL" }) {
    const [value, setValue] = useState(props.value);

    useEffect(() => {
        if (props.value !== value) {
            setValue(props.value);
        }
    }, [props.value]);

    function onTextChanged(text: string) {
        props.onTextChanged(text);
        setValue(text);
    }

    if (CommonUtils.isClientSide()) {
        const ReactQuill = require('react-quill')

        let toolbarOptions = [
            [{'header': [1, 2, 3, 4, false]}],
            [{align: ''}, {align: 'center'}, {align: 'right'}, {align: 'justify'}],
            ['bold', 'italic', 'underline', 'strike', 'blockquote'],
            [{'list': 'ordered'}, {'list': 'bullet'}, {'indent': '-1'}, {'indent': '+1'}],
            ['link'],
            // ['link', 'image'],
            ['clean']
        ];

        if (props.toolBarType === "MINIMAL") {
            toolbarOptions = [
                [{'header': [1, 2, 3, 4, false]}],
                ['bold', 'italic', 'underline',],
            ];
        }

        const modules = {
            toolbar: toolbarOptions,
        };

        return (
            <article className='prose flex w-full pb-10 grow bg-white'
                     style={{minHeight: props.height ? props.height : 400}}>
                <ReactQuill className='w-full' theme="snow" value={value} modules={modules} onChange={onTextChanged}/>
            </article>
        );
    } else {
        return (<></>);
    }

}