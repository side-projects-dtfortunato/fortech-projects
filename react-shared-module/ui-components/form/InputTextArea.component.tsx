import {HTMLInputTypeAttribute, useState} from "react";

export default function InputTextAreaComponent(props: {topLabel: string, hint: string, disabled?: boolean, onChanged: (text: string) => void, defaultValue?: string, value?: string, inputType?: HTMLInputTypeAttribute, maxLength?: number, hookForm?: any, customHeight?: string}) {
    let inputType = props.inputType ? props.inputType : "text";
    const [textForm, setTextForm] = useState(props.defaultValue ? props.defaultValue : "");

    function onTextChanged(text: string) {
        if (text !== props.value) {
            props.onChanged(text);
        }
        setTextForm(text);
    }

    function renderMaxLength() {
        if (props.maxLength) {
            return (
                <span className={"text-xs text-slate-300 font-light"}>{textForm?.length + "/" + props.maxLength}</span>
            )
        } else {
            return <></>
        }
    }

    return (
        <div className="w-full">
            <label className="label">
                <span className="label-text font-bold text-base">{props.topLabel}</span>
            </label>
            <textarea className={`p-2 rounded-xl textarea textarea-bordered w-full ${props.customHeight ? props.customHeight : ""}`} {...props.hookForm} disabled={props.disabled} placeholder={props.hint} maxLength={props.maxLength} defaultValue={props.defaultValue} value={props.value} onChange={(htmlEl) => onTextChanged(htmlEl.target.value)}/>
            {renderMaxLength()}
        </div>
    )
}