import EditorJS from '@editorjs/editorjs';
import {useEffect, useRef} from "react";
import CommonUtils from "../../logic/commonutils";
import {ArticleEditorImageFileBlockModel} from "../../logic/shared-data/datamodel/posts-model/post-article-blocks";

const Embed =  require('@editorjs/embed');
const Table =  require('@editorjs/table');
const List =  require('@editorjs/list');
const Code =  require('@editorjs/code');
const Image =  require('@editorjs/image');
const Raw =  require('@editorjs/raw');
const Header =  require('@editorjs/header');
const Quote =  require('@editorjs/quote');
const Marker =  require('@editorjs/marker');
const Delimiter =  require('@editorjs/delimiter');
const ColorPlugin =  require('editorjs-text-color-plugin');

export const EDITOR_JS_TOOLS = {
    header: {
        class: Header,
        inlineToolbar: true,
    },
    image: {
        class: Image,
        inlineToolbar: true,
    },
    embed: {
        class: Embed,
        inlineToolbar: true,
    },
    table: {
        class: Table,
        inlineToolbar: true,
    },
    Color: {
        class: ColorPlugin, // if load from CDN, please try: window.ColorPlugin
        config: {
            colorCollections: ['#EC7878','#9C27B0','#673AB7','#3F51B5','#0070FF','#03A9F4','#00BCD4','#4CAF50','#8BC34A','#CDDC39', '#FFF'],
            defaultColor: '#FF1300',
            type: 'text',
            customPicker: true // add a button to allow selecting any colour
        }
    },
    Marker: {
        class: ColorPlugin, // if load from CDN, please try: window.ColorPlugin
        config: {
            defaultColor: '#FFBF00',
            type: 'marker',
            icon: `<svg fill="#000000" height="200px" width="200px" version="1.1" id="Icons" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 32 32" xml:space="preserve"><g id="SVGRepo_bgCarrier" stroke-width="0"></g><g id="SVGRepo_tracerCarrier" strokeLinecap="round" stroke-linejoin="round"></g><g id="SVGRepo_iconCarrier"> <g> <path d="M17.6,6L6.9,16.7c-0.2,0.2-0.3,0.4-0.3,0.6L6,23.9c0,0.3,0.1,0.6,0.3,0.8C6.5,24.9,6.7,25,7,25c0,0,0.1,0,0.1,0l6.6-0.6 c0.2,0,0.5-0.1,0.6-0.3L25,13.4L17.6,6z"></path> <path d="M26.4,12l1.4-1.4c1.2-1.2,1.1-3.1-0.1-4.3l-3-3c-0.6-0.6-1.3-0.9-2.2-0.9c-0.8,0-1.6,0.3-2.2,0.9L19,4.6L26.4,12z"></path> </g> <g> <path d="M28,29H4c-0.6,0-1-0.4-1-1s0.4-1,1-1h24c0.6,0,1,0.4,1,1S28.6,29,28,29z"></path> </g> </g></svg>`
        }
    },
    list: {
        class: List,
        inlineToolbar: true,
    },
    code: Code,
    raw: {
        class: Raw,

    },
    quote: Quote,
    delimiter: Delimiter,
}

const MAX_SIZE_IMAGE = 1000000;


export function InputRichEditorJSComponent(props: {initialData?: any, onDataChanged: (data: any) => void}) {

    const ejInstance = useRef<any>(null);

    const initEditor = () => {
        const editor = new EditorJS({
            minHeight: 75,
            holder: 'editorjs',
            onReady: () => {
                ejInstance.current = editor;
            },
            placeholder: "Click here to start writing your article...",
            data: props.initialData,
            onChange: async () => {
                let content = await editor.saver.save();
                props.onDataChanged(content);
            },
            tools: {
                ...EDITOR_JS_TOOLS,
                image: {
                    class: Image,
                    config: {
                        uploader: {
                            uploadByFile: async (file: File) => {
                                return new Promise(async (resolve: any, reject: any)=> {
                                    let base64: string = await CommonUtils.fileToBase64(file);
                                    base64 = await CommonUtils.resizeBase64Image(base64, MAX_SIZE_IMAGE); // MAX 1mb
                                    resolve({
                                        "success" : 1,
                                        "file": {
                                            "url" : base64,
                                            "isServerSaved": false,
                                        } as ArticleEditorImageFileBlockModel,
                                    })
                                });
                            },
                            uploadByUrl: async (url: string) => {
                                return new Promise((resolve: any, reject: any)=> {
                                    resolve({
                                        "success" : 1,
                                        "file": {
                                            "url" : url,
                                            "isServerSaved": false,
                                        } as ArticleEditorImageFileBlockModel,
                                    })
                                });
                            }
                        }
                    }
                }
            },
        });

        editor.isReady.then(() => {
        });
    };

    // This will run only once
    useEffect(() => {
        if (ejInstance.current === null) {
            initEditor();
        }

        return () => {
            ejInstance?.current?.destroy();
            ejInstance.current = null;
        };
    }, []);

    return  <><div id='editorjs' className={"flex w-full"}></div></>;
}
