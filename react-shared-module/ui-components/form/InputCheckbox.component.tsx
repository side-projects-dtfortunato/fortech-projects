export default function InputCheckboxComponent(props: {label: string, isChecked: boolean, onCheckedChanged: (isChecked: boolean) => void, hookForm?: any}) {
    function onCheckboxClick(event: any) {
        props.onCheckedChanged(event.target.checked);
    }

    return (
        <div className="flex flex-row align-items-center justify-content-start">
            <label className="label cursor-pointer gap-2">
                <input type="checkbox" {...props.hookForm} defaultChecked={props.isChecked} className="checkbox" onChange={onCheckboxClick}/>
                <span className="label-text">{props.label}</span>
            </label>
        </div>
    )
}