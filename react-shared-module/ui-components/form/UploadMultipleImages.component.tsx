import {StorageFileModel} from "../../logic/shared-data/datamodel/storage-file.model";
import {useState} from "react";
import UploadImageComponent from "./UploadImage.component";
import {MdDelete} from "react-icons/md";

const IMAGE_PLACEHOLDER_SIZE = 100;

export default function UploadMultipleImagesComponent(props: {defaultSelectedImages?: StorageFileModel[], onSelectedImagesChanged: (selectedImages: StorageFileModel[]) => void, maxImages: number}) {
    const [selectedImageFiles, setSelectedImageFiles] = useState<StorageFileModel[]>(props.defaultSelectedImages ? props.defaultSelectedImages : []);


    function onDeleteImage(imageFile: StorageFileModel) {
        setSelectedImageFiles(selectedImageFiles.filter((image) => image !== imageFile));
    }

    function onSelectedImagesChanges() {
        props.onSelectedImagesChanged(selectedImageFiles);
    }

    function renderImagePreviewItem(imageFile?: StorageFileModel) {
        return (
            <div className='flex flex-col gap-2 items-center'>
                <div
                    style={{
                        width: IMAGE_PLACEHOLDER_SIZE + 'px',
                        height: IMAGE_PLACEHOLDER_SIZE + 'px',
                        border: '2px dashed #aaa',
                        borderRadius: '10px',
                        display: 'flex',
                        justifyContent: 'center',
                        alignItems: 'center',
                    }}
                >
                    {
                        imageFile ? <img
                            src={imageFile.tempFile ? URL.createObjectURL(imageFile.tempFile) : imageFile.tempBase64}
                            alt="Selected"
                            style={{ maxWidth: '100%', maxHeight: '100%',
                                borderRadius: '10px', objectFit: "cover"}}
                        /> : <></>
                    }
                </div>
                {
                    imageFile ?
                        <button className='btn btn-error btn-xs' onClick={() => onDeleteImage(imageFile!)}><MdDelete/> Delete</button> : <></>
                }
            </div>
        )
    }

    function renderListImages() {
        let listItems: any[] = selectedImageFiles.map((item) => renderImagePreviewItem(item));

        if (selectedImageFiles.length < props.maxImages) {
            listItems.push(<UploadImageComponent customId={"MultiImagesUpload"} size={IMAGE_PLACEHOLDER_SIZE} onImageBase64={async (base64Image) => {
                if (base64Image) {
                    setSelectedImageFiles(selectedImageFiles.concat([{
                        tempBase64: base64Image,
                        fileUrl: "",
                        storagePath: "",
                    }]));
                    onSelectedImagesChanges();
                }
            }
            } disablePreview={true} />)
        }


        return (
            <div className='flex flex-wrap gap-2'>
                {listItems}
            </div>
        )
    }

    return (
        <div className='flex flex-col gap-2'>
            {renderListImages()}
        </div>
    )
}