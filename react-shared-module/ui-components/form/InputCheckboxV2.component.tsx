export default function InputCheckboxV2Component(props: {label: string, isChecked: boolean, onCheckedChanged: (isChecked: boolean) => void}) {
    return (
        <div className="form-control justify-start">
            <label className="label cursor-pointer form-control flex justify-start gap-5">
                <span className="label-text font-bold text-lg">{props.label}</span>
                <input type="checkbox" checked={props.isChecked}
                       className="checkbox" onClick={() => {props.onCheckedChanged(!props.isChecked)}}/>
            </label>
        </div>
    )
}