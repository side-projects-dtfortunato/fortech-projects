import {TagsInput} from "react-tag-input-component";

export default function InputTagsFieldComponent(props: {defaultTags: string[], disabled?: boolean, onTagsChanged: (tags: string[]) => void, label: string, placeholder: string}) {

    return (
        <div className='flex flex-col gap-2 w-full'>
            <span className='font-bold text-base'>{props.label}</span>
            <TagsInput
                value={props.defaultTags}
                onChange={props.onTagsChanged}
                name={props.label}
                placeHolder={props.disabled ? undefined : props.placeholder}
                disabled={props.disabled}
            />
        </div>
    )
}