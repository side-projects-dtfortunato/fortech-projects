import {FormOption} from "./RadioButton.component";
import {ChangeEvent} from "react";

export default function DropdownFormComponent(props: {label: string, defaultLabel: string, options: {[optionId: string]: FormOption}, onOptionSelected: (optionKey: string) => void, currentSelectionKey?: string, disabled?: boolean, hookForm?: any}) {
    let listOptions: any[] = [];

    Object.values(props.options).filter((option) => option.displayOption || option.displayOption === undefined).forEach((optionItem) => {
        listOptions.push((
            <option key={optionItem.optionId} value={optionItem.optionId}
                    selected={optionItem.optionId === props.currentSelectionKey}>{optionItem.label}</option>
        ));
    });

    function onOptionSelected(event: ChangeEvent<HTMLSelectElement>) {
        if (props.currentSelectionKey !== props.onOptionSelected(event.target.value)) {
            props.onOptionSelected(event.target.value);
        }
    }

    return (
        <div className="max-w-sm">
            <label className="label">
                <span className="font-bold text-base">{props.label}</span>
            </label>
            <select className="select select-bordered max-w-xs" {...props.hookForm} value={props.currentSelectionKey} onChange={onOptionSelected} disabled={props.disabled}>
                <option disabled {...props.hookForm} value="" key={"default"}>{props.defaultLabel}</option>
                {listOptions}
            </select>
        </div>
    )
}