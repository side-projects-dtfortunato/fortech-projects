import {LuImagePlus} from "react-icons/lu";
import {useEffect, useState} from "react";
import InlineLabelSeparatorComponent from "../shared/inline-label-separator.component";
import CommonUtils from "../../logic/commonutils";


const MAX_SIZE_IMAGE = 1000000;

export default function UploadImageComponent(props: {title?: string, size: number,
    onImageSelected?: (file: File) => void,
    onImageBase64?: (image: string) => void,
    defaultImage?: string, recommendedSize?: string, customId?: string, displayButton?: boolean, disablePreview?: boolean}) {

    const [selectedImage, setSelectedImage] = useState(props.defaultImage);

    useEffect(() => {
        if (props.defaultImage && props.defaultImage !== selectedImage) {
            setSelectedImage(props.defaultImage);
        }
    }, [props.defaultImage])

    const handleImageUpload = (e: any) => {
        const file = e.target.files[0];
        if (!props.disablePreview) {
            setSelectedImage(URL.createObjectURL(file));
        }
        if (props.onImageSelected) {
            props.onImageSelected(file);
        }
        if (props.onImageBase64) {
            CommonUtils.fileToBase64(file).then(async (base64: string) => {
                props.onImageBase64!(await CommonUtils.resizeBase64Image(base64, MAX_SIZE_IMAGE));
            });
        }
    };

    function renderTitle() {
        if (props.title) {
            return (
                <InlineLabelSeparatorComponent title={props.title} />
            )
        } else {
            return <></>;
        }
    }

    return (
        <div className='flex flex-col gap-2'>
            {renderTitle()}
            <label htmlFor={props.customId ? props.customId : "imageInput"} className='flex flex-row items-center start gap-2'>
                <div
                    style={{
                        width: props.size + 'px',
                        height: props.size + 'px',
                        border: '2px dashed #aaa',
                        borderRadius: '10px',
                        display: 'flex',
                        justifyContent: 'center',
                        alignItems: 'center',
                        cursor: 'pointer',
                    }}
                >
                    {selectedImage ? (
                        <img
                            src={selectedImage}
                            alt="Selected"
                            style={{ maxWidth: '100%', maxHeight: '100%',
                                borderRadius: '10px', }}
                        />
                    ) : (
                        <LuImagePlus color={"#aaa"} size={(props.size/2) + "px"} />
                    )}
                </div>
                {
                    props.displayButton ?

                        <div className='flex flex-col items-start gap-2'>
                            <div className='btn btn-xs btn-outline btn-primary'>Select an image</div>
                            <span className='text-xs font-light'>Recommended size: {props.recommendedSize ? props.recommendedSize : (props.size + "x"+props.size)} | JPG, PNG, GIF. Max size: 2MB</span>
                        </div>
                        : <></>
                }
            </label>
            <input
                type="file"
                id={props.customId ? props.customId : "imageInput"}
                accept="image/*"
                style={{ display: 'none' }}
                onChange={handleImageUpload}
            />
        </div>
    );
}