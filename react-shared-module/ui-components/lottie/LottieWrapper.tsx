import React, { lazy, Suspense, useEffect, useState } from 'react';

const Lottie = lazy(() => import('lottie-react'));

interface LottieWrapperProps {
    iconPath: string;
}

const LottieWrapper: React.FC<LottieWrapperProps> = ({ iconPath }) => {
    const [animationData, setAnimationData] = useState<any>(null);

    useEffect(() => {
        loadLottieUrl(iconPath).then((response) => {
            setAnimationData(response);
        });
    }, [iconPath]);

    if (!animationData) {
        return <div className="w-full h-full bg-gray-200 animate-pulse"></div>; // Placeholder with loading animation
    }

    return (
        <Suspense fallback={<div className="w-full h-full bg-gray-200 animate-pulse"></div>}>
            <Lottie animationData={animationData} loop={false} />
        </Suspense>
    );
};

const loadLottieUrl = async (url: string) => {
    const response = await fetch(url);
    return response.json();
};

export default LottieWrapper;