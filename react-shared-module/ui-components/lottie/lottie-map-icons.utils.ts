export const LottieMapIconsUtils = {
    lottie_success_icon: "/lottie/lottie_success_icon.json",
    lottie_waiting_payment: "/lottie/lottie_waiting_payment.json",
    lottiePaymentConfirmedIcon: "/lottie/lottie_payment_confirmed.json",
    lottieWikiGuideIcon: "/lottie/lottie_wiki_guide_icon.json",
    lottieWebsiteAnalyzing: "/lottie/lottie_website_analyzing.json",
}