export default function BaseRootContainerComponent(props: {children: any, extraLarge?: boolean}) {


    return (
        <div className={'flex justify-center items-center w-full ' + (props.extraLarge ? "max-w-4xl" : "max-w-3xl")}>
            {props.children}
        </div>
    )
}