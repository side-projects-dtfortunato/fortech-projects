export default function PageHeaderTitleComponent(props: {icon?: any, title: string, subtitle: string}) {


    return (
        <div className='flex flex-col justify-center items-center gap-2 mt-5'>
            <div className='flex flex-col justify-center items-center gap-4'>
                {props.icon ? props.icon : <></>}
                <h1 className='text-2xl md:text-4xl text-center font-bold '>{props.title}</h1>
            </div>
            <h4 className='text-slate-500 text-center text-sm md:lg'>{props.subtitle}</h4>
        </div>
    )
}