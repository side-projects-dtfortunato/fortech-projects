
// Inspiration: https://shuffle.dev/editor?project=a7f14ec929a762ab58302746a31fbcea04dff770&tutorial=1
export default function BaseStepFormContainerComponent(props: {title: string, subtitle: string, children: any}) {

    return (
        <div className="flex flex-wrap -m-3 mb-3">
            <div className="flex flex-col gap-3 w-full md:w-1/4 p-3">
                <h2 className="text-coolGray-900 text-xl md:text-2xl font-semibold">{props.title}</h2>
                <p className="text-xs text-coolGray-500 font-medium">{props.subtitle}</p>
            </div>
            <div className="w-full md:w-3/4 p-3 md:px-20">
                <div className="h-full bg-white rounded-md max-w-xl">
                    {props.children}
                </div>
            </div>
        </div>
    )
}