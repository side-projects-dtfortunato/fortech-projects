import {UserShortProfileModel} from "../../logic/shared-data/datamodel/shared-public-userprofile.model";
import {getLanguageLabel} from "../../logic/language/language.helper";
import {useEffect, useState} from "react";
import {Avatar} from "@chakra-ui/react";

export default function UserHeaderInfosComponent(props: { userId: string, userInfos?: UserShortProfileModel, isCreator?: boolean}) {
    const [isClient, setIsClient] = useState(false);

    useEffect(() => {
        // This is a workaround to fix an hydration issue that was caused by Link (idky)
        setIsClient(true);
    }, [isClient]);

    function renderCreator() {
        if (props.isCreator) {
            return (<div className='badge badge-xs badge-outline'>{getLanguageLabel("GLOBAL_USER_CREATOR")}</div> )
        } else {
            return (<></>);
        }
    }

    const displayName: string|undefined = props.userInfos?.name ? props.userInfos?.name : props.userInfos?.username;

    if (!isClient) {
        return (<></>)
    } else {
        return (
            <div className='flex flex-row items-center justify-start gap-2'>
                <div className='flex w-8'>
                    <Avatar name={props.userInfos?.name} src={props.userInfos?.pictureUrl} size={"sm"} />
                </div>
                <div className='flex flex-col justify-center'>
                    <div className='flex flex-wrap gap-2 items-center'>
                        <span className='text-base font-bold text-slate-700'>{displayName ? displayName : ""}</span>
                        {renderCreator()}
                        {/* <UserFollowingButtonComponent userIdToFollow={props.userId} /> */}
                    </div>
                    <span className='text-xs text-slate-400'>{props.userInfos?.subtitleLabel}</span>
                </div>
            </div>
        )
    }


}



/*return (
        <Link className='flex flex-row items-center justify-start gap-2' href={SharedRoutesUtils.getPublicProfilePageURL(props.userId, props.userInfos?.username)}>
            <UserPictureAvatarComponent width={"w-8"} pictureUrl={props.userInfos?.pictureUrl} />
            <div className='flex flex-col justify-center'>
                <div className='flex flex-wrap gap-2 items-center'>
                    <span className='text-sm text-black'>{props.userInfos?.name}</span>
                    {renderCreator()}
                    <UserFollowingButtonComponent userIdToFollow={props.userId} />
                </div>
                <span className='text-xs text-slate-400'>{props.userInfos?.subtitleLabel}</span>
            </div>
        </Link>
    ) */

/*
<Link className='flex flex-row items-center justify-start gap-2' href={profileUrl}>
                <UserPictureAvatarComponent width={"w-8"} pictureUrl={props.userInfos?.pictureUrl} />
                <div className='flex flex-col justify-center'>
                    <div className='flex flex-wrap gap-2 items-center'>
                        <span className='text-sm text-black'>{props.userInfos?.name}</span>
                        {renderCreator()}
                        <UserFollowingButtonComponent userIdToFollow={props.userId} />
                    </div>
                    <span className='text-xs text-slate-400'>{props.userInfos?.subtitleLabel}</span>
                </div>
            </Link>

 */