import RootConfigs from "../../../../configs";
import ImageAmpComponent from "./ImageAmp.component";


export default function LogoComponent(props: {type?: "TEXT" | "ICON", height?: number, width?: number}) {
    let logoUrl: string = "/images/logo.png";
    if (props.type) {
        switch (props.type) {
            case "ICON":
                logoUrl = "/images/logo-192.png";
                break;

            case "TEXT":
            default:
                logoUrl = "/images/logo.png";
                break;
        }
    }
    return (
        <div className='flex justify-center'>
            <ImageAmpComponent src={logoUrl} height={props.height ? props.height : 250} width={props.width ? props.width : 300} alt={RootConfigs.SITE_NAME + " logo"}/>
        </div>
    )
}