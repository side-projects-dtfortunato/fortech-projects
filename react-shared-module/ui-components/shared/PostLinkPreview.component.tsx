import {MetaData} from "../../logic/shared-data/datamodel/posts-model/post-share-link";
import Link from "next/link";
import CommonUtils from "../../logic/commonutils";
import LinesEllipsis from "react-lines-ellipsis";

export function PostLinkPreviewComponent(props: {linkUrl: string, metadata?: MetaData}) {

    const renderImage = props.metadata?.image ? (
        <div className='self-stretch rounded' style={{backgroundImage: "Url(" + props.metadata?.image + ")", backgroundSize: "cover", width: "100%", height: "100%"}}/>
    ) : (
        <div className='bg-slate-300'/>
    )

    function renderHead() {
        let headLabel: string = "";
        if (props.metadata?.provider) {
            headLabel += props.metadata.provider;
        }
        if (props.metadata?.author) {
            if (headLabel.length > 0) {
                headLabel += " - ";
            }
            headLabel += props.metadata?.author!;
        }
        const renderIcon = props.metadata?.icon ? (<img className='m-0' src={props.metadata?.icon} width={16} height={16} />) : (<></>)

        if (headLabel) {
            return (
                <div className="flex flex-row gap-2 items-start justify-start">
                    {renderIcon}
                    <span className='text-slate-400 text-xs'>{headLabel}</span>
                </div>
            )
        } else {
            return (<></>);
        }
    }

    let linkUrl: string = props.linkUrl;
    linkUrl = CommonUtils.addLinkSource(linkUrl);

    return (
        <Link href={linkUrl} target="_blank">
            {/** Landscape*/}
            <div className='hidden sm:flex'>
                <div className='flex flex-row list-item-bg gap-2 drop-shadow p-2  hover:bg-slate-100'>
                    <div className='basis-1/5' style={{minWidth: 125, height:125}}>
                        {renderImage}
                    </div>
                    <article className='basis-3/4 prose'>
                        <div className='flex flex-col gap-1 px-3 items-start justify-start'>
                            <div>
                                {renderHead()}
                            </div>
                            <h2 className='text-lg font-bold m-0'>{props.metadata?.title}</h2>
                            <LinesEllipsis text={props.metadata?.description} maxLine='4'
                                           ellipsis='...'
                                           trimRight
                                           basedOn='letters'/>
                        </div>
                    </article>
                </div>
            </div>

            {/** Mobile*/}
            <div className='sm:hidden'>
                <div className='flex flex-col list-item-bg gap-2 drop-shadow p-2  hover:bg-slate-100'>
                    <div className='w-full' style={{minWidth: 125, height:125}}>
                        {renderImage}
                    </div>
                    <article className='w-full prose'>
                        <div className='flex flex-col gap-1 px-3 items-start justify-start'>
                            <div>
                                {renderHead()}
                            </div>
                            <h2 className='text-lg font-bold m-0'>{props.metadata?.title}</h2>
                            <LinesEllipsis text={props.metadata?.description} maxLine='4'
                                           ellipsis='...'
                                           trimRight
                                           basedOn='letters'/>
                        </div>
                    </article>
                </div>
            </div>
        </Link>
    )
}