import React, {useCallback, useEffect, useState} from 'react';
import Image from 'next/image';
import {ChevronLeft, ChevronRight, X} from 'lucide-react';
import {AnimatePresence, motion} from 'framer-motion';
import {SwipeEventData, useSwipeable} from 'react-swipeable';

interface ImageGalleryPopupProps {
    images: string[];
    defaultImage: string;
    onClose: () => void;
}

const ImageGalleryPopup: React.FC<ImageGalleryPopupProps> = ({ images, defaultImage, onClose }) => {
    const [currentIndex, setCurrentIndex] = useState(0);
    const [direction, setDirection] = useState(0);

    useEffect(() => {
        const defaultIndex = images.indexOf(defaultImage);
        setCurrentIndex(defaultIndex >= 0 ? defaultIndex : 0);
    }, [images, defaultImage]);

    const handlePrevious = useCallback(() => {
        setDirection(-1);
        setCurrentIndex((prevIndex) => (prevIndex > 0 ? prevIndex - 1 : images.length - 1));
    }, [images.length]);

    const handleNext = useCallback(() => {
        setDirection(1);
        setCurrentIndex((prevIndex) => (prevIndex < images.length - 1 ? prevIndex + 1 : 0));
    }, [images.length]);

    const handleThumbnailClick = (index: number) => {
        setDirection(index > currentIndex ? 1 : -1);
        setCurrentIndex(index);
    };

    const swipeHandlers = useSwipeable({
        onSwipedLeft: (eventData: SwipeEventData) => {
            handleNext();
        },
        onSwipedRight: (eventData: SwipeEventData) => {
            handlePrevious();
        },
        trackMouse: true,
        trackTouch: true,
        delta: 10,
        swipeDuration: 500,
        preventScrollOnSwipe: true,
    });

    const slideVariants = {
        enter: (direction: number) => ({
            x: direction > 0 ? 1000 : -1000,
            opacity: 0,
        }),
        center: {
            zIndex: 1,
            x: 0,
            opacity: 1,
        },
        exit: (direction: number) => ({
            zIndex: 0,
            x: direction < 0 ? 1000 : -1000,
            opacity: 0,
        }),
    };

    return (
        <div className="fixed inset-0 bg-black bg-opacity-75 flex items-center justify-center z-50">
            <div className="bg-white p-4 rounded-lg shadow-lg max-w-4xl w-full">
                <div className="relative">
                    <button
                        className="absolute top-2 right-2 text-gray-500 hover:text-gray-700 z-10"
                        onClick={onClose}
                    >
                        <X size={24} />
                    </button>
                    <div
                        className="relative h-96 w-full overflow-hidden touch-pan-y"
                        {...swipeHandlers}
                    >
                        <AnimatePresence initial={false} custom={direction}>
                            <motion.div
                                key={currentIndex}
                                custom={direction}
                                variants={slideVariants}
                                initial="enter"
                                animate="center"
                                exit="exit"
                                transition={{
                                    x: { type: 'spring', stiffness: 300, damping: 30 },
                                    opacity: { duration: 0.2 },
                                }}
                                className="absolute inset-0"
                            >
                                <Image
                                    src={images[currentIndex]}
                                    alt={`Image ${currentIndex + 1}`}
                                    layout="fill"
                                    objectFit="contain"
                                />
                            </motion.div>
                        </AnimatePresence>
                    </div>
                    <button
                        className="absolute top-1/2 left-4 transform -translate-y-1/2 bg-white bg-opacity-50 hover:bg-opacity-75 rounded-full p-2 focus:outline-none z-10"
                        onClick={handlePrevious}
                    >
                        <ChevronLeft size={24} />
                    </button>
                    <button
                        className="absolute top-1/2 right-4 transform -translate-y-1/2 bg-white bg-opacity-50 hover:bg-opacity-75 rounded-full p-2 focus:outline-none z-10"
                        onClick={handleNext}
                    >
                        <ChevronRight size={24} />
                    </button>
                </div>
                <div className="mt-4 flex justify-center space-x-2 overflow-x-auto">
                    {images.map((image, index) => (
                        <div
                            key={index}
                            className={`w-16 h-16 relative cursor-pointer ${
                                index === currentIndex ? 'ring-2 ring-blue-500' : ''
                            }`}
                            onClick={() => handleThumbnailClick(index)}
                        >
                            <Image
                                src={image}
                                alt={`Thumbnail ${index + 1}`}
                                layout="fill"
                                objectFit="cover"
                                className="rounded"
                            />
                        </div>
                    ))}
                </div>
            </div>
        </div>
    );
};

export default ImageGalleryPopup;