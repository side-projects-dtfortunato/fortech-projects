import {useEffect, useState} from "react";
import {FaSearch} from 'react-icons/fa';
import TagItemComponent from "./TagItem.component";

const MAX_COLLAPSABLE_TAGS = 12;

export interface SearchTagFilterData {
    tagLabel: string,
    tagValue: string,
}
interface SearchBarParams {

}

export default function SearchFieldComponent(props: {
    onSearch: (query: string, tagsSelect: string[]) => void,
    hint?: string,
    filterTags?: SearchTagFilterData[]
}) {
    const [query, setQuery] = useState('');
    const [selectedTags, setSelectedTags] = useState<string[]>([]);
    const [isTagsExpanded, setTagsExpanded] = useState(false);

    useEffect(() => {
        props.onSearch(query, selectedTags);
    }, [query, selectedTags]);

    const handleInputChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        setQuery(e.target.value);
    };

    const toggleTag = (tag: string) => {
        if (selectedTags.includes(tag)) {
            setSelectedTags(selectedTags.filter((t) => t !== tag));
        } else {
            setSelectedTags([...selectedTags, tag]);
        }
    };



    function renderExpandButton() {
        if (props.filterTags && props.filterTags?.length > MAX_COLLAPSABLE_TAGS) {

            return (
                <div className='divider'>
                    <button className='btn btn-ghost btn-xs text-slate-400' onClick={() => setTagsExpanded(!isTagsExpanded)}>{isTagsExpanded ? "Show Less Filters" : "Show More Filters"}</button>
                </div>
            )
        } else {
            return (
                <></>
            )
        }
    }

    function renderTagsFilter() {
        if (props.filterTags && props.filterTags.length > 0) {
            let filterTags: SearchTagFilterData[] = props.filterTags;
            if (filterTags.length > MAX_COLLAPSABLE_TAGS && !isTagsExpanded) {
                filterTags = filterTags.slice(0, MAX_COLLAPSABLE_TAGS);
            }

            return (
                <>
                    <span className='text-slate-400 text-xs'>Filter jobs by tags:</span>
                    <div className="flex flex-wrap gap-1">
                        {filterTags.map((tag, index) => (
                            <div key={index} className="cursor-pointer" onClick={() => toggleTag(tag.tagValue)}>
                                <TagItemComponent label={tag.tagLabel} filled={selectedTags.includes(tag.tagValue)} />
                            </div>
                        ))}
                    </div>
                    {renderExpandButton()}
                </>
            )
        } else {
            return (<></>)
        }
    }

    return (
        <div className='flex flex-col gap-2 items-center w-full p-2'>
            <div className="relative">
                <div className="flex items-center">
                    <span className="absolute inset-y-0 left-2 flex items-center pl-3"> <FaSearch color='#9EA4B0'/></span>
                    <input
                        type="text"
                        placeholder={props.hint ? props.hint : "Search..."}
                        value={query}
                        onChange={handleInputChange}
                        className="input input-bordered w-full rounded-full pl-12 pr-5"
                    />
                </div>
            </div>
            {renderTagsFilter()}
        </div>

    );
}