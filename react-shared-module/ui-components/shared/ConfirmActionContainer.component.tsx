import {CheckIcon, XMarkIcon} from "@heroicons/react/24/outline";

export default function ConfirmActionContainerComponent(props: {label: string, size: number, onCancel: () => void, onConfirm: () => void}) {
    return (
        <div className='list-item-bg drop-shadow p-1 bg-slate-50 flex flex-col justify-center items-center gap-2'>
            <span className='custom subtitle text-sm'>{props.label}</span>
            <div className='flex flex-row gap-1'>
                <button className='btn btn-error btn-ghost btn-xs' onClick={props.onCancel}><XMarkIcon height={props.size} color={'#FF0000'} /></button>
                <button className='btn btn-success btn-ghost btn-xs' onClick={props.onConfirm}><CheckIcon height={props.size} color={'#337537'} /></button>
            </div>
        </div>
    )
}