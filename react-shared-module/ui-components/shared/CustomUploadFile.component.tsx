export default function CustomUploadFileComponent(props: {children: any, onFileSelected: (file: File) => void}) {


    const handleFileUpload = (e: any) => {
        const file = e.target.files[0];
        props.onFileSelected(file);
    };

    return (
        <div>
            <label htmlFor="fileInput">
                {props.children}
            </label>
            <input
                type="file"
                id="fileInput"
                style={{ display: 'none' }}
                onChange={handleFileUpload}
            />
        </div>
    );
}