import Link from "next/link";

export default function NotFoundContentComponent() {
    return (
        <div className='h-screen flex flex-col justify-start items-center gap-5 pt-20'>
            <Link className={'px-10 sm:px-20 mx-9'} href={"/"}>
                <img src={"/images/logo.png"}/>
            </Link>
            <h1 className='font-bold text-9xl mt-10'>404</h1>
            <h2 className='my-4 p-2 text-4xl text-black text-center'>Page Not Found</h2>
            <span className='my-2 text-center'>Please, try again later or contact us to report the problem</span>
            <Link href='/' className='btn btn-outline'>Back home</Link>
        </div>
    )
}