import React from 'react';
import Markdown from 'react-markdown';
import GoogleAdsComponent, {AdsSlots} from "../ads/GoogleAds.component";

interface BaseTextComponentProps {
    text: string;
    extraClassName?: string;
}

const ResponsiveIframeWrapper: React.FC<React.PropsWithChildren> = ({ children }) => {
    return (
        <div className="relative w-full pb-[56.25%]">
            <div className="absolute top-0 left-0 w-full h-full">
                {children}
            </div>
        </div>
    );
};

const IframeRenderer: React.FC<React.IframeHTMLAttributes<HTMLIFrameElement>> = (props) => {
    return (
        <ResponsiveIframeWrapper>
            <iframe {...props} className="w-full h-full" />
        </ResponsiveIframeWrapper>
    );
};

const TableRenderer: React.FC<React.TableHTMLAttributes<HTMLTableElement>> = ({ children, ...props }) => {
    return (
        <div className="overflow-x-auto">
            <table {...props} className="table-auto border-collapse border border-gray-300 w-full">
                {children}
            </table>
        </div>
    );
};

const TableCellRenderer: React.FC<React.TdHTMLAttributes<HTMLTableCellElement>> = (props) => {
    return <td {...props} className="border border-gray-300 px-4 py-2" />;
};

const TableRowRenderer: React.FC<React.HTMLAttributes<HTMLTableRowElement>> = (props) => {
    return <tr {...props} className="bg-white even:bg-gray-100" />;
};

const TableHeaderRenderer: React.FC<React.ThHTMLAttributes<HTMLTableHeaderCellElement>> = (props) => {
    return <th {...props} className="border border-gray-300 px-4 py-2 font-bold bg-gray-200" />;
};

const ImageRenderer: React.FC<React.ImgHTMLAttributes<HTMLImageElement>> = (props) => {
    return <img {...props} className="max-w-full h-auto my-4" />;
};

const AdComponent: React.FC<{}> = ({}) => {
    return (
        <div className="my-8">
            <GoogleAdsComponent slot={AdsSlots.IN_ARTICLE} format={"fluid"} layout={"in-article"} />
        </div>
    );
};

export default function BaseTextComponent({ text, extraClassName = '' }: BaseTextComponentProps) {
    const renderContent = () => {
        const parts = text.split(/(<iframe.*?<\/iframe>|<table[\s\S]*?<\/table>|<img.*?\/>)/);
        const renderedParts: JSX.Element[] = [];

        parts.forEach((part, index) => {
            if (part.startsWith('<iframe')) {
                const srcMatch = part.match(/src="(.*?)"/);
                const titleMatch = part.match(/title="(.*?)"/);

                renderedParts.push(
                    <IframeRenderer
                        key={`iframe-${index}`}
                        src={srcMatch ? srcMatch[1] : ''}
                        title={titleMatch ? titleMatch[1] : ''}
                        frameBorder="0"
                        allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share"
                        allowFullScreen
                    />
                );
            } else if (part.startsWith('<table')) {
                renderedParts.push(
                    <Markdown
                        key={`table-${index}`}
                        components={{
                            table: TableRenderer,
                            td: TableCellRenderer,
                            tr: TableRowRenderer,
                            th: TableHeaderRenderer,
                        }}
                    >
                        {part}
                    </Markdown>
                );
            } else if (part.startsWith('<img')) {
                const srcMatch = part.match(/src="(.*?)"/);
                const altMatch = part.match(/alt="(.*?)"/);
                const loadingMatch = part.match(/loading="(.*?)"/);

                renderedParts.push(
                    <ImageRenderer
                        key={`img-${index}`}
                        src={srcMatch ? srcMatch[1] : ''}
                        alt={altMatch ? altMatch[1] : ''}
                        loading={loadingMatch ? loadingMatch[1] as "lazy" | "eager" : undefined}
                    />
                );
            } else {
                const paragraphs = part.split('\n\n');
                paragraphs.forEach((paragraph, pIndex) => {
                    renderedParts.push(<Markdown key={`p-${index}-${pIndex}`}>{paragraph}</Markdown>);

                    // Insert ads after every 4 paragraphs
                    if ((renderedParts.length + 1) % 5 === 0 && renderedParts.length > 0) {
                        renderedParts.push(<AdComponent key={`ad-${index}-${pIndex}`} />);
                    }
                });
            }
        });

        return renderedParts;
    };

    return (
        <div className={`text-base sm:text-lg text-slate-700 ${extraClassName}`}>
            {renderContent()}
        </div>
    );
}