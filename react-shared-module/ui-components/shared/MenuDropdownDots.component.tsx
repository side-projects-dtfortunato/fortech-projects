import {EllipsisVerticalIcon} from "@heroicons/react/24/outline";

export interface MenuDropdownDotsItemModel {
    icon?: any,
    label: string,
    onClicked: () => void,
};

export default function MenuDropdownDotsComponent(props: {listItems: MenuDropdownDotsItemModel[]}) {

    function renderMenuItems() {
        return props.listItems.map((item) => {
            return (
                <li key={item.label} className='flex flex-row gap-2' onClick={item.onClicked}>
                    {item.icon ? item.icon : <></>}
                    <span>{item.label}</span>
                </li>
            )
        });
    }

    return (
        <div className="dropdown dropdown-open">
            <button className="btn btn-square btn-ghost">
                <EllipsisVerticalIcon height={10} width={10} color={"#333333"} />
            </button>
            <ul tabIndex={0} className="dropdown-content menu p-2 shadow bg-base-100 rounded-box w-52">
                {renderMenuItems()}
            </ul>
        </div>
    )
}