import React from 'react';
import { ShoppingCart, CreditCard, ArrowRight } from "lucide-react";
import SpinnerComponent from "./Spinner.component";
import RootConfigs from "../../../../configs";

export default function PaymentLoadingComponent() {
    return (
        <div className="flex flex-col items-center justify-center min-h-[400px] w-full bg-gradient-to-b from-white to-gray-50 rounded-xl shadow-xl p-12 relative overflow-hidden">
            {/* Animated background patterns */}
            <div className="absolute inset-0 overflow-hidden">
                <div className="absolute inset-0 opacity-5">
                    <div className="absolute top-0 -right-4 w-96 h-96 bg-gradient-radial from-primary/20 to-transparent rounded-full blur-2xl transform rotate-12 animate-pulse-subtle" />
                    <div className="absolute -bottom-20 -left-4 w-96 h-96 bg-gradient-radial from-primary/20 to-transparent rounded-full blur-2xl transform -rotate-12 animate-pulse-subtle" />
                </div>
                <div className="absolute inset-0 bg-grid-primary/[0.02] bg-[size:20px_20px]" />
            </div>

            {/* Main content */}
            <div className="relative z-10 flex flex-col items-center space-y-8">
                {/* Icon container with animation */}
                <div className="relative">
                    <div className="p-8 rounded-2xl bg-white shadow-lg border border-gray-100 relative">
                        <div className="flex items-center space-x-8 animate-pulse-subtle">
                            {/* Shopping Cart */}
                            <div className="relative">
                                <ShoppingCart className={`w-8 h-8 text-${RootConfigs.THEME.PRIMARY_COLOR}-500`} />
                                <div className={`absolute -top-1 -right-1 w-3 h-3 rounded-full bg-${RootConfigs.THEME.PRIMARY_COLOR}-500`} />
                            </div>
                            
                            {/* Arrow animation */}
                            <div className="flex items-center space-x-2">
                                <ArrowRight className={`w-6 h-6 text-${RootConfigs.THEME.PRIMARY_COLOR}-400 animate-slide-right`} />
                                <ArrowRight className={`w-6 h-6 text-${RootConfigs.THEME.PRIMARY_COLOR}-300 animate-slide-right-delay-1`} />
                            </div>
                            
                            {/* Credit Card */}
                            <div className="relative transform hover:scale-105 transition-transform">
                                <CreditCard className={`w-8 h-8 text-${RootConfigs.THEME.PRIMARY_COLOR}-500`} />
                                <div className={`absolute -bottom-2 left-1/2 transform -translate-x-1/2 w-8 h-1 bg-${RootConfigs.THEME.PRIMARY_COLOR}-500/20 rounded-full blur-sm`} />
                            </div>
                        </div>
                    </div>
                </div>

                <div className="space-y-3 text-center max-w-md">
                    <h3 className="text-2xl font-bold text-gray-800 tracking-tight">
                        Preparing Secure Checkout
                    </h3>
                    <p className="text-gray-500 text-base leading-relaxed">
                        We&apos;re connecting you to our secure payment system.
                        <span className="block mt-1 text-sm">This will only take a moment.</span>
                    </p>
                </div>

                {/* Enhanced Loading Indicator */}
                <div className="w-80 flex flex-col items-center space-y-4">
                    <div className="w-full h-1 bg-gray-100 rounded-full overflow-hidden">
                        <div className={`h-full bg-${RootConfigs.THEME.PRIMARY_COLOR}-500/80 animate-progress-infinite rounded-full`} 
                             style={{ width: '100%' }}>
                        </div>
                    </div>
                    <div className="flex items-center justify-center space-x-2">
                        <span className={`inline-block w-1.5 h-1.5 rounded-full bg-${RootConfigs.THEME.PRIMARY_COLOR}-500 animate-loading-dot-1`}></span>
                        <span className="text-sm text-gray-400">
                            Initializing checkout...
                        </span>
                    </div>
                </div>

                {/* Enhanced Note */}
                <div className="flex items-center justify-center space-x-2 bg-gray-50/80 backdrop-blur-sm px-5 py-2.5 rounded-full border border-gray-100">
                    <span className="text-xs text-gray-500">Please don&apos;t close this window</span>
                </div>
            </div>
        </div>
    );
}