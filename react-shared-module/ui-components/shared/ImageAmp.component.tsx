import {useAmp} from "next/amp";
import Image from "next/image";

export default function ImageAmpComponent(props: {src: string, width: number,  height: number, alt: string}) {
    const isAmp = useAmp();

    return isAmp ? (
            // @ts-ignore
            <amp-img
                width={props.width}
                height={props.height}
                src={props.src}
                alt={props.alt}
                layout="responsive"
            />
        ) : (
            <Image src={props.src} width={props.width} height={props.height} alt={props.alt} />
        )
}