export default function NotFoundSimpleMessageComponent(props: {message: string}) {

    return (
        <div className='w-full mx-3 my-8 flex items-center justify-center'>
            <h3 className='text-slate-400 custom'>{props.message}</h3>
        </div>
    )
}