import React, {useEffect, useState} from 'react';
import {FaFacebook, FaLinkedin, FaReddit, FaTwitter, FaWhatsapp} from 'react-icons/fa';

export default function ShareButtonsComponent(props: {customLabel?: string}) {
    const [currentUrl, setCurrentUrl] = useState('');
    const [pageTitle, setPageTitle] = useState('');

    useEffect(() => {
        setCurrentUrl(encodeURIComponent(window.location.href));
        setPageTitle(encodeURIComponent(document.title));
    }, []);

    const shareUrls = {
        facebook: `https://www.facebook.com/sharer/sharer.php?u=${currentUrl}`,
        reddit: `https://reddit.com/submit?url=${currentUrl}&title=${pageTitle}`,
        twitter: `https://twitter.com/intent/tweet?url=${currentUrl}&text=${pageTitle}`,
        linkedin: `https://www.linkedin.com/shareArticle?mini=true&url=${currentUrl}&title=${pageTitle}`,
        whatsapp: `https://api.whatsapp.com/send?text=${pageTitle}%20${currentUrl}`
    };

    const handleShare = (platform: keyof typeof shareUrls) => {
        window.open(shareUrls[platform], '_blank', 'width=600,height=400');
    };

    const ShareButton = ({ platform, icon: Icon }: { platform: keyof typeof shareUrls; icon: React.ComponentType<{ size: number }> }) => (
        <button
            onClick={() => handleShare(platform)}
            className="p-2 text-gray-600 hover:text-gray-800 focus:outline-none focus:ring-2 focus:ring-gray-300 focus:ring-offset-2 transition-colors"
            aria-label={`Share on ${platform}`}
        >
            <Icon size={24} />
        </button>
    );

    return (
        <div className="flex flex-col items-center space-y-2">
            <span className="text-sm font-medium text-gray-700">{props.customLabel ? props.customLabel : "Share this content:"}</span>
            <div className="flex space-x-4">
                <ShareButton platform="facebook" icon={FaFacebook} />
                <ShareButton platform="reddit" icon={FaReddit} />
                <ShareButton platform="twitter" icon={FaTwitter} />
                <ShareButton platform="linkedin" icon={FaLinkedin} />
                <ShareButton platform="whatsapp" icon={FaWhatsapp} />
            </div>
        </div>
    );
}