import {FaEye, FaRegCommentDots} from "react-icons/fa";

export default function StatsPostItemComponent(props: {numComments?: number, numViews?: number}) {

    function renderNumComments() {
        return (
            <span className='flex gap-1 items-center'><FaRegCommentDots/>{props.numComments ? props.numComments : 0}</span>
        )
    }

    function renderNumViews() {
        return (
            <span className='flex gap-1 items-center'><FaEye/>{props.numViews ? props.numViews : 0}</span>
        )
    }

    return (
        <div className='flex flex-row gap-3 items-center text-xs font-light'>
            {renderNumViews()}
            {renderNumComments()}
        </div>
    )
}