import {Button, Menu, MenuButton, MenuItemOption, MenuList, MenuOptionGroup} from "@chakra-ui/react";
import {GoEye, GoSortDesc} from "react-icons/go";
import {MdLocalFireDepartment} from "react-icons/md";
import LocalStorageDataManager from "../../logic/managers/LocalStorageData.manager";

export enum SortListTypes {MOST_RECENT = "Most Recent", MOST_VIEWED = "Most Viewed", TRENDING = "Trending"}

export const SORTLIST_LOCALSTORAGE_KEY = "default.listsortby";

export default function SharedSortListByComponent(props: {selectedSortType: SortListTypes, onSelectedChanged: (selectedSortType: SortListTypes) => void}) {


    function onSelectionChanged(selectedSortType: SortListTypes) {
        LocalStorageDataManager.saveString(selectedSortType, SORTLIST_LOCALSTORAGE_KEY);
        props.onSelectedChanged(selectedSortType);
    }

    function getTypeIcon(sortType: SortListTypes) {
        switch (sortType) {
            case SortListTypes.MOST_VIEWED:
                return <GoEye />;

            case SortListTypes.TRENDING:
                return <MdLocalFireDepartment />;

            case SortListTypes.MOST_RECENT:
            default:
                return <GoSortDesc />;
        }
    }

    return (
        <Menu closeOnSelect={false}>
            <MenuButton as={Button} leftIcon={getTypeIcon(props.selectedSortType)}>
                Sort by {props.selectedSortType} articles
            </MenuButton>
            <MenuList >
                <MenuOptionGroup defaultValue={props.selectedSortType} title='Sort By' type='radio' onChange={(value) => onSelectionChanged(value as SortListTypes)}>
                    <MenuItemOption value={SortListTypes.MOST_RECENT} icon={getTypeIcon(SortListTypes.MOST_RECENT)}>Most Recent</MenuItemOption>
                    <MenuItemOption value={SortListTypes.MOST_VIEWED} icon={getTypeIcon(SortListTypes.MOST_VIEWED)}>Most Viewed</MenuItemOption>
                    <MenuItemOption value={SortListTypes.TRENDING} icon={getTypeIcon(SortListTypes.TRENDING)}>Trending</MenuItemOption>
                </MenuOptionGroup>
            </MenuList>
        </Menu>
    )
}