import React from 'react';
import RootConfigs from "../../../../configs";

export default function JoinTelegramButtonComponent() {
    if (RootConfigs.SOCIAL_NETWORKS["telegram"]) {
        return (
            <div className='w-full flex flex-col gap-1 items-center justify-center'>
                <div className={"divider text-slate-400"}>OR</div>
                <a
                    href={RootConfigs.SOCIAL_NETWORKS["telegram"].link}
                    target="_blank"
                    rel="noopener noreferrer"
                    className="group inline-flex items-center gap-2 bg-sky-500 text-white px-6 py-3 rounded-lg hover:bg-sky-600 transition-all duration-300 shadow-lg hover:shadow-xl hover:-translate-y-0.5"
                >
                    <img
                        className="w-4 h-4 transform transition-transform duration-300 group-hover:translate-x-1 group-hover:-translate-y-1"
                        src={"/images/ic_social_telegram.png"}
                        alt="Telegram icon"
                    />
                    <span className="font-medium">Join Our Telegram Community</span>
                </a>
            </div>
        );
    }
    return null;
}