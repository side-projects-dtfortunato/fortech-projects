import {useAuthState} from "react-firebase-hooks/auth";
import {auth} from "../../logic/shared-data/firebase.utils";
import AuthManager from "../../logic/auth/auth.manager";
import {getLoginPageLink} from "../../../../pages/login";
import Link from "next/link";
import UserPictureAvatarComponent from "./UserPictureAvatar.component";
import {UIHelper} from "../../../ui-helper/UIHelper";
import {useCentralUserDataState} from "../../logic/global-hooks/root-global-state";

export default function NavbarUserLinkComponent() {
    // Auth Mechanism
    const [currentUser, isAuthLoading] = useAuthState(auth);
    const [centralUserData, setCentralUserData] = useCentralUserDataState();

    if (!isAuthLoading && !AuthManager.isUserLogged()) {
        return (
            <Link className="btn btn-primary btn-ghost text-sm" href={getLoginPageLink()}>Sign up/Login</Link>
        );
    } else if (isAuthLoading) {
        return (<></>);
    } else {
        let userPictureUrl: string | undefined = centralUserData
        && centralUserData?.publicProfile?.userPicture
        && centralUserData?.publicProfile?.userPicture.fileUrl ? centralUserData?.publicProfile?.userPicture.fileUrl : undefined;

        return (
            <div className="dropdown dropdown-end">
                <label tabIndex={0} className="btn btn-ghost btn-circle avatar">
                    <UserPictureAvatarComponent width={"w-10"} pictureUrl={userPictureUrl}/>
                </label>
                <ul tabIndex={0}
                    className="mt-3 p-2 shadow menu menu-compact dropdown-content bg-base-100 rounded-box w-52">
                    {UIHelper.renderNavbarUserLinks()}
                </ul>
            </div>
        )
    }
}