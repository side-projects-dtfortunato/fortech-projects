import GoogleSigninButtonComponent from "../login/shared/GoogleSigninButton.component";
import Link from "next/link";
import {getLoginPageLink} from "../../../../pages/login";
import {getLanguageLabel} from "../../logic/language/language.helper";
import RootConfigs from "../../../../configs";

export default function SignupLoginCallToActionContainerComponent(props: {customMessage?: any}) {

    function renderGoogleSigninContainer() {
        if (RootConfigs.META_CONFIGS.disableGoogleSignin) {
            return (<></>);
        } else {
            return (
                <>
                    <GoogleSigninButtonComponent goAutoBack={false} />
                </>
            )
        }
    }

    function renderMessage() {
        if (props.customMessage) {
            return props.customMessage;
        } else {
            return (<span>Get your <b>free account today</b></span>)
        }
    }

    return (
        <div className='flex flex-col gap-3 my-2 items-center'>
            {renderMessage()}
            <div className='flex flex-wrap gap-5 justify-center items-center '>
                {renderGoogleSigninContainer()}
                <Link className='btn btn-outline btn-primary text-white w-56' href={getLoginPageLink()}>{getLanguageLabel("LANDING_PAGE_SIGNUP_WITH_EMAIL")}</Link>
            </div>
            <span className='subtitle'>No credit card required</span>
        </div>
    )
}