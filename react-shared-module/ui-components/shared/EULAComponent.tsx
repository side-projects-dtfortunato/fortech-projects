import React, {useState} from 'react';
import RootConfigs from "../../../../configs";
import {useDisclosure} from "@chakra-ui/react";

export default function EULAComponent() {
    const [agreed, setAgreed] = useState(false);
    const { isOpen, onOpen, onClose } = useDisclosure();

    const handleAgree = () => {
        setAgreed(!agreed);
    };

    return (
        <div className="max-w-2xl mx-auto p-6 bg-white shadow-md rounded-lg">
            <h1 className="text-2xl font-bold mb-4">End-User License Agreement for {RootConfigs.SITE_NAME}</h1>
            <div className="text-sm space-y-4 mb-6 h-96 overflow-y-auto border p-4 rounded">
                <p>PLEASE READ THIS END-USER LICENSE AGREEMENT (&quot;EULA&quot;) CAREFULLY BEFORE USING THE {RootConfigs.SITE_NAME} APPLICATION (&quot;THE APP&quot;). BY USING THE APP, YOU AGREE TO BE BOUND BY THE TERMS OF THIS EULA.</p>

                <h2 className="text-lg font-semibold">1. AGREEMENT TO TERMS</h2>
                <p>By using {RootConfigs.SITE_NAME}, you agree to be bound by this EULA. If you do not agree to this EULA, do not use the App.</p>

                <h2 className="text-lg font-semibold">2. LICENSE GRANT</h2>
                <p>{RootConfigs.SITE_NAME} grants you a revocable, non-exclusive, non-transferable, limited license to download, install, and use the App for your personal, non-commercial purposes strictly in accordance with the terms of this EULA.</p>

                <h2 className="text-lg font-semibold">3. RESTRICTIONS</h2>
                <p>You agree not to, and you will not permit others to:</p>
                <ul className="list-disc list-inside pl-4">
                    <li>License, sell, rent, lease, assign, distribute, transmit, host, outsource, disclose or otherwise commercially exploit the App or make the App available to any third party.</li>
                    <li>Modify, make derivative works of, disassemble, decrypt, reverse compile or reverse engineer any part of the App.</li>
                    <li>Remove, alter or obscure any proprietary notice (including any notice of copyright or trademark) of {RootConfigs.SITE_NAME} or its affiliates, partners, suppliers or the licensors of the App.</li>
                </ul>

                <h2 className="text-lg font-semibold">4. USER-GENERATED CONTENT</h2>
                <p>a) The App may allow you to create, post, share, or publish content (&quot;User Content&quot;). You are solely responsible for your User Content.</p>
                <p>b) You agree that you will not post, share, or publish any User Content that:</p>
                <ul className="list-disc list-inside pl-4">
                    <li>Is unlawful, harmful, threatening, abusive, harassing, tortious, defamatory, vulgar, obscene, libelous, invasive of another&apos;s privacy, hateful, or racially, ethnically or otherwise objectionable.</li>
                    <li>Infringes any patent, trademark, trade secret, copyright or other proprietary rights of any party.</li>
                    <li>Contains software viruses or any other computer code, files or programs designed to interrupt, destroy or limit the functionality of any computer software or hardware.</li>
                </ul>
                <p>c) {RootConfigs.SITE_NAME} has the right (but not the obligation) to, in its sole discretion, refuse or remove any User Content that violates these terms or is otherwise objectionable.</p>

                <h2 className="text-lg font-semibold">5. CONTENT MODERATION</h2>
                <p>a) Users can report objectionable content through the App&apos;s reporting mechanism. {RootConfigs.SITE_NAME} will review and act on reports within 24 hours.</p>
                <p>b) Users have the ability to block other users they find abusive or objectionable.</p>
                <p>c) {RootConfigs.SITE_NAME} reserves the right to remove any content and/or ban any user who violates this EULA.</p>

                <h2 className="text-lg font-semibold">6. TERMINATION</h2>
                <p>This EULA is effective until terminated by you or {RootConfigs.SITE_NAME}. Your rights under this EULA will terminate automatically without notice if you fail to comply with any term(s) of this EULA.</p>

                <h2 className="text-lg font-semibold">7. GOVERNING LAW</h2>
                <p>This EULA shall be governed by and construed in accordance with the laws of Portugal.</p>

                <h2 className="text-lg font-semibold">8. CHANGES TO THIS EULA</h2>
                <p>{RootConfigs.SITE_NAME} reserves the right, at its sole discretion, to modify or replace this EULA at any time. If a revision is material, we will provide at least 30 days&apos; notice prior to any new terms taking effect.</p>
                <p>By continuing to access or use our App after those revisions become effective, you agree to be bound by the revised terms. If you do not agree to the new terms, please stop using the App.</p>

                <h2 className="text-lg font-semibold">9. CONTACT INFORMATION</h2>
                <p>If you have any questions about this EULA, please contact us at {RootConfigs.CONTACT_EMAIL}.</p>

                <p>By using the {RootConfigs.SITE_NAME} app, you acknowledge that you have read this EULA, understand it, and agree to be bound by its terms and conditions.</p>
            </div>
        </div>
    );
};
