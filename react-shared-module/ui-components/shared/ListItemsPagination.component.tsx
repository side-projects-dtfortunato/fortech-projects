import {QueryPaginationDataModel} from "../../logic/shared-data/datamodel/query-pagination-data.model";
import {useState} from "react";
import NotFoundSimpleMessageComponent from "./NotFoundSimpleMessage.component";
import SpinnerComponent from "./Spinner.component";

export default function ListItemsPaginationComponent<ItemDataType>(props: {
    initLoader: () => Promise<QueryPaginationDataModel<ItemDataType>>,
    loadMore: (queryPagination: QueryPaginationDataModel<ItemDataType>) => Promise<QueryPaginationDataModel<ItemDataType>>,
    renderItem: (listItem: ItemDataType) => any,
    renderContentLoader?: () => any,
    emptyMessage: string}) {
    const [queryPaginationState, setQueryPaginationState] = useState<QueryPaginationDataModel<ItemDataType> | undefined>();
    const [isLoading, setIsLoading] = useState(true);

    if (!queryPaginationState) {
        props.initLoader().then((data) => {
            setQueryPaginationState(data);
            setIsLoading(false);
        });
    }

    function renderLoading() {
        if (props.renderContentLoader) {
            return (
                <>
                    {props.renderContentLoader()}
                </>
            )
        } else {
            return (
                <>
                    <SpinnerComponent />
                </>
            )
        }
    }
    function renderListItems() {
        if (queryPaginationState && queryPaginationState.listItems.length > 0) {
            return (<div className='flex flex-wrap justify-center gap-3'>
                {isLoading ? renderLoading() : <></>}
                {queryPaginationState.listItems.map((item) => {
                    return props.renderItem(item);
                })}
            </div>)
        } else if (!queryPaginationState && isLoading) {
            return (<div className='flex flex-wrap justify-center gap-3'>
                {isLoading ? renderLoading() : <></>}
            </div>)
        } else {
            return (
                <NotFoundSimpleMessageComponent message={props.emptyMessage} />
            )
        }

    }

    async function onLoadMore() {
        if (queryPaginationState) {
            setIsLoading(true);

            setQueryPaginationState(await props.loadMore(queryPaginationState));

            setIsLoading(false);
        }

    }
    function renderLoadMore() {
        if (queryPaginationState && queryPaginationState.hasMorePages) {
            return (
                <div className='flex flex-col gap-1 justify-center items'>
                    <button className='btn btn-outline' disabled={isLoading} onClick={onLoadMore}>Load More</button>
                </div>
            )
        } else {
            return (<></>)
        }
    }

    return (
        <div className={"flex flex-col items-center gap-2"}>
            {renderListItems()}
            {renderLoadMore()}
        </div>
    )
}