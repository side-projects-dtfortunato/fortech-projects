export default function InlineLabelSeparatorComponent(props: { title: string, subtitle?: string, titleSize?: "medium" | "large"}) {

    let titleElement: any = <h2 className={"font-bold text-2xl sm:text-3xl"}>{props.title}</h2>

    switch (props.titleSize) {
        case "large":
            titleElement = <h1 className={"font-bold text-3xl"}>{props.title}</h1>
            break;
        case "medium":
        default:
            titleElement = <h2 className={"font-bold text-2xl"}>{props.title}</h2>
            break;
    }
    return (
        <div className='flex flex-col gap-1'>
            {titleElement}
            {props.subtitle ?
                <span className='text-sm font-light italic text-slate-600'>{props.subtitle}</span> : <></>}
            <div className='divider my-1'></div>
        </div>
    )
}