export default function CountryFlagIconComponent(props: {countryCode?: string, width: number}) {

    if (props.countryCode && props.countryCode.length === 2) {
        const url: string = "https://flagsapi.com/" + props.countryCode.toUpperCase() + "/flat/64.png"
        return (
            <img src={url} width={props.width} alt={props.countryCode}/>
        )
    } else {
        return (
            <></>
        )
    }

}