import {useAmp} from "next/amp";
import ImageAmpComponent from "./ImageAmp.component";

export default function UserPictureAvatarComponent(props: {pictureUrl?: string, width: "w-2" | "w-4" | "w-6" | "w-8" | "w-10" | "w-12" | "w-14" | "w-16"  | "w-20" | "w-24" | "w-32" | "w-40"}) {
    const isAmp = useAmp()
    let avatarPictureUrl: string = props.pictureUrl ? props.pictureUrl : "/images/ic_user.png";


    return (
        <div className="avatar">
            <div className={"rounded-full drop-shadow border " + props.width}>
                <ImageAmpComponent src={avatarPictureUrl} width={25} height={25} alt={"User picture"} />
            </div>
        </div>
    )
}