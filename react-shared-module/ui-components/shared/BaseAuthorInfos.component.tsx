import UserPictureAvatarComponent from "./UserPictureAvatar.component";

export default function BaseAuthorInfosComponent(props: {pictureUrl?: string, name: string, subtitle?: string}) {

    return (
        <div className="flex items-center gap-4 flex-wrap">
            <UserPictureAvatarComponent width={"w-12"} pictureUrl={props.pictureUrl ? props.pictureUrl : "/images/logo-512.png"} />

            <div>
                <p className="text-lg font-semibold">{props.name}</p>
                <span className="text-gray-500 font-light italic">{props.subtitle ? props.subtitle : ""}</span>
            </div>
        </div>
    )
}