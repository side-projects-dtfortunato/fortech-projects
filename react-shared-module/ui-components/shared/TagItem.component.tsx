import Link from "next/link";

export default function TagItemComponent(props: {label: string, filled?: boolean, color?: string, href?: string}) {
    let color: string = props.color ? props.color : "[#05132E]";
    let hoverBgColor: string = "hover:bg-" + color + "/50";

    let bgColor: string = "bg-" + (props.filled ? color : "transparent");
    let borderColor: string = "border-"+color;
    let textColor: string = "text-" + (props.filled ? "white" : color);

    const itemRender = (
        <div className={'flex px-1 py-1 border mr-2 mb-2 rounded ' + borderColor + " " + bgColor + " " + hoverBgColor}>
            <span className={'font-xs ' + textColor} style={{fontSize: 11}}>{props.label}</span>
        </div>
    )

    return props.href ? (<Link href={props.href}>{itemRender}</Link>) : itemRender;
}