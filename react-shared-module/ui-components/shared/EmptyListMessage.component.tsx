export default function EmptyListMessageComponent(props: {message: string, icon?: any}) {
    return (
        <div className='flex flex-col p-8 justify-center items-center w-full text-center text-lg text-slate-400 my-20 border border-dashed rounded-xl'>
            {props.icon ? props.icon : (<></>)}
            {props.message}
        </div>
    )
}