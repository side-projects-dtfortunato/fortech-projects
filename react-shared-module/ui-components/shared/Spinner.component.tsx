export default function SpinnerComponent(props: {message?: string}) {
    return (
        <div className='p-2 w-full flex flex-col justify-center items-center gap-2'>
            {props.message ? <span className='custom'>{props.message}</span> : <></>}
            <progress className="progress w-56"></progress>
        </div>
    )
}