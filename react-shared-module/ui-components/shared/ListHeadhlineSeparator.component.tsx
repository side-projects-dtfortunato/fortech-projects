
export default function ListHeadhlineSeparatorComponent(props: { text: string, color: string }) {
    const lineStyle = {
        backgroundColor: props.color, // Set the background color of the line
        height: '2px', // Set the height of the line
        width: '100%', // Make the line span the full width of the container
    };

    // Recommended Colors: https://www.schemecolor.com/reading-headlines.php
    return (
        <div className='flex flex-col w-full'>
            <h3 className='text-xl font-bold' style={{ color: props.color }}>{props.text.toUpperCase()}</h3>
            <div style={lineStyle}></div>
        </div>
    );
};