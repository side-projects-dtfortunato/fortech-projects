import {CookieConsent} from "react-cookie-consent";
import React from "react";
import {getLanguageLabel} from "../../logic/language/language.helper";
import RootConfigs from "../../../../configs";

export default function CookiesBannerConsentComponent() {


    if (RootConfigs.META_CONFIGS.disableCookies) {
        return <></>
    } else {
        return (
            <CookieConsent
                location="bottom"
                buttonText="Accept"
                cookieName="cookies"

                style={{
                    background: "#F2F2F2",
                    opacity: 0.8,
                    color: "#000000", margin: "5px", borderRadius: "25px",}}
                buttonStyle={{ color: "#A3C6FF", backgroundColor: "#021531", fontSize: "13px", borderRadius: "25px" }}
                expires={150}

            >
                <span style={{color: "#333333", fontSize: "15px" }}>{getLanguageLabel("COOKIES_CONSENT_MESSAGE")}</span>
            </CookieConsent>
        )
    }
}