// Inspired by: https://shuffle.dev/editor?project=894bce0f725bf6cbb604957159f1b6dde69fa7e1&tutorial=1
import {IoIosCheckmarkCircleOutline} from "react-icons/io";
import {VscError} from "react-icons/vsc";

export default function BannerMessageComponent(props: {message: string, bannerType: "success" | "warning" | "error" | "in_progress"}) {

    let bgColor: string = "bg-green-100";
    let icon: any = <IoIosCheckmarkCircleOutline />

    switch (props.bannerType) {
        case "success":
            bgColor = "bg-green-100";
            icon = <IoIosCheckmarkCircleOutline />
            break;
        case "error":
            bgColor = "bg-orange-100";
            icon = <VscError />
            break;
        case "warning":
            bgColor = "bg-yellow-100";
            icon = <VscError />
            break;
        case "in_progress":
            bgColor = "bg-yellow-100";
            icon = <span className="loading loading-spinner loading-lg"></span>
            break;

    }

    return (
        <div className={"max-w-max mx-auto rounded-6xl py-4 px-6 sm:px-10 " + bgColor}>
            <div className="flex items-center justify-center gap-5">
                <div className="w-auto">
                    <div className="w-8 h-8 rounded-full border border-neutral-900 flex items-center justify-center">
                        {icon}
                    </div>
                </div>
                <div className="w-px h-10 bg-green-900 bg-opacity-10"></div>
                <p className="font-medium tracking-tight">{props.message}</p>
            </div>
        </div>
    )
}