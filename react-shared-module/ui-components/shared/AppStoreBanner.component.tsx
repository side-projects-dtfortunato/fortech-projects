import React, {useEffect, useState} from 'react';
import {FaAndroid, FaApple} from 'react-icons/fa';
import Image from 'next/image';
import RootConfigs from "../../../../configs";
import {SharedAnalyticsManager} from "../../utils/shared-analytics.utils";

declare global {
    interface Window {
        ReactNativeWebView?: any;
    }
}

const AppStoreBanner = ({
                            companyName = RootConfigs.SITE_NAME,
                            appName = RootConfigs.SITE_NAME
                        }) => {
    const [isAndroid, setIsAndroid] = useState(false);
    const [isIOS, setIsIOS] = useState(false);
    const [isInWebView, setIsInWebView] = useState(false);

    useEffect(() => {
        const userAgent = navigator.userAgent.toLowerCase();
        setIsAndroid(/android/.test(userAgent));
        setIsIOS(/iphone|ipad|ipod/.test(userAgent));

        const detectWebView = () => {
            return !!(window.ReactNativeWebView || userAgent.includes('wv') ||
                ('webkit' in window && 'messageHandlers' in (window as any).webkit));
        };

        setIsInWebView(detectWebView());
    }, []);

    const primaryColor = RootConfigs.THEME.PRIMARY_COLOR;
    const textColor = 'text-white';

    const StoreButton: React.FC<{
        platform: 'android' | 'ios';
        href: string;
        icon: React.ReactNode;
        text: string;
    }> = ({ platform, href, icon, text }) => (
        <a
            href={href}
            target="_blank"
            rel="noopener noreferrer"
            className={`flex items-center justify-center ${platform === 'android' ? 'bg-green-500 hover:bg-green-600' : 'bg-gray-800 hover:bg-gray-900'} ${textColor} py-2 px-4 rounded-lg hover:shadow-lg transition-all duration-300 text-sm`}
            onClick={() => {
                SharedAnalyticsManager.getInstance().logUserInteraction("app_store_opened", {
                    "platform": platform,
                });
            }}
        >
            {icon}
            <span className="ml-2 font-semibold">{text}</span>
        </a>
    );

    if (RootConfigs.META_CONFIGS.disableAppStoreBanner || isInWebView) {
        return null;
    }

    return (
        <div className="mx-4 my-6">
            <div className={`bg-gradient-to-r from-${primaryColor}-700 to-${primaryColor}-900 ${textColor} p-4 sm:p-6 rounded-xl shadow-lg`}>
                <div className="flex flex-col sm:flex-wrap  items-center justify-between gap-5">
                    <div className="flex items-center mb-4 sm:mb-0 mr-2">
                        <Image
                            src={`${RootConfigs.BASE_URL}/images/logo-512.png`}
                            alt={`${companyName} logo`}
                            width={48}
                            height={48}
                            className="rounded-lg shadow-md"
                        />
                        <div className="ml-4">
                            <h2 className="text-xl font-bold">{companyName}</h2>
                            <p className="text-sm opacity-90">Get {appName} on your phone!</p>
                        </div>
                    </div>
                    <div className="flex flex-wrap gap-3">
                        {(isAndroid || (!isAndroid && !isIOS)) && (
                            <StoreButton
                                platform="android"
                                href={`https://play.google.com/store/apps/details?id=${RootConfigs.APP_STORE_LINKS.androidAppId}`}
                                icon={<FaAndroid className="text-lg" size={25} />}
                                text="Get on Google Play"
                            />
                        )}
                        {(isIOS || (!isAndroid && !isIOS)) && (
                            <StoreButton
                                platform="ios"
                                href={`https://apps.apple.com/app/${RootConfigs.APP_STORE_LINKS.iosAppId}`}
                                icon={<FaApple className="text-lg" size={25} />}
                                text="Get on App Store"
                            />
                        )}
                    </div>
                </div>
            </div>
        </div>
    );
};

export default AppStoreBanner;