

export default function StepsProgressComponent(props: {steps: string[], stepIndex: number}) {

    function renderSteps() {
        return props.steps.map((stepLabel, index) => {
            return (<li key={stepLabel} className={"step " + (index <= props.stepIndex) ? "step-neutral" : ""}>{stepLabel}</li>)
        });
    }

    return (
        <ul className="steps">
            {renderSteps()}
        </ul>
    )
}