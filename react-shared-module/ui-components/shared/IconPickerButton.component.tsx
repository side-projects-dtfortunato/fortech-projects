import {LuImagePlus} from "react-icons/lu";
import {useState} from "react";
import Modal from 'react-modal';
import IconPickerComponent from "./IconPicker.component";

export default function IconPickerButtonComponent(props: {onIconSelected: (imageBase64: string) => void, btnSize: number}) {
    const [selectedImage, setSelectedImage] = useState<string|undefined>();
    const [isModalOpen, setIsModalOpen] = useState(false);

    function onImageSelected(selectedImage: string) {
        setSelectedImage(selectedImage);
        props.onIconSelected(selectedImage);
        setIsModalOpen(false);
    }

    return (
        <div>
            <div
            style={{
                width: props.btnSize + 'px',
                height: props.btnSize + 'px',
                border: '2px dashed #aaa',
                borderRadius: '10px',
                display: 'flex',
                justifyContent: 'center',
                alignItems: 'center',
                cursor: 'pointer',
            }} onClick={() => setIsModalOpen(true)}
        >
            {selectedImage ? (
                <img
                    src={selectedImage}
                    alt="Selected"
                    style={{ maxWidth: '100%', maxHeight: '100%',
                        borderRadius: '10px', padding: "5px" }}
                />
            ) : (
                <LuImagePlus color={"#aaa"} size={(props.btnSize/2) + "px"} />
            )}
            </div>

            <Modal
                isOpen={isModalOpen}
                onRequestClose={() => setIsModalOpen(false)}
                contentLabel="Icon Picker Modal"
                overlayClassName="modal-overlay"
                className="modal-content" // Add a custom class for styling the content
            >
                <div className='flex flex-col items-center overflow-auto'>
                    <IconPickerComponent onIconSelected={onImageSelected} onCancel={() => {}} />
                </div>
            </Modal>
        </div>
    )
}