export default function BaseListItemCardComponent(props: {children: any}) {
    return (
        <div className='flex list-item-bg p-5 drop-shadow'>
            {props.children}
        </div>
    )
}