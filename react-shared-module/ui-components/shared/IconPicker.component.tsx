import React, {useState} from 'react';
import {IconContext} from 'react-icons';
import * as ReactIcons from 'react-icons/fa';
import ReactDOMServer from 'react-dom/server';
import InputTextComponent from "../form/InputText.component";

export default function IconPickerComponent(props: {onIconSelected: (iconBase64: string) => void, onCancel: () => void}) {
    const [selectedIcon, setSelectedIcon] = useState<string | null>(null);
    const [searchTerm, setSearchTerm] = useState<string>('');

    const allIcons: string[] = Object.keys(ReactIcons);

    const filteredIcons: string[] = allIcons.filter((iconName) =>
        iconName.toLowerCase().includes(searchTerm.toLowerCase())
    );

    const handleIconClick = (iconName: string): void => {
        // @ts-ignore
        const IconComponent = ReactIcons[iconName];
        const imageBase64 = generateBase64Image(IconComponent);
        setSelectedIcon(imageBase64);
        props.onIconSelected(imageBase64);
    };

    const generateBase64Image = (IconComponent: React.FC): string => {
        const iconElement = React.createElement(IconComponent);
        // @ts-ignore
        const IconWithColor = React.cloneElement(React.createElement(IconComponent), { fill: "#000000" });
        const svgString: string = ReactDOMServer.renderToStaticMarkup(IconWithColor);
        const base64Image: string = `data:image/svg+xml;base64,${btoa(svgString)}`;
        return base64Image;
    };

    const handleSearchChange = (text: string): void => {
        setSearchTerm(text);
    };

    return (
        <div className='flex flex-col items-center p-4 gap-4'>
            <InputTextComponent
                inputType={"text"}
                topLabel={"Search for an icon"}
                hint={"e.g: Home"}
                value={searchTerm}
                onChanged={handleSearchChange}
            />

            <div className="flex flex-wrap gap-2 justify-between">
                {filteredIcons.map((iconName, index) => {
                    // @ts-ignore
                    let IconComponent = ReactIcons[iconName];
                    IconComponent.color = "#000000";
                    return (
                        <div
                            key={index}
                            className="flex flex-col items-center cursor-pointer list-item-bg border rounded p-2"
                            onClick={() => handleIconClick(iconName)}
                        >
                            <IconContext.Provider value={{ size: '2em' }}>
                                <IconComponent />
                            </IconContext.Provider>
                        </div>
                    );
                })}
            </div>
            <div className='flex justify-end w-full'>
                <button className='btn btn-outline btn-md' onClick={props.onCancel}>Cancel</button>
            </div>
        </div>
    );
}