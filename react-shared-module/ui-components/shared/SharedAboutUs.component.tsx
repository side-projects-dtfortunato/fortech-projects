import RootConfigs from "../../../../configs";
import {PageHeadProps} from "../root/RootLayout.component";
import CustomRootLayoutComponent from "../../../app-components/root/CustomRootLayout.component";
import {Globe2, Mail, Users} from "lucide-react";
import {CgMenuMotion} from "react-icons/cg";
import AppStoreBanner from "./AppStoreBanner.component";
import {JsonLd} from "react-schemaorg";
import {Organization} from "schema-dts";
import React from "react";

export default function SharedAboutUsComponent() {
    const socialNetworks = Object.entries(RootConfigs.SOCIAL_NETWORKS).map(([key, value]) => ({
        id: key,
        ...value
    }));

    const pageHeadParams: PageHeadProps = {
        title: `About Us - ${RootConfigs.SITE_TITLE}`,
        description: RootConfigs.SITE_DESCRIPTION,
        imgUrl: "/images/logo-512.png",
        jsonLdMarkup: getSchemaMarkup(),
    }

    return (
        <CustomRootLayoutComponent pageHeadProps={pageHeadParams}>
            <main className="min-h-screen bg-gradient-to-b from-gray-50 to-white">
                {/* Hero Section */}
                <div className={`relative overflow-hidden bg-gradient-to-r from-${RootConfigs.THEME.PRIMARY_COLOR}-500 to-${RootConfigs.THEME.PRIMARY_COLOR}-800 text-white`}>
                    <div className="absolute inset-0 bg-black opacity-10"></div>
                    <div className="relative max-w-7xl mx-auto px-4 py-24 sm:px-6 lg:px-8">
                        <div className="flex flex-col items-center text-center gap-5">
                            <img src={"/images/logo-white.png"} className='w-64 mb-5' alt={"Logo"}/>
                            <h1 className="text-4xl md:text-5xl font-bold mb-6">
                                About {RootConfigs.SITE_NAME}
                            </h1>
                            <p className="text-xl md:text-2xl max-w-3xl mx-auto text-gray-100">
                                {RootConfigs.SITE_DESCRIPTION}
                            </p>
                        </div>
                    </div>
                </div>

                {/* Stats Section */}
                <div className="max-w-7xl mx-auto px-4 sm:px-6 lg:px-8 -mt-12">
                    <div className="grid grid-cols-1 md:grid-cols-3 gap-6">
                        <div className="bg-white rounded-xl shadow-lg p-6 transform hover:scale-105 transition-transform duration-300">
                            <Globe2 className={`w-12 h-12 text-${RootConfigs.THEME.PRIMARY_COLOR}-600 mb-4`} />
                            <h3 className="text-lg font-semibold mb-2">Global Coverage</h3>
                            <p className="text-gray-600">Delivering {RootConfigs.SITE_TOPIC} to readers worldwide</p>
                        </div>
                        <div className="bg-white rounded-xl shadow-lg p-6 transform hover:scale-105 transition-transform duration-300">
                            <CgMenuMotion className={`w-12 h-12 text-${RootConfigs.THEME.PRIMARY_COLOR}-600 mb-4`} />
                            <h3 className="text-lg font-semibold mb-2">Real-time Updates</h3>
                            <p className="text-gray-600">Stay informed with the latest news and developments</p>
                        </div>
                        <div className="bg-white rounded-xl shadow-lg p-6 transform hover:scale-105 transition-transform duration-300">
                            <Users className={`w-12 h-12 text-${RootConfigs.THEME.PRIMARY_COLOR}-600 mb-4`} />
                            <h3 className="text-lg font-semibold mb-2">Community Focused</h3>
                            <p className="text-gray-600">Building connections through reliable journalism</p>
                        </div>
                    </div>
                </div>

                {/* Main Content */}
                <div className="max-w-7xl mx-auto px-4 py-16 sm:px-6 lg:px-8">
                    <div className="grid grid-cols-1 lg:grid-cols-2 gap-12">
                        {/* Mission Section */}
                        <div className="bg-white rounded-xl shadow-lg p-8 hover:shadow-xl transition-shadow duration-300">
                            <h2 className="text-3xl font-bold mb-6 text-gray-900">Our Mission</h2>
                            <p className="text-lg text-gray-700 leading-relaxed">
                                As your trusted source for {RootConfigs.SITE_TOPIC}, we are committed to delivering accurate,
                                timely, and relevant information to our global audience. Our dedication to journalistic excellence
                                drives us to provide comprehensive coverage that matters to you.
                            </p>
                        </div>

                        {/* Connect Section */}
                        <div className="bg-white rounded-xl shadow-lg p-8 hover:shadow-xl transition-shadow duration-300">
                            <h2 className="text-3xl font-bold mb-6 text-gray-900">Connect With Us</h2>
                            <div className="grid grid-cols-2 gap-4">
                                {socialNetworks.map((network) => (
                                    <a
                                        key={network.id}
                                        href={network.link}
                                        target="_blank"
                                        rel="noopener noreferrer"
                                        className={`group flex items-center p-4 rounded-lg bg-${RootConfigs.THEME.PRIMARY_COLOR}-200 hover:bg-${RootConfigs.THEME.PRIMARY_COLOR}-50 transition-colors duration-300`}
                                    >
                                        <img
                                            src={network.iconUrl}
                                            alt={`${network.label} icon`}
                                            className="w-8 h-8 mr-3 group-hover:scale-110 transition-transform duration-300"
                                        />
                                        <span className={`text-gray-700 group-hover:text-${RootConfigs.THEME.PRIMARY_COLOR}-600 transition-colors duration-300`}>
                      {network.label}
                    </span>
                                    </a>
                                ))}
                            </div>
                        </div>
                    </div>

                    {/* Contact Section */}
                    <div className={`mt-12 bg-gradient-to-r from-${RootConfigs.THEME.PRIMARY_COLOR}-50 to-${RootConfigs.THEME.PRIMARY_COLOR}-100 rounded-xl shadow-lg p-8`}>
                        <div className="flex items-center justify-center mb-6">
                            <Mail className={`w-12 h-12 text-${RootConfigs.THEME.PRIMARY_COLOR}-600 mr-4`} />
                            <h2 className="text-3xl font-bold text-gray-900">Get in Touch</h2>
                        </div>
                        <p className="text-center text-lg text-gray-700 mb-6">
                            Have questions or feedback? We&apos;d love to hear from you.
                        </p>
                        <div className="text-center">
                            <a
                                href={`mailto:${RootConfigs.CONTACT_EMAIL}`}
                                className="inline-flex items-center px-6 py-3 bg-blue-600 hover:bg-blue-700 transition-colors duration-300 rounded-full text-white font-semibold shadow-lg hover:shadow-xl"
                            >
                                Contact Us
                            </a>
                        </div>
                    </div>

                    {/* App Download Section */}
                    {!RootConfigs.META_CONFIGS.disableAppStoreBanner && (
                        <div className="mt-12 bg-white rounded-xl shadow-lg p-8 text-center">
                            <h2 className="text-3xl font-bold text-gray-900 mb-6">Download Our App</h2>
                            <p className="text-lg text-gray-700 mb-8">
                                Stay connected on the go with our mobile app
                            </p>
                            <AppStoreBanner />
                        </div>
                    )}
                </div>
            </main>
        </CustomRootLayoutComponent>
    )
}

function getSchemaMarkup() {
    return (
        <JsonLd<Organization>
            item={{
                "@context": "https://schema.org",
                "@type": "Organization",
                name: RootConfigs.SITE_NAME,
                url: RootConfigs.BASE_URL,
                description: RootConfigs.SITE_DESCRIPTION,
                email: RootConfigs.CONTACT_EMAIL,
                sameAs: Object.values(RootConfigs.SOCIAL_NETWORKS).map(network => network.link)
            }}
        />
    )
}