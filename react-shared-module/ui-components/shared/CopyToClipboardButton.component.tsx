import {FaRegCopy} from "react-icons/fa";

export default function CopyToClipboardButtonComponent(props: {text: string}) {

    return (
        <button className='btn btn-xs btn-ghost text-slate-400' onClick={() => {navigator.clipboard.writeText(props.text)}}><FaRegCopy /></button>
    )
}