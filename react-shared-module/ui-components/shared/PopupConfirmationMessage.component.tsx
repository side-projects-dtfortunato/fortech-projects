import {useDisclosure, Modal, ModalContent, ModalOverlay} from "@chakra-ui/react";
import {useEffect} from "react";

export default function PopupConfirmationMessageComponent(props: {icon: any, message: string, durationSeconds: number}) {
    const { isOpen, onOpen, onClose } = useDisclosure();

    useEffect(() => {
        onOpen();
        const timer = setTimeout(() => {
            onClose();
        }, props.durationSeconds * 1000);

        return () => clearTimeout(timer);
    }, []);


    return (
        <Modal isOpen={isOpen} onClose={onClose} isCentered={true}>
            <ModalOverlay />

            <ModalContent backgroundColor={"transparent"}>
                <div className='flex flex-col items-center'>
                    {props.icon}
                    <h2 className='text-center text-white text-xl'>{props.message}</h2>
                </div>
            </ModalContent>
        </Modal>
    )
}