import {Box, Skeleton, SkeletonCircle, SkeletonText, Stack} from "@chakra-ui/react";

export default function PageLoadingSkeletonComponent() {

    return (
        <Box padding="6" boxShadow="lg" bg="white" maxW="800px" mx="auto" mt="4">
            <Skeleton height="40px" width="60%" mb="4" />
            <SkeletonText mt="4" noOfLines={4} spacing="4" />
            <Stack direction="row" spacing="4" mt="8">
                <SkeletonCircle size="10" />
                <Box flex="1">
                    <Skeleton height="20px" width="80%" mb="2" />
                    <Skeleton height="20px" width="60%" />
                </Box>
            </Stack>
            <SkeletonText mt="8" noOfLines={6} spacing="4" />
            <Skeleton height="200px" mt="8" />
        </Box>
    );
}