export default function BaseRootFlatContainerComponent(props: {children: any}) {
    return (
        <div className='flex flex-col gap-5 p-5 py-10'>
            {props.children}
        </div>
    )
}