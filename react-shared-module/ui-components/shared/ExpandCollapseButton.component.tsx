import {MdExpandLess, MdExpandMore} from "react-icons/md";

export default function ExpandCollapseButtonComponent(props: {
    collapsedLabel: string, expandedLabel: string, isCollapsed: boolean, onClick: () => void
}) {
    return (
        <div className='w-full divider'>
            <button className='btn btn-ghost flex flex-row gap-2' onClick={props.onClick}>
                {
                    props.isCollapsed ? (<>{props.collapsedLabel} <MdExpandMore /></>)
                        : (<>{props.expandedLabel} <MdExpandLess /></>)
                }
            </button>
        </div>
    )
}