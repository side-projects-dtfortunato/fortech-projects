import React from 'react';
import { useRouter } from 'next/router';
import { motion, AnimatePresence } from 'framer-motion';
import { IconType } from 'react-icons';
import RootConfigs from "../../../../configs";

export interface NavigationMenuItem {
    label: string;
    path: string;
    icon?: IconType;
    forceActive?: boolean;
    rightComponent?: React.ReactNode;
}

interface NavigationHeaderMenuProps {
    menuItems: NavigationMenuItem[];
    activeColor?: string;
}

const NavigationHeaderMenu: React.FC<NavigationHeaderMenuProps> = ({
                                                                       menuItems,
                                                                       activeColor = `text-${RootConfigs.THEME.PRIMARY_COLOR}-700`
                                                                   }) => {
    const router = useRouter();
    const [isLargerThan768, setIsLargerThan768] = React.useState(false);

    React.useEffect(() => {
        const checkScreenSize = () => {
            setIsLargerThan768(window.innerWidth >= 768);
        };

        checkScreenSize();
        window.addEventListener('resize', checkScreenSize);

        return () => {
            window.removeEventListener('resize', checkScreenSize);
        };
    }, []);

    const isMenuItemSelected = (menuItem: NavigationMenuItem) =>
        menuItem.forceActive || router.asPath === menuItem.path;

    const renderMenuItem = (menuItem: NavigationMenuItem, isDesktop: boolean) => {
        const selected = isMenuItemSelected(menuItem);
        const baseColorClass = activeColor.startsWith('text-') ? activeColor : `text-${activeColor}`;

        return (
            <motion.div
                key={menuItem.path}
                onClick={() => router.push(menuItem.path)}
                className={`cursor-pointer relative ${isDesktop ? 'px-4 py-2' : 'p-2'}`}
                whileHover={isDesktop ? { scale: 1.05 } : undefined}
                whileTap={{ scale: 0.95 }}
            >
                <div className="relative flex items-center justify-center">
                    {menuItem.rightComponent && (
                        <div className="absolute -top-1 -left-1 z-10">
                            {menuItem.rightComponent}
                        </div>
                    )}

                    <div className={`flex items-center ${
                        isDesktop ? 'space-x-2' : 'flex-col space-y-1'
                    }`}>
                        {menuItem.icon && (
                            <div className={`transition-opacity duration-200 ${baseColorClass} ${
                                selected ? 'opacity-100' : 'opacity-40'
                            }`}>
                                <menuItem.icon size={isDesktop ? 20 : 24} />
                            </div>
                        )}

                        <AnimatePresence>
                            {(selected || !isDesktop) && (
                                <motion.span
                                    initial={isDesktop ? { width: 0, opacity: 0 } : undefined}
                                    animate={isDesktop ? { width: 'auto', opacity: 1 } : undefined}
                                    exit={isDesktop ? { width: 0, opacity: 0 } : undefined}
                                    transition={{ duration: 0.2 }}
                                    className={`whitespace-nowrap ${baseColorClass} ${
                                        selected ? 'opacity-100 font-semibold' : 'opacity-40'
                                    } ${isDesktop ? 'text-sm' : 'text-xs'}`}
                                >
                                    {menuItem.label}
                                </motion.span>
                            )}
                        </AnimatePresence>
                    </div>
                </div>

                {isDesktop && selected && (
                    <motion.div
                        layoutId="activeIndicator"
                        className={`absolute -bottom-1 left-0 right-0 h-0.5 ${baseColorClass.replace('text-', 'bg-')}`}
                        transition={{ duration: 0.3 }}
                    />
                )}
            </motion.div>
        );
    };

    const renderDesktopNavigation = () => (
        <nav className="flex items-center justify-center space-x-8 px-4 py-2">
            {menuItems.map((menuItem) => renderMenuItem(menuItem, true))}
        </nav>
    );

    const renderBottomNavigation = () => (
        <nav className="fixed bottom-0 left-0 right-0 bg-white border-t border-gray-200 px-2 py-1 shadow-[0_-4px_6px_-1px_rgba(0,0,0,0.1)] z-10">
            <div className="flex justify-around items-center">
                {menuItems.map((menuItem) => renderMenuItem(menuItem, false))}
            </div>
        </nav>
    );

    return (
        <div className="w-full">
            {isLargerThan768 ? renderDesktopNavigation() : renderBottomNavigation()}
        </div>
    );
};

export default NavigationHeaderMenu;