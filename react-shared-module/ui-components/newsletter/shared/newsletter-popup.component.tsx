import {
    Modal,
    ModalBody,
    ModalCloseButton,
    ModalContent,
    ModalHeader,
    ModalOverlay,
    useDisclosure
} from "@chakra-ui/react";
import NewsletterSignupListBannerComponent from "./newsletter-signup-list-banner.component";
import LocalStorageDataManager from "../../../logic/managers/LocalStorageData.manager";
import {useEffect} from "react";
import CommonUtils from "../../../logic/commonutils";
import {useSubscribedNewsletterState} from "../../../logic/global-hooks/root-global-state";
import {useAuthState} from "react-firebase-hooks/auth";
import {auth} from "../../../logic/shared-data/firebase.utils";
import AuthManager from "../../../logic/auth/auth.manager";

const LOCALSTORAGE_NEWSLETTER_POPUP = "newsletterPopup";
const NEWSLETTER_POPUP_DISPLAY_DELAY = 10 * 1000;

export default function NewsletterPopupComponent() {
    const { isOpen, onOpen, onClose } = useDisclosure();
    const [hasSubscribedNewsletter, setSubscribedNewsletter] = useSubscribedNewsletterState();
    const [currentUser, isAuthLoading] = useAuthState(auth);

    useEffect(() => {
        if (!isAuthLoading && !hasSubscribedNewsletter && !AuthManager.isUserLogged()) {
            const lastNewsletterPopup: number | undefined = LocalStorageDataManager.getItem(LOCALSTORAGE_NEWSLETTER_POPUP);
            if (!lastNewsletterPopup || CommonUtils.isOlderThanHours(lastNewsletterPopup, 24)) {
                setTimeout(() => {
                    onOpen();
                }, NEWSLETTER_POPUP_DISPLAY_DELAY);
            }

        }

        if (hasSubscribedNewsletter && isOpen) {
            onClose();
        }

    }, [isAuthLoading]);

    function onClosePopup() {
        LocalStorageDataManager.saveItem(LOCALSTORAGE_NEWSLETTER_POPUP, Date.now());
        onClose();
    }


    return (
        <div className='flex flex-col'>
            <Modal isOpen={isOpen} onClose={onClosePopup} size={"xl"} closeOnEsc={false} closeOnOverlayClick={true} isCentered={true}
                   motionPreset='slideInBottom'>
                <ModalOverlay />
                <ModalContent backgroundColor={"transparent"} shadow={""}>
                    <ModalHeader>
                        <ModalCloseButton color={"#FFFFFF"} />
                    </ModalHeader>
                    <ModalBody>
                        <NewsletterSignupListBannerComponent autoFocus={true} />
                    </ModalBody>
                </ModalContent>
            </Modal>
        </div>
    )
}