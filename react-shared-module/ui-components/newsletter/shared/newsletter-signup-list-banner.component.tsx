import {useEffect, useState} from "react";
import { Mail, ArrowRight, CheckCircle2, Loader2, X } from "lucide-react";
import AuthManager from "../../../logic/auth/auth.manager";
import {useSubscribedNewsletterState} from "../../../logic/global-hooks/root-global-state";
import CommonUtils from "../../../logic/commonutils";
import {SharedBackendApi} from "../../../logic/shared-data/sharedbackend.api";
import {SharedAnalyticsManager} from "../../../utils/shared-analytics.utils";
import JoinTelegramButtonComponent from "../../shared/JoinTelegramButton.component";
import LocalStorageDataManager from "../../../logic/managers/LocalStorageData.manager";

export default function NewsletterSignupListBannerComponent(props:{ title?: string, autoFocus?: boolean, isDismissEnabled?: boolean}) {
    const [formEmailValue, setFormEmailValue] = useState("");
    const [hasSubscribedNewsletter, setSubscribedNewsletter] = useSubscribedNewsletterState();
    const [hasSubscribedNow, setHasSubscribedNow] = useState(false);
    const [isLoading, setIsLoading] = useState(false);
    const [hasDismissNewsletter, setHasDismissNewsletter] = useState<boolean>(false);

    useEffect(() => {
        setHasDismissNewsletter(LocalStorageDataManager.getItem("hasDismissNewsletter", false));
    }, [hasDismissNewsletter]);

    const onFormEmailChanged = (e: any) => {
        setFormEmailValue(e.target.value.trim());
    };

    const isValidEmail = (email: any) => {
        return /^[^\s@]+@[^\s@]+\.[^\s@]+$/.test(email);
    };

    const onSubscribeClick = async () => {
        if (!CommonUtils.isValidEmail(formEmailValue)) {
            return;
        }
        setIsLoading(true);

        try {
            await SharedBackendApi.newsletterSubscribe({email: formEmailValue});
            setSubscribedNewsletter(true);
            setHasSubscribedNow(true);

            SharedAnalyticsManager.getInstance().logUserInteraction("newsletter_subscription", {
                "email": formEmailValue,
            });
        } catch (e) {
            console.error(e);
        }

        setIsLoading(false);
    };

    const renderSubscribeForm = () => (
        <div className="w-full max-w-md mx-auto">
            <div className="relative">
                <div className="absolute inset-y-0 left-0 flex items-center pl-4 pointer-events-none">
                    <Mail className="w-5 h-5 text-gray-400" />
                </div>
                <input
                    type="email"
                    autoFocus={props.autoFocus || false}
                    className="w-full pl-12 pr-32 py-3 bg-white rounded-full border border-gray-200 focus:border-blue-500 focus:ring-2 focus:ring-blue-200 outline-none transition-all duration-200"
                    placeholder="Enter your email"
                    value={formEmailValue}
                    onChange={onFormEmailChanged}
                />
                <button
                    className="absolute right-1 top-1 px-6 py-2 bg-gradient-to-r from-blue-500 to-blue-600 text-white rounded-full font-medium hover:from-blue-600 hover:to-blue-700 transition-all duration-200 flex items-center gap-2 disabled:opacity-50 disabled:cursor-not-allowed"
                    disabled={!isValidEmail(formEmailValue) || isLoading}
                    onClick={onSubscribeClick}
                >
                    {isLoading ? (
                        <Loader2 className="w-4 h-4 animate-spin" />
                    ) : (
                        <>
                            Subscribe
                            <ArrowRight className="w-4 h-4" />
                        </>
                    )}
                </button>
            </div>
        </div>
    );

    const renderSuccessSubscribed = () => (
        <div className="flex items-center justify-center gap-3 bg-green-50 py-4 px-6 rounded-full">
            <CheckCircle2 className="w-6 h-6 text-green-500" />
            <span className="text-green-800 font-medium">
                Thank you for subscribing to our newsletter!
            </span>
        </div>
    );

    if (hasSubscribedNow) {
        return renderSuccessSubscribed();
    }


    function onNewsletterDismiss() {
        setHasDismissNewsletter(true);
        LocalStorageDataManager.saveItem("hasDismissNewsletter", true);
    }

    function renderDismissButton() {
        if (props.isDismissEnabled) {
            return (
                <button
                    onClick={onNewsletterDismiss}
                    className="absolute top-4 right-4 p-2 text-gray-400 hover:text-gray-600 hover:bg-gray-100 rounded-full transition-all duration-200"
                    aria-label="Dismiss newsletter"
                >
                    <X className="w-5 h-5" />
                </button>
            )
        } else {
            return <></>
        }
    }

    const isDismissed: boolean = props.isDismissEnabled === true && hasDismissNewsletter;

    if (!AuthManager.isUserLogged() && !hasSubscribedNewsletter && !isDismissed)  {
        return (
            <div className="w-full px-4 py-8">
                <div className="max-w-4xl mx-auto bg-gradient-to-br from-gray-50 to-blue-50 rounded-2xl shadow-lg p-8 relative">
                    {renderDismissButton()}
                    <div className="text-center mb-8">
                        <span className="inline-block px-4 py-1 bg-blue-100 text-blue-600 rounded-full text-sm font-medium mb-4">
                            Newsletter
                        </span>
                        <p className="text-3xl font-bold text-gray-900 mb-4">
                            {props.title || "Stay up to date with our latest news"}
                        </p>
                        <p className="text-gray-600 max-w-md mx-auto">
                            Join our newsletter and get the latest updates delivered straight to your inbox.
                        </p>
                    </div>
                    {renderSubscribeForm()}
                    <JoinTelegramButtonComponent />
                </div>
            </div>
        );
    } else {
        return <></>
    }
}