import {SearchableItemModel} from "../../logic/shared-data/datamodel/searchable-item.model";
import {useState} from "react";
import {SharedBackendApi} from "../../logic/shared-data/sharedbackend.api";
import {getLanguageLabel} from "../../logic/language/language.helper";
import InputTextComponent from "../form/InputText.component";
import NotFoundSimpleMessageComponent from "../shared/NotFoundSimpleMessage.component";
import SearchResultItemDefaultComponent from "./search-result-items/search-result-item-default.component";
import Image from "next/image";

var stringSimilarity = require("string-similarity");
export default function SearchPageComponent(props: {renderSearchableItem?: (searchableItem: SearchableItemModel) => any}) {
    const [searchText, setSearchText] = useState("");
    const [isLoading, setIsLoading] = useState(true);
    const [listSearchResults, setListSearchResults] = useState<SearchableItemModel[]>([]);
    const [searchableItems, setSearchableItems] = useState<{[uid: string]: SearchableItemModel} | undefined>();

    // load searchable items
    if (!searchableItems) {
        SharedBackendApi.getSearchableItems().then((result) => {
            setSearchableItems(result);
            setIsLoading(false);
        });
    }

    function onSearchTextChanged(searchText: string) {
        setSearchText(searchText);
        if (searchableItems) {
            // Sort search items based on similarity
            let searchResults: SearchableItemModel[] = Object.values(searchableItems).sort((s1: SearchableItemModel, s2: SearchableItemModel) => {
                let textS1: string = s1.title;
                let textS2: string = s2.title;

                return stringSimilarity.compareTwoStrings(searchText, textS2) - stringSimilarity.compareTwoStrings(searchText, textS1);
            });

            if (searchResults.length > 20) {
                searchResults = searchResults.slice(0, 20);
            }
            setListSearchResults(searchResults);
        }
    }

    // Display header with the input box
    function renderSearchBox() {
        return (
            <div className='flex flex-col gap-10 items-center justify-center list-item-bg drop-shadow p-20 w-full'>
                <Image src={"/images/logo.png"} height={200} width={400} alt={"Logo"} />
                <h2 className='custom mb-10'>{getLanguageLabel("SEARCH_TITLE")}</h2>
                <InputTextComponent topLabel={""} hint={"Search for something"} onChanged={onSearchTextChanged} />
            </div>
        )
    }

    function renderResultItems() {
        if (listSearchResults) {
            return listSearchResults
                .map((searchItem) => {
                    if (props.renderSearchableItem) {
                        return props.renderSearchableItem(searchItem);
                    } else {
                        return (<SearchResultItemDefaultComponent key={searchItem.uid} searchItem={searchItem} />)
                    }
                });
        } else {
            return <NotFoundSimpleMessageComponent message={"No results yet..."} />
        }
    }

    return (
        <div className='flex flex-col gap-5 items-stretch w-full'>
            {renderSearchBox()}
            <div className='divider text-slate-400'>Search Results:</div>
            {renderResultItems()}
        </div>
    )

}