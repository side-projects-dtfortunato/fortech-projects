import {SearchableItemModel} from "../../../logic/shared-data/datamodel/searchable-item.model";
import Link from "next/link";
import {SharedRoutesUtils} from "../../../utils/shared-routes.utils";
import LinesEllipsis from "react-lines-ellipsis";

export default function SearchResultItemDefaultComponent(props: {searchItem: SearchableItemModel}) {

    function renderThumbnail() {
        let thumbnailShape: string = "rounded";
        if  (props.searchItem.type.toUpperCase() === "USER") {
            thumbnailShape = "rounded-full";
        }

        let thumbnailUrl: string = props.searchItem.thumbnail ? props.searchItem.thumbnail : "https://images.placeholders.dev/?width=500&height=500&text=No%20Pic&bgColor=%23f7f6f6&textColor=%236d6e71";
        return (
            <div className='avatar'>
                <div className={"w-20 h-20" + thumbnailShape}>
                    <img src={thumbnailUrl} alt={props.searchItem.title} />
                </div>
            </div>
        )
    }

    function getLinkHref() {
        switch (props.searchItem.type.toUpperCase()) {
            case "USER": return SharedRoutesUtils.getPublicProfilePageURL(props.searchItem.uid!);
            case "POST":
            default:
                return SharedRoutesUtils.getPostDetailsPageURL(props.searchItem.uid!);
        }
    }

    return (
        <Link className='list-item-bg p-3 drop-shadow flex flex-row gap-2 items-center hover:bg-slate-50' href={getLinkHref()}>
            {renderThumbnail()}
            <div className='flex flex-col gap-2'>
                <div className="badge badge-outline">{props.searchItem.type.toUpperCase()}</div>
                <LinesEllipsis text={props.searchItem.title} maxLine='2'
                               ellipsis='...'
                               trimRight
                               basedOn='letters'/>
            </div>
        </Link>
    )
}