import UserProfilePictureUpload from "./shared/UserProfilePictureUpload.component";
import {useState} from "react";
import AuthManager from "../../logic/auth/auth.manager";
import InputTextComponent from "../form/InputText.component";
import CountrySelectorFormComponent from "../form/CountrySelectorForm.component";
import InputTextAreaComponent from "../form/InputTextArea.component";
import {SharedCentralUserDataModel} from "../../logic/shared-data/datamodel/shared-central-user-data.model";
import {
    RootGlobalStateActions,
    useAlertMessageGlobalState,
    useCentralUserDataState,
    useIsLoadingGlobalState
} from "../../logic/global-hooks/root-global-state";
import {useRouter} from "next/router";
import {SharedBackendApi} from "../../logic/shared-data/sharedbackend.api";
import {APIResponseMessageUtils} from "../../logic/global-hooks/APIResponseMessageUtils";
import SpinnerComponent from "../shared/Spinner.component";
import EditProfileLinksComponent from "./shared/EditProfileLinks.component";
import CommonUtils from "../../logic/commonutils";
import InputCheckboxComponent from "../form/InputCheckbox.component";
import {getLanguageLabel} from "../../logic/language/language.helper";

export default function EditProfileFormComponent(props: { isNewUser?: boolean, headerTitle?: string, headerSubtitle?: string }) {
    const router = useRouter();
    const [globalCentralUserData, setGlobalCentralUserData] = useCentralUserDataState();
    const [centralUserData, setCentralUserData] = useState<SharedCentralUserDataModel | undefined | null>(globalCentralUserData);
    const [isLoading, setIsLoading] = useIsLoadingGlobalState();
    const [alertMessageState, setAlertMessageState] = useAlertMessageGlobalState();
    const [isUsernameChanged, setIsUsernameChanged] = useState(false);
    const [loadAttemptsCounter, setLoadAttemptsCounter] = useState(0);

    if (!centralUserData) {
        //setCentralUserData(CentralUserDataInstance);
        if (props.isNewUser && loadAttemptsCounter < 6) {
            // Wait a bit to get the Central User Data created
            setTimeout(() => {
                AuthManager.getCentralUserData(true).then((data) => {
                    setLoadAttemptsCounter(loadAttemptsCounter + 1);
                    setCentralUserData(data);
                });
            }, 4000);
        } else {
            AuthManager.getCentralUserData(true).then((data) => {
                setLoadAttemptsCounter(loadAttemptsCounter + 1);
                setCentralUserData(data);
            });
        }
    }

    function onNameChanged(text: string) {
        setCentralUserData({
            ...centralUserData,
            publicProfile: {
                ...centralUserData!.publicProfile,
                name: text,
            },
        });
    }

    function onUsernameChanged(text: string) {
        let slugUsername: string = CommonUtils.string_to_slug(text);
        setIsUsernameChanged(true);
        setCentralUserData({
            ...centralUserData,
            username: slugUsername,
            publicProfile: {
                ...centralUserData!.publicProfile,
                username: slugUsername,
            },
        });
    }

    function onLocationSelected(countryCode: string, countryLabel: string) {
        setCentralUserData({
            ...centralUserData,
            publicProfile: {
                ...centralUserData?.publicProfile,
                location: {
                    countryCode,
                    countryName: countryLabel,
                },
            }
        });
    }

    function onSubtitleChanged(text: string) {
        setCentralUserData({
            ...centralUserData,
            publicProfile: {
                ...centralUserData?.publicProfile,
                publicSubtitle: text,
            }
        });
    }

    function onBioDescription(text: string) {
        setCentralUserData({
            ...centralUserData,
            publicProfile: {
                ...centralUserData?.publicProfile,
                bioDescription: text,
            }
        });
    }

    function onNewsletterSubscribed(isChecked: boolean) {
        setCentralUserData({
            ...centralUserData,
            isNewsletterSubscribed: isChecked,
        });
    }

    async function onSaveChanges() {

        if (!centralUserData?.username || centralUserData.username.length === 0) {
            RootGlobalStateActions.displayAlertMessage({
                alertType: "ERROR",
                message: "Username can't be empty. Please, set a unique username",
                setAlertMessageState: setAlertMessageState,
            });
        } else {
            setIsLoading(true);

            // Check if the new username is valid
            if (isUsernameChanged && centralUserData?.username && centralUserData?.username.length > 0) {
                let checkUsernameResponse = await SharedBackendApi.checkUsernameValid({username: centralUserData?.username});

                if (!checkUsernameResponse.isSuccess) {
                    RootGlobalStateActions.displayAlertMessage({
                        alertType: "ERROR",
                        message: APIResponseMessageUtils.getAPIResponseMessage(checkUsernameResponse),
                        setAlertMessageState: setAlertMessageState,
                    });
                    setIsLoading(false);
                    return;
                } else {
                    // Disable username field now on
                    centralUserData.disableUsernameChange = true;
                    setIsUsernameChanged(false);
                }
            }

            const apiResponse = await SharedBackendApi.updateUserProfile({centralUserData: centralUserData!});

            if (apiResponse?.isSuccess) {
                RootGlobalStateActions.displayAlertMessage({
                    alertType: "SUCCESS",
                    message: "Great, your profile was successfully updated",
                    setAlertMessageState: setAlertMessageState,
                });
                setGlobalCentralUserData(centralUserData);
            } else {
                RootGlobalStateActions.displayAlertMessage({
                    alertType: "ERROR",
                    message: APIResponseMessageUtils.getAPIResponseMessage(apiResponse),
                    setAlertMessageState: setAlertMessageState,
                });
            }
            setIsLoading(false);
        }

    }

    async function onDeleteAccount() {
        setIsLoading(true);
        SharedBackendApi.deleteAccount().then((response) => {
            setIsLoading(false);
            if (response && response.isSuccess) {
                AuthManager.signOut();
                router.back();
            }
        })
    }

    function renderDeleteAccountModal() {

        return (
            <>
                <a href="#deleteaccountmodal" className='btn btn-ghost mt-5 btn-xs text-red-500'>Delete your account</a>
                <div id="deleteaccountmodal" className="modal" >
                    <div className="modal-box flex flex-col z-10 w-screen min-w-screen">
                        <p className="py-4">Are you sure you want to delete your account permanently?</p>
                        <div className="flex flex-row gap-2">
                            <a href="#" className="btn btn-ghost">Cancel</a>
                            <a href="#" className='btn btn-outline btn-error' onClick={onDeleteAccount}>Delete permanently
                            </a>
                        </div>
                    </div>
                </div>
            </>
        )
    }

    if (!centralUserData) {
        return (
            <div className='list-item-bg drop-shadow flex flex-col items-center justify-center p-3 my-5 gap-5'
                 style={{height: 300}}>
                <h3 className='custom'>{getLanguageLabel("EDIT_PROFILE_NEW_USER")}</h3>
                <SpinnerComponent/>
            </div>
        )
    } else {
        // @ts-ignore
        return (
            <div className='list-item-bg drop-shadow flex flex-col p-3 my-5 gap-8'>
                <div className='flex flex-col'>
                    <h2 className='custom'>{props.headerTitle ? props.headerTitle : "Edit my public profile"}</h2>
                    <span
                        className='subtitle'>{props.headerSubtitle ? props.headerSubtitle : "These infos will be display in your public profile."}</span>
                </div>

                <div className='flex flex-wrap gap-4 items-center'>
                    <UserProfilePictureUpload disabled={isLoading} centralUserData={centralUserData}
                                              setCentralUserData={setCentralUserData}/>
                    <div className='flex grow'><InputTextComponent
                        disabled={isLoading || centralUserData.disableUsernameChange} topLabel={"Username"}
                        hint={"Choose wisely, you can't change it after"} onChanged={onUsernameChanged}
                        value={centralUserData?.username} defaultValue={centralUserData?.username}/></div>
                    <div className='flex grow'><InputTextComponent topLabel={"Your email account"} hint={""}
                                                                   onChanged={onSubtitleChanged} disabled={true}
                                                                   defaultValue={centralUserData?.email}/></div>
                </div>

                <div className='my-1'>
                    <InputCheckboxComponent label={"I want to receive a Weekly Newsletter Digest"}
                                            isChecked={centralUserData.isNewsletterSubscribed !== false}
                                            onCheckedChanged={onNewsletterSubscribed}/>
                </div>


                <div className='flex flex-col sm:flex-row gap-2'>
                    <InputTextComponent disabled={isLoading} topLabel={"Your name"} hint={"What is your user name?"}
                                        onChanged={onNameChanged} defaultValue={centralUserData?.publicProfile?.name}/>
                    <CountrySelectorFormComponent disabled={isLoading}
                                                  defaultVal={centralUserData?.publicProfile?.location?.countryCode}
                                                  onCountrySelected={onLocationSelected}/>
                </div>

                <InputTextComponent disabled={isLoading} topLabel={"Headline"}
                                    hint={"Describe yourself or what you are doing in a few words"}
                                    maxLength={80} onChanged={onSubtitleChanged}
                                    defaultValue={centralUserData?.publicProfile?.publicSubtitle}/>

                <InputTextAreaComponent disabled={isLoading} topLabel={"About you"}
                                        hint={"Tell the community about yourself, your goals, your ideas..."}
                                        defaultValue={centralUserData?.publicProfile?.bioDescription}
                                        onChanged={onBioDescription}/>

                <div className='divider text-slate-400'>Additional links</div>

                <EditProfileLinksComponent centralUserData={centralUserData} setCentralUserData={setCentralUserData}/>

                <div className='flex flex-row gap-2'>
                    <button className={'btn btn-primary ' + (isLoading ? "loading" : "")} disabled={isLoading}
                            onClick={onSaveChanges}>Save changes
                    </button>
                    <button className='btn btn-ghost' disabled={isLoading} onClick={() => {
                        router.back();
                    }}>Cancel
                    </button>
                </div>

                {renderDeleteAccountModal()}
            </div>
        )
    }
}