import {useState} from "react";
import {SharedBackendApi} from "../../../logic/shared-data/sharedbackend.api";
import {SharedFirestoreCollectionDB} from "../../../logic/shared-data/datamodel/shared-firestore-collections";
import {SharedCentralUserDataModel} from "../../../logic/shared-data/datamodel/shared-central-user-data.model";
import AuthManager from "../../../logic/auth/auth.manager";
import {uploadFileStorage} from "../../../logic/shared-data/firebase.utils";
import {StorageFileModel} from "../../../logic/shared-data/datamodel/storage-file.model";
import UploadImageComponent from "../../form/UploadImage.component";

export default function UserProfilePictureUpload(props: {centralUserData: SharedCentralUserDataModel, setCentralUserData: (centralUserData: SharedCentralUserDataModel) => void, disabled?: boolean}) {
    // Form State
    const [formUserPictureState, setFormUserPictureState] = useState<string>("");
    const [uploadProgressPercentage, setProgressPercentage] = useState(0);

    function handlePictureChange(uploadFile: File) {

        // Upload picture
        let userProfilePicPath: string = AuthManager.getUserId() + "/images/" + "user_profile_photo";
        uploadFileStorage(uploadFile, userProfilePicPath, async (url: string) => {
            let profilePictureStorage: StorageFileModel = {
                fileUrl: url,
                storagePath: userProfilePicPath,
            };

            // Update states
            setFormUserPictureState(url);
            let updatedCentralUserData: SharedCentralUserDataModel = {
                ...props.centralUserData,
                publicProfile: {
                    ...props.centralUserData.publicProfile,
                    userPicture: profilePictureStorage,
                },
            };
            props.setCentralUserData(updatedCentralUserData);


            // Upload Central User Data
            await SharedBackendApi.requestDocumentChanges({collectionName: SharedFirestoreCollectionDB.CentralUserData, docId: AuthManager.getUserId(), docData: updatedCentralUserData, methodType: "MERGE"});
            setProgressPercentage(0);

        }, (percentage) => {
            setProgressPercentage(percentage);
        });

    }


    function renderPictureUpload() {
        let pictureUrl: string | undefined = formUserPictureState;
        if (!pictureUrl) {
            pictureUrl = props.centralUserData?.publicProfile?.userPicture?.fileUrl;
        }
        if (!pictureUrl) {
            pictureUrl = "/images/ic_user.png";
        }
        return (
            <div className='flex flex-col justify-start items-center'>
                {uploadProgressPercentage > 0 ? (<progress className="progress progress-primary w-16 mb-2" value={uploadProgressPercentage} max="100"></progress>) : (<></>)}
                <UploadImageComponent size={80} defaultImage={pictureUrl} onImageSelected={handlePictureChange} />
            </div>
        )
    }

    return (
        <div>
            {renderPictureUpload()}
        </div>
    );

}