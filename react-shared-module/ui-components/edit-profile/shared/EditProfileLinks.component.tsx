import {SharedCentralUserDataModel} from "../../../logic/shared-data/datamodel/shared-central-user-data.model";
import AddProfileLinkFormComponent from "./AddProfileLinkForm.component";
import {
    UserProfileLinkModel,
    UserProfileUtils
} from "../../../logic/shared-data/datamodel/shared-public-userprofile.model";
import {useState} from "react";

export default function EditProfileLinksComponent(props: {centralUserData: SharedCentralUserDataModel, setCentralUserData: (centralUserData: SharedCentralUserDataModel) => void}) {
    const [formLinksState, setLinksState] = useState(props.centralUserData.publicProfile?.links ? props.centralUserData.publicProfile?.links! : {});


    function onRemovedLink(removedIndex: string) {
        if (formLinksState[removedIndex]) {

            delete formLinksState[removedIndex];
            setLinksState(formLinksState);
            props.setCentralUserData({
                ...props.centralUserData,
                publicProfile: {
                    ...props.centralUserData.publicProfile,
                    links: formLinksState,
                },
            });
        }
    }

    function onLinkDataChanged(linkData: UserProfileLinkModel, index: string) {
        const updatedData: any = {
            ...formLinksState,
            [index]: linkData,
        };
        setLinksState(updatedData);
        props.setCentralUserData({
            ...props.centralUserData,
            publicProfile: {
                ...props.centralUserData.publicProfile,
                links: updatedData,
            },
        });
    }

    function onAddLinkClicked() {
        let nextLinkIndex: string = Date.now().toString();

        setLinksState({
            ...formLinksState,
            [nextLinkIndex]: {
                link: "",
                label: "",
            } as UserProfileLinkModel,
        });

        props.setCentralUserData({
            ...props.centralUserData,
            publicProfile: {
                ...props.centralUserData.publicProfile,
                links: formLinksState,
            },
        });
    }

    function renderListFields() {
        return Object.keys(formLinksState).sort((key1, key2) => key1.localeCompare(key2)).map((key, index) => {
            let linkData = formLinksState[key];
            return (
                <div key={key}>
                    <AddProfileLinkFormComponent index={key} linkData={linkData} displayLabel={index === 0} onRemovedClicked={onRemovedLink} onDataChanged={onLinkDataChanged} />
                </div>
            )
        });
    }

    const allValidLinks: boolean = UserProfileUtils.allLinkValid(props.centralUserData.publicProfile?.links);

    return (
        <div className='flex flex-col gap-4'>
            <h3 className='custom'>Additional Links</h3>
            {renderListFields()}
            <button className='btn btn-outline' disabled={!allValidLinks} onClick={onAddLinkClicked}>+ Add Link</button>
        </div>
    );
}