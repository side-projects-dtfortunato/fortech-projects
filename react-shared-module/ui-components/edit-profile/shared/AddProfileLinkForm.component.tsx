import {UserProfileLinkModel} from "../../../logic/shared-data/datamodel/shared-public-userprofile.model";
import InputTextComponent from "../../form/InputText.component";
import {useState} from "react";
import {useIsLoadingGlobalState} from "../../../logic/global-hooks/root-global-state";
import {TrashIcon} from "@heroicons/react/24/outline";
import CommonUtils from "../../../logic/commonutils";

export default function AddProfileLinkFormComponent(props: {index: string, linkData: UserProfileLinkModel, displayLabel: boolean, onRemovedClicked: (index: string) => void, onDataChanged: (linkData: UserProfileLinkModel, index: string) => void}) {
    const [formLinkData, setFormLinkData] = useState({...props.linkData});
    const [isLoading,  setIsLoading] = useIsLoadingGlobalState();


    function onLabelChanged(label: string) {
        let updatedFormLinkData: UserProfileLinkModel = {
            ...formLinkData,
            label: label,
        };
        setFormLinkData(updatedFormLinkData);
        props.onDataChanged(updatedFormLinkData, props.index);
    }

    function onLinkChanged(link: string) {
        let updatedFormLinkData: UserProfileLinkModel = {
            ...formLinkData,
            link: link,
        };

        setFormLinkData(updatedFormLinkData);
        props.onDataChanged(updatedFormLinkData, props.index);

        // Try to get Icon
        if (CommonUtils.isValidHttpUrl(link)) {
            CommonUtils.getUrlMetadata(link).then((metadata) => {
                if (metadata && metadata.icon) {
                    updatedFormLinkData = {
                        ...updatedFormLinkData,
                        iconUrl: metadata.icon,
                        link: link,
                    };
                    setFormLinkData(updatedFormLinkData);
                    props.onDataChanged(updatedFormLinkData, props.index);
                }
            });
        }
    }

    return (
        <div className='flex flex-row items-end gap-3'>
            <div className='mb-2 rounded bg-slate-200' style={{width: 32, height: 32}}>
                {formLinkData.iconUrl ? <img className='rounded' src={formLinkData.iconUrl} alt={"Weblink source"} width={32} height={32} /> : <></> }
            </div>
            <div className='flex basis-1/4'>
                <InputTextComponent inputType={"text"} disabled={isLoading} topLabel={props.displayLabel ? "Link Title" : ""} hint={"Link label"} defaultValue={formLinkData.label} onChanged={onLabelChanged} />
            </div>
            <div className='flex grow'>
                <InputTextComponent inputType={"url"} disabled={isLoading} topLabel={props.displayLabel ? "Website URL" : ""} hint={"Ex: https://twitter.com/username"} defaultValue={formLinkData.link} onChanged={onLinkChanged} />
            </div>
            <div>
                <button className='hidden sm:flex btn btn-ghost' disabled={isLoading} onClick={() => {props.onRemovedClicked(props.index)}}><span className='hover:text-red-600'>Remove</span></button>
                <button className='visible sm:hidden btn btn-ghost' disabled={isLoading} onClick={() => {props.onRemovedClicked(props.index)}}><TrashIcon width={20}/></button>
            </div>

        </div>
    )
}