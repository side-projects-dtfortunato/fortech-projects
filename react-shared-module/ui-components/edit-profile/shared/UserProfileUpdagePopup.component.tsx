import {useCentralUserDataState} from "../../../logic/global-hooks/root-global-state";
import {useDisclosure, Modal, ModalBody, ModalContent, ModalOverlay} from "@chakra-ui/react";
import UserProfilePictureUpload from "./UserProfilePictureUpload.component";
import InputTextComponent from "../../form/InputText.component";
import CommonUtils from "../../../logic/commonutils";
import {useEffect, useState} from "react";
import {SharedBackendApi} from "../../../logic/shared-data/sharedbackend.api";
import RootConfigs from "../../../../../configs";

export default function UserProfileUpdatePopupComponent() {
    const [centralUserData, setCentralUserData] = useCentralUserDataState();
    const { isOpen, onOpen, onClose } = useDisclosure();
    const [isLoading, setIsLoading] = useState(false);
    const [errorMessage, setErrorMessage] = useState("");

    useEffect(() => {
        if (centralUserData && !isProfileValid() && !isOpen) {
            onOpen();
        }
    }, [centralUserData]);



    function onClosePopup() {
        onClose();
    }

    function onUsernameChanged(text: string) {
        setErrorMessage("");
        let slugUsername: string = CommonUtils.string_to_slug(text);
        setCentralUserData({
            ...centralUserData,
            username: slugUsername,
            publicProfile: {
                ...centralUserData!.publicProfile,
                username: slugUsername,
            },
        });
    }

    function onDisplayNameChanged(text: string) {
        setErrorMessage("");
        setCentralUserData({
            ...centralUserData,
            publicProfile: {
                ...centralUserData!.publicProfile,
                name: text,
            },
        });
    }

    function onPublicSubtitleChanged(text: string) {
        setErrorMessage("");
        setCentralUserData({
            ...centralUserData,
            publicProfile: {
                ...centralUserData!.publicProfile,
                publicSubtitle: text,
            },
        });
    }

    async function onSaveChanges() {
        setIsLoading(true)
        // Check if the new username is valid
        /*if (centralUserData?.username && centralUserData?.username.length > 0) {
            let checkUsernameResponse = await SharedBackendApi.checkUsernameValid({username: centralUserData?.username});

            if (!checkUsernameResponse.isSuccess) {
                setIsLoading(false);
                setErrorMessage("Username is invalid or is already in use by other user. Please, try another one.");
                return;
            } else {
                // Disable username field now on
                centralUserData.disableUsernameChange = true;
            }
        }*/

        const apiResponse = await SharedBackendApi.updateUserProfile({centralUserData: centralUserData!});
        if (apiResponse.isSuccess) {
            onClose();
        } else {
            setErrorMessage(apiResponse.message ? apiResponse.message : "Some error occurred. Please, try again...");
        }

    }

    function isProfileValid(): boolean {
        return  CommonUtils.isStringNotEmpty(centralUserData?.username);
    }

    function renderUserProfileForm() {
        return (
            <div className='flex flex-col gap-2 bg-white rounded-lg p-5 drop-shadow-xl'>
                <h3 className='text-xl font-bold mb-3 text-slate-800'>Complete your profile</h3>
                <div className='flex flex-row gap-4 items-center justify-center'>
                    <UserProfilePictureUpload centralUserData={centralUserData!} setCentralUserData={setCentralUserData} />
                    <InputTextComponent key={"username"}
                        disabled={isLoading || centralUserData!.disableUsernameChange} topLabel={"Username"}
                        hint={"Get your unique username"} onChanged={onUsernameChanged}
                        value={centralUserData?.username} />
                </div>
                <InputTextComponent key={"name"}
                    disabled={isLoading} topLabel={"Your name"}
                    hint={"What is your name?"} onChanged={onDisplayNameChanged}
                    value={centralUserData?.publicProfile!.name} />
                <InputTextComponent key={"headline"}
                    disabled={isLoading} topLabel={"Headline"}
                    hint={"Tell us something about you (optional)"} onChanged={onPublicSubtitleChanged}
                    value={centralUserData?.publicProfile!.publicSubtitle} maxLength={80} />
                <div className='flex flex-row justify-end mt-4'>
                    <button className={`btn btn-success btn-sm w-28 ${isLoading ? "loading" : ""}`} disabled={!isProfileValid()} onClick={onSaveChanges}>Save</button>
                </div>
                <span className='w-full text-center text-red-500'>{errorMessage}</span>
            </div>
        )
    }


    if (centralUserData && RootConfigs.META_CONFIGS["disablePromptUsernameUpdate"] !== false) {
        return (
            <div className='flex flex-col'>
                <Modal isOpen={isOpen} onClose={onClosePopup} size={"xl"} closeOnEsc={false} closeOnOverlayClick={false} isCentered={true}
                       motionPreset='slideInBottom'>
                    <ModalOverlay />
                    <ModalContent backgroundColor={"transparent"} shadow={""}>
                        <ModalBody>
                            {renderUserProfileForm()}
                        </ModalBody>
                    </ModalContent>
                </Modal>
            </div>
        )
    } else {
        return (<></>);
    }

}