import {useState} from "react";
import ExpandCollapseButtonComponent from "../shared/ExpandCollapseButton.component";

export default function ListLoadMoreComponent<T>(props: {listItems: T[],
    onRenderItem: (item: T, index: number) => any, initialDisplayedItems: number}) {

    const [isLoadMoreOpened, setIsLoadMoreOpenedState] = useState(props.listItems.length <= props.initialDisplayedItems);

    function renderItems(): any[] {
        let listToRender = props.listItems;
        if (!isLoadMoreOpened) {
            listToRender = props.listItems.slice(0, props.initialDisplayedItems);
        }
        return listToRender.map((item, index) => (<li key={index} className='w-full'>{props.onRenderItem(item, index)}</li>));
    }


    return (
        <ul className='flex flex-col w-full gap-3'>
            {renderItems()}
            {!isLoadMoreOpened ? (
                <ExpandCollapseButtonComponent collapsedLabel={"Show More"}
                                               expandedLabel={"Show less"}
                                               isCollapsed={!isLoadMoreOpened} onClick={() => setIsLoadMoreOpenedState(true)} />
            ) : (<></>)}
        </ul>
    )
}