import Image from "next/image";
import AuthManager from "../../../logic/auth/auth.manager";
import {useRouter} from "next/router";
import {useAuthState} from "react-firebase-hooks/auth";
import {auth} from "../../../logic/shared-data/firebase.utils";

export default function GoogleSigninButtonComponent(props: {goAutoBack?: boolean}) {
    const route = useRouter();
    const [currentUser, isAuthLoading] = useAuthState(auth);

    async function onGoogleSignin() {
        AuthManager.signinWithGoogle(route, props.goAutoBack);
    }

    return (
        <button className='btn btn-accent text-white flex gap-2' onClick={onGoogleSignin} disabled={isAuthLoading}>
            <Image src={"/images/ic_google.png"} alt={"Google Signin"} width={20} height={20}/>
            <span>Sign in with Google</span>
        </button>
    )
}