import {useEffect} from "react";
import {useDisclosure, Modal, ModalBody, ModalCloseButton, ModalContent, ModalHeader, ModalOverlay} from "@chakra-ui/react";
import {useAuthState} from "react-firebase-hooks/auth";
import {auth} from "../../../logic/shared-data/firebase.utils";
import AuthManager from "../../../logic/auth/auth.manager";
import LoginRootComponent from "../LoginRoot.component";

export default function LoginPopupComponent(props: {isPopupOpen: boolean, onClosed: () => void}) {
    const { isOpen, onOpen, onClose } = useDisclosure();
    const [currentUser, isAuthLoading] = useAuthState(auth);


    useEffect(() => {
        if (props.isPopupOpen && !isOpen && !AuthManager.isUserLogged()) {
            onOpen();
        }

        if (AuthManager.isUserLogged() && isOpen) {
            onClosePopup();
        }
    }, [props.isPopupOpen, currentUser]);

    function onClosePopup() {
        onClose();
        props.onClosed();
    }

    return (
        <div className='flex flex-col'>
            <Modal isOpen={isOpen} onClose={onClosePopup} size={"xl"} closeOnEsc={false} closeOnOverlayClick={false} isCentered={true}
                   motionPreset='slideInBottom'>
                <ModalOverlay />
                <ModalContent backgroundColor={"transparent"} shadow={""} >
                    <ModalHeader>
                        <ModalCloseButton color={"#FFFFFF"} />
                    </ModalHeader>
                    <ModalBody>
                        <LoginRootComponent autoBack={false} />
                    </ModalBody>
                </ModalContent>
            </Modal>
        </div>
    )
}