import {useAuthState} from "react-firebase-hooks/auth";
import {useRouter} from "next/router";
import Link from "next/link";
import {getRecordPasswordPageUrl} from "../../../../pages/login/recover-password";
import {auth} from "../../logic/shared-data/firebase.utils";
import AlertMessageComponent from "../shared/AlertMessage.component";
import LogoComponent from "../shared/Logo.component";
import GoogleSigninButtonComponent from "./shared/GoogleSigninButton.component";
import RootConfigs from "../../../../configs";
import InputCheckboxV2Component from "../form/InputCheckboxV2.component";
import {useState} from "react";
import InputCheckboxComponent from "../form/InputCheckbox.component";

export default function LoginFormComponent(props: { submitSignin: (email: string, password: string) => void, submitCreateAccount: (email: string, password: string) => void, errorMessage?: string, autoBack?: boolean }) {
    const [isNewsletterChecked, setIsNewsletterChecked] = useState(false);

    // Auth Mechanism
    const route = useRouter();
    const [currentUser, isAuthLoading] = useAuthState(auth);
    let formSigninEmail: any;
    let formSigninPassword: any;



    function onForgotPasswordClick() {
        route.push(getRecordPasswordPageUrl(formSigninEmail.value));
    }

    function onSigninAccountClick(event: any) {
        if (event && props.submitSignin) {
            props.submitSignin(formSigninEmail.value, formSigninPassword.value);
        }
    }

    function onCreateAccountClick(event: any) {
        if (event && props.submitCreateAccount) {
            props.submitCreateAccount(formSigninEmail.value, formSigninPassword.value);
        }
    }

    const renderErrorMessage = props.errorMessage ? (
        <AlertMessageComponent type={"ERROR"} messageText={props.errorMessage}/>
    ) : (<></>);

    const submitButton = (

        <div className='flex flex-col items-center my-10'>
            <div className='flex flex-row justify-between w-full'>
                <button className='btn btn-primary text-white' disabled={isAuthLoading} onClick={onSigninAccountClick}>Login</button>
                <button className='ml-1 btn btn-secondary btn-outline' disabled={isAuthLoading || !isNewsletterChecked} onClick={onCreateAccountClick}>Create an
                    account
                </button>
            </div>
            {renderGoogleSigninContainer()}
        </div>
    )

    function renderGoogleSigninContainer() {
        if (RootConfigs.META_CONFIGS.disableGoogleSignin) {
            return (<></>);
        } else {
            return (
                <>
                    <div className='divider text-slate-400'>OR</div>
                    <GoogleSigninButtonComponent goAutoBack={props.autoBack === true || props.autoBack === undefined ? true : false}/>
                </>
            )
        }
    }

    function renderEULAAgreement() {
        return (
            <div>
                <Link className='text-xs italic text-slate-400 underline' href={"/eula"} target={"_blank"}>By creating an account, you agree to our End-User License Agreement (EULA). This includes our content policies and rules for user behavior.</Link>

            </div>
            
        )
    }

    const loadingButton = (
        <button className='btn loading'></button>
    )

    return (

        <div
            className="flex flex-col justify-center content-center w-full max-w-xl px-4 py-8 bg-white rounded-lg drop-shadow-xl sm:px-6 md:px-8 lg:px-10">
            <LogoComponent/>
            <div className="self-center mb-3 text-xl font-light text-gray-600 sm:text-2xl text-center mt-2">
                Login To Your Account
            </div>
            <div className="mt-8 w-full">
                <div className="flex flex-col mb-2 w-full">
                    <div className="flex relative ">
                    <span
                        className="rounded-l-md inline-flex  items-center px-3 border-t bg-white border-l border-b  border-gray-300 text-gray-500 shadow-sm text-sm">
                        <svg width="15" height="15" fill="currentColor" viewBox="0 0 1792 1792"
                             xmlns="http://www.w3.org/2000/svg">
                            <path
                                d="M1792 710v794q0 66-47 113t-113 47h-1472q-66 0-113-47t-47-113v-794q44 49 101 87 362 246 497 345 57 42 92.5 65.5t94.5 48 110 24.5h2q51 0 110-24.5t94.5-48 92.5-65.5q170-123 498-345 57-39 100-87zm0-294q0 79-49 151t-122 123q-376 261-468 325-10 7-42.5 30.5t-54 38-52 32.5-57.5 27-50 9h-2q-23 0-50-9t-57.5-27-52-32.5-54-38-42.5-30.5q-91-64-262-182.5t-205-142.5q-62-42-117-115.5t-55-136.5q0-78 41.5-130t118.5-52h1472q65 0 112.5 47t47.5 113z">
                            </path>
                        </svg>
                    </span>
                        <input type="text" id="signinEmail" ref={(el) => formSigninEmail = el}
                               className=" rounded-r-lg flex-1 appearance-none border border-gray-300 w-full py-2 px-4 bg-white text-gray-700 placeholder-gray-400 shadow-sm text-base focus:outline-none focus:ring-2 focus:ring-purple-600 focus:border-transparent"
                               placeholder="Your email"/>
                    </div>
                </div>
                <div className="flex flex-col mb-2">
                    <div className="flex relative ">
                        <span
                            className="rounded-l-md inline-flex  items-center px-3 border-t bg-white border-l border-b  border-gray-300 text-gray-500 shadow-sm text-sm">
                            <svg width="15" height="15" fill="currentColor" viewBox="0 0 1792 1792"
                                 xmlns="http://www.w3.org/2000/svg">
                                <path
                                    d="M1376 768q40 0 68 28t28 68v576q0 40-28 68t-68 28h-960q-40 0-68-28t-28-68v-576q0-40 28-68t68-28h32v-320q0-185 131.5-316.5t316.5-131.5 316.5 131.5 131.5 316.5q0 26-19 45t-45 19h-64q-26 0-45-19t-19-45q0-106-75-181t-181-75-181 75-75 181v320h736z">
                                </path>
                            </svg>
                        </span>
                        <input type="password" id="signinPassword" ref={(el) => formSigninPassword = el}
                               className=" rounded-r-lg flex-1 appearance-none border border-gray-300 w-full py-2 px-4 bg-white text-gray-700 placeholder-gray-400 shadow-sm text-base focus:outline-none focus:ring-2 focus:ring-purple-600 focus:border-transparent"
                               placeholder="Your password"/>
                    </div>
                </div>
                <div className="flex items-center mb-2 mt-1">
                    <div className="flex ml-auto">
                        <span onClick={onForgotPasswordClick}
                              className="cursor-pointer inline-flex text-xs font-thin text-black sm:text-sm hover:text-gray-700">
                            Forgot Your Password?
                        </span>
                    </div>
                </div>

                <InputCheckboxComponent label={"I understand that by creating an account, I will receive the Daily Newsletter"} isChecked={isNewsletterChecked} onCheckedChanged={setIsNewsletterChecked} />

                <div className="flex justify-center w-full mb-4">
                    {
                        (isAuthLoading) ? loadingButton : submitButton
                    }
                </div>

                {renderErrorMessage}
            </div>

            {renderEULAAgreement()}

            <div className="flex flex-col items-center justify-center mt-6">
                <Link href={"/"} className='btn btn-xs btn-ghost mt-5 w-24'>Back Home</Link>
            </div>
        </div>
    )
}