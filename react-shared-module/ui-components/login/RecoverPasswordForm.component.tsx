import {useState} from "react";
import Link from "next/link";
import CommonUtils from "../../logic/commonutils";
import AuthManager from "../../logic/auth/auth.manager";
import LogoComponent from "../shared/Logo.component";
import AlertMessageComponent from "../shared/AlertMessage.component";

export default function RecoverPasswordFormComponent(props: { email?: string }) {
    const [formEmail, setFormEmail] = useState<string | undefined>(props.email);
    const [isValidEmail, setIsValidEmail] = useState<boolean>(CommonUtils.isValidEmail(formEmail));
    const [isLoading, setIsLoading] = useState<boolean>(false);
    const [displayMessageSuccess, setDisplayMessageSuccess] = useState<boolean>(false);

    let formSigninEmail: any;

    function onEmailInputChanged(text: string) {
        setFormEmail(text);
        setIsValidEmail(CommonUtils.isValidEmail(text));
        setDisplayMessageSuccess(false);
    }

    async function onRecoverPasswordClicked() {
        if (isValidEmail && formEmail) {
            setIsLoading(true);
            await AuthManager.recoverPassword(formEmail);
            setIsLoading(false);
            setDisplayMessageSuccess(true);
        }
    }

    return (

        <div className="flex flex-col justify-content-center align-items-center w-full max-w-md px-4 py-8 bg-white rounded-lg shadow sm:px-6 md:px-8 lg:px-10">
            <LogoComponent/>
            <div className="self-center mb-3 text-xl font-light text-gray-600 sm:text-2xl text-center mt-2">
                Recover your password
            </div>
            <div className="mt-8">
                <div>
                    <div className="flex flex-col mb-2">
                        <div className="flex relative ">
                    <span className="rounded-l-md inline-flex  items-center px-3 border-t bg-white border-l border-b  border-gray-300 text-gray-500 shadow-sm text-sm">
                        <svg width="15" height="15" fill="currentColor" viewBox="0 0 1792 1792" xmlns="http://www.w3.org/2000/svg">
                            <path d="M1792 710v794q0 66-47 113t-113 47h-1472q-66 0-113-47t-47-113v-794q44 49 101 87 362 246 497 345 57 42 92.5 65.5t94.5 48 110 24.5h2q51 0 110-24.5t94.5-48 92.5-65.5q170-123 498-345 57-39 100-87zm0-294q0 79-49 151t-122 123q-376 261-468 325-10 7-42.5 30.5t-54 38-52 32.5-57.5 27-50 9h-2q-23 0-50-9t-57.5-27-52-32.5-54-38-42.5-30.5q-91-64-262-182.5t-205-142.5q-62-42-117-115.5t-55-136.5q0-78 41.5-130t118.5-52h1472q65 0 112.5 47t47.5 113z">
                            </path>
                        </svg>
                    </span>
                            <input type="text" id="signinEmail" ref={(el) => formSigninEmail = el} onChange={(e) => onEmailInputChanged(e.target.value)} defaultValue={props.email} className=" rounded-r-lg flex-1 appearance-none border border-gray-300 w-full py-2 px-4 bg-white text-gray-700 placeholder-gray-400 shadow-sm text-base focus:outline-none focus:ring-2 focus:ring-purple-600 focus:border-transparent" placeholder="Your email"/>
                        </div>
                    </div>

                    <div className="flex flex-col justify-center items-center w-full my-4">
                        <div className='flex flex-row'>
                            <button className='btn btn-outline' disabled={!isValidEmail} onClick={onRecoverPasswordClicked}>Recover password</button>
                        </div>
                        <div className={displayMessageSuccess ? "visible mt-3" : "hidden"}>
                            <AlertMessageComponent type={"SUCCESS"} messageText={"We just sent you an email with the next steps to recover your password."} />
                        </div>
                    </div>
                </div>
            </div>
            <div className="flex items-center justify-center mt-6">
                <Link href="/login" className="inline-flex items-center text-xs font-thin text-center text-gray-500 hover:text-gray-700">
                    <span className="ml-2 text-slate-600">
                        You don&#x27;t have an account? Create one now.
                    </span>
                </Link>
            </div>
        </div>
    )
}