import {useRouter} from "next/router";
import {useAuthState} from "react-firebase-hooks/auth";
import {auth} from "../../logic/shared-data/firebase.utils";
import {useState} from "react";
import AuthManager from "../../logic/auth/auth.manager";
import SpinnerComponent from "../shared/Spinner.component";
import LoginFormComponent from "./LoginForm.component";

interface SuccessMessageData {
    message: string,
    redirectLink: string,
}

export default function LoginRootComponent(props: {autoBack?: boolean}) {
    const route = useRouter();

    // Auth Mechanism
    const [currentUser, isAuthLoading] = useAuthState(auth);
    const [errorMessage, setErrorMessage] = useState<string | undefined>(undefined);
    const [isAPILoading, setIsAPILoading] = useState<boolean>(false);

    // Signin Function
    function onSubmitSignin(email: string, password: string, succesRedirect?: SuccessMessageData) {
        if (!isAuthLoading) {
            // setIsAPILoading(true);
            AuthManager.signInUser(email, password).then(async (res) => {
                if (res && props.autoBack) {
                    // Redirect user
                    if (succesRedirect) {
                        route.push(succesRedirect.redirectLink);
                    } else {
                        route.back();
                    }
                }

                setIsAPILoading(false);

            }).catch((err: any) => {
                let errorMessage = "Something failed. Probably because of invalid user credentials. Please, try again with the correct";

                switch (err.code) {
                    case "auth/invalid-email":
                        errorMessage = "Invalid email. Please, insert a valid email.";
                        break;
                    case "auth/user-not-found":
                        errorMessage = "User not found. Create an account to register a Remote Work account.";
                        break;
                    case "auth/wrong-password":
                        errorMessage = "Invalid password. Please, verify if you set the correct password.";
                        break;
                }
                setErrorMessage(errorMessage);
                setIsAPILoading(false);
            });
        }
    }

    // Create an Account Function
    async function onSubmitCreateAccount(email: string, password: string) {
        let errorMessage = "Something failed. Probably because of invalid user credentials. Please, try again with the correct";
        let isValid: boolean = true;

        // Internal validation
        if (!password || password.length < 8) {
            isValid = false;
            errorMessage = "Invalid password. Please, choose a password with at least 8 characters.";
        }

        if (isValid && !isAuthLoading) {
            AuthManager.createUserAccount(email, password).then((res) => {
                // route.push(getUserProfileLink(true));
                if (props.autoBack) {
                    route.back();
                }
            }).catch((err: any) => {
                errorMessage = err.code;

                switch (err.code) {
                    case "auth/invalid-email":
                        errorMessage = "Invalid email. Please, insert a valid email to create an account.";
                        break;
                    case "auth/email-already-in-use":
                        errorMessage = "We already have an account created with this email.";
                        break;
                }
                setErrorMessage(errorMessage);
            });
        } else {
            setIsAPILoading(false);
            setErrorMessage(errorMessage);
        }
    }

    // Render the root container
    function renderRootContainer() {

        if (isAuthLoading) {
            // Is  loading
            return (
                <div>
                    <SpinnerComponent/>
                </div>)
        } else if (AuthManager.isUserLogged()) {
            return (
                <div className='flex flex-col justify-content-center align-items-center list-item-bg drop-shadow p-5'>
                    <span className='my-3 text-xl'>Your are logged in</span>
                    <button className='btn btn-outline' onClick={() => route.push("/")}>BACK TO HOME</button>
                </div>
            )
        } else {
            // Sigin/create account component
            return (
                (<LoginFormComponent submitSignin={onSubmitSignin} submitCreateAccount={onSubmitCreateAccount} errorMessage={errorMessage} autoBack={props.autoBack}/>)
            )
        }
    }

    return (
        <>
            {renderRootContainer()}
        </>
    )
}