import React, { useEffect, useState } from 'react';
import RootConfigs from "../../../../configs";

export const AdsSlots = {
    HORIZONTAL_ADS: "3865374269",
    RECTANGULAR_ADS: "8160761702",
    VERTICAL_ADS: "3426390564",
    IN_ARTICLE: "4363917962", // format: fluid, layout: in-article
}

export default function GoogleAdsComponent(props: { slot: string, layout?: string, format?: string }) {
    const [adLoaded, setAdLoaded] = useState(true);


    const loadAds = () => {
        try {
            if (typeof window !== "undefined") {
                ((window as any).adsbygoogle = (window as any).adsbygoogle || []).push({});
                setAdLoaded(true);
            }
        } catch (error: any) {
            console.error("adsense error", error.message);
            setAdLoaded(false); // Mark the ad as failed to load
        }
    };

    useEffect(() => {
        const timer = setTimeout(() => {
            loadAds();
        }, 3000); // 3 second delay

        return () => clearTimeout(timer);
    }, []);



    if (RootConfigs.META_CONFIGS.disableGoogleAds) {
        return (<></>);
    } else {
        return (
            <div style={{ display: adLoaded ? "block" : "none" }}>
                <ins
                    className="adsbygoogle w-full h-full"
                    style={{ display: "block", textAlign: "center" }}
                    data-ad-client="ca-pub-5492791201497132"
                    data-ad-slot={props.slot}
                    data-ad-format={props.format ? props.format : "auto"}
                    data-ad-layout={props.layout ? props.layout : ""}
                    data-full-width-responsive="true"
                ></ins>
            </div>
        );
    }
}
