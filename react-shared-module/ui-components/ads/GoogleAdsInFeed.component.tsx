import React, { useCallback, useEffect, useState } from 'react';
import RootConfigs from "../../../../configs";

const MOBILE_BREAKPOINT = 640; // Corresponds to Tailwind's 'sm' breakpoint

export default function GoogleAdsInFeedComponent() {
    const [isMobile, setIsMobile] = useState(false);

    const checkMobile = useCallback(() => {
        setIsMobile(window.innerWidth < MOBILE_BREAKPOINT);
    }, []);

    useEffect(() => {
        // Check initial size
        checkMobile();

        // Add event listener for window resize
        window.addEventListener('resize', checkMobile);


        // Cleanup
        return () => {
            window.removeEventListener('resize', checkMobile);
        };
    }, [checkMobile]);

    useEffect(() => {
        // Load ads when showAd becomes true and isMobile is set
        try {
            if (typeof window !== "undefined") {
                ((window as any).adsbygoogle = (window as any).adsbygoogle || []).push({});
            }
        } catch (error: any) {
            console.error("adsense error", error.message);
        }
    }, [isMobile]);


    if (RootConfigs.META_CONFIGS.disableGoogleAds) {
        return (<></>);
    } else {
        if (isMobile) {
            return (
                <ins
                    className="adsbygoogle w-full"
                    style={{ display: "block" }}
                    data-ad-format="fluid"
                    data-ad-layout-key="-5o+bi-2m-15+xl"
                    data-ad-client="ca-pub-5492791201497132"
                    data-ad-slot="7650326781"
                />
            );
        } else {
            return (
                <ins
                    className="adsbygoogle w-full"
                    style={{ display: "block" }}
                    data-ad-format="fluid"
                    data-ad-layout-key="-go-3t+bx-4h-g1"
                    data-ad-client="ca-pub-5492791201497132"
                    data-ad-slot="2228709216"
                />
            );
        }
    }
}