import { useEffect } from 'react';
import Script from 'next/script';

const GoogleAdSense = () => {
    useEffect(() => {
        if (typeof window !== 'undefined') {
            try {
                ((window as any).adsbygoogle = (window as any).adsbygoogle || []).push({});
            } catch (error) {
                console.error("AdSense script error:", error);
            }
        }
    }, []);

    return (
        <>
            <Script
                id="adsense-script"
                async
                strategy="afterInteractive"
                onError={(e) => console.error('Failed to load AdSense script', e)}
                src={`https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js?client=ca-pub-5492791201497132`}
            />
        </>
    );
};

export default GoogleAdSense;
