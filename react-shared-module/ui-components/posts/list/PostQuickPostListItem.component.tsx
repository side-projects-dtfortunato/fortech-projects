import {BasePostDataModel, PostDataUtils} from "../../../logic/shared-data/datamodel/base-post-data.model";
import TagItemComponent from "../../shared/TagItem.component";
import UserHeaderInfosComponent from "../../shared/UserHeaderInfos.component";
import PostBottomStatsComponent from "../shared/PostBottomStats.component";
import PostUpvoteComponent from "../shared/PostUpvote.component";
import PostBookmarkToggleComponent from "../shared/PostBookmarkToggle.component";
import {CATEGORIES_OPTIONS} from "../../../../ui-helper/UIHelper";
import LinesEllipsis from 'react-lines-ellipsis'
import Link from "next/link";
import {SharedRoutesUtils} from "../../../utils/shared-routes.utils";
import {useEffect, useState} from "react";

export interface PostListItemContent {
    title: string;
    subtitle?: string;
    thumbnailUrl?: string;
}

export default function PostQuickPostListItemComponent(props: {basePostItem: BasePostDataModel, hideStats?: boolean, displayFullBody?: boolean}) {
    const [renderTagsList, setRenderTagsList] = useState<any[]>([]);

    useEffect(() => {
        setRenderTagsList(renderTags());
    }, []);

    function renderThumbnail() {
        if (props.basePostItem.thumbnailUrl) {
            return (
                <div className='w-full sm:basis-2/5' style={{minWidth: 125, height:140}}>
                    <div className='self-stretch rounded drop-shadow border bg-white' style={{backgroundImage: "Url(" + props.basePostItem?.thumbnailUrl + ")", backgroundSize: "cover", backgroundPosition: "center", width: "100%", height: "100%"}}/>
                </div>
            )
        } else {
           return (<></>);
        }
    }

    function renderTags() {
        let tagsList: any[] = [];

        if (props.basePostItem.category && CATEGORIES_OPTIONS[props.basePostItem.category]) {
            tagsList.push((<TagItemComponent key={props.basePostItem.category} label={CATEGORIES_OPTIONS[props.basePostItem.category].label} href={SharedRoutesUtils.getCategoryPostsPageURL(props.basePostItem.category)} filled={false} />));
        }

        if (props.basePostItem.tags && props.basePostItem.tags.length > 0) {
            tagsList = tagsList.concat(props.basePostItem.tags.map((tag) => {
                return (<TagItemComponent key={tag} label={tag} filled={false} href={SharedRoutesUtils.getTaggedPostsPageURL(tag)} />);
            }));
        }
        return tagsList;
    }

    return (
        <Link className='list-item-bg drop-shadow p-4 flex flex-col gap-3 hover:bg-slate-100' href={SharedRoutesUtils.getPostDetailsPageURL(props.basePostItem.uid!)}>
            {<UserHeaderInfosComponent userId={props.basePostItem.userCreatorId} isCreator={props.basePostItem.shortenPostData.isTheAuthor} userInfos={PostDataUtils.getUserInfos(props.basePostItem, props.basePostItem.userCreatorId)} />}
            <div className='flex flex-col sm:flex-row gap-3 items-start'>
                {renderThumbnail()}
                <div className='flex flex-row gap-3 grow w-full'>
                    <div className='flex flex-col gap-2 grow text-lg'>
                        {
                            props.displayFullBody && props.basePostItem.detailsPostData.body ?
                                (<span className='custom text-lg'>{props.basePostItem.detailsPostData.body}</span>) : (
                                    <LinesEllipsis text={props.basePostItem.summary} maxLine='3'
                                                   ellipsis='...'
                                                   trimRight
                                                   basedOn='letters'/>
                                )
                        }
                        <div className='flex flex-col h-full justify-end'>
                            <PostBottomStatsComponent postData={props.basePostItem} hideStats={props.hideStats}/>
                        </div>
                    </div>
                    <div className='grow flex flex-row justify-end'>
                        <div className={'flex flex-col items-center justify-start gap-1 ' + (props.hideStats ? " hidden" : "")}>
                            <PostUpvoteComponent postData={props.basePostItem} />
                            <PostBookmarkToggleComponent postData={props.basePostItem} />
                        </div>
                    </div>
                </div>
            </div>
            <div className='flex flex-wrap gap-2'>{renderTagsList}</div>
        </Link>
    )
}
