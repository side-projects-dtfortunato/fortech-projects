import {BasePostDataModel} from "../../../logic/shared-data/datamodel/base-post-data.model";
import {
    RootGlobalStateActions,
    useAlertMessageGlobalState,
    useCentralUserDataState,
    useIsLoadingGlobalState
} from "../../../logic/global-hooks/root-global-state";
import {UserRole} from "../../../logic/shared-data/datamodel/shared-central-user-data.model";
import {TrashIcon} from "@heroicons/react/24/outline";
import {useState} from "react";
import ConfirmActionContainerComponent from "../../shared/ConfirmActionContainer.component";
import {SharedBackendApi} from "../../../logic/shared-data/sharedbackend.api";
import {useRouter} from "next/router";

export default function PostRemoveBtnComponent(props: { postData: BasePostDataModel }) {
    const [centralUserData, setCentralUserData] = useCentralUserDataState();
    const [displayConfirmRemove, setDisplayConfirmRemove] = useState(false);
    const [isLoading, setIsLoading] = useIsLoadingGlobalState();
    const [displayErrorMessage, setDisplayErrorMessage] = useAlertMessageGlobalState();
    const router = useRouter();

    function onConfirmedRemovePost() {
        // API Request
        setIsLoading(true);
        setDisplayConfirmRemove(false);
        SharedBackendApi.removePostData({postId: props.postData.uid!}).then((response) => {
            setIsLoading(false);
            if (response.isSuccess) {
                router.push("/");
            } else {
                RootGlobalStateActions.displayAPIResponseError({apiResponse: response, setAlertMessageState: setDisplayErrorMessage});
            }
        });
    }

    function onRemoveClicked() {
        // Popup to confirm
        setDisplayConfirmRemove(!displayConfirmRemove);
    }

    function renderConfirmRemove() {
        if (displayConfirmRemove) {
            return (<ConfirmActionContainerComponent label={"Are you sure?"} size={15}
                                                     onCancel={() => {
                                                         setDisplayConfirmRemove(false)
                                                     }}
                                                     onConfirm={onConfirmedRemovePost}/>)
        } else {
            return (<></>);
        }

    }

    // Check if user is allowed to remove the post
    if (centralUserData && (props.postData.userCreatorId === centralUserData.uid || centralUserData.userRole === UserRole.ADMIN)) {
        return (
            <div className='flex flex-col items-center justify-center gap-2'>
                <button className={'btn btn-ghost flex flex-col items-center gap-1 ' + (isLoading ? "loading" : "")} disabled={isLoading} onClick={onRemoveClicked}>
                    {isLoading ? <></> : <TrashIcon height={20} color={"#FF0000"}/>}
                </button>
                {renderConfirmRemove()}
            </div>
        );
    } else {
        return (<></>);
    }
}