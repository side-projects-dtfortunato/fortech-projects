import {BasePostDataModel} from "../../../logic/shared-data/datamodel/base-post-data.model";
import {useCentralUserDataState} from "../../../logic/global-hooks/root-global-state";
import {UserRole} from "../../../logic/shared-data/datamodel/shared-central-user-data.model";
import {RiPushpinFill, RiUnpinLine} from "react-icons/ri";
import {useState} from "react";
import {SharedBackendApi} from "../../../logic/shared-data/sharedbackend.api";

export default function PostPinBtnComponent(props: {postData: BasePostDataModel}) {
    const [centralUserData, setCentralUserData] = useCentralUserDataState();
    const [isFeatured, setIsFeatued] = useState(props.postData.isFeatured);
    const [isLoading, setIsLoading] = useState(false);

    function onPinToggled() {
        if (centralUserData && centralUserData.userRole === UserRole.ADMIN) {
            setIsLoading(true);
            SharedBackendApi.setFeaturePost({postId: props.postData.uid!, isFeatured: !isFeatured})
                .then((response) => {
                    setIsLoading(false);

                    setIsFeatued(response.isSuccess ? !isFeatured : isFeatured);
                }).catch((err) => {
                    console.error(err);
                    setIsLoading(false);
            });
        }
    }

    if (centralUserData && centralUserData.userRole === UserRole.ADMIN) {
        return (
            <div className={"btn btn-ghost " + (isLoading ? "loading" : "")} onClick={onPinToggled}>
                {isFeatured ? <RiUnpinLine /> : < RiPushpinFill/>}
            </div>
        )
    } else {
        return (
            <></>
        )
    }

}