import {BasePostDataModel} from "../../../logic/shared-data/datamodel/base-post-data.model";
import {UIHelper} from "../../../../ui-helper/UIHelper";
import ListLoadMoreComponent from "../../lists/ListLoadMore.component";
import {useRouter} from "next/router";

export default function ListPostsComponent(props: {listPosts?: BasePostDataModel[], emptyMessage?: string, hideStats?: boolean, customSort?: (post1: BasePostDataModel, post2: BasePostDataModel) => number}) {
    const router = useRouter();

    if (props.listPosts && props.listPosts.length > 0) {
        return (
            <ListLoadMoreComponent
                listItems={props.listPosts}
                onRenderItem={(data, index) => UIHelper.renderPostListItem(data, index, props.hideStats)} initialDisplayedItems={12} />
        )
    } else {
        let emptyMessage: string = props.emptyMessage ? props.emptyMessage : "We don't have any post to display here yet...";

        return (
            <div className='flex flex-col py-10 px-3 gap-4 items-center'>
                <h3 className='custom text-center text-slate-300'>{emptyMessage}</h3>
                <button className='btn btn-outline' onClick={router.back}>Go back</button>
            </div>
        )
    }
}

function sortItemsByPublishedDate(p1: BasePostDataModel, p2: BasePostDataModel) {
    return p2.publishedAt - p1.publishedAt;
}