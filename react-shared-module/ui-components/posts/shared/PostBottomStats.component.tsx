import {BasePostDataModel} from "../../../logic/shared-data/datamodel/base-post-data.model";
import {ChatBubbleOvalLeftIcon, EyeIcon} from "@heroicons/react/24/outline";
import CommonUtils from "../../../logic/commonutils";
import {useEffect, useState} from "react";

const COLOR = "#999999";

export default function PostBottomStatsComponent(props: {postData: BasePostDataModel, hideStats?: boolean}) {
    const [publishAt, setPublishAt] = useState<string>("");

    useEffect(() => {
        if (props.postData.publishedAt) {
            setPublishAt(CommonUtils.getTimeAgo(props.postData.publishedAt));
        }

    }, []);


    function renderItem(key: string, icon: any, value: number) {
        return (
            <div key={key} className='flex flex-row justify-start items-center gap-1' color={"#FAFAFA"}>
                {icon}
                <span className='text-xs'>{value}</span>
            </div>
        )
    }
    function renderStats() {
        if (props.hideStats) {
            return (<></>);
        } else {
            let listItems: any[] = [];
            listItems.push(renderItem("views", (<EyeIcon width={15} />), props.postData.openViews));
            listItems.push(renderItem("comments",(<ChatBubbleOvalLeftIcon width={15} />), props.postData.numComments));
            return listItems;
        }
    }

    return (
        <div className='flex flex-row gap-2' style={{color: COLOR}}>
            {publishAt ? <span className='text-xs'>Published {publishAt}</span> : <></>}
            {renderStats()}
        </div>
    )
}