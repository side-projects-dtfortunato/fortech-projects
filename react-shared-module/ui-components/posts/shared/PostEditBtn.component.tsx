import {BasePostDataModel} from "../../../logic/shared-data/datamodel/base-post-data.model";
import {useCentralUserDataState} from "../../../logic/global-hooks/root-global-state";
import {UserRole} from "../../../logic/shared-data/datamodel/shared-central-user-data.model";
import {PencilIcon} from "@heroicons/react/24/outline";
import Link from "next/link";
import {PostTypes} from "../../../utils/shared-ui-helper";
import {SharedRoutesUtils} from "../../../utils/shared-routes.utils";

export default function PostEditBtnComponent(props: { postData: BasePostDataModel }) {
    const [centralUserData, setCentralUserData] = useCentralUserDataState();

    function isAllowedEdit(): boolean {
        if (props.postData.postType !== PostTypes.WRITE_ARTICLE && props.postData.postType !== PostTypes.ARTICLE_BLOCKS) {
            return false;
        }
        if (centralUserData && (props.postData.userCreatorId === centralUserData.uid || centralUserData.userRole === UserRole.ADMIN)) {
            return true;
        }
        return false;
    }

    // Check if user is allowed to remove the post
    if (isAllowedEdit()) {
        return (
            <div className='flex flex-col items-center justify-center gap-2'>
                <Link className={'btn btn-ghost flex flex-col items-center gap-1'} href={SharedRoutesUtils.getEditPostUrl(props.postData.uid!)}>
                    <PencilIcon height={20}/>
                </Link>
            </div>
        );
    } else {
        return (<></>);
    }
}