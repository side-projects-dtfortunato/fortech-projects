import {useAuthState} from "react-firebase-hooks/auth";
import {auth} from "../../../logic/shared-data/firebase.utils";
import {useState} from "react";
import AuthManager from "../../../logic/auth/auth.manager";
import {BasePostDataModel} from "../../../logic/shared-data/datamodel/base-post-data.model";
import {SharedBackendApi} from "../../../logic/shared-data/sharedbackend.api";
import {useRouter} from "next/router";
import {getLoginPageLink} from "../../../../../pages/login";
import {BookmarkIcon} from "@heroicons/react/24/outline";
import {BookmarkIcon as BookmarkIconFilled} from "@heroicons/react/24/solid";
import CommonUtils from "../../../logic/commonutils";

export default function PostBookmarkToggleComponent(props: {postData: BasePostDataModel}) {
    const router = useRouter();
    const [currentUser, isAuthLoading] = useAuthState(auth);
    const [isUserBookmarked, setUserBookmarkedState] = useState<boolean|undefined>(undefined);
    const [isLoading, setIsLoading] = useState(false);

    if (!isAuthLoading && AuthManager.isUserLogged() && isUserBookmarked === undefined) {
        let isBookmarkedUpdate = Object.keys(props.postData.bookmarkedBy ? props.postData.bookmarkedBy : {}).includes(AuthManager.getUserId()!);

        // Recheck if the user have bookmarked the post
        setUserBookmarkedState(isBookmarkedUpdate);
    }

    async function onToggleBookmark(e: any) {
        CommonUtils.preventClickPropagation(e);

        if (!AuthManager.isUserLogged()) {
            await router.push(getLoginPageLink());
            return;
        }

        let changeTo: boolean = !isUserBookmarked;
        setIsLoading(true);
        await SharedBackendApi.updatePostBookmark({postId: props.postData.uid!, postData: props.postData, addBookmark: changeTo});
        setUserBookmarkedState(changeTo);
        setIsLoading(false);
    }

    function renderBookmarkIcon() {
        if (!isUserBookmarked) {
            return (<BookmarkIcon height={20}/>);
        } else {
            return (<BookmarkIconFilled height={20} />);
        }

    }

    return (
        <button className={"btn btn-ghost " + (isLoading ? "loading" : "")} onClick={onToggleBookmark}>
            {isLoading ? (<></>) : renderBookmarkIcon()}
        </button>
    );
}