import {BasePostDataModel} from "../../../logic/shared-data/datamodel/base-post-data.model";
import {HandThumbUpIcon as OutlineHandThumbUpIcon} from "@heroicons/react/24/outline";
import {useState} from "react";
import AuthManager from "../../../logic/auth/auth.manager";
import {SharedBackendApi} from "../../../logic/shared-data/sharedbackend.api";
import {useAuthState} from "react-firebase-hooks/auth";
import {auth} from "../../../logic/shared-data/firebase.utils";
import {useRouter} from "next/router";
import {getLoginPageLink} from "../../../../../pages/login";
import CommonUtils from "../../../logic/commonutils";
import {HandThumbUpIcon as SolidHandThumbUpIcon} from "@heroicons/react/24/solid";

export default function PostUpvoteComponent(props: {postData: BasePostDataModel}) {
    const router = useRouter();

    const [upVotesCount, setUpVotesCount] = useState(props.postData.numReactions);
    const [isUpvotedByUser, setIsVotedByUser] = useState<boolean | undefined>( );
    const [isLoading, setIsLoading] = useState(false);
    const [currentUser, isAuthLoading] = useAuthState(auth);

    if (!isAuthLoading && props.postData.responses.reactions && isUpvotedByUser === undefined && AuthManager.getUserId()) {
        if (isUpvotedByUser !== (props.postData.responses.reactions[AuthManager.getUserId()!] !== undefined)) {
            setIsVotedByUser(props.postData.responses.reactions[AuthManager.getUserId()!] !== undefined);
        }
    }

    async function onUpvoteClicked(e: any) {
        CommonUtils.preventClickPropagation(e);

        // API Request
        if (AuthManager.isUserLogged()) {
            if (!isUpvotedByUser) {
                setUpVotesCount(upVotesCount + 1);
            } else {
                setUpVotesCount(upVotesCount - 1);
            }
            setIsVotedByUser(!isUpvotedByUser);
            SharedBackendApi.pushPostReaction({postId: props.postData.uid!, reactionType: "upvote", isToAdd: !isUpvotedByUser});
        } else {
            router.push(getLoginPageLink());
        }
    }

    function renderUpButton() {
        if (isUpvotedByUser) {
            return (
                <SolidHandThumbUpIcon height={20} color={"#E6375A"} />
            )
        } else {
            return (
                <OutlineHandThumbUpIcon height={20} color={"#333333"} />
            )
        }
    }

    return (
        <button className='btn btn-ghost flex flex-col items-center gap-1' disabled={isLoading} color={isUpvotedByUser ? "#E6375A" : "#333333"} onClick={onUpvoteClicked}>
            {renderUpButton()}
            <span className='text-sm' color={isUpvotedByUser ? "#E6375A" : "#333333"}>{upVotesCount}</span>
        </button>
    )
}