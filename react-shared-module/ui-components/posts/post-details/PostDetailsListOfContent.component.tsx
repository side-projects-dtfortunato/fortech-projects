import {BasePostDataModel} from "../../../logic/shared-data/datamodel/base-post-data.model";
import PostDetailsHeaderComponent from "./shared/PostDetailsHeader.component";
import PostDetailsCommentsSectionComponent from "./shared/PostDetailsCommentsSection.component";
import NotFoundContentComponent from "../../shared/NotFoundContent.component";
import {getLanguageLabel} from "../../../logic/language/language.helper";
import {PostListOfDetailsModel} from "../../../logic/shared-data/datamodel/posts-model/post-list-of";

export default function PostDetailsListOfContentComponent(props: { postDetails?: BasePostDataModel<any, PostListOfDetailsModel> }) {


    if (props.postDetails) {
        return (
            <div className='flex flex-col justify-center items-center gap-4'>
                <PostDetailsHeaderComponent showThumbnail={false} customSummary={props.postDetails.detailsPostData.bodyText} postDetails={props.postDetails} topLabel={getLanguageLabel("POST_HEADER_LIST_OF_TOP_LABEL")}/>
                <PostDetailsCommentsSectionComponent postDetails={props.postDetails} />
            </div>
        )
    } else {
        return <NotFoundContentComponent />;
    }

}