import {BasePostDataModel} from "../../../../logic/shared-data/datamodel/base-post-data.model";
import {getLanguageLabel} from "../../../../logic/language/language.helper";
import {SharedRoutesUtils} from "../../../../utils/shared-routes.utils";
import {UIHelper} from "../../../../../ui-helper/UIHelper";

export default function PostBreadcrumbsComponent(props: {postDetails: BasePostDataModel}) {

    return (
        <div className="text-sm breadcrumbs text-gray-500">
            <ul className='flex flex-wrap'>
                <li><a href={"/"}>{getLanguageLabel("ROOT_BREADCRUMB_LABEL")}</a></li>
                {props.postDetails.category.length > 0 ? <li><a href={SharedRoutesUtils.getCategoryPostsPageURL(props.postDetails.category)}>{UIHelper.getPostCategoryLabel(props.postDetails.category)}</a></li> : <></>}
                <li>{props.postDetails.title}</li>
            </ul>
        </div>
    )
}