import {BasePostDataModel} from "../../../../logic/shared-data/datamodel/base-post-data.model";
import {useState} from "react";
import PostCommentInputBoxComponent from "./PostCommentInputBox.component";
import AuthManager from "../../../../logic/auth/auth.manager";
import EmptyListMessageComponent from "../../../shared/EmptyListMessage.component";
import PostCommentListItemComponent from "./PostCommentListItem.component";
import {useAlertMessageGlobalState, useIsLoadingGlobalState} from "../../../../logic/global-hooks/root-global-state";
import {SharedBackendApi} from "../../../../logic/shared-data/sharedbackend.api";
import {APIResponseMessageUtils} from "../../../../logic/global-hooks/APIResponseMessageUtils";
import {useRouter} from "next/router";
import {getLoginPageLink} from "../../../../../../pages/login";

export default function PostDetailsCommentsSectionComponent(props: { postDetails: BasePostDataModel }) {
    const router = useRouter();
    const [postComments, setPostComments] = useState(props.postDetails.responses.comments);
    const [isLoading, setIsLoading] = useIsLoadingGlobalState();
    const [alertMessage, setAlertMessage] = useAlertMessageGlobalState();

    // List of comments
    function renderListComments() {
        if (postComments && Object.keys(postComments).length > 0) {
            return Object.values(postComments).sort((c1, c2) => c2.publishedAt - c1.publishedAt).map((commentData) => {
                return (<div key={commentData.uid} className='list-item-bg drop-shadow p-3'>
                    <PostCommentListItemComponent key={props.postDetails.uid} postData={props.postDetails} commentData={commentData} allowReply={true} />
                </div>);
            });
        } else {
            return (
                <EmptyListMessageComponent message={"Be the first to comment this post"} />
            )
        }
    }


    async function onSubmitComment(text: string) {
        // API Request
        if (AuthManager.isUserLogged()) {
            setIsLoading(true);
            SharedBackendApi.pushPostComment({postId: props.postDetails.uid!, message: text}).then((res) => {
                setIsLoading(false);
                if (res && res.isSuccess) {
                    // Force update
                    // setForcePageUpdate(true);
                    router.reload();
                    setAlertMessage({
                        alertType: "SUCCESS",
                        message: "Great, your comment was successfully published",
                        display: true,
                    });
                } else {
                    setAlertMessage({
                        alertType: "ERROR",
                        message: APIResponseMessageUtils.getAPIResponseMessage(res),
                        display: true,
                    });
                }
            }).catch((err) => {
                setIsLoading(false);
                setAlertMessage({
                    alertType: "ERROR",
                    message: "Some error occurred, please try again...",
                    display: true,
                });
            });
        } else {
            router.push(getLoginPageLink());
        }

    }

    return (
        <div className='flex flex-col gap-5 w-full'>
            <div className="divider text-slate-400">Discussion</div>
            <PostCommentInputBoxComponent onSubmitComment={onSubmitComment} />
            {renderListComments()}
        </div>
    )
}