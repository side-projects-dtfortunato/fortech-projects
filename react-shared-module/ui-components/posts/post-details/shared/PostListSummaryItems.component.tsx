import Link from "next/link";
import {
    PostListBulletItemModel,
    PostListItemsModel
} from "../../../../logic/shared-data/datamodel/base-post-data.model";

export default function PostListSummaryItemsComponent(props: {listItems?: PostListItemsModel}) {


    if (!props.listItems || props.listItems.listTitle.length < 2 || props.listItems.listItems.length === 0) {
        return (<></>);
    } else {
        return (
            <div className='flex flex-col gap-2 w-full'>
                <h2 className='custom'>{props.listItems.listTitle}</h2>
                <ul className="list-decimal ml-5">
                    {props.listItems.listItems.map((item: PostListBulletItemModel) => renderListItem(item))}
                </ul>
            </div>
        )
    }
}

export function renderListItem(postSummaryListItem: PostListBulletItemModel) {
    let renderTitle: any = (
        <li><h3 className='custom'>{postSummaryListItem.title}</h3></li>
    );
    if (postSummaryListItem.link) {
        renderTitle = (
            <Link href={postSummaryListItem.link}>{renderTitle}</Link>
        )
    }

    let thumbnailComponent: any = (<></>);
    if (postSummaryListItem.imageUrl) {
        thumbnailComponent = (
            <div className='flex justify-center w-full my-2'>
                <img className='rounded drop-shadow' src={postSummaryListItem.imageUrl} alt={postSummaryListItem.title} width={250} height={175}/>
            </div>
        );

        if (postSummaryListItem.link) {
            thumbnailComponent = (
                <Link href={postSummaryListItem.link}>
                    {thumbnailComponent}
                </Link>
            )
        }
    }

    return (
        <div className='gap-1 my-3'>
            {renderTitle}
            {thumbnailComponent}
            {postSummaryListItem.summary && postSummaryListItem.summary.length > 0 ? <span className="text">{postSummaryListItem.summary}</span> : <></>}
        </div>
    )
}