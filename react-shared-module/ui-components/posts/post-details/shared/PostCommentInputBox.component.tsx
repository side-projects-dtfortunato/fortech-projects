import {useState} from "react";
import {useIsLoadingGlobalState} from "../../../../logic/global-hooks/root-global-state";

export default function PostCommentInputBoxComponent(props: {onSubmitComment: (text: string) => Promise<void>, onCancel?: () => void}) {
    const [formInputText, setInputText] = useState("");
    const [isLoading, setIsLoading] = useIsLoadingGlobalState();


    let isValidSubmit: boolean = formInputText.length > 8;

    async function onSubmitClicked() {
        await props.onSubmitComment(formInputText);
        setInputText("");
    }

    function onCancelClicked() {
        setInputText("");
        if (props.onCancel) {
            props.onCancel();
        }
    }

    return (
        <div className='flex flex-col w-full gap-2'>
            <textarea className="p-2 rounded textarea textarea-bordered w-full" disabled={isLoading} placeholder="Say something about this post..." maxLength={1000} value={formInputText} onChange={(event) => setInputText(event.target.value)}/>
            <div className='flex flex-row justify-start gap-3'>
                <button className={"btn btn-outline " + (isLoading ? "loading" : "")} disabled={!isValidSubmit} onClick={onSubmitClicked}>POST COMMENT</button>
                {props.onCancel ? (<button className='btn btn-ghost' onClick={onCancelClicked}>CANCEL</button>) : (<></>) }
            </div>
        </div>
    )
}