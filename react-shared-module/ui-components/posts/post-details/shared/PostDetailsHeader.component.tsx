import {BasePostDataModel, PostDataUtils} from "../../../../logic/shared-data/datamodel/base-post-data.model";
import PostUpvoteComponent from "../../shared/PostUpvote.component";
import PostBottomStatsComponent from "../../shared/PostBottomStats.component";
import UserHeaderInfosComponent from "../../../shared/UserHeaderInfos.component";
import TagItemComponent from "../../../shared/TagItem.component";
import PostBookmarkToggleComponent from "../../shared/PostBookmarkToggle.component";
import {CATEGORIES_OPTIONS} from "../../../../../ui-helper/UIHelper";
import Linkify from 'react-linkify';
import Link from "next/link";
import PostListSummaryItemsComponent from "./PostListSummaryItems.component";
import {SharedRoutesUtils} from "../../../../utils/shared-routes.utils";
import PostRemoveBtnComponent from "../../shared/PostRemoveBtn.component";
import PostEditBtnComponent from "../../shared/PostEditBtn.component";
import PostShareThisComponent from "./PostShareThis.component";

export default function PostDetailsHeaderComponent(props: {showThumbnail?: boolean, titleLink?: string, postDetails: BasePostDataModel, withoutBgContainer?: boolean, topLabel?: string, customSummary?: string}) {

    function renderTags() {
        let tagsList: any[] = [];

        if (props.postDetails.category && CATEGORIES_OPTIONS[props.postDetails.category]) {
            tagsList.push((<TagItemComponent key={props.postDetails.category} label={CATEGORIES_OPTIONS[props.postDetails.category].label} href={SharedRoutesUtils.getCategoryPostsPageURL(props.postDetails.category)} filled={false} />));
        }

        if (props.postDetails.tags) {
            tagsList = tagsList.concat(props.postDetails.tags.map((tag) => {
                return (<TagItemComponent key={tag} label={tag} filled={false} href={SharedRoutesUtils.getTaggedPostsPageURL(tag)} />);
            }));
        }
        return tagsList;
    }

    function renderTopLabel() {
        if (props.topLabel) {
            return (
                <div className='badge'>
                    {props.topLabel}
                </div>
            )
        } else {
            return (<></>)
        }
    }

    function renderThumbnail() {
        if (props.postDetails.thumbnailUrl) {
            let thumbnailItem = (
                <div className='flex w-full' style={{height: 150, width: 150}}>
                    <div className='self-stretch rounded drop-shadow border bg-white' style={{backgroundImage: "Url(" + props.postDetails?.thumbnailUrl + ")", backgroundSize: "cover", backgroundPosition: "center", width: "100%", height: "100%"}}/>
                </div>
            );

            if (props.titleLink) {
                return (
                    <Link className='hover:opacity-70' href={props.titleLink} target={"_blank"}>
                        {thumbnailItem}
                    </Link>
                )
            } else {
                return thumbnailItem;
            }
        } else {
            return (
                <></>
            )
        }

    }

    function renderTitle() {
        if (props.titleLink) {
            return (
                <Link className='hover:opacity-70' href={props.titleLink} target={"_blank"}>
                    <h1 className='custom text-lg text-center'>{props.postDetails.title}</h1>
                </Link>
            )
        } else {
            return (
                <h1 className='custom text-center'>{props.postDetails.title}</h1>
            )
        }
    }

    function renderPostListItems() {
        if (props.postDetails.detailsPostData && props.postDetails.detailsPostData.postSummaryListItems) {
            return <PostListSummaryItemsComponent listItems={props.postDetails.detailsPostData.postSummaryListItems} />
        } else {
            return <></>
        }
    }

    function renderRootContainer() {
        return (
            <>
                {/*renderTopLabel()*/}
                <div className='flex flex-row gap-4 items-center w-full'>
                    <header className='flex flex-col flex-grow items-center justify-center gap-5'>
                        {renderTitle()}
                    </header>
                </div>
                <div className='flex'>
                    {props.showThumbnail ? renderThumbnail() : <></>}
                </div>

                <Linkify><span className='custom text-start italic'>{props.customSummary ? props.customSummary : props.postDetails.summary}</span></Linkify>

                <Linkify>{renderPostListItems()}</Linkify>



                <div className='flex flex-wrap gap-2 justify-center'>
                    {renderTags()}
                </div>
                <div className='flex flex-row items-center gap-2'>
                    <UserHeaderInfosComponent userId={props.postDetails.userCreatorId}
                                              isCreator={props.postDetails.shortenPostData.isTheAuthor}
                                              userInfos={PostDataUtils.getUserInfos(props.postDetails, props.postDetails.userCreatorId)}/>
                </div>
                <PostBottomStatsComponent postData={props.postDetails} />

                {/* Vertical Screen */}
                <div className='justify-center w-full'>
                    <div className='flex flex-row gap-2 items-center justify-center'>
                        <PostUpvoteComponent postData={props.postDetails} />
                        <PostBookmarkToggleComponent postData={props.postDetails} />
                        <PostRemoveBtnComponent postData={props.postDetails} />
                        <PostEditBtnComponent postData={props.postDetails} />
                    </div>
                </div>

                <div className='flex w-full justify-center items-center'>
                    <PostShareThisComponent title={props.postDetails.title} description={props.postDetails.summary} />
                </div>
            </>
        )
    }

    if (props.withoutBgContainer) {
        return renderRootContainer();
    } else {
        return (
            <div className='list-item-bg drop-shadow p-3 flex flex-col justify-center items-center gap-3 w-full'>
                {renderRootContainer()}
            </div>
        )
    }

}