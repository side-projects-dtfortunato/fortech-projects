import {
    BasePostComment,
    BasePostDataModel,
    PostDataUtils
} from "../../../../logic/shared-data/datamodel/base-post-data.model";
import UserHeaderInfosComponent from "../../../shared/UserHeaderInfos.component";
import CommonUtils from "../../../../logic/commonutils";
import {useState} from "react";
import PostCommentInputBoxComponent from "./PostCommentInputBox.component";
import {SharedBackendApi} from "../../../../logic/shared-data/sharedbackend.api";
import {APIResponseMessageUtils} from "../../../../logic/global-hooks/APIResponseMessageUtils";
import {useAlertMessageGlobalState, useIsLoadingGlobalState} from "../../../../logic/global-hooks/root-global-state";
import {useRouter} from "next/router";
import Linkify from 'react-linkify';

export default function PostCommentListItemComponent(props: {postData: BasePostDataModel, commentData: BasePostComment, allowReply: boolean}) {
    const router = useRouter();
    const [isReplyOpen, setReplyBoxOpen] = useState(false);
    const [commentReplies, setCommentReplies] = useState(props.commentData.replies);
    const [isLoading, setIsLoading] = useIsLoadingGlobalState();
    const [alertMessage, setAlertMessage] = useAlertMessageGlobalState();

    function renderListCommentReplies() {
        let listItems: any[] = [];

        if (commentReplies && Object.keys(commentReplies).length > 0) {
            listItems.push((<div className='divider text-slate-400'>Replies</div>))
            listItems = listItems.concat(Object.values(commentReplies).sort((c1, c2) => c1.publishedAt - c2.publishedAt).map((commentData) => {
                return (
                    <PostCommentListItemComponent key={commentData.uid} postData={props.postData} commentData={commentData} allowReply={false} />
                )
            }));
        }

        if (listItems.length > 0 && !isReplyOpen){
            // Also add the reply button on the bottom
            listItems.push(renderReplyBtn);
        }
        return listItems;
    }

    function renderReplyCommentBox() {
        if (!isReplyOpen) {
            return (<></>);
        } else {
            return (
                <PostCommentInputBoxComponent onSubmitComment={onPostCommentSubmit} onCancel={() => {
                    setReplyBoxOpen(false);
                }}/>
            )
        }
    }

    async function onPostCommentSubmit(text: string) {
        // API Request
        setIsLoading(true);
        const res = await SharedBackendApi.pushPostComment({postId: props.postData.uid!, message: text, replyCommentId: props.commentData.uid});
        setIsLoading(false);
        if (res && res.isSuccess) {
            // Force update
            // setForcePageUpdate(true);
            router.reload();
            setAlertMessage({
                alertType: "SUCCESS",
                message: "Great, your comment was successfully published",
                display: true,
            });
        } else {
            setAlertMessage({
                alertType: "ERROR",
                message: APIResponseMessageUtils.getAPIResponseMessage(res),
                display: true,
            });
        }
        setReplyBoxOpen(false);
    }

    function onReplyButtonClicked() {
        setReplyBoxOpen(!isReplyOpen);
    }

    const renderReplyBtn = props.allowReply ? (<div className='font-bold text-slate-700 cursor-pointer text-sm' onClick={onReplyButtonClicked}>Reply</div>) : (<></>);

    return (
        <div className='flex flex-col items-start gap-2'>
            <div className='flex flex-row gap-2 items-center'>
                <UserHeaderInfosComponent userId={props.commentData.userCreatorId}  isCreator={props.postData.userCreatorId === props.commentData.userCreatorId} userInfos={PostDataUtils.getUserInfos(props.postData, props.commentData.userCreatorId)} />
                <span className='text-xs text-slate-400'>{CommonUtils.getTimeAgo(props.commentData.publishedAt)}</span>
            </div>
            <Linkify><span className='text-slate-900 text-base'>{props.commentData.message}</span></Linkify>
            {renderReplyBtn}
            <div className='flex flex-col gap-4 pl-10 w-full'>
                {renderListCommentReplies()}
                {renderReplyCommentBox()}
            </div>
        </div>
    )
}