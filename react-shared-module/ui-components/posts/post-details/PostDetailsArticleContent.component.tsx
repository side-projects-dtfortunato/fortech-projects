import {BasePostDataModel} from "../../../logic/shared-data/datamodel/base-post-data.model";
import PostDetailsHeaderComponent from "./shared/PostDetailsHeader.component";
import PostDetailsCommentsSectionComponent from "./shared/PostDetailsCommentsSection.component";
import NotFoundContentComponent from "../../shared/NotFoundContent.component";
import {PostFormFullArticleDetails} from "../../../logic/shared-data/datamodel/posts-model/post-full-article";
import {getLanguageLabel} from "../../../logic/language/language.helper";

export default function PostDetailsArticleContentComponent(props: { postDetails?: BasePostDataModel<any, PostFormFullArticleDetails> }) {


    if (props.postDetails) {
        return (
            <div className='flex flex-col justify-center items-center gap-4'>
                <div className='list-item-bg drop-shadow p-5 flex flex-col justify-center items-center gap-3 w-full'>
                    <PostDetailsHeaderComponent postDetails={props.postDetails} withoutBgContainer={true} topLabel={getLanguageLabel("POST_HEADER_ARTICLE_TOP_LABEL")}/>
                    <div className='divider'/>
                    <article className='prose w-full flex max-w-full'>
                        <div dangerouslySetInnerHTML={{__html: props.postDetails.detailsPostData.bodyText}}/>
                    </article>
                </div>
                <PostDetailsCommentsSectionComponent postDetails={props.postDetails} />
            </div>
        )
    } else {
        return <NotFoundContentComponent />;
    }

}