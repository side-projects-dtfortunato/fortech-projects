import {BasePostDataModel} from "../../../logic/shared-data/datamodel/base-post-data.model";
import {PostFormShareLinkShortenModel} from "../../../logic/shared-data/datamodel/posts-model/post-share-link";
import PostDetailsHeaderComponent from "./shared/PostDetailsHeader.component";
import PostDetailsCommentsSectionComponent from "./shared/PostDetailsCommentsSection.component";
import NotFoundContentComponent from "../../shared/NotFoundContent.component";
import {getLanguageLabel} from "../../../logic/language/language.helper";

export default function PostDetailsDiscussionTopicContentComponent(props: { postDetails?: BasePostDataModel<PostFormShareLinkShortenModel> }) {


    if (props.postDetails) {
        return (
            <div className='flex flex-col justify-center items-center gap-4'>
                <PostDetailsHeaderComponent customSummary={props.postDetails.detailsPostData.bodyText} postDetails={props.postDetails} topLabel={getLanguageLabel("POST_HEADER_DISCUSSION_TOP_LABEL")}/>
                <PostDetailsCommentsSectionComponent postDetails={props.postDetails} />
            </div>
        )
    } else {
        return <NotFoundContentComponent />;
    }

}