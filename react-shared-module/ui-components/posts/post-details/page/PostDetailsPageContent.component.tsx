import {BasePostDataModel} from "../../../../logic/shared-data/datamodel/base-post-data.model";
import {useEffect, useState} from "react";
import {SharedBackendApi} from "../../../../logic/shared-data/sharedbackend.api";
import {PostTypes} from "../../../../utils/shared-ui-helper";
import PostDetailsLinkContentComponent from "../PostDetailsLinkContent.component";
import PostDetailsListOfContentComponent from "../PostDetailsListOfContent.component";
import PostDetailsDiscussionTopicContentComponent from "../PostDetailsDiscussionTopicContent.component";
import PostDetailsArticleContentComponent from "../PostDetailsArticleContent.component";
import NotFoundContentComponent from "../../../shared/NotFoundContent.component";
import ListPostsComponent from "../../shared/ListPosts.component";
import SideSimilarListPostsComponent from "../../../landing-page-content/shared/SideSimilarListPosts.component";
import {PageHeadProps} from "../../../root/RootLayout.component";
import RootConfigs from "../../../../../../configs";
import {SharedUtils} from "../../../../utils/shared-utils";
import CustomRootLayoutComponent from "../../../../../app-components/root/CustomRootLayout.component";
import PostBreadcrumbsComponent from "../shared/PostBreadcrumbs.component";
import GoogleAdsComponent, {AdsSlots} from "../../../ads/GoogleAds.component";
import PostDetailsProductContentComponent from "../PostDetailsProductContent.component";
import PostDetailsArticleBlocksContentComponent from "../PostDetailsArticleBlocksContent.component";
import PostDetailsQuickPostContentComponent from "../PostDetailsQuickPostContent.component";

export default function PostDetailsPageContentComponent(props: {postData?: BasePostDataModel, listSimilarPosts?: BasePostDataModel[]}) {
    const [postData, setPostData] = useState(props.postData);
    let hasPushedViewEvent: boolean = false;

    useEffect(() => {
        // Update views
        if (props.postData?.uid && !hasPushedViewEvent) {
            hasPushedViewEvent = true;
            SharedBackendApi.pushPostPageView({postId: props.postData?.uid});
        }
    });

    function renderPageContent() {
        if (props.postData) {
            switch (props.postData.postType as PostTypes) {
                case PostTypes.SHARE_LINK:
                    return (<PostDetailsLinkContentComponent postDetails={postData} />);
                case PostTypes.LIST_OF:
                    return (<PostDetailsListOfContentComponent postDetails={postData} />);
                case PostTypes.DISCUSSION:
                    return (<PostDetailsDiscussionTopicContentComponent postDetails={postData} />);
                case PostTypes.WRITE_ARTICLE:
                    return (<PostDetailsArticleContentComponent postDetails={postData} />);
                case PostTypes.ARTICLE_BLOCKS:
                    return (<PostDetailsArticleBlocksContentComponent postDetails={postData} />);
                case PostTypes.PRODUCT:
                    return (<PostDetailsProductContentComponent postDetails={postData} /> );
                case PostTypes.QUICK_ARTICLE:
                    return (<PostDetailsQuickPostContentComponent postDetails={postData} />);
            }
        }
        return (<NotFoundContentComponent />);
    }

    function renderSimilarPosts(displayMainListContent: boolean) {
        if (props.listSimilarPosts && props.listSimilarPosts.length > 0) {
            if (displayMainListContent) {
                return <ListPostsComponent listPosts={props.listSimilarPosts} />
            } else {
                return <SideSimilarListPostsComponent posts={props.listSimilarPosts} />
            }
        } else {
            return (<></>);
        }
    }

    const pageHeadProps: PageHeadProps = {
        title: props.postData?.title ? props.postData?.title : RootConfigs.SITE_TITLE,
        description: props.postData?.summary ? props.postData.summary : RootConfigs.SITE_DESCRIPTION,
        publishedTime: new Date(props.postData?.publishedAt!).toUTCString(),
        imgUrl: props.postData?.thumbnailUrl,
        jsonLdMarkup: props.postData ? SharedUtils.getSchemaMarkupFromPost(props.postData) : undefined,
    };


    const isIndexable: boolean = props.postData?.postType !== PostTypes.SHARE_LINK;
    return (
        <CustomRootLayoutComponent pageHeadProps={pageHeadProps} rigthChilds={renderSimilarPosts(false)} isIndexable={isIndexable}>
            <div className='w-full flex flex-col gap-2'>
                {props.postData ? <PostBreadcrumbsComponent postDetails={props.postData} /> : <></>}
                {renderPageContent()}
                <GoogleAdsComponent slot={AdsSlots.HORIZONTAL_ADS} />
                <div className='visible lg:hidden flex flex-col gap-2 my-10'>
                    <div className='divider text-slate-400'>RELATED POSTS</div>
                    {renderSimilarPosts(true)}
                </div>
            </div>
        </CustomRootLayoutComponent>
    )
}