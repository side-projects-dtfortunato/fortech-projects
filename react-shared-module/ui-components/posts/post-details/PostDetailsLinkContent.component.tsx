import {BasePostDataModel} from "../../../logic/shared-data/datamodel/base-post-data.model";
import {PostFormShareLinkShortenModel} from "../../../logic/shared-data/datamodel/posts-model/post-share-link";
import {PostLinkPreviewComponent} from "../../shared/PostLinkPreview.component";
import PostDetailsHeaderComponent from "./shared/PostDetailsHeader.component";
import PostDetailsCommentsSectionComponent from "./shared/PostDetailsCommentsSection.component";
import Link from "next/link";
import NotFoundContentComponent from "../../shared/NotFoundContent.component";
import CommonUtils from "../../../logic/commonutils";
import {getLanguageLabel} from "../../../logic/language/language.helper";

export default function PostDetailsLinkContentComponent(props: { postDetails?: BasePostDataModel<PostFormShareLinkShortenModel> }) {


    if (props.postDetails) {
        return (
            <div className='flex flex-col justify-center items-center gap-4'>
                <PostDetailsHeaderComponent titleLink={props.postDetails.shortenPostData.link} showThumbnail={true} postDetails={props.postDetails} topLabel={getLanguageLabel("POST_HEADER_LINK_TOP_LABEL")}/>
                <Link href={CommonUtils.addLinkSource(props.postDetails.shortenPostData.link)} className='btn btn-xs btn-outline' target="_blank">Visit full article</Link>
                <PostLinkPreviewComponent linkUrl={props.postDetails.shortenPostData.link} metadata={props.postDetails.shortenPostData.metaData} />
                <PostDetailsCommentsSectionComponent postDetails={props.postDetails} />
            </div>
        )
    } else {
        return <NotFoundContentComponent />;
    }

}