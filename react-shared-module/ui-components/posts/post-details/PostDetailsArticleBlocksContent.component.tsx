import {BasePostDataModel} from "../../../logic/shared-data/datamodel/base-post-data.model";
import PostDetailsHeaderComponent from "./shared/PostDetailsHeader.component";
import PostDetailsCommentsSectionComponent from "./shared/PostDetailsCommentsSection.component";
import NotFoundContentComponent from "../../shared/NotFoundContent.component";
import {getLanguageLabel} from "../../../logic/language/language.helper";
import {editorJsParser} from "editorjs-html-output";
import {PostFormArticleBlocksDetails} from "../../../logic/shared-data/datamodel/posts-model/post-article-blocks";
import GoogleAdsComponent, {AdsSlots} from "../../ads/GoogleAds.component";
import Image from "next/image";
import CommonUtils from "../../../logic/commonutils";

export default function PostDetailsArticleBlocksContentComponent(props: { postDetails?: BasePostDataModel<any, PostFormArticleBlocksDetails>, disableComments?: boolean}) {


    function getHtmlContent() {
        return editorJsParser((props.postDetails?.detailsPostData as PostFormArticleBlocksDetails).editorJSData!.blocks);
    }


    function renderBlock(blockData: any, type: string) {
        let blockToHtml: string = editorJsParser([blockData]);
        switch (type) {
            case "header":
                return (<div className='flex flex-col gap-1 w-full'>
                    {CommonUtils.getRandomNumber(101) > 49 ? <GoogleAdsComponent slot={AdsSlots.RECTANGULAR_ADS} /> : (<></>)}
                    <div className='flex w-full justify-start' dangerouslySetInnerHTML={{__html: blockToHtml}}/>
                </div>);
            case "image":
                return (
                    <Image className='list-item-bg drop-shadow w-full max-w-max' src={blockData.data.file.url} alt={blockData.data.caption} width={1000} height={400}  />
                )
            default:
                return (<div dangerouslySetInnerHTML={{__html: blockToHtml}}/>)
        }

    }

    function renderAggBlocks(blockData: any[]) {
        let blockToHtml: string = editorJsParser(blockData);
        return (<div className='w-full' dangerouslySetInnerHTML={{__html: blockToHtml}}/>)
    }

    function renderAllBlocks() {
        let listItems: any[] = [];

        let aggBlocks: any[] = [];

        for (let i = 0; i < (props.postDetails?.detailsPostData as PostFormArticleBlocksDetails).editorJSData!.blocks.length; i++) {
            let currentBlock: any = (props.postDetails?.detailsPostData as PostFormArticleBlocksDetails).editorJSData!.blocks[i];

            if (currentBlock.type !== "header" && currentBlock.type !== "image") {
                aggBlocks.push(currentBlock);
            } else {
                // First Render the previous ones
                listItems.push(renderAggBlocks(aggBlocks));
                aggBlocks = [];
                // Render the current index
                aggBlocks.push(currentBlock);
                listItems.push(renderBlock(currentBlock, currentBlock.type));
                aggBlocks = [];
            }
        }

        if (aggBlocks.length > 0) {
            listItems.push(renderAggBlocks(aggBlocks));
            aggBlocks = [];
        }
        /*(props.postDetails?.detailsPostData as PostFormArticleBlocksDetails).editorJSData!.blocks.map((blockItem) => {
            listItems.push(renderBlock(blockItem));
        });*/

        return <div className='flex flex-col justify-center items-center w-full'>
            {listItems}
        </div>;
    }

    if (props.postDetails) {
        return (
            <div className='flex flex-col justify-center items-center gap-4'>
                <div className='list-item-bg sm:drop-shadow p-5 flex flex-col justify-center items-center gap-3 w-full'>
                    <PostDetailsHeaderComponent postDetails={props.postDetails} withoutBgContainer={true} topLabel={getLanguageLabel("POST_HEADER_ARTICLE_TOP_LABEL")}/>
                    <div className='divider'/>
                    <article className='prose w-full flex max-w-full'>
                        {/* <div dangerouslySetInnerHTML={{__html: getHtmlContent()}}/> */}
                        {renderAllBlocks()}
                    </article>
                </div>
                {
                    props.disableComments ? <></> :
                        <PostDetailsCommentsSectionComponent postDetails={props.postDetails} />
                }
            </div>
        )
    } else {
        return <NotFoundContentComponent />;
    }

}