import {BasePostDataModel} from "../../../logic/shared-data/datamodel/base-post-data.model";
import GoogleAdsComponent, {AdsSlots} from "../../ads/GoogleAds.component";
import PostDetailsCommentsSectionComponent from "./shared/PostDetailsCommentsSection.component";
import NotFoundContentComponent from "../../shared/NotFoundContent.component";
import PostQuickPostListItemComponent from "../list/PostQuickPostListItem.component";

export default function PostDetailsQuickPostContentComponent(props: { postDetails?: BasePostDataModel<any, {body: string}>}) {

    if (props.postDetails) {
        return (
            <div className='flex flex-col justify-center items-stretch gap-4'>
                <PostQuickPostListItemComponent basePostItem={props.postDetails} displayFullBody={true} />
                <PostDetailsCommentsSectionComponent postDetails={props.postDetails} />
                <GoogleAdsComponent slot={AdsSlots.HORIZONTAL_ADS} />
            </div>
        )
    } else {
        return <NotFoundContentComponent />;
    }
}