import {useState} from "react";
import {SvgIconsUtils} from "../../../ui-helper/SvgIcons.utils";
import TagItemComponent from "../shared/TagItem.component";

export interface PricingTablePlanContentModel {
    planName: string,
    planDescription: string,
    priceLabel: string,
    listFeatures: PricingTableFeatureItem[],
}

export interface PricingTableFeatureItem {
    label: string,
    type: "INCLUDED" | "NEUTRAL" | "NOT_INCLUDED",

}

export type PaymentPeriodType = "monthlyPrice" | "yearlyPrice";

export default function PricingTableComponent(props: {
    planTableContent: PricingTablePlanContentModel,
    isCurrentPlan?: boolean,
    billingPeriod: PaymentPeriodType,
    buttonComponent: any,
    isRecommended?: boolean,
}) {
    const [isLoading, setIsLoading] = useState(false);

    function renderIsCurrentPlan() {
        return (
            <div className="badge badge-primary badge-outline">Your Current Plan</div>
        )
    }

    function renderFeatureItem(featureItem: PricingTableFeatureItem) {
        let renderType: any;

        switch (featureItem.type) {
            case "INCLUDED":
                renderType = (
                    <div className='rounded-full w-6 h-6 p-0.5 bg-green-100 text-green-500'>
                        {SvgIconsUtils.icon_check}
                    </div>
                )
                break;
            case "NEUTRAL":
                renderType = (
                    <div className='rounded-full w-8 h-8 p-2 bg-slate-100 text-slate-500'>
                        {SvgIconsUtils.icon_minus}
                    </div>
                )
                break;
            case "NOT_INCLUDED":
                renderType = (
                    <div className='rounded-full w-8 h-8 p-2 bg-red-100 text-red-500'>
                        {SvgIconsUtils.icon_xmark}
                    </div>
                )
                break;

        }

        return (
            <div className='flex flex-row justify-start items-center gap-2'>
                {renderType}
                <span className='text-slate-700 text-sm'>{featureItem.label}</span>
            </div>
        )
    }

    function renderPricingFeatures() {
        return props.planTableContent.listFeatures
            .map((featureItem) => renderFeatureItem(featureItem));
    }

    function renderPriceLabel() {
        let periodLabel: string = props.billingPeriod === "yearlyPrice" ? "Year" : "Month";
        let renderBilledYearlyLabel: any = props.billingPeriod === "yearlyPrice" ? (
            <span className='text-sm text-slate-300 text-center'></span>) : <></>;
        return (
            <div className='flex flex-col gap-1 justify-center items-center'>
                <div className='flex flex-row justify-center items-center'>
                    <h2 className="custom">{props.planTableContent.priceLabel}</h2>
                    <span className='text-base text-slate-400'>/{periodLabel}</span>
                </div>
                {renderBilledYearlyLabel}
            </div>
        )
    }

    function renderRecommendedLabel() {
        if (props.isRecommended) {
            return <TagItemComponent label={"RECOMMENDED"} />
        } else {
            return <></>
        }
    }

    return (
        <div className='list-item-bg drop-shadow flex flex-col gap-2 items-stretch justify-start max-w-64 py-2'>
            <div className='flex flex-col items-center justify-between p-2 gap-3' style={{minHeight: "21vh"}}>
                <h2 className='custom'>{props.planTableContent.planName}</h2>
                {renderRecommendedLabel()}
                {props.isCurrentPlan ? renderIsCurrentPlan() : <></>}
                <span className='subtitle text-center'>{props.planTableContent.planDescription}</span>
                {renderPriceLabel()}
            </div>
            <div className='flex w-full justify-center p-2'>
                {props.buttonComponent}
            </div>
            <div className='divider'/>
            <div className='flex flex-col items-start p-2 gap-2'>
                {renderPricingFeatures()}
            </div>
        </div>
    )
}