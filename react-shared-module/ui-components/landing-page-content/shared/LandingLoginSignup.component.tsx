import {useAuthState} from "react-firebase-hooks/auth";
import {auth} from "../../../logic/shared-data/firebase.utils";
import AuthManager from "../../../logic/auth/auth.manager";
import Link from "next/link";
import {getLoginPageLink} from "../../../../../pages/login";
import GoogleSigninButtonComponent from "../../login/shared/GoogleSigninButton.component";
import {getLanguageLabel} from "../../../logic/language/language.helper";
import LogoComponent from "../../shared/Logo.component";
import RootConfigs from "../../../../../configs";
import JoinTelegramButtonComponent from "../../shared/JoinTelegramButton.component";

export default function LandingLoginSignupComponent() {
    const [currentUser, isAuthLoading] = useAuthState(auth);

    function renderRandomUsers() {
        return (
            <div className="avatar-group -space-x-6 flex flex-row justify-center items-center">
                <div className="avatar">
                    <div className="w-10">
                        <img src="https://xsgames.co/randomusers/avatar.php?g=female" />
                    </div>
                </div>
                <div className="avatar">
                    <div className="w-10">
                        <img src="https://xsgames.co/randomusers/avatar.php?g=male" />
                    </div>
                </div>
                <div className="avatar">
                    <div className="w-10">
                        <img src="https://placeimg.com/192/192/people" />
                    </div>
                </div>
                <div className="avatar placeholder">
                    <div className="w-10 bg-slate-200 text-black">
                        <span>+2K</span>
                    </div>
                </div>
            </div>
        )
    }

    function renderGoogleSigninContainer() {
        if (RootConfigs.META_CONFIGS.disableGoogleSignin) {
            return (<></>);
        } else {
            return (
                <>
                    <div className='divider sm:divider-horizontal text-slate-400'>OR</div>
                    <GoogleSigninButtonComponent goAutoBack={false} />
                </>
            )
        }
    }

    if (!isAuthLoading && !AuthManager.isUserLogged()) {
        return (
            <div className='list-item-bg drop-shadow p-5 flex flex-col justify-center items-center gap-4'>
                <LogoComponent height={100} width={200}/>
                <h2 className='custom text-center'>{getLanguageLabel("LANDING_PAGE_HEADER_JOIN_COMMUNITY_TITLE")}</h2>
                <span className='subtitle text-center'>{getLanguageLabel("LANDING_PAGE_HEADER_JOIN_COMMUNITY_SUBTITLE")}</span>
                <div className='flex flex-col sm:flex-row gap-1 w-full items-center justify-center'>
                    <Link href={getLoginPageLink()} className="btn btn-primary btn-outline">Signup/Sign-in</Link>
                    {renderGoogleSigninContainer()}
                </div>

                <JoinTelegramButtonComponent/>
            </div>
        )
    } else {
        return (<></>)
    }
}