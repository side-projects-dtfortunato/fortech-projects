import {BasePostDataModel} from "../../../logic/shared-data/datamodel/base-post-data.model";
import Link from "next/link";
import {SharedRoutesUtils} from "../../../utils/shared-routes.utils";

export default function LandingTagPopularSidelistComponent(props: {tag: string, posts: BasePostDataModel[]}) {

    function renderPostItems() {
        return props.posts.map((post) => {
            return (
                <li className='flex flex-col bordered"' key={post.uid}>
                    <Link className='flex flex-col items-start' href={SharedRoutesUtils.getPostDetailsPageURL(post.uid!)}>
                        <span className='custom text-start'>{post.title}</span>
                        {post.listUsersInfos ? (<span className='subtitle'>{post.listUsersInfos[post.userCreatorId]?.name}</span>) : (<></>)}
                    </Link>
                </li>
            )
        });
    }

    return (
        <ul className="list-item-bg drop-shadow menu bg-base-100 w-72">
            <li><Link href={SharedRoutesUtils.getTaggedPostsPageURL(props.tag.toLowerCase())}><h3 className='custom'>#{props.tag.toUpperCase()}</h3></Link></li>
            {renderPostItems()}
        </ul>
    )
}