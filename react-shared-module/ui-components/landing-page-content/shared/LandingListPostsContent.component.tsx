import {
    SharedLandingPageAggregatorsModel
} from "../../../logic/shared-data/datamodel/shared-landing-page-aggregators.model";
import {useState} from "react";
import {useCentralUserDataState} from "../../../logic/global-hooks/root-global-state";
import {BasePostDataModel} from "../../../logic/shared-data/datamodel/base-post-data.model";
import ListPostsComponent from "../../posts/shared/ListPosts.component";
import NotFoundContentComponent from "../../shared/NotFoundContent.component";
import {LandingPageTabTypes, UIHelper} from "../../../../ui-helper/UIHelper";

export default function LandingListPostsContentComponent(props: { landingDataAgg: SharedLandingPageAggregatorsModel }) {
    const [tabSelection, setTabSelection] = useState<LandingPageTabTypes>(Object.values(LandingPageTabTypes)[0]);
    const [centralUserData, setCentralUserData] = useCentralUserDataState();


    function renderListPosts() {
        if (props.landingDataAgg) {
            let listPosts: BasePostDataModel[] = UIHelper.getLandingPageTabListPosts(tabSelection, props.landingDataAgg, centralUserData);

            return (
                <ListPostsComponent listPosts={listPosts} />
            )

        } else {
            return (<NotFoundContentComponent />)
        }
    }

    function onTabSelectionChanged(tabType: LandingPageTabTypes) {
        setTabSelection(tabType);
    }
    function renderTab(tabType: LandingPageTabTypes) {
        let tabLabel: string = tabType;

        let suffixClass: string = "";
        if (tabType === tabSelection) {
            suffixClass = "tab-active"
        }
        return (
            <button key={tabType} className={"carousel-item tab lg:tab-lg tab-lifted " + suffixClass} onClick={() => onTabSelectionChanged(tabType)}>{tabLabel}</button>
        )
    }



    return (
        <div className='flex flex-col'>
            <div className='carousel'>
                {Object.values(LandingPageTabTypes).map((tabType) => renderTab(tabType))}
            </div>
            {renderListPosts()}
        </div>
    )
}
