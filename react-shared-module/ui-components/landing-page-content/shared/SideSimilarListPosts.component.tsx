import {BasePostDataModel} from "../../../logic/shared-data/datamodel/base-post-data.model";
import Link from "next/link";
import {SharedRoutesUtils} from "../../../utils/shared-routes.utils";
import ListHeadhlineSeparatorComponent from "../../shared/ListHeadhlineSeparator.component";

export default function SideSimilarListPostsComponent(props: {posts: BasePostDataModel[]}) {

    function renderPostItems() {
        return props.posts.map((post) => {
            return (
                <li className='flex flex-col bordered"' key={post.uid}>
                    <Link className='flex flex-col items-start' href={SharedRoutesUtils.getPostDetailsPageURL(post.uid!)}>
                        <span className='custom text-start'>{post.title}</span>
                        <span className='subtitle'>{post.listUsersInfos[post.userCreatorId].name}</span>
                    </Link>
                </li>
            )
        });
    }

    return (
        <div className='flex flex-col gap-2'>
            <ListHeadhlineSeparatorComponent text={"Related Posts"} color={"#080D4A"} />
            <ul className="list-item-bg drop-shadow menu bg-base-100 w-full">
                {renderPostItems()}
            </ul>
        </div>
    )
}