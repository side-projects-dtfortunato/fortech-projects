import {useEffect, useState} from "react";
import LocalStorageDataManager from "../../../logic/managers/LocalStorageData.manager";
import {
    SharedFirestoreCollectionDB,
    SharedFirestoreDocsDB
} from "../../../logic/shared-data/datamodel/shared-firestore-collections";
import Link from "next/link";
import {getLanguageLabel} from "../../../logic/language/language.helper";
import {SharedRoutesUtils} from "../../../utils/shared-routes.utils";

export default function LandingListAllPopularTagsComponent() {
    const [listTags, setListTags] = useState<{ [tag: string]: number } | undefined>(undefined);
    const [hasLoadedData, setHasLoadedData] = useState(false);

    useEffect(() => {
        if (!listTags && !hasLoadedData) {
            LocalStorageDataManager.getLocalData({
                collection: SharedFirestoreCollectionDB.DataCatalog,
                docId: SharedFirestoreDocsDB.DataCatalog.PopularTags
            })
                .then((data) => {
                    setListTags(data);
                }).finally(() => {
                setHasLoadedData(true);
            });
        }
        return () => {
        };
    }, []);



    function renderTags() {
        let arrTags: string[] = [];

        Object.keys(listTags!).sort((tag1: string, tag2: string) => {
            return listTags![tag2] - listTags![tag1];
        }).forEach((tag) => {
            if (arrTags.length < 10) {
                arrTags.push(tag);
            }
        })

        return arrTags.map((tag) => {
            return (
                <li className='flex flex-col bordered"' key={tag}>
                    <Link className='flex flex-col items-start' href={SharedRoutesUtils.getTaggedPostsPageURL(tag)}>
                        <span className='custom text-start'>#{tag}</span>
                    </Link>
                </li>
            )
        });
    }

    if (listTags && Object.keys(listTags).length >= 3) {
        return (
            <div className='flex flex-col gap-2'>
                <h3 className='custom'>{getLanguageLabel("LANDING_PAGE_POPULAR_TAGS_TITLE")}</h3>
                <ul className="list-item-bg drop-shadow menu bg-base-100 w-72">
                    {renderTags()}
                </ul>
            </div>
        )
    } else {
        return (<></>);
    }
}