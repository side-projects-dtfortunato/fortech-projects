import {useAuthState} from "react-firebase-hooks/auth";
import {auth} from "../../../logic/shared-data/firebase.utils";
import AuthManager from "../../../logic/auth/auth.manager";
import LandingLoginSignupComponent from "./LandingLoginSignup.component";
import {useCentralUserDataState} from "../../../logic/global-hooks/root-global-state";
import {getLanguageLabel} from "../../../logic/language/language.helper";
import UserPictureAvatarComponent from "../../shared/UserPictureAvatar.component";
import Link from "next/link";
import {UIHelper} from "../../../../ui-helper/UIHelper";
import {SharedRoutesUtils} from "../../../utils/shared-routes.utils";
import {PostTypes} from "../../../utils/shared-ui-helper";

export default function LandingPageSubmitPostComponent() {
    const [currentUser, isAuthLoading] = useAuthState(auth);
    const [centralUserData, setCentralUserData] = useCentralUserDataState();

    function renderPostTypeLink(postType: PostTypes) {
        return (
            <Link key={postType} href={SharedRoutesUtils.getPublishPostPageUrl(postType)} className='btn btn-xs btn-outline carousel-item'>
                {UIHelper.getPostFormTypeLabel(postType)}
            </Link>
        )
    }
    function renderPostTypes() {
        let postTypesList: any[] = [];

        UIHelper.getLandingPagePostTypes().forEach((postType) => {
            postTypesList.push(renderPostTypeLink(postType));
        });

        return (
            <div className='flex flex-col gap-2'>
                <div className='flex flex-row gap-2 items-start'>
                    <UserPictureAvatarComponent width={"w-8"} pictureUrl={centralUserData?.publicProfile?.userPicture?.fileUrl} />
                    <div className='flex flex-col items-start w-full gap-2'>
                        <Link href={SharedRoutesUtils.getPublishPostPageUrl(PostTypes.ARTICLE_BLOCKS)} className='flex w-full cursor-pointer'>
                            <span className='input input-bordered w-full p-2 text-slate-400'>What do you want to share?</span>
                        </Link>
                    </div>
                </div>
                <div className='flex flex-row carousel gap-2'>{postTypesList}</div>
            </div>
        )
    }

    if (isAuthLoading && !AuthManager.isUserLogged() && !centralUserData) {
        return (<></>);
    } else if (!AuthManager.isUserLogged() && !centralUserData) {
        return (
            <div>
                <LandingLoginSignupComponent />
            </div>
        )
    } else {
        return (
            <div className='list-item-bg drop-shadow p-5 flex flex-col gap-2'>
                <h3 className='custom'>{getLanguageLabel("LANDING_PAGE_SUBMIT_POST_TITLE")}</h3>
                {renderPostTypes()}
            </div>
        )
    }
}