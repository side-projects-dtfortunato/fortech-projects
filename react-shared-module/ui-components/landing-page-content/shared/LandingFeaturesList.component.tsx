import LandingPageSecondaryTitleComponent from "../../saas-landing-page/shared/landing-page-secondary-title.component";

export interface LandingFeatureListItemModel {
    icon: any,
    color: string, // red/green/yellow/slate
    title: string,
    description: any,
}

// Inspired by: https://shuffle.dev/editor?project=11a4d66fc6c298aee200a659ef4182e9fb15dc07&tutorial=1
export default function LandingFeaturesListComponent(props: { listFeatures: LandingFeatureListItemModel[] }) {

    function renderFeatureItemV2(listItem: LandingFeatureListItemModel) {
        return (
            <div className="w-full md:w-1/2 lg:w-1/3 px-3 mb-6">
                <div className="text-center px-1">
                    <span className={"flex items-center justify-center mx-auto mb-8 md:mb-12 w-16 h-16 rounded-full text-3xl " + ("bg-" + listItem.color + "-100 text-" + listItem.color + "-600")}>
                        {listItem.icon}
                    </span>
                    <div>
                        <h3 className="mb-4 text-2xl font-semibold font-heading">{listItem.title}</h3>
                        <p className="text-base text-gray-500">{listItem.description}</p>
                    </div>
                </div>
            </div>
        )
    }

    return (
        <section className="py-5">
            <LandingPageSecondaryTitleComponent
                subtitle={"Here's the list of our main feature available in our platform."}>Feature</LandingPageSecondaryTitleComponent>

            <div className="container mx-auto px-4 mt-10">
                <div className="flex flex-wrap justify-center -mx-3 -mb-6">
                    {props.listFeatures.map((listItem) => renderFeatureItemV2(listItem))}
                </div>
            </div>
        </section>
    )
}