import LandingPageSecondaryTitleComponent from "../../saas-landing-page/shared/landing-page-secondary-title.component";

export interface LandingHowItWorksStepModel {
    icon: any,
    title: string,
    description: string,
}

// Inspired by: https://shuffle.dev/editor?project=ce0c4fdca884f941abfd727eddfc285fb499a3ed&tutorial=1
export default function LandingHowItWorksMinimalistComponent(props: {title: string, subtitle: string, steps: LandingHowItWorksStepModel[], videoUrl?: string}) {


    function renderStep(stepItem: LandingHowItWorksStepModel) {

        return (
            <div className="w-full md:w-1/2 p-8">
                <div className="flex flex-wrap -m-7">
                    <div className="w-auto p-7">
                        <div className="relative w-11 h-11 border border-blueGray-200 rounded-full">
                            <div
                                className="absolute left-1/2 top-1/2 transform -translate-x-1/2 -translate-y-1/2 text-red-500 text-2xl">
                                {stepItem.icon}
                            </div>
                        </div>
                        <div className="w-px h-28 bg-blueGray-200 mx-auto"></div>
                    </div>
                    <div className="flex-1 p-7">
                        <div className="md:max-w-sm pb-8">
                            <h3 className="mb-4 text-xl font-semibold leading-normal">{stepItem.title}</h3>
                            <p className="text-gray-600 font-medium leading-relaxed">{stepItem.description}</p>
                        </div>
                    </div>
                </div>
            </div>
        )
    }

    function renderVideo() {
        if (!props.videoUrl) {
            return <></>
        }

        return (
            <div className='flex w-full p-4 justify-center my-10'>
                <div className='p-4 bg-slate-200 rounded-xl border shadow-xl w-full sm:w-3/5 ' dangerouslySetInnerHTML={{__html: props.videoUrl}} />
            </div>
        )
    }

    return (
        <section className="py-5 pt-10 bg-white overflow-hidden">
            <div className="container px-4 mx-auto">
                <LandingPageSecondaryTitleComponent subtitle={props.subtitle}>{props.title}</LandingPageSecondaryTitleComponent>
                {renderVideo()}
                <div className="flex flex-wrap justify-center items-start">
                    {props.steps.map((stepItem) => renderStep(stepItem))}
                </div>
            </div>
        </section>
    )
}