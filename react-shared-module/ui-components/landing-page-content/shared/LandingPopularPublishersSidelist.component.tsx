import {UserShortProfileModel} from "../../../logic/shared-data/datamodel/shared-public-userprofile.model";
import UserHeaderInfosComponent from "../../shared/UserHeaderInfos.component";

export default function LandingPopularPublishersSidelistComponent(props: {listUsers: UserShortProfileModel[]}) {


    function renderPostItems() {
        return props.listUsers.map((user) => {
            return (
                <li className='flex flex-col bordered"' key={user.uid}>
                    <UserHeaderInfosComponent userId={user.uid!} userInfos={user} />
                </li>
            )
        });
    }

    return (
        <ul className="list-item-bg drop-shadow menu bg-base-100 w-72">
            <li><h3 className='custom'>Popular publishers</h3></li>
            {renderPostItems()}
        </ul>
    )
}