import {
    SharedLandingPageAggregatorsModel
} from "../../logic/shared-data/datamodel/shared-landing-page-aggregators.model";
import {BasePostDataModel} from "../../logic/shared-data/datamodel/base-post-data.model";
import LandingTagPopularSidelistComponent from "./shared/LandingTagPopularSidelist.component";
import CustomRootLayoutComponent from "../../../app-components/root/CustomRootLayout.component";
import LandingPopularPublishersSidelistComponent from "./shared/LandingPopularPublishersSidelist.component";
import {UserShortProfileModel} from "../../logic/shared-data/datamodel/shared-public-userprofile.model";
import LandingListPostsContentComponent from "./shared/LandingListPostsContent.component";
import LandingPageSubmitPostComponent from "./shared/LandingPageSubmitPost.component";
import {getLanguageLabel} from "../../logic/language/language.helper";
import RootConfigs from "../../../../configs";
import {SharedUtils} from "../../utils/shared-utils";


export default function LandingPageContentComponent(props: { landingDataAgg: SharedLandingPageAggregatorsModel, headerComponent?: any }) {

    function renderPopularTags() {
        let listTags: {
            rank: number,
            tag: string,
            posts: BasePostDataModel[],
        }[] = Object.values(props.landingDataAgg.popularTagsPosts).sort((t1, t2) => {
            return t2.rank - t1.rank;
        });

        return listTags.map((tagAgg) => {
            return (
                <LandingTagPopularSidelistComponent key={tagAgg.tag} tag={tagAgg.tag} posts={tagAgg.posts} />
            )
        });
    }




    function renderRightSideContent() {

        let listPopularPublishers: UserShortProfileModel[] = [];
        if (props.landingDataAgg.popularWriters) {
            Object.values(props.landingDataAgg.popularWriters).sort((u1, u2) => {
                return u2.rank - u1.rank;
            }).forEach((userData) => {
                if (listPopularPublishers.length < 10) {
                    listPopularPublishers.push(userData.userInfos);
                }
            });
        }

        return (
            <div className='flex flex-col gap-3' key={"rightSideMenu"}>
                {listPopularPublishers && listPopularPublishers.length > 0 ? <LandingPopularPublishersSidelistComponent key={"popularPublishers"} listUsers={listPopularPublishers} /> : <></>}
                <h2 className='custom pt-10'>Trends topics</h2>
                {renderPopularTags()}
            </div>
        )
    }

    function renderHeader() {
        if (props.headerComponent) {
            return (
                <div className='flex mb-5' key={"Header Title"}>
                    {props.headerComponent}
                </div>
            );
        } else {
            return (<></>);
        }
    }

    return (
        <CustomRootLayoutComponent rigthChilds={renderRightSideContent()} pageHeadProps={{
            title: RootConfigs.SITE_TITLE,
            description: RootConfigs.SITE_DESCRIPTION,
            imgUrl: RootConfigs.BASE_URL + "/images/logo.png",
            jsonLdMarkup: SharedUtils.getSchemaMarkupSite(),
        }}>
            <div className='flex flex-col gap-3'>
                {renderHeader()}
                <LandingPageSubmitPostComponent />
                <div className='divider text-slate-400'>{getLanguageLabel("LANDING_PAGE_DIVIDER_PUBLICATIONS", "Publications")}</div>
                <LandingListPostsContentComponent landingDataAgg={props.landingDataAgg} />
            </div>
        </CustomRootLayoutComponent>
    )
}
