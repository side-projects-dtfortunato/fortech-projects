import {BasePostDataModel, PushPostDataModel} from "../../logic/shared-data/datamodel/base-post-data.model";
import {useRouter} from "next/router";
import {useState} from "react";
import {PostTypes} from "../../utils/shared-ui-helper";
import {useAlertMessageGlobalState, useIsLoadingGlobalState} from "../../logic/global-hooks/root-global-state";
import {ApiResponse} from "../../logic/shared-data/datamodel/base.model";
import {SharedBackendApi} from "../../logic/shared-data/sharedbackend.api";
import {SharedRoutesUtils} from "../../utils/shared-routes.utils";
import {APIResponseMessageUtils} from "../../logic/global-hooks/APIResponseMessageUtils";
import {CATEGORIES_OPTIONS, UIHelper} from "../../../ui-helper/UIHelper";
import PostFormWriteArticleComponent from "../post-form/PostFormWriteArticle.component";
import PostFormArticleBlocksComponent from "../post-form/PostFormArticleBlocks.component";

export default function EditPostPageComponent(props: {postData: BasePostDataModel}) {
    const router = useRouter();
    const [postTypeSelection, setPostTypeSelection] = useState<PostTypes>(props.postData.postType as PostTypes);
    const [alertMessageState, setAlertMessageState] = useAlertMessageGlobalState();
    const [isLoadingState, setIsLoadingState] = useIsLoadingGlobalState();
    const [thumbnailToUploadData, setThumbnailToUploadData] = useState<string|undefined>();

    async function onSubmitClicked(postFormData: PushPostDataModel) {
        // API Call
        const apiResponse: ApiResponse<any> = await SharedBackendApi.pushPostData({
            postData: postFormData,
            isCreation: false,
            thumbnailBase: thumbnailToUploadData,
        });

        if (apiResponse && apiResponse.isSuccess) {
            // Redirect user
            await router.push(SharedRoutesUtils.getPostDetailsPageURL(postFormData.uid!));
        } else {
            // Handle errors
            setAlertMessageState(
                {
                    alertType: "ERROR",
                    message: APIResponseMessageUtils.getAPIResponseMessage(apiResponse),
                    display: true,
                }
            );
        }
    }

    function onTabSelected(tabPostType: PostTypes) {
        setPostTypeSelection(tabPostType);
    }

    function renderTab(tabPostType: PostTypes) {
        let tabLabel: string = UIHelper.getPostFormTypeLabel(tabPostType);

        let suffixClass: string = "";
        if (tabPostType === postTypeSelection) {
            suffixClass = "tab-active"
        }
        return (
            <button className={"tab tab-lifted " + suffixClass} onClick={() => onTabSelected(tabPostType)}>{tabLabel}</button>
        )
    }

    function onThumbnailToUpload(thumbnailBase64: string) {
        // Upload in the moment of creating the post
        setThumbnailToUploadData(thumbnailBase64);
    }

    function renderFormContainer() {
        switch (postTypeSelection) {
            case PostTypes.WRITE_ARTICLE:
                return (<PostFormWriteArticleComponent postType={postTypeSelection} onSubmitClicked={onSubmitClicked}
                                                       defaultArticleData={props.postData}
                                                       onThumbnailBase64={onThumbnailToUpload}
                                                       listCategories={CATEGORIES_OPTIONS} isEditing={true}/>);
            case PostTypes.ARTICLE_BLOCKS:
                return (<PostFormArticleBlocksComponent postType={postTypeSelection} defaultArticleData={props.postData}
                                                        onSubmitClicked={onSubmitClicked}
                                                        listCategories={CATEGORIES_OPTIONS} isEditing={true}/>);
        }
        return (<></>);
    }

    return (
        <div className='flex flex-col'>
            <div className='list-item-bg drop-shadow px-4 py-3'>
                {renderFormContainer()}
            </div>
        </div>

    )
}