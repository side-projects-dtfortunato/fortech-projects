import {useState} from "react";

export interface PricingShortenPlansTableModel {
    id: string,
    title: string,
    description: string,
    isRecommended: boolean,
    monthlyPrice: {
        label: string,
        btnComponent: any,
    },
    yearlyPrice: {
        label: string,
        btnComponent: any,
    },
    icon?: any,
}


// Inspired by:
// - https://shuffle.dev/editor?project=6a63f295c4dd836f12dbf927abe62fd1a89ce344&tutorial=1
export default function PricingShortenTableComponent(props: { pricePlans: PricingShortenPlansTableModel[], yearlySave: string, hideTitle?: boolean }) {
    const [isAnnualBilled, setIsAnnualBilled] = useState(true);

    function renderToggle() {
        return (
            <div className='flex w-full justify-center'>
                <div className="w-auto p-4">
                    <div className="flex gap-2 inline-flex items-center max-w-max">
                        <p className="text-gray-600 font-medium leading-relaxed">Monthly</p>
                        <div className='form-control'>
                            <input type="checkbox" className="toggle toggle-primary" checked={isAnnualBilled}
                                   onClick={() => setIsAnnualBilled(!isAnnualBilled)}/>
                        </div>
                        <p className="text-gray-600 font-medium leading-relaxed">
                            <span>Yearly </span>
                            <span className="text-indigo-600 font-semibold">({props.yearlySave})</span>
                        </p>
                    </div>
                </div>
            </div>
        )
    }

    function renderPricingTable(pricingTable: PricingShortenPlansTableModel) {
        return (
            <div className="w-full md:w-1/3 p-4">
                <div
                    className={"flex flex-col px-9 pt-8 pb-9 h-full bg-white bg-opacity-90 border-blueGray-100 rounded-4xl " + (pricingTable.isRecommended ? "drop-shadow-xl" : "")}
                    style={{backdropFilter: "blur(46px)"}}>

                    {pricingTable.isRecommended ? (<><img className="absolute -top-11 -right-8"
                                                          src="/images/ic_pricing_popular.png"
                                                          alt=""/></>) : (<></>)}

                    <div className='flex flex-col'>
                        {
                            pricingTable.icon ? (
                                <div className={"flex max-h-20 mb-4 w-full justify-center"}>
                                    {pricingTable.icon}
                                </div>
                            ) : <></>
                        }
                                    <span
                                        className="mb-9 inline-block text-sm text-indigo-600 font-semibold uppercase tracking-px leading-snug">{pricingTable.title}</span>
                        <h3 className="mb-1 text-4xl text-gray-900 font-bold leading-none">{isAnnualBilled ? pricingTable.yearlyPrice.label : pricingTable.monthlyPrice.label}</h3>
                        <p className="mb-6 text-sm text-gray-600 font-medium leading-relaxed">{isAnnualBilled ? "Billed Annually" : "Billed Monthly"}</p>
                        <p className="mb-9 text-gray-600 font-medium leading-relaxed">{pricingTable.description}</p>
                    </div>
                    {isAnnualBilled ? pricingTable.yearlyPrice.btnComponent : pricingTable.monthlyPrice.btnComponent}
                </div>
            </div>
        )
    }

    return (
        <section className="pt-5 pb-5 bg-blueGray-50 overflow-hidden">
            <div className="flex flex-col items-center gap-10 container px-4 mx-auto">
                {props.hideTitle ? <></> :
                    <h2 className="mb-5 text-6xl md:text-7xl font-bold font-heading text-center tracking-px-n leading-tight md:max-w-xl mx-auto">Pricing
                        Plans</h2>}

                {renderToggle()}

                <div className="mb-5 md:max-w-6xl mx-auto">
                    <div className="flex flex-wrap -m-4">
                        {props.pricePlans.map((pricingPlan) => renderPricingTable(pricingPlan))}

                    </div>
                </div>
            </div>
        </section>
    )
}