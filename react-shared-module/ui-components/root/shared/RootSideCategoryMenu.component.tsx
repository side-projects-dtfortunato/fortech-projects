import Link from "next/link";
import {CATEGORIES_OPTIONS} from "../../../../ui-helper/UIHelper";
import {FormOption} from "../../form/RadioButton.component";
import {SharedRoutesUtils} from "../../../utils/shared-routes.utils";

export default function RootSideCategoryMenuComponent() {
    function renderMenuItems() {
        return Object.keys(CATEGORIES_OPTIONS).filter((option) => CATEGORIES_OPTIONS[option].displayOption || CATEGORIES_OPTIONS[option].displayOption === undefined).map((catId) => {
            let categoryItem: FormOption = CATEGORIES_OPTIONS[catId];

            return (
                <li className='flex flex-col bordered"' key={catId}>
                    <Link className='flex flex-col items-start' href={SharedRoutesUtils.getCategoryPostsPageURL(catId)}>
                        <span className='custom text-start'>{categoryItem.label}</span>
                    </Link>
                </li>
            )
        });
    }

    return (
        <div className='flex flex-col gap-2'>
            <h3 className='custom px-2'>Categories</h3>
            <ul className="list-item-bg drop-shadow menu bg-base-100 w-72">
                {renderMenuItems()}
            </ul>
        </div>
    )
}