
export default function AdsBlockerDetectorComponent() {

    return (
        <div dangerouslySetInnerHTML={{__html: `<script async src="https://fundingchoicesmessages.google.com/i/pub-5492791201497132?ers=1" nonce="9W9Uv2j_2MAosTgXULHS4Q"></script><script nonce="9W9Uv2j_2MAosTgXULHS4Q">(function() {function signalGooglefcPresent() {if (!window.frames['googlefcPresent']) {if (document.body) {const iframe = document.createElement('iframe'); iframe.style = 'width: 0; height: 0; border: none; z-index: -1000; left: -1000px; top: -1000px;'; iframe.style.display = 'none'; iframe.name = 'googlefcPresent'; document.body.appendChild(iframe);} else {setTimeout(signalGooglefcPresent, 0);}}}signalGooglefcPresent();})();</script>`}}></div>
    )
}