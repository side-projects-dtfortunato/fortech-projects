import Head from 'next/head'
import {useRouter} from "next/router";
import FooterComponent, {FooterLinkItem} from "./Footer.component";
import NavbarComponent from "./Navbar.component";
import RootConfigs from "../../../../configs";
import SpinnerComponent from "../shared/Spinner.component";
import React, {useEffect, useState} from "react";
import AlertMessageComponent from "../shared/AlertMessage.component";
import {
    AlertType,
    useAlertMessageGlobalState,
    useCentralUserDataState,
    useSubscribedNewsletterState
} from "../../logic/global-hooks/root-global-state";
import AuthManager from "../../logic/auth/auth.manager";
import {useAuthState} from "react-firebase-hooks/auth";
import {auth} from "../../logic/shared-data/firebase.utils";
import CookiesBannerConsentComponent from "../shared/CookiesBannerConsent.component";
import {SharedBackendApi} from "../../logic/shared-data/sharedbackend.api";
import LocalStorageDataManager from "../../logic/managers/LocalStorageData.manager";
import AdsBlockerDetectorComponent from "./shared/AdsBlockerDetector.component";
import OneSignal from "react-onesignal";
import UserProfileUpdatePopupComponent from "../edit-profile/shared/UserProfileUpdagePopup.component";
import {SharedUtils} from "../../utils/shared-utils";

export interface PageHeadProps {
    title: string;
    description: string;
    imgUrl?: string;
    publishedTime?: string;
    jsonLdMarkup?: any; // https://developers.google.com/search/docs/appearance/structured-data/article?hl=pt_PT#json-ld,
    canonicalUrl?: string;
}

export default function RootLayoutComponent(props: { pageHeadProps?: PageHeadProps, children: any, customBody?: boolean,
    listFooterLinks?: FooterLinkItem[],
    isIndexable?: boolean, isLoading?: boolean, customNavbarItems?: any,
    leftChilds?: any, rigthChilds?: any, backgroundColor?: string, headerBanner?: any, pageRssFeedUrl?: string, backPath?: string}) {
    const router = useRouter();

    const [isOneSignalInit, setOneSignalInit] = useState(LocalStorageDataManager.getItem("isOneSignalInit"));
    let alertTimeoutOngoing: NodeJS.Timeout;
    const [currentUser, isAuthLoading] = useAuthState(auth);
    const [alertMessageState, setAlertMessageState] = useAlertMessageGlobalState();
    const [centralUserDataState, setCentralUserState] = useCentralUserDataState();
    const [hasNewsletterSubscribed, setSubscribedNewsletter] = useSubscribedNewsletterState();

    /*if (!centralUserDataState && AuthManager.getUserId()) {
        SharedBackendApi.getCentralUserData({userId: AuthManager.getUserId()!}).then((response) => {
            setCentralUserState(response);
        });
    }*/


    useEffect(() => {

        if (!hasNewsletterSubscribed) {
            let localHasSubscribedNewsletter = LocalStorageDataManager.getItem("subscribedNewsletter");
            setSubscribedNewsletter(localHasSubscribedNewsletter ? localHasSubscribedNewsletter : false);
        }

        if (!centralUserDataState && AuthManager.getUserId()) {
            SharedBackendApi.getCentralUserData({userId: AuthManager.getUserId()!}).then(async (response) => {
                setCentralUserState(response);
            });

            OneSignal.login(AuthManager.getUserId()!);
        }


        // Extract oneSignalUserId from URL query parameters
        const oneSignalUserId: string | undefined = router.query.oneSignalUserId as string;
        if (oneSignalUserId) {
            AuthManager.setOneSignalUserId(oneSignalUserId);
        }

        // Send currentUserId to the mobile app
        SharedUtils.sendUserIdToFlutter(AuthManager.getUserId());

        // If doesn't have yet the centralUserData and has the userId try it again in 2 seconds
        let timeout: number = ((!centralUserDataState || !isAuthLoading) && AuthManager.getUserId()) ? 2000 : 30000;

        // Timer get Central User Data each minute
        let timer = setTimeout(function refreshCentralUserData() {
            if (AuthManager.getUserId()) {
                SharedBackendApi.getCentralUserData({userId: AuthManager.getUserId()!}).then((response) => {
                    setCentralUserState(response);
                    timer = setTimeout(refreshCentralUserData, timeout);
                });
            }
        }, 30000);
        // const unsubscribeCentralListener = SharedBackendApi.listCentralUserDataChanges({setCentralUserData: setCentralUserState});

        if (alertMessageState && alertMessageState.display && !alertTimeoutOngoing) {
            alertTimeoutOngoing = setTimeout(() => {
                setAlertMessageState({});
            }, 3000);
        }

        async function initOneSignal() {
            if (RootConfigs.ONE_SIGNAL_API && !isOneSignalInit) {
                setOneSignalInit(true);
                LocalStorageDataManager.saveItem("isOneSignalInit", true);
                await OneSignal.init({
                    appId: RootConfigs.ONE_SIGNAL_API});
                OneSignal.Slidedown.promptPush();
            }
        }

        // Init OneSignal
        initOneSignal();

        return () => {
            if (timer) {
                clearTimeout(timer);
            }

            // Save SubscribedNewsletter
            LocalStorageDataManager.saveItem("subscribedNewsletter", hasNewsletterSubscribed);
        }
    });

    function genSocialMeta(pageHead: PageHeadProps) {

        let imageUrl: string | undefined = pageHead.imgUrl;
        if (!imageUrl) {
            imageUrl = RootConfigs.BASE_URL + "/images/ic_logo.png";
        }
        let url = RootConfigs.BASE_URL + router.asPath;

        return (
            <>
                {pageHead.publishedTime ? <meta property="article:published_time" content={pageHead.publishedTime} /> : ""}
                <meta name="robots" content={props.isIndexable === false ? "noindex" : "index, follow"} />
                <meta name="twitter:card" content="summary"/>
                <meta name="twitter:url" content={url}/>
                <meta name="twitter:title" content={pageHead.title}/>
                <meta name="twitter:description" content={pageHead.description}/>
                <meta name="twitter:image" content={imageUrl}/>
                <meta name="twitter:creator" content={RootConfigs.SITE_NAME}/>
                <meta property="og:type" content="website"/>
                <meta property="og:title" content={pageHead.title}/>
                <meta property="og:description" content={pageHead.description}/>
                <meta property="og:site_name" content={RootConfigs.SITE_NAME}/>
                <meta property="og:url" content={url}/>
                <meta property="og:image" content={imageUrl}/>

                {
                    pageHead.canonicalUrl ? <link rel="canonical" href={pageHead.canonicalUrl} /> : <></>
                }

                {
                    props.pageRssFeedUrl ?
                        <link rel="alternate" type="application/rss+xml" href={props.pageRssFeedUrl} /> : <></>
                }


            </>
        )
    }

    const pageHeadProps: PageHeadProps = props.pageHeadProps ? props.pageHeadProps : {
        title: RootConfigs.SITE_TITLE,
        description: RootConfigs.SITE_DESCRIPTION,
        imgUrl: RootConfigs.BASE_URL + "/images/logo-white-bg.jpg"
    };

    function genHead() {
        return pageHeadProps ? (
            <Head>
                <title>{pageHeadProps.title}</title>
                <meta name="description" content={pageHeadProps.description}/>
                {genSocialMeta(pageHeadProps)}
                {props.pageHeadProps?.jsonLdMarkup ? props.pageHeadProps.jsonLdMarkup : <></>}
            </Head>
        ) : (<></>);
    }

    function renderLoadingScreen() {
        return (
            <div className='flex h-screen justify-content-center align-items-center'>
                <SpinnerComponent />
            </div>
        )
    }

    function renderBodyContent() {
        if (props.customBody) {
            return props.children;
        } else {
            return (
                <>
                    {
                        props.leftChilds ? (
                            <div className='hidden lg:flex flex-col justify-center p-1 h-full sticky top-0 min-w-fit max-w-xs'>
                                {props.leftChilds ? props.leftChilds : (<></>)}
                            </div>
                        ): (<></>)
                    }
                    <div className={'mx-0 lg:mx-2 w-full grow max-w-4xl justify-center items-center'}>
                        {props.isLoading ? renderLoadingScreen() : props.children}
                    </div>

                    {
                        props.rigthChilds ? (
                            <div className='hidden lg:flex justify-center p-1 h-full sticky top-0' style={{width: 300}}>
                                {props.rigthChilds}
                            </div>
                        ) : (<></>)
                    }

                </>
            )
        }
    }

    function renderAlertMessage() {
        if (alertMessageState && alertMessageState.display) {
            return (
                <div className='p-2 max-w-sm flex justify-center'>
                    <AlertMessageComponent type={alertMessageState.alertType as AlertType} messageText={alertMessageState.message!} />
                </div>
            )
        } else {
            return (
                <></>
            )
        }
    }

    return (
        <div className={'flex flex-col h-full w-screen min-h-screen ' + (props.backgroundColor ? props.backgroundColor : " root-bg")} data-theme="mytheme">
            {genHead()}
            {/*Popup to force the user to update his profile if not valid yet*/}
            <UserProfileUpdatePopupComponent />

            <AdsBlockerDetectorComponent />
            <CookiesBannerConsentComponent />
            <header className="sticky top-0 z-50 flex flex-col justify-center items-center">
                <NavbarComponent customNavbarItems={props.customNavbarItems} backPath={props.backPath}></NavbarComponent>
                {props.headerBanner ? props.headerBanner : <></>}
                {renderAlertMessage()}
            </header>
            <main className={"flex flex-row items-start w-screen gap-3 " + (props.customBody ? "justify-center" : "justify-center p-0 sm:px-2")}>
                {renderBodyContent()}
            </main>

            <div className='w-full'>
                <FooterComponent listFooterLinks={props.listFooterLinks}/>
            </div>
        </div>
    )
}

