import Link from "next/link";
import Image from 'next/image'
import {useAuthState} from "react-firebase-hooks/auth";
import {auth} from "../../logic/shared-data/firebase.utils";
import {UIHelper} from "../../../ui-helper/UIHelper";
import {useAmp} from "next/amp";
import {useRouter} from "next/router";
import RootConfigs from "../../../../configs";
import {IoIosArrowBack} from "react-icons/io";

export interface NavbarLinkItem {linkUrl: string, label: string, onClick?: () => void}

/**
 * UI developed using daisyUI: https://daisyui.com/
 * @constructor
 */
export default function NavbarComponent(props: { customNavbarItems?: any, customLinks?: NavbarLinkItem[], backPath?: string}) {
    const isAmp = useAmp()
    const [currentUser, isAuthLoading] = useAuthState(auth);
    const router = useRouter();

    function renderNavbarItems() {
        /*if (props.customNavbarItems) {
            return (
                <div className="flex-none">
                    <ul className="menu menu-horizontal p-0 mr-2">
                        {props.customNavbarItems}
                    </ul>
                </div>
            )
        } else {
            return UIHelper.renderNavbarRightItems();
        }*/

        return UIHelper.renderNavbarRightItems();
    }

    function renderNavbarLeftItem() {
        if (props.backPath) {
            return (
                <>
                    <Link href={props.backPath} className='btn btn-ghost btn-md'><IoIosArrowBack size={"25"}/></Link>
                    <Link className={`hover:bg-${RootConfigs.THEME.PRIMARY_COLOR}-200 hover:bg-opacity-25 rounded`} href="/">
                        <Image className='flex h-12 object-contain' src={"/images/logo.png"} alt={RootConfigs.SITE_TITLE} width={200} height={50}/>
                    </Link>
                </>
            )
        } else {
            return (
                <>
                    {UIHelper.renderLeftNavbar({customLinks: props.customLinks, customNavbarItems: props.customNavbarItems})}
                    <Link className={`hover:bg-${RootConfigs.THEME.PRIMARY_COLOR}-200 hover:bg-opacity-25 rounded`} href="/">
                        <Image className='flex h-12 object-contain' src={"/images/logo.png"} alt={RootConfigs.SITE_TITLE} width={200} height={50}/>
                    </Link>
                </>
            )
        }
    }

    return (
        <div className="navbar bg-base-100 drop-shadow">
            <div className="navbar-start">
                <div className='flex flex-row grow'>
                    {renderNavbarLeftItem()}
                </div>
            </div>
            <div className={'navbar-center hidden md:flex'}>
                {renderCenterMenuNavbar(router.asPath)}
            </div>
            <div className='navbar-end w-max-xs'>
                {renderNavbarItems()}
            </div>
        </div>
    )
}

function renderCenterMenuNavbar(currentPath: string) {
    return UIHelper.renderNavbarCenterItems(currentPath);
}