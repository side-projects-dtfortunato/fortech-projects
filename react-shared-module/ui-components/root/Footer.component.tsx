import Link from "next/link";
import RootConfigs from "../../../../configs";
import Image from "next/image";
import {UIHelper} from "../../../ui-helper/UIHelper";
import {useEffect, useState} from "react";

export interface FooterLinkItem {
    categoryId: string,
    categoryLabel: string,
    label: string,
    linkUrl: string,
    iconUrl?: string,
    newTab?: boolean,
}

export default function FooterComponent(props: { listFooterLinks?: FooterLinkItem[] }) {
    const [isInWebView, setIsInWebView] = useState(false);

    useEffect(() => {
        const userAgent = navigator.userAgent.toLowerCase();

        const detectWebView = () => {
            return !!(window.ReactNativeWebView || userAgent.includes('wv') ||
                ('webkit' in window && 'messageHandlers' in (window as any).webkit));
        };

        setIsInWebView(detectWebView());
    }, []);

    let listFooterLinksData: FooterLinkItem[] = [];
    if (props.listFooterLinks) {
        listFooterLinksData = listFooterLinksData.concat(props.listFooterLinks);
    }

    listFooterLinksData = listFooterLinksData.concat(UIHelper.getFooterLinks());

    let categoriesLinks: { [categoryId: string]: FooterLinkItem[] } = getLinksByCategory(listFooterLinksData);

    function renderCategoryTitle(categoryTitle: string) {
        return (
            <span key={categoryTitle} className="footer-title">{categoryTitle}</span>
        );
    }

    function renderLinkItem(footerLinkItem: FooterLinkItem) {
        return (
            <Link key={footerLinkItem.linkUrl} href={footerLinkItem.linkUrl} className="link link-hover footer-link" target={footerLinkItem.newTab ? "_blank" : ""}>{footerLinkItem.label}</Link>
        )
    }
    function renderLinks() {
        return Object.keys(categoriesLinks).map((catId) => {
            let listItems: any [] = [];
            listItems.push(renderCategoryTitle(categoriesLinks[catId][0].categoryLabel));

            // render category links
            listItems = listItems.concat(categoriesLinks[catId].map((linkItem) => {
                return renderLinkItem(linkItem);
            }));

            return (
                <div key={catId} style={{minWidth: 100}}>
                    {listItems}
                </div>
            );
        });
    }

    function renderSocialNetworkLinks() {
        let listItems: any[] = [];

        if (RootConfigs.SOCIAL_NETWORKS && Object.keys(RootConfigs.SOCIAL_NETWORKS).length > 0) {
            listItems.push(renderCategoryTitle("Social Networks"));
            listItems = listItems.concat(Object.values(RootConfigs.SOCIAL_NETWORKS).map((socialItem) => {
                return (
                    <Link className='link link-hover flex flex-row gap-2 footer-link' href={socialItem.link} key={socialItem.link}>
                        <img className='rounded drop-shadow' src={socialItem.iconUrl} alt={socialItem.label} width={20} height={20}/>
                        {socialItem.label}
                    </Link>
                )
            }));
        }

        return (
            <div key={"Social Networks"} style={{minWidth: 100}}>
                {listItems}
            </div>
        );
    }

    if (!isInWebView) {
        return (
            <div className={`bg-slate-50 flex flex-col justify-center items-stretch p-4 sm:px-10 footer-background`}>
                <footer className="footer flex flex-col items-center justify-center gap-5">
                    <Link href={RootConfigs.BASE_URL} ><Image src={"/images/logo.png"} alt={RootConfigs.SITE_NAME} height={75} width={130} /></Link>
                    <span className='footer-link text-center'>{RootConfigs.SITE_DESCRIPTION}</span>
                </footer>
                <footer className="footer flex flex-wrap justify-evenly gap-5 p-10 text-slate-400">
                    {renderLinks()}
                    {renderSocialNetworkLinks()}
                </footer>
            </div>
        )
    } else {
        return (
            <div className='w-full h-20'/>
        )
    }

}

function getLinksByCategory(listFooterLinks: FooterLinkItem[]): { [categoryId: string]: FooterLinkItem[] } {
    // Organize by categories
    let categoriesLinks: { [categoryId: string]: FooterLinkItem[] } = {};

    if (listFooterLinks) {
        listFooterLinks.forEach((footerItemLink) => {
            if (!categoriesLinks[footerItemLink.categoryId]) categoriesLinks[footerItemLink.categoryId] = [];
            categoriesLinks[footerItemLink.categoryId].push(footerItemLink);
        })
    }

    return categoriesLinks;
}