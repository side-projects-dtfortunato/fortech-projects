// This is a React component that converts the blockData of EditorJS into a Blog Post content
import React from "react";
import {SharedEditorBlockModel} from "../../../logic/shared-data/datamodel/shared-editor-block.model";
import CommonUtils from "../../../logic/commonutils";

// A function to render a header block
const HeaderBlock = ({ data }: { data: any }) => {
    const { text, level } = data;

    let className: string = "text-xl lg:text-4xl";
    switch (level) {
        case 1: className = "text-3xl lg:text-5xl font-bold my-16"; break;
        case 2: className = "text-xl lg:text-4xl font-bold my-6"; break;
        case 3: className = "text-lg lg:text-3xl font-bold my-5"; break;
        case 4: className = "text-lg lg:text-2xl mb-y"; break;
        case 5: className = "text-base lg:text-xl font-bold my-3"; break;
        case 6: className = "text-base lg:text-xl my-2"; break;
    }

    const Tag = `h${level}` as keyof JSX.IntrinsicElements;
    return <Tag className={"text-gray-900 " + className} id={CommonUtils.string_to_slug(text)}>{text}</Tag>;
};

// A function to render a paragraph block
const ParagraphBlock = ({ data }: { data: any }) => {
    const { text } = data;
    return <div className={'font-medium text-lg mb-2'} dangerouslySetInnerHTML={{__html: text}}/>;
};

// A function to render a list block
const ListBlock = ({ data }: { data: any }) => {
    const { style, items } = data;
    const Tag = style === "ordered" ? "ol" : "ul";
    return (
        <Tag className='mb-8'>
            {items.map((item: string, index: number) => (
                <li className='text-gray-700 text-lg font-medium' key={index}><div dangerouslySetInnerHTML={{__html: item}} /> </li>
            ))}
        </Tag>
    );
};

// A function to render an image block
const ImageBlock = ({ data }: { data: any }) => {
    const { file, caption, withBorder, stretched, withBackground } = data;
    const { url } = file;
    return (
        <div
            className={`mb-5 mx-8 my-2 drop-shadow-md image-block ${
                withBorder ? "border" : ""
            } ${stretched ? "stretched" : ""} ${withBackground ? "background" : ""}`}
        >
            <img className='w-full rounded-2xl drop-shadow max-h-96 object-cover my-4' src={url} alt={caption} />
            {caption && <span className="caption text-xs text-slate-800 text-center">{caption}</span>}
        </div>
    );
};

// A function to render a delimiter block
const DelimiterBlock = () => {
    return <hr />;
};

// A function to render an embed block
const EmbedBlock = ({ data }: { data: any }) => {
    const { service, source, embed, width, height, caption } = data;
    return (
        <div className="embed-block">
            <iframe
                src={embed}
                width={width}
                height={height}
                frameBorder="0"
                allowFullScreen
            ></iframe>
            {caption && <span className="caption">{caption}</span>}
        </div>
    );
};

// A function to render a block based on its type
const renderBlock = (block: any) => {
    const { type, data } = block;
    switch (type) {
        case "header":
            return <HeaderBlock data={data} />;
        case "paragraph":
            return <ParagraphBlock data={data} />;
        case "list":
            return <ListBlock data={data} />;
        case "image":
            return <ImageBlock data={data} />;
        case "delimiter":
            return <DelimiterBlock />;
        case "embed":
            return <EmbedBlock data={data} />;
        default:
            return null;
    }
};

// The main component
export default function ArticleBlocksViewerComponent(props: {blocks: SharedEditorBlockModel[]}) {
    return (
        <article className="prose max-w-none blog-post text-gray-700">
            {props.blocks.map((block: any, index: number) => (
                <div key={index} className="block">
                    {renderBlock(block)}
                </div>
            ))}
        </article>
    );
}