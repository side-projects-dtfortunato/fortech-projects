import {BasePostDataModel} from "../../../logic/shared-data/datamodel/base-post-data.model";
import BlogPostListItemComponent from "./blog-post-list-item.component";
import Link from "next/link";
import {SharedRoutesUtils} from "../../../utils/shared-routes.utils";
import LandingPageSecondaryTitleComponent from "../../saas-landing-page/shared/landing-page-secondary-title.component";

export default function BlogLatestPostsContainerComponent(props: { listPosts: BasePostDataModel[], seeMoreArticle: boolean }) {
    let listPostsSorted: BasePostDataModel[] = props.listPosts.sort((p1, p2) => p2.updatedAt - p1.updatedAt);

    function renderTitle() {
        if (props.seeMoreArticle) {
            return (
                <LandingPageSecondaryTitleComponent>Latest Blog Articles</LandingPageSecondaryTitleComponent>);
        } else {
            return (
                <div className='flex w-full justify-center'>
                    <h1 className="font-heading text-5xl xs:text-6xl md:text-7xl font-bold">
                        <span>Latest Blog Articles</span>
                    </h1>
                </div>
                );
        }
    }

    function renderSideItems() {
        let listItems: any[] = listPostsSorted.slice(1, 4).map((post) => (
            <BlogPostListItemComponent key={post.uid!} postData={post} type={"highlight_small"}/>));

        if (listItems.length > 0) {
            return (
                <div className="w-full lg:w-1/2 px-4">
                    {listItems}
                </div>
            )
        } else {
            return <></>;
        }
    }

    function renderSeeMoreArticles() {
        if (!props.seeMoreArticle) {
            return <></>;
        }

        return (
            <div className="text-center mt-10">
                <Link
                    className="relative group inline-block py-4 px-7 font-semibold text-orange-900 hover:text-orange-50 rounded-full bg-orange-50 transition duration-300 overflow-hidden"
                    href={SharedRoutesUtils.getBlogPageUrl()}>
                    <div
                        className="absolute top-0 right-full w-full h-full bg-gray-900 transform group-hover:translate-x-full group-hover:scale-102 transition duration-500"></div>
                    <span className="relative">See More Articles</span>
                </Link>
            </div>
        )
    }

    if (props.listPosts.length === 0) {
        return <></>;
    }
    return (
        <section className="relative py-5 overflow-hidden">
            {renderTitle()}
            <div className="relative container px-4 mx-auto">
                <div className="max-w-xl lg:max-w-7xl mx-auto">
                    <div className="max-w-2xl mx-auto mt-5 mb-16 text-center">
                        <span
                            className="inline-block py-1 px-3 mb-4 text-xs font-semibold text-red-900 bg-red-50 rounded-full">OUR BLOG</span>
                    </div>

                    <div className="flex flex-wrap -mx-4 mb-18">
                        <BlogPostListItemComponent postData={listPostsSorted[0]} type={"highlight"}/>
                        {renderSideItems()}
                    </div>

                    {renderSeeMoreArticles()}
                </div>
            </div>
        </section>
    )
}