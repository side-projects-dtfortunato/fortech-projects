import {BasePostDataModel} from "../../../logic/shared-data/datamodel/base-post-data.model";
import dateFormat from "dateformat";
import Link from "next/link";
import {SharedRoutesUtils} from "../../../utils/shared-routes.utils";

export default function BlogPostListItemComponent(props: { postData: BasePostDataModel, type: "highlight" | "highlight_small" | "normal_list" | "carousel_small"}) {
    const dateLabel: string = dateFormat(props.postData.publishedAt, "dd mmm yyyy");
    const imgUrl: string = props.postData.thumbnailUrl ? props.postData.thumbnailUrl : "/images/logo-512.png";

    switch (props.type) {
        case "highlight":
            return (
                <div className="w-full lg:w-1/2 p-4 mb-12 lg:mb-0 rounded-lg hover:bg-slate-50">
                    <Link className="block group w-full"
                          href={SharedRoutesUtils.getBlogPostDetailsPageURL(props.postData.uid!)}>
                        <img className="block w-full mb-5 rounded-lg drop-shadow border object-cover" src={imgUrl}
                             alt={props.postData.title}/>
                        <span
                            className="block text-gray-500 mb-5">{dateLabel}</span>
                        <h4 className="text-3xl font-semibold text-gray-900 group-hover:text-orange-900 mb-5">{props.postData.title}</h4>
                        <p className="max-w-xl text-lg text-gray-500">{props.postData.summary}</p>
                    </Link>
                </div>
            );

        case "highlight_small":
            return (
                <Link className="md:flex group mb-8 p-4 rounded-lg hover:bg-slate-50"
                      href={SharedRoutesUtils.getBlogPostDetailsPageURL(props.postData.uid!)}>
                    <img className="w-48 h-40 rounded-lg drop-shadow border object-cover" src={imgUrl}
                         alt={props.postData.title}/>
                    <div className="mt-4 md:mt-0 md:ml-6 pt-2">
                        <span className="block text-gray-500 mb-2">{dateLabel}</span>
                        <h4 className="text-xl font-semibold text-gray-900 group-hover:text-orange-900">{props.postData.title}</h4>
                    </div>
                </Link>
            );

        case "carousel_small":
            return (
                <Link className="flex flex-col group mb-8 p-4 rounded-lg hover:bg-slate-100 bg-slate-50 drop-shadow-xl"
                      href={SharedRoutesUtils.getBlogPostDetailsPageURL(props.postData.uid!)}>
                    <img className="w-full h-40 rounded-lg drop-shadow border object-cover" src={imgUrl}
                         alt={props.postData.title}/>
                    <div className="mt-4 pt-2">
                        <span className="block text-gray-500 mb-2">{dateLabel}</span>
                        <h4 className="text-xl font-semibold text-gray-900 group-hover:text-orange-900">{props.postData.title}</h4>
                    </div>
                </Link>
            );

        case "normal_list":
        default:
            return (
                <div className="py-12 border-t-2 border-gray-100">
                    <Link className="flex flex-wrap lg:flex-nowrap items-center p-4 rounded-lg hover:bg-slate-50" href={SharedRoutesUtils.getBlogPostDetailsPageURL(props.postData.uid!)}>
                        <div className="w-full lg:w-auto px-4 mb-8 lg:mb-0">
                            <img className="block w-44 h-30 rounded-lg drop-shadow border center-crop" src={imgUrl}
                                 alt={props.postData.title}/>
                        </div>

                        <div className="w-full lg:w-9/12 px-4 mb-10 lg:mb-0">
                            <div className="max-w-2xl">
                                <span className="block text-gray-400 mb-1">{dateLabel}</span>
                                <p className="text-2xl font-semibold text-gray-900">{props.postData.title}</p>
                            </div>
                        </div>
                        <div className="w-full lg:w-auto px-4 ml-auto text-right">
                            <div className="inline-flex items-center text-xl font-semibold text-orange-700 hover:text-gray-900">
                                <span className="mr-2">Read</span>
                                <svg className="animate-bounce" width="16" height="16" viewBox="0 0 16 16" fill="none"
                                     xmlns="http://www.w3.org/2000/svg">
                                    <path d="M1.33301 14.6668L14.6663 1.3335" stroke="currentColor" stroke-width="2"
                                          stroke-linecap="round" stroke-linejoin="round"></path>
                                    <path d="M1.33301 1.3335H14.6663V14.6668" stroke="currentColor" stroke-width="2"
                                          stroke-linecap="round" stroke-linejoin="round"></path>
                                </svg>
                            </div>
                        </div>
                    </Link>
                </div>
            )
    }
}