import {BasePostDataModel} from "../../../logic/shared-data/datamodel/base-post-data.model";
import {PostFormArticleBlocksDetails} from "../../../logic/shared-data/datamodel/posts-model/post-article-blocks";
import Slider from "react-slick";
import BlogPostListItemComponent from "./blog-post-list-item.component";

// Import css files
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";

export default function BlogHorizontalCarouselComponent(props: {listPosts: BasePostDataModel<any, PostFormArticleBlocksDetails>[]}) {

    function renderSlideItem(content: any, key: string) {
        return (
            <div key={key} className='flex items-center rounded justify-center max-w-56 sm:max-w-96 p-2'>
                {content}
            </div>
        )
    }

    function renderItems() {
        return props.listPosts.map((postItem) => {
            return renderSlideItem(
                <BlogPostListItemComponent postData={postItem} type={"carousel_small"} />,
                postItem.uid!
            )
        });
    }

    var settings = {
        className: "slider variable-width",
        dots: false,
        infinite: true,
        centerMode: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        variableWidth: true,
        autoplay: true,
        adaptiveHeight: true,
    };

    if (props.listPosts.length > 0) {
        return (
            <div className='flex flex-col w-full overflow-x-hidden'>
                <Slider {...settings} className={"w-full max-w-fit p-0"}>
                    {renderItems()}
                </Slider>
            </div>
        )
    } else {
        return (<></>);
    }

}