import {BasePostDataModel, PostUserInfos} from "../../../logic/shared-data/datamodel/base-post-data.model";
import CommonUtils from "../../../logic/commonutils";
import {SharedEditorBlockModel} from "../../../logic/shared-data/datamodel/shared-editor-block.model";
import {PostFormArticleBlocksDetails} from "../../../logic/shared-data/datamodel/posts-model/post-article-blocks";
import dateFormat from "dateformat";
import BaseAuthorInfosComponent from "../../shared/BaseAuthorInfos.component";

export default function BlogPostAuthorHeaderComponent(props: {postData: BasePostDataModel}) {
    const articleBlocks: SharedEditorBlockModel[] = (props.postData?.detailsPostData as PostFormArticleBlocksDetails).editorJSData!.blocks;

    function renderAuthorInfos() {
        const authorInfos: PostUserInfos = props.postData.listUsersInfos[props.postData.userCreatorId];
        return (
            <BaseAuthorInfosComponent pictureUrl={authorInfos.pictureUrl} name={authorInfos.name} subtitle={authorInfos.subtitleLabel} />
        )
    }

    function renderTags() {
        let listTags: any[] = props.postData.tags.map((tag) => {
            return (
                <div key={tag} className="rounded-md border border-gray-100 py-0.5 px-2">
                    <span className="text-gray-700 text-xs font-medium">{tag}</span>
                </div>
            )
        });

        return (
            <div className="flex items-center gap-3 flex-wrap">
                {listTags}
            </div>
        )
    }

    return (
        <div className='flex flex-col py-5'>
            <div className='divider my-0'/>
            <div className='flex items-center justify-between flex-wrap gap-4 py-2'>
                {renderAuthorInfos()}
                <div className="flex items-center gap-3 flex-wrap">
                    {renderTags()}
                    <div className="flex items-center gap-3 flex-wrap">
                        <div className="h-6 w-px bg-gray-200"></div>
                        <span className="text-gray-500">{dateFormat(props.postData.publishedAt, "dd mmm yyyy")}</span>
                        <svg xmlns="http://www.w3.org/2000/svg" width="4" height="5" viewBox="0 0 4 5" fill="none">
                            <circle cx="2" cy="2.66669" r="2" fill="#D1D1D1"></circle>
                        </svg>
                        <span className="text-gray-500">{CommonUtils.calculateReadingTimeFromEditorBlocks(articleBlocks)} min read</span>
                    </div>
                </div>
            </div>
            <div className='divider my-0'/>
        </div>
    )
}