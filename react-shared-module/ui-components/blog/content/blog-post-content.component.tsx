import {BasePostDataModel} from "../../../logic/shared-data/datamodel/base-post-data.model";
import {PostFormArticleBlocksDetails} from "../../../logic/shared-data/datamodel/posts-model/post-article-blocks";
import Link from "next/link";
import {SharedEditorBlockModel} from "../../../logic/shared-data/datamodel/shared-editor-block.model";
import CommonUtils from "../../../logic/commonutils";
import ArticleBlocksViewerComponent from "./../shared/ArticleBlocksViewer.component";
import BlogPostAuthorHeaderComponent from "./../shared/blog-post-author-header.component";
import {SharedRoutesUtils} from "../../../utils/shared-routes.utils";
import Image from "next/image";

// Inspired by:
// https://shuffle.dev/editor?project=ebba95de5d650c27360bd7172a7fb202ffbdcdb6&tutorial=1
// and https://shuffle.dev/editor?project=723c410cd22b678203f9e8cf5f5f9ccc10285f15&tutorial=1
export default function BlogPostContentComponent(props: { postDetails: BasePostDataModel<any, PostFormArticleBlocksDetails>, backUrl?: string}) {
    const articleBlocks: SharedEditorBlockModel[] = (props.postDetails?.detailsPostData as PostFormArticleBlocksDetails).editorJSData!.blocks;


    function renderTableOfContents() {
        let listItems: any[] = [];

        listItems = articleBlocks
            .filter((block) => block.type === "header" && block.data!.text)
            .map((block) => {
                const padding = "pl-"+(block.data!.level!-1);
                return (<Link key={block.id} href={"#" + CommonUtils.string_to_slug(block.data!.text!)}
                              className={"inline-block text-gray-500 hover:text-red-500 transition duration-200 text-lg font-semibold " + padding}>{block.data!.text}</Link>)
            });

        return (
           <>
               <p className="font-semibold mb-6">TABLE OF CONTENTS:</p>
               <div className="flex flex-col gap-5">
                   {listItems}
               </div>
           </>
        )
    }

    function renderThumbnail() {
        if (!props.postDetails.thumbnailUrl) {
            return <></>
        }

        return (
            <Image className="w-full rounded-2xl mb-16"
               width={500} height={500}
                 src={props.postDetails.thumbnailUrl!}
                 alt={props.postDetails.title!}/>
        )
    }

    return (
        <section className="overflow-hidden py-8 pl-7 lg:pl-15 pr-7 lg:pr-30">
            <div className="flex flex-wrap -mx-4">
                <div className="w-full lg:w-1/3 xl:w-1/6 p-2">

                    <Link className=" text-gray-500 group mb-24 lg:mb-24 inline-flex items-center gap-2 flex-wrap"
                          href={props.backUrl ? props.backUrl : SharedRoutesUtils.getBlogPageUrl()}>
                        <div className="group-hover:text-gray-600 transition duration-200">
                            <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 20 20"
                                 fill="none">
                                <path d="M15.4167 10H5M5 10L10 5M5 10L10 15" stroke="currentColor" stroke-width="1.5"
                                      stroke-linecap="round" stroke-linejoin="round"></path>
                            </svg>
                        </div>
                        <span
                            className="group-hover:text-gray-600 transition duration-200 font-bold">Back to Blog</span>
                    </Link>

                    {renderTableOfContents()}
                </div>

                <div className="w-full lg:w-2/3 pt-5 xl:w-5/6 lg:p-7 xl:px-10">
                    <div className="container max-w-xl lg:max-w-4xl">
                        <h1 className="text-3xl lg:text-5xl font-bold mb-2 ">{props.postDetails.title!}</h1>
                        <span className="text-sm lg:text-base font-light mb-16 max-w-xl lg:max-w-3xl text-gray-500">{props.postDetails.summary!}</span>
                        <BlogPostAuthorHeaderComponent postData={props.postDetails} />
                        <ArticleBlocksViewerComponent blocks={articleBlocks} />

                    </div>
                </div>
            </div>


        </section>
    );
}