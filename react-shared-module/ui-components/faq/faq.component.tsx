import {useState} from "react";
import LandingPageSecondaryTitleComponent from "../saas-landing-page/shared/landing-page-secondary-title.component";

export interface FAQItemModel {
    title: string,
    message: string,
}

export default function FaqComponent(props: {listFAQItems: FAQItemModel[], isLandingPage: boolean}) {
    const [openedIndex, setOpenedIndex] = useState(-1);

    function renderFAQDropdownItem(faqItem: FAQItemModel, index: number) {
        const isOpened = openedIndex === index;

        return (
            <div className="w-full p-1">
                <button onClick={() => setOpenedIndex(isOpened ? -1 : index)} className='w-full'>
                    <div
                        className={"py-7 px-8 bg-white bg-opacity-60 border-2 rounded-2xl shadow-xl " + (isOpened ? "border-red-500" : "border-slate-200")}>
                        <div className="flex flex-wrap justify-between -m-2">
                            <div className="flex-1 p-2">
                                <h3 className="mb-4 text-lg font-semibold leading-normal">{faqItem.title}</h3>
                                {isOpened ?
                                    <p className="text-gray-600 font-medium">{faqItem.message}</p> : <></>}
                            </div>
                            <div className="w-auto p-2">

                                {isOpened ?
                                    (
                                        <svg className="relative top-1" width="20" height="20" viewBox="0 0 20 20" fill="none"
                                             xmlns="http://www.w3.org/2000/svg">
                                            <path d="M4.16732 12.5L10.0007 6.66667L15.834 12.5" stroke="#4F46E5"
                                                  stroke-width="2" stroke-linecap="round" stroke-linejoin="round"></path>
                                        </svg>) : (
                                        <svg className="relative top-1" width="18" height="18" viewBox="0 0 18 18"
                                             fill="none" xmlns="http://www.w3.org/2000/svg">
                                            <path d="M14.25 6.75L9 12L3.75 6.75" stroke="#18181B" stroke-width="2"
                                                  stroke-linecap="round" stroke-linejoin="round"></path>
                                        </svg>)
                                }
                            </div>
                        </div>
                    </div>
                </button>
            </div>
        )

    }

    function renderTitle() {
        if (props.isLandingPage) {
            return (<LandingPageSecondaryTitleComponent>Frequently Asked Questions</LandingPageSecondaryTitleComponent>)
        } else {
            return (
                <h1 className="my-10 text-2xl md:text-4xl xl:text-6xl text-center font-bold font-heading tracking-px-n leading-none">Frequently
                    Asked Questions</h1>
            )
        }
    }

    return (
        <section className="relative pt-5 pb-5 bg-blueGray-50 overflow-hidden w-full">
            <div className="relative z-10 container px-4 mx-auto">
                {renderTitle()}
                <div className="md:max-w-4xl mx-auto gap-5 my-10">
                    {props.listFAQItems.map((faqItem, index) => renderFAQDropdownItem(faqItem, index))}
                </div>
            </div>
        </section>
    )
}