import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:material_color_scheme/material_color_scheme.dart';

class AppTheme {
  static ThemeData appTheme = ThemeData(
      primaryColor: ThemeColors.BASE_MAIN_COLOR,
      primarySwatch: generateSwatch(ThemeColors.BASE_MAIN_COLOR),
      backgroundColor: ThemeColors.BASE_BG_LIST_COLOR,
      accentColor: generateSwatch(ThemeColors.BASE_MAIN_COLOR),
      scaffoldBackgroundColor: ThemeColors.BASE_BG_LIST_COLOR,
      textTheme: TextTheme(
        headline1: GoogleFonts.fredokaOne(color: ThemeColors.TXT_BLACK_COLOR, fontSize: 30, fontWeight: FontWeight.bold),
        headline2: GoogleFonts.robotoMono(color: ThemeColors.BTN_TXT_LIGHT_BG, fontSize: 20, fontWeight: FontWeight.bold),
        headline3: GoogleFonts.robotoMono(color: ThemeColors.TXT_BLACK_COLOR, fontSize: 18),
        subtitle1: GoogleFonts.oxanium(color: ThemeColors.TXT_BLACK_COLOR, fontSize: 20, fontWeight: FontWeight.bold),
        subtitle2: GoogleFonts.oxanium(color: ThemeColors.TXT_GREY_COLOR, fontSize: 18),
        bodyText1: GoogleFonts.oxanium(color: ThemeColors.TXT_BLACK_COLOR, fontSize: 16),
        bodyText2: GoogleFonts.oxanium(color: ThemeColors.TXT_BLACK_COLOR, fontSize: 14),
      ),
      appBarTheme: AppBarTheme(elevation: 10, color: ThemeColors.COLOR_WHITE, iconTheme: IconThemeData(color: ThemeColors.BASE_MAIN_COLOR_DARK), titleTextStyle: GoogleFonts.oxanium(color: ThemeColors.BASE_MAIN_COLOR_DARK, fontSize: 16)));

  static TextStyle getTitleStyle() {
    return appTheme.textTheme.headline1!;
  }

  static TextStyle getTitle2Style() {
    return appTheme.textTheme.headline2!;
  }

  static TextStyle getTitle3Style() {
    return appTheme.textTheme.headline3!;
  }

  static TextStyle getSubtitleStyle() {
    return appTheme.textTheme.subtitle1!;
  }

  static TextStyle getSubtitle2Style() {
    return appTheme.textTheme.subtitle1!;
  }

  static TextStyle getBodyStyle() {
    return appTheme.textTheme.bodyText1!;
  }

  static TextStyle getBody2Style() {
    return appTheme.textTheme.bodyText2!;
  }


}

class ThemeColors {
  // Constant colors
  static const Color DEFAULT_CONSTANT_COLOR = Color(0xFFD97D7F);
  static const Color DEFAULT_BASE_BORDER_COLOR = DEFAULT_CONSTANT_COLOR;


  static Color BASE_MAIN_COLOR_LIGHT3 = Color(0xFFF2C0AD);
  static Color BASE_MAIN_COLOR_LIGHT2 = Color(0xFFEEAD94);
  static const Color BASE_MAIN_COLOR_LIGHT1 = Color(0xFFE2958A);
  static Color BASE_MAIN_COLOR = Color(0xFFD97D7F);
  static Color BASE_MAIN_COLOR_DARK = Color(0xFFCD6475);
  static Color BASE_MAIN_COLOR_DARK1 = Color(0xFFC34E6C);
  static Color BASE_MAIN_COLOR_DARK2 = Color(0xFFB74162);
  static Color BASE_SECOND_COLOR_DARK = Color(0xFF373c48);
  static Color BASE_SECOND_COLOR = Color(0xFF4e5667);
  static Color BASE_SECOND_COLOR_LIGHT1 = Color(0xFF717885);
  static Color BASE_SECOND_COLOR_LIGHT2 = Color(0xFF959aa4);
  static Color BASE_BORDER_COLOR = BASE_MAIN_COLOR;
  static Color BASE_BORDER_NO_FOCUS_COLOR = Color(0xFFAAAAAA);
  static Color BASE_BORDER_DISABLE_COLOR = Color(0x66AAAAAA);

  static Color BASE_BG_LIST_COLOR = Color(0xFFFFFFFF);
  static Color BASE_BG_LIST_GREY_COLOR = Color(0xFFFAFAFA);
  static Color BASE_BG_LIST_GREY_DARK_COLOR = Color(0xFFE9E9EA);
  static Color BASE_BG_ACTION_ITEM_COLOR = BASE_MAIN_COLOR_LIGHT2;
  static Color BASE_BG_ACTION_ITEM_BLUE_COLOR = BASE_MAIN_COLOR_LIGHT1;

  static const Color TXT_BLACK_COLOR = Color(0xFF4D5669);
  static Color TXT_GREY_COLOR = Color(0xFFAAAAAA);

  // Buttons colors
  static Color BTN_BG_RED = Color(0xFFDE4E47);
  static Color BTN_BG_BLUE = Color(0xFF4882F7);
  static Color BTN_BG_DARK = BASE_MAIN_COLOR;
  static Color BTN_BG_LIGHT = BASE_MAIN_COLOR_LIGHT1;
  static Color BTN_TXT_DARK_BG = Color(0xFFEEE6FE); // When is a text in a button with a dark BG Color
  static Color BTN_TXT_LIGHT_BG = BASE_MAIN_COLOR_DARK2; // When is a text in a button with a light BG Color

  // State colors
  static Color STATE_SUCCESS_NORMAL = Color(0xFF7cb342);
  static Color STATE_FAILED_NORMAL = Color(0xFFef5350);
  static Color STATE_FAILED_DARK = Color(0xFF9E0701);
  static Color STATE_WARNING_NORMAL = Color(0xFFffca28);
  static Color STATE_NEUTRAL_NORMAL = Color(0xFFAAAAAA);

  // CustomColors
  static const Color COLOR_WHITE = Color(0xFFFFFFFF);
  static const Color COLOR_BLACK = Color(0xFF000000);
  static const Color COLOR_GRAY = Color(0xFFAAAAAA);
  static const Color COLOR_GRAY_DRK = Color(0xFF4D5669);
  static const Color COLOR_OVERLAY = Color(0x66333333);
  static const Color COLOR_TRANSPARENT = Color(0x00FFFFFF);

}

class CustomStyles {
  static final ERROR_TEXT_STYLE = GoogleFonts.oxanium(color: ThemeColors.STATE_FAILED_NORMAL, fontSize: 14);
  static final TEXT_FIELD_STYLE = TextStyle(color: ThemeColors.TXT_BLACK_COLOR, fontSize: 15);
  static final TEXT_FIELD_HINT_STYLE = TextStyle(color: ThemeColors.TXT_GREY_COLOR, fontSize: 13);
}

class DefaultValues {
  static double BORDER_RADIUS = 10;
  static double CONTAINER_PADDING = 15;
  static double LIST_MARGINS = 12;
  static double LIST_MARGIN_BETWEEN = 20;
  static double AVATAR_TEXT_SPACE_BETWEEN = 15;
  static double BIG_MARGIN_TOP = 20;
}
