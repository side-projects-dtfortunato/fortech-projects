import 'package:flutter/cupertino.dart';
import 'package:fortechbase/shareable/shareable-ui/theme/app.theme.dart';
import 'package:linkable/linkable.dart';

class AppItemTitleValueFieldComponent extends StatefulWidget {

  String title;
  String value;
  TextAlign? textAlign;

  AppItemTitleValueFieldComponent(this.title, this.value, {this.textAlign});

  @override
  State<StatefulWidget> createState() => _State();

}

class _State extends State<AppItemTitleValueFieldComponent> {

  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.centerLeft,
      padding: EdgeInsets.only(top: DefaultValues.CONTAINER_PADDING),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(widget.title, style: AppTheme.appTheme.textTheme.headline2,),
          SizedBox(height: widget.value != null ? 4 : 0),
          widget.value != null ? Linkable(text: widget.value, style: AppTheme.appTheme.textTheme.bodyText1) : Container()
        ],
      ),
    );
  }

}