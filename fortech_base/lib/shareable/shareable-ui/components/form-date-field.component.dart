import 'package:dash_chat/dash_chat.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:fortechbase/shareable/shareable-ui/components/app-textfield.component.dart';

typedef OnDateSelected(DateTime? date);

class FormDateFieldComponent extends StatefulWidget {
  String label;
  String hint;
  OnDateSelected onDateSelected;
  DateTime? defaultSelectedTime;

  FormDateFieldComponent(this.onDateSelected, {this.label = "Pick a date", this.hint = "dd/mm/yyyy", this.defaultSelectedTime});

  @override
  State<StatefulWidget> createState() => _State();
}

class _State extends State<FormDateFieldComponent> {
  DateTime? selectedDate;
  TextEditingController _controller = TextEditingController();

  @override
  void initState() {
    super.initState();

    if (widget.defaultSelectedTime != null) {
      this.selectedDate = widget.defaultSelectedTime;
    }
    this._controller.text = getSelectedDateStr();
  }

  String getSelectedDateStr() {
    if (this.selectedDate != null) {
      return DateFormat("dd/MM/yyyy").format(this.selectedDate!);
    } else {
      return "";
    }
  }

  void onPickDatePressed() {
    DatePicker.showDatePicker(context, currentTime: this.selectedDate != null ? this.selectedDate : DateTime.now(), onConfirm: (dateChanged) {
      setState(() {
        this.selectedDate = dateChanged;
        this._controller.text = getSelectedDateStr();
        widget.onDateSelected(this.selectedDate);
      });
    });
  }

  Widget _genRootContainer() {
    return GestureDetector(
      onTap: this.onPickDatePressed,
      child: AppTextFieldComponent(
        hintText: widget.hint,
        labelText: widget.label,
        maxLength: 15,
        maxLines: 1,
        textInputType: TextInputType.text,
        isEditable: false,
        controller: this._controller,
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: _genRootContainer(),
    );
  }
}
