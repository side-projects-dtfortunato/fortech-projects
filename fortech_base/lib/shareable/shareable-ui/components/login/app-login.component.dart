import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:fortechbase/shareable/shareable-managers/base-central-user-data.manager.dart';
import 'package:fortechbase/shareable/shareable-ui/components/app-scaffold.component.dart';
import 'package:fortechbase/shareable/shareable-ui/components/login/login-screen.component.dart';
import 'package:fortechbase/shareable/shareable-ui/theme/app.theme.dart';



class AppLoginComponent extends StatefulWidget {
  VoidCallback? onUserIsLoggedIn;
  VoidCallback? onEulaPressed;
  bool hasCancelButton;

  AppLoginComponent({this.onUserIsLoggedIn, this.onEulaPressed, this.hasCancelButton = true});


  @override
  State<StatefulWidget> createState() => _State();

}


class _State extends State<AppLoginComponent> {
  StreamSubscription? _subCentralUserData;

  @override
  void initState() {
    super.initState();

    this._subCentralUserData = BaseCentralUserDataManager().onCentralUserData().listen((event) {
      if (BaseCentralUserDataManager().isUserLoggedin() && widget.onUserIsLoggedIn != null) {
        widget.onUserIsLoggedIn!();
      }
    });
  }

  @override
  Widget build(BuildContext context) {

    return AppScaffold(
      bgFullWidth: true,
      //key: BannerMessageHandlerManager().scaffoldKey,
      backgroundColor: ThemeColors.BASE_MAIN_COLOR,
      body: LoginScreenComponent(onUserIsLoggedIn: widget.onUserIsLoggedIn, onEulaPressed: widget.onEulaPressed, hasCancelButton: widget.hasCancelButton,),
    );
  }

  @override
  void dispose() {

    if (this._subCentralUserData != null) {
      this._subCentralUserData!.cancel();
      this._subCentralUserData = null;
    }

    super.dispose();
  }
}