import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fortechbase/shareable/shareable-ui/base-page-body.component.dart';
import 'package:fortechbase/shareable/shareable-ui/components/app-bar.component.dart';
import 'package:fortechbase/shareable/shareable-ui/components/app-button.component.dart';
import 'package:fortechbase/shareable/shareable-ui/theme/app.theme.dart';

class LoginEulaPage extends StatelessWidget {
  static String PAGE_ROUTE = "eula";
  static String PARAM_APP_NAME = "appname";

  String appName;

  LoginEulaPage(this.appName);


  BuildContext? _context;

  Widget _genTitle() {
    return Text(this.appName + " User Agreement", style: AppTheme.appTheme.textTheme.headline1,);
  }

  Widget _genSubTitle(String titleText) {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 10),
      child: Text(titleText, style: AppTheme.appTheme.textTheme.subtitle1),
    );
  }

  Widget _genBodyText(String text) {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 10),
      child: Text(text, style: AppTheme.appTheme.textTheme.subtitle2!.apply(color: ThemeColors.COLOR_BLACK)),
    );
  }

  Widget _genBody() {
    return Container(
      padding: EdgeInsets.all(DefaultValues.CONTAINER_PADDING),
      child: ListView(
        children: [
          _genTitle(),
          _genBodyText('David Fort built the ${this.appName} app as a Free app. This SERVICE is provided by David Fort at no cost and is intended for use as is.\nThis page is used to inform visitors regarding my policies with the collection, use, and disclosure of Personal Information if anyone decided to use my Service.\nIf you choose to use my Service, then you agree to the collection and use of information in relation to this policy. The Personal Information that I collect is used for providing and improving the Service. I will not use or share your information with anyone except as described in this Privacy Policy.\nThe terms used in this Privacy Policy have the same meanings as in our Terms and Conditions, which is accessible at ${this.appName} unless otherwise defined in this Privacy Policy.'),
          _genSubTitle("Information Collection and Use"),
          _genBodyText("For a better experience, while using our Service, I may require you to provide us with certain personally identifiable information, including but not limited to David Fort. The information that I request will be retained on your device and is not collected by me in any way.\nThe app does use third party services that may collect information used to identify you.\nLink to privacy policy of third party service providers used by the app \n-Google Play Services\n-AdMob\n-Google Analytics for Firebase\n-Firebase Crashlytics"),
          _genSubTitle("Log Data"),
          _genBodyText("I want to inform you that whenever you use my Service, in a case of an error in the app I collect data and information (through third party products) on your phone called Log Data. This Log Data may include information such as your device Internet Protocol (“IP”) address, device name, operating system version, the configuration of the app when utilizing my Service, the time and date of your use of the Service, and other statistics."),
          _genSubTitle("Service Providers"),
          _genBodyText("I may employ third-party companies and individuals due to the following reasons:\n-To facilitate our Service;\n-To provide the Service on our behalf;\n-To perform Service-related services; or\n-To assist us in analyzing how our Service is used.\n-I want to inform users of this Service that these third parties have access to your Personal Information. The reason is to perform the tasks assigned to them on our behalf. However, they are obligated not to disclose or use the information for any other purpose."),
          _genSubTitle("Childrens's Privacy"),
          _genBodyText("These Services do not address anyone under the age of 18. I do not knowingly collect personally identifiable information from children under 18. In the case I discover that a child under 18 has provided me with personal information, I immediately delete this from our servers. If you are a parent or guardian and you are aware that your child has provided us with personal information, please contact me so that I will be able to do necessary actions."),
          _genSubTitle("User Generated Content"),
          _genBodyText("${this.appName} app allows you (and any user with access to the app) to generate their own content and publish it publicly. The type of content shouldn't contain abusive, sexual, nudity, violence or any other objectionable content. \nIf you receive any of this type of content, you have the option to report that content and block the user permanently and we will review your report in during the next 24 hours. We will not tolerate this kind of content and so, we will block permanently all abusive users.\nPlease, before you publish any type of content, think if that content is something that other users around the world are interested to see it."),
          AppButtonComponent(text: "OK", color: ThemeColors.STATE_SUCCESS_NORMAL, onPressed: () {
            Navigator.pop(this._context!);
          },)
        ],
      ),
    );
  }

 @override
  Widget build(BuildContext context) {
    this._context = context;
    return Scaffold(
      appBar: CustomAppBar.withTitle("Terms & Conditions"),
      body: BasePageBodyComponent(
        mobileBody: _genBody(),
      ),
    );
  }

}