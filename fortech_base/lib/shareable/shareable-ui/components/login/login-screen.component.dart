import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:fortechbase/shareable/shareable-managers/app-utils.manager.dart';
import 'package:fortechbase/shareable/shareable-managers/auth-firebase.manager.dart';
import 'package:fortechbase/shareable/shareable-managers/banner-handler.manager.dart';
import 'package:fortechbase/shareable/shareable-managers/base-central-user-data.manager.dart';
import 'package:fortechbase/shareable/shareable-ui/base-page-body.component.dart';
import 'package:fortechbase/shareable/shareable-ui/components/app-button.component.dart' as AppButtonComponent;
import 'package:fortechbase/shareable/shareable-ui/decoration/app-field-input-decoration.style.dart';
import 'package:fortechbase/shareable/shareable-ui/theme/app.theme.dart';
import 'package:fortechbase/shareable/utils/log.dart';
import 'package:fortechbase/shareable/utils/resource.assets.dart';
import 'package:fortechbase/shareable/utils/resources.language.dart';
import 'package:fortechbase/shareable/utils/ui-utils.dart';
import 'package:fortechbase/shareable/utils/utils.dart';

class LoginScreenComponent extends StatefulWidget {
  VoidCallback? onUserIsLoggedIn;
  VoidCallback? onEulaPressed;
  bool hasCancelButton;

  LoginScreenComponent({this.onUserIsLoggedIn, this.onEulaPressed, this.hasCancelButton = true});

  @override
  State<StatefulWidget> createState() => _State();
}

class _State extends State<LoginScreenComponent> {
  StreamSubscription? _subOnAuthState;
  bool isLoading = false;

  // Data
  String? _formEmail;
  String? _formPassword;

  @override
  void initState() {
    super.initState();

    this._subOnAuthState = BaseCentralUserDataManager().onCentralUserData().listen((centralUserData) {
      Log.d("LoginPage - isLoggedIn: " + BaseCentralUserDataManager().isUserLoggedin().toString());
      if (BaseCentralUserDataManager().isUserLoggedin() && centralUserData != null) {
        AppUtilsManager().dismissLoadingPopup();

        if (widget.onUserIsLoggedIn != null) {
          widget.onUserIsLoggedIn!();
        }
      }
    });
  }

  void _onLogin(String? email, String? password) {
    setState(() {
      this.isLoading = true;
    });
    // Check if the email is a valid email
    if (!Utils.isEmailValid(email)) {
      BannerMessageHandlerManager().displayMessage(context, AppLocalizations.of(context)!.translate("login_signup_invalid_email"), false);
    } else if (password == null || password.length < 8) {
      BannerMessageHandlerManager().displayMessage(context, AppLocalizations.of(context)!.translate("login_invalid_password"), false);
    } else {
      AuthFirebaseManager().onLoginPressed(context, email!, password!, onComplete: (isSuccess) {
        // only clear loading if not success
        if (!isSuccess) {
          setState(() {
            this.isLoading = false;
          });
        }
      });
    }
  }

  void _onSignup(String? email, String? password) {
    setState(() {
      this.isLoading = true;
    });

    // Check if the email is a valid email
    if (!Utils.isEmailValid(email)) {
      BannerMessageHandlerManager().displayMessage(context, AppLocalizations.of(context)!.translate("login_signup_invalid_email"), false);
    } else if (password == null || password.length < 8) {
      BannerMessageHandlerManager().displayMessage(context, AppLocalizations.of(context)!.translate("login_invalid_password"), false);
    } else {
      AuthFirebaseManager().onSignupPressed(context, email!, password!, onComplete: (isSuccess) {
        // only clear loading if not success
        if (!isSuccess) {
          setState(() {
            this.isLoading = false;
          });
        }
      });
    }
  }

  void _recoverPassword(String email) {
    // Check if the email is a valid email
    if (!Utils.isEmailValid(email)) {
      BannerMessageHandlerManager().displayMessage(context, AppLocalizations.of(context)!.translate("login_signup_invalid_email"), false);
    } else {
      AuthFirebaseManager().onForgotPasswordPressed(context, email);
    }
  }

  Widget _genEulaAgreement() {
    return GestureDetector(
      onTap: () {
        // LoginEulaPage.openPage(context);
        if (widget.onEulaPressed != null) {
          widget.onEulaPressed!();
        }
        // launch("https://panify.pt/privacy-policy.html");
      },
      child: Container(
        margin: EdgeInsets.symmetric(vertical: 10),
        child: Text(AppLocalizations.of(context)!.translate("login_eula_agreement")!, style: AppTheme.appTheme.textTheme.bodyText1!.apply(color: ThemeColors.COLOR_BLACK, decoration: TextDecoration.underline)),
      ),
    );
  }

  Widget _genEmailField() {
    return TextFormField(
      autofocus: false,
      onChanged: (email) {
        this._formEmail = email;
      },
      keyboardType: TextInputType.emailAddress,
      inputFormatters: [FilteringTextInputFormatter.deny(RegExp(r"\s\b|\b\s"))],
      decoration: AppFieldInputDecorationStyle(
          prefixIcon: Icon(
            Icons.email,
            color: ThemeColors.BASE_BORDER_COLOR,
          ),
          hintText: AppLocalizations.of(context)!.translate("login_email_hint")!,
          labelText: AppLocalizations.of(context)!.translate("login_email")!,
          borderColor: ThemeColors.BASE_BORDER_COLOR),
      validator: (val) => Utils.formFieldValidatorNotEmpty(context, val),
      maxLines: 1,
      style: AppTheme.appTheme.textTheme.bodyText1,
    );
  }

  Widget _genForgotPasswordBtn() {
    return Container(
        child: GestureDetector(
      onTap: () {
        _recoverPassword(this._formEmail!);
      },
      child: Text(
        AppLocalizations.of(context)!.translate("login_forgot_password")!,
        style: AppTheme.appTheme.textTheme.bodyText1!.apply(color: ThemeColors.BASE_MAIN_COLOR, decoration: TextDecoration.underline),
      ),
    ));
  }

  Widget _genPasswordField() {
    return TextFormField(
      autofocus: false,
      onChanged: (password) {
        this._formPassword = password;
      },
      obscureText: true,
      inputFormatters: [FilteringTextInputFormatter.deny(new RegExp(r"\s\b|\b\s"))],
      decoration: AppFieldInputDecorationStyle(
          prefixIcon: Icon(
            Icons.lock,
            color: ThemeColors.BASE_BORDER_COLOR,
          ),
          hintText: AppLocalizations.of(context)!.translate("login_password_hint")!,
          labelText: AppLocalizations.of(context)!.translate("login_password")!,
          borderColor: ThemeColors.BASE_BORDER_COLOR),
      validator: (val) => Utils.formFieldValidatorNotEmpty(context, val),
      maxLength: 25,
      maxLines: 1,
      style: AppTheme.appTheme.textTheme.bodyText1,
    );
  }

  Widget _genSigninOrSignupButton() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: [
        // Login
        AppButtonComponent.AppButtonComponent(
          onPressed: () {
            this._onLogin(this._formEmail, this._formPassword);
          },
          color: ThemeColors.BASE_MAIN_COLOR,
          text: AppLocalizations.of(context)!.translate("login_signin_btn")!,
          buttonStyle: AppButtonComponent.AppButtonStyle.FILLED,
        ),
        // Login
        AppButtonComponent.AppButtonComponent(
          onPressed: () {
            this._onSignup(this._formEmail, this._formPassword);
          },
          text: AppLocalizations.of(context)!.translate("login_signup_btn")!,
          color: ThemeColors.BASE_MAIN_COLOR,
          buttonStyle: AppButtonComponent.AppButtonStyle.FILLED,
        )
      ],
    );
  }

  Widget _genHeader() {
    return Column(
      children: [Image.asset(ResourcesAssets.IC_LOGO, height: 50), SizedBox(height: 10), Image.asset(ResourcesAssets.IMAGE_LOGO_NAME, height: 50), Text(AppLocalizations.of(context)!.translate("logo_slogan")!, style: AppTheme.appTheme.textTheme.bodyText1)],
    );
  }

  Widget _genCancelBtn() {
    if (widget.hasCancelButton) {
      return Container(
          child: GestureDetector(
        onTap: () {
          Navigator.pop(context);
        },
        child: Text(
          AppLocalizations.of(context)!.translate("generic_cancel")!,
          style: AppTheme.appTheme.textTheme.bodyText1!.apply(color: ThemeColors.BASE_MAIN_COLOR_LIGHT1, decoration: TextDecoration.underline),
          textAlign: TextAlign.center,
        ),
      ));
    } else {
      return Container();
    }
  }

  Widget _genMobileBody() {
    List<Widget> listChilds = [];

    listChilds.add(_genHeader());
    listChilds.add(SizedBox(height: 25));

    if (isLoading) {
      listChilds.add(Container(
        height: 100,
        alignment: Alignment.center,
        child: CircularProgressIndicator(),
      ));
    } else {
      listChilds.add(_genEulaAgreement());
      listChilds.add(_genEmailField());
      listChilds.add(SizedBox(height: 4));
      listChilds.add(_genForgotPasswordBtn());
      listChilds.add(SizedBox(height: 12));
      listChilds.add(_genPasswordField());
      listChilds.add(SizedBox(height: 8));
      listChilds.add(_genSigninOrSignupButton());
      listChilds.add(SizedBox(height: 8));
      /*PlatformUtils().isIOS() ? Container() : Text(AppLocalizations.of(context).translate("generic_or"), textAlign: TextAlign.center,),
          SizedBox(height: 8),
          PlatformUtils().isIOS() ? Container() : _genGoogleSigninBtn(),
          SizedBox(height: 8),*/
      listChilds.add(_genCancelBtn());
    }

    return Container(
      width: 480,
      padding: EdgeInsets.all(DefaultValues.CONTAINER_PADDING),
      margin: EdgeInsets.all(DefaultValues.CONTAINER_PADDING),
      decoration: UIUtils.getRoundedBackgroundBoxDecoration(ThemeColors.BASE_BG_LIST_COLOR.withOpacity(0.9), ThemeColors.BASE_BG_LIST_COLOR.withOpacity(0.9), null),
      child: ListView(
        shrinkWrap: true,
        children: listChilds,
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: MediaQuery.of(context).size.height,
      width: MediaQuery.of(context).size.width,
      decoration: new BoxDecoration(
        image: new DecorationImage(
          image: AssetImage(ResourcesAssets.IMG_LOGIN_BG),
          fit: BoxFit.cover,
        ),
      ),
      child: Center(
        child: BasePageBodyComponent(
          mobileBody: _genMobileBody(),
        ),
      ),
    );
  }

  @override
  void dispose() {
    if (_subOnAuthState != null) {
      _subOnAuthState!.cancel();
      _subOnAuthState = null;
    }

    super.dispose();
  }
}
