import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fortechbase/shareable/shareable-ui/theme/app.theme.dart';


enum AppButtonStyle {TRANSPARENT, FILLED}

class AppButtonComponent extends StatelessWidget{

  Color color;
  String text;
  AppButtonStyle? buttonStyle;
  VoidCallback? onPressed;


  AppButtonComponent({this.color = ThemeColors.DEFAULT_CONSTANT_COLOR, this.text: "", this.onPressed, this.buttonStyle = AppButtonStyle.FILLED});

  @override
  Widget build(BuildContext context) {


    return TextButton(onPressed: onPressed,
        style: TextButton.styleFrom(
          backgroundColor: this.buttonStyle == AppButtonStyle.FILLED ? this.color : ThemeColors.COLOR_TRANSPARENT,
          disabledBackgroundColor: this.color.withOpacity(0.6),
        ),
        child: Text(this.text,
          style: TextStyle(fontSize: 12, color: this.buttonStyle == AppButtonStyle.FILLED ? ThemeColors.COLOR_WHITE : this.color, fontWeight: FontWeight.normal),
          textAlign: TextAlign.center,));

    /*return FlatButton(
      onPressed: this.onPressed,
      color: this.buttonStyle == ButtonStyle.FILLED ? this.color : ThemeColors.COLOR_TRANSPARENT,
      textColor: this.buttonStyle == ButtonStyle.FILLED ? ThemeColors.COLOR_WHITE : this.color,
      shape: UIUtils.getRoundedButtonShape(this.color),
      child: Text(this.text, style: TextStyle(fontSize: 12, fontWeight: FontWeight.normal), textAlign: TextAlign.center,),
    ); */
  }
}