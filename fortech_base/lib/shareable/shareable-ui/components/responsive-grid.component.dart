import 'package:flutter/cupertino.dart';

class AppResponsiveGridComponent extends StatefulWidget {
  List<ResponseGridChild> childrens;
  int numberColumns;

  AppResponsiveGridComponent(this.childrens, {this.numberColumns = 1});

  @override
  State<StatefulWidget> createState() => _State();
}

class _State extends State<AppResponsiveGridComponent> {
  int getColumnMax() {
    return widget.numberColumns;
  }

  Widget _genListChilds() {
    List<Widget> listChilds = [];

    List<Widget> rowCols = [];

    // Add childrens
    widget.childrens.forEach((childWidget) {

      if (childWidget.isFullWidth && rowCols.isNotEmpty) {
        // Add columns and create a new row
        listChilds.add(IntrinsicHeight(child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: rowCols,
        ),));
        rowCols = [];

        // Add the current row directly on list, because the new child should occupy the full width
        rowCols.add(
          Flexible(
            child: childWidget,
            flex: 1,
          ),
        );

        listChilds.add(IntrinsicHeight(child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: rowCols,
        ),));
        rowCols = [];
      } else {
        // Normal flow
        rowCols.add(
          Flexible(
            child: childWidget,
            flex: 1,
          ),
        );

        if (getColumnMax() == rowCols.length || childWidget.isFullWidth) {
          // Add columns and create a new row
          listChilds.add(IntrinsicHeight(child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: rowCols,
          ),));
          rowCols = [];
        }
      }

    });

    // Add Columns
    if (rowCols.isNotEmpty) {
      listChilds.add(IntrinsicHeight(child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: rowCols,
      ),));
    }


    return ListView.builder(
        padding: EdgeInsets.symmetric(vertical: 8),
        itemCount: listChilds.length,
        itemBuilder: (ctx, index) {
          return listChilds[index];
        });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: _genListChilds(),
    );
  }
}


class ResponseGridChild extends StatelessWidget {
  Widget child;
  bool isFullWidth;

  ResponseGridChild(this.child, {this.isFullWidth = false});

  @override
  Widget build(BuildContext context) {
    return this.child;
  }
}