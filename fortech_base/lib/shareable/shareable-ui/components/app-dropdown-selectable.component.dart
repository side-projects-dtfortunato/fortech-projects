import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fortechbase/shareable/shareable-ui/theme/app.theme.dart';

typedef OnDropdownSelected(DropdownItemModel? selectedItem);

class DropdownItemModel {
  String id;
  String label;

  DropdownItemModel(this.id, this.label);
}

class AppDropdownSelectableComponent extends StatefulWidget {
  List<DropdownItemModel> listDropdownItems;
  DropdownItemModel? selectedDropdownItem;
  OnDropdownSelected? onDropdownSelected;

  AppDropdownSelectableComponent(this.listDropdownItems, {this.selectedDropdownItem, this.onDropdownSelected});

  @override
  State<StatefulWidget> createState() => _State();

}

class _State extends State<AppDropdownSelectableComponent> {

  @override
  Widget build(BuildContext context) {
    return Container(
      child: DropdownButton(
        icon: Icon(Icons.arrow_drop_down),
        iconSize: 20,
        elevation: 16,
        style: AppTheme.appTheme.textTheme.bodyText1,
        underline: Container(
          height: 1,
          color: ThemeColors.BASE_BORDER_COLOR,
        ),
        onChanged: (String? newValue) {
          setState(() {
            this.widget.selectedDropdownItem = widget.listDropdownItems.firstWhere((item) => item.id == newValue);
            this.widget.onDropdownSelected!(this.widget.selectedDropdownItem);
          });
        },
        value: this.widget.selectedDropdownItem?.id,
        items: widget.listDropdownItems.map((item) {
          return DropdownMenuItem<String>(
            value: item.id,
            child: Text(item.label),
          );
        }).toList(),
      ),
    );
  }

}
