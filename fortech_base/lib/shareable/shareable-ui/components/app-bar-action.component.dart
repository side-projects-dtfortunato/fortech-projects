import 'package:flutter/cupertino.dart';
import 'package:fortechbase/shareable/shareable-ui/theme/app.theme.dart';

class AppBarActionComponent extends StatelessWidget {

  VoidCallback? callbackAction;
  String? label;
  Widget? icon;

  AppBarActionComponent({this.label, this.icon, this.callbackAction});

  Widget? _genIconAction() {
    return this.icon;
  }

  Widget _genLabelAction() {
    return Text(this.label!, style: AppTheme.appTheme.textTheme.bodyText2?.apply(color: ThemeColors.BASE_MAIN_COLOR), textAlign: TextAlign.center,);
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.center,
      margin: EdgeInsets.only(right: 10),
      child: GestureDetector(
          onTap: () {
            if (callbackAction != null) {
              callbackAction!();
            }
          },
          child: this.icon != null ? _genIconAction() : _genLabelAction()),
    );
  }

}