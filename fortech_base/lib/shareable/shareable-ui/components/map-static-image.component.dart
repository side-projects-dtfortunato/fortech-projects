import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fortechbase/shareable/shareable-ui/theme/app.theme.dart';

class MapStaticImageComponent extends StatelessWidget {
  // static final URL_REQUEST = "https://maps.googleapis.com/maps/api/staticmap?center=[LAT],[LNG]&zoom=15&size=[WIDTH]x[HEIGHT]&key=AIzaSyCAHrXg6PopizfOTRI6MEqWAqhje1S0RyQ";
  static final URL_REQUEST = "https://api.mapbox.com/styles/v1/mapbox/streets-v11/static/[LNG],[LAT],14,0,0/[WIDTH]x[HEIGHT]?access_token=[API_KEY]";

  String? lng;
  String? lat;
  int height;
  int width;
  String apiKey;

  late String _imageUrl;


  MapStaticImageComponent({this.lng, this.lat, this.height = 200, this.width = 300, this.apiKey = "pk.eyJ1IjoicGFvYXBvcnRhIiwiYSI6ImNrbGp0eXNxODM2MTYybnM4enV0ZmF0b2wifQ.9sIDn-xL9_h9LVfNLPDBRA"}) {
    this._imageUrl = URL_REQUEST;

    // Replace params
    this._imageUrl = this._imageUrl.replaceFirst("[LAT]", this.lat!);
    this._imageUrl = this._imageUrl.replaceFirst("[LNG]", this.lng!);
    this._imageUrl = this._imageUrl.replaceFirst("[WIDTH]", this.width.toString());
    this._imageUrl = this._imageUrl.replaceFirst("[HEIGHT]", this.height.toString());
    this._imageUrl = this._imageUrl.replaceFirst("[API_KEY]", this.apiKey);
  }

  Widget _genMapImage() {
    return CachedNetworkImage(imageUrl: this._imageUrl, cacheKey: this._imageUrl);
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Stack(
        alignment: Alignment.center,
        children: [
          _genMapImage(),
          Icon(Icons.home, color: ThemeColors.BASE_MAIN_COLOR, size: 30,)
        ],
      ),
    );
  }

}