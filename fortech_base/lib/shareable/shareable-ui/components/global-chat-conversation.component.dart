import 'dart:async';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:dash_chat/dash_chat.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fortechbase/shareable/shareable-api/chat-conversations.api.dart';
import 'package:fortechbase/shareable/shareable-api/shared-firebase-collection-names.api.dart';
import 'package:fortechbase/shareable/shareable-datamodel/chat-conversation.model.dart';
import 'package:fortechbase/shareable/shareable-managers/banner-handler.manager.dart';
import 'package:fortechbase/shareable/shareable-managers/base-central-user-data.manager.dart';
import 'package:fortechbase/shareable/shareable-managers/chat-data.manager.dart';
import 'package:fortechbase/shareable/shareable-ui/theme/app.theme.dart';
import 'package:fortechbase/shareable/utils/log.dart';

class GlobalChatConversationComponent extends StatefulWidget {
  String chatConversationId;
  String username;

  GlobalChatConversationComponent(this.chatConversationId, this.username);

  @override
  State<StatefulWidget> createState() => _State();
}

class _State extends State<GlobalChatConversationComponent> {
  bool isLoading = true;
  ChatConversationModel? _chatConversationModel;
  StreamSubscription? _subChatConversation;
  List<String>? listBlackListedUsers;

  @override
  void initState() {
    super.initState();

    this.listenChatConversation();
  }

  void listenChatConversation() async {
    
    this.listBlackListedUsers = await ChatDataManager().getListBlackListedUserIds();
    
    this._subChatConversation = FirebaseFirestore.instance.collection(SharedFirebaseCollectionNames.CHAT_CONVERSATIONS).doc(widget.chatConversationId).snapshots().listen((snapshot) {
      Log.d("ChatConversation - snapshot.exists " + (snapshot != null && snapshot.exists).toString());

      ChatConversationModel? chatConversationModel;

      if (snapshot.exists && snapshot.data() != null && snapshot.data() is Map<String, dynamic>) {
        chatConversationModel = ChatConversationModel.fromJson(snapshot.data()! as Map<String, dynamic>);
      }

      setState(() {
        this._chatConversationModel = chatConversationModel;
        this.isLoading = false;
      });
    });
  }

  @deprecated
  void loadChatConversation() {
    ChatConversationsAPI.getChatConversation(widget.chatConversationId).then((response) {
      setState(() {
        this._chatConversationModel = response;
        this.isLoading = false;
      });
    }).catchError((err) {
      setState(() {
        this.isLoading = false;
        BannerMessageHandlerManager().displayMessage(context, err.toString(), false);
      });
    });
  }

  Widget _genChatMessagesRoot() {
    List<ChatMessage> listMessages = [];

    if (this._chatConversationModel != null && this._chatConversationModel!.getSortedMessages().isNotEmpty) {
      this._chatConversationModel!.getSortedMessages().forEach((value) {
        
        if (!this.listBlackListedUsers!.contains(value.userId)) {
          String messageFinal = "@" + value.username! + ": " + value.message!;

          listMessages.add(ChatMessage(text: messageFinal,
            user: ChatUser(uid: value.userId, name: value.username), id: value.uid, createdAt: DateTime.fromMillisecondsSinceEpoch(value.timestamp!),),

          );
        }
        
      });
    }

    return DashChat(
        messages: listMessages,
        user: ChatUser(
          uid: BaseCentralUserDataManager().getUserID(),
          name: widget.username,
        ),
        onSend: (ChatMessage message) {
          if (message.text != null) {
            ChatConversationsAPI.sendChatMessage(context, widget.chatConversationId, widget.username, message.text!).then((apiResponse) {
              if (!apiResponse.isSuccess) {
                BannerMessageHandlerManager().handleAPIResponse(context, apiResponse);
              }
            }).catchError((err) => BannerMessageHandlerManager().displayMessage(context, err.toString(), false));
          } else {
            BannerMessageHandlerManager().displayMessage(context, "Invalid chat message. Please, only send normal characters", false);
          }
        },
        maxInputLength: 300,
        showInputCursor: true,
        inputMaxLines: 3,
        inputDecoration: InputDecoration.collapsed(hintText: "Add your message here..."),
        showUserAvatar: false,
        showAvatarForEveryMessage: false,
        inputTextStyle: AppTheme.appTheme.textTheme.bodyText1!,
        avatarBuilder: (user) => Container(),
        onLongPressMessage: (chatMessage) {
          // Display popup
          ChatDataManager().displayLongpressPopup(context, chatMessage.user.uid!, onUserBlocked: () async {
            this.listBlackListedUsers = await ChatDataManager().getListBlackListedUserIds();
            setState(() {
            });
          });
        },
    );
  }

  Widget _genRootContainer() {
    if (this.isLoading) {
      return Center(
        child: CircularProgressIndicator(),
      );
    } else {
      return _genChatMessagesRoot();
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: _genRootContainer(),
    );
  }

  @override
  void dispose() {
    if (this._subChatConversation != null) {
      this._subChatConversation!.cancel();
      this._subChatConversation = null;
    }

    super.dispose();
  }
}
