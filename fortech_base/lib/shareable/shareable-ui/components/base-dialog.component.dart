import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fortechbase/shareable/shareable-ui/theme/app.theme.dart';
import 'package:fortechbase/shareable/utils/ui-utils.dart';
import 'package:fortechbase/shareable/utils/utils.dart';

class BaseDialogComponent extends StatelessWidget {
  String? headerTitle;
  Widget? child;
  Widget? bottomChild;

  BaseDialogComponent({this.headerTitle, this.child, this.bottomChild});

  Widget _genHeader() {
    if (Utils.isStringNotEmpty(this.headerTitle)) {
      return Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        mainAxisSize: MainAxisSize.min,
        children: [
          Container(
            margin: EdgeInsets.symmetric(horizontal: 5),
            child: Text(this.headerTitle!, style: AppTheme.getSubtitle2Style(), textAlign: TextAlign.start,),
          ),
          Divider(height: 1, color: ThemeColors.COLOR_GRAY),
        ],
      );
    } else {
      return Container(height: 1,);
    }
  }

  @override
  Widget build(BuildContext context) {
    double width = UIUtils.getPopupWidth(context);

    return Dialog(
      backgroundColor: ThemeColors.BASE_BG_LIST_GREY_COLOR,
      child: Container(
        padding: EdgeInsets.all(10),
        width: width,
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            _genHeader(),
            this.child!,
            this.bottomChild != null ? this.bottomChild! : Container(height: 1,),
          ],
        ),
      ),
    );
  }
}