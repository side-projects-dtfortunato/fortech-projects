import 'package:flutter/material.dart';
import 'package:fortechbase/shareable/utils/platform.utils.dart';

const double MAX_WIDTH = 1024;

class AppScaffold extends Scaffold {
  AppScaffold({Key? key, PreferredSizeWidget? appBar, Widget? body, Widget? floatingActionButton, Widget? drawer, Widget? bottomNavigationBar, Widget? bottomSheet, bool extendBody = false, Color? backgroundColor, bgFullWidth: false})
      : super(
            key: key,
            appBar: appBar,
            body: Container(
              alignment: Alignment.center,
              child: PlatformUtils().isWeb() && !bgFullWidth
                  ? Container(
                      width: MAX_WIDTH,
                      child: body,
                    )
                  : body,
            ),
            floatingActionButton: floatingActionButton,
            drawer: drawer,
            bottomNavigationBar: bottomNavigationBar,
            bottomSheet: bottomSheet,
            extendBody: extendBody,
            backgroundColor: backgroundColor);
}
