import 'package:flutter/cupertino.dart';
import 'package:fortechbase/shareable/shareable-ui/theme/app.theme.dart';

class TagLabelItemComponent extends StatelessWidget {
  String label;
  Color labelColor;
  Color bgColor;

  TagLabelItemComponent(this.label, {this.labelColor = ThemeColors.COLOR_WHITE, this.bgColor = ThemeColors.COLOR_BLACK});


  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(5),
      decoration: BoxDecoration(
        color: this.bgColor,
        borderRadius: BorderRadius.circular(50),
        shape: BoxShape.rectangle
      ),
      child: Text(label, style: AppTheme.getBodyStyle().apply(color: labelColor), textAlign: TextAlign.center,),
    );
  }

}