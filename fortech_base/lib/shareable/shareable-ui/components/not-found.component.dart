import 'package:flutter/cupertino.dart';
import 'package:fortechbase/shareable/shareable-ui/theme/app.theme.dart';
import 'package:fortechbase/shareable/utils/resource.assets.dart';
import 'package:fortechbase/shareable/utils/resources.language.dart';

class NotFoundComponent extends StatelessWidget {

  String? message;
  String imagePath;
  Color? color;
  double? iconSize;

  NotFoundComponent({this.message, this.imagePath = ResourcesAssets.IC_NOT_FOUND, this.color, this.iconSize});

  Widget _genImage(BuildContext context) {
    if (imagePath != null) {
      return Image.asset(imagePath,
          width: this.iconSize != null ? this.iconSize! : 100,
          color: this.color,);
    } else {
      return Container();
    }
  }

  Widget _genMessage(BuildContext context) {
    // Check if has message, otherwise display a generic one
    if (this.message == null) {
      this.message = AppLocalizations.of(context)?.translate("generic_not_found");
    }

    return Text(
      this.message!,
      style: AppTheme.appTheme.textTheme.subtitle1!.apply(color: this.color),
      textAlign: TextAlign.center,
    );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(DefaultValues.CONTAINER_PADDING),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          _genImage(context),
          SizedBox(height: DefaultValues.BIG_MARGIN_TOP),
          _genMessage(context)
        ],
      ),
    );
  }
}