import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fortechbase/shareable/shareable-ui/components/removable-tag.component.dart';
import 'package:fortechbase/shareable/shareable-ui/decoration/app-field-input-decoration.style.dart';
import 'package:fortechbase/shareable/shareable-ui/theme/app.theme.dart';
import 'package:fortechbase/shareable/utils/utils.dart';
import 'package:getwidget/components/button/gf_icon_button.dart';
import 'package:getwidget/getwidget.dart';
import 'package:textfield_search/textfield_search.dart';

typedef Future<List<SuggestionSelectableItem>> OnGeneratingSuggestions(String insertedText);
typedef void OnSelectedSuggestion(SuggestionSelectableItem? suggestionSelectableItem);

class AppTextFieldSelectableSuggestionsComponent extends StatefulWidget {
  OnGeneratingSuggestions? onGeneratingSuggestions;
  OnSelectedSuggestion? onSelectedSuggestion;
  SuggestionSelectableItem? selectedSuggestion;
  String label;
  String hint;

  AppTextFieldSelectableSuggestionsComponent({this.onGeneratingSuggestions, this.label = "", this.hint = "", this.onSelectedSuggestion, this.selectedSuggestion});

  @override
  State<StatefulWidget> createState() => _State();
}

class _State extends State<AppTextFieldSelectableSuggestionsComponent> {
  List<SuggestionSelectableItem>? resultList;
  TextEditingController? myController = TextEditingController();
  String insertedText = "";

  Widget _genSelectedItem() {
    return RemovableTagComponent(
      this.widget.selectedSuggestion!.label!,
      Icons.cancel,
      onIconClicked: () {
        setState(() {
          this.widget.selectedSuggestion = null;
          if (this.widget.onSelectedSuggestion != null) {
            this.widget.onSelectedSuggestion!(null);
          }
        });
      },
    );
  }

  void onSelectedItem(SuggestionSelectableItem suggestionSelectableItem) {
    setState(() {
      this.widget.selectedSuggestion = suggestionSelectableItem;
      if (widget.onSelectedSuggestion != null) {
        widget.onSelectedSuggestion!(suggestionSelectableItem);
      }
      this.myController = null;
    });
  }

  Widget _genTextField() {
    if (this.myController == null) {
      this.myController = TextEditingController();
    }

    return Container(
      child: Row(
        mainAxisSize: MainAxisSize.min,
        children: [
          Flexible(child: TextFieldSearch(
            label: widget.label,
            controller: myController!,
            decoration: AppFieldInputDecorationStyle(labelText: widget.label, hintText: widget.hint, borderColor: ThemeColors.BASE_BORDER_COLOR),
            future: () async {
              setState(() {
                this.insertedText = this.myController!.text;
              });

              if (this.widget.onGeneratingSuggestions != null) {
                this.resultList = await this.widget.onGeneratingSuggestions!(this.insertedText);
              }

              return this.resultList;
            },
            getSelectedValue: (SuggestionSelectableItem item) {
              this.onSelectedItem(item);
            },
            initialList: [],
          ), flex: 4,),
          SizedBox(width: 8,),
          Flexible(child: GFIconButton(
            icon: Icon(
              Icons.add,
              size: 18,
            ),
            onPressed: Utils.isStringNotEmpty(this.insertedText) ? () {
              // Add custom skill
              this.onSelectedItem(SuggestionSelectableItem(this.insertedText.toLowerCase()));
            } : null,
            size: 18,
            color: ThemeColors.BASE_MAIN_COLOR,
            shape: GFIconButtonShape.circle,
            type: GFButtonType.outline,), flex: 1,),
        ],
      ),
    );
  }

  Widget _genRootContainer() {
    if (this.widget.selectedSuggestion != null) {
      return _genSelectedItem();
    } else {
      return _genTextField();
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: _genRootContainer(),
    );
  }
}

class SuggestionSelectableItem {
  String id;
  String? label;

  SuggestionSelectableItem(this.id, {this.label}) {
    if (this.label == null) {
      this.label = this.id;
    }
  }
}
