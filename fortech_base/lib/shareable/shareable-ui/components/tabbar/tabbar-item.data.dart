import 'package:flutter/cupertino.dart';

class TabbarItemData {
  String id;
  String label;
  Widget icon;

  TabbarItemData(this.id, this.label, this.icon);

}