import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fortechbase/shareable/shareable-ui/components/tabbar/tabbar-item.data.dart';
import 'package:fortechbase/shareable/shareable-ui/theme/app.theme.dart';
import 'package:fortechbase/shareable/utils/ui-utils.dart';

typedef OnTabbarChanged(TabbarItemData selectedTab);

class AppTabbarComponent extends StatefulWidget {
  List<TabbarItemData> listTabbarsData;
  OnTabbarChanged? onTabbarChanged;
  int defaultSelectedIndex;
  Color bgColor;
  Color labelColor;
  bool roundedTop;
  bool roundedBottom;

  AppTabbarComponent(this.listTabbarsData, {this.onTabbarChanged, this.defaultSelectedIndex = 0, this.bgColor = ThemeColors.COLOR_WHITE, this.labelColor = ThemeColors.TXT_BLACK_COLOR, this.roundedTop = true, this.roundedBottom = false});

  @override
  State<StatefulWidget> createState() => _State();
}

class _State extends State<AppTabbarComponent> {
  TabbarItemData? selectedTabbarItem;

  @override
  void initState() {
    super.initState();

    this.selectedTabbarItem = widget.listTabbarsData[widget.defaultSelectedIndex];

    this.onTabSelected(this.selectedTabbarItem!);
  }

  void onTabSelected(TabbarItemData tabbarItemData) {
    if (this.selectedTabbarItem!.id != tabbarItemData.id) {
      setState(() {
        this.selectedTabbarItem = tabbarItemData;
        if (widget.onTabbarChanged != null) {
          widget.onTabbarChanged!(tabbarItemData);
        }
      });
    }
  }

  Widget _genTabbarItem(TabbarItemData tabbarItemData) {
    bool isSelected = this.selectedTabbarItem!.id == tabbarItemData.id;

    if (isSelected) {
      return Container(
        padding: EdgeInsets.all(6),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            tabbarItemData.icon,
            SizedBox(height: 5,),
            Text(
              tabbarItemData.label,
              style: AppTheme.getBodyStyle().apply(color: widget.labelColor,fontSizeDelta: -4),
              textAlign: TextAlign.center,
            ),
            SizedBox(height: 5,),
            /*Divider(
              height: 4,
              color: widget.labelColor,
            )*/
          ],
        ),
      );
    } else {
      return Opacity(
        opacity: 0.4,
        child: Container(
          padding: EdgeInsets.all(6),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              tabbarItemData.icon,
              SizedBox(height: 3,),
              Text(
                tabbarItemData.label,
                style: AppTheme.getBodyStyle().apply(color: widget.labelColor,fontSizeDelta: -4),
                textAlign: TextAlign.center,
              )
            ],
          ),
        ),
      );
    }
  }

  Widget _genRootContainer() {
    List<Widget> listChilds = [];

    this.widget.listTabbarsData.forEach((element) {
      listChilds.add(Expanded(
        child: GestureDetector(
          onTap: () => onTabSelected(element),
          child: Container(
            child: _genTabbarItem(element)
            /* FIXME Expanded(
              child: _genTabbarItem(element),
            ),*/
          ),
        ),
      ));
    });

    return Container(
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: listChilds,
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: widget.bgColor,
        borderRadius: BorderRadius.vertical(top: widget.roundedTop ? Radius.circular(50) : Radius.zero, bottom: widget.roundedBottom ? Radius.circular(50) : Radius.zero),
        boxShadow: [BoxShadow(
          color: Colors.grey.withOpacity(0.5),
          spreadRadius: 5,
          blurRadius: 7,
          offset: Offset(0, 3), // changes position of shadow
        )]
      ),
      child: _genRootContainer(),
    );
  }
}
