import 'package:flutter/material.dart';
import 'package:fortechbase/shareable/shareable-ui/theme/app.theme.dart';
import 'package:fortechbase/shareable/utils/resource.assets.dart';

class CustomAppBar extends AppBar {
  late String pageTitle;

  CustomAppBar({List<Widget>? actions}) : super(
      title: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          Container(
            padding: EdgeInsets.all(4),
            child: Image.asset(
                ResourcesAssets.IMAGE_LOGO_NAME, fit: BoxFit.fitWidth, height: 25),
          )

        ],
      ),
      centerTitle: false, actions: actions);

  CustomAppBar.withTitle(this.pageTitle, {List<Widget>? actions}) : super(
      title: Text(pageTitle),
      actions: actions,
      centerTitle: true);

}