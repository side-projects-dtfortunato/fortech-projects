import 'package:flutter/cupertino.dart';
import 'package:fortechbase/shareable/shareable-ui/theme/app.theme.dart';
import 'package:fortechbase/shareable/utils/resource.assets.dart';
import 'package:fortechbase/shareable/utils/utils.dart';

class CircularImageComponent extends StatelessWidget {
  final double size;
  String? imageUrl;
  String? imageFile;
  final String placeholderUrl;
  Color? borderColor;

  CircularImageComponent({this.size = 50, this.imageUrl, this.placeholderUrl = ResourcesAssets.IMAGE_IC_USER_PLACEHOLDER, this.borderColor}) {
    if (this.borderColor == null) {
      this.borderColor = ThemeColors.BASE_BORDER_COLOR;
    }
  }

  CircularImageComponent.fromFile({this.size = 50, this.imageFile, this.placeholderUrl = ResourcesAssets.IMAGE_IC_USER_PLACEHOLDER, this.borderColor}) {
    if (this.borderColor == null) {
      this.borderColor = ThemeColors.BASE_BORDER_COLOR;
    }
  }

  Widget _getImageWidget() {
    if (Utils.isStringNotEmpty(this.imageUrl)) {
      return ClipOval(child: FadeInImage.assetNetwork(fit: BoxFit.cover, placeholder: placeholderUrl, image: imageUrl!));
    } else if (this.imageFile != null) {
      return ClipOval(child: Image.asset(this.imageFile!, fit: BoxFit.cover));
    } else {
      return ClipOval(child: Image.asset(placeholderUrl, fit: BoxFit.cover));
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      width: size,
      height: size,
      decoration: BoxDecoration(
        color: Color(0x00FFFFFF),
        borderRadius: new BorderRadius.all(new Radius.circular(size / 2)),
        border: new Border.all(
          color: ThemeColors.BASE_BORDER_COLOR,
          width: 1.0,
        ),
      ),
      child: _getImageWidget(),
    );
  }
}
