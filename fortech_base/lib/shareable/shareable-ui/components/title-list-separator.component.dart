import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fortechbase/shareable/shareable-ui/theme/app.theme.dart';


class TitleListSeparatorComponent extends StatelessWidget {

  // Data
  String? title;
  String? subtitle;
  bool hasBottomDivider;
  TextStyle? fontStyle;
  bool isCentered;

  TitleListSeparatorComponent({this.title, this.subtitle, this.hasBottomDivider = true, this.fontStyle, this.isCentered = false});

  TextStyle getFontStyle() {
    return this.fontStyle != null ? fontStyle! : AppTheme.appTheme.textTheme.headline1!.apply(color: ThemeColors.BASE_MAIN_COLOR);
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(top: DefaultValues.LIST_MARGINS, bottom: DefaultValues.LIST_MARGIN_BETWEEN),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          this.title != null ? Container(
            margin: EdgeInsets.only(bottom: DefaultValues.LIST_MARGINS),
            child: Text(this.title!, style: getFontStyle(), textAlign: this.isCentered ? TextAlign.center : TextAlign.start),
          ) : Container(),
          this.subtitle != null ? Text(this.subtitle!, style: AppTheme.appTheme.textTheme.subtitle2, textAlign: this.isCentered ? TextAlign.center : TextAlign.start) : Container(),
          hasBottomDivider ? Divider(height: 2, color: ThemeColors.COLOR_WHITE,) : Container()
        ],
      ),
    );
  }
}