import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fortechbase/shareable/shareable-ui/decoration/app-field-input-decoration.style.dart';
import 'package:fortechbase/shareable/shareable-ui/theme/app.theme.dart';

typedef OnValueChanged(String value);
typedef String OnValueValidator(String? value);

class AppTextFieldComponent extends StatelessWidget {

  OnValueChanged? onValueChanged;
  OnValueValidator? onValueValidator;
  int maxLength;
  int maxLines;
  String defaultValue;
  String labelText;
  String hintText;
  TextInputType textInputType;
  bool isEditable;
  TextEditingController? controller;
  Color? borderColor;
  bool? autoFocus;

  AppTextFieldComponent({this.textInputType = TextInputType.text, this.maxLength = 100, this.maxLines = 1, this.onValueChanged, this.onValueValidator, this.defaultValue = "", this.labelText = "", this.hintText = "", this.isEditable = true, this.controller, this.borderColor, this.autoFocus = false});

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      autofocus: this.autoFocus!,
      onChanged: this.onValueChanged,
      decoration: AppFieldInputDecorationStyle(labelText: labelText,
          hintText: hintText != null ? hintText : "",
          borderColor: this.borderColor != null ? this.borderColor : ThemeColors.BASE_BORDER_COLOR),
      validator: onValueValidator,
      maxLength: this.maxLength,
      maxLines: this.maxLines,
      keyboardType: textInputType,
      style: AppTheme.appTheme.textTheme.bodyText1,
      controller: this.controller,
      initialValue: this.controller != null ? null : this.defaultValue,
      enabled: this.isEditable,
    );
  }

}