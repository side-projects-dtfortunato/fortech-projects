import 'package:flutter/cupertino.dart';
import 'package:google_mobile_ads/google_mobile_ads.dart';

class AdmobBannerComponent extends StatefulWidget {
  String adId;
  AdSize adSize;
  List<String>? keywords;


  AdmobBannerComponent(this.adId, {this.adSize = AdSize.banner, this.keywords});

  @override
  State<StatefulWidget> createState() => _State();
}

class _State extends State<AdmobBannerComponent> {


  @override
  Widget build(BuildContext context) {
    AdWidget adWidget = AdWidget(ad: BannerAd(
      adUnitId: widget.adId,
      size: widget.adSize,
      request: AdRequest(keywords: widget.keywords),
      listener: BannerAdListener(),
    ));
    adWidget.ad.load();

    return adWidget;
  }
}