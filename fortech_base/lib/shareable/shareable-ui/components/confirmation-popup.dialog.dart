import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fortechbase/shareable/shareable-ui/components/app-button.component.dart';
import 'package:fortechbase/shareable/shareable-ui/theme/app.theme.dart';
import 'package:fortechbase/shareable/utils/resources.language.dart';
import 'package:fortechbase/shareable/utils/ui-utils.dart';

typedef OnPositiveClick = void Function();
typedef OnNegativeClick = void Function();

class ConfirmationPopupDialog extends StatelessWidget
{
  String _message;
  String? _labelOk;
  String? _labelCancel;
  OnPositiveClick _onPositiveClickListener;
  OnNegativeClick _onNegativeClickListener;

  ConfirmationPopupDialog(this._message, this._onPositiveClickListener, this._onNegativeClickListener);
  ConfirmationPopupDialog.customBtns(this._message,
      this._labelOk,
      this._labelCancel,
      this._onPositiveClickListener,
      this._onNegativeClickListener);

  void _onPositiveClick(BuildContext context)
  {
    Navigator.pop(context);
    this._onPositiveClickListener();
  }

  void _onNegativeClick(BuildContext context)
  {
    Navigator.pop(context);
    this._onNegativeClickListener();
  }

  @override
  Widget build(BuildContext context) {
    double width = UIUtils.getPopupWidth(context);

    return Dialog(
      backgroundColor: ThemeColors.BASE_BG_LIST_GREY_COLOR,
        child: Container(
            width: width,
            margin: EdgeInsets.all(DefaultValues.CONTAINER_PADDING),
            decoration: UIUtils.getRoundedBackgroundBoxDecoration(
              ThemeColors.BASE_BG_LIST_GREY_COLOR,
              ThemeColors.BASE_BG_LIST_GREY_COLOR,
              null
            ),
            child: ClipRRect(
              child: Container(
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    Text(_message, style: TextStyle(color: ThemeColors.TXT_BLACK_COLOR, fontSize: 15), textAlign: TextAlign.center,),
                    Container(
                      margin: EdgeInsets.all(8),
                      child: Wrap(
                        alignment: WrapAlignment.end,
                        children: <Widget>[
                          // Negative Button
                          AppButtonComponent(
                              color: ThemeColors.STATE_FAILED_NORMAL,
                              buttonStyle: AppButtonStyle.FILLED,
                              onPressed: () => _onNegativeClick(context),
                              text: this._labelCancel != null ? this._labelCancel! : AppLocalizations.of(context)!.translate("generic_no")!,
                          ),
                          SizedBox(width: 10,),
                          // Positive Button
                          AppButtonComponent(
                            color: ThemeColors.STATE_SUCCESS_NORMAL,
                            buttonStyle: AppButtonStyle.FILLED,
                            onPressed: () => _onPositiveClick(context),
                            text: this._labelOk != null ? this._labelOk! : AppLocalizations.of(context)!.translate("generic_yes")!,
                          )
                        ],
                      )
                    )
                  ],
                ),
              ),
            ))
    );
  }

  static void showAsDialog(BuildContext context, String message, OnPositiveClick onPositiveClickListener,
  OnNegativeClick onNegativeClickListener) {
    showDialog(
        context: context,
        builder: (ctx) => Center(child: ConfirmationPopupDialog(message, onPositiveClickListener, onNegativeClickListener)));
  }
  static void showAsDialogCustomBtn(BuildContext context, String message, String labelOk, String labelCancel, OnPositiveClick onPositiveClickListener,
  OnNegativeClick onNegativeClickListener) {
    showDialog(
        barrierDismissible: false,
        context: context,
        builder: (ctx) => Center(child: ConfirmationPopupDialog.customBtns(message, labelOk, labelCancel, onPositiveClickListener, onNegativeClickListener)));
  }
}