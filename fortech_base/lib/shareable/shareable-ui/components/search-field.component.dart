import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fortechbase/shareable/shareable-ui/theme/app.theme.dart';
import 'package:fortechbase/shareable/utils/utils.dart';
import 'package:fsearch/fsearch.dart';

typedef OnTextChanged(String text);

class SearchFieldComponent extends StatefulWidget {
  OnTextChanged _onTextChanged;
  List<String>? listHintSuggestions;
  String? defaultSearchQuery;


  SearchFieldComponent(this._onTextChanged, {this.listHintSuggestions, this.defaultSearchQuery});

  @override
  State<StatefulWidget> createState() => _State();

}

class _State extends State<SearchFieldComponent> {
  FSearchController _fSearchController = FSearchController();

  @override
  void initState() {
    super.initState();



    this._fSearchController.setListener(() {
      widget._onTextChanged(this._fSearchController.text);
    });

    WidgetsBinding.instance!
        .addPostFrameCallback((_) {
      if (Utils.isStringNotEmpty(widget.defaultSearchQuery)) {
        _fSearchController.text = widget.defaultSearchQuery;
      }
    });
  }


  @override
  Widget build(BuildContext context) {
    return FSearch(
      controller: _fSearchController,
      cornerStyle: FSearchCornerStyle.round,
      hints: widget.listHintSuggestions,
      hintSwitchEnable: widget.listHintSuggestions?.isNotEmpty,
      hintSwitchType: FSearchAnimationType.Scroll,
      backgroundColor: ThemeColors.COLOR_WHITE,
      strokeColor: ThemeColors.COLOR_GRAY,
      prefixes: [
        const SizedBox(width: 6.0),
        Icon(
          Icons.search,
          size: 20,
          color: ThemeColors.COLOR_GRAY,
        ),
        const SizedBox(width: 5.0)
      ],
      style: TextStyle(fontSize: 20.0, height: 1.0, color: ThemeColors.TXT_BLACK_COLOR),
      padding: EdgeInsets.only(left: 10, right: 10, top: 5, bottom: 5),
      corner: FSearchCorner.all(50),
      height: 40,


    );
  }

}