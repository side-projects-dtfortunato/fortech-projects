import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fortechbase/shareable/shareable-ui/theme/app.theme.dart';

class RemovableTagComponent extends StatelessWidget {
  String label;
  IconData icon;
  VoidCallback? onIconClicked;
  Color textColor;
  Color backgroundColor;

  RemovableTagComponent(this.label, this.icon, {this.onIconClicked, this.textColor = ThemeColors.COLOR_WHITE, this.backgroundColor = ThemeColors.BASE_MAIN_COLOR_LIGHT1});

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(8),
      decoration: BoxDecoration(shape: BoxShape.rectangle, color: backgroundColor),
      child: Row(
        children: [
          GestureDetector(
            child: Icon(icon, color: textColor, size: 20),
            onTap: onIconClicked,
          ),
          SizedBox(
            width: 8,
          ),
          Text(
            this.label,
            style: AppTheme.appTheme.textTheme.bodyText2!.apply(color: textColor),
          )
        ],
      ),
    );
  }
}
