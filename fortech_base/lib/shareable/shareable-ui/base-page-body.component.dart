import 'package:flutter/cupertino.dart';
import 'package:fortechbase/shareable/utils/ui-utils.dart';

class BasePageBodyComponent extends StatefulWidget {
  Widget? mobileBody;
  Widget? landscapeBody;


  BasePageBodyComponent({this.mobileBody, this.landscapeBody});

  @override
  State<StatefulWidget> createState() => _State();

}


class _State extends State<BasePageBodyComponent> {

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {

    if (widget.landscapeBody != null && (UIUtils.getWidthScreen(context) >= 800 || UIUtils.getWidthScreen(context) > UIUtils.getHeightScreen(context))) {
      return widget.landscapeBody!;
    } else if (widget.mobileBody != null){
      return widget.mobileBody!;
    } else {
      return Container();
    }
  }

}