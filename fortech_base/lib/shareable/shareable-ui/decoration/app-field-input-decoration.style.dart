import 'package:flutter/material.dart';
import 'package:fortechbase/shareable/shareable-ui/theme/app.theme.dart';

class AppFieldInputDecorationStyle extends InputDecoration {
  AppFieldInputDecorationStyle({prefixIcon, labelText, hintText, borderColor, suffixIcon})
      : super(
            prefixIcon: prefixIcon,
            suffixIcon: suffixIcon,
            labelText: labelText,
            hintText: hintText,
            errorStyle: CustomStyles.ERROR_TEXT_STYLE,
            fillColor: borderColor,
            focusColor: borderColor,
            hintStyle: CustomStyles.TEXT_FIELD_STYLE.apply(color: ThemeColors.COLOR_GRAY),
            labelStyle: CustomStyles.TEXT_FIELD_STYLE.apply(color: ThemeColors.BASE_BORDER_NO_FOCUS_COLOR),
            counterStyle: TextStyle(color: Color(0xFFFFFFF), fontSize: 12),
            focusedBorder: OutlineInputBorder(
              borderSide: BorderSide(color: ThemeColors.BASE_BORDER_COLOR, width: 1.0),
            ),
            enabledBorder: OutlineInputBorder(
              borderSide: BorderSide(color: ThemeColors.BASE_BORDER_NO_FOCUS_COLOR, width: 1.0),
            ),
            disabledBorder: OutlineInputBorder(
              borderSide: BorderSide(color: ThemeColors.BASE_BORDER_DISABLE_COLOR, width: 1.0),
            ),
            contentPadding: EdgeInsets.symmetric(vertical: 12, horizontal: 10),
            isCollapsed: true);

  AppFieldInputDecorationStyle.underline({prefixIcon, labelText, hintText, borderColor})
      : super(
            prefixIcon: prefixIcon,
            labelText: labelText,
            hintText: hintText,
            errorStyle: CustomStyles.ERROR_TEXT_STYLE,
            fillColor: borderColor,
            hintStyle: CustomStyles.TEXT_FIELD_STYLE.apply(color: ThemeColors.COLOR_GRAY),
            labelStyle: CustomStyles.TEXT_FIELD_STYLE.apply(color: borderColor),
            counterStyle: TextStyle(color: Color(0xFFFFFFF), fontSize: 12),
            focusedBorder: UnderlineInputBorder(
              borderSide: BorderSide(color: ThemeColors.BASE_BORDER_COLOR, width: 1.0),
            ),
            enabledBorder: UnderlineInputBorder(
              borderSide: BorderSide(color: ThemeColors.BASE_BORDER_COLOR, width: 1.0),
            ),
            disabledBorder: UnderlineInputBorder(
              borderSide: BorderSide(color: ThemeColors.BASE_BORDER_DISABLE_COLOR, width: 1.0),
            ),
            contentPadding: EdgeInsets.symmetric(vertical: 5, horizontal: 8),
            isCollapsed: true);
}
