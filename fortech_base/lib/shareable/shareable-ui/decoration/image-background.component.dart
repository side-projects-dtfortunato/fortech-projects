import 'package:flutter/cupertino.dart';
import 'package:fortechbase/shareable/shareable-ui/theme/app.theme.dart';

class ImageBackgroundDecorationWidget extends DecorationImage {

  ImageBackgroundDecorationWidget({imageUrl}):super(fit: BoxFit.cover,
      colorFilter: new ColorFilter.mode(
          ThemeColors.COLOR_BLACK.withOpacity(0.6), BlendMode.darken),
      image: NetworkImage(imageUrl));

}