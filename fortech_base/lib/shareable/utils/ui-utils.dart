import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fortechbase/shareable/shareable-ui/decoration/image-background.component.dart';
import 'package:fortechbase/shareable/shareable-ui/theme/app.theme.dart';

class UIUtils {
  static RoundedRectangleBorder getRoundedButtonShape(Color borderColor) {
    return RoundedRectangleBorder(borderRadius: new BorderRadius.circular(18.0), side: BorderSide(color: borderColor));
  }

  static BoxDecoration getDefaultRoundedBackgroundBoxDecoration() {
    return getRoundedBackgroundBoxDecoration(null, null, null);
  }

  static BoxDecoration getRoundedBackgroundBoxDecoration(Color? bgColor, Color? borderColor, String? bgImageUrl) {
    return BoxDecoration(
        // Background with Game Image
        image: bgImageUrl != null ? ImageBackgroundDecorationWidget(imageUrl: bgImageUrl) : null,
        color: bgColor != null ? bgColor : ThemeColors.BASE_MAIN_COLOR_LIGHT2,
        border: Border.all(color: borderColor != null ? borderColor : ThemeColors.BASE_BORDER_COLOR, width: 2, style: BorderStyle.solid),
        borderRadius: BorderRadius.circular(DefaultValues.BORDER_RADIUS));
  }

  static BoxDecoration getNotRoundedBackgroundBoxDecoration(Color bgColor, Color borderColor, String bgImageUrl) {
    return BoxDecoration(
        // Background with Game Image
        image: bgImageUrl != null ? ImageBackgroundDecorationWidget(imageUrl: bgImageUrl) : null,
        color: bgColor != null ? bgColor : ThemeColors.BASE_MAIN_COLOR_LIGHT1,
        border: Border.all(color: borderColor != null ? borderColor : ThemeColors.BASE_BORDER_COLOR, width: 2, style: BorderStyle.solid));
  }

  static double getWidthScreen(BuildContext context) {
    return MediaQuery.of(context).size.width;
  }

  static double getHeightScreen(BuildContext context) {
    return MediaQuery.of(context).size.height;
  }

  static double getPopupWidth(BuildContext context) {
    double width = UIUtils.getWidthScreen(context) * 0.9;

    if (width > 500) {
      width = 400;
    }
    return width;
  }

  static double getPopupHeight(BuildContext context) {
    double height = UIUtils.getHeightScreen(context) * 0.7;

    if (height > 400) {
      height = 500;
    }
    return height;
  }

  static BoxShadow generateShadow() {
    return BoxShadow(
      color: Colors.grey.withOpacity(0.5),
      spreadRadius: 5,
      blurRadius: 7,
      offset: Offset(0, 3), // changes position of shadow
    );
  }
}
