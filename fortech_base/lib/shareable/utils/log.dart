import 'package:quick_log/quick_log.dart';

class Log {
  static Logger _logger = Logger("Fortuly.apps");

  static Logger getLogger() {
    return _logger;
  }

  static void d(dynamic message) {
    getLogger().debug(message.toString());
  }

  static void e(dynamic error) {
    getLogger().error(error.toString());
  }

  static void w(dynamic warning) {
    getLogger().warning(warning.toString());
  }

  static void i(dynamic info) {
    getLogger().info(info.toString());
  }


}