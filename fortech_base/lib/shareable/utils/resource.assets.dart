class ResourcesAssets {
  static const String ASSETS_ROOT_PATH = 'assets/images/';
  // IMAGES
  static const String IMAGE_LOGO_ICON_NAME =
      ASSETS_ROOT_PATH + 'logo_icon_name.png';
  static const String IMAGE_LOGO_ICON_NAME_WHITE =
      ASSETS_ROOT_PATH + 'logo_icon_name_white.png';
  static const String IMAGE_LOGO_NAME =
      ASSETS_ROOT_PATH + 'logo_name.png';
  static const String IMAGE_LOGO_NAME_WHITE =
      ASSETS_ROOT_PATH + 'logo_name_white.png';
  static const String IC_LOGO =
      ASSETS_ROOT_PATH + 'ic_logo.png';
  static const String IC_LOGO_BG =
      ASSETS_ROOT_PATH + 'ic_logo_bg.png';
  static const String IC_REVIEWING_ILLUSTRATION =
      ASSETS_ROOT_PATH + 'ic_reviewing_illustration.png';
  static const String IC_NOT_FOUND =
      ASSETS_ROOT_PATH + 'ic_not_found.png';
  static const String IMG_LOGIN_BG =
      ASSETS_ROOT_PATH + 'img_login_bg.jpg';
  static const String IC_SUBMISSION_SUCCESS =
      ASSETS_ROOT_PATH + 'ic_submission_success.png';
  static const String IMAGE_IC_USER_PLACEHOLDER = ASSETS_ROOT_PATH + 'ic_user_placeholder.png';
  static const String IC_SUCCESS_DEAL = ASSETS_ROOT_PATH + 'ic_sucess_deal.png';
  static const String IC_WEEK = ASSETS_ROOT_PATH + 'ic_week.png';

  static const String IC_PAYMENT_MB = ASSETS_ROOT_PATH + 'ic_payment_mb.png';
  static const String IC_PAYMENT_VISA = ASSETS_ROOT_PATH + 'ic_payment_visa.png';
  static const String IC_WEEK_PLAN = ASSETS_ROOT_PATH + 'ic_week_plan.png';
  static const String IC_ORDER = ASSETS_ROOT_PATH + 'ic_order.png';
  static const String IC_DELIVERY = ASSETS_ROOT_PATH + 'ic_delivery.png';
  static const String IC_HOME_ADDRESS = ASSETS_ROOT_PATH + 'ic_home_address.png';




}
