import 'dart:io';

enum PlatformTypes { ANDROID, IOS, WEB, UNKNOWN }

class PlatformUtils {
  static final PlatformUtils _instance = PlatformUtils._internal();

  factory PlatformUtils() {
    return _instance;
  }

  PlatformUtils._internal() {
    try {
      if (Platform.isAndroid) {
        this._platformType = PlatformTypes.ANDROID;
      } else if (Platform.isIOS) {
        this._platformType = PlatformTypes.IOS;
      } else {
        this._platformType = PlatformTypes.WEB;
      }
    } catch (e) {
      // When something wrong happen with Platform, means it is web
      this._platformType = PlatformTypes.WEB;
    }
  }

  // Data
  PlatformTypes _platformType = PlatformTypes.UNKNOWN;

  PlatformTypes get platformType => _platformType;

  bool isIOS() {
    return this._platformType == PlatformTypes.IOS;
  }

  bool isAndroid() {
    return this._platformType == PlatformTypes.ANDROID;
  }

  bool isWeb() {
    return this._platformType == PlatformTypes.WEB;
  }

  String getBaseUrl() {
    if (isWeb()) {
      return Uri.base.host == "localhost" ? Uri.base.host + ":" + Uri.base.port.toString() : Uri.base.host;
    } else {
      return "web.keepmybag.app";
    }
  }
}
