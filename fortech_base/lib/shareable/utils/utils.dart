import 'dart:math';

import 'package:ars_progress_dialog/ars_progress_dialog.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:fortechbase/shareable/shareable-api/model/shared-types.model.dart';
import 'package:fortechbase/shareable/shareable-ui/theme/app.theme.dart';
import 'package:fortechbase/shareable/utils/resources.language.dart';
import 'package:image_picker/image_picker.dart';
import 'package:intl/intl.dart';


class Utils {

  static calculateAge(DateTime birthDate) {
    DateTime currentDate = DateTime.now();
    int age = currentDate.year - birthDate.year;
    int month1 = currentDate.month;
    int month2 = birthDate.month;
    if (month2 > month1) {
      age--;
    } else if (month1 == month2) {
      int day1 = currentDate.day;
      int day2 = birthDate.day;
      if (day2 > day1) {
        age--;
      }
    }
    return age;
  }

  static String getTimeWithZero(int time) {
    return (time < 10 ? "0" : "") + time.toString();
  }

  static String? formFieldValidatorNotEmpty(BuildContext context, String? value) {
    if (value != null && value.isEmpty) {
      return AppLocalizations.of(context)?.translate("error_field_not_empty");
    } else {
      return null;
    }
  }

  static Future<PickedFile?> pickImage() async {
    ImagePicker imagePicker = ImagePicker();
    return await imagePicker.getImage(source: ImageSource.gallery, maxWidth: 1000, maxHeight: 1000, imageQuality: 50);
  }

  static Future<PickedFile?> takePhoto(BuildContext context) async {
    ImagePicker imagePicker = ImagePicker();
    return await imagePicker.getImage(source: ImageSource.camera, maxWidth: 1000, maxHeight: 1000, imageQuality: 50);
  }

  static String enumToString(dynamic enumType) {
    if (enumType != null) {
      return enumType.toString().split('.').last;
    } else {
      return "";
    }

  }

  static ArsProgressDialog getCustomProgressDialog(BuildContext context) {

    return ArsProgressDialog(
        context,
        blur: 2,
        backgroundColor: ThemeColors.COLOR_TRANSPARENT,
        animationDuration: Duration(milliseconds: 500));
    /*return ProgressDialog(context,
        isDismissible: false,
        customBody: Container(
              color: ThemeColors.BASE_MAIN_COLOR_DARK,
          padding: EdgeInsets.all(8),
          child: Image.asset(circularProgressIndicator, scale: 20),
        ));*/
  }

  static T getEnumFromString<T>(Iterable<T> values, String value) {
    return values.firstWhere((type) => type.toString().split(".").last == value);
  }

  static bool isEmailValid(String? email)
  {
    if (email != null)
    {
      return RegExp(r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+").hasMatch(email);
    } else {
      return false;
    }
  }

  static bool isStringMinLength(String str, int minLength) {
    return str != null && str.length >= minLength;
  }

  static bool isStringNotEmpty(String? str) {
    if (str != null) {
      return isStringMinLength(str, 1);
    } else {
      return false;
    }

  }

  static int randomNumber(int min, int max) {
    Random random = Random();

    return min + random.nextInt(max - min);
  }

  static bool isNumeric(String s) {
    if(s == null) {
      return false;
    }
    return double.parse(s, (e) => null!) != null;
  }

  static WeekDay? getTodayWeekday() {
    switch(DateTime.now().weekday) {
      case DateTime.monday:
        return WeekDay.MONDAY;
      case DateTime.tuesday:
        return WeekDay.TUESDAY;
      case DateTime.wednesday:
        return WeekDay.WEDNESDAY;
      case DateTime.thursday:
        return WeekDay.THURSDAY;
      case DateTime.friday:
        return WeekDay.FRIDAY;
      case DateTime.saturday:
        return WeekDay.SATURDAY;
      case DateTime.sunday:
        return WeekDay.SUNDAY;
    }
    return null;
  }

  static String getTodayStr() {
    return DateFormat('dd-MM-yyyy').format(DateTime.now());
  }

  static int convertWeekdayToInt(WeekDay weekDay) {
    switch(weekDay) {
      case WeekDay.MONDAY: return 0;
      case WeekDay.TUESDAY: return 1;
      case WeekDay.WEDNESDAY: return 2;
      case WeekDay.THURSDAY: return 3;
      case WeekDay.FRIDAY: return 4;
      case WeekDay.SATURDAY: return 5;
      case WeekDay.SUNDAY: return 6;
    }
  }

  static List<WeekDay> getListWeekdaysSorted() {
    List<WeekDay> listWeekdays = WeekDay.values.toList();

    listWeekdays.sort((wd1, wd2) {
      return convertWeekdayToInt(wd1).compareTo(convertWeekdayToInt(wd2));
    });

    return listWeekdays;
  }

  static void forceLandscape() {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.landscapeRight,
      DeviceOrientation.landscapeLeft,
    ]);
  }

  static void resetScreenOrientation() {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.landscapeRight,
      DeviceOrientation.landscapeLeft,
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);
  }

  static bool isLinkValid(String? link) {
    return isStringNotEmpty(link) ? Uri.parse(link!).isAbsolute : false;
  }


}
