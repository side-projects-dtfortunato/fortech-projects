// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'app-configs.model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

AppConfigsData _$AppConfigsDataFromJson(Map<String, dynamic> json) =>
    AppConfigsData()
      ..initialized = json['initialized'] as bool?
      ..store = json['store'] == null
          ? null
          : AppStoreLinks.fromJson(json['store'] as Map<String, dynamic>)
      ..version = json['version'] == null
          ? null
          : AppVersionConfigs.fromJson(json['version'] as Map<String, dynamic>)
      ..startupMessage = json['startupMessage'] == null
          ? null
          : AppStartupMessage.fromJson(
              json['startupMessage'] as Map<String, dynamic>);

Map<String, dynamic> _$AppConfigsDataToJson(AppConfigsData instance) =>
    <String, dynamic>{
      'initialized': instance.initialized,
      'store': instance.store,
      'version': instance.version,
      'startupMessage': instance.startupMessage,
    };

AppStoreLinks _$AppStoreLinksFromJson(Map<String, dynamic> json) =>
    AppStoreLinks()
      ..androidLink = json['androidLink'] as String?
      ..iosLink = json['iosLink'] as String?;

Map<String, dynamic> _$AppStoreLinksToJson(AppStoreLinks instance) =>
    <String, dynamic>{
      'androidLink': instance.androidLink,
      'iosLink': instance.iosLink,
    };

AppVersionConfigs _$AppVersionConfigsFromJson(Map<String, dynamic> json) =>
    AppVersionConfigs()
      ..isMaintenance = json['isMaintenance'] as bool?
      ..minVersion = json['minVersion'] as int?
      ..currentVersion = json['currentVersion'] as int?
      ..recommendVersion = json['recommendVersion'] as int?
      ..iosReviewingVersion = json['iosReviewingVersion'] as int?
      ..androidReviewingVersion = json['androidReviewingVersion'] as int?;

Map<String, dynamic> _$AppVersionConfigsToJson(AppVersionConfigs instance) =>
    <String, dynamic>{
      'isMaintenance': instance.isMaintenance,
      'minVersion': instance.minVersion,
      'currentVersion': instance.currentVersion,
      'recommendVersion': instance.recommendVersion,
      'iosReviewingVersion': instance.iosReviewingVersion,
      'androidReviewingVersion': instance.androidReviewingVersion,
    };

AppStartupMessage _$AppStartupMessageFromJson(Map<String, dynamic> json) =>
    AppStartupMessage()
      ..display = json['display'] as bool
      ..message = json['message'] as String
      ..yesLink = json['yesLink'] as String;

Map<String, dynamic> _$AppStartupMessageToJson(AppStartupMessage instance) =>
    <String, dynamic>{
      'display': instance.display,
      'message': instance.message,
      'yesLink': instance.yesLink,
    };
