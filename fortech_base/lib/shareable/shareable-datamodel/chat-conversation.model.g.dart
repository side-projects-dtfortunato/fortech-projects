// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'chat-conversation.model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ChatConversationModel _$ChatConversationModelFromJson(
        Map<String, dynamic> json) =>
    ChatConversationModel()
      ..uid = json['uid'] as String?
      ..listMessages = (json['listMessages'] as Map<String, dynamic>?)?.map(
        (k, e) =>
            MapEntry(k, ChatMessageModel.fromJson(e as Map<String, dynamic>)),
      )
      ..lastMessage = json['lastMessage'] == null
          ? null
          : ChatMessageModel.fromJson(
              json['lastMessage'] as Map<String, dynamic>);

Map<String, dynamic> _$ChatConversationModelToJson(
        ChatConversationModel instance) =>
    <String, dynamic>{
      'uid': instance.uid,
      'listMessages':
          instance.listMessages?.map((k, e) => MapEntry(k, e?.toJson())),
      'lastMessage': instance.lastMessage?.toJson(),
    };

ChatMessageModel _$ChatMessageModelFromJson(Map<String, dynamic> json) =>
    ChatMessageModel()
      ..uid = json['uid'] as String?
      ..userId = json['userId'] as String?
      ..conversationId = json['conversationId'] as String?
      ..timestamp = json['timestamp'] as int?
      ..username = json['username'] as String?
      ..message = json['message'] as String?;

Map<String, dynamic> _$ChatMessageModelToJson(ChatMessageModel instance) =>
    <String, dynamic>{
      'uid': instance.uid,
      'userId': instance.userId,
      'conversationId': instance.conversationId,
      'timestamp': instance.timestamp,
      'username': instance.username,
      'message': instance.message,
    };
