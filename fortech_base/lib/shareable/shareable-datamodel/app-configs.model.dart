import 'package:fortechbase/shareable/utils/platform.utils.dart';
import 'package:json_annotation/json_annotation.dart';

part 'app-configs.model.g.dart';

@JsonSerializable()
class AppConfigsData {
  late bool? initialized;
  late AppStoreLinks? store;
  late AppVersionConfigs? version;
  late AppStartupMessage? startupMessage;

  AppConfigsData();

  factory AppConfigsData.fromJson(Map<String, dynamic> json) =>
      _$AppConfigsDataFromJson(json);

  Map<String, dynamic> toJson() => _$AppConfigsDataToJson(this);


  /*static Future<AppConfigsData> loadLocalConfigs() async {
    return AppConfigsData.fromJson(jsonDecode(await rootBundle.loadString(ResourcesAssets.APP_CONFIGS_WEB, cache: false)));
  }*/
}

@JsonSerializable()
class AppStoreLinks {
  late String? androidLink;
  late String? iosLink;

  AppStoreLinks();

  factory AppStoreLinks.fromJson(Map<String, dynamic> json) =>
      _$AppStoreLinksFromJson(json);

  Map<String, dynamic> toJson() => _$AppStoreLinksToJson(this);

  String getAppStoreLink()
  {
    if (PlatformUtils().isIOS())
    {
      return iosLink!;
    } else {
      return androidLink!;
    }

  }
}

@JsonSerializable()
class AppVersionConfigs {
  late bool? isMaintenance;
  late int? minVersion;
  late int? currentVersion;
  late int? recommendVersion;
  late int? iosReviewingVersion;
  late int? androidReviewingVersion;

  AppVersionConfigs();

  factory AppVersionConfigs.fromJson(Map<String, dynamic> json) =>
      _$AppVersionConfigsFromJson(json);

  Map<String, dynamic> toJson() => _$AppVersionConfigsToJson(this);
}

@JsonSerializable()
class AppStartupMessage {
  late bool display;
  late String message;
  late String yesLink;

  AppStartupMessage();

  factory AppStartupMessage.fromJson(Map<String, dynamic> json) =>
      _$AppStartupMessageFromJson(json);

  Map<String, dynamic> toJson() => _$AppStartupMessageToJson(this);
}