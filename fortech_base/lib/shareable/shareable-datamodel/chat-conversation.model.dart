import 'package:fortechbase/shareable/shareable-api/model/base.model.dart';
import 'package:json_annotation/json_annotation.dart';


part 'chat-conversation.model.g.dart';

@JsonSerializable(explicitToJson: true)
class ChatConversationModel extends BaseModel {
  Map<String, ChatMessageModel?>? listMessages;
  ChatMessageModel? lastMessage;


  ChatConversationModel();

  factory ChatConversationModel.fromJson(Map<String, dynamic> json) =>
      _$ChatConversationModelFromJson(json);

  Map<String, dynamic> toJson() => _$ChatConversationModelToJson(this);

  List<ChatMessageModel> getSortedMessages() {
    List<ChatMessageModel> sortedListMessages = [];

    if (listMessages != null && listMessages!.isNotEmpty) {
      listMessages!.forEach((key, value) {
        sortedListMessages.add(value!);
      });

      sortedListMessages.sort((m1, m2) => m1.timestamp!.compareTo(m2.timestamp!));
    }

    return sortedListMessages;
  }

}


@JsonSerializable()
class ChatMessageModel extends BaseModel {
  String? userId;
  String? conversationId;
  int? timestamp;
  String? username;
  String? message;


  ChatMessageModel();

  factory ChatMessageModel.fromJson(Map<String, dynamic> json) =>
      _$ChatMessageModelFromJson(json);

  Map<String, dynamic> toJson() => _$ChatMessageModelToJson(this);

}