import 'dart:convert';

import 'package:cloud_functions/cloud_functions.dart';
import 'package:flutter/cupertino.dart';
import 'package:fortechbase/shareable/shareable-api/model/api-response.model.dart';
import 'package:fortechbase/shareable/shareable-managers/app-utils.manager.dart';
import 'package:fortechbase/shareable/utils/log.dart';

class CloudFunctionsHelperAPI {



  static Future<APIResponseModel> executeFunction(BuildContext? context, String functionName, Map<dynamic, dynamic> params, bool displayProgress) async
  {
    // Add Dev suffix if Dev environment
    /*if (AppConfigsManager.IS_DEV) {
      functionName = functionName + "Dev";
    }*/

    Log.d('API.executeFunction: ' + functionName + ' Params: ' + params.toString());

    if (displayProgress && context != null)
    {
      AppUtilsManager().displayLoadingPopup(context);
    }

    APIResponseModel apiResponseModel;

    try {
      HttpsCallableResult result = await FirebaseFunctions.instance.httpsCallable(functionName).call(params);


      Log.d('API.executeFunction: ' + functionName + ' Params: ' + params.toString() + " - Response: " + result.data.toString());

      apiResponseModel =
          APIResponseModel.fromJson(jsonDecode(jsonEncode(result.data)));

    } on Exception catch (e) {
      Log.e(e.toString());
      apiResponseModel = APIResponseModel(1000, null, e, null, false, false);
    }

    if (displayProgress)
    {
      AppUtilsManager().dismissLoadingPopup();
    }

    return apiResponseModel;
  }
}