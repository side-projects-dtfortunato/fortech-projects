class SharedFirebaseCollectionNames {
  static const CENTRAL_USER_DATA = "CentralUserData";

  static const CHAT_CONVERSATIONS = "ChatConversations";

  // Shared
  static const SUB_COLLECTION_PAGINATION = "Pagination";

}

class SharedFirebaseStaticDocs {
}