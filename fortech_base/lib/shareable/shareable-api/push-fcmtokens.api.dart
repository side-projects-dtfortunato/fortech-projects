import 'dart:collection';

import 'package:fortechbase/shareable/shareable-api/cloud-functions-helper.api.dart';
import 'package:fortechbase/shareable/shareable-api/model/api-response.model.dart';

class PushFCMTokensAPI {

  static Future<APIResponseModel> updateFCMTokens(String? token, bool isToRemove)
  {
    Map<dynamic, dynamic> params = HashMap();
    params['token'] = token != null ? token : "";
    params['isToRemove'] = isToRemove;

    return CloudFunctionsHelperAPI.executeFunction(null, 'updateFCMTokens', params, false);
  }

}