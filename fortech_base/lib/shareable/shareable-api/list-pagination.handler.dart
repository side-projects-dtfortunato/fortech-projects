
import 'dart:collection';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:fortechbase/shareable/shareable-api/model/pagination-container.model.dart';
import 'package:fortechbase/shareable/shareable-api/shared-firebase-collection-names.api.dart';
import 'package:rxdart/rxdart.dart';

typedef T fromJson<T>(Map<String, dynamic> json);

class ListPaginationHandler<T> {
  final String _docId;
  final String _collectionName;
  final fromJson _factory;
  final BehaviorSubject<List<T>> _listItems = BehaviorSubject();

  List<PaginationContainerModel> _listPages = [];
  int currentPage = -1;
  bool hasMorePages = true;


  ListPaginationHandler(this._docId, this._collectionName, this._factory);

  void reloadData()
  {
    // Clean lists and get data again
    this._listPages.clear();
    this.currentPage = -1;
    this.hasMorePages = true;

    this.getNextPage();
  }

  void getNextPage() async
  {
    if (this.hasMorePages)
    {
      DocumentSnapshot doc;

      if (this._listPages.isEmpty)
      {
        // If no pages yet, means it should get the root
        doc = await FirebaseFirestore.instance
            .collection(this._collectionName)
            .doc(this._docId).get();
      } else {
        int nextPage = (this.currentPage - 1);
        doc = await FirebaseFirestore.instance
            .collection(this._collectionName)
            .doc(this._docId + "/" + SharedFirebaseCollectionNames.SUB_COLLECTION_PAGINATION + "/" + nextPage.toString()).get();
      }

      if (doc != null && doc.exists)
      {
        PaginationContainerModel pageContainer = PaginationContainerModel.fromJson(doc.data()! as Map<String, dynamic>);
        if (pageContainer != null)
        {
          this.currentPage = pageContainer.pageNum!;
          this._listPages.add(pageContainer);
          this.hasMorePages = pageContainer.pageNum != null && pageContainer.pageNum! > 0; // Page 0 is the last page
          // We received a new page
          this._generateListItems();
        }
      } else {
        // Just generate the list of items
        this._generateListItems();
      }
    }
  }

  void _generateListItems()
  {
    HashMap<String, T> mapAllItems = HashMap();

    _listPages.forEach((pageContainer) {
      pageContainer.mapItems!.keys.forEach((itemId) {

        // First Check if already exist on the map of all items
        if (!mapAllItems.containsKey(itemId))
        {
          mapAllItems[itemId] = this._factory(pageContainer.mapItems![itemId]!);
        }
      });
    });
    this._listItems.add(mapAllItems.values.toList());
  }

  Stream<List<T>> getListItems()
  {
    return this._listItems;
  }

}