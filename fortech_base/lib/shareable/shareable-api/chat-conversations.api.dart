import 'dart:collection';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:fortechbase/shareable/shareable-api/cloud-functions-helper.api.dart';
import 'package:fortechbase/shareable/shareable-api/model/api-response.model.dart';
import 'package:fortechbase/shareable/shareable-api/shared-firebase-collection-names.api.dart';
import 'package:fortechbase/shareable/shareable-datamodel/chat-conversation.model.dart';

class ChatConversationsAPI {



  static Future<APIResponseModel> sendChatMessage(BuildContext context, String chatConversationId, String username, String message) async
  {
    Map<dynamic, dynamic> params = HashMap();
    params['chatConversationId'] = chatConversationId;
    params['username'] = username;
    params['message'] = message;

    return CloudFunctionsHelperAPI.executeFunction(context, 'sendChatMessage', params, true);
  }

  static Future<ChatConversationModel?> getChatConversation(String chatConversationId) async {
    ChatConversationModel? chatConversationModel;

    DocumentSnapshot snapshot = await FirebaseFirestore.instance.collection(SharedFirebaseCollectionNames.CHAT_CONVERSATIONS)
        .doc(chatConversationId).get(GetOptions(source: Source.server));

    if (snapshot.exists && snapshot.data() != null && snapshot.data() is Map<String, dynamic>) {

      chatConversationModel = ChatConversationModel.fromJson(snapshot.data()! as Map<String, dynamic>);
    }

    return chatConversationModel;
  }

}