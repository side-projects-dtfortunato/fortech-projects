import 'dart:io';

import 'package:firebase_storage/firebase_storage.dart';
// import 'package:fortechbase/shareable/shareable-api/files-storage-web.api.dart';
import 'package:fortechbase/shareable/shareable-api/model/storage-file.model.dart';
import 'package:fortechbase/shareable/utils/platform.utils.dart';
import 'package:image_picker/image_picker.dart';


class FilesStorageAPI {
  static FirebaseStorage _storage = FirebaseStorage.instance;

  static Future<StorageFileModel?> uploadUserPhotoProfileFileV2(String userId, PickedFile imageFile)
  {
    return uploadPickedFile(userId + "/images/"+ "user_profile_photo", imageFile);
  }

  @deprecated
  static Future<StorageFileModel> uploadUserPhotoProfileFile(String userId, File imageFile)
  {
    return uploadFile(userId + "/images/"+ "user_profile_photo", imageFile);
  }

  static Future<StorageFileModel> uploadFile(String storagePath, File file) async {
    StorageFileModel storageFileModel = StorageFileModel();

    //Create a reference to the location you want to upload to in firebase
    Reference reference = _storage.ref().child(storagePath);

    //Upload the file to firebase
    UploadTask uploadTask = reference.putFile(file);

    // Waits till the file is uploaded then stores the download url
    TaskSnapshot taskSnapshot = await uploadTask.whenComplete(() => null);

    //returns the download url
    String fileUrl = await taskSnapshot.ref.getDownloadURL() as String;

    storageFileModel.storagePath = storagePath;
    storageFileModel.fileUrl = fileUrl;

    return storageFileModel;
  }

  static Future<StorageFileModel?> uploadPickedFile(String storagePath, PickedFile file) async {
    if (PlatformUtils().isWeb()) {
      //return FilesStorageWebAPI.uploadFileBytes(storagePath, await file.readAsBytes());
      return null;
    } else {
      return uploadFile(storagePath, File(file.path));
    }
  }

}