import 'dart:typed_data';

import 'package:firebase/firebase.dart' as fb;
import 'package:fortechbase/shareable/shareable-api/model/storage-file.model.dart';


class FilesStorageWebAPI {

  static Future<StorageFileModel> uploadFileBytes(String storagePath, Uint8List bytesData) async {

    StorageFileModel storageFileModel = StorageFileModel();

    //Create a reference to the location you want to upload to in firebase
    fb.StorageReference reference = fb.storage().ref(storagePath);

    //Upload the file to firebase
    fb.UploadTaskSnapshot uploadTaskSnapshot = await reference.put(bytesData, fb.UploadMetadata(contentType: 'image/*')).future;

    //returns the download url
    Uri imageUri = await uploadTaskSnapshot.ref.getDownloadURL();

    storageFileModel.storagePath = storagePath;
    storageFileModel.fileUrl = imageUri.toString();

    return storageFileModel;
  }

}