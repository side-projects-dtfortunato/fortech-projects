import 'package:json_annotation/json_annotation.dart';

part 'storage-file.model.g.dart';

@JsonSerializable()
class StorageFileModel {
  String? fileUrl;
  String? storagePath;

  StorageFileModel();

  factory StorageFileModel.fromJson(Map<String, dynamic> json) => _$StorageFileModelFromJson(json);

  Map<String, dynamic> toJson() => _$StorageFileModelToJson(this);
}