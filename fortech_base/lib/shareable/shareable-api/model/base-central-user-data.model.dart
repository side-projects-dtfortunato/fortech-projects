import 'dart:collection';

import 'package:fortechbase/shareable/shareable-api/model/base.model.dart';
import 'package:json_annotation/json_annotation.dart';

part 'base-central-user-data.model.g.dart';

@JsonSerializable(explicitToJson: true)
class BaseCentralUserDataModel extends BaseModel {
  String? userFCMToken;

  // Local
  late Map<String, dynamic> originalJson;


  BaseCentralUserDataModel() {
    originalJson = HashMap();
  }

  factory BaseCentralUserDataModel.fromJson(Map<String, dynamic> json) {
    BaseCentralUserDataModel object = _$BaseCentralUserDataModelFromJson(json);
    object.originalJson = json;
    return object;
  }

  Map<String, dynamic> toJson() => _$BaseCentralUserDataModelToJson(this);

  T? getCentralUserDataParsed<T extends BaseCentralUserDataModel>(T parser(Map<String, dynamic> json))  {
    return parser(originalJson);
  }
}