// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'storage-file.model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

StorageFileModel _$StorageFileModelFromJson(Map<String, dynamic> json) =>
    StorageFileModel()
      ..fileUrl = json['fileUrl'] as String?
      ..storagePath = json['storagePath'] as String?;

Map<String, dynamic> _$StorageFileModelToJson(StorageFileModel instance) =>
    <String, dynamic>{
      'fileUrl': instance.fileUrl,
      'storagePath': instance.storagePath,
    };
