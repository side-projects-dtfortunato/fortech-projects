// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'search-city-response.model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

SearchCityResponseModel _$SearchCityResponseModelFromJson(
        Map<String, dynamic> json) =>
    SearchCityResponseModel()
      ..count = json['count'] as int?
      ..results = (json['results'] as List<dynamic>?)
          ?.map((e) => SearchCityResult.fromJson(e as Map<String, dynamic>))
          .toList();

Map<String, dynamic> _$SearchCityResponseModelToJson(
        SearchCityResponseModel instance) =>
    <String, dynamic>{
      'count': instance.count,
      'results': instance.results?.map((e) => e?.toJson()).toList(),
    };

SearchCityResult _$SearchCityResultFromJson(Map<String, dynamic> json) =>
    SearchCityResult()
      ..name = json['name'] as String?
      ..iso2 = json['iso2'] as String?
      ..country = json['country'] as String?
      ..admin1 = json['admin1'] as String?
      ..lat = (json['lat'] as num?)?.toDouble()
      ..lon = (json['lon'] as num?)?.toDouble()
      ..timezone = json['timezone'] as String?
      ..iata = json['iata'] as String?;

Map<String, dynamic> _$SearchCityResultToJson(SearchCityResult instance) =>
    <String, dynamic>{
      'name': instance.name,
      'iso2': instance.iso2,
      'country': instance.country,
      'admin1': instance.admin1,
      'lat': instance.lat,
      'lon': instance.lon,
      'timezone': instance.timezone,
      'iata': instance.iata,
    };
