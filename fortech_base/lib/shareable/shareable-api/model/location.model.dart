
import 'package:json_annotation/json_annotation.dart';


part 'location.model.g.dart';

@JsonSerializable(explicitToJson: true)
class LocationModel {
  String? cityName;
  String? countryName;
  String? cityCode;
  String? countryCode;
  String? timezone;
  LocationCoordinates? coordinations;

  LocationModel();

  factory LocationModel.fromJson(Map<String, dynamic> json) =>
      _$LocationModelFromJson(json);

  Map<String, dynamic> toJson() => _$LocationModelToJson(this);

  String getLocationLabel() {
    return cityName! + ", " + countryName!;
  }
}

@JsonSerializable(explicitToJson: true)
class LocationCoordinates {
  String? lng;
  String? lat;

  LocationCoordinates();

  factory LocationCoordinates.fromJson(Map<String, dynamic> json) =>
      _$LocationCoordinatesFromJson(json);

  Map<String, dynamic> toJson() => _$LocationCoordinatesToJson(this);

  String getCoordinatesStr() {
    if (lat != null && lng != null) {
      return "[" + lat! + ", " + lng! + "]";
    } else {
      return "";
    }

  }
}
