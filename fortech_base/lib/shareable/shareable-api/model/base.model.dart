import 'package:json_annotation/json_annotation.dart';

part 'base.model.g.dart';

@JsonSerializable()
class BaseModel extends Object {
  String? uid;
}