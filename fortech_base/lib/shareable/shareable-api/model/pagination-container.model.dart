
import 'package:json_annotation/json_annotation.dart';

part 'pagination-container.model.g.dart';

@JsonSerializable()
class PaginationContainerModel {
  int? pageNum;
  Map<String, Map<String, dynamic>>? mapItems;

  PaginationContainerModel();

  factory PaginationContainerModel.fromJson(Map<String, dynamic> json) =>
      _$PaginationContainerModelFromJson(json);

  Map<String, dynamic> toJson() => _$PaginationContainerModelToJson(this);

}