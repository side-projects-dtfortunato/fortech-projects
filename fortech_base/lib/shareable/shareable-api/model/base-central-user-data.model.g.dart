// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'base-central-user-data.model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

BaseCentralUserDataModel _$BaseCentralUserDataModelFromJson(
        Map<String, dynamic> json) =>
    BaseCentralUserDataModel()
      ..uid = json['uid'] as String?
      ..userFCMToken = json['userFCMToken'] as String?
      ..originalJson = json['originalJson'] as Map<String, dynamic>;

Map<String, dynamic> _$BaseCentralUserDataModelToJson(
        BaseCentralUserDataModel instance) =>
    <String, dynamic>{
      'uid': instance.uid,
      'userFCMToken': instance.userFCMToken,
      'originalJson': instance.originalJson,
    };
