import 'package:fortechbase/shareable/shareable-api/model/location.model.dart';
import 'package:json_annotation/json_annotation.dart';

part 'search-city-response.model.g.dart';

@JsonSerializable(explicitToJson: true)
class SearchCityResponseModel {
  int? count;
  List<SearchCityResult?>? results;

  SearchCityResponseModel();

  factory SearchCityResponseModel.fromJson(Map<String, dynamic> json) =>
      _$SearchCityResponseModelFromJson(json);

  Map<String, dynamic> toJson() => _$SearchCityResponseModelToJson(this);
}

@JsonSerializable(explicitToJson: true)
class SearchCityResult {
  String? name;
  String? iso2;
  String? country;
  String? admin1;
  double? lat;
  double? lon;
  String? timezone;
  String? iata;


  SearchCityResult();

  factory SearchCityResult.fromJson(Map<String, dynamic> json) =>
      _$SearchCityResultFromJson(json);

  Map<String, dynamic> toJson() => _$SearchCityResultToJson(this);

  LocationModel parseToLocationModel() {
    LocationModel locationModel = LocationModel();
    locationModel.cityName = name;
    locationModel.countryCode = iso2;
    locationModel.countryName = country;
    locationModel.cityCode = (iata != null && iata!.isNotEmpty) ? iata : name;
    locationModel.coordinations = LocationCoordinates();
    locationModel.coordinations!.lat = lat.toString();
    locationModel.coordinations!.lng = lon.toString();
    locationModel.timezone = timezone;

    return locationModel;
  }

}