// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'api-response.model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

APIResponseModel _$APIResponseModelFromJson(Map<String, dynamic> json) =>
    APIResponseModel(
      json['responseCode'] as int?,
      json['responseData'] as Map<String, dynamic>?,
      json['errorData'],
      json['message'] as String?,
      json['displayMessage'] as bool,
      json['isSuccess'] as bool,
    )..body = json['body'] as String?;

Map<String, dynamic> _$APIResponseModelToJson(APIResponseModel instance) =>
    <String, dynamic>{
      'responseCode': instance.responseCode,
      'responseData': instance.responseData,
      'errorData': instance.errorData,
      'message': instance.message,
      'body': instance.body,
      'displayMessage': instance.displayMessage,
      'isSuccess': instance.isSuccess,
    };
