import 'package:json_annotation/json_annotation.dart';

part 'value-currency.model.g.dart';

@JsonSerializable()
class ValueCurrencyModel {
  double? value;
  String? currencyLabel;
  String? currencyISOCode;


  ValueCurrencyModel();

  factory ValueCurrencyModel.fromJson(Map<String, dynamic> json) => _$ValueCurrencyModelFromJson(json);

  Map<String, dynamic> toJson() => _$ValueCurrencyModelToJson(this);
}