import 'dart:convert';
import 'package:json_annotation/json_annotation.dart';
import 'package:http/http.dart' as http;

part 'api-response.model.g.dart';

@JsonSerializable()
class APIResponseModel {
  int? responseCode;
  Map<String, dynamic>? responseData;
  dynamic errorData;
  String? message;
  String? body;
  bool displayMessage = false;
  bool isSuccess = false;
  bool isResponseList = false;

  APIResponseModel(this.responseCode, this.responseData, this.errorData,
      this.message, this.displayMessage, this.isSuccess);

  APIResponseModel.quickResponse({required this.isSuccess, this.responseData, this.message, this.displayMessage = false}) {
    if(this.isSuccess)
    {
      this.responseCode = 2000;
    } else {
      this.responseCode = 1000;
    }
  }

  APIResponseModel.httpResponse(http.Response httpResponse, {this.isResponseList = false}) {
    this.body = httpResponse.body;
    if (httpResponse != null && httpResponse.statusCode < 300) {
      this.responseCode = 2000;
      this.isSuccess = true;

      // Check if response is json
      if (httpResponse.headers != null && this.body != null && httpResponse.headers["content-type"] != null && (httpResponse.headers["content-type"]?.toLowerCase()) == "application/json") {
        if (isResponseList != true) {
          // Convert to response map
          responseData = jsonDecode(this.body!);
        }
      }
    } else {
      this.responseCode = 1000;
      this.isSuccess = false;
      this.errorData = this.body;
    }
  }

  factory APIResponseModel.fromJson(Map<String, dynamic> json) => _$APIResponseModelFromJson(json);

  Map<String, dynamic> toJson() => _$APIResponseModelToJson(this);

  Map<String, dynamic> getResponseDataMap()
  {
    if (responseData != null)
    {
     return Map<String, dynamic>.from(this.responseData!);
    } else {
      return Map();
    }
  }

}