// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'base.model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

BaseModel _$BaseModelFromJson(Map<String, dynamic> json) =>
    BaseModel()..uid = json['uid'] as String?;

Map<String, dynamic> _$BaseModelToJson(BaseModel instance) => <String, dynamic>{
      'uid': instance.uid,
    };
