// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'value-currency.model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ValueCurrencyModel _$ValueCurrencyModelFromJson(Map<String, dynamic> json) =>
    ValueCurrencyModel()
      ..value = (json['value'] as num?)?.toDouble()
      ..currencyLabel = json['currencyLabel'] as String?
      ..currencyISOCode = json['currencyISOCode'] as String?;

Map<String, dynamic> _$ValueCurrencyModelToJson(ValueCurrencyModel instance) =>
    <String, dynamic>{
      'value': instance.value,
      'currencyLabel': instance.currencyLabel,
      'currencyISOCode': instance.currencyISOCode,
    };
