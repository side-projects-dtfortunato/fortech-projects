// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'pagination-container.model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

PaginationContainerModel _$PaginationContainerModelFromJson(
        Map<String, dynamic> json) =>
    PaginationContainerModel()
      ..pageNum = json['pageNum'] as int?
      ..mapItems = (json['mapItems'] as Map<String, dynamic>?)?.map(
        (k, e) => MapEntry(k, e as Map<String, dynamic>),
      );

Map<String, dynamic> _$PaginationContainerModelToJson(
        PaginationContainerModel instance) =>
    <String, dynamic>{
      'pageNum': instance.pageNum,
      'mapItems': instance.mapItems,
    };
