// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'location.model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

LocationModel _$LocationModelFromJson(Map<String, dynamic> json) =>
    LocationModel()
      ..cityName = json['cityName'] as String?
      ..countryName = json['countryName'] as String?
      ..cityCode = json['cityCode'] as String?
      ..countryCode = json['countryCode'] as String?
      ..timezone = json['timezone'] as String?
      ..coordinations = json['coordinations'] == null
          ? null
          : LocationCoordinates.fromJson(
              json['coordinations'] as Map<String, dynamic>);

Map<String, dynamic> _$LocationModelToJson(LocationModel instance) =>
    <String, dynamic>{
      'cityName': instance.cityName,
      'countryName': instance.countryName,
      'cityCode': instance.cityCode,
      'countryCode': instance.countryCode,
      'timezone': instance.timezone,
      'coordinations': instance.coordinations?.toJson(),
    };

LocationCoordinates _$LocationCoordinatesFromJson(Map<String, dynamic> json) =>
    LocationCoordinates()
      ..lng = json['lng'] as String?
      ..lat = json['lat'] as String?;

Map<String, dynamic> _$LocationCoordinatesToJson(
        LocationCoordinates instance) =>
    <String, dynamic>{
      'lng': instance.lng,
      'lat': instance.lat,
    };
