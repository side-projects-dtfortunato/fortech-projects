import 'dart:collection';

import 'package:fortechbase/shareable/shareable-api/cloud-functions-helper.api.dart';
import 'package:fortechbase/shareable/shareable-api/model/api-response.model.dart';

class AuthAPI {


  static Future<APIResponseModel> resetCentralUserData() async
  {
    Map<dynamic, dynamic> params = HashMap();

    return await CloudFunctionsHelperAPI.executeFunction(null, 'resetCentralUserData', params, false);
  }

}