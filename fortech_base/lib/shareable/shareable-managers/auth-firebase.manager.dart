
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:fortechbase/shareable/shareable-managers/app-utils.manager.dart';
import 'package:fortechbase/shareable/shareable-managers/banner-handler.manager.dart';
import 'package:fortechbase/shareable/utils/log.dart';
import 'package:fortechbase/shareable/utils/platform.utils.dart';
import 'package:fortechbase/shareable/utils/utils.dart';
import 'package:google_sign_in/google_sign_in.dart';

typedef OnCompleteAction(bool success);

class AuthFirebaseManager {
  static final AuthFirebaseManager _instance = AuthFirebaseManager._internal();

  factory AuthFirebaseManager() {
    return _instance;
  }

  AuthFirebaseManager._internal() {
  }


  Future<void> logout() async {
    await FirebaseAuth.instance.signOut();
  }

  /**
   * On Signup pressed
   */
  void onSignupPressed(BuildContext context, String email, String password, {OnCompleteAction? onComplete}) {
    // Handle manual signin
    FirebaseAuth.instance.createUserWithEmailAndPassword(email: email, password: password)
        .then((response){
      BannerMessageHandlerManager().displayMessageLocalised(context, "generic_welcome", true);
      if (onComplete != null) {
        onComplete(true);
      }
    }).catchError((err) {
      if (FirebaseAuth.instance.currentUser == null || FirebaseAuth.instance.currentUser!.isAnonymous) {
        BannerMessageHandlerManager().displayMessageLocalised(context, "error_create_account_invalid", false);
      } else {
        FirebaseAuth.instance.currentUser?.sendEmailVerification();
      }
      Log.e(err.toString());
      if (onComplete != null) {
        onComplete(false);
      }
    });
  }

  /**
   * Execute Login
   */
  void onLoginPressed(BuildContext context, String email, String password, {OnCompleteAction? onComplete}) {
    FirebaseAuth.instance.signInWithEmailAndPassword(email: email, password: password)
        .then((response){
      if (onComplete != null) {
        onComplete(true);
      }
      // BannerMessageHandlerManager().displayMessageLocalised(context, "generic_welcome", true);
    }).catchError((err) {
      if (FirebaseAuth.instance.currentUser == null || FirebaseAuth.instance.currentUser!.isAnonymous) {
        BannerMessageHandlerManager().displayMessageLocalised(context, "login_error_invalid", false);
      }
      if (onComplete != null) {
        onComplete(false);
      }
      Log.e(err.toString());
    });
  }

  /**
   * On Forgot password pressed
   */
  void onForgotPasswordPressed(BuildContext context, String email) {
    if (Utils.isEmailValid(email)) {
      FirebaseAuth.instance.sendPasswordResetEmail(email: email);
      BannerMessageHandlerManager().displayMessageLocalised(context, "login_forgot_reset_success", true);
    } else {
      BannerMessageHandlerManager().displayMessageLocalised(context, "login_invalid_email", false);
    }
  }

  void onGoogleSigninPressed(BuildContext context) async {
    Log.d("onGoogleSigninPressed");
    AppUtilsManager().displayLoadingPopup(context);

    if (PlatformUtils().isWeb()) {
      // Google Signin (From web)
      FirebaseAuth.instance.signInWithPopup(GoogleAuthProvider()).then((response) {
        AppUtilsManager().dismissLoadingPopup();
        //BannerMessageHandlerManager().displayMessageLocalised(context, "generic_welcome", true);
      }).catchError((err) {
        AppUtilsManager().dismissLoadingPopup();
        BannerMessageHandlerManager().displayMessageLocalised(context, "error_create_account_invalid", false);
        Log.e(err.toString());
      });
    } else {
      // Google Signin
      signInWithGoogleNative().then((value) {
        AppUtilsManager().dismissLoadingPopup();
        //BannerMessageHandlerManager().displayMessageLocalised(context, "generic_welcome", true);
      }).catchError((err) {
        AppUtilsManager().dismissLoadingPopup();
        Log.e(err.toString());
      });
    }
  }

  /**
   * Execute the signin in a native device
   */
  Future<String> signInWithGoogleNative() async {
    final FirebaseAuth _auth = FirebaseAuth.instance;
    final GoogleSignIn googleSignIn = GoogleSignIn();

    final GoogleSignInAccount? googleSignInAccount = await googleSignIn.signIn();
    final GoogleSignInAuthentication googleSignInAuthentication = await googleSignInAccount!.authentication;

    final AuthCredential credential = GoogleAuthProvider.credential(
      accessToken: googleSignInAuthentication.accessToken,
      idToken: googleSignInAuthentication.idToken,
    );

    final UserCredential authResult = await _auth.signInWithCredential(credential);
    final User? user = authResult.user;

    assert(!user!.isAnonymous);
    assert(await user!.getIdToken() != null);

    return 'signInWithGoogle succeeded: $user';
  }
}