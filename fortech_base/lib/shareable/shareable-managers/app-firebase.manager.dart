import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/cupertino.dart';
import 'package:fortechbase/shareable/utils/platform.utils.dart';

class AppFirebaseManager {

  static final AppFirebaseManager _instance = AppFirebaseManager._internal();

  factory AppFirebaseManager() {
    return _instance;
  }

  AppFirebaseManager._internal() {}


  Future<FirebaseApp?> initFirebase(FirebaseOptions firebaseOptions, {String? projectName}) async {
    WidgetsFlutterBinding.ensureInitialized();
    if (Firebase.apps == null || Firebase.apps.isEmpty) {
      // return await Firebase.initializeApp(name: projectName, options: firebaseOptions);
      return await Firebase.initializeApp();
    }
    return null;
  }
}