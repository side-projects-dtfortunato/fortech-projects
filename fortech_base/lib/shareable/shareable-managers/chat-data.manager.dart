import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fortechbase/shareable/shareable-managers/base-central-user-data.manager.dart';
import 'package:fortechbase/shareable/shareable-ui/theme/app.theme.dart';
import 'package:fortechbase/shareable/utils/ui-utils.dart';
import 'package:getwidget/components/button/gf_button.dart';
import 'package:getwidget/getwidget.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ChatDataManager {

  static const String KEY_CHAT_BLACK_LISTED = "prefs.chat-blacklisted";

  static final ChatDataManager _instance = ChatDataManager._internal();

  factory ChatDataManager() {
    return _instance;
  }

  ChatDataManager._internal();

  // Data
  List<String>? listBlackListedUserIds;

  Future<void> addUserIdBlackList(String userId) async {
    if (this.listBlackListedUserIds == null) {
      await getListBlackListedUserIds();
    }

    if (!this.listBlackListedUserIds!.contains(userId)) {
      this.listBlackListedUserIds!.add(userId);

      SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
      sharedPreferences.setStringList(KEY_CHAT_BLACK_LISTED, this.listBlackListedUserIds!);
    }
  }

  Future<List<String>> getListBlackListedUserIds() async {
    if (this.listBlackListedUserIds == null) {
      SharedPreferences sharedPreferences = await SharedPreferences.getInstance();

      this.listBlackListedUserIds = sharedPreferences.getStringList(KEY_CHAT_BLACK_LISTED);
      if (this.listBlackListedUserIds == null) {
        this.listBlackListedUserIds = [];
      }
    }

    return this.listBlackListedUserIds!;
  }


  void displayLongpressPopup(BuildContext context, String userId, {VoidCallback? onUserBlocked}) {
    if (userId != BaseCentralUserDataManager().getUserID()) {
      showDialog(context: context, builder: (ctx) {

        double width = UIUtils.getPopupWidth(context);
        return Dialog(
          backgroundColor: ThemeColors.BASE_BG_LIST_GREY_COLOR,
          child: Card(
            child: Container(
              alignment: Alignment.center,
              width: width,
              height: 200,
              child: ClipRRect(
                child: Container(
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      Text("Do you want to block this user permanently?\nAfter you block this user, you will never see content from him anymore.", textAlign: TextAlign.center, style: AppTheme.appTheme.textTheme.bodyText1,),
                      SizedBox(height: 20,),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          GFButton(
                              text: "No",
                              textColor: ThemeColors.COLOR_GRAY_DRK,
                              color: ThemeColors.COLOR_GRAY_DRK,
                              type: GFButtonType.outline,
                              onPressed: () {
                                Navigator.pop(context);
                              }),
                          GFButton(
                              text: "Yes, block this user",
                              textColor: ThemeColors.COLOR_WHITE,
                              color: ThemeColors.STATE_FAILED_NORMAL,
                              onPressed: () {
                                Navigator.pop(context);
                                addUserIdBlackList(userId).then((value) {
                                  if (onUserBlocked != null) {
                                    onUserBlocked();
                                  }
                                });

                              }),
                        ],
                      )
                    ],
                  ),
                ),
              ),
            ),
          ),
        );
      });
    }
    
  }
}