import 'package:flushbar/flushbar.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fortechbase/shareable/shareable-api/model/api-response.model.dart';
import 'package:fortechbase/shareable/shareable-managers/error-api-response.handler.dart';
import 'package:fortechbase/shareable/shareable-ui/theme/app.theme.dart';
import 'package:fortechbase/shareable/utils/log.dart';
import 'package:fortechbase/shareable/utils/resources.language.dart';

class BannerMessageHandlerManager {
  static final BannerMessageHandlerManager _instance = BannerMessageHandlerManager._internal();

  factory BannerMessageHandlerManager()
  {
    return _instance;
  }

  BannerMessageHandlerManager._internal();

  GlobalKey<ScaffoldState> scaffoldKey = GlobalKey<ScaffoldState>();

  void handleFutureAPIResponse(BuildContext context, Future<APIResponseModel> future) {
    future.then((APIResponseModel res) => BannerMessageHandlerManager().handleAPIResponse(context, res))
        .catchError((err) => BannerMessageHandlerManager().displayMessage(context, err.toString(), false));
  }

  void handleAPIResponse(BuildContext context, APIResponseModel apiResponse, [Map<String, Object>? extraData]) {
    // Check if has a string for the Error code
    String? apiResponseMessage = AppLocalizations.of(context)!.translate("api_response_code_" + apiResponse.responseCode.toString());
    Log.d('API Response: ' + apiResponse.toJson().toString());
    if (apiResponseMessage != null) {
      displayMessage(context, apiResponseMessage, apiResponse.isSuccess);
    } else if (apiResponse.displayMessage != null && apiResponse.displayMessage && apiResponse.message != null) {
      displayMessage(context, apiResponse.message, apiResponse.isSuccess);
    } else {
      displayMessageLocalised(context, "generic_error", false);
    }

    ErrorAPIResponseHandler.onErrorAPIReceived(context, apiResponse, extraData);
  }

  void displayMessage(BuildContext context, String? message, bool isSuccess) {
    Log.d('ErrorHandler - Success: ' + isSuccess.toString() + ' - ' + message!);
    // AnalyticsManager().logEvent(AnalyticsCategories.BANNER_MESSAGE, isSuccess ? AnalyticsActions.BANNER_SUCCESS : AnalyticsActions.BANNER_FAILED, message);

    /*SnackBar snackBar = SnackBar(content: Text(message,
      style: TextStyle(
          color: isSuccess
              ? ThemeColors.STATE_SUCCESS_NORMAL : ThemeColors.STATE_FAILED_NORMAL
      ),));

    try {
      ScaffoldMessenger.of(context).showSnackBar(snackBar);
    } on Exception catch (_) {
      scaffoldKey.currentState.showSnackBar(snackBar);
    } */
    Flushbar(
      message: message,
      flushbarPosition: FlushbarPosition.TOP,
      duration: Duration(seconds: 3),
      backgroundColor: isSuccess
          ? ThemeColors.STATE_SUCCESS_NORMAL : ThemeColors.STATE_FAILED_NORMAL,
    )..show(context);
  }

  /**
   * To be able to display the snack bar message, we should set the Scaffold Key on the root Scaffold
   */
  void showMessage(String message, bool isSuccess) {
    /*SnackBar snackBar = SnackBar(content: Text(message,
      style: TextStyle(
          color: isSuccess
              ? ThemeColors.STATE_SUCCESS_NORMAL : ThemeColors.STATE_FAILED_NORMAL
      ),));
    scaffoldKey.currentState.showSnackBar(snackBar); */

  }

  void displayMessageLocalised(BuildContext context, String i18nKey, bool isSuccess) {
    displayMessage(context, AppLocalizations.of(context)?.translate(i18nKey), isSuccess);
    // showMessageLocalised(context, i18nKey, isSuccess);
  }

  void showMessageLocalised(BuildContext context, String i18nKey, bool isSuccess) {
    displayMessage(context, AppLocalizations.of(context)?.translate(i18nKey), isSuccess);
    //showMessage(AppLocalizations.of(context).translate(i18nKey), isSuccess);
  }

}