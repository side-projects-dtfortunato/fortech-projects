import 'package:flutter_udid/flutter_udid.dart';

class StaticUserDataManager {

  static final StaticUserDataManager _instance = StaticUserDataManager._internal();

  factory StaticUserDataManager() {
    return _instance;
  }

  StaticUserDataManager._internal() {}


  // Data
  String? staticUdid;

  Future<void> loadUserUdid() async {
    this.staticUdid = await FlutterUdid.consistentUdid;

    if (this.staticUdid == null) {
      // Try to get the other Udid
      this.staticUdid = await FlutterUdid.udid;
    }
  }

  Future<String?> getUserUdid() async {
    if (this.staticUdid == null) {
      await this.loadUserUdid();
    }
    return this.staticUdid;
  }
}