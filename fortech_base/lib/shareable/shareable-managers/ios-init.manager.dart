import 'package:app_tracking_transparency/app_tracking_transparency.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/services.dart';
import 'package:fortechbase/shareable/utils/platform.utils.dart';

class IOSInitManager{

  static init(BuildContext context) async {
    if (PlatformUtils().isIOS()) {

      // Show tracking authorization dialog and ask for permission
      final status = await AppTrackingTransparency.requestTrackingAuthorization();

      if (status == TrackingStatus.notDetermined) {
        try {
          // If the system can show an authorization request dialog
          if (await AppTrackingTransparency.trackingAuthorizationStatus ==
              TrackingStatus.notDetermined) {
            // Request system's tracking authorization dialog
            await AppTrackingTransparency.requestTrackingAuthorization();
          }
        } on PlatformException {
          // Unexpected exception was thrown
        }
      }

    }
  }

}