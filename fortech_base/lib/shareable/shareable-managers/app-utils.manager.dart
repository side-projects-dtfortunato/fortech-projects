import 'package:ars_progress_dialog/ars_progress_dialog.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';

class AppUtilsManager {
  static final AppUtilsManager _instance = AppUtilsManager._internal();

  factory AppUtilsManager() {
    return _instance;
  }

  AppUtilsManager._internal();

  void displayLoadingPopup(BuildContext context) {
    EasyLoading.show(maskType: EasyLoadingMaskType.clear);

    /*if (_progressDialog != null) {
      dismissLoadingPopup();
    }
    this._progressDialog = Utils.getCustomProgressDialog(context);

    this._progressDialog.show(); */
  }

  void dismissLoadingPopup() {
    // Clean progress dialog
    EasyLoading.dismiss();
    /*if (this._progressDialog != null && this._progressDialog.isShowing) {
      this._progressDialog.dismiss();
      this._progressDialog = null;
    } */
  }
}
