import 'package:fortechbase/shareable/shareable-api/push-fcmtokens.api.dart';
import 'package:fortechbase/shareable/shareable-managers/base-central-user-data.manager.dart';
import 'package:fortechbase/shareable/utils/log.dart';
import 'package:fortechbase/shareable/utils/platform.utils.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';


class PushNotificationsManager {
  static final PushNotificationsManager _instance = PushNotificationsManager._internal();

  factory PushNotificationsManager() {
    return _instance;
  }

  PushNotificationsManager._internal() {
  }

  // Data
  final FirebaseMessaging _firebaseMessaging = FirebaseMessaging.instance;
  String? _serverToken = null;
  bool _initialized = false;
  static FlutterLocalNotificationsPlugin _flutterLocalNotificationsPlugin = FlutterLocalNotificationsPlugin();
  dynamic pendingOpenedNotification;

  Future<void> init() async {
    if (!PlatformUtils().isWeb()) {
      try {
        if (!_initialized) {
          // For iOS request permission first.
          NotificationSettings settings = await _firebaseMessaging.requestPermission(
            alert: true,
            announcement: false,
            badge: true,
            carPlay: false,
            criticalAlert: false,
            provisional: false,
            sound: true,
          );

          if (settings.authorizationStatus == AuthorizationStatus.authorized) {
            FirebaseMessaging.onMessage.listen((remoteMessage) {
              _onMessage(remoteMessage);
            });

            FirebaseMessaging.onBackgroundMessage(OnBackgroundMessage);
          }

          // For testing purposes print the Firebase Messaging token
          String? token = await _firebaseMessaging.getToken();
          _firebaseMessaging.onTokenRefresh.listen((tokenChanged) {

            if (this._serverToken != null)
            {
              // Remove the previous fcm token
              PushFCMTokensAPI.updateFCMTokens(this._serverToken, true);
            }
            // On Token changed
            this._onTokenReceived(tokenChanged);
          });
          this._onTokenReceived(token);
          Log.d("FirebaseMessaging token: $token");

          // Init Local Notifications
          _initLocalNotifications();

          _initialized = true;
        } else {
          this._updateUserFCMToken();
        }
      } catch (e)
      {
        Log.d(e);
      }
    }

  }


  _initLocalNotifications() async {
    var initializationSettingsAndroid = new AndroidInitializationSettings('@mipmap/ic_launcher');
    var initializationSettingsIOS = new DarwinInitializationSettings();
    var initializationSettings = new InitializationSettings(android: initializationSettingsAndroid, iOS: initializationSettingsIOS);
    _flutterLocalNotificationsPlugin.initialize(initializationSettings);
  }

  void _onTokenReceived(String? token)
  {
    this._serverToken = token;

    this._updateUserFCMToken();
  }

  void onUserLoggedin()
  {
    this._updateUserFCMToken();
  }

  void onUserLoggedout()
  {
    // Should remove the token
    PushFCMTokensAPI.updateFCMTokens(this._serverToken, true);
  }

  void _updateUserFCMToken()
  {
    // Check if is different than the one the user already have
    if (BaseCentralUserDataManager().isUserLoggedin() && BaseCentralUserDataManager().getUserFCMToken() != this._serverToken) {
      // If the user is logged in, check if should update list of FCM Tokens
      PushFCMTokensAPI.updateFCMTokens(this._serverToken, false);
    }
  }

  Future<void> _onMessage(RemoteMessage message) async
  {
    Log.d("_onMessage - onMessage: $message");
    _showNotification(message);
  }

  static Future<dynamic> OnBackgroundMessage(RemoteMessage message) async
  {
    Log.d("OnBackgroundMessage - onMessage: $message");
    _showNotification(message);
    return Future<void>.value();
  }

  static Future _showNotification(RemoteMessage message) async {
    var pushTitle;
    var pushText;
    var action;

    if (message.notification?.android != null && PlatformUtils().platformType == PlatformTypes.ANDROID) {
      pushTitle = message.notification?.title;
      pushText = message.notification?.body;
      action = message.notification?.android?.clickAction;
    } else if (message.notification?.apple != null){
      pushTitle = message.notification?.title;
      pushText = message.notification?.body;
      action = "";
    }
    Log.d("AppPushs params pushTitle : $pushTitle");
    Log.d("AppPushs params pushText : $pushText");
    Log.d("AppPushs params pushAction : $action");

    // @formatter:off
    var platformChannelSpecificsAndroid = new AndroidNotificationDetails(
        '24052020',
        'Remote-Work.app - Channel',
        playSound: false,
        enableVibration: false,
        importance: Importance.max,
        priority: Priority.high);
    // @formatter:on
    var platformChannelSpecificsIos = new DarwinNotificationDetails(presentSound: false);
    var platformChannelSpecifics = new NotificationDetails(android: platformChannelSpecificsAndroid, iOS: platformChannelSpecificsIos);

    new Future.delayed(Duration.zero, () {
      _flutterLocalNotificationsPlugin.show(
        0,
        pushTitle,
        pushText,
        platformChannelSpecifics,
        // payload: 'No_Sound',
      );
    });
  }
}