

import 'package:fortechbase/shareable/shareable-api/model/api-response.model.dart';
import 'package:fortechbase/shareable/shareable-api/model/location.model.dart';
import 'package:fortechbase/shareable/shareable-api/model/search-city-response.model.dart';
import 'package:fortechbase/shareable/shareable-managers/http-request.manager.dart';
import 'package:fortechbase/shareable/utils/log.dart';

class LocationSearchManager {
  static final LocationSearchManager _instance = LocationSearchManager._internal();

  factory LocationSearchManager() {
    return _instance;
  }

  LocationSearchManager._internal() {}

  Future<List<LocationModel>> searchCity(String query) async {
    Log.d("searchCity: " + query);
    APIResponseModel apiResponse = await HTTPRequestManager().get("https://www.meteoblue.com/en/server/search/query3?query=" + query);

    // Parse response to List of locations
    List<LocationModel> listLocations = [];

    if (apiResponse != null && apiResponse.isSuccess && apiResponse.responseData != null) {
      // Parse to search city response
      SearchCityResponseModel responseModel = SearchCityResponseModel.fromJson(apiResponse.responseData!);

      // Parse to location model
      if (responseModel.results != null && responseModel.results!.isNotEmpty) {
        responseModel.results!.forEach((cityResponse) {
          LocationModel locationModel = cityResponse!.parseToLocationModel();

          //Check if already exists
          bool alreadyExists = false;
          listLocations.forEach((itLocation) {
            if (!alreadyExists) {
              alreadyExists = itLocation.cityCode == locationModel.cityCode;
            }
          });

          if(!alreadyExists) {
            // Doesn't exist yet with this city code, can add it into the list of cities
            listLocations.add(locationModel);
          }
        });
      }
    }

    return listLocations;
  }

}

