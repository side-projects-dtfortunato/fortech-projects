import 'package:flutter/cupertino.dart';
import 'package:google_mobile_ads/google_mobile_ads.dart';

class AdsAdmobManager {
  static final AdsAdmobManager _instance = AdsAdmobManager._internal();

  factory AdsAdmobManager() {
    return _instance;
  }

  AdsAdmobManager._internal() {}

  // Data
  InterstitialAd? _loadedInterstitialAd;

  void init() {
    WidgetsFlutterBinding.ensureInitialized();
    MobileAds.instance.initialize();
  }

  void showInterstitialAds(String adsId, {List<String>? keywords}) {
    InterstitialAd.load(
        adUnitId: adsId,
        request: AdRequest(keywords: keywords),
        adLoadCallback: InterstitialAdLoadCallback(
          onAdLoaded: (InterstitialAd ad) {
            // Keep a reference to the ad so you can show it later.
            this._loadedInterstitialAd = ad;
            this._loadedInterstitialAd!.show();
          },
          onAdFailedToLoad: (LoadAdError error) {
            print('InterstitialAd failed to load: $error');
          },
        ));
  }

  Widget getBannerView(String adId, {List<String>? keywords, AdSize adSize : AdSize.banner}) {

    AdWidget adWidget = AdWidget(ad: BannerAd(
      adUnitId: adId,
      size: adSize,
      request: AdRequest(keywords: keywords),
      listener: BannerAdListener(),
    ));
    adWidget.ad.load();

    return adWidget;
  }

}