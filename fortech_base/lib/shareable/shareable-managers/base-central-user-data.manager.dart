import 'dart:async';

import 'package:cloud_firestore/cloud_firestore.dart' as cloudFirestore;
import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:fortechbase/shareable/shareable-api/auth.api.dart';
import 'package:fortechbase/shareable/shareable-api/model/base-central-user-data.model.dart';
import 'package:fortechbase/shareable/shareable-api/shared-firebase-collection-names.api.dart';
import 'package:fortechbase/shareable/shareable-managers/push-notifications.manager.dart';
import 'package:fortechbase/shareable/utils/log.dart';
import 'package:rxdart/rxdart.dart';

class BaseCentralUserDataManager {
  static final BaseCentralUserDataManager _instance = BaseCentralUserDataManager._internal();

  factory BaseCentralUserDataManager() {
    return _instance;
  }

  BaseCentralUserDataManager._internal() {
    _centralUserDataSubject = BehaviorSubject();
    _centralUserDataSubject!.add(BaseCentralUserDataModel());

    // Listen to user auth changes
    FirebaseAuth.instance.authStateChanges().listen((user) {
      Log.d("FirebaseAuth - userChanges: " + (user != null ? user.uid : "NA"));
      this.firebaseUser = user;

      // Reinit Central User Data
      if (user != null) {
        _initCentralUserDataListening(user.uid);
      } else {
        _initCentralUserDataListening(null);
      }
    });
  }

  // Subscriptions
  StreamSubscription? _centralDataSubscription;

  // State
  BaseCentralUserDataModel? lastCentralUserState;
  BehaviorSubject<BaseCentralUserDataModel?>? _centralUserDataSubject;
  User? firebaseUser;

  void _initCentralUserDataListening(String? userId) {
    if (_centralDataSubscription != null && !_centralDataSubscription!.isPaused) {
      _centralDataSubscription?.cancel();
      this.lastCentralUserState = BaseCentralUserDataModel();
      _centralUserDataSubject!.add(this.lastCentralUserState);
    }

    if (userId != null) {
      FirebaseAnalytics.instance.setUserId(id: userId);

      _centralDataSubscription = cloudFirestore.FirebaseFirestore.instance
          .collection(SharedFirebaseCollectionNames.CENTRAL_USER_DATA)
          .doc(userId)
          .snapshots()
          .listen((data) {
        if (data != null && data.exists) {
          Log.d('Received new CentralUserData with id: ' + data.data().toString());
          this.lastCentralUserState = BaseCentralUserDataModel.fromJson(data.data()!);


          PushNotificationsManager().onUserLoggedin();
        } else {
          this.lastCentralUserState = null;

          // Reset Central User Data
          AuthAPI.resetCentralUserData();
        }


        Log.d('Update last central user state: ' + (lastCentralUserState != null ? lastCentralUserState!.toJson().toString() : "NULL"));
        // Update Stream
        _centralUserDataSubject!.add(this.lastCentralUserState);
      });
    }
  }

  Stream<BaseCentralUserDataModel?> onCentralUserData() {
    return this._centralUserDataSubject!.stream;
  }

  Stream<bool> onUserLoggedin() {
    return this._centralUserDataSubject!.stream.map((centralData) => centralData != null && centralData.uid != null);
  }

  Future<bool> isUserloggedIn() async {
    return this.onUserLoggedin().first;
  }

  bool isUserLoggedin() {
    return this.firebaseUser != null && this.firebaseUser?.uid != null && !this.firebaseUser!.isAnonymous && this.lastCentralUserState != null && this.lastCentralUserState!.uid != null;
  }

  String? getUserID() {
    return this.firebaseUser != null ? this.firebaseUser!.uid : null;
  }

  String? getUserFCMToken() {
    return lastCentralUserState != null ? lastCentralUserState?.userFCMToken : null;
  }

  void notifyStaticCentralDataChanged() {
    // Update Stream
    _centralUserDataSubject!.add(this.lastCentralUserState);
  }

}