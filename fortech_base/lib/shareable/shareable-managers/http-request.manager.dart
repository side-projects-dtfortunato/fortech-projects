import 'package:fortechbase/shareable/shareable-api/model/api-response.model.dart';
import 'package:http/http.dart' as http;

class HTTPRequestManager {
  static final HTTPRequestManager _instance = HTTPRequestManager._internal();

  factory HTTPRequestManager() {
    return _instance;
  }

  HTTPRequestManager._internal() {
  }

  Future<APIResponseModel> get(String url) async {
    http.Response response = await http.get(Uri.parse(url));
    APIResponseModel apiResponseModel = APIResponseModel.httpResponse(response);

    return apiResponseModel;
  }

}