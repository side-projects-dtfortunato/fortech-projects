#!/bin/bash

# Function to deploy a single project's functions
deploy_project() {
    local project=$1
    cd "$project/backend/functions"
    
    echo "Starting functions deployment for $project"
    npm install && npm run deploy-prod
    echo "Finished functions deployment for $project"
    
    cd ../../..
}

# Array to store background process PIDs
pids=()
current_parallel=0
max_parallel=3

# Deploy projects with parallel limit
for project in jobsinuk-app remotejobshub-app customerremotejobs marketingremotejobs-app remoteitjobs-app design-remote-jobs react-remote-jobs juniorremotejobs-com remoteinaustralia remote-work-webapp-v2; do
    # If we've reached the parallel limit, wait for one to finish
    if [ $current_parallel -ge $max_parallel ]; then
        wait -n  # Wait for any background process to finish
        current_parallel=$((current_parallel - 1))
    fi
    
    # Start the deployment in background
    deploy_project "$project" &
    pids+=($!)
    current_parallel=$((current_parallel + 1))
done

# Wait for all remaining processes to complete
wait

echo "All functions deployments completed!"
