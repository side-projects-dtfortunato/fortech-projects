import {AdsType} from "../components/ads/BannerAds.component";

export interface AffiliateDataModel {
    id: string;
    label: string;
    link: string;
    horizontalBanner: string;
    verticalBanner: string;
    rectangleBenner: string;
}

export const AffiliatesData: { [affiliateId: string]: AffiliateDataModel } = {
    "1xbet": {
        id: "1xbet",
        label: "1xbet",
        link: "https://affpa.top/L?tag=d_1852033m_97c_&site=1852033&ad=97",
        horizontalBanner: "<iframe scrolling='no' frameBorder='0' style='padding:0px; margin:0px; border:0px;border-style:none;border-style:none;' width='728' height='90' src=\"https://refbanners.com/I?tag=d_1852033m_32557c_&site=1852033&ad=32557\" ></iframe>",
        verticalBanner: "<iframe scrolling='no' frameBorder='0' style='padding:0px; margin:0px; border:0px;border-style:none;border-style:none;' width='160' height='600' src=\"https://refbanners.com/I?tag=d_1852033m_32543c_&site=1852033&ad=32543\" ></iframe>",
        rectangleBenner: "<iframe scrolling='no' frameBorder='0' style='padding:0px; margin:0px; border:0px;border-style:none;border-style:none;' width='300' height='250' src=\"https://refbanners.com/I?tag=d_1852033m_4639c_&site=1852033&ad=4639\" ></iframe>",
    },
    "melbet": {
        id: "melbet",
        label: "melbet",
        link: "",
        horizontalBanner: "<iframe scrolling='no' frameBorder='0' style='padding:0px; margin:0px; border:0px;border-style:none;border-style:none;' width='728' height='90' src=\"https://melbanusd.top/I?tag=d_1859205m_26911c_&site=1859205&ad=26911\" ></iframe>",
        verticalBanner: "<iframe scrolling='no' frameBorder='0' style='padding:0px; margin:0px; border:0px;border-style:none;border-style:none;' width='160' height='600' src=\"https://melbanusd.top/I?tag=d_1859205m_26981c_&site=1859205&ad=26981\" ></iframe>",
        rectangleBenner: "<iframe scrolling='no' frameBorder='0' style='padding:0px; margin:0px; border:0px;border-style:none;border-style:none;' width='300' height='250' src=\"https://melbanusd.top/I?tag=d_1859205m_26907c_&site=1859205&ad=26907\" ></iframe>",
    }
}

export class AffiliateHelper {

    static hasAffiliateId(affiliateId: string): boolean {
        return this.getAffiliateLink(affiliateId) !== undefined;
    }

    static getAffiliateLink(affiliateId: string): AffiliateDataModel | undefined {
        return Object.values(AffiliatesData)
            .find((affiliateItem) => affiliateItem.id.toLowerCase() === affiliateId.toLowerCase()
            && affiliateItem.label.toLowerCase() === affiliateId.toLowerCase());
    }


    static getRandomAd(adType: AdsType, adsProvider?: string ) {
        if (!adsProvider) {
            adsProvider = "melbet";
        }
        let adAffiliateObj: AffiliateDataModel =  AffiliatesData[adsProvider];

        switch (adType) {
            case AdsType.HORIZONTAL:return adAffiliateObj.horizontalBanner;
            case AdsType.VERTICAL:return adAffiliateObj.verticalBanner;
            case AdsType.RECTANGLE:return adAffiliateObj.rectangleBenner;
        }

        // Fallback
        return adAffiliateObj.horizontalBanner;
    }

}