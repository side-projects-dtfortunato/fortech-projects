import MatchPredictedComponent from "../components/list-items/MatchPredicted.component";
import {NextMatchesByLeague} from "../data/model/LeagueMatches.model";
import LeagueListItemComponent from "../components/list-items/LeagueListItem.component";
import EmptyListComponent from "../components/EmptyList.component";
import GoogleAdsComponent from "../components/ads/GoogleAds.component";

export default function TodayTipsContent(props: { nextPredictedMatches: NextMatchesByLeague }) {
    function genListMatches() {
        let listRenderItems = [];


        if (props.nextPredictedMatches) {
            Object.values(props.nextPredictedMatches).forEach((leagueItem) => {

                if (leagueItem.listMatches && Object.keys(leagueItem.listMatches).length > 0) {
                    // Add league separator
                    listRenderItems.push((
                        <div className='mt-2' key={"league_" + leagueItem.leagueData.id}>
                            <LeagueListItemComponent
                                                     leagueData={leagueItem.leagueData} />
                        </div>
                        ));

                    // List Matches of this league
                    Object.values(leagueItem.listMatches)
                        .sort((m1, m2) => m1.matchObject.fixture.date.localeCompare(m2.matchObject.fixture.date))
                        .forEach((matchItem) => {
                            listRenderItems.push((<div key={"match_" + matchItem.id} className='mt-1'>
                                <MatchPredictedComponent matchModel={matchItem}
                                                         leagueLabel={leagueItem.leagueData.label} showTIPBtn={true}/>
                            </div>));
                        });
                }
            });
        }

        if (listRenderItems.length > 0) {
            return listRenderItems;
        } else {
            return (
                <EmptyListComponent emptyMessage='Unfortunately we don&apos;t have anymore tips for today. Please, come back tomorrow for more tips' />
            );
        }
    }

    return (
        <div className='flex flex-column p-1'>
            <div className='w-full bg-white flex flex-col justify-content-center align-items-center p-3 mt-3'>
                <h4 className='text-center text-slate-900 text-center mt-4'>Today Football Betting Tips</h4>
                <span className='text-center text-slate-500 text-center mt-2 font-xs' style={{fontSize: 13}}>Find the best tips for today principal football european leagues. We calculate the prediction result using a unique algorithm IA system</span>
            </div>
            {genListMatches()}
        </div>
    )
}