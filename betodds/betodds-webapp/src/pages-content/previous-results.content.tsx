import {MatchesFinishedModel} from "../data/model/MatchesFinished.model";
import MatchFinishedComponent from "../components/list-items/MatchFinished.component";
import MatchModel, {MatchTipType} from "../data/model/Match.model";
import EmptyListComponent from "../components/EmptyList.component";
import {Image} from "react-bootstrap";
import dateFormat from "dateformat";
import GoogleAdsComponent from "../components/ads/GoogleAds.component";


export default function PreviousResultsContent(props: {matchesFinished: MatchesFinishedModel}) {

    return (
        <div className='flex flex-col'>
            <div className='w-full bg-white flex flex-col justify-content-center align-items-center p-3 mt-3 mb-3'>
                <h4 className='text-center text-slate-900 text-center mt-1'>Last results of our predictions</h4>
                <span className='text-center text-slate-500 text-center mt-1 font-xs' style={{fontSize: 13}}>Check out which football matches our predictions were correct.</span>
                {/*genNumResults(props.matchesFinished)*/}
            </div>
            {genListMatches(props.matchesFinished)}
        </div>
    )
}


function genNumResults(matchesFinished: MatchesFinishedModel) {
    let countCorrectResults: number = 0;
    let countWrongResults: number = 0;

    Object.values(matchesFinished).forEach((matchItem) => {
        let isPredictionCorrect: boolean = matchItem.prediction.calculatedPrediction === matchItem.prediction.finalResult;

        if (isPredictionCorrect) {
            countCorrectResults++;
        } else {
            countWrongResults++;
        }
    });

    // New results
    let betProfitResults: number = 0;

    Object.values(matchesFinished).forEach((matchItem) => {
        let isPredictionCorrect: boolean = matchItem.prediction.calculatedPrediction === matchItem.prediction.finalResult;

        betProfitResults -= 1; // Firstly, remove the value of our bet
        if (isPredictionCorrect) {
            // Get odds
            switch (matchItem.prediction.calculatedPrediction) {
                case MatchTipType.HOME:
                    betProfitResults += matchItem.oddsAvg.home;
                    break;
                case MatchTipType.DRAW:
                    betProfitResults += matchItem.oddsAvg.draw;
                    break;
                case MatchTipType.AWAY:
                    betProfitResults += matchItem.oddsAvg.away;
                    break;
            }
        }
    });
    let textColor: string = betProfitResults < 0 ? "text-red-500" : "text-green-700";

    return (
        <div className='flex flex-col align-items-center justify-content-center p-1'>
            <span className='text-slate-900 font-sm ml-1'>Our current balance if we bet 1€/$ in our predictions whould be: </span>
            <div className='flex align-items-end justify-content-center border rounded px-2 ml-1 mr-4'>
                <span className={'font-sm ' + textColor}>{betProfitResults.toPrecision(2) + " €/$"}</span>
            </div>
        </div>
    )
}

function genDateSeparator(dateStr: string) {
    return (
        <div className='flex bg-white align-items-center justify-content-start p-2 text-slate-600 mt-1 mb-1'>
            <Image src="/icons/calendar-128.png" width={20} style={{color: "#D2D2D2"}} />
            <span className='mx-2'>{dateStr}</span>
        </div>
    )
}

function genListMatches(matchesFinished: MatchesFinishedModel) {

    let listMatches: MatchModel[] = Object.keys(matchesFinished)
            .filter((key) => key !== "uid").map((key) => matchesFinished[key]).sort((m1, m2) => m2.matchObject.fixture.timestamp - m1.matchObject.fixture.timestamp);

    let lastDateStr: string = "";
    // Sort matches
    let listRenderItems = listMatches.map((matchData) => {
        let matchDateStr = dateFormat(new Date(matchData.matchObject.fixture.date), "yyyy/mm/dd")

        let separatorRender = (<></>);
        if (lastDateStr !== matchDateStr) {
            lastDateStr = matchDateStr;
            separatorRender = genDateSeparator(lastDateStr);
        }
        return (
            <>
                {separatorRender}
                <div className='mb-1' key={matchData.id}>
                    <MatchFinishedComponent matchModel={matchData} />
                </div>
            </>
        )
    });

    if (listRenderItems.length > 0) {
        return listRenderItems;
    } else {
        return (
            <EmptyListComponent emptyMessage='We don&apos;t have any previous result to show you. come back tomorrow for more tips' />
        );
    }
}