import {Image} from "react-bootstrap";

export default function EmptyListComponent(props: {emptyMessage: string}) {
    return (
        <div className='flex flex-col justify-content-center align-items-center w-full p-2 mt-5'>
            <Image src='/images/logo.png' className='w-20'/>
            <span className='text-center font-lg mt-5'>{props.emptyMessage}</span>
        </div>
    )
}