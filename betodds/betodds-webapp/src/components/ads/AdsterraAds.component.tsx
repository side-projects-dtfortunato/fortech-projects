export enum AdsterraAdType  {HORIZONTAL = "HORIZONTAL", VERTICAL = "VERTICAL"}

export default function AdsterraAdsComponent(props: {adType: AdsterraAdType}) {

    let adsCode: string = HORIZONTAL_ADS;

    switch (props.adType) {

        case AdsterraAdType.HORIZONTAL:
            adsCode = HORIZONTAL_ADS;
            break;
        case AdsterraAdType.VERTICAL:
            adsCode = VERTICAL_ADS;
            break;
        default:
            return (<></>);
    }

    return (
        <div className='flex w-full h-full justify-content-center py-2'>
            <div dangerouslySetInnerHTML={{__html: adsCode}}/>
        </div>
    )
}

const HORIZONTAL_ADS = "<script type=\"text/javascript\">\n" +
    "\tatOptions = {\n" +
    "\t\t'key' : '2133df7c3eca91ab6256125a51c4fd4c',\n" +
    "\t\t'format' : 'iframe',\n" +
    "\t\t'height' : 90,\n" +
    "\t\t'width' : 728,\n" +
    "\t\t'params' : {}\n" +
    "\t};\n" +
    "\tdocument.write('<scr' + 'ipt type=\"text/javascript\" src=\"http' + (location.protocol === 'https:' ? 's' : '') + '://www.profitabledisplayformat.com/2133df7c3eca91ab6256125a51c4fd4c/invoke.js\"></scr' + 'ipt>');\n" +
    "</script>"

const VERTICAL_ADS = "<script type=\"text/javascript\">\n" +
    "\tatOptions = {\n" +
    "\t\t'key' : '66808dfd38713019cbe23dda9694a7ae',\n" +
    "\t\t'format' : 'iframe',\n" +
    "\t\t'height' : 600,\n" +
    "\t\t'width' : 160,\n" +
    "\t\t'params' : {}\n" +
    "\t};\n" +
    "\tdocument.write('<scr' + 'ipt type=\"text/javascript\" src=\"http' + (location.protocol === 'https:' ? 's' : '') + '://www.profitabledisplayformat.com/66808dfd38713019cbe23dda9694a7ae/invoke.js\"></scr' + 'ipt>');\n" +
    "</script>";