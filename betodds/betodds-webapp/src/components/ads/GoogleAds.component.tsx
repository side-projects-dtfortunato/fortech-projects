import React, {useEffect} from 'react';

/**
 * Horizontal:
 data-ad-client="ca-pub-5145255847286009"
 data-ad-slot="9882788597"
 */
export default function GoogleAdsComponent(props: { slot: string }) {


    const loadAds = () => {
        try {
            if (typeof window !== "undefined") {
                ((window as any).adsbygoogle = (window as any).adsbygoogle || []).push({});
            }
        } catch (error: any) {
            console.log("adsense error", error.message);
        }
    };

    useEffect(() => {
        loadAds();
    }, []);

    return (
        <ins
            className="adsbygoogle w-full h-full"
            style={{display: "block"}}
            data-ad-client="ca-pub-5492791201497132"
            data-ad-slot={props.slot}
            data-ad-format="auto"
            data-full-width-responsive="true"
            data-adtest="on"
        ></ins>
    )
    /*
    const loadAds = () => {
        try {
            if (typeof window !== "undefined") {
                ((window as any).adsbygoogle = (window as any).adsbygoogle || []).push({});
            }
        } catch (error) {
            console.log("adsense error", error.message);
        }
    };
    useEffect(() => {
        if (!AppConfigs.REMOVE_ADS) {
            loadAds();
        }

    }, []);


    if (AppConfigs.REMOVE_ADS) {
        return <></>;
    } else {
        return(
            <ins
                className="adsbygoogle"
                style={{ display: "block" }}
                data-ad-client="ca-pub-5492791201497132"
                data-ad-slot={props.slot}
                data-ad-format="auto"
                data-full-width-responsive="true"
            ></ins>
        )
    }
*/
}
