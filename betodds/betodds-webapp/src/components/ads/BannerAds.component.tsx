import {AffiliateHelper} from "../../utils/AffiliateHelper";

export enum AdsType {HORIZONTAL, VERTICAL, RECTANGLE, VIDEO_HORIZONTAL};

export default function BannerAdsComponent(props: {adType: AdsType, adsProvider?:string}) {

    let componentRender: any;

    switch (props.adType) {

        case AdsType.VERTICAL:
            componentRender = (
                <div className='flex justify-content-center w-full object-contain'>
                    <div dangerouslySetInnerHTML={{ __html: AffiliateHelper.getRandomAd(AdsType.VERTICAL, props.adsProvider)}}></div>
                </div>
            );
            break;
        default:
        case AdsType.HORIZONTAL:
        case AdsType.RECTANGLE:
            componentRender= (
                <>
                    <div className='visible sm:hidden flex justify-content-center w-full object-contain'>
                        <div dangerouslySetInnerHTML={{ __html: AffiliateHelper.getRandomAd(AdsType.RECTANGLE, props.adsProvider)}}></div>
                    </div>

                    <div className='hidden sm:flex flex justify-content-center w-full object-contain'>
                        <div dangerouslySetInnerHTML={{ __html: AffiliateHelper.getRandomAd(AdsType.HORIZONTAL, props.adsProvider)}}></div>
                    </div>

                </>
            )
    }

    return (
        <div className='my-2'>
            {componentRender}
        </div>
    )
}