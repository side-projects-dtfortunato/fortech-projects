import {AdsterraAdType} from "./AdsterraAds.component";

export enum AdsType {INTERSTITIAL, HORIZONTAL, VERTICAL}

export default function AdsExoclickComponent(props: {adType: AdsType}) {
    let adsCode: string = HORIZONTAL_BANNER_ADS;

    switch (props.adType) {

        case AdsType.HORIZONTAL:
            adsCode = HORIZONTAL_BANNER_ADS;
            break;
        case AdsType.VERTICAL:
            adsCode = VERTICAL_BANNER_ADS;
            break;
        case AdsType.INTERSTITIAL:
            adsCode = INTERSTITIAL_ADS;
            break;
        default:
            return (<></>);
    }

    return (
        <div className='flex w-full h-full justify-content-center py-2'>
            <div dangerouslySetInnerHTML={{__html: adsCode}}/>
        </div>
    )
}

const INTERSTITIAL_ADS = "<script type=\"application/javascript\" \n" +
    "data-idzone=\"4868654\"  data-ad_frequency_count=\"1\"  data-ad_frequency_period=\"10\"  data-type=\"desktop\" \n" +
    "data-browser_settings=\"1\" \n" +
    "data-ad_trigger_method=\"3\" \n" +
    "\n" +
    "src=\"https://a.exdynsrv.com/fp-interstitial.js\"></script>";

const HORIZONTAL_BANNER_ADS = "<script async type=\"application/javascript\" src=\"https://a.exdynsrv.com/ad-provider.js\"></script> \n" +
    " <ins class=\"adsbyexoclick\" data-zoneid=\"4868658\" data-keywords=\"keywords\"></ins> \n" +
    " <script>(AdProvider = window.AdProvider || []).push({\"serve\": {}});</script>";

const VERTICAL_BANNER_ADS = "<script async type=\"application/javascript\" src=\"https://a.exdynsrv.com/ad-provider.js\"></script> \n" +
    " <ins class=\"adsbyexoclick\" data-zoneid=\"4868660\" data-keywords=\"keywords\"></ins> \n" +
    " <script>(AdProvider = window.AdProvider || []).push({\"serve\": {}});</script>";