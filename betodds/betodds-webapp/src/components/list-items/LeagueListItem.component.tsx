import {LeagueAvailableModel} from "../../data/model/LeagueMatches.model";
import {Image} from "react-bootstrap";
import GoogleAdsComponent from "../ads/GoogleAds.component";
import {AppConfigs} from "../../data/AppConfigs";

export default function LeagueListItemComponent(props: { leagueData: LeagueAvailableModel }) {
    function getLeagueIconUrl() {
        return "https://media.api-sports.io/football/leagues/" + props.leagueData.id + ".png"
    }

    function renderLogo() {
        if (AppConfigs.HIDE_LOGOS) {
            return <></>
        } else {
            return <div className='ml-4 mr-4'>
                {AppConfigs.renderLogo(props.leagueData.label, getLeagueIconUrl(), 30)}
            </div>
        }
    }

    return (
        <div className='flex flex-col w-full'>
            <div className='w-full bg-white flex justify-content-start align-items-center p-3 mt-3'>
                {renderLogo()}
                <h5 className='text-left text-slate-700 text-center mt-2'>{AppConfigs.getLeagueLabel(props.leagueData.label)}</h5>
            </div>
        </div>
    )
}