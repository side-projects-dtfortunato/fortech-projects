import MatchModel, {MatchTipType} from "../../data/model/Match.model";
import {Image} from "react-bootstrap";
import MatchOddsComponent from "../match-tips/MatchOdds.component";
import dateFormat from "dateformat";
import {generatePageMatchTipPath} from "../../pages/match-tip/[id]";
import {AppConfigs} from "../../data/AppConfigs";

export default function MatchFinishedComponent(props: {matchModel: MatchModel}) {

    let isPredictionCorrect: boolean = props.matchModel.prediction.calculatedPrediction === props.matchModel.prediction.finalResult;

    // let rootStyle: string = 'w-full flex flex-col p-2 pb-3 border ' + (isPredictionCorrect ? " bg-emerald-50" : " bg-rose-50");
    let rootStyle: string = 'w-full flex flex-col p-2 pb-3 border  bg-white';
    // {'w-full flex flex-col p-2 pb-3' + (isPredictionCorrect ? "bg-rose-400" : "bg-emerald-400")}

    if (props.matchModel.matchObject && props.matchModel.matchObject.teams) {
        return (
            <a href={generatePageMatchTipPath(props.matchModel, props.matchModel.matchObject.league.name)} className={rootStyle} style={{textDecoration: "none"}}>
                <div className='w-full flex align-items-center'>
                    <div className='mr-4'>
                        <Image src={isPredictionCorrect ? "/icons/check-128.png" : "/icons/cross-128.png"} width='20'/>
                    </div>
                    <div className='flex flex-column justify-content-center text-slate-500 mr-2'>
                        <span className='text-sm text-center'>{dateFormat(new Date(props.matchModel.matchObject.fixture.date), "mm/dd")}</span>
                        <span className='text-base text-center'>{dateFormat(new Date(props.matchModel.matchObject.fixture.date), "HH:MM")}</span>
                    </div>

                    <div className='flex flex-col justify-content-start align-items-start p-2 basis-1/3'>
                        <div className='flex flex-grow justify-content-start align-items-center'>
                            {AppConfigs.renderLogo(props.matchModel.matchObject.teams.home.name, props.matchModel.matchObject?.teams.home.logo, 30)}
                            <span className='mx-2 text-left text-slate-700'>{AppConfigs.getTeamName(props.matchModel.matchObject.teams.home.name)}</span>
                        </div>
                        <div className='flex flex-grow justify-content-start align-items-center mt-3'>
                            {AppConfigs.renderLogo(props.matchModel.matchObject.teams.away.name, props.matchModel.matchObject?.teams.away.logo, 30)}
                            <span className='mx-2 text-left text-slate-700'>{AppConfigs.getTeamName(props.matchModel.matchObject.teams.away.name)}</span>
                        </div>
                    </div>
                    <div className="hidden sm:flex flex-col justify-content-center">
                        <div className="ml-2 mb-1 flex w-full justify-content-center">
                            <MatchOddsComponent
                                showLabel={true}
                                homeOdds={props.matchModel.oddsAvg.home}
                                drawOdds={props.matchModel.oddsAvg.draw}
                                awayOdds={props.matchModel.oddsAvg.away}
                                prediction={props.matchModel.prediction.calculatedPrediction}
                                finalResult={props.matchModel.prediction.finalResult}/>
                        </div>

                        <div className='mt-2'>
                            {genPredictionAndResultContainer(props.matchModel)}
                        </div>
                    </div>
                </div>
                <div className='mt-1 flex-col visible sm:hidden'>
                    <div className="ml-2 mb-1 flex w-full justify-content-center">
                        <MatchOddsComponent
                            showLabel={true}
                            homeOdds={props.matchModel.oddsAvg.home}
                            drawOdds={props.matchModel.oddsAvg.draw}
                            awayOdds={props.matchModel.oddsAvg.away}
                            prediction={props.matchModel.prediction.calculatedPrediction}
                            finalResult={props.matchModel.prediction.finalResult}/>
                    </div>
                    {genPredictionAndResultContainer(props.matchModel)}
                </div>
            </a>
        )
    } else {
        return (
            <div>
                Teams not found
            </div>
        )
    }
}

function genPredictionAndResultContainer(matchData: MatchModel) {
    let predictionColor: string = matchData.prediction.calculatedPrediction === matchData.prediction.finalResult ? "border-green-700 text-green-700" : "border-red-400 text-red-400";
    return (
        <div className='flex align-items-center justify-content-center text-xs'>
            <div className='flex align-items-center'>
                <span className='text-black'>Our Tip: </span>
                <div className={'mx-2 border p-1 ' + predictionColor}>{convertPredictionToText(matchData.prediction.calculatedPrediction)}</div>
            </div>
            <div className='flex align-items-center ml-3'>
                <span className='text-black'>Final Result: </span>
                <div className={'mx-2 border p-1 border-slate-400 text-slate-900'}>{convertPredictionToText(matchData.prediction.finalResult)}</div>
            </div>
        </div>
    )
}

function convertPredictionToText(prediction: MatchTipType) {
    switch (prediction) {
        case MatchTipType.HOME: return "Home team";
        case MatchTipType.AWAY: return "Away team";
        case MatchTipType.DRAW: return "Draw";
        case MatchTipType.UNPREDICTABLE: return "Can't predict the match";
    }
}