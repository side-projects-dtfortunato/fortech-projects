import MatchModel from "../../data/model/Match.model";
import {Button, Card, Image} from "react-bootstrap";
import dateFormat from "dateformat";
import {useRouter} from "next/router";
import {generatePageMatchTipPath} from "../../pages/match-tip/[id]";
import MatchOddsComponent from "../match-tips/MatchOdds.component";
import Link from "next/link";
import {AppConfigs} from "../../data/AppConfigs";


export default function MatchPredictedComponent(props: {matchModel: MatchModel,
        leagueLabel: string,
        showTIPBtn?: boolean,
        showPrediction?: boolean}) {

    const router = useRouter();
    async function onShowTipClick() {
        await router.push(generatePageMatchTipPath(props.matchModel, props.leagueLabel));
    }

    function showTipBtn() {
        if (props.showTIPBtn) {
            return (<Link href={generatePageMatchTipPath(props.matchModel, props.leagueLabel)} className={"rounded border-green-500 text-green-500 px-5 py-2"}>SHOW TIP</Link>);
        } else {
            return (<></>);
        }
    }

    function showPrediction() {
        if (props.showPrediction) {
            return (
                <div className='flex justify-content-center align-items-center'>
                    <span className='text-base text-slate-500'>Prediction:</span>
                    <div className='mx-1 border border-green-400 p-2 text-green-400 text-xs'>{props.matchModel.prediction.calculatedPrediction}</div>
                </div>
            )
        }
    }

    let rootClassName = 'w-full bg-white p-2 pb-3 flex flex-col';
    if (props.showTIPBtn) {
        rootClassName += " cursor-pointer";
    }


    return (
        <a href={props.showTIPBtn ? generatePageMatchTipPath(props.matchModel, props.leagueLabel) : ""} className={rootClassName} style={{textDecoration: "none"}}>
            <div className='w-full flex justify-content-start align-items-center p-2'>
                <div className='flex flex-column justify-content-center text-slate-500 mr-2'>
                    <span className='text-sm text-center'>{dateFormat(new Date(props.matchModel.matchObject.fixture.date), "mm/dd")}</span>
                    <span className='text-base text-center'>{dateFormat(new Date(props.matchModel.matchObject.fixture.date), "HH:MM")}</span>
                </div>

                <div className='flex flex-col justify-content-start align-items-start p-2 basis-1/3'>
                    <div className='flex flex-grow justify-content-start align-items-center'>
                        {AppConfigs.renderLogo(props.matchModel.matchObject.teams.home.name, props.matchModel.matchObject?.teams.home.logo, 30)}
                        <span className='mx-2 text-left text-slate-700'>{AppConfigs.getTeamName(props.matchModel.matchObject.teams.home.name)}</span>
                    </div>
                    <div className='flex flex-grow justify-content-start align-items-center mt-3'>
                        {AppConfigs.renderLogo(props.matchModel.matchObject.teams.away.name, props.matchModel.matchObject?.teams.away.logo, 30)}
                        <span className='mx-2 text-left text-slate-700'>{AppConfigs.getTeamName(props.matchModel.matchObject.teams.away.name)}</span>
                    </div>
                </div>

                <div className="hidden sm:flex">
                    <MatchOddsComponent
                        showLabel={true}
                        homeOdds={props.matchModel.oddsAvg.home}
                        drawOdds={props.matchModel.oddsAvg.draw}
                        awayOdds={props.matchModel.oddsAvg.away}
                        prediction={props.showPrediction ? props.matchModel.prediction.calculatedPrediction : null}/>
                </div>
                {
                    /*<div className='flex grow justify-end'>
                        {showTipBtn()}
                    </div>*/
                }
            </div>
            <div className="visible flex w-full justify-content-center sm:hidden">
                <MatchOddsComponent
                    showLabel={true}
                    homeOdds={props.matchModel.oddsAvg.home}
                    drawOdds={props.matchModel.oddsAvg.draw}
                    awayOdds={props.matchModel.oddsAvg.away}
                    prediction={props.showPrediction ? props.matchModel.prediction.calculatedPrediction : null}/>
            </div>
        </a>
    )
}