import Image from 'next/image'


export default function FollowUsTwitterComponent() {

    return (
        <div className='flex flex-col w-full bg-white p-2 justify-content-center align-items-center'>
            <div className='flex align-items-center'>
                <div style={{width: 30}}>
                    <Image src='/icons/ic_twitter.png' width={30} height={30}/>
                </div>
                <h4 className='ml-2 text-black text-center'>Follow us on Twitter</h4>
            </div>
            <span className='text-slate-500 text-center my-1 text-xs'>To receive our tips and don&apos;t miss any profitable bet prediction</span>
            <div className='flex justify-content-center align-items-center mt-2'>
                <div className='visible'>
                    <a href="https://twitter.com/FootballTipsApp?ref_src=twsrc%5Etfw" className="twitter-follow-button"
                       data-show-count="false">Follow @FootballTipsApp</a>
                    <script async src="https://platform.twitter.com/widgets.js" charSet="utf-8"></script>
                </div>
            </div>
        </div>
    )
}