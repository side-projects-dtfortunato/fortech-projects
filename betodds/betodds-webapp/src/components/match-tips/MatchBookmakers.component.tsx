import MatchModel, {MatchTipType} from "../../data/model/Match.model";
import MatchOddsComponent from "./MatchOdds.component";
import {AffiliateDataModel, AffiliateHelper} from "../../utils/AffiliateHelper";

interface BookmakerOddsData {
    label: string;
    homeOdd?: number;
    drawOdd?: number;
    awayOdd?: number;
}

export default function MatchBookmakersComponent(props: {match: MatchModel}) {
    if (!props.match.oddsDetails || !props.match.oddsDetails.bookmakers) {
        return (<></>);
    }

    let listBookmakers: BookmakerOddsData[] = exportBookmakersData(props.match);
    listBookmakers = listBookmakers.filter((item) => item && item.awayOdd && item.drawOdd && item.homeOdd);

    // Sort by odds with the prediction
    if (props.match.prediction.calculatedPrediction !== MatchTipType.UNPREDICTABLE) {
        listBookmakers = listBookmakers.sort((item1, item2) => {
            switch (props.match.prediction.calculatedPrediction) {
                case MatchTipType.HOME: return item2.homeOdd - item1.homeOdd;
                case MatchTipType.DRAW: return item2.drawOdd - item1.drawOdd;
                case MatchTipType.AWAY: return item2.awayOdd - item1.awayOdd;
            }
            // Fallback, compare homeOdd
            return item2.homeOdd - item1.homeOdd;
        });

        // Sort by recommended bookers
        listBookmakers = listBookmakers.sort((item1, item2) => {
            if (AffiliateHelper.hasAffiliateId(item1.label)) {
                return -1;
            } else if (AffiliateHelper.hasAffiliateId(item2.label)) {
                return 1;
            } else {
                return 0;
            }
        });
    }

    function genListBookmakers() {
        return listBookmakers.map((item) => {
            let affiliateData: AffiliateDataModel = AffiliateHelper.getAffiliateLink(item.label);

            if (affiliateData) {
                return (
                    <a href={affiliateData.link} style={{textDecoration: "none"}} key={item.label}>
                        <div className={'mb-1 flex align-items-center p-2 ' + (affiliateData ? "bg-amber-50" : "bg-white")}>
                            <div className='flex flex-col sm:flex-row'>
                                <span className='text-slate-900 mr-1'>{item.label}</span>
                                <div className='border rounded px-1 bg-amber-400 text-white' style={{fontSize: 12}}>
                                    Recommended
                                </div>
                            </div>
                            <div className='flex flex-wrap ml-1 grow justify-content-center align-items-end'>
                                <MatchOddsComponent showLabel homeOdds={item.homeOdd} drawOdds={item.drawOdd} awayOdds={item.awayOdd} prediction={props.match.prediction.calculatedPrediction} />
                                <div className='border rounded bg-green-600 text-white px-2 py-1 mt-1 mb-1' style={{fontSize: 14}}>
                                    BET NOW
                                </div>
                            </div>

                        </div>
                    </a>
                )
            } else {
                return (
                    <div className={'mb-1 flex align-items-center p-2 bg-white'} key={item.label}>
                        <span className='text-slate-900 w-25'>{item.label}</span>
                        <div className='flex ml-2 grow justify-content-center'>
                            <MatchOddsComponent showLabel homeOdds={item.homeOdd} drawOdds={item.drawOdd} awayOdds={item.awayOdd} prediction={props.match.prediction.calculatedPrediction} />
                        </div>
                    </div>
                )
            }

        })
    }

    return (
        <div className='flex flex-col justify-content-center'>
            <h5 className='text-black text-center'>Bet Bookmakers</h5>
            {genListBookmakers()}
        </div>
    )

}


/** UTILS **/
function exportBookmakersData(match: MatchModel): BookmakerOddsData[] {
    return match.oddsDetails.bookmakers.map((item) => {
        let bookmakerDataItem:BookmakerOddsData = {
            label: item.name,
        };

        if (item.bets[0].values) {
            item.bets[0].values.forEach((oddValue) => {

                switch (oddValue.value) {
                    case "Home": bookmakerDataItem.homeOdd = Number(oddValue.odd);
                    case "Draw": bookmakerDataItem.drawOdd = Number(oddValue.odd);
                    case "Away": bookmakerDataItem.awayOdd = Number(oddValue.odd);
                }
            })
            return bookmakerDataItem;
        } else {
            return null;
        }

    })
}