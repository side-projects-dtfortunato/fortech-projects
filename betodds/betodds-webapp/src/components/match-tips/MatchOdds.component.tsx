import {Card} from "react-bootstrap";
import {MatchTipType} from "../../data/model/Match.model";

export default function MatchOddsComponent(props: { homeOdds: number, drawOdds: number, awayOdds: number, showLabel: boolean, hideOdds?: boolean, prediction?: MatchTipType, finalResult?: MatchTipType }) {

    function genOddsContainer(label: string, odds: number, oddsType: MatchTipType) {
        let oddsColors = "#708090";

        if (oddsType === props.finalResult) {
            oddsColors = "#00FF00";
        } else if (oddsType === props.prediction) {
            // Check if the prediction was wrong
            if (props.finalResult && props.prediction !== props.finalResult) {
                oddsColors = "#FF0000";
            } else {
                oddsColors = "#00FF00";
            }
        }


        let renderOdd = props.hideOdds ? (<></>) : (
            <div className='justify-content-center align-items-center text-slate-900 border rounded text-center px-2 py-1'
                 style={{borderColor: oddsColors, color: oddsColors}}>{odds.toFixed(2)}</div>
        )

        return (
            <div className='shrink flex flex-col justify-content-center mx-2'>
                <span className='text-xs text-slate-500 mb-1 text-center'>{props.showLabel ? label : ""}</span>
                {renderOdd}
            </div>
        )
    }

    return (
        <div className='flex'>
            {genOddsContainer("1",
                props.homeOdds,
                MatchTipType.HOME)}
            {genOddsContainer("X",
                props.drawOdds,
                MatchTipType.DRAW)}
            {genOddsContainer("2",
                props.awayOdds,
                MatchTipType.AWAY)}
        </div>
    )
}