import MatchModel, {MatchTipType} from "../../data/model/Match.model";
import {useRouter} from "next/router";
import {Image, ProgressBar} from "react-bootstrap";

/** List All Tips Predictions */
export default function MatchTipPredictionsComponent(props: {match: MatchModel}) {
    const router = useRouter();

    function renderChartPrediction() {
        let totalPrediction: number = props.match.prediction.homeValuePrediction + props.match.prediction.drawValuePrediction + props.match.prediction.awayValuePrediction;
        let homePercent: number = Math.round((props.match.prediction.homeValuePrediction * 100) / totalPrediction);
        let drawPercent: number = Math.round((props.match.prediction.drawValuePrediction * 100) / totalPrediction);
        let awayPercent: number = Math.round((props.match.prediction.awayValuePrediction * 100) / totalPrediction);
        let homeColor: string = getColorByProbability(homePercent);
        let drawColor: string = getColorByProbability(drawPercent);
        let awayColor: string = getColorByProbability(awayPercent);


        return (
            <div className='bg-white mb-1 flex flex-col gap-2 w-full align-items-stretch p-4' key="chartPrediction">
                <div className='flex flex-col'>
                    <span className={'text-['+homeColor + "]"}>{props.match.matchObject.teams.home.name} winning:</span>
                    <ProgressBar variant={homeColor} now={homePercent} label={`${homePercent}%`} />
                </div>
                <div className='flex flex-col'>
                    <span className='text-black'>Draw:</span>
                    <ProgressBar variant={drawColor} now={drawPercent} label={`${drawPercent}%`} />
                </div>
                <div className='flex flex-col'>
                    <span className='text-black'>{props.match.matchObject.teams.away.name} winning:</span>
                    <ProgressBar variant={awayColor} now={awayPercent} label={`${awayPercent}%`} />
                </div>
            </div>
        )
    }

    function genAllPridctionsSources() {
        let listTips = [];

        // Our Prediction
        listTips.push(
            (
                <div className='font-bold' key='prediction-header'>
                    {genPredictionSource("Our Prediction", props.match.prediction.calculatedPrediction, props.match.prediction.finalResult)}
                </div>
            )
        );

        if (props.match.tips) {
            Object.values(props.match.tips).forEach((matchTip) => {
                listTips.push(genPredictionSource(matchTip.sourceLabel, matchTip.tipSuggestion, props.match.prediction.finalResult))
            })
        }

        return listTips;
    }

    function genPredictionSource(label: string, matchTip: MatchTipType, matchResult?: MatchTipType) {

        function renderMatchResult() {
            if (matchResult) {
                const isPredictionCorrect: boolean = matchTip === matchResult;
                return (<Image src={isPredictionCorrect ? "/icons/check-128.png" : "/icons/cross-128.png"} width='20'/>)
            } else {
                return (<></>);
            }
        }

        return (
            <div className='bg-white mb-1 flex w-full align-items-center p-4' key={label}>
                <span className='text-slate-900'>{label.toUpperCase()}:</span>
                <div className='mx-2 border border-green-400 p-2 text-green-400 text-xs'>{matchTip}</div>
                {renderMatchResult()}
            </div>
        )
    }

    return (
        <div className='w-full flex flex-col justify-content-center pt-1 pb-1'>
            <div className='bg-white mb-1 p-3'>
                <h5>Match Prediction Tips</h5>
                <span className='text-slate-300 text-xs'>We calculate our predictions base on the average of other predictors and with our own prediction algorithm.</span>
            </div>
            {renderChartPrediction()}
            {genAllPridctionsSources()}
        </div>
    );
}


function getColorByProbability(percentage: number) {
    if (percentage >= 75) {
        return "success";
    } else if (percentage > 40) {
        return "warning";
    } else {
        return "danger"
    }
}