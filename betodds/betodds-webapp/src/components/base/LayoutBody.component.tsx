export default function LayoutBodyComponent(props) {
    return (
        <div style={{minHeight: 800}}>
            {props.children}
        </div>
    )
}