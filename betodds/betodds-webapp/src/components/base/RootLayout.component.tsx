import NavbarComponent from "./navbar/Navbar.component";
import Head from 'next/head'
import {useRouter} from "next/router";
import Utils from "../../utils";
import FooterComponent from "./Footer.component";
import {NextMatchesByLeague} from "../../data/model/LeagueMatches.model";
import GoogleAdsComponent from "../ads/GoogleAds.component";
import SubscribeNewsletterComponent from "../SubscribeNewsletter.component";
import OneSignal from "react-onesignal";
import {useEffect, useState} from "react";

export interface PageHeadProps {
    title: string;
    description: string;
    imgUrl?: string;
}

export default function RootLayoutComponent(props: { pageHeadProps?: PageHeadProps, children, nextMatches?: NextMatchesByLeague, customBody?: boolean }) {
    const router = useRouter();
    const [isOneSignalInit, setOneSignalInit] = useState(false);

    async function initOneSignal() {
        if (!isOneSignalInit) {
            setOneSignalInit(true);
            await OneSignal.init({
                appId: "f6cddeef-447b-434d-b1fa-2fc90a09f84c"});
            OneSignal.Slidedown.promptPush();

        }
    }

    useEffect(() => {
        initOneSignal();
    }, []);

    function genSocialMeta(pageHead: PageHeadProps) {

        let imageUrl: string = pageHead.imgUrl;
        if (!imageUrl) {
            imageUrl = Utils.BASE_URL + "/images/logo.png";
        }
        let url = Utils.BASE_URL + router.route;
        return (
            <>
                <meta name="twitter:card" content="summary"/>
                <meta name="twitter:url" content={url}/>
                <meta name="twitter:title" content={pageHead.title}/>
                <meta name="twitter:description" content={pageHead.description}/>
                <meta name="twitter:image" content={imageUrl}/>
                <meta name="twitter:creator" content={Utils.SITE_NAME}/>
                <meta property="og:type" content="website"/>
                <meta property="og:title" content={pageHead.title}/>
                <meta property="og:description" content={pageHead.description}/>
                <meta property="og:site_name" content="FootballTips-Today.com"/>
                <meta property="og:url" content={url}/>
                <meta property="og:image" content={imageUrl}/>
            </>
        )
    }

    function genHead() {
        return props.pageHeadProps ? (
            <Head>
                <title>{props.pageHeadProps.title}</title>
                <meta name="description" content={props.pageHeadProps.description}/>
                {genSocialMeta(props.pageHeadProps)}
            </Head>
        ) : (<></>);
    }

    function renderBodyContent() {
        if (props.customBody) {
            return props.children;
        } else {
            return (
                <>
                    <div className='hidden lg:flex flex-col justify-end p-1 h-full sticky top-0 z-30'>
                        <div style={{width: 200}}>
                            <GoogleAdsComponent slot={'5014804407'}/>
                        </div>
                    </div>
                    <div className='mx-2 max-w-3xl w-full grow'>
                        {props.children}
                        <GoogleAdsComponent slot={"3865374269"} />
                    </div>
                    <div className='hidden lg:flex justify-content-center p-1 h-full sticky top-0 z-30'>

                        <div style={{width: 200}}>
                            <GoogleAdsComponent slot={'3426390564'}/>
                        </div>
                    </div>
                </>
            )
        }
    }

    return (
        <div className='flex-col bg-slate-100 h-screen w-screen'>
            {genHead()}
            <NavbarComponent></NavbarComponent>
            <div className='flex flex-row bg-slate-100 align-items-start justify-content-center w-screen'>
                {renderBodyContent()}
            </div>
            <div className='w-full pt-3 bg-slate-100'>
                <FooterComponent nextMatches={props.nextMatches}/>
            </div>
        </div>
    )
}

