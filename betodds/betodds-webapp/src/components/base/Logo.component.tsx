import {Image} from "react-bootstrap";
import Navbar from "react-bootstrap/Navbar";

export default function LogoComponent() {
    return (
        <div className='flex align-items-center'>
            <Image className='mr-2' src="/images/logo.png" width="40" alt='FootballTips.app Logo'/>
            <span className='logo text-teal-700 font-bold text-base sm:text-lg'>Football Tips</span>
        </div>
    )
}