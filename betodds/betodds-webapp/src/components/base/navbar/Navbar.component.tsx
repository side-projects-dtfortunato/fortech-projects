import Navbar from 'react-bootstrap/Navbar';
import {Container, Image, Nav} from "react-bootstrap";
import NavbarItemComponent from "./NavbarItem.component";
import {useRouter} from  'next/router';
import LogoComponent from "../Logo.component";



export default function NavbarComponent() {
    const router = useRouter();

    return (
        <div className='drop-shadow-lg'>
            <Navbar className='bg-white' expand="lg" variant="light">
                <Container>
                    <Navbar.Brand className='flex text-black align-items-center' href="/">
                        <LogoComponent/>
                    </Navbar.Brand>
                    <Navbar.Toggle aria-controls="basic-navbar-nav" />
                    <Navbar.Collapse className='w-full' id="basic-navbar-nav">
                        <Nav className="w-full justify-content-center" activeKey={router.asPath}>
                            <NavbarItemComponent href="/" label="Today Tips" icon='/images/today-bet.png'/>
                            <NavbarItemComponent href="/previous-results" label="Previous results" icon='/images/previous-results.png'/>
                        </Nav>
                    </Navbar.Collapse>
                </Container>
            </Navbar>
        </div>
    )
}