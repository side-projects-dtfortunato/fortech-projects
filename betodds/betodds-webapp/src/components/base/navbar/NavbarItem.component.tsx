import {Image, Nav} from "react-bootstrap";
import Link from "next/link";

export default function NavbarItemComponent(props:{href: string, label: string, icon: string}) {
    return (
        <div>
            <Nav.Item className='flex justify-content-center align-items-center mx-5'>
                <Image src={props.icon} width='20'/>
                <Nav.Link href={props.href}>{props.label}</Nav.Link>
            </Nav.Item>
        </div>

    )
}