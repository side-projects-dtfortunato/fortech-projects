export default function SubscribeNewsletterComponent() {
    return (
        <div className='flex justify-content-center w-full' style={{height: 450}}>
            <div dangerouslySetInnerHTML={{__html: "<iframe width=\"540\" height=\"100%\" src=\"https://fe68464e.sibforms.com/serve/MUIEADLR8JnObMgfuWnWlgWFpcY_0VyyduC059e_zkiPDl0R46ae1fSJ1kg5jixqUdwuc3Q2uZTPilKmCEcV8PPXEFG9Adk-0Ide6zRuE0D2dtIOUWsxfED332tB1jXuuWSiSPqR6ei-LDQbb-AdEQb-H3Ku4hfqd36iWEiX1XgGAB0bdmkVByfrlmYbKOMDY8-2uSo3HQHF8-v8\" frameborder=\"0\" scrolling=\"auto\" allowfullscreen style=\"display: block;margin-left: auto;margin-right: auto;max-width: 100%;\"></iframe>"}}>
            </div>
        </div>
    )
}