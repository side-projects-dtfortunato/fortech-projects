import {NextMatchesByLeague} from "../model/LeagueMatches.model";
import {getAPIDocument} from "./firebase";
import {MatchesFinishedModel} from "../model/MatchesFinished.model";

export const FirestoreCollectionDB = {
    Table_OddsBeaterData: "OddsBeaterData",
    Doc_OddsBeaterDataNextMatches: "NextMatchesToPredict",
    Doc_OddsBeaterDataPredictedMatches: "PredictedMatches",
    Doc_OddsBeaterDataFinishedPredictedMatches: "FinishedPredictedMatches",
    Doc_OddsBeaterDataPredictionsSources: "PredictedSources",
}

export class BackendAPI {
    static async getAPIPredictedMatches(): Promise<NextMatchesByLeague | undefined> {
        return await getAPIDocument<NextMatchesByLeague>(FirestoreCollectionDB.Table_OddsBeaterData, FirestoreCollectionDB.Doc_OddsBeaterDataPredictedMatches);
    }

    static async getAPIMatchesFinished(): Promise<MatchesFinishedModel | undefined> {
        return await getAPIDocument<MatchesFinishedModel>(FirestoreCollectionDB.Table_OddsBeaterData, FirestoreCollectionDB.Doc_OddsBeaterDataFinishedPredictedMatches);
    }
}

