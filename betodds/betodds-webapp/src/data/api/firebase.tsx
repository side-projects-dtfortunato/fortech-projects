import * as Firebase from "firebase/app";
// import { getAnalytics, isSupported } from "firebase/analytics";
import {doc, getDoc, getFirestore} from "firebase/firestore"
import { GoogleAuthProvider, getAuth, signInWithPopup, signInWithEmailAndPassword, createUserWithEmailAndPassword, sendPasswordResetEmail, signOut,} from "firebase/auth";

const firebaseConfig = {
    apiKey: "AIzaSyBVwrnRKVtvOOPDSkXzZjTLoTgtDsYtOmE",
    authDomain: "core-fortuly-backend.firebaseapp.com",
    projectId: "core-fortuly-backend",
    storageBucket: "core-fortuly-backend.appspot.com",
    messagingSenderId: "612179893723",
    appId: "1:612179893723:web:bc2a971897f15706399bfe",
    measurementId: "G-77VR89T7K5"
};

// Initialize Firebase
const app = Firebase.initializeApp(firebaseConfig);
const db = getFirestore(app);
// let analytics;

export {db}

/*export async function getAnalyticsInstance() {
    if (!analytics) {
        let isSupported = await isSupported();

        analytics = await getAnalytics(app);
    }

    return analytics;
}*/

export async function getAPIDocument<T>(collectionId: string, docId: string): Promise<T | undefined> {
    const docRef = doc(db, collectionId, docId);
    const docSnap = await getDoc(docRef);

    if (docSnap.exists()) {
        return new Promise((resolve, reject) => {
            resolve({
                ...docSnap.data()
            } as T);
            return
        });
    } else {
        return null;
    }
}
