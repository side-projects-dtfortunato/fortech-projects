import {Image} from "react-bootstrap";

export class AppConfigs {
    static HIDE_LOGOS = false;
    static HIDE_LABELS = false;
    static REMOVE_ADS = false;
    static HIDE_TEAM_NAMES = false;


    static renderLogo(label: string, imageUrl: string, size: number) {
        if (AppConfigs.HIDE_LOGOS) {
            return <></>;
        } else {
            return <Image width={size} src={imageUrl} alt={label} className='ml-4 mr-4'/>
        }
    }

    static getLeagueLabel(label: string) {
        if (!AppConfigs.HIDE_LABELS) {
            return label;
        }

        switch (label.toLowerCase()) {
            case "primeira liga": return "Portuguese League";
            case "serie a": return "Italian Leage";
            case "premier eague": return "English League I";
            case "la liga": return "Spanish League";
            case "championship": return "English League II";
            case "Bundesliga": return "German League";
            case "ligue 1": return "French Leage";
            case "eredivisie": return "Dutch League";
        }
    }

    static getTeamName(originalName: string) {
        if (!AppConfigs.HIDE_TEAM_NAMES) {
            return originalName;
        } else {
            return AppConfigs.createAbbreviation(originalName);
        }
    }

    static createAbbreviation(teamName: string): string {
        const words = teamName.split(' ');
        let abbreviation = words.slice(0, 2).map(word => word[0]).join('').toUpperCase();

        if (abbreviation.length < 3 && words.length > 1) {
            abbreviation += words[words.length - 1][1].toUpperCase();
        }

        if (abbreviation.length < 3) {
            abbreviation = words[0].substring(0, 3).toUpperCase();
        }

        return abbreviation.substring(0, 3);
    }
}