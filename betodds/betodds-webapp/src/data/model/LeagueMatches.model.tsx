import MatchModel from "./Match.model";


export interface NextMatchesByLeague {
    [leagueId: string]: {
        leagueData: LeagueAvailableModel;
        listMatches: {
            [fixtureId: string]: MatchModel;
        },
    },
}

export interface LeagueAvailableModel {
    id: string;
    label: string;
    countryCode: string;
}

export function getLeagueIcon(leagueData: LeagueAvailableModel) {
    return "https://media.api-sports.io/football/leagues/" + leagueData.id + ".png";
}

export function getListMatchesFromNextLeagueMatches(nextMatchesByLeague: NextMatchesByLeague): {matchItem: MatchModel, leagueItem: LeagueAvailableModel}[] {
    let listMatches: {matchItem: MatchModel, leagueItem: LeagueAvailableModel}[] = [];

    if (nextMatchesByLeague) {
        Object.values(nextMatchesByLeague).forEach((leagueItem) => {
           if (leagueItem.listMatches && Object.values(leagueItem.listMatches).length > 0) {

               Object.values(leagueItem.listMatches).forEach((matchItem) => {
                   listMatches.push({
                       leagueItem: leagueItem.leagueData,
                       matchItem,
                   });
               });
           }
        });
    }

    return listMatches;
}