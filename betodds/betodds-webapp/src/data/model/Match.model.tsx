export default interface MatchModel {
    id: string;
    matchObject?: MatchDetailsObject;
    oddsDetails?: MatchOddsDetails;
    oddsAvg?: MatchOddsAvg;
    tips?: {
        [sourceId: string]: SourceMatchTip;
    };
    prediction?: {
        calculatedPrediction: MatchTipType;
        homeValuePrediction?: number;
        drawValuePrediction?: number;
        awayValuePrediction?: number;
        finalResult?: MatchTipType;
    }
    predictionStatus: MatchPredictionStatus;
    metadata?: {};
}

export interface MatchDetailsObject {
    fixture: {
        id: string;
        date: string;
        timestamp: number;
        status?: {
            long: string;
            short: string; // "FT" for finished matches
            elapsed: number;
        }
    },
    league: {
        id: string;
        name: string;
        country: string;
        logo: string;
        flag: string;
        season: number;
        round: string;
    },
    teams: {
        home: {
            id: number;
            name: string;
            logo: string;
            winner: boolean;
        },
        away: {
            id: number;
            name: string;
            logo: string;
            winner: boolean;
        },
    },
}

export interface MatchOddsAvg {
    home: number;
    draw: number;
    away: number;
}

export enum MatchTipType {
    HOME = "HOME", DRAW = "DRAW", AWAY = "AWAY", UNPREDICTABLE = "UNPREDICTABLE"
}

export enum MatchPredictionStatus {
    PENDING = "PENDING", IN_PROGRESS = "IN_PROGRESS", PREDICTION_COMPLETED = "PREDICTION_COMPLETED"
}

export interface SourceMatchTip {
    tipSuggestion: MatchTipType;
    sourceId: string;
    sourceLabel: string;
}

export interface MatchOddsDetails {
    bookmakers: [
        {
            id: number;
            name: string;
            bets: [
                {
                    id: number;
                    name: string;
                    values: [
                        {
                            odd: string;
                            value: "Home" | "Draw" | "Away";
                        }
                    ]
                }
            ]
        }
    ]
}