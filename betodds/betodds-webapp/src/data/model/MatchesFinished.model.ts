import MatchModel from "./Match.model";

export interface MatchesFinishedModel {
    [matchId: string]: MatchModel;
}