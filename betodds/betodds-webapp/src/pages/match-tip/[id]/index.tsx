import MatchModel from "../../../data/model/Match.model";
import RootLayoutComponent, {PageHeadProps} from "../../../components/base/RootLayout.component";
import {getLeagueIcon, LeagueAvailableModel, NextMatchesByLeague} from "../../../data/model/LeagueMatches.model";
import {BackendAPI} from "../../../data/api/backend-api";
import MatchPredictedComponent from "../../../components/list-items/MatchPredicted.component";
import MatchTipPredictionsComponent from "../../../components/match-tips/MatchTipPredictions.component";
import LeagueListItemComponent from "../../../components/list-items/LeagueListItem.component";
import {MatchesFinishedModel} from "../../../data/model/MatchesFinished.model";
import GoogleAdsComponent from "../../../components/ads/GoogleAds.component";
import MatchFinishedComponent from "../../../components/list-items/MatchFinished.component";
import {ResponsiveAdUnit} from "nextjs-google-adsense";

// Generates list of predicted matches
export async function getStaticPaths() {
    // Load Matches
    let nextPredictedMatches: NextMatchesByLeague | undefined = await BackendAPI.getAPIPredictedMatches();


    let listPaths: { params: { id: string } }[] = [];

    if (nextPredictedMatches) {
        // Load matches
        Object.values(nextPredictedMatches).forEach((leagueItem) => {
            if (leagueItem.listMatches && Object.keys(leagueItem.listMatches).length > 0) {
                Object.values(leagueItem.listMatches).forEach((matchData) => {
                    listPaths.push({
                        params: {
                            id: generatePageMatchTipPath(matchData, leagueItem.leagueData.label),
                        },
                    });
                });
            }
        })
    }


    let matchesFinished: MatchesFinishedModel | undefined = await BackendAPI.getAPIMatchesFinished();
    if (matchesFinished) {
        Object.values(matchesFinished).forEach((match: MatchModel) => {
            listPaths.push({
                params: {
                    id: generatePageMatchTipPath(match, match.matchObject.league.name),
                },
            });
        });
    }


    return {
        paths: listPaths,
        fallback: true, // can also be true or 'blocking'
    }
}

// `getStaticPaths` requires using `getStaticProps`
export async function getStaticProps(context) {
    // Load Matches
    let nextPredictedMatches: NextMatchesByLeague | undefined = await BackendAPI.getAPIPredictedMatches();
    const matchId = parseMatchIdFromUrl(context.params.id as string);
    let matchData: MatchModel | undefined;
    let leagueData: LeagueAvailableModel;

    if (nextPredictedMatches) {
        Object.values(nextPredictedMatches).forEach((leagueItem) => {
            if (leagueItem.listMatches && leagueItem.listMatches[matchId]) {
                matchData = leagueItem.listMatches[matchId];
                leagueData = leagueItem.leagueData;
                return;
            }
        });
    }

    // If didn't found the match, try to find it on previous results
    if (!matchData) {
        // Load Matches finished
        let matchesFinished: MatchesFinishedModel | undefined = await BackendAPI.getAPIMatchesFinished();
        if (matchesFinished) {
            matchData = matchesFinished[matchId];
            if (matchData) {
                leagueData = {
                    id: matchData.matchObject.league.id,
                    label: matchData.matchObject.league.name,
                    countryCode: matchData.matchObject.league.country,
                };
            }
        }
    }


    return {
        // Passed to the page component as props
        props: {matchData, leagueData, nextMatches: nextPredictedMatches},
        revalidate: (60 * 12), // 3600 = 1h -> revalidate each 12 hours
    }
}

export default function matchTipDetails(props: { matchData?: MatchModel, leagueData?: LeagueAvailableModel, nextMatches?: NextMatchesByLeague }) {

    function renderMatchItem() {
        if (props.matchData.prediction.finalResult) {
            return (<MatchFinishedComponent matchModel={props.matchData} />);
        } else {
            return (<MatchPredictedComponent matchModel={props.matchData} leagueLabel="" showTIPBtn={false}
                                             showPrediction={true}/>)
        }
    }

    if (props.matchData && props.leagueData) {
        let pageHeadProps: PageHeadProps = {
            title: "Football Match Prediction Tip " + props.matchData.matchObject.teams.home.name + " - " + props.matchData.matchObject.teams.away.name,
            description: "Today bet tip prediction for the match " + props.matchData.matchObject.teams.home.name + " against " + props.matchData.matchObject.teams.away.name + " on the football competition " + props.leagueData.label,
            imgUrl: getLeagueIcon(props.leagueData)
        };

        return (
            <div className='h-screen'>
                <RootLayoutComponent pageHeadProps={pageHeadProps} nextMatches={props.nextMatches}>
                    <GoogleAdsComponent slot={"3865374269"} />
                    <div className='flex flex-col w-full p-2'>
                        <div className='mb-2'>
                            <LeagueListItemComponent leagueData={props.leagueData}/>
                        </div>
                        {renderMatchItem()}
                        <div className='flex w-full mt-3'>
                            <MatchTipPredictionsComponent match={props.matchData}/>
                        </div>
                        {/** <div className='mt-3'>
                         <MatchBookmakersComponent match={props.matchData}/>
                         </div>*/}

                    </div>
                </RootLayoutComponent>
            </div>
        );
    } else {
        <div className='h-screen'>
            <RootLayoutComponent>
            </RootLayoutComponent>
        </div>
    }

}


/*** UTILS **/


export function generatePageMatchTipPath(match: MatchModel, leagueLabel: string) {

    return "/match-tip/"
        + leagueLabel.replaceAll(" ", "-")
        + "-" + match.matchObject.teams.home.name.replaceAll(" ", "-").replaceAll("/", "")
        + "-" + match.matchObject.teams.away.name.replaceAll(" ", "-").replaceAll("/", "")
        + "-" + match.id;
}

function parseMatchIdFromUrl(url: string): string {
    let path: string[] = url.split("-");

    // The last item in the path is the id

    return path[path.length - 1];
}