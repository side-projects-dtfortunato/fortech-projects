// pages/_document.js
import { Html, Head, Main, NextScript } from 'next/document'
import {AppConfigs} from "../data/AppConfigs";

export default function Document() {
    return (
        <Html>
            <Head>

                <meta name="exoclick-site-verification" content="19a338f39f3760ee80974d6f607eca19"/>

                <meta name="application-name" content="FootballTips.app" />
                <meta name="apple-mobile-web-app-capable" content="yes" />
                <meta name="apple-mobile-web-app-status-bar-style" content="default" />
                <meta name="apple-mobile-web-app-title" content="FootballTips.app" />
                <meta name="description" content="FootballTips.app gives with football bets predictions based on Artificial Intelligence and in other bets predictors." />
                <meta name="format-detection" content="telephone=no" />
                <meta name="mobile-web-app-capable" content="yes" />
                <meta name="msapplication-config" content="/icons/browserconfig.xml" />
                <meta name="msapplication-TileColor" content="#FFFFFF" />
                <meta name="msapplication-tap-highlight" content="no" />
                <meta name="theme-color" content="#0F172A" />

                <link rel="apple-touch-icon" href="/images/logo.png" />
                <link rel="icon" type="image/png" sizes="32x32" href="/icons/icon-32x32.png" />
                <link rel="icon" type="image/png" sizes="16x16" href="/icons/icon-16x16.png" />
                <link rel="shortcut icon" href="/favicon.ico" />
                <link rel="manifest" href="/manifest.json" />


                <link
                    href="https://fonts.googleapis.com/css2?family=Inter&display=optional"
                    rel="stylesheet"
                />

                {/*AppConfigs.REMOVE_ADS
                    ? <></>
                    : <script async
                              src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js?client=ca-pub-5492791201497132"
                              crossOrigin="anonymous"></script>*/}

            </Head>
            <body>
            <Main />
            <NextScript />
            </body>
        </Html>
    )
}