import "../styles/globals.css";
import 'bootstrap/dist/css/bootstrap.min.css';
import Script from 'next/script';

function MyApp({
                   Component, pageProps
               }) {

    /* <Script id="Adsense-id"
                    async={true}
                    strategy="beforeInteractive"
                    data-ad-client="ca-pub-5145255847286009"
                    src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"/> */
    return (
        <>

            <Script
                id="gtm_0"
                src="https://www.googletagmanager.com/gtag/js?id=G-77VR89T7K5"
                strategy="afterInteractive"
            />
            <Script id="google-analytics" strategy="afterInteractive">
                {`
              window.dataLayer = window.dataLayer || [];
              function gtag(){window.dataLayer.push(arguments);}
              gtag('js', new Date());
        
              gtag('config', 'G-77VR89T7K5');
            `}
            </Script>

            <Script
                async
                src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js?client=ca-pub-5492791201497132"
                strategy="afterInteractive"
                crossOrigin={"anonymous"}
            />

            <Component {...pageProps} />
        </>
    );
}

export default MyApp;