import {getAPIDocument} from "../../data/api/firebase";

export default async function handler(req, res) {
    console.log(req);

    if (!req.query.colId) {
        res.status(404).json({error: "Param colId not found"});
        return;
    }

    if (!req.query.docId) {
        res.status(404).json({error: "Param docId not found"});
        return;
    }

    let docResponse = await getAPIDocument<any>(req.query.colId, req.query.docId);

    if (docResponse) {
        res.status(200).json(docResponse);
    } else {
        res.status(404).json({error: "Document not found"});
        return;
    }
}