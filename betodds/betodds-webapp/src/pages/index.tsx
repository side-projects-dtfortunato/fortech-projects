import TodayTipsContent from "../pages-content/today-tips.content";
import {NextMatchesByLeague} from "../data/model/LeagueMatches.model";
import {BackendAPI} from "../data/api/backend-api";
import RootLayoutComponent from "../components/base/RootLayout.component";

export async function getStaticProps() {
    // Load Matches
    let nextPredictedMatches: NextMatchesByLeague | undefined = await BackendAPI.getAPIPredictedMatches();

    return {
        props: {
            nextMatchesByLeague: nextPredictedMatches,
        },
        revalidate: (600), // 10 mins
    };
}

export default function Home(props: {nextMatchesByLeague: NextMatchesByLeague}) {
    let metaDescription: string = generateHomePageHeadDescription(props.nextMatchesByLeague);

    return (
        <div className='h-screen'>
            <RootLayoutComponent pageHeadProps={{title: "Football Betting Tips Predictions",
                description: metaDescription}} nextMatches={props.nextMatchesByLeague}>
                <TodayTipsContent nextPredictedMatches={props.nextMatchesByLeague} />
            </RootLayoutComponent>
        </div>
    )
}

/*** UTILS **/

function generateHomePageHeadDescription(nextMatches: NextMatchesByLeague): string {
    let metaDescription: string = "Football betting tips for today.";

    Object.values(nextMatches).forEach((leagueItem) => {
        if (leagueItem.listMatches && Object.values(leagueItem.listMatches).length > 0 && leagueItem.leagueData){
            metaDescription += " Today bet tips of " + leagueItem.leagueData.label + ".";

            Object.values(leagueItem.listMatches).forEach((matchItem) => {
                metaDescription += " Bet tip prediction of " + matchItem.matchObject.teams.home.name;
                metaDescription += " against " + matchItem.matchObject.teams.away.name + ".";
            });

        }

    });

    return metaDescription;
}