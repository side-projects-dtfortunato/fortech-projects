import RootLayoutComponent, {PageHeadProps} from "../components/base/RootLayout.component";
import {MatchesFinishedModel} from "../data/model/MatchesFinished.model";
import {BackendAPI} from "../data/api/backend-api";
import PreviousResultsContent from "../pages-content/previous-results.content";
import GoogleAdsComponent from "../components/ads/GoogleAds.component";


export async function getStaticProps() {
    let matchesFinished: MatchesFinishedModel | undefined = await BackendAPI.getAPIMatchesFinished();

    return {
        props: {
            matchesFinished,
        },
        revalidate: (600), // 10 mins
    };
}

export default function previousResults(props: {matchesFinished: MatchesFinishedModel}) {
    let pageHead: PageHeadProps = {
        title: "FootballTips.app previous tips results",
        description: generatePreviousResultsPageHeadDescription(props.matchesFinished)
    };
    return (
        <div className='h-screen'>
            <RootLayoutComponent pageHeadProps={pageHead}>
                <PreviousResultsContent matchesFinished={props.matchesFinished} />
            </RootLayoutComponent>
        </div>
    )
}


/*** UTILS **/

function generatePreviousResultsPageHeadDescription(matchesFinished: MatchesFinishedModel): string {
    let metaDescription: string = "Our previous results for our football bet predictions. ";

    Object.values(matchesFinished).forEach((matchItem) => {
        if (matchItem.matchObject.teams) {
            metaDescription += " Our result of our bet tip prediction for the match " + matchItem.matchObject.teams.home.name;
            metaDescription += " against " + matchItem.matchObject.teams.away.name + ".";
        }

    });

    return metaDescription;
}