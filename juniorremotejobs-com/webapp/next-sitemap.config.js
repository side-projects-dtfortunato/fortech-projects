/** @type {import('next-sitemap').IConfig} */
module.exports = {
    siteUrl: process.env.SITE_URL || 'https://www.juniorremotejobs.com',
    generateRobotsTxt: true,
    exclude: ["/login", "/login/recover-password", "/edit-post/*", "/edit-profile/user-profile", "/bookmarks", "/privacy-policy", "/profile/user-profile", "/eula"],
}