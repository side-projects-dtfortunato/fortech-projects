import {
    ArticleImportedModel
} from "../../../components/react-shared-module/base-projects/articles-digest/data/model/ArticleImported.model";
import {
    SharedFirestoreCollectionDB
} from "../../../components/react-shared-module/logic/shared-data/datamodel/shared-firestore-collections";
import {
    FirebaseClientFirestoreUtils, getAPIDocument
} from "../../../components/react-shared-module/logic/shared-data/firebase.utils";
import CustomRootLayoutComponent from "../../../components/app-components/root/CustomRootLayout.component";
import ArticleDetailsPageComponent
    from "../../../components/react-shared-module/base-projects/articles-digest/ui-components/article-details-page.component";
import NotFoundContentComponent
    from "../../../components/react-shared-module/ui-components/shared/NotFoundContent.component";
import {PageHeadProps} from "../../../components/react-shared-module/ui-components/root/RootLayout.component";
import RootConfigs from "../../../configs";
import {useEffect} from "react";
import {SharedBackendApi} from "../../../components/react-shared-module/logic/shared-data/sharedbackend.api";
import {jsonLdScriptProps} from "react-schemaorg";
import {NewsArticle} from "schema-dts";
import {SharedRoutesUtils} from "../../../components/react-shared-module/utils/shared-routes.utils";
import {
    DailyPublishedArticlesModel
} from "../../../components/react-shared-module/base-projects/articles-digest/data/model/DailyPublishedArticles.model";
import ArticleDetailsLatestArticlesComponent
    from "../../../components/react-shared-module/base-projects/articles-digest/ui-components/shared/article-details-latest-articles.component";
import CommonUtils from "../../../components/react-shared-module/logic/commonutils";
import useBackButtonRedirectHook
    from "../../../components/react-shared-module/logic/global-hooks/useBackButtonRedirect.hook";


const MAX_LATEST_ARTICLES = 7;

export async function getStaticPaths() {
    let listRecentArticles: ArticleImportedModel[] | undefined = await FirebaseClientFirestoreUtils.getDocumentsWithPagination(SharedFirestoreCollectionDB.ArticlesDigestProject.ArticlePublished, 15, "createdAt");
    let listPaths: any[] = [];

    if (listRecentArticles) {
        listPaths = listRecentArticles
            .map((articleItem) => {
                return {
                    params: {
                        articleId: articleItem.uid!,
                    },
                }
            });
    }

    return {
        paths: listPaths,
        fallback: true, // can also be true or 'blocking'
    }
}

export async function getStaticProps(context: any) {
    const articleId: string = context.params.articleId as string;

    const articleData: ArticleImportedModel | undefined = await getAPIDocument(SharedFirestoreCollectionDB.ArticlesDigestProject.ArticlePublished, articleId);


    const listPublishedDays: DailyPublishedArticlesModel[] = await FirebaseClientFirestoreUtils
        .getDocumentsWithPagination(SharedFirestoreCollectionDB.ArticlesDigestProject.DailyPublishedArticles, 2, "publishedDayId");

    let latestArticles: ArticleImportedModel[] = [];
    listPublishedDays.map((dailyItem) => {
        latestArticles = latestArticles
            .concat(Object.values(dailyItem.listArticles)
                .filter((item) => item.uid !== articleId)).sort((a1, a2) => a2.publishedDate - a1.publishedDate);
    });



    if (articleData) {
        return {
            props: {
                articleData: articleData,
                latestArticles: latestArticles.slice(0, MAX_LATEST_ARTICLES),
            },
            revalidate: 60, // each 60 seconds
        };
    } else {
        return {
            props: {
            },
            revalidate: 60, // each 60 seconds
        }
    }

}


export default function ArticlePageComponent(props: {articleData?: ArticleImportedModel, latestArticles: ArticleImportedModel[]}) {

    useEffect(() => {
        if (props.articleData) {
            SharedBackendApi.incrementArticleViewEvent({
                parentCollectionId: SharedFirestoreCollectionDB.ArticlesDigestProject.ArticlePublished,
                docId: props.articleData?.uid!,
            });
        }
    }, []);

    if (props.articleData) {
        const pageHeadParams: PageHeadProps = {
            title: props.articleData.title.replaceAll("*", ""),
            description: props.articleData.aiGeneratedContent?.socialNetworkPost ? props.articleData.aiGeneratedContent?.socialNetworkPost : CommonUtils.cleanMarkdown(props.articleData.aiGeneratedContent?.summary!).slice(0, 180) + "...",
            imgUrl: props.articleData.imageUrl,
            publishedTime: new Date(props.articleData.createdAt!).toString(),
            jsonLdMarkup: getSchemaMarkup(props.articleData),
            canonicalUrl: `${RootConfigs.BASE_URL}${SharedRoutesUtils.getArticleDetails(props.articleData.uid!)}`
        }
        return (
            <CustomRootLayoutComponent pageHeadProps={pageHeadParams} rigthChilds={<ArticleDetailsLatestArticlesComponent latestArticles={props.latestArticles}/>} >
                <ArticleDetailsPageComponent articleData={props.articleData} latestArticles={props.latestArticles}  />
            </CustomRootLayoutComponent>
        )
    } else {
        return (
            <CustomRootLayoutComponent pageHeadProps={{
                title: RootConfigs.SITE_TITLE + " - Page Not Found",
                description: RootConfigs.SITE_DESCRIPTION,
            }}>
                <NotFoundContentComponent/>
            </CustomRootLayoutComponent>
        )
    }
}

function getSchemaMarkup(articleData: ArticleImportedModel) {
    return (
        <script
            {...jsonLdScriptProps<NewsArticle>(
                {
                    "@context": "https://schema.org",
                    "@type": "NewsArticle",
                    "@id": articleData.uid!,
                    inLanguage: "English",
                    publisher: {
                        "@type": "Organization",
                        url: RootConfigs.BASE_URL,
                        name: RootConfigs.SITE_NAME,
                    },
                    keywords: articleData.aiGeneratedContent?.tags!,
                    description: articleData.aiGeneratedContent?.summary!,
                    thumbnailUrl: articleData.imageUrl!,
                    about: RootConfigs.SITE_DESCRIPTION,
                    url: SharedRoutesUtils.getArticleDetails(articleData.uid!),
                    image: articleData.imageUrl! ? [
                        articleData.imageUrl!,
                    ] : [],
                    articleBody: articleData.aiGeneratedContent?.summary,
                    headline: articleData.title,
                    commentCount: articleData.stats?.commentsCounter,
                    datePublished: new Date(articleData.createdAt!).toLocaleDateString(),
                    dateCreated: new Date(articleData.createdAt!).toLocaleDateString(),
                    dateModified: new Date(articleData.updatedAt!).toLocaleDateString()
                }
            )}
        />
    )
}