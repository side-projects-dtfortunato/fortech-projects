import {
    ArticleImportedModel
} from "../../../../components/react-shared-module/base-projects/articles-digest/data/model/ArticleImported.model";
import {getAPIDocument} from "../../../../components/react-shared-module/logic/shared-data/firebase.utils";
import {
    SharedFirestoreCollectionDB
} from "../../../../components/react-shared-module/logic/shared-data/datamodel/shared-firestore-collections";
import CustomRootLayoutComponent from "../../../../components/app-components/root/CustomRootLayout.component";
import ArticleDetailsPageComponent
    from "../../../../components/react-shared-module/base-projects/articles-digest/ui-components/article-details-page.component";
import AdminThumbnailGeneratorComponent
    from "../../../../components/react-shared-module/ui-components/admin/shared/admin-thumbnail-generator.component";
import AdminOnlyRootComponent
    from "../../../../components/react-shared-module/ui-components/admin/admin-only-root.component";
import NotFoundContentComponent
    from "../../../../components/react-shared-module/ui-components/shared/NotFoundContent.component";

export default function AdminArticleDetailsPage(props: {articleData?: ArticleImportedModel}) {

    function renderSocialNetworkHelper() {
        return (
            <div className='flex flex-col my-10'>
                <AdminThumbnailGeneratorComponent title={props.articleData!.title} bgImageUrl={props.articleData!.imageUrl!} category={props.articleData!.aiGeneratedContent?.category} />
            </div>
        )
    }

    if (props.articleData) {
        return (
            <AdminOnlyRootComponent>
                <CustomRootLayoutComponent isIndexable={false}>
                    {renderSocialNetworkHelper()}
                    <ArticleDetailsPageComponent articleData={props.articleData}  />
                </CustomRootLayoutComponent>
            </AdminOnlyRootComponent>



        )
    } else {
        return (
            <CustomRootLayoutComponent isIndexable={false}>
                <NotFoundContentComponent />
            </CustomRootLayoutComponent>
        )
    }
}


AdminArticleDetailsPage.getInitialProps = async (context: any) => {
    const articleId: string = context.query.articleId as string;
    const articleData: ArticleImportedModel | undefined = await getAPIDocument(SharedFirestoreCollectionDB.ArticlesDigestProject.ArticlePublished, articleId);
    return {
        articleData: articleData
    };
}