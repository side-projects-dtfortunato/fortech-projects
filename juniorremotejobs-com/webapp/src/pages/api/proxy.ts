import { NextApiRequest, NextApiResponse } from 'next';

export const config = {
    api: {
        bodyParser: false,
    },
};

export default async function handler(req: NextApiRequest, res: NextApiResponse) {
    if (req.method !== 'GET') {
        return res.status(405).json({ error: 'Method not allowed' });
    }

    const { url } = req.query;

    if (!url || typeof url !== 'string') {
        return res.status(400).json({ error: 'URL parameter is required' });
    }

    console.log("proxy.url: ", url);
    try {
        const response = await fetch(url);
        const body = await response.arrayBuffer();

        // Copy all headers from the original response
        /*response.headers.forEach((value, key) => {
            res.setHeader(key, value);
        });*/

        // Set the status code
        res.status(response.status);

        // Send the response body
        res.send(Buffer.from(body));
    } catch (error) {
        console.error('Proxy request failed:', error);
        res.status(500).json({ error: 'Failed to fetch the requested URL' });
    }
}