
export default class RootConfigs {
    static ENVIRONMENT_DEV = false;
    static BASE_URL = "https://www.juniorremotejobs.com";
    static SITE_NAME = "JuniorRemoteJobs.com";
    static SITE_TITLE = "Junior Remote Jobs | Find Junior and Entry-Level Remote Job Positions";
    static SITE_DESCRIPTION = "Looking for junior or entry-level remote jobs? JuniorRemoteJobs.com connects you with the best junior remote positions. Start your remote career journey today!";
    static CURRENT_NICHE_SITE = RootConfigs.SITE_NAME;
    static CONTACT_EMAIL = "contact@juniorremotejobs.com";
    static SITE_TOPIC = "Junior Remote Jobs";
    static THEME = {
        PRIMARY_COLOR: "green",
    }
    static META_CONFIGS: any = {
        updateNicheBackend: false,
        disableGoogleSignin: true,
        disableCookies: true,
        disableAppStoreBanner: false,
        disableGoogleAds: false,
        disableArticlesSeparator: true,
        disableCommunitySeparator: true,
        enableJobSearch: true,
    }
    static APP_STORE_LINKS = {
        androidAppId: "juniorremotejobs.com",
        iosAppId: "6737202691",
    }
    static SOCIAL_NETWORKS: {
        [socialId: string]: {
            iconUrl: string,
            label: string,
            link: string,
        },
    } = {
        facebook: {
            label: "Facebook",
            link: "https://www.facebook.com/profile.php?id=61567060929104",
            iconUrl: "/images/ic_social_facebook.png",
        },
        instagram: {
            label: "Instagram",
            link: "https://www.instagram.com/juniorremotejobs/",
            iconUrl: "/images/ic_social_instagram.png",
        },
        threads: {
            label: "Threads",
            link: "https://www.threads.net/@juniorremotejobs",
            iconUrl: "/images/ic_social_threads.png",
        },
        bluesky: {
            label: "Bluesky",
            link: "https://juniorremotejobs.bsky.social",
            iconUrl: "/images/ic_social_bluesky.png",
        },
        telegram: {
            label: "Telegram Community",
            link: "https://t.me/juniorremotejobs",
            iconUrl: "/images/ic_social_threads.png",
        }
    }
    static FIREBASE_CONFIG = this.ENVIRONMENT_DEV ?

        // DEV Config - // TODO Change it
        {
            apiKey: "AIzaSyCB2yrNkEHbpShTH_xwHOzcl2mXJT5i1K8",
            authDomain: "fortuly-qa-env.firebaseapp.com",
            projectId: "fortuly-qa-env",
            storageBucket: "fortuly-qa-env.appspot.com",
            messagingSenderId: "650128691290",
            appId: "1:650128691290:web:69f8a283090d23e0454863",
            measurementId: "G-V6XN6K51VZ"
        } :
        // PROD
        {
            apiKey: "AIzaSyDC4lJdXOEUUSWOjb1Lm119qU1HDj8UqFc",
            authDomain: "juniorremotejobs-com.firebaseapp.com",
            databaseURL: "https://juniorremotejobs-com-default-rtdb.firebaseio.com",
            projectId: "juniorremotejobs-com",
            storageBucket: "juniorremotejobs-com.appspot.com",
            messagingSenderId: "463777556250",
            appId: "1:463777556250:web:258a8be4a20e41e5beaea3",
            measurementId: "G-R6F9T9ZXDV"
        };


    static IMAGEKIT_API = {
        publicKey : "public_OurCtnmj0jWqjVslk5YDfKE4aHQ=",
        privateKey : "private_vRUtPf6HGOj68f5rZZN8u9RHfiQ=",
        urlEndpoint : "https://ik.imagekit.io/fortulyapps",
    }

    static ONE_SIGNAL_API = "9f925702-7ab4-4106-8e8f-9d5e46a32978";
}
