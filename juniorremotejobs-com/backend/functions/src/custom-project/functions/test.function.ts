import * as functions from "firebase-functions";
import * as admin from "firebase-admin";

export const testFunction = functions.https.onRequest(
    async (req, resp) => {

        const totalUsers: number = await getTotalUsers();
        resp.send({totalUsers});

    });

async function getTotalUsers() {
    let userCount = 0;
    let nextPageToken;

    do {
        const listUsersResult: any = await admin.auth().listUsers(1000, nextPageToken);
        userCount += listUsersResult.users.length;
        nextPageToken = listUsersResult.pageToken;
    } while (nextPageToken);

    return userCount;
}