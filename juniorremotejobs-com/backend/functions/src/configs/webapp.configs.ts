
export const PRODUCTION_GCLOUD_PROJECT = "juniorremotejobs-com";

export const WEBAPP_CONFIGS = {
    LOGO_URL: "https://www.juniorremotejobs.com/images/logo-white-bg.jpg",
    SITE_TAGLINE: "Junior Remote Jobs | Find Junior and Entry-Level Remote Job Positions",
    SITE_NAME: "juniorremotejobs.com",
    WEBSITE_URL: "https://juniorremotejobs.com",
    COMPANY_ADDRESS: "3030 Coimbra (Portugal)",
    COMPANY_EMAIL: "contact@juniorremotejobs.com",
    POST_PREFIX_URL: "https://www.juniorremotejobs.com/post/",
    PROFILE_PREFIX_URL: "https://www.juniorremotejobs.com/public-profile/",
    UNSUBSCRIBE_URL: `https://us-central1-${PRODUCTION_GCLOUD_PROJECT}.cloudfunctions.net/unsubscribeEmail?uid=`,


    CUSTOM_CONFIGS: {
        openPushInApp: true,
    } as any,

    // Account: contact@sidehustlify.com - Di.....oc7
    IMAGE_KIT_API: {
        publicKey : "TODO", // "public_QPb3OvUFUKRLd2VLZtV+ExCVDTo=",
        privateKey : "TODO", //"private_z60vq5B2MydsmNQ+c2dMsOG0RBg=",
        urlEndpoint : "TODO", //"https://ik.imagekit.io/sidehustlify",
    },

    STRIPE_API: {
        prod: "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",
        dev: "sk_test_51PueA3HT2hxH9IRDdlrmFSjPbHmjQ1An0PNkz74ZkAOw50c1MSK5nG0MGBWOvbm5RjkBDgG4VXnMDZ2yRBWJGtpA00wlrvUV9J",
        webhook_dev: "whsec_jySvwwZUSe3BLDFdJPYWn6c2vABfZbDd",
        webhook_prod: "whsec_ue2OY5RvpjMUIE8jUqlHuoNoAr0RDxMG",
    },

    ONESIGNAL_CONFIGS: {
        apiKey: "MGE4ZWI2NWUtZTI2OS00N2ZhLWE1M2MtY2YyNDdhZWJiMWEx",
        appId: "9f925702-7ab4-4106-8e8f-9d5e46a32978"
    },

    SOCIAL_NETWORK_API_KEY: {
        metaAccessToken: "EAAh3oPkjTZAEBO1XucYIGTLVrglBKwGIJmg8laGqhqqzZBePuy4flhLXILFAFJiK0lt2ZA7J2uneHuv8x3ZBEC9HTCvYFRIsxSTOzhwLOAwJIdgLJSwDkeiSWzg8rjv75rcAV29wq4nZCSpwVEzghSG5l2yUNQzrAreNZCz6EzdW2fswTuXcM3aUELHmA2GpIZD",
        facebookPageId: "388670671005661",
        instagramId: "17841447260264991",
        threadsAPI: {
            clientId: "1446417969362149",
            clientSecret: "a0c8f000dcbdf843e60e0920e265613f",
            redirectUri: `https://us-central1-${PRODUCTION_GCLOUD_PROJECT}.cloudfunctions.net/threadsAPIHandleAuthCallback`,
        },
        redditAPI: {
            clientId: "4AipG2HfGPnuOhtXtOYEmA",
            clientSecret: "9swPagUhH079Yq3uzQjyimQssUYhow",
            username: "FortulyApps",
            password: "d****c7",
            subReddit: "",
        },
        bluesky: {
            username: "juniorremotejobs.bsky.social",
            password: "divadlooc7",
        },
        telegram: {
            channel: "@juniorremotejobs",
            botToken: "7921577950:AAFLJA_gqJ_fKch0e1BfV7Oqnt9YAjky5Mg",
        }
    }
}