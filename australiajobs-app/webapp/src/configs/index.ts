export default class RootConfigs {
    static ENVIRONMENT_DEV = false;
    static BASE_URL = "https://www.australiajobs.app";
    static SITE_NAME = "AustraliaJobs.app";
    static SITE_TITLE = "Australia Jobs: Find Work & Career Opportunities in Australia";
    static SITE_DESCRIPTION = "Discover the latest job opportunities across Australia. Search thousands of full-time and part-time positions, career advice, and employment resources for working in Australia.";
    static CURRENT_NICHE_SITE = RootConfigs.SITE_NAME;
    static CONTACT_EMAIL = "contact@australiajobs.app";
    static SITE_TOPIC = "Australia Jobs";
    static THEME = {
        PRIMARY_COLOR: "indigo",
    }
    static META_CONFIGS: any = {
        updateNicheBackend: false,
        disableGoogleSignin: true,
        disableCookies: true,
        disableAppStoreBanner: false,
        disableGoogleAds: true,
        enableJobSearch: true,
    }
    static APP_STORE_LINKS = {
        androidAppId: "australiajobs.app",
        iosAppId: "6742204019",
    }
    static SOCIAL_NETWORKS: {
        [socialId: string]: {
            iconUrl: string,
            label: string,
            link: string,
        },
    } = {
        /*facebook: {
            label: "Facebook",
            link: "https://www.facebook.com/profile.php?id=61565899179933",
            iconUrl: "/images/ic_social_facebook.png",
        },
        instagram: {
            label: "Instagram",
            link: "https://www.instagram.com/customer_remote_jobs/",
            iconUrl: "/images/ic_social_instagram.png",
        },
        threads: {
            label: "Threads",
            link: "https://www.threads.net/@customer_remote_jobs",
            iconUrl: "/images/ic_social_threads.png",
        },  
        */
        bluesky: {
            label: "Bluesky",
            link: "https://australiajobs-app.bsky.social",
            iconUrl: "/images/ic_social_bluesky.png",
        },
        telegram: {
            label: "Telegram",
            link: "https://t.me/+e27aT7RK0LA3ZTA0",
            iconUrl: "/images/ic_social_telegram.png",
        }
    }
    static FIREBASE_CONFIG = this.ENVIRONMENT_DEV ?

        // DEV Config
        {
            apiKey: "AIzaSyCB2yrNkEHbpShTH_xwHOzcl2mXJT5i1K8",
            authDomain: "fortuly-qa-env.firebaseapp.com",
            projectId: "fortuly-qa-env",
            storageBucket: "fortuly-qa-env.appspot.com",
            messagingSenderId: "650128691290",
            appId: "1:650128691290:web:69f8a283090d23e0454863",
            measurementId: "G-V6XN6K51VZ"
        } :
        // PROD
        {
            apiKey: "AIzaSyBLiAbgFSYupvHlyWXMuqRW5uoXLlsapPQ",
            authDomain: "australiajobs-app.firebaseapp.com",
            projectId: "australiajobs-app",
            storageBucket: "australiajobs-app.firebasestorage.app",
            messagingSenderId: "730258791409",
            appId: "1:730258791409:web:abf548d9305ab8bc40c250",
            measurementId: "G-PECEM5S1XX"
          };


    static IMAGEKIT_API = {
        publicKey : "public_OurCtnmj0jWqjVslk5YDfKE4aHQ=",
        privateKey : "private_vRUtPf6HGOj68f5rZZN8u9RHfiQ=",
        urlEndpoint : "https://ik.imagekit.io/fortulyapps",
    }

    static ONE_SIGNAL_API = "a690755b-00f4-4dfc-8fb2-73026f1c8a8b";
}
