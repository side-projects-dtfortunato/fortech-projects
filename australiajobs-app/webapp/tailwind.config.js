/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./src/pages/**/*.{js,ts,jsx,tsx}",
    "./src/components/**/*.{js,ts,jsx,tsx}",
  ],
  theme: {
    extend: {},
  },
  plugins: [require("@tailwindcss/typography"), require("daisyui")],
  daisyui: {
    themes: [
      {
        mytheme: {
          "primary": "#4338ca",    // indigo-600: Professional and trustworthy
          
          "secondary": "#048A81",  // dark cyan: Fresh and modern contrast
          
          "accent": "#8b5cf6",     // violet-500: Vibrant complementary color
          
          "neutral": "#1f2937",    // gray-800: Deep neutral for text
          
          "base-100": "#ffffff",   // Pure white for background
          
          "info": "#60a5fa",       // blue-400: Informational elements
          
          "success": "#10b981",    // emerald-500: Success states
          
          "warning": "#f59e0b",    // amber-500: Warning states
          
          "error": "#ef4444",      // red-500: Error states
        }
      }
    ]
    // themes: ["winter"],
  },
  corePlugins: {
    preflight: false,
  },
}
