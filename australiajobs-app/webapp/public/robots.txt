# *
User-agent: *
Allow: /

# Host
Host: https://www.australiajobs.app

# Sitemaps
Sitemap: https://www.australiajobs.app/sitemap.xml
