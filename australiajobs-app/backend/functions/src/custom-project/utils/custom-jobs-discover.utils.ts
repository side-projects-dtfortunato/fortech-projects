import {
    JobDetailsModel, JobWorkModeType
} from "../../shareable-firebase-backend/base-projects/jobs-discover/shared-jobs-discover.data";


/**
 * Regions ids:
 * - UK: 101165590
 * - Australia: 101452733
 * - USA: 103644278
 * - EU: 91000002
 */
/*export const LINKEDIN_SOURCES: LinkedinSourceModel[] = [
    {
        id: "1",
        category: "Software Development",
        region: "USA",
        url: "https://www.linkedin.com/jobs/search?keywords=Software Engineer&geoId=107766340&f_TPR=&f_WT=2&f_E=2%2C3%2C4%2C5&position=1&pageNum=0",
    },
]*/

export const JOB_REGIONS = [
    {
        regionLabel: "Adelaide",
        regionId: "90009516"
    },
    {
        regionLabel: "Brisbane",
        regionId: "90009518"
    },
    {
        regionLabel: "Canberra",
        regionId: "106089960"
    },
    {
        regionLabel: "Darwin",
        regionId: "90009519"
    },
    {
        regionLabel: "Geelong",
        regionId: "102367614"
    },
    {
        regionLabel: "Gold Coast",
        regionId: "90009515"
    },
    {
        regionLabel: "Hobart",
        regionId: "90009520"
    },
    {
        regionLabel: "Melbourne",
        regionId: "90009521"
    },
    {
        regionLabel: "Newcastle",
        regionId: "90009522"
    },
    {
        regionLabel: "Perth",
        regionId: "90009523"
    },
    {
        regionLabel: "Sidney",
        regionId: "90009524"
    },
    {
        regionLabel: "Wollongong",
        regionId: "90009527"
    }
] as {regionLabel: string, regionId: string}[];

export const JOB_CATEGORIES = [
    {
        jobCategoryLabel: "Accounting",
        jobCategoryQuery: "Accounting",
    },
    {
        jobCategoryLabel: "Administrative",
        jobCategoryQuery: "Administrative",
    },
    {
        jobCategoryLabel: "Business Development",
        jobCategoryQuery: "Business Development",
    },
    {
        jobCategoryLabel: "Consulting",
        jobCategoryQuery: "Consulting",
    },
    {
        jobCategoryLabel: "Construction",
        jobCategoryQuery: "Construction",
    },
    {
        jobCategoryLabel: "Customer Service",
        jobCategoryQuery: "Customer Service",
    },
    {
        jobCategoryLabel: "Design",
        jobCategoryQuery: "Design",
    },
    {
        jobCategoryLabel: "Education",
        jobCategoryQuery: "Education",
    },
    {
        jobCategoryLabel: "Engineering",
        jobCategoryQuery: "Engineering",
    },
    {
        jobCategoryLabel: "Finance",
        jobCategoryQuery: "Finance",
    },
    {
        jobCategoryLabel: "Healthcare",
        jobCategoryQuery: "Healthcare",
    },
    {
        jobCategoryLabel: "Human Resources",
        jobCategoryQuery: "Human Resources",
    },
    {
        jobCategoryLabel: "Legal",
        jobCategoryQuery: "Legal",
    },
    {
        jobCategoryLabel: "Manufacturing",
        jobCategoryQuery: "Manufacturing",
    },
    {
        jobCategoryLabel: "Marketing",
        jobCategoryQuery: "Marketing",
    },
    {
        jobCategoryLabel: "Operations",
        jobCategoryQuery: "Operations",
    },
    {
        jobCategoryLabel: "Project Management",
        jobCategoryQuery: "Project Management",
    },
    {
        jobCategoryLabel: "Sales",
        jobCategoryQuery: "Sales",
    },
    {
        jobCategoryLabel: "Software Development",
        jobCategoryQuery: "Software Engineer",
    }
] as {jobCategoryLabel: string, jobCategoryQuery: string}[];


export const ALLOWED_WORK_MODE = ["REMOTE", "ON-SITE", "HYBRID"] as JobWorkModeType[];


export class CustomJobsDiscoverUtils {

    static importJobsFromRemoteJobsHub(): JobDetailsModel[] {
        return [];
    }

    static getIsValidRule() {
        return "";
    }

}