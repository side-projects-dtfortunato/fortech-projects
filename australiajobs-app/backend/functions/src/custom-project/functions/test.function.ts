import * as functions from "firebase-functions";
import { generateEmailTemplate } from "../../shareable-firebase-backend/email-templates/email-templates/email-digest-generator.utils";
import { WEBAPP_CONFIGS } from "../../configs/webapp.configs";
import { BackendRouteUtils } from "../../shareable-firebase-backend/utils/backend-route.utils";
import { getQuickResponse } from "../../shareable-firebase-backend/model-data/api-response.model";
import { ArticleImportedModel } from "../../shareable-firebase-backend/base-projects/articles-digest/data/model/ArticleImported.model";
import { JobDetailsModel } from "../../shareable-firebase-backend/base-projects/jobs-discover/shared-jobs-discover.data";
import { getRecentDocuments } from "../../shareable-firebase-backend/utils/utils";
import { SharedFirestoreCollectionDB } from "../../shareable-firebase-backend/model-data/shared-firestore-collections";
import { CommunityCommentPostModel } from "../../shareable-firebase-backend/base-projects/community/data/community-data.model";

export const testFunction = functions.https.onRequest(
    async (req, resp) => {

        // Get content
    const listArticles: ArticleImportedModel[] = await getRecentDocuments(SharedFirestoreCollectionDB.ArticlesDigestProject.ArticlePublished,24, 10, "createdAt");
    const listJobs: JobDetailsModel[] = await getRecentDocuments(SharedFirestoreCollectionDB.JobsDiscoverProject.JobDetails,24*3, 30, "createdAt");
    const listCommunityPosts: CommunityCommentPostModel[] = await getRecentDocuments(SharedFirestoreCollectionDB.CommunityCommentPosts, 24, 12, "createdAt");

    if  (listJobs.length === 0 && listArticles.length === 0 && listCommunityPosts.length === 0) {
        resp.send(getQuickResponse(false, null, "No content to digest"));
        return;
    }

    // Sort jobs
    listJobs.sort((a, b) => {
        // Featured jobs first (1 for featured, 0 for others)
        const aFeatured = a.publishPlanType === 'FEATURED' ? 1 : 0;
        const bFeatured = b.publishPlanType === 'FEATURED' ? 1 : 0;
        if (aFeatured !== bFeatured) return bFeatured - aFeatured;

        // Then by views counter (descending)
        const aViews = (a as any).stats?.viewsCounter || 0;
        const bViews = (b as any).stats?.viewsCounter || 0;
        if (aViews !== bViews) return bViews - aViews;

        // Finally by creation date (newest first)
        return (b.createdAt || 0) - (a.createdAt || 0);
    });

    // Generate Email Digest HTML
    const emailGenerator = generateEmailTemplate({
        introText: "Daily Digest",
        unsubscribeLink: WEBAPP_CONFIGS.UNSUBSCRIBE_URL,
        articles: listArticles,
        communityPosts: listCommunityPosts.map((communityItem) => {
            return {
                userName: communityItem.usersInfos[communityItem.userCreatorId].name,
                userAvatar: communityItem.usersInfos[communityItem.userCreatorId].pictureUrl,
                postText: communityItem.message,
                link: BackendRouteUtils.getCommunityDetailsPage(communityItem.uid!),
            };
        }),
        jobPositions: listJobs,
        socialNetworks: [],
    });


        resp.send(emailGenerator.html);

    });