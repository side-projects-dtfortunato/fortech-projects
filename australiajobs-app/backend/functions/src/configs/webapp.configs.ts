
export const PRODUCTION_GCLOUD_PROJECT = "australiajobs-app";

export const WEBAPP_CONFIGS = {
    LOGO_URL: "https://www.australiajobs.app/images/logo-white-bg.jpg",
    SITE_TAGLINE: "Find Work & Career Opportunities in Australia",
    SITE_NAME: "AustraliaJobs.app",
    WEBSITE_URL: "https://www.australiajobs.app",
    COMPANY_ADDRESS: "3030 Coimbra (Portugal)",
    COMPANY_EMAIL: "contact@australiajobs.app",
    POST_PREFIX_URL: "https://www.australiajobs.app/post/",
    PROFILE_PREFIX_URL: "https://www.australiajobs.app/public-profile/",
    UNSUBSCRIBE_URL: `https://us-central1-${PRODUCTION_GCLOUD_PROJECT}.cloudfunctions.net/unsubscribeEmail?uid=`,

    MAILERSEND_API_KEY: "TODO",

    CUSTOM_CONFIGS: {
        openPushInApp: true,
    } as any,

    // Account: contact@sidehustlify.com - Di.....oc7
    IMAGE_KIT_API: {
        publicKey : "TODO", // "public_QPb3OvUFUKRLd2VLZtV+ExCVDTo=",
        privateKey : "TODO", //"private_z60vq5B2MydsmNQ+c2dMsOG0RBg=",
        urlEndpoint : "TODO", //"https://ik.imagekit.io/sidehustlify",
    },

    STRIPE_API: {
        prod: "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",
        dev: "sk_test_51PueA3HT2hxH9IRDdlrmFSjPbHmjQ1An0PNkz74ZkAOw50c1MSK5nG0MGBWOvbm5RjkBDgG4VXnMDZ2yRBWJGtpA00wlrvUV9J",
        webhook_dev: "whsec_jySvwwZUSe3BLDFdJPYWn6c2vABfZbDd",
        webhook_prod: "whsec_K1BiE8ibPTCRBegIHAYAifjYaDp463Mm",
    },

    ONESIGNAL_CONFIGS: {
        apiKey: "os_v2_app_u2ihkwya6rg7zd5sombg6hekrplyvwca77ueth4yt4ct6ypeuibavrketsmbcudiqj2ceyyrviqry2agwdrqucg2rs53gevsj6kkxrq",
        appId: "a690755b-00f4-4dfc-8fb2-73026f1c8a8b"
    },

    SOCIAL_NETWORK_API_KEY: {
        metaAccessToken: "",
        facebookPageId: "",
        instagramId: "",
        threadsAPI: {
            clientId: "",
            clientSecret: "",
            redirectUri: `https://us-central1-${PRODUCTION_GCLOUD_PROJECT}.cloudfunctions.net/threadsAPIHandleAuthCallback`,
        },
        redditAPI: {
            clientId: "4AipG2HfGPnuOhtXtOYEmA",
            clientSecret: "9swPagUhH079Yq3uzQjyimQssUYhow",
            username: "FortulyApps",
            password: "d****c7",
            subReddit: "",
        }, bluesky: {
            username: "australiajobs-app.bsky.social",
            password: "divadlooc7",
        }, telegram: {
            channel: "@australiajobsapp",
            botToken: "7570908318:AAESsCTxnQxqdIwSeLVMiPX_C4op3QUtDk4",
        }
    }
}