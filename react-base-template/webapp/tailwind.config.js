/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./src/pages/**/*.{js,ts,jsx,tsx}",
    "./src/components/**/*.{js,ts,jsx,tsx}",
  ],
  theme: {
    extend: {},
  },
  plugins: [require("@tailwindcss/typography"), require("daisyui")],
  daisyui: {
    themes: [
      {
        mytheme: {
          "primary": "#031631",

          "secondary": "#EB174D",

          "accent": "#031631",

          "neutral": "#1E2A39",

          "base-100": "#FFFFFF",

          "info": "#7398DD",

          "success": "#1ADB97",

          "warning": "#F3B062",

          "error": "#E7443C",
        }
      }
    ]
    // themes: ["winter"],
  },
  corePlugins: {
    preflight: false,
  },
}
