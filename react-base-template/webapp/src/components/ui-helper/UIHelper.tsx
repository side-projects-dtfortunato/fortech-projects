import Link from "next/link";
import NavbarUserLinkComponent from "../react-shared-module/ui-components/shared/NavbarUserLink.component";
import {BasePostDataModel} from "../react-shared-module/logic/shared-data/datamodel/base-post-data.model";
import PostBaseListItemComponent, {
    PostListItemContent
} from "../react-shared-module/ui-components/posts/list/PostBaseListItem.component";
import {PostFormShareLinkShortenModel} from "../react-shared-module/logic/shared-data/datamodel/posts-model/post-share-link";
import {FormOption} from "../react-shared-module/ui-components/form/RadioButton.component";
import {NavbarLinkItem} from "../react-shared-module/ui-components/root/Navbar.component";
import AuthManager from "../react-shared-module/logic/auth/auth.manager";
import {getLoginPageLink} from "../../pages/login";
import {getUserProfileLink} from "../../pages/edit-profile/user-profile";
import {FooterLinkItem} from "../react-shared-module/ui-components/root/Footer.component";
import {
    SharedLandingPageAggregatorsModel
} from "../react-shared-module/logic/shared-data/datamodel/shared-landing-page-aggregators.model";
import {
    SharedCentralUserDataModel
} from "../react-shared-module/logic/shared-data/datamodel/shared-central-user-data.model";
import GoogleAdsComponent, {AdsSlots} from "../react-shared-module/ui-components/ads/GoogleAds.component";
import {NextRouter} from "next/router";
import {PostTypes, SharedUIHelper} from "../react-shared-module/utils/shared-ui-helper";
import {SharedRoutesUtils} from "../react-shared-module/utils/shared-routes.utils";
import NavigationHeaderMenuComponent
    , {NavigationMenuItem} from "../react-shared-module/ui-components/navigation-menu/navigation-header-menu.component";
import CommunityNewCommentsBulletComponent
    from "../react-shared-module/base-projects/community/ui/community-new-comments-bullet.component";
import {FaLaptopHouse, FaNewspaper, FaPaperPlane, FaUsers} from "react-icons/fa";
import {
    JobRemoteRegion,
    JobsDiscoverDataUtils,
    JobWorkModeType
} from "../react-shared-module/base-projects/jobs-discover/jobs-discover-data.model";
import RootConfigs from "../../configs";

export const CATEGORIES_OPTIONS: { [optionId: string]: FormOption } = {
    story: {label: "📖 Inspiring stories", optionId: "story"},
    sidehustle: {label: "💡 Side Hustle Ideas", optionId: "sidehustle"},
    businessidea: {label: "💸 Make Money Online", optionId: "businessidea"},
    passiveincome: {label: "📈 Passive Income", optionId: "passiveincome"},
    product: {label: "🚀 Product Promotion", optionId: "product"},
    tools: {label: "🧰 Helpful Tools", optionId: "tools"},
    discussion: {label: "💬 Discussion topics", optionId: "discussion"},
}

export enum LandingPageTabTypes {POPULAR = "Popular posts", RECENT = "Recent posts", FOLLOWING = "Following"}

const ADS_LIST_INDEX = [2, 7, 12, 16];

export class UIHelper {

    static getJobsCategories() {
        return [
            "Software Development",
            "Sales",
            "Construction",
            "Healthcare",
            "Business Development",
            "Operations",
            "Finance",
            "Human Resources",
            "Education",
            "Customer Service",
            "Project Management",
            "Administrative",
            "Accounting",
            "Manufacturing",
            "Consulting",
            "Design"
        ]
    }

    static getJobsFilterRegions(): JobRemoteRegion[] {
        return [
            "Greater London, England",
            "South East England",
            "South West England",
            "West Midlands, England",
            "East Midlands, England",
            "Yorkshire and the Humber",
            "North West England",
            "North East England",
            "Manchester, England",
            "Birmingham, England",
            "Leeds, England",
            "Edinburgh, Scotland",
            "Glasgow, Scotland",
            "Aberdeen, Scotland",
            "Cardiff, Wales",
            "Swansea, Wales",
            "Newport, Wales",
            "Belfast, Northern Ireland",
            "Derry, Northern Ireland"
        ];
    }

    static getWorkModeAllowed(): JobWorkModeType[] {
        return ["HYBRID", "REMOTE", "ON-SITE"];
    }

    static renderNavigationHeaderBottomMenu() {
        let listMenuItems: NavigationMenuItem[] = [
            {
                path: "/",
                label: RootConfigs.SITE_TOPIC,
                icon: FaLaptopHouse
            },
            {
                path: "/latest-articles",
                label: "News",
                icon: FaNewspaper
            },
            {
                path: SharedRoutesUtils.getCommunityPageUrl(),
                label: "Community",
                icon: FaUsers,
                rightComponent: (<CommunityNewCommentsBulletComponent />)
            },
            {
                path: SharedRoutesUtils.getSubmitJob(),
                label: "Post a Job",
                icon: FaPaperPlane,
            }
        ];
        return (
            <NavigationHeaderMenuComponent menuItems={listMenuItems} />
        )
    }

    static getArticlesCategories() {
        const CATEGORIES_LIST = ""
        return CATEGORIES_LIST.split(" | ");
    }

    static renderNavbarCenterItems(currentPath: string) {
        return (
            <div className="navbar-center hidden md:flex grow justify-content-center align-items-center">
                <ul className="menu menu-horizontal p-0">
                    {this.renderNavigationHeaderBottomMenu()}
                </ul>
            </div>
        )
    }

    static renderNavbarRightItems() {
        // <div className="hidden lg:flex flex-none">
        return (
            <div className="flex flex-none">
                <ul className="hidden sm:flex menu menu-horizontal p-0 mr-2">
                    {/*More Items*/}
                </ul>
                <NavbarUserLinkComponent />
            </div>
        );
    }

    static renderNavbarUserLinks() {
        return (
            <>
                <li key="Edit profile"><Link href={getUserProfileLink()}>Edit your profile</Link></li>
                <li key="Logout"><Link href={getLoginPageLink()} onClick={async () => {
                    await AuthManager.signOut();
                }}>Logout</Link></li>
            </>
        )
    }

    static renderNavbarLink(label: string, href: string) {
        return (<li key='label'><Link href={href}>{label}</Link></li>)
    }

    static renderPostListItem(postBaseData: BasePostDataModel, index: number, hideStats?: boolean) {
        let postContent: PostListItemContent | undefined;

        switch (postBaseData.postType as PostTypes) {
            case PostTypes.SHARE_LINK:
                postContent = {
                    title: postBaseData.title!,
                    subtitle: postBaseData.summary ? postBaseData.summary : (postBaseData.shortenPostData as PostFormShareLinkShortenModel).metaData?.description,
                    thumbnailUrl: postBaseData.thumbnailUrl ? postBaseData.thumbnailUrl : (postBaseData.shortenPostData as PostFormShareLinkShortenModel).metaData?.image,
                };
                break;
            default:
                postContent = {
                    title: postBaseData.title!,
                    subtitle: postBaseData.summary,
                    thumbnailUrl: postBaseData.thumbnailUrl,
                };
        }

        if (postContent) {
            return (
                <div className='flex flex-col'>
                    {ADS_LIST_INDEX.includes(index) ? <GoogleAdsComponent slot={AdsSlots.HORIZONTAL_ADS} /> : <></>}
                    <PostBaseListItemComponent key={postBaseData.uid} basePostItem={postBaseData} postListItemContent={postContent!} hideStats={hideStats} />
                </div>
            )
        } else {
            return (<></>)
        }
    }

    static renderLeftNavbar(props: {customLinks?: NavbarLinkItem[], customNavbarItems: any[]}) {
        let listLinks: NavbarLinkItem[] = [];

        // Add menu links
        listLinks = listLinks.concat(this.getMenuLinkItems({customLinks: props.customLinks}));

        function renderLinks() {
            if (listLinks) {
                return listLinks.map((linkItem) => {
                    return (
                        <li key={linkItem.linkUrl}><Link href={linkItem.linkUrl} onClick={linkItem.onClick}>{linkItem.label}</Link></li>
                    )
                });
            }
        }

        return (
            <div key={"side-menu"} className="dropdown">
                <label tabIndex={0} className="btn btn-ghost btn-circle">
                    <svg xmlns="http://www.w3.org/2000/svg" className="h-5 w-5" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                        <path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M4 6h16M4 12h16M4 18h7"/>
                    </svg>
                </label>
                <ul tabIndex={0} className="menu menu-compact dropdown-content mt-3 p-2 shadow bg-base-100 rounded-box w-52">
                    {renderLinks()}
                </ul>
            </div>
        )
    }
    static getMenuLinkItems(props: {customLinks?: NavbarLinkItem[]}): NavbarLinkItem[] {
        let listItems: NavbarLinkItem[] = [];

        if (props.customLinks) {
            listItems = listItems.concat(props.customLinks);
        }

        // Add Categories links
        /*listItems = listItems.concat(Object.values(CATEGORIES_OPTIONS).map((catData) => {
            return {label: catData.label, linkUrl: getCategoryPostsPageURL(catData.optionId)};
        }));*/


        listItems.push({linkUrl: "/", label: RootConfigs.SITE_TOPIC});
        listItems.push({linkUrl: "/latest-articles", label: "Latest Articles"});
        listItems.push({linkUrl: SharedRoutesUtils.getCommunityPageUrl(), label: "Community"});
        listItems.push({linkUrl: SharedRoutesUtils.getContactUsUrl(), label: "Contact Us"});

        if (AuthManager.isUserLogged()) {
            listItems.push({linkUrl: getLoginPageLink(), label: "Logout", onClick: () => AuthManager.signOut()});
        } else {
            listItems.push({linkUrl: getLoginPageLink(), label: "Login/Signup"});
        }


        return listItems;
    }
    static getPostCategoryLabel(categoryId: string): string {
        return CATEGORIES_OPTIONS[categoryId] ? CATEGORIES_OPTIONS[categoryId].label : "";
    }

    static getFooterLinks(): FooterLinkItem[] {
        let listFooterLinks: FooterLinkItem[] = [];

        // Navigation
        listFooterLinks.push({
            categoryId: "Navigation",
            categoryLabel: "Navigation",
            label: "Login/Signup",
            linkUrl: getLoginPageLink(),
        });
        listFooterLinks.push({
            categoryId: "Navigation",
            categoryLabel: "Navigation",
            label: RootConfigs.SITE_TOPIC,
            linkUrl: "/",
        });
        listFooterLinks.push({
            categoryId: "Navigation",
            categoryLabel: "Navigation",
            label: "Latest Articles",
            linkUrl: "/latest-articles",
        });
        listFooterLinks.push({
            categoryId: "Navigation",
            categoryLabel: "Navigation",
            label: "Community",
            linkUrl: SharedRoutesUtils.getCommunityPageUrl(),
        });
        listFooterLinks.push({
            categoryId: "Navigation",
            categoryLabel: "Navigation",
            label: "Contact Us",
            linkUrl: SharedRoutesUtils.getContactUsUrl(),
        });

        // Generate Jobs Categories links
        if (this.getJobsCategories().length > 0) {
            this.getJobsCategories().forEach((category) => {
                listFooterLinks.push({
                    categoryId: "Jobs Category",
                    categoryLabel: `Find ${RootConfigs.SITE_TOPIC}`,
                    label: `${category} ${RootConfigs.SITE_TOPIC}`,
                    linkUrl: SharedRoutesUtils.getJobsCategoryUrl(category),
                });
            });


            // Other Remote Jobs
            listFooterLinks = listFooterLinks.concat(JobsDiscoverDataUtils.getOtherRemoteJobLinks());
        }



        if (this.getJobsFilterRegions().length > 0) {
            // Generate Jobs Regions links
            this.getJobsFilterRegions().forEach((region) => {
            listFooterLinks.push({
                categoryId: "Jobs by Region",
                categoryLabel: "Jobs by Region",
                label: `Jobs in ${region}`,
                    linkUrl: SharedRoutesUtils.getJobsRegionUrl(region),
                });
            });
        }

        return listFooterLinks;
    }

    static getLandingPagePostTypes() {
        let listPostTypes: PostTypes[] = [];
        listPostTypes.push(PostTypes.ARTICLE_BLOCKS);
        listPostTypes.push(PostTypes.SHARE_LINK);
        return listPostTypes;
    }


    static getLandingPageTabListPosts(tabSelection: LandingPageTabTypes, landingDataAgg: SharedLandingPageAggregatorsModel, centralUserData?: SharedCentralUserDataModel | undefined | null) {
        let listPosts: BasePostDataModel[] = [];
        switch (tabSelection) {
            case LandingPageTabTypes.RECENT:
                listPosts = listPosts.concat(landingDataAgg.recentPosts);
                break;
            case LandingPageTabTypes.FOLLOWING:
                listPosts = listPosts.concat(this.getFollowingPosts(landingDataAgg.recentPosts, centralUserData));
                break;
            case LandingPageTabTypes.POPULAR:
            default:
                listPosts = listPosts.concat(landingDataAgg.popularPosts);
                break;
        }
        return listPosts;
    }
    static getFollowingPosts(listPosts?: BasePostDataModel[], centralUserData?: SharedCentralUserDataModel | undefined | null) {
        if (listPosts && centralUserData && centralUserData.usersFollowing) {
            return listPosts.filter((post) => {
                return Object.keys(centralUserData.usersFollowing!).includes(post.userCreatorId);
            });
        } else {
            return [];
        }
    }


    static onUserCreation(params: {router: NextRouter, goAutoBack?: boolean, centralUserData?: SharedCentralUserDataModel}) {
        if (params.goAutoBack) {
            params.router.back();
        }
    }

    static getPostFormTypeLabel(postType: PostTypes) {
        let label: string = SharedUIHelper.getPostFormTypeLabel(postType);
        switch (postType) {
            case PostTypes.SHARE_LINK: label = "🔗 Share a link"; break;
            case PostTypes.PRODUCT: label = "🚀 Share a product"; break;
            case PostTypes.WRITE_ARTICLE:
            case PostTypes.ARTICLE_BLOCKS:
                label = "📰 Write an article"; break;
            case PostTypes.DISCUSSION: label = "💬 Discussion topic"; break;
        }
        return label;
    }


    static isPostAvailableForCurrentSite(post: BasePostDataModel) {
        return true;
    }
}