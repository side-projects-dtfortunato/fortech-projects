import {FirebaseClientFirestoreUtils} from "../../components/react-shared-module/logic/shared-data/firebase.utils";
import {
    SharedFirestoreCollectionDB
} from "../../components/react-shared-module/logic/shared-data/datamodel/shared-firestore-collections";
import CustomRootLayoutComponent from "../../components/app-components/root/CustomRootLayout.component";
import CommunityPageComponent
    from "../../components/react-shared-module/base-projects/community/community-page.component";
import {
    DailyCommunityPublishes
} from "../../components/react-shared-module/base-projects/community/data/community-data.model";
import {PageHeadProps} from "../../components/react-shared-module/ui-components/root/RootLayout.component";
import RootConfigs from "../../configs";

const ITEMS_PER_PAGE = 3;

export async function getStaticProps() {
    let listPublishedDays: DailyCommunityPublishes[] = await FirebaseClientFirestoreUtils
        .getDocumentsWithPagination(SharedFirestoreCollectionDB.CommunityDailyPublishes, ITEMS_PER_PAGE, "publishedDayId");

    if (listPublishedDays) {
        listPublishedDays = listPublishedDays.filter((listItem) => listItem.listPosts && Object.keys(listItem.listPosts).length > 0)
    }
    return {
        props: {
            listPublishedDays,
        },
        revalidate: 30,
    }
}

export default function CommunityPage(props: {listPublishedDays: DailyCommunityPublishes[]}) {

    const pageHeadProps: PageHeadProps = {
        title: RootConfigs.SITE_TITLE + " - Community",
        description: `Discuss with our community the most recent news about ${RootConfigs.SITE_TOPIC}.`,
        imgUrl: RootConfigs.BASE_URL + "/images/logo-512.png",
    }
    return (
        <CustomRootLayoutComponent pageHeadProps={pageHeadProps} customBackgroundColor={"bg-slate-50"}>
            <CommunityPageComponent preloadedData={props.listPublishedDays} itemsPerPage={ITEMS_PER_PAGE} />
        </CustomRootLayoutComponent>
    )
}