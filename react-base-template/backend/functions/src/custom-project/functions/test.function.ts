import * as functions from "firebase-functions";
import {
    SharedJobsDiscoverUtils
} from "../../shareable-firebase-backend/base-projects/jobs-discover/shared-jobs-discover.utils";

export const testFunction = functions.https.onRequest(
    async (req, resp) => {

        let jobs = await SharedJobsDiscoverUtils.extractJobListings("https://www.linkedin.com/jobs/search/?location=Portugal&geoId=100364837&f_WT=2&position=1&pageNum=0");
        resp.send({jobs});

    });