import {
    JobDetailsModel, JobWorkModeType
} from "../../shareable-firebase-backend/base-projects/jobs-discover/shared-jobs-discover.data";


/**
 * Regions ids:
 * - UK: 101165590
 * - Australia: 101452733
 * - USA: 103644278
 * - EU: 91000002
 */
/*export const LINKEDIN_SOURCES: LinkedinSourceModel[] = [
    {
        id: "1",
        category: "Software Development",
        region: "USA",
        url: "https://www.linkedin.com/jobs/search?keywords=Software Engineer&geoId=103644278&f_TPR=&f_WT=2&f_E=2%2C3%2C4%2C5&position=1&pageNum=0",
    },
]*/

export const JOB_REGIONS = [
    {
        regionLabel: "Greater London, England",
        regionId: "102257491"
    },
    {
        regionLabel: "South East England",
        regionId: "119576547"
    },
    // etc..
] as {regionLabel: string, regionId: string}[];
export const JOB_CATEGORIES = [
    {
        jobCategoryLabel: "Software Development",
        jobCategoryQuery: "Software Engineer",
    },
    {
        jobCategoryLabel: "Sales",
        jobCategoryQuery: "Sales",
    },
    // etc..
] as {jobCategoryLabel: string, jobCategoryQuery: string}[];


export const ALLOWED_WORK_MODE = ["REMOTE", "ON-SITE", "HYBRID"] as JobWorkModeType[];


export class CustomJobsDiscoverUtils {

    static importJobsFromRemoteJobsHub(): JobDetailsModel[] {
        return [];
    }

    static getIsValidRule() {
        return "";
    }

}