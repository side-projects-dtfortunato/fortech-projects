import {
    JobDetailsModel,
    LinkedinSourceModel
} from "../../shareable-firebase-backend/base-projects/jobs-discover/shared-jobs-discover.data";
import axios from "axios";


/**
 * Regions ids:
 * - UK: 101165590
 * - Australia: 101452733
 * - USA: 103644278
 * - EU: 91000002
 */
/*export const LINKEDIN_SOURCES: LinkedinSourceModel[] = [
    {
        id: "1",
        category: "Customer Service",
        region: "USA",
        url: "https://www.linkedin.com/jobs/search?keywords=Customer%20Service&geoId=103644278&f_TPR=&f_WT=2&f_E=2%2C3%2C4%2C5&position=1&pageNum=0",
    },
    {
        id: "2",
        category: "Customer Service",
        region: "EU",
        url: "https://www.linkedin.com/jobs/search?keywords=Customer%20Service&geoId=91000002&f_TPR=&f_WT=2&f_E=2%2C3%2C4%2C5&position=1&pageNum=0",
    },
    {
        id: "3",
        category: "Customer Service",
        region: "Australia",
        url: "https://www.linkedin.com/jobs/search?keywords=Customer%20Service&geoId=101452733&f_TPR=&f_WT=2&f_E=2%2C3%2C4%2C5&position=1&pageNum=0",
    },
    {
        id: "4",
        category: "Customer Service",
        region: "UK",
        url: "https://www.linkedin.com/jobs/search?keywords=Customer%20Service&geoId=101165590&f_TPR=&f_WT=2&f_E=2%2C3%2C4%2C5&position=1&pageNum=0",
    },
]*/

export const JOB_REGIONS = [
    {
        regionLabel: "EU",
        regionId: "91000002"    
    },
    {
        regionLabel: "UK",
        regionId: "101165590"
    },
    {
        regionLabel: "Australia",
        regionId: "101452733"
    },
    {
        regionLabel: "USA",
        regionId: "103644278"
    },
] as {regionLabel: string, regionId: string}[];

export const JOB_CATEGORIES = [
    {
        jobCategoryLabel: "Customer Service",
        jobCategoryQuery: "Customer Service"
    },
    {
        jobCategoryLabel: "Customer Support",
        jobCategoryQuery: "Customer Support"
    },
    {
        jobCategoryLabel: "Technical Support",
        jobCategoryQuery: "Technical Support"
    },
    {
        jobCategoryLabel: "Customer Success",
        jobCategoryQuery: "Customer Success"
    },
    {
        jobCategoryLabel: "Client Relations",
        jobCategoryQuery: "Client Relations"
    },
    {
        jobCategoryLabel: "Help Desk Support",
        jobCategoryQuery: "Help Desk Support"
    },
    {
        jobCategoryLabel: "Account Manager",
        jobCategoryQuery: "Account Manager"
    },
    {
        jobCategoryLabel: "Customer Experience",
        jobCategoryQuery: "Customer Experience"
    },
    {
        jobCategoryLabel: "Call Center",
        jobCategoryQuery: "Call Center"
    },
    {
        jobCategoryLabel: "Customer Care",
        jobCategoryQuery: "Customer Care"
    }
] as {jobCategoryLabel: string, jobCategoryQuery: string}[];

export const ALLOWED_WORK_MODE = ["REMOTE"];


export class CustomJobsDiscoverUtils {


    static async importJobsFromRemoteJobsHub() {
        const category = "Customer%20Service";
        const listJobs: JobDetailsModel[] = (await axios.get(`https://us-central1-remotejobshub-app.cloudfunctions.net/getLatestJobsFunction?category=${category}`)).data;
        return listJobs;
    }

    static getIsValidRule() {
        return undefined;
    }

}