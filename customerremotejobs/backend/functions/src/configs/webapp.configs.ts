
export const PRODUCTION_GCLOUD_PROJECT = "customerremotejobs-com";

export const WEBAPP_CONFIGS = {
    LOGO_URL: "https://www.customerremotejobs.com/images/logo-white-bg.jpg",
    SITE_TAGLINE: "",
    SITE_NAME: "CustomerRemoteJobs.com",
    WEBSITE_URL: "https://customerremotejobs.com",
    COMPANY_ADDRESS: "3030 Coimbra (Portugal)",
    COMPANY_EMAIL: "contact@customerremotejobs.com",
    POST_PREFIX_URL: "https://www.customerremotejobs.com/post/",
    PROFILE_PREFIX_URL: "https://www.customerremotejobs.com/public-profile/",
    UNSUBSCRIBE_URL: `https://us-central1-${PRODUCTION_GCLOUD_PROJECT}.cloudfunctions.net/unsubscribeEmail?uid=`,

    MAILERSEND_API_KEY: "TODO",

    CUSTOM_CONFIGS: {
        openPushInApp: true,
    } as any,

    // Account: contact@sidehustlify.com - Di.....oc7
    IMAGE_KIT_API: {
        publicKey : "public_OurCtnmj0jWqjVslk5YDfKE4aHQ=",
        privateKey : "private_vRUtPf6HGOj68f5rZZN8u9RHfiQ=",
        urlEndpoint : "https://ik.imagekit.io/fortulyapps",
    },

    STRIPE_API: {
        prod: "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",
        dev: "sk_test_51PueA3HT2hxH9IRDdlrmFSjPbHmjQ1An0PNkz74ZkAOw50c1MSK5nG0MGBWOvbm5RjkBDgG4VXnMDZ2yRBWJGtpA00wlrvUV9J",
        webhook_dev: "whsec_jySvwwZUSe3BLDFdJPYWn6c2vABfZbDd",
        webhook_prod: "whsec_ViZPVUDrqaKrXK5SOl2gus7cOmSe7IcG",
    },

    ONESIGNAL_CONFIGS: {
        apiKey: "ZDMxYmNkYTktNWFiOC00MWM1LWIwNTctYzg2ODliZDUyM2U1",
        appId: "6b3459f2-ac74-48f7-9687-8b40e36cf223"
    },

    SOCIAL_NETWORK_API_KEY: {
        metaAccessToken: "EAAcc7A6gOzMBO1gZCrE4mV7SGb6rhlB7Ri22LK5uUMSLapZCCEPojxllSbZA7xDLRo0ZBtZByLwUXXJsjThirqRtTEFX5aKu9DKNXOwfzpgKNMoWUkO7KOORLbJO8dnKKPGquHhi7j9wKxKp7lH7MxFrZCSmMNZBjX1K0kjxslNsI38oZBbRLgS4I4PsVzkJzkAZD",
        facebookPageId: "470468172805615",
        instagramId: "17841469752490831",
        threadsAPI: {
            clientId: "1733012790836437",
            clientSecret: "7c0431995453441401acf9787795b07e",
            redirectUri: `https://us-central1-${PRODUCTION_GCLOUD_PROJECT}.cloudfunctions.net/threadsAPIHandleAuthCallback`,
        },
        redditAPI: {
            clientId: "4AipG2HfGPnuOhtXtOYEmA",
            clientSecret: "9swPagUhH079Yq3uzQjyimQssUYhow",
            username: "FortulyApps",
            password: "d****c7",
            subReddit: "",
        },
        bluesky: {
            username: "customerremotejobs.bsky.social",
            password: "divadlooc7",
        },
        telegram: {
            channel: "@CustomerRemoteJobs",
            botToken: "7706596535:AAGW3Ldpz_ixnLHl5I5VV2vOPUFnKbZy0Ac",
        },
    }
}