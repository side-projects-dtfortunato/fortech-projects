/** @type {import('next').NextConfig} */
const withPWA = require('next-pwa')({
  dest: 'public'
})

module.exports = withPWA({
  // next.js config
  reactStrictMode: false,
  swcMinify: true,
  images: {
    unoptimized: true,
    remotePatterns: [
      {
        protocol: "https",
        hostname: "**",
      },
    ],
  },
  async redirects() {
    return [
      {
        source: '/post/:slug',
        destination: '/:slug',
        permanent: true,
      },
    ]
  },
  webpack: (config, { buildId, dev, isServer }) => {
    config.resolve.symlinks = false;
    return config
  }
})