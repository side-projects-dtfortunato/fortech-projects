
export default class RootConfigs {
    static ENVIRONMENT_DEV = false;
    static BASE_URL = "https://www.customerremotejobs.com";
    static SITE_NAME = "CustomerRemoteJobs.com";
    static SITE_TITLE = "Customer Remote Jobs | Find the Best Remote Customer Service Positions";
    static SITE_DESCRIPTION = "Discover top remote customer service jobs at CustomerRemoteJobs.com. Connect with leading companies offering flexible work-from-home opportunities in customer support, service, and success roles. Start your remote career today!";
    static CURRENT_NICHE_SITE = RootConfigs.SITE_NAME;
    static CONTACT_EMAIL = "contact@customerremotejobs.com";
    static SITE_TOPIC = "Customer Services";
    static THEME = {
        PRIMARY_COLOR: "blue",
    }
    static META_CONFIGS: any = {
        updateNicheBackend: false,
        disableGoogleSignin: true,
        disableCookies: true,
        disableAppStoreBanner: false,
        disableGoogleAds: false,
        enableJobSearch: true,
    }
    static APP_STORE_LINKS = {
        androidAppId: "customerremotejobs.com",
        iosAppId: "customer-service-remote-jobs/id6711372887",
    }
    static SOCIAL_NETWORKS: {
        [socialId: string]: {
            iconUrl: string,
            label: string,
            link: string,
        },
    } = {
        facebook: {
            label: "Facebook",
            link: "https://www.facebook.com/profile.php?id=61565899179933",
            iconUrl: "/images/ic_social_facebook.png",
        },
        instagram: {
            label: "Instagram",
            link: "https://www.instagram.com/customer_remote_jobs/",
            iconUrl: "/images/ic_social_instagram.png",
        },
        threads: {
            label: "Threads",
            link: "https://www.threads.net/@customer_remote_jobs",
            iconUrl: "/images/ic_social_threads.png",
        },
        telegram: {
            label: "Telegram Community",
            link: "https://t.me/CustomerRemoteJobs",
            iconUrl: "/images/ic_social_threads.png",
        },
    }
    static FIREBASE_CONFIG = this.ENVIRONMENT_DEV ?

        // DEV Config
        {
            apiKey: "AIzaSyCB2yrNkEHbpShTH_xwHOzcl2mXJT5i1K8",
            authDomain: "fortuly-qa-env.firebaseapp.com",
            projectId: "fortuly-qa-env",
            storageBucket: "fortuly-qa-env.appspot.com",
            messagingSenderId: "650128691290",
            appId: "1:650128691290:web:69f8a283090d23e0454863",
            measurementId: "G-V6XN6K51VZ"
        } :
        // PROD
        {
            apiKey: "AIzaSyCxzE6KxqqTFP22hhmKc0VrC1HTWxup02Y",
            authDomain: "customerremotejobs-com.firebaseapp.com",
            databaseURL: "https://customerremotejobs-com-default-rtdb.firebaseio.com",
            projectId: "customerremotejobs-com",
            storageBucket: "customerremotejobs-com.appspot.com",
            messagingSenderId: "74381989136",
            appId: "1:74381989136:web:d6298b74997ad9db740195",
            measurementId: "G-JDZ5P84N17"
        };


    static IMAGEKIT_API = {
        publicKey : "public_OurCtnmj0jWqjVslk5YDfKE4aHQ=",
        privateKey : "private_vRUtPf6HGOj68f5rZZN8u9RHfiQ=",
        urlEndpoint : "https://ik.imagekit.io/fortulyapps",
    }

    static ONE_SIGNAL_API = "6b3459f2-ac74-48f7-9687-8b40e36cf223";
}
