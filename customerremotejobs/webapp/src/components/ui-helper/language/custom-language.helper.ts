

export const CustomLanguageLabels: {[labelId: string]: string} = {
    LANDING_PAGE_HEADER_JOIN_COMMUNITY_TITLE: "Join our community of Side Hustlers",
    LANDING_PAGE_HEADER_JOIN_COMMUNITY_SUBTITLE: "To discover, share or discuss side hustles ideas and results. Also, share your own side hustles with our community of Side Hustlers",


    // Jobs
    REMOTE_JOBS_EXPLORE_JOBS_TITLE: "Explore Remote Customer Service Jobs",
    REMOTE_JOBS_LATEST_JOBS_IN_TITLE: "Latest Remote Jobs in",
    REMOTE_JOBS_DISCOVER_JOBS_SUBTITLE: "Discover new remote Customer Service opportunities worldwide."
}

export function getCustomLanguageLabel(labelId: string): string | undefined {
    if (Object.keys(CustomLanguageLabels).includes(labelId)) {
        // @ts-ignore
        return CustomLanguageLabels[labelId]! as string;
    } else {
        return undefined;
    }
}