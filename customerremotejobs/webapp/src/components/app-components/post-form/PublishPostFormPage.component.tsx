import {useState} from "react";
import PostFormShareLinkComponent from "../../react-shared-module/ui-components/post-form/PostFormShareLink.component";
import {PushPostDataModel} from "../../react-shared-module/logic/shared-data/datamodel/base-post-data.model";
import {ApiResponse} from "../../react-shared-module/logic/shared-data/datamodel/base.model";
import {SharedBackendApi} from "../../react-shared-module/logic/shared-data/sharedbackend.api";
import {useRouter} from "next/router";
import {
    useAlertMessageGlobalState,
    useIsLoadingGlobalState
} from "../../react-shared-module/logic/global-hooks/root-global-state";
import {APIResponseMessageUtils} from "../../react-shared-module/logic/global-hooks/APIResponseMessageUtils";
import {CATEGORIES_OPTIONS, UIHelper} from "../../ui-helper/UIHelper";
import PostFormDiscussionComponent
    from "../../react-shared-module/ui-components/post-form/PostFormDiscussion.component";
import PostFormWriteArticleComponent
    from "../../react-shared-module/ui-components/post-form/PostFormWriteArticle.component";
import {PostTypes} from "../../react-shared-module/utils/shared-ui-helper";
import {SharedRoutesUtils} from "../../react-shared-module/utils/shared-routes.utils";


export default function PublishPostFormPageComponent(props: {defaultPostType?: PostTypes}) {
    const router = useRouter();
    const [postTypeSelection, setPostTypeSelection] = useState<PostTypes>(props.defaultPostType ?props.defaultPostType : PostTypes.SHARE_LINK);
    const [alertMessageState, setAlertMessageState] = useAlertMessageGlobalState();
    const [isLoadingState, setIsLoadingState] = useIsLoadingGlobalState();
    const [thumbnailToUploadData, setThumbnailToUploadData] = useState<string|undefined>();

    async function onSubmitClicked(postFormData: PushPostDataModel) {
        // API Call
        const apiResponse: ApiResponse<any> = await SharedBackendApi.pushPostData({
            postData: postFormData,
            isCreation: true,
            thumbnailBase: thumbnailToUploadData,
        });

        if (apiResponse && apiResponse.isSuccess) {
            // Redirect user
            await router.push(SharedRoutesUtils.getPostDetailsPageURL(postFormData.uid!));
        } else {
            // Handle errors
            setAlertMessageState(
                {
                    alertType: "ERROR",
                    message: APIResponseMessageUtils.getAPIResponseMessage(apiResponse),
                    display: true,
                }
            );
        }
    }

    function onTabSelected(tabPostType: PostTypes) {
        setPostTypeSelection(tabPostType);
    }

    function renderTab(tabPostType: PostTypes) {
        let tabLabel: string = UIHelper.getPostFormTypeLabel(tabPostType);

        let suffixClass: string = "";
        if (tabPostType === postTypeSelection) {
            suffixClass = "tab-active"
        }
        return (
            <button className={"tab tab-lifted " + suffixClass} onClick={() => onTabSelected(tabPostType)}>{tabLabel}</button>
        )
    }

    function onThumbnailToUpload(thumbnailBase64: string) {
        // Upload in the moment of creating the post
        setThumbnailToUploadData(thumbnailBase64);
    }

    function renderFormContainer() {
        switch (postTypeSelection) {
            case PostTypes.SHARE_LINK:
                return (<PostFormShareLinkComponent postType={postTypeSelection} onSubmitClicked={onSubmitClicked}
                                                    listCategories={CATEGORIES_OPTIONS}/>);
            case PostTypes.WRITE_ARTICLE:
                return (<PostFormWriteArticleComponent postType={postTypeSelection} onSubmitClicked={onSubmitClicked} onThumbnailBase64={onThumbnailToUpload}
                                                    listCategories={CATEGORIES_OPTIONS}/>);
            case PostTypes.DISCUSSION:
            default:
                return (<PostFormDiscussionComponent postType={postTypeSelection} onSubmitClicked={onSubmitClicked}
                                                    listCategories={CATEGORIES_OPTIONS}/>);
        }
        return (<></>);
    }

    return (
        <div className='flex flex-col'>
            <div className="tabs">
                {renderTab(PostTypes.SHARE_LINK)}
                {renderTab(PostTypes.DISCUSSION)}
                {renderTab(PostTypes.ARTICLE_BLOCKS)}
            </div>
            <div className='list-item-bg drop-shadow px-4 py-3'>
                {renderFormContainer()}
            </div>
        </div>

    )
}
