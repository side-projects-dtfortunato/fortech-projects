import CustomRootLayoutComponent from "../../../components/app-components/root/CustomRootLayout.component";
import { JobListModel, JobsDiscoverDataUtils } from "../../../components/react-shared-module/base-projects/jobs-discover/jobs-discover-data.model";
import JobListItemComponent from "../../../components/react-shared-module/base-projects/jobs-discover/ui/job-list-item.component";
import { getLanguageLabel } from "../../../components/react-shared-module/logic/language/language.helper";
import { PageHeadProps } from "../../../components/react-shared-module/ui-components/root/RootLayout.component";
import EmptyListMessageComponent from "../../../components/react-shared-module/ui-components/shared/EmptyListMessage.component";
import { UIHelper } from "../../../components/ui-helper/UIHelper";
import RootConfigs from "../../../configs";
import JobRegionHeader
    from "../../../components/react-shared-module/base-projects/jobs-discover/ui/job-region-header.component";
import { SharedRoutesUtils } from "../../../components/react-shared-module/utils/shared-routes.utils";
import { FooterLinkItem } from "../../../components/react-shared-module/ui-components/root/Footer.component";

export async function getStaticPaths() {
    let listPaths: any[] = [];

    listPaths = UIHelper.getJobsFilterRegions()
        .map((region) => {
            return {
                params: {
                    region: region,
                },
            }
        });

    return {
        paths: listPaths,
        fallback: true, // can also be true or 'blocking'
    }
}

export async function getStaticProps(context: any) {
    const region: string = context.params.region as string;

    let listJobs: JobListModel[] | undefined = await JobsDiscoverDataUtils.fetchFilteredJobs({
        category: "",
        remoteRegion: region,
    });

    if (!listJobs) {
        listJobs = [];
    }


    return {
        props: {
            listJobs: listJobs,
            region: region,
        },
        revalidate: 60, // each 60 seconds
    };

}



export default function JobByRegion(props: {listJobs: JobListModel[], region: string}) {

    function renderListJobs() {
        if (props.listJobs && props.listJobs.length > 0) {
            return props.listJobs.map((job) => {
                return <JobListItemComponent key={job.uid!} jobData={job}/>
            });
        } else {
            return <EmptyListMessageComponent message={"We don't have any remote job for this category. Please, come back tomorrow."} />
        }

    }

    const pageHeadProps: PageHeadProps = {
        title: getLanguageLabel("JOBS_REGION_TITLE", `Recent Remote Jobs in ${props.region}`).replace("#REGION", props.region),
        description: generateSEODescription({
            region: props.region,
            jobCount: props.listJobs ? props.listJobs.length : 0,
        }),
        imgUrl: RootConfigs.BASE_URL + "/images/logo-white-bg.png",
        publishedTime: props.listJobs && props.listJobs.length > 0 ? new Date(props.listJobs[0].createdAt!).toString() : new Date().toString(),
    }


    // Add Footer links for categotires
    let listFooterLinks: FooterLinkItem[] = [];

    UIHelper.getJobsCategories().forEach((category) => {
        listFooterLinks.push({
            categoryId: "Jobs by Category",
            categoryLabel: "Jobs by Category",
            label: `Jobs in ${props.region} for ${category}`,
            linkUrl: SharedRoutesUtils.getJobsRegionCategoryUrl(props.region, category),
        });
    });

    return (
        <CustomRootLayoutComponent pageHeadProps={pageHeadProps} listFooterLinks={listFooterLinks}>
            <JobRegionHeader region={props.region} />

            <div className='flex flex-col'>
                {renderListJobs()}
            </div>
        </CustomRootLayoutComponent>
    )
}


interface SEODescriptionParams {
    region: string;
    jobCount?: number;
    location?: string;
    updateFrequency?: string;
    topCompanies?: string[];
}

function generateSEODescription({
                                    region,
                                    jobCount,
                                    location = 'worldwide',
                                    updateFrequency = 'daily',
                                    topCompanies = [],
                                }: SEODescriptionParams): string {
    let description = `Explore the latest ${RootConfigs.SITE_TOPIC} in ${region}.`;

    if (jobCount !== undefined) {
        description += ` Find ${jobCount}+ open positions`;
    } else {
        description += ' Discover numerous open positions';
    }

    description += ` updated ${updateFrequency}.`;

    if (topCompanies.length > 0) {
        const companiesList = topCompanies.slice(0, 3).join(', ');
        description += ` Top companies like ${companiesList} are hiring now.`;
    }

    description += ' Start your remote career today!';

    // Ensure the description doesn't exceed a reasonable length for SEO
    return description.slice(0, 160);
}