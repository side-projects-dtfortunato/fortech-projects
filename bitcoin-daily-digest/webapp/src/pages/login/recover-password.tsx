import {useRouter} from "next/router";
import RecoverPasswordFormComponent
    from "../../components/react-shared-module/ui-components/login/RecoverPasswordForm.component";
import CustomRootLayoutComponent from "../../components/app-components/root/CustomRootLayout.component";

export default function RecoverPassword(props: {}) {
    const route = useRouter();
    let paramEmail: string | undefined;

    if (route.query['email']) {
        paramEmail = route.query['email'] as string;
    }

    return (
        <CustomRootLayoutComponent customBody>
            <div className='flex h-screen w-screen justify-center items-start mt-10 login-bg-image p-2'>
                <RecoverPasswordFormComponent email={paramEmail} />
            </div>
        </CustomRootLayoutComponent>
    )
}

export function getRecordPasswordPageUrl(email?: string)
{
    let urlBuilder: string = "/login/recover-password";

    if (email) {
        urlBuilder += "?email=" + email;
    }
    return urlBuilder;
}