import {useState} from "react";
import {useRouter} from "next/router";
import {getUserProfileLink} from "../edit-profile/user-profile";
import {useAuthState} from "react-firebase-hooks/auth";
import {auth} from "../../components/react-shared-module/logic/shared-data/firebase.utils";
import AuthManager from "../../components/react-shared-module/logic/auth/auth.manager";
import LoginFormComponent from "../../components/react-shared-module/ui-components/login/LoginForm.component";
import SpinnerComponent from "../../components/react-shared-module/ui-components/shared/Spinner.component";
import CustomRootLayoutComponent from "../../components/app-components/root/CustomRootLayout.component";

interface SuccessMessageData {
    message: string,
    redirectLink: string,
}

export default function LoginPage() {
    const route = useRouter();

    // Auth Mechanism
    const [currentUser, isAuthLoading] = useAuthState(auth);
    const [errorMessage, setErrorMessage] = useState<string | undefined>(undefined);
    const [isAPILoading, setIsAPILoading] = useState<boolean>(false);

    // Signin Function
    function onSubmitSignin(email: string, password: string, succesRedirect?: SuccessMessageData) {
        if (!isAuthLoading) {
            // setIsAPILoading(true);
            AuthManager.signInUser(email, password).then(async (res) => {
                if (res) {
                    // Redirect user
                    if (succesRedirect) {
                        route.push(succesRedirect.redirectLink);
                    } else {
                        route.back();
                    }
                }

                setIsAPILoading(false);

            }).catch((err: any) => {
                let errorMessage = "Something failed. Probably because of invalid user credentials. Please, try again with the correct";

                switch (err.code) {
                    case "auth/invalid-email":
                        errorMessage = "Invalid email. Please, insert a valid email.";
                        break;
                    case "auth/user-not-found":
                        errorMessage = "User not found. Create an account to register a Remote Work account.";
                        break;
                    case "auth/wrong-password":
                        errorMessage = "Invalid password. Please, verify if you set the correct password.";
                        break;
                }
                setErrorMessage(errorMessage);
                setIsAPILoading(false);
            });
        }
    }

    // Create an Account Function
    async function onSubmitCreateAccount(email: string, password: string) {
        let errorMessage = "Something failed. Probably because of invalid user credentials. Please, try again with the correct";
        let isValid: boolean = true;

        // Internal validation
        if (!password || password.length < 8) {
            isValid = false;
            errorMessage = "Invalid password. Please, choose a password with at least 8 characters.";
        }

        if (isValid && !isAuthLoading) {
            AuthManager.createUserAccount(email, password).then((res) => {
                route.push(getUserProfileLink(true));
            }).catch((err: any) => {
                errorMessage = err.code;

                switch (err.code) {
                    case "auth/invalid-email":
                        errorMessage = "Invalid email. Please, insert a valid email to create an account.";
                        break;
                    case "auth/email-already-in-use":
                        errorMessage = "We already have an account created with this email.";
                        break;
                }
                setErrorMessage(errorMessage);
            });
        } else {
            setIsAPILoading(false);
            setErrorMessage(errorMessage);
        }
    }

    // Render the root container
    function renderRootContainer() {

        if (isAuthLoading) {
            // Is  loading
            return (
                <div>
                    <SpinnerComponent/>
                </div>)
        } else if (AuthManager.isUserLogged()) {
            return (
                <div className='flex flex-col justify-content-center align-items-center list-item-bg drop-shadow p-5'>
                    <span className='my-3 text-xl'>Your are logged in</span>
                    <button className='btn btn-outline' onClick={() => route.push("/")}>BACK TO HOME</button>
                </div>
            )
        } else {
            // Sigin/create account component
            return (
                (<LoginFormComponent submitSignin={onSubmitSignin} submitCreateAccount={onSubmitCreateAccount} errorMessage={errorMessage}/>)
            )
        }
    }

    return (
        <CustomRootLayoutComponent customBody>
            <div className='login-bg-image w-full h-full'>
                <div className='flex h-screen w-full justify-center items-start mt-10 p-2'>
                    <div>
                        {renderRootContainer()}
                    </div>
                </div>
            </div>
        </CustomRootLayoutComponent>
    )
}

export function getLoginPageLink() {
    return "/login";
}