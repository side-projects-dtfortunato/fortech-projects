// pages/_document.js
import {Head, Html, Main, NextScript} from 'next/document'
import RootConfigs from "../configs";

export default function Document() {
    // <script async
    //                         src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js?client=ca-pub-5492791201497132"
    //                         crossOrigin="anonymous"></script>
    return (
        <Html lang="en-US">

            <Head>

                <meta charSet="UTF-8" />
                <meta name="application-name" content={RootConfigs.SITE_NAME} />
                <meta name="apple-mobile-web-app-capable" content="yes" />
                <meta name="apple-mobile-web-app-status-bar-style" content="default" />
                <meta name="apple-mobile-web-app-title" content={RootConfigs.SITE_NAME} />
                <meta name="format-detection" content="telephone=no" />
                <meta name="mobile-web-app-capable" content="yes" />
                <meta name="msapplication-config" content="none"/>
                <meta name="msapplication-TileColor" content="#073264" />
                <meta name="msapplication-tap-highlight" content="no" />
                <meta name="theme-color" content="#EEB0BC" />
                <meta name="ahrefs-site-verification" content="5657a2d93c8df92da115eaacbc702b11c20b8b8b377ada59bcfc8c20cd91b82c"/>

                <link rel="apple-touch-icon" href="/images/ic_logo.png" />
                <link rel="icon" type="image/png" sizes="32x32" href="/images/icon-32.png" />
                <link rel="icon" type="image/png" sizes="16x16" href="/images/icon-16.png" />
                <link rel="shortcut icon" href="/favicon.ico" />
                <link rel="manifest" href="/manifest.json" />


                <link rel="stylesheet" href="https://rsms.me/inter/inter.css" />
                <link
                    href="https://fonts.googleapis.com/css2?family=Inter&display=optional"
                    rel="stylesheet"
                />

                {/*<script async
                         src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js?client=ca-pub-5492791201497132"
                         crossOrigin="anonymous"></script>*/}


                <div dangerouslySetInnerHTML={{__html: '<script async type="application/javascript"\n' +
                        '                        src="https://news.google.com/swg/js/v1/swg-basic.js"></script>\n' +
                        '                <script>\n' +
                        '                    (self.SWG_BASIC = self.SWG_BASIC || []).push((basicSubscriptions) => {\n' +
                        '                    basicSubscriptions.init({\n' +
                        '                        type: "NewsArticle",\n' +
                        '                        isPartOfType: ["Product"],\n' +
                        '                        isPartOfProductId: "CAowm_uxDA:openaccess",\n' +
                        '                        clientOptions: { theme: "light", lang: "en" },\n' +
                        '                    });\n' +
                        '                });\n' +
                        '                </script>'}} />


            </Head>
            <body>
            <Main />
            <NextScript />
            </body>

        </Html>
    )
}