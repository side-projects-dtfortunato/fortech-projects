import {getLoginPageLink} from "../login";
import {useAuthState} from "react-firebase-hooks/auth";
import {useRouter} from "next/router";
import {useState} from "react";
import RootConfigs from "../../configs";
import {auth} from "../../components/react-shared-module/logic/shared-data/firebase.utils";
import RootLayoutComponent, {
    PageHeadProps
} from "../../components/react-shared-module/ui-components/root/RootLayout.component";
import AuthManager from "../../components/react-shared-module/logic/auth/auth.manager";

export default function UserProfilePage(props: {}) {
    const router = useRouter();
    const [currentUser, isAuthLoading] = useAuthState(auth);
    const [hasDataChanged, setHasDataChanged] = useState<boolean>(false);

    let paramIsNewUser: boolean = false;
    if (router.query["newUser"]) {
        paramIsNewUser = Boolean(router.query["newUser"] as string);
    }


    const pageHeadProps: PageHeadProps = {
        title: "Edit your profile",
        description: RootConfigs.SITE_DESCRIPTION,
    }

    if (AuthManager.isUserLogged() || isAuthLoading) {
        // Is loading Central User Data
        return (
            <RootLayoutComponent pageHeadProps={pageHeadProps}>
                <div className='flex h-screen w-full justify-content-center align-items-center'>
                    <div className='loading' />
                </div>
            </RootLayoutComponent>
        )
    } else {
        router.push(getLoginPageLink());
        return (
            <RootLayoutComponent isLoading={true} isIndexable={false}>
                <div className="h-screen"></div>
            </RootLayoutComponent>
        )
    }
}


export function getUserProfileLink(newUser?: boolean) {
    let urlBuilder: string = "/profile/user-profile";
    if (newUser) {
        urlBuilder += "?newUser=" + true;
    }
    return urlBuilder;
}