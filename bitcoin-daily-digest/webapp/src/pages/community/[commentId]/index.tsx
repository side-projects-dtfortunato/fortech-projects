import {
    FirebaseClientFirestoreUtils,
    getAPIDocument
} from "../../../components/react-shared-module/logic/shared-data/firebase.utils";
import {
    SharedFirestoreCollectionDB
} from "../../../components/react-shared-module/logic/shared-data/datamodel/shared-firestore-collections";
import {
    CommunityCommentPostModel
} from "../../../components/react-shared-module/base-projects/community/data/community-data.model";
import CustomRootLayoutComponent from "../../../components/app-components/root/CustomRootLayout.component";
import CommunityCommentItem
    from "../../../components/react-shared-module/base-projects/community/ui/community-comment-item";
import NotFoundContentComponent
    from "../../../components/react-shared-module/ui-components/shared/NotFoundContent.component";

export async function getStaticPaths() {
    let listRecentComments: CommunityCommentPostModel[] | undefined = await FirebaseClientFirestoreUtils.getDocumentsWithPagination(SharedFirestoreCollectionDB.CommunityCommentPosts, 15, "createdAt");
    let listPaths: any[] = [];

    if (listRecentComments) {
        listPaths = listRecentComments
            .map((communityItem) => {
                return {
                    params: {
                        commentId: communityItem.uid!,
                    },
                }
            });
    }

    return {
        paths: listPaths,
        fallback: true, // can also be true or 'blocking'
    }
}

export async function getStaticProps(context: any) {
    const commentId: string = context.params.commentId as string;


    const communityCommentData: CommunityCommentPostModel | undefined = await getAPIDocument(SharedFirestoreCollectionDB.CommunityCommentPosts, commentId);


    if (communityCommentData) {
        return {
            props: {
                communityCommentData: communityCommentData,
            },
            revalidate: 60, // each 60 seconds
        };
    } else {
        return {
            props: {
            },
            revalidate: 60, // each 60 seconds
        }
    }
}

export default function CommunityCommentPage(props: {communityCommentData?: CommunityCommentPostModel}) {

    if (props.communityCommentData) {
        return (
            <CustomRootLayoutComponent customBackgroundColor={"bg-slate-50"}>
                <div className='my-10 py-5 w-full sm:rounded-xl drop-shadow-xl bg-white'>
                    <CommunityCommentItem communityCommentPost={props.communityCommentData} displayAttachedItem={true} showReplies={true} isLinkableToDetails={false}/>
                </div>
            </CustomRootLayoutComponent>
        )
    } else {
        return (
            <CustomRootLayoutComponent>
                <NotFoundContentComponent />
            </CustomRootLayoutComponent>
        )
    }

}