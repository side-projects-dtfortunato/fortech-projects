import {
    ArticleImportedModel
} from "../../../components/react-shared-module/base-projects/articles-digest/data/model/ArticleImported.model";
import {FirebaseClientFirestoreUtils} from "../../../components/react-shared-module/logic/shared-data/firebase.utils";
import {
    SharedFirestoreCollectionDB
} from "../../../components/react-shared-module/logic/shared-data/datamodel/shared-firestore-collections";
import {UIHelper} from "../../../components/ui-helper/UIHelper";
import CustomRootLayoutComponent from "../../../components/app-components/root/CustomRootLayout.component";
import ArticlesCategorySelectorComponent
    from "../../../components/react-shared-module/base-projects/articles-digest/ui-components/articles-category-selector.component";
import NewsletterSignupListBannerComponent
    from "../../../components/react-shared-module/ui-components/newsletter/shared/newsletter-signup-list-banner.component";
import NewsletterPopupComponent
    from "../../../components/react-shared-module/ui-components/newsletter/shared/newsletter-popup.component";
import ArticlesListContainerComponent
    from "../../../components/react-shared-module/base-projects/articles-digest/ui-components/articles-list-container.component";
import {PageHeadProps} from "../../../components/react-shared-module/ui-components/root/RootLayout.component";
import RootConfigs from "../../../configs";


const MAX_LATEST_ARTICLES = 30;

export async function getStaticPaths() {
    let listPaths: any[] = [];

    listPaths = UIHelper.getArticlesCategories()
        .map((category) => {
            return {
                params: {
                    category: category,
                },
            }
        });

    return {
        paths: listPaths,
        fallback: true, // can also be true or 'blocking'
    }
}

export async function getStaticProps(context: any) {
    const category: string = context.params.category as string;

    let listRecentArticles: ArticleImportedModel[] | undefined = await FirebaseClientFirestoreUtils.getDocumentsFilteredWithPagination({
        collectionPath: SharedFirestoreCollectionDB.ArticlesDigestProject.ArticlePublished,
        pageSize: MAX_LATEST_ARTICLES,
        fieldOrderBy: "createdAt",
        filter: {
            field: "aiGeneratedContent.category",
            value: category,
        }
    });

    if (listRecentArticles) {
        return {
            props: {
                latestArticles: listRecentArticles,
                category: category,
            },
            revalidate: 60, // each 60 seconds
        };
    } else {
        return {
            props: {
                latestArticles: [],
                category: category,
            },
            revalidate: 60, // each 60 seconds
        }
    }

}
export default function ArticlesCategoryPage(props: {latestArticles?: ArticleImportedModel[], category: string}) {


    const pageHeadParams: PageHeadProps = {
        title: RootConfigs.SITE_TITLE + " - Latest News About " + props.category,
        description: RootConfigs.SITE_DESCRIPTION,
        publishedTime: props.latestArticles && props.latestArticles.length > 0 ? new Date(props.latestArticles[0].createdAt!).toString() : new Date().toString(),
    }

    return (
        <CustomRootLayoutComponent pageHeadProps={pageHeadParams} headerBanner={<ArticlesCategorySelectorComponent categories={UIHelper.getArticlesCategories()} articlesUrl={"/"} selectedCategory={props.category} />}>
            <div className='flex flex-col items-center w-full'>
                <NewsletterSignupListBannerComponent title={"Subscribe our Newsletter"} />
            </div>
            <ArticlesListContainerComponent listArticles={props.latestArticles} title={"Latest News About"} titleHighlight={props.category} />

            {/* Popups */}
            <NewsletterPopupComponent />
        </CustomRootLayoutComponent>
    )

}