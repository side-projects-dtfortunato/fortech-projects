import {NextApiRequest, NextApiResponse} from "next";
import {
    DailyPublishedArticlesModel
} from "../../components/react-shared-module/base-projects/articles-digest/data/model/DailyPublishedArticles.model";
import {FirebaseClientFirestoreUtils} from "../../components/react-shared-module/logic/shared-data/firebase.utils";
import {
    SharedFirestoreCollectionDB
} from "../../components/react-shared-module/logic/shared-data/datamodel/shared-firestore-collections";
import RootConfigs from "../../configs";
import {Feed} from "feed";
import {SharedRoutesUtils} from "../../components/react-shared-module/utils/shared-routes.utils";

const ITEMS_PER_PAGE = 5;

export default async function RssFeed(req: NextApiRequest, res: NextApiResponse) {
    const listPublishedDays: DailyPublishedArticlesModel[] = await FirebaseClientFirestoreUtils
        .getDocumentsWithPagination(SharedFirestoreCollectionDB.ArticlesDigestProject.DailyPublishedArticles, ITEMS_PER_PAGE, "publishedDayId");
    let author = {
        name: RootConfigs.SITE_NAME,
        email: "contact@bitcointoday.app",
        link: RootConfigs.BASE_URL,
    }
    const feed = new Feed({
        title: RootConfigs.SITE_TITLE,
        description: RootConfigs.SITE_DESCRIPTION,
        id: RootConfigs.BASE_URL,
        link: RootConfigs.BASE_URL,
        language: "en", // optional, used only in RSS 2.0, possible values: http://www.w3.org/TR/REC-html40/struct/dirlang.html#langcodes
        image: RootConfigs.BASE_URL + "/images/logo-512.png",
        favicon: RootConfigs.BASE_URL + "/favicon.ico",
        copyright: "All rights reserved " + new Date().getUTCFullYear()  + ", " + RootConfigs.SITE_NAME,
        updated: new Date(), // optional, default = today
        author: author
    });

    listPublishedDays.forEach((dailyPublished: DailyPublishedArticlesModel) => {
        if (dailyPublished.listArticles) {
            Object.values(dailyPublished.listArticles).forEach((articleItem) => {
                const imageUrl: string = validateAndFixImageUrl(articleItem.imageUrl!);
                console.log(imageUrl);
                feed.addItem({
                    title: articleItem.title,
                    link: RootConfigs.BASE_URL + SharedRoutesUtils.getArticleDetails(articleItem.uid!),
                    id: articleItem.uid!,
                    image: imageUrl,
                    description: articleItem.aiGeneratedContent?.summary!,
                    category: articleItem.aiGeneratedContent?.tags ? articleItem.aiGeneratedContent?.tags.map((tag) => {
                        return {name: tag}
                    }) : [],
                    published: new Date(articleItem.createdAt!),
                    date: new Date(articleItem.createdAt!),
                    author: [author],
                });
            });
        }
    });

    feed.addCategory("Bitcoin News");

    res.status(200).setHeader('Content-Type', 'application/xml').send(feed.rss2());
}

function validateAndFixImageUrl(imageUrl: string): string {
    // Regular expression to match common image file extensions
    const imageRegex = /^https?:\/\/.*\.(jpg|jpeg|gif|png|tiff|bmp)$/i;

    if (imageRegex.test(imageUrl)) {
        // Valid image URL
        return imageUrl;
    } else {
        // Replace with a default placeholder image URL
        return 'https://example.com/placeholder-image.png';
    }
}