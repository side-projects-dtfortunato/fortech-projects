
export default class RootConfigs {
    static ENVIRONMENT_DEV = false;
    static BASE_URL = "https://www.bitcointoday.app";
    static CONTACT_EMAIL = "contact@bitcoin.app";
    static SITE_NAME = "BitcoinToday.app";
    static SITE_TITLE = "Bitcoin Today - Bitcoin News Curated and Powered by AI";
    static SITE_DESCRIPTION = "Get daily updates on Bitcoin's price, market trends, analysis, and breaking news curated and powered by AI - all digestible in minutes. Make BitcoinToday.app your one-stop shop for staying informed in the fast-paced world of Bitcoin.";
    static CURRENT_NICHE_SITE = RootConfigs.SITE_NAME;
    static SITE_TOPIC = "Bitcoin";
    static THEME = {
        PRIMARY_COLOR: "yellow",
    }
    static META_CONFIGS: any = {
        updateNicheBackend: false,
        disableGoogleSignin: true,
        disableCookies: true,
        disableAppStoreBanner: false,
    }
    static APP_STORE_LINKS = {
        androidAppId: "bitcointoday.app",
        iosAppId: "6503717549",
    }

    static SOCIAL_NETWORKS: {
        [socialId: string]: {
            iconUrl: string,
            label: string,
            link: string,
        },
    } = {
        facebook: {
            label: "Facebook",
            link: "https://www.facebook.com/profile.php?id=61561659922018",
            iconUrl: "/images/ic_social_facebook.png",
        },
        instagram: {
            label: "Instagram",
            link: "https://www.instagram.com/bitcointoday.app/",
            iconUrl: "/images/ic_social_instagram.png",
        },
        threads: {
            label: "Threads",
            link: "https://www.threads.net/@bitcointoday.app",
            iconUrl: "/images/ic_social_threads.png",
        },
        reddit: {
            label: "Reddit",
            link: "https://www.reddit.com/r/BitcoinTodayNews/",
            iconUrl: "/images/ic_social_reddit.png",
        },
    }
    static FIREBASE_CONFIG = this.ENVIRONMENT_DEV ?

        // DEV Config - // TODO Change it
        {
            apiKey: "AIzaSyCB2yrNkEHbpShTH_xwHOzcl2mXJT5i1K8",
            authDomain: "fortuly-qa-env.firebaseapp.com",
            projectId: "fortuly-qa-env",
            storageBucket: "fortuly-qa-env.appspot.com",
            messagingSenderId: "650128691290",
            appId: "1:650128691290:web:69f8a283090d23e0454863",
            measurementId: "G-V6XN6K51VZ"
        } :
        // PROD
        {
            apiKey: "AIzaSyCUd0l3vhtLyzJ-vZK6AHeXgKw6gq9_t9c",
            authDomain: "bitcoin-daily-digest.firebaseapp.com",
            projectId: "bitcoin-daily-digest",
            storageBucket: "bitcoin-daily-digest.appspot.com",
            messagingSenderId: "975068125766",
            appId: "1:975068125766:web:06cc9257f93f1013980847",
            measurementId: "G-TJEGLBMY7R"
        };


    static IMAGEKIT_API = {
        publicKey : "public_OurCtnmj0jWqjVslk5YDfKE4aHQ=",
        privateKey : "private_vRUtPf6HGOj68f5rZZN8u9RHfiQ=",
        urlEndpoint : "https://ik.imagekit.io/fortulyapps",
    }

    static ONE_SIGNAL_API = "85481ed6-4da1-4fb5-8977-7ac0aef1ba95"
}
