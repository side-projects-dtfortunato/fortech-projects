/** @type {import('next-sitemap').IConfig} */
module.exports = {
    siteUrl: process.env.SITE_URL || 'https://bitcointoday.app',
    generateRobotsTxt: true, // (optional)
    exclude: ["/login", "/login/recover-password", "/edit-post/*", "/edit-profile/user-profile", "/bookmarks", "/privacy-policy"],
    // ...other options
}