import * as functions from "firebase-functions";
import {ArticlesDigestUtils} from "../utils/articles-digest.utils";
import {
    ArticleImportedModel
} from "../../shareable-firebase-backend/base-projects/articles-digest/data/model/ArticleImported.model";
import {UrlExtractDataUtils} from "../../shareable-firebase-backend/utils/url-extract-data.utils";
import {
    SocialNetworksPostUtils
} from "../../shareable-firebase-backend/base-projects/social-networks-autopost/SocialNetworksPost.utils";
import {
    generateDailyDigestHtml
} from "../../shareable-firebase-backend/base-projects/articles-digest/functions/articles-daily-digest-email.function";
import {MailerSendUtils} from "../../shareable-firebase-backend/utils/email-utils/email-sender-utils";
import {WEBAPP_CONFIGS} from "../../configs/webapp.configs";

export const testFunction = functions.https.onRequest(
    async (req, resp) => {
        const html = await UrlExtractDataUtils.extractArticleContent("https://news.google.com/read/CBMiggFBVV95cUxNSy1yc1Nib3Nyd01xeGlSTWt2Mmo5S2c1Z1g0UTJCYXRJM0kxTTRSb0lWTzVfN080c1BCeVBWcTBoNkwzaXBIZzJ6YkQtTmFjT09qVG9xNVVoM3RDc1R0YW9FU0hnVndkSXpFYWptTkRnd1dqNWdPQVRiSjBYNzhWWlFR?oc=5&hl=pt-PT&gl=PT&ceid=PT:pt-150");

        resp.send(html?.content);

    });