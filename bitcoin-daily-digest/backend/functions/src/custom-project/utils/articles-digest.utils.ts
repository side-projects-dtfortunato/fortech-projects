import {
    ArticleImportedModel
} from "../../shareable-firebase-backend/base-projects/articles-digest/data/model/ArticleImported.model";
import {BackendSharedUtils} from "../../shareable-firebase-backend/utils/backend-shared-utils";
import {AIGeminiUtils} from "../../shareable-firebase-backend/utils/aigemini.utils";
import {OpenaiUtils} from "../../shareable-firebase-backend/utils/openai/openai.utils";
import {BackendRouteUtils} from "../../shareable-firebase-backend/utils/backend-route.utils";
import {
    SocialNetworksPostUtils
} from "../../shareable-firebase-backend/base-projects/social-networks-autopost/SocialNetworksPost.utils";
import {BackendImageUtils} from "../../shareable-firebase-backend/utils/backend-image.utils";
import {
    SharedArticlesDigestUtils
} from "../../shareable-firebase-backend/base-projects/articles-digest/shared-articles-digest.utils";
import {NotificationsQueueManager} from "../../shareable-firebase-backend/utils/notifications-queue.manager";



const ARTICLES_CONTENT_TOPIC = "Bitcoin and Cryptocurrency";
const CATEGORIES_LIST = "Opinion | Education | Global Economy | General Bitcoin News | Market Sentiment | Technology | Fundamental Analysis | Technical Analysis"

export class ArticlesDigestUtils {

    static SOURCES_BLACKLIST = ["BLOOMBERG", "COINTELEGRAPH", "BITCOIN.COM NEWS", "USA TODAY"];
    // static SOURCES_BLACKLIST_DOMAINS = ["bloomberg.com", "bitcoin.com", "youtube.com"]
    static SOURCES_BLACKLIST_DOMAINS = []

    static async fetchCoindeskRssArticles(): Promise<ArticleImportedModel[]> {
        const dataFetched: any = await BackendSharedUtils.fetchAndParseXML("https://www.coindesk.com/arc/outboundfeeds/rss/");

        let dataItems: any[] | any | undefined = dataFetched.rss.channel.item;
        if (dataItems && !Array.isArray(dataItems)) {
            dataItems = [dataItems];
        }

        let listParsedArticles: ArticleImportedModel[] = [];

        if (dataItems) {
            listParsedArticles = dataItems.map((rssItem: any) => {
                const url: string = BackendSharedUtils.removeQueryParams(rssItem.link);
                const pubDate: number = new Date(rssItem.pubDate).valueOf();
                const originalLinkId: string = BackendSharedUtils.string_to_slug(rssItem.link);
                return {
                    uid: BackendSharedUtils.string_to_slug(rssItem.title) + "-" + pubDate,
                    originalLinkId: originalLinkId,
                    originalLink: url,
                    title: rssItem.title,
                    originalTitle: rssItem.title,
                    description: rssItem.description,
                    publishedDate: pubDate,
                    imageUrl: rssItem["media:content"] ? rssItem["media:content"].url : undefined,
                    source: "coindesk",
                    createdAt: Date.now(),
                    updatedAt: Date.now(),
                    aiGeneratedContent: {},
                    upvotes: {},
                    stats: {
                        commentsCounter: 0,
                        viewsCounter: 0,
                    },
                }
            });
        }

        return listParsedArticles;
    }

    static async fetchBenzingaRssArticles(): Promise<any> {
        const dataFetched: any = await BackendSharedUtils.fetchAndParseXML("https://www.benzinga.com/markets/cryptocurrency/feed");

        let dataItems: any[] | any | undefined = dataFetched.rss.channel.item;
        if (dataItems && !Array.isArray(dataItems)) {
            dataItems = [dataItems];
        }

        let listParsedArticles: ArticleImportedModel[] = [];

        if (dataItems) {
            listParsedArticles = dataItems
                .filter((rssItem: any) => {
                    let hasBtcCategory: boolean = false;
                    if (rssItem.category) {
                        rssItem.category.forEach((category: any) => {
                            if (category["_"] && category["_"].toUpperCase().includes("BTC")) {
                                hasBtcCategory = true;
                            }
                        })
                    }
                    return hasBtcCategory;
                })
                .map((rssItem: any) => {
                const url: string = BackendSharedUtils.removeQueryParams(rssItem.link);
                const pubDate: number = new Date(rssItem.pubDate).valueOf();
                const originalLinkId: string = BackendSharedUtils.string_to_slug(rssItem.link);
                return {
                    uid: BackendSharedUtils.string_to_slug(rssItem.title) + "-" + pubDate,
                    originalLinkId: originalLinkId,
                    originalLink: url,
                    title: rssItem.title,
                    originalTitle: rssItem.title,
                    description: rssItem.description,
                    publishedDate: pubDate,
                    imageUrl: rssItem["media:thumbnail"] ? rssItem["media:thumbnail"].url : undefined,
                    source: "benzinga",
                    createdAt: Date.now(),
                    updatedAt: Date.now(),
                    customHTMLContentElement: "#article-body",
                    aiGeneratedContent: {},
                    upvotes: {},
                    stats: {
                        commentsCounter: 0,
                        viewsCounter: 0,
                    },
                }
            });
        }

        return listParsedArticles;
    }

    static async fetchBloombergRssArticles(): Promise<any> {
        const dataFetched: any = await BackendSharedUtils.fetchAndParseXML("https://feeds.bloomberg.com/crypto/news.rss");

        let dataItems: any[] | any | undefined = dataFetched.rss.channel.item;
        if (dataItems && !Array.isArray(dataItems)) {
            dataItems = [dataItems];
        }
        let listParsedArticles: ArticleImportedModel[] = [];

        if (dataItems) {
            listParsedArticles = dataItems.map((rssItem: any) => {
                const url: string = BackendSharedUtils.removeQueryParams(rssItem.link);
                const pubDate: number = new Date(rssItem.pubDate).valueOf();
                const originalLinkId: string = BackendSharedUtils.string_to_slug(rssItem.link);
                return {
                    uid: BackendSharedUtils.string_to_slug(rssItem.title) + "-" + pubDate,
                    originalLinkId: originalLinkId,
                    originalLink: url,
                    title: rssItem.title,
                    originalTitle: rssItem.title,
                    description: rssItem.description,
                    publishedDate: pubDate,
                    imageUrl: rssItem["media:content"] ? rssItem["media:content"].url : undefined,
                    source: "bloomberg",
                    createdAt: Date.now(),
                    updatedAt: Date.now(),
                    customHTMLContentElement: "article",
                    aiGeneratedContent: {},
                    upvotes: {},
                    stats: {
                        commentsCounter: 0,
                        viewsCounter: 0,
                    },
                }
            });
        }

        return listParsedArticles;
    }

    static async fetchCointelegraphRssArticles(): Promise<any> {
        const dataFetched: any = await BackendSharedUtils.fetchAndParseXML("https://cointelegraph.com/rss/tag/bitcoin");

        let dataItems: any[] | any | undefined = dataFetched.rss.channel.item;
        if (dataItems && !Array.isArray(dataItems)) {
            dataItems = [dataItems];
        }

        let listParsedArticles: ArticleImportedModel[] = [];

        if (dataItems) {
            listParsedArticles = dataItems.map((rssItem: any) => {
                const url: string = BackendSharedUtils.removeQueryParams(rssItem.link);
                const pubDate: number = new Date(rssItem.pubDate).valueOf();
                const originalLinkId: string = BackendSharedUtils.string_to_slug(rssItem.link);
                return {
                    uid: BackendSharedUtils.string_to_slug(rssItem.title) + "-" + pubDate,
                    originalLinkId: originalLinkId,
                    originalLink: url,
                    title: rssItem.title.trim(),
                    description: rssItem.description.trim(),
                    publishedDate: pubDate,
                    imageUrl: rssItem["media:content"] ? rssItem["media:content"].url : undefined,
                    source: "cointelegraph",
                    createdAt: Date.now(),
                    updatedAt: Date.now(),
                    customHTMLContentElement: "article",
                    aiGeneratedContent: {},
                    upvotes: {},
                    stats: {
                        commentsCounter: 0,
                        viewsCounter: 0,
                    },
                } as ArticleImportedModel
            }).filter((rssItem: any) => !rssItem.originalLink.startsWith("https://cointelegraph.com/news/what-happened-in-crypto-today"));
        }

        return listParsedArticles;
    }

    static async fetchCoingapeRssArticles(): Promise<any> {
        const dataFetched: any = await BackendSharedUtils.fetchAndParseXML("https://coingape.com/category/news/bitcoin-news/feed/");

        let dataItems: any[] | any | undefined = dataFetched.rss.channel.item;
        if (dataItems && !Array.isArray(dataItems)) {
            dataItems = [dataItems];
        }

        let listParsedArticles: ArticleImportedModel[] = [];

        if (dataItems) {
            listParsedArticles = dataItems.map((rssItem: any) => {
                const url: string = BackendSharedUtils.removeQueryParams(rssItem.link);
                const pubDate: number = new Date(rssItem.pubDate).valueOf();
                const originalLinkId: string = BackendSharedUtils.string_to_slug(rssItem.link);
                return {
                    uid: BackendSharedUtils.string_to_slug(rssItem.title) + "-" + pubDate,
                    originalLinkId: originalLinkId,
                    originalLink: url,
                    title: rssItem.title.trim(),
                    description: rssItem.description.trim(),
                    publishedDate: pubDate,
                    imageUrl: rssItem["media:content"] ? rssItem["media:content"].url : undefined,
                    source: "Coingape",
                    createdAt: Date.now(),
                    updatedAt: Date.now(),
                    customHTMLContentElement: "article",
                    fullContent: rssItem["content:encoded"],
                    aiGeneratedContent: {},
                    upvotes: {},
                    stats: {
                        commentsCounter: 0,
                        viewsCounter: 0,
                    },
                } as ArticleImportedModel
            });
        }

        return listParsedArticles;
    }

    static async fetchCryptoNewsRssArticles(): Promise<any> {
        const dataFetched: any = await BackendSharedUtils.fetchAndParseXML("https://crypto.news/feed/");

        let dataItems: any[] | any | undefined = dataFetched.rss.channel.item;
        if (dataItems && !Array.isArray(dataItems)) {
            dataItems = [dataItems];
        }

        let listParsedArticles: ArticleImportedModel[] = [];

        if (dataItems) {
            listParsedArticles = dataItems.map((rssItem: any) => {
                const url: string = BackendSharedUtils.removeQueryParams(rssItem.link);
                const pubDate: number = new Date(rssItem.pubDate).valueOf();
                const originalLinkId: string = BackendSharedUtils.string_to_slug(rssItem.link);
                return {
                    uid: BackendSharedUtils.string_to_slug(rssItem.title) + "-" + pubDate,
                    originalLinkId: originalLinkId,
                    originalLink: url,
                    title: rssItem.title.trim(),
                    description: rssItem.description.trim(),
                    publishedDate: pubDate,
                    imageUrl: rssItem["media:content"] ? rssItem["media:content"].url : undefined,
                    source: "Crypto.News",
                    createdAt: Date.now(),
                    updatedAt: Date.now(),
                    customHTMLContentElement: "article",
                    fullContent: rssItem["content:encoded"],
                    aiGeneratedContent: {},
                    upvotes: {},
                    stats: {
                        commentsCounter: 0,
                        viewsCounter: 0,
                    },
                } as ArticleImportedModel
            });
        }

        return listParsedArticles.filter((article) => article.title.toLowerCase().includes("bitcoin") || article.description.toLowerCase().includes("bitcoin"));
    }

    static async fetchGoogleNews(searchTerm: string, searchType: "TOPIC" | "SEARCH"): Promise<any> {
        let listArticles: ArticleImportedModel[] = await SharedArticlesDigestUtils.fetchGoogleNews({
            searchTerm: searchTerm,
            searchType: searchType,
            timeFrame: "",
            domainsBlackList: this.SOURCES_BLACKLIST_DOMAINS,
        });

        listArticles = listArticles
            .filter((googleArticle) => {
                return !this.SOURCES_BLACKLIST.includes(googleArticle.source.toUpperCase())
            });
        // filter by the most relevant and recent articles
        listArticles = listArticles.slice(0, 20); // Only get the top 20 articles
        listArticles = listArticles.filter((googleArticle) => BackendSharedUtils.isLessThanHoursAgo(googleArticle.publishedDate, 10));

        return listArticles;
    }




    static async getMostRecentArticles(): Promise<ArticleImportedModel[]> {
        let listParsedArticles: ArticleImportedModel[] = [];

        // Fetch CoinDesk
        // listParsedArticles = listParsedArticles.concat(await this.fetchBloombergRssArticles());
        /*listParsedArticles = listParsedArticles.concat(await this.fetchCoindeskRssArticles());
        listParsedArticles = listParsedArticles.concat(await this.fetchBenzingaRssArticles());
        listParsedArticles = listParsedArticles.concat(await this.fetchCointelegraphRssArticles());
        listParsedArticles = listParsedArticles.concat(await this.fetchCoingapeRssArticles());
        listParsedArticles = listParsedArticles.concat(await this.fetchCryptoNewsRssArticles());*/
        listParsedArticles = listParsedArticles.concat(await this.fetchGoogleNews("CAAqJAgKIh5DQkFTRUFvS0wyMHZNRFZ3TUhKeWVCSUNaVzRvQUFQAQ", "TOPIC")); // Topic: Bitcoin
        listParsedArticles = listParsedArticles.concat(await this.fetchGoogleNews("CAAqJAgKIh5DQkFTRUFvS0wyMHZNSFp3YWpSZlloSUNaVzRvQUFQAQ", "TOPIC")); // Topic: Cryptocurrency

        return listParsedArticles;
    }

    static async generateAIContent(articleData: ArticleImportedModel, otherArticles?: string[]): Promise<{title: string, category: string, content: string, highlightsBullets: string[], tags: string[], socialNetworkPost: string, isArticleContentValid: boolean} | undefined> {
        const generatedAIContent = await SharedArticlesDigestUtils.generateAIContentOpenAI(articleData, {
            topic: ARTICLES_CONTENT_TOPIC,
            extraJsonParams: `marketSentiment: \"bullish\"|\"bearish\"|\"neutral\", `,
            categoryList: CATEGORIES_LIST,
            otherArticles: otherArticles,
        });
        if (generatedAIContent) {
            (articleData.aiGeneratedContent as any).marketSentiment = generatedAIContent?.marketSentiment;
        }
        return generatedAIContent;
    }

    static async generateAIContentClaudAI(articleData: ArticleImportedModel): Promise<any> {
        const jsonContent: string = JSON.stringify({
            title: articleData.title,
            description: articleData.description,
            articleDetails: articleData.fullContent
        })

        let promptBuild: string = `You are an assistant that reads an article about Bitcoin Market from a JSON string 
        and summarizes the content of the article. Respond in the following JSON format (valid json format, without any prefix or suffix, just the JSON object): 
        { 
            title: string, // rewrite and improve the title to be more clickable, focusing on capturing interest and curiosity 
            content: string, // rewrite the article content styled with markdown (use headings, bold the most relevant parts and keywords) and make it more summarized while keeping the most relevant and interesting parts 
            category: string, // select a unique concise and accurate category for the article 
            highlightsBullets: string[], // list of key points of the article, bold the most relevant parts and keywords in each key point (do not add any prefix bullet to the strings) 
            socialNetworkPost: string, // create an engaging social post to publish this article, including relevant hashtags in keywords (do not add any CTA or placeholder to the link article) 
            tags: string[], // provide 5 relevant tags without hashtag and spaces 
            isContentValidAndRelevant: boolean, // true if the article content is not advertisement to promote external products or services, and is relevant (covers key points and main ideas), detailed (provides sufficient information), and interesting (engages the reader and holds their attention)
        }
        }\n`;
        promptBuild += `JSON Object with the article original content:\n${jsonContent}\n\n**TL;DR:**`

        return promptBuild;

        /*try {
            const aiGeneratedContent: any = BackendSharedUtils.extractJsonObject(await ClaudeAiUtils.generateContent(promptBuild));
            if (aiGeneratedContent && aiGeneratedContent.highlightsBullets) {
                aiGeneratedContent.highlightsBullets = aiGeneratedContent.highlightsBullets.map((bullet: string) => bullet.replace(/^- |^\* /, ''));
            }
            if (aiGeneratedContent.tags) {
                aiGeneratedContent.tags = aiGeneratedContent.tags.map((tag: string) => tag.replace(/[#\s]/g, ""));
            }

            // Update Article Data
            articleData.aiGeneratedContent.tags = aiGeneratedContent?.tags;
            articleData.aiGeneratedContent.summary = aiGeneratedContent?.content;
            articleData.aiGeneratedContent.highlightsBullets = aiGeneratedContent?.highlightsBullets;
            articleData.aiGeneratedContent.socialNetworkPost = aiGeneratedContent?.socialNetworkPost;
            articleData.aiGeneratedContent.category = aiGeneratedContent?.category;
            articleData.isArticleValid = aiGeneratedContent?.isContentValidAndRelevant;
            articleData.title = aiGeneratedContent?.title ? aiGeneratedContent?.title! : articleData.title;
            articleData.uid = BackendSharedUtils.string_to_slug(articleData.title);

            return aiGeneratedContent;
        } catch (e: any) {
            return undefined;
        }*/
    }



    static async rankListArticles(listArticles: ArticleImportedModel[], otherArticlesTitles?: string[]): Promise<{listArticles: {articleUid: string, title: string, articleRelevancy: number, hasSimilarArticles: boolean}[]} | any | undefined> {
        return SharedArticlesDigestUtils.rankListArticles(listArticles, ARTICLES_CONTENT_TOPIC, otherArticlesTitles);
    }

    static generateSocialNetworkPost(articleData: ArticleImportedModel) {
        let postMessage: string = "";

        if (articleData.aiGeneratedContent.socialNetworkPost) {
            postMessage = articleData.aiGeneratedContent.socialNetworkPost
        } else {
            postMessage = articleData.title + "\n\n";
            articleData.aiGeneratedContent.tags?.forEach((tag, index) => {
                if (index > 0) {
                    postMessage += " ";
                }
                postMessage += "#" + BackendSharedUtils.replaceAll(tag, " ", "");
            });
        }
        postMessage += "\n\nRead more: " + BackendRouteUtils.getArticleDetails(articleData.uid!);
        postMessage = BackendSharedUtils.replaceAll(postMessage, "*", "");
        return postMessage;
    }

    static async publishArticleSocialNetworks(articleData: ArticleImportedModel) {

        // Add Notification to the queue
        await NotificationsQueueManager.addNotificationItemToQueue({
            uid: articleData.uid!,
            socialPost: this.generateSocialNetworkPost(articleData),
            category: articleData.aiGeneratedContent!.category ? articleData.aiGeneratedContent!.category : articleData.aiGeneratedContent!.tags![0],
            title: BackendSharedUtils.replaceAll(articleData.title, "*", ""),
            link: BackendRouteUtils.getArticleDetails(articleData.uid!),
            socialNetworks: ["REDDIT", "FACEBOOK", "INSTAGRAM", "THREADS"],
            bgImageUrl: articleData.imageUrl!,
            pushNotification: true,
            description: BackendSharedUtils.cleanMarkdown(articleData.aiGeneratedContent.summary!),
        });

        /*// Facebook
        await SocialNetworksPostUtils.facebookPublishTextPost({
            message: this.generateSocialNetworkPost(articleData),
            link: BackendRouteUtils.getArticleDetails(articleData.uid!),
        });

        // Publish to Threads
        await SocialNetworksPostUtils.threadsPublishPost({
            message: this.generateSocialNetworkPost(articleData),
        });

        // Publish on Reddit
        await SocialNetworksPostUtils.redditPostLink({
            url: BackendRouteUtils.getArticleDetails(articleData.uid!),
            title: BackendSharedUtils.replaceAll(articleData.title, "*", ""),
            text: this.generateSocialNetworkPost(articleData),
        });


        // Publish to Instagram
        try {
            const imageBase64: string = await BackendImageUtils.generateThumbnail({
                title: articleData.title,
                bgImageUrl: articleData.imageUrl!,
                category: articleData.aiGeneratedContent!.category ? articleData.aiGeneratedContent!.category : articleData.aiGeneratedContent!.tags![0],
            });
            await SocialNetworksPostUtils.instagramPublishPhoto({
                uid: articleData.uid!,
                imageBase64: imageBase64,
                message: this.generateSocialNetworkPost(articleData),
            });
        } catch (e: any) {
            console.log("Error publishing instagram post: ", e);
        }*/
    }
}