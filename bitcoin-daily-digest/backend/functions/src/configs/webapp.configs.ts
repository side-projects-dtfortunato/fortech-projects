
export const PRODUCTION_GCLOUD_PROJECT = "bitcoin-daily-digest";

export const WEBAPP_CONFIGS = {
    LOGO_URL: "https://www.bitcointoday.app/images/logo-white-bg.jpg",
    SITE_NAME: "BitcoinToday.app",
    SITE_TAGLINE: "Bitcoin Daily Digested News",
    WEBSITE_URL: "https://bitcointoday.app",
    COMPANY_ADDRESS: "3030 Coimbra (Portugal)",
    COMPANY_EMAIL: "contact@bitcointoday.app",
    POST_PREFIX_URL: "https://www.bitcointoday.app/post/",
    PROFILE_PREFIX_URL: "https://www.bitcointoday.app/public-profile/",
    UNSUBSCRIBE_URL: `https://us-central1-${PRODUCTION_GCLOUD_PROJECT}.cloudfunctions.net/unsubscribeEmail?uid=`,

    MAILERSEND_API_KEY: "mlsn.8b130e6646698eabfa11bf5601d5b87368c72d338f7e5ca06989f966d5cbd65f",

    CUSTOM_CONFIGS: {
        openPushInApp: true,
    } as any,

    // Account: contact@sidehustlify.com - Di.....oc7
    IMAGE_KIT_API: {
        publicKey : "TODO", // "public_QPb3OvUFUKRLd2VLZtV+ExCVDTo=",
        privateKey : "TODO", //"private_z60vq5B2MydsmNQ+c2dMsOG0RBg=",
        urlEndpoint : "TODO", //"https://ik.imagekit.io/sidehustlify",
    },

    STRIPE_API: {
        prod: "",
        dev: "",
        webhook_dev: "",
        webhook_prod: "",
    },

    ONESIGNAL_CONFIGS: {
        apiKey: "Zjg0YWQ0N2ItZjE1My00YzA0LWEyZjQtNzYwNzRlYTYwYWRm",
        appId: "85481ed6-4da1-4fb5-8977-7ac0aef1ba95"
    },

    SOCIAL_NETWORK_API_KEY: {
        metaAccessToken: "EAAQ3R0WZBuhQBO4wgk0FVBYtR5AW3LXdt16ooXhQWFblBAAcs4regd7oPneyvttZANmZBxK2DeWh0dWNCHeonCNpGLZBdvFiRZCnaccfaVR38JaGk6nq4TqCGos2qcKT6mvG93tRlc3P5BtYoFRbgZBAOuywFpD1D2raC6fDtN9P2ppTNHQiiqK2pBAJ6CZCXoZD",
        facebookPageId: "369397752917028",
        instagramId: "17841467508644379",
        threadsAPI: {
            clientId: "475396208369345",
            clientSecret: "f602953900ad64eee7e90641bcc47e85",
            redirectUri: `https://us-central1-${PRODUCTION_GCLOUD_PROJECT}.cloudfunctions.net/threadsAPIHandleAuthCallback`,
        },
        redditAPI: {
            clientId: "4AipG2HfGPnuOhtXtOYEmA",
            clientSecret: "9swPagUhH079Yq3uzQjyimQssUYhow",
            username: "FortulyApps",
            password: "divadlooc7",
            subReddit: "BitcoinTodayNews",
        },
        telegram: {
            channel: "",
        },
        bluesky: {
            username: "",
            password: "",
        }
    }
}