import {
    JobDetailsModel, JobListModel, JobsDiscoverDataUtils
} from "../../components/react-shared-module/base-projects/jobs-discover/jobs-discover-data.model";
import CustomRootLayoutComponent from "../../components/app-components/root/CustomRootLayout.component";
import {
    FirebaseClientFirestoreUtils,
    getAPIDocument
} from "../../components/react-shared-module/logic/shared-data/firebase.utils";
import {
    SharedFirestoreCollectionDB
} from "../../components/react-shared-module/logic/shared-data/datamodel/shared-firestore-collections";
import JobDetailsPageComponent
    from "../../components/react-shared-module/base-projects/jobs-discover/ui/job-details-page.component";
import NotFoundContentComponent
    from "../../components/react-shared-module/ui-components/shared/NotFoundContent.component";
import {PageHeadProps} from "../../components/react-shared-module/ui-components/root/RootLayout.component";
import {useRouter} from "next/router";
import PageLoadingSkeletonComponent
    from "../../components/react-shared-module/ui-components/shared/PageLoadingSkeleton.component";
import { SharedRoutesUtils } from "../../components/react-shared-module/utils/shared-routes.utils";
import RootConfigs from "../../configs";

export async function getStaticPaths() {
    let listRecentJobs: JobDetailsModel[] | undefined = await FirebaseClientFirestoreUtils.getDocumentsWithPagination(SharedFirestoreCollectionDB.JobsDiscoverProject.JobDetails, 50, "createdAt");
    let listPaths: any[] = [];

    if (listRecentJobs) {
        listPaths = listRecentJobs
            .filter((jobItem) => jobItem && jobItem.companyLogo && jobItem.title && jobItem.applicationUrl)
            .map((jobItem) => {
                return {
                    params: {
                        jobId: jobItem.uid!,
                    },
                }
            });
    }

    return {
        paths: listPaths,
        fallback: true, // can also be true or 'blocking'
    }
}

export async function getStaticProps(context: any) {
    const jobId: string = context.params.jobId as string;

    const jobData: JobDetailsModel | undefined = await getAPIDocument(SharedFirestoreCollectionDB.JobsDiscoverProject.JobDetails, jobId);


    if (jobData) {
        // Get similar jobs
        let listSimilarJobs: JobDetailsModel[] = await FirebaseClientFirestoreUtils
            .getDocumentsFilteredWithPagination({
                collectionPath: SharedFirestoreCollectionDB.JobsDiscoverProject.JobDetails,
                fieldOrderBy: "createdAt",
                filter: {
                    field: "category",
                    value: jobData?.category!,
                },
                pageSize: 5,
            });

        return {
            props: {
                jobData: jobData,
                similarJobs: listSimilarJobs,
            },
            revalidate: 60, // each 60 seconds
        };
    } else {
        return {
            props: {
            },
            revalidate: 60, // each 60 seconds
        }
    }

}

export default function JobDetailsPage(props: {jobData?: JobDetailsModel, similarJobs?: JobListModel[]}) {
    const router = useRouter();

    // Handle the fallback state
    if (router.isFallback) {
        return (
            <CustomRootLayoutComponent>
                <PageLoadingSkeletonComponent />
            </CustomRootLayoutComponent>
        );
    }


    if (props.jobData) {
        const thumbnailUrl: string = props.jobData.companyThumbnailUrl ? props.jobData.companyThumbnailUrl : props.jobData?.companyLogo.replaceAll("amp;", "");
        const pageHeadParams: PageHeadProps = {
            title: `${props.jobData?.title!} [Remote Job] @${props.jobData.company}`,
            description: JobsDiscoverDataUtils.generateSEODescription(props.jobData),
            imgUrl: thumbnailUrl,
            publishedTime: new Date(props.jobData?.createdAt!).toString(),
            jsonLdMarkup: JobsDiscoverDataUtils.generateJobPostingSchema(props.jobData),
            canonicalUrl: `${RootConfigs.BASE_URL}${SharedRoutesUtils.getJobDetailsPageUrl(props.jobData.uid!)}`
        };
        return (
            <CustomRootLayoutComponent customBody={true} pageHeadProps={pageHeadParams} customBackgroundColor={"bg-slate-50"}>
                <JobDetailsPageComponent jobData={props.jobData} similarJobs={props.similarJobs} />
            </CustomRootLayoutComponent>
        )
    } else {
        return (
            <CustomRootLayoutComponent isIndexable={false}>
                <NotFoundContentComponent />
            </CustomRootLayoutComponent>
        )
    }
}
