import CustomRootLayoutComponent from "../../components/app-components/root/CustomRootLayout.component";
import React, { useState } from 'react';
import { useRouter } from 'next/router';
import JobSubmissionResultPage from "../../components/react-shared-module/base-projects/jobs-discover/ui/job-submission-result.page";
import SpinnerComponent from "../../components/react-shared-module/ui-components/shared/Spinner.component";

export default function JobPostSubmissionPage() {
    const router = useRouter();
    const { result, jobId } = router.query;

    if (result && jobId) {
        return (
            <CustomRootLayoutComponent isIndexable={false} customBody={true}>
                <JobSubmissionResultPage isSuccess={result === 'success'} jobId={jobId as string} />
            </CustomRootLayoutComponent>
        )
    }

    return (

        <CustomRootLayoutComponent isIndexable={false} customBody={true}>
            <div className="min-h-screen py-8">
                <SpinnerComponent />
            </div>
        </CustomRootLayoutComponent>
    )
}
