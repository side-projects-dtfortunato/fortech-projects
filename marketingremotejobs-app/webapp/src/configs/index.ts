
export default class RootConfigs {
    static ENVIRONMENT_DEV = false;
    static BASE_URL = "https://www.marketingremotejobs.app";
    static SITE_NAME = "MarketingRemoteJobs.app";
    static SITE_TITLE = "Marketing Remote Jobs | Find Remote Marketing Positions";
    static SITE_DESCRIPTION = "Discover top remote marketing jobs worldwide. Find remote positions in digital marketing, content, SEO, social media, and more. Apply to work-from-home marketing roles today.";
    static CURRENT_NICHE_SITE = RootConfigs.SITE_NAME;
    static CONTACT_EMAIL = "contact@marketingremotejobs.app";
    static SITE_TOPIC = "Marketing Jobs";
    static THEME = {
        PRIMARY_COLOR: "rose",
    }
    static META_CONFIGS: any = {
        updateNicheBackend: false,
        disableGoogleSignin: true,
        disableCookies: true,
        disableAppStoreBanner: false,
        disableGoogleAds: false,
        enableJobSearch: true,
    }
    static APP_STORE_LINKS = {
        androidAppId: "TODO",
        iosAppId: "TODO",
    }
    static SOCIAL_NETWORKS: {
        [socialId: string]: {
            iconUrl: string,
            label: string,
            link: string,
        },
    } = {
        facebook: {
            label: "Facebook",
            link: "https://www.facebook.com/profile.php?id=61569097752032",
            iconUrl: "/images/ic_social_facebook.png",
        },
        instagram: {
            label: "Instagram",
            link: "https://www.instagram.com/marketingremotejobs.app/",
            iconUrl: "/images/ic_social_instagram.png",
        },
        threads: {
            label: "Threads",
            link: "https://www.threads.net/@marketingremotejobs.app",
            iconUrl: "/images/ic_social_threads.png",
        },
        bluesky: {
            label: "Bluesky",
            link: "https://marketingremote.bsky.social",
            iconUrl: "/images/ic_social_bluesky.png",
        },
        telegram: {
            label: "Telegram Community",
            link: "https://t.me/marketingremotejobs",
            iconUrl: "/images/ic_social_threads.png",
        },
    }
    static FIREBASE_CONFIG = this.ENVIRONMENT_DEV ?

        // DEV Config
        {
            apiKey: "AIzaSyCB2yrNkEHbpShTH_xwHOzcl2mXJT5i1K8",
            authDomain: "fortuly-qa-env.firebaseapp.com",
            projectId: "fortuly-qa-env",
            storageBucket: "fortuly-qa-env.appspot.com",
            messagingSenderId: "650128691290",
            appId: "1:650128691290:web:69f8a283090d23e0454863",
            measurementId: "G-V6XN6K51VZ"
        } :
        // PROD
        {
            apiKey: "AIzaSyCWysnX8m8bxkL8yagvYT9n2iB62P9H1C4",
            authDomain: "marketingremotejobs-app.firebaseapp.com",
            databaseURL: "https://marketingremotejobs-app-default-rtdb.firebaseio.com",
            projectId: "marketingremotejobs-app",
            storageBucket: "marketingremotejobs-app.firebasestorage.app",
            messagingSenderId: "677184024828",
            appId: "1:677184024828:web:1053e6658b745def9ce779",
            measurementId: "G-6LRMV696KK"
        };


    static IMAGEKIT_API = {
        publicKey : "public_OurCtnmj0jWqjVslk5YDfKE4aHQ=",
        privateKey : "private_vRUtPf6HGOj68f5rZZN8u9RHfiQ=",
        urlEndpoint : "https://ik.imagekit.io/fortulyapps",
    }

    static ONE_SIGNAL_API = "baab181f-77b9-4a8a-9a29-c9a0b11d1e92";
}
