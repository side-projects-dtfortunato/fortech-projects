
export const PRODUCTION_GCLOUD_PROJECT = "marketingremotejobs-app";

export const WEBAPP_CONFIGS = {
    LOGO_URL: "https://www.marketingremotejobs.app/images/logo-white-bg.jpg",
    SITE_TAGLINE: "Marketing Remote Jobs | Find Remote Marketing Positions",
    SITE_NAME: "MarketingRemoteJobs.app",
    WEBSITE_URL: "https://marketingremotejobs.app",
    COMPANY_ADDRESS: "3030 Coimbra (Portugal)",
    COMPANY_EMAIL: "contact@marketingremotejobs.app",
    POST_PREFIX_URL: "https://www.marketingremotejobs.app/post/",
    PROFILE_PREFIX_URL: "https://www.marketingremotejobs.app/public-profile/",
    UNSUBSCRIBE_URL: `https://us-central1-${PRODUCTION_GCLOUD_PROJECT}.cloudfunctions.net/unsubscribeEmail?uid=`,

    MAILERSEND_API_KEY: "TODO",

    CUSTOM_CONFIGS: {
        openPushInApp: true,
    } as any,

    // Account: contact@sidehustlify.com - Di.....oc7
    IMAGE_KIT_API: {
        publicKey : "TODO", // "public_QPb3OvUFUKRLd2VLZtV+ExCVDTo=",
        privateKey : "TODO", //"private_z60vq5B2MydsmNQ+c2dMsOG0RBg=",
        urlEndpoint : "TODO", //"https://ik.imagekit.io/sidehustlify",
    },

    STRIPE_API: {
        prod: "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",
        dev: "sk_test_51PueA3HT2hxH9IRDdlrmFSjPbHmjQ1An0PNkz74ZkAOw50c1MSK5nG0MGBWOvbm5RjkBDgG4VXnMDZ2yRBWJGtpA00wlrvUV9J",
        webhook_dev: "whsec_jySvwwZUSe3BLDFdJPYWn6c2vABfZbDd",
        webhook_prod: "whsec_X9XsUXjjwUJak0SIcsqPcFOa7jcPjdph",
    },

    ONESIGNAL_CONFIGS: {
        apiKey: "os_v2_app_xkvrqh3xxffivgrjzgqlchi6sk6tw5luyzxeisnhj3do3ajjl2c6wfud2unn5pk7piay3wj57eas5ilgb7s2d5rwan6t7bjpwkrehya",
        appId: "baab181f-77b9-4a8a-9a29-c9a0b11d1e92"
    },

    SOCIAL_NETWORK_API_KEY: {
        metaAccessToken: "EAAN9wFXTOkMBO9IoQCiqcEiJpYrZApUufBfZAVhmuWmYqLaKf2YqGu2bkTQi34pRkt0xMuOzLgExwGD9K51isExxZCZAfNfe4GkE5bTAcyzouUSzlpbtXlmWXCr5W9p5ZA1bGtz65PMefEZCwZBt2VhSIzrpwERpHvC1r8JXbPeoZCWfgoGSy1QpZCmoly7PD0UViXLL8WkkZB",
        facebookPageId: "488412227688139",
        instagramId: "17841470908669996",
        threadsAPI: {
            clientId: "2709114229297275",
            clientSecret: "dd6a94cb58372504a723aa1681b7ee57",
            redirectUri: `https://us-central1-${PRODUCTION_GCLOUD_PROJECT}.cloudfunctions.net/threadsAPIHandleAuthCallback`,
        },
        redditAPI: {
            clientId: "4AipG2HfGPnuOhtXtOYEmA",
            clientSecret: "9swPagUhH079Yq3uzQjyimQssUYhow",
            username: "FortulyApps",
            password: "d****c7",
            subReddit: "",
        },
        bluesky: {
            username: "marketingremote.bsky.social",
            password: "divadlooc7",
        },
        telegram: {
            channel: "@marketingremotejobs",
            botToken: "7977634269:AAGhiPyhBSmXAVpwwk3aE_l34CfBNhMvTa0",
        }
    }
}