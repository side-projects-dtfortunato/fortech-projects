import {
    JobDetailsModel,
    LinkedinSourceModel
} from "../../shareable-firebase-backend/base-projects/jobs-discover/shared-jobs-discover.data";


/**
 * Regions ids:
 * - UK: 101165590
 * - Australia: 101452733
 * - USA: 103644278
 * - EU: 91000002
 */
/*export const LINKEDIN_SOURCES: LinkedinSourceModel[] = [
    {
        id: "1",
        category: "Content Marketing",
        region: "USA",
        url: "https://www.linkedin.com/jobs/search?keywords=Content%20Marketing&geoId=103644278&f_TPR=&f_WT=2&f_E=2%2C3%2C4%2C5&position=1&pageNum=0",
    },
    {
        id: "2",
        category: "Digital Marketing",
        region: "USA",
        url: "https://www.linkedin.com/jobs/search?keywords=Digital%20Marketing&geoId=103644278&f_TPR=&f_WT=2&f_E=2%2C3%2C4%2C5&position=1&pageNum=0",
    },
    {
        id: "3",
        category: "Social Media Marketing",
        region: "USA",
        url: "https://www.linkedin.com/jobs/search?keywords=Social%20Media%20Marketing&geoId=103644278&f_TPR=&f_WT=2&f_E=2%2C3%2C4%2C5&position=1&pageNum=0",
    },
    {
        id: "4",
        category: "SEO Marketing",
        region: "USA",
        url: "https://www.linkedin.com/jobs/search?keywords=SEO%20Marketing&geoId=103644278&f_TPR=&f_WT=2&f_E=2%2C3%2C4%2C5&position=1&pageNum=0",
    },
    {
        id: "5",
        category: "Growth Marketing",
        region: "USA",
        url: "https://www.linkedin.com/jobs/search?keywords=Growth%20Marketing&geoId=103644278&f_TPR=&f_WT=2&f_E=2%2C3%2C4%2C5&position=1&pageNum=0",
    },
    {
        id: "6",
        category: "Performance Marketing",
        region: "USA",
        url: "https://www.linkedin.com/jobs/search?keywords=Performance%20Marketing&geoId=103644278&f_TPR=&f_WT=2&f_E=2%2C3%2C4%2C5&position=1&pageNum=0",
    },
    {
        id: "7",
        category: "Email Marketing",
        region: "USA",
        url: "https://www.linkedin.com/jobs/search?keywords=Marketing%20Analytics&geoId=103644278&f_TPR=&f_WT=2&f_E=2%2C3%2C4%2C5&position=1&pageNum=0",
    },
    {
        id: "8",
        category: "Product Marketing",
        region: "USA",
        url: "https://www.linkedin.com/jobs/search?keywords=Product%20Marketing&geoId=103644278&f_TPR=&f_WT=2&f_E=2%2C3%2C4%2C5&position=1&pageNum=0",
    },
    {
        id: "9",
        category: "Brand Marketing",
        region: "USA",
        url: "https://www.linkedin.com/jobs/search?keywords=Brand%20Marketing&geoId=103644278&f_TPR=&f_WT=2&f_E=2%2C3%2C4%2C5&position=1&pageNum=0",
    },
    // EU
    {
        id: "10",
        category: "Content Marketing",
        region: "EU",
        url: "https://www.linkedin.com/jobs/search?keywords=Content%20Marketing&geoId=91000002&f_TPR=&f_WT=2&f_E=2%2C3%2C4%2C5&position=1&pageNum=0",
    },
    {
        id: "11",
        category: "Digital Marketing",
        region: "EU",
        url: "https://www.linkedin.com/jobs/search?keywords=Digital%20Marketing&geoId=91000002&f_TPR=&f_WT=2&f_E=2%2C3%2C4%2C5&position=1&pageNum=0",
    },
    {
        id: "12",
        category: "Social Media Marketing",
        region: "EU",
        url: "https://www.linkedin.com/jobs/search?keywords=Social%20Media%20Marketing&geoId=91000002&f_TPR=&f_WT=2&f_E=2%2C3%2C4%2C5&position=1&pageNum=0",
    },
    {
        id: "13",
        category: "SEO Marketing",
        region: "EU",
        url: "https://www.linkedin.com/jobs/search?keywords=SEO%20arketing&geoId=91000002&f_TPR=&f_WT=2&f_E=2%2C3%2C4%2C5&position=1&pageNum=0",
    },
    {
        id: "14",
        category: "Growth Marketing",
        region: "EU",
        url: "https://www.linkedin.com/jobs/search?keywords=Growth%20Marketing&geoId=91000002&f_TPR=&f_WT=2&f_E=2%2C3%2C4%2C5&position=1&pageNum=0",
    },
    {
        id: "15",
        category: "Performance Marketing",
        region: "EU",
        url: "https://www.linkedin.com/jobs/search?keywords=Performance%20Marketing&geoId=91000002&f_TPR=&f_WT=2&f_E=2%2C3%2C4%2C5&position=1&pageNum=0",
    },
    {
        id: "16",
        category: "Email Marketing",
        region: "EU",
        url: "https://www.linkedin.com/jobs/search?keywords=Marketing%20Analytics&geoId=91000002&f_TPR=&f_WT=2&f_E=2%2C3%2C4%2C5&position=1&pageNum=0",
    },
    {
        id: "17",
        category: "Product Marketing",
        region: "EU",
        url: "https://www.linkedin.com/jobs/search?keywords=Product%20Marketing&geoId=91000002&f_TPR=&f_WT=2&f_E=2%2C3%2C4%2C5&position=1&pageNum=0",
    },
    {
        id: "18",
        category: "Brand Marketing",
        region: "EU",
        url: "https://www.linkedin.com/jobs/search?keywords=Brand%20Marketing&geoId=91000002&f_TPR=&f_WT=2&f_E=2%2C3%2C4%2C5&position=1&pageNum=0",
    },
    // Australia
    {
        id: "19",
        category: "Content Marketing",
        region: "Australia",
        url: "https://www.linkedin.com/jobs/search?keywords=Content%20Marketing&geoId=101452733&f_TPR=&f_WT=2&f_E=2%2C3%2C4%2C5&position=1&pageNum=0",
    },
    {
        id: "20",
        category: "Digital Marketing",
        region: "Australia",
        url: "https://www.linkedin.com/jobs/search?keywords=Digital%20Marketing&geoId=101452733&f_TPR=&f_WT=2&f_E=2%2C3%2C4%2C5&position=1&pageNum=0",
    },
    {
        id: "21",
        category: "Social Media Marketing",
        region: "Australia",
        url: "https://www.linkedin.com/jobs/search?keywords=Social%20Media%20Marketing&geoId=101452733&f_TPR=&f_WT=2&f_E=2%2C3%2C4%2C5&position=1&pageNum=0",
    },
    {
        id: "22",
        category: "SEO Marketing",
        region: "Australia",
        url: "https://www.linkedin.com/jobs/search?keywords=SEO%20Marketing&geoId=101452733&f_TPR=&f_WT=2&f_E=2%2C3%2C4%2C5&position=1&pageNum=0",
    },
    {
        id: "23",
        category: "Growth Marketing",
        region: "Australia",
        url: "https://www.linkedin.com/jobs/search?keywords=Growth%20Marketing&geoId=101452733&f_TPR=&f_WT=2&f_E=2%2C3%2C4%2C5&position=1&pageNum=0",
    },
    {
        id: "24",
        category: "Performance Marketing",
        region: "Australia",
        url: "https://www.linkedin.com/jobs/search?keywords=Performance%20Marketing&geoId=101452733&f_TPR=&f_WT=2&f_E=2%2C3%2C4%2C5&position=1&pageNum=0",
    },
    {
        id: "25",
        category: "Email Marketing",
        region: "Australia",
        url: "https://www.linkedin.com/jobs/search?keywords=Marketing%20Analytics&geoId=101452733&f_TPR=&f_WT=2&f_E=2%2C3%2C4%2C5&position=1&pageNum=0",
    },
    {
        id: "26",
        category: "Product Marketing",
        region: "Australia",
        url: "https://www.linkedin.com/jobs/search?keywords=Product%20Marketing&geoId=101452733&f_TPR=&f_WT=2&f_E=2%2C3%2C4%2C5&position=1&pageNum=0",
    },
    {
        id: "27",
        category: "Brand Marketing",
        region: "Australia",
        url: "https://www.linkedin.com/jobs/search?keywords=Brand%20Marketing&geoId=101452733&f_TPR=&f_WT=2&f_E=2%2C3%2C4%2C5&position=1&pageNum=0",
    },
    // UK
    {
        id: "28",
        category: "Content Marketing",
        region: "UK",
        url: "https://www.linkedin.com/jobs/search?keywords=Content%20Marketing&geoId=101165590&f_TPR=&f_WT=2&f_E=2%2C3%2C4%2C5&position=1&pageNum=0",
    },
    {
        id: "29",
        category: "Digital Marketing",
        region: "UK",
        url: "https://www.linkedin.com/jobs/search?keywords=Digital%20Marketing&geoId=101165590&f_TPR=&f_WT=2&f_E=2%2C3%2C4%2C5&position=1&pageNum=0",
    },
    {
        id: "30",
        category: "Social Media Marketing",
        region: "UK",
        url: "https://www.linkedin.com/jobs/search?keywords=Social%20Media%20Marketing&geoId=101165590&f_TPR=&f_WT=2&f_E=2%2C3%2C4%2C5&position=1&pageNum=0",
    },
    {
        id: "31",
        category: "SEO Marketing",
        region: "UK",
        url: "https://www.linkedin.com/jobs/search?keywords=SEO%20Marketing&geoId=101165590&f_TPR=&f_WT=2&f_E=2%2C3%2C4%2C5&position=1&pageNum=0",
    },
    {
        id: "32",
        category: "Growth Marketing",
        region: "UK",
        url: "https://www.linkedin.com/jobs/search?keywords=Growth%20Marketing&geoId=101165590&f_TPR=&f_WT=2&f_E=2%2C3%2C4%2C5&position=1&pageNum=0",
    },
    {
        id: "33",
        category: "Performance Marketing",
        region: "UK",
        url: "https://www.linkedin.com/jobs/search?keywords=Performance%20Marketing&geoId=101165590&f_TPR=&f_WT=2&f_E=2%2C3%2C4%2C5&position=1&pageNum=0",
    },
    {
        id: "34",
        category: "Email Marketing",
        region: "UK",
        url: "https://www.linkedin.com/jobs/search?keywords=Marketing%20Analytics&geoId=101165590&f_TPR=&f_WT=2&f_E=2%2C3%2C4%2C5&position=1&pageNum=0",
    },
    {
        id: "35",
        category: "Product Marketing",
        region: "UK",
        url: "https://www.linkedin.com/jobs/search?keywords=Product%20Marketing&geoId=101165590&f_TPR=&f_WT=2&f_E=2%2C3%2C4%2C5&position=1&pageNum=0",
    },
    {
        id: "36",
        category: "Brand Marketing",
        region: "UK",
        url: "https://www.linkedin.com/jobs/search?keywords=Brand%20Marketing&geoId=101165590&f_TPR=&f_WT=2&f_E=2%2C3%2C4%2C5&position=1&pageNum=0",
    },
]*/

export const ALLOWED_WORK_MODE = ["REMOTE"];

export const JOB_REGIONS = [
    {
        regionLabel: "EU",
        regionId: "91000002"
    },
    {
        regionLabel: "UK",
        regionId: "101165590"
    },
    {
        regionLabel: "Australia",
        regionId: "101452733"
    },
    {
        regionLabel: "USA",
        regionId: "103644278"
    },
] as {regionLabel: string, regionId: string}[];

export const JOB_CATEGORIES = [
    {
        jobCategoryLabel: "Content Marketing",
        jobCategoryQuery: "Content Marketing"
    },
    {
        jobCategoryLabel: "Digital Marketing",
        jobCategoryQuery: "Digital Marketing"
    },
    {
        jobCategoryLabel: "Social Media Marketing",
        jobCategoryQuery: "Social Media Marketing"
    },
    {
        jobCategoryLabel: "SEO Marketing",
        jobCategoryQuery: "SEO Marketing"
    },
    {
        jobCategoryLabel: "Growth Marketing",
        jobCategoryQuery: "Growth Marketing"
    },
    {
        jobCategoryLabel: "Performance Marketing",
        jobCategoryQuery: "Performance Marketing"
    },
    {
        jobCategoryLabel: "Email Marketing",
        jobCategoryQuery: "Email Marketing"
    },
    {
        jobCategoryLabel: "Product Marketing",
        jobCategoryQuery: "Product Marketing"
    },
    {
        jobCategoryLabel: "Brand Marketing",
        jobCategoryQuery: "Brand Marketing"
    }
] as {jobCategoryLabel: string, jobCategoryQuery: string}[];

export class CustomJobsDiscoverUtils {

    static importJobsFromRemoteJobsHub(): JobDetailsModel[] {
        return [];
    }

    static getIsValidRule() {
        return "";
    }

}