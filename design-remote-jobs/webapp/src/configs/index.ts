
export default class RootConfigs {
    static ENVIRONMENT_DEV = false;
    static BASE_URL = "https://www.designremotejobs.com";
    static SITE_NAME = "DesignRemoteJobs.com";
    static SITE_TITLE = "Design Remote Jobs | Find Remote Graphic Designer Job Positions";
    static SITE_DESCRIPTION = "Find remote graphic design jobs worldwide. Browse hundreds of remote positions for graphic designers, UI/UX designers, and creative professionals. Work from anywhere.";
    static CURRENT_NICHE_SITE = RootConfigs.SITE_NAME;
    static CONTACT_EMAIL = "contact@designremotejobs.com";
    static SITE_TOPIC = "Design Remote Jobs";
    static THEME = {
        PRIMARY_COLOR: "rose",
    }
    static META_CONFIGS: any = {
        updateNicheBackend: false,
        disableGoogleSignin: true,
        disableCookies: true,
        disableAppStoreBanner: false,
        disableGoogleAds: false,
        enableJobSearch: true,
    }
    static APP_STORE_LINKS = {
        androidAppId: "TODO",
        iosAppId: "TODO",
    }
    static SOCIAL_NETWORKS: {
        [socialId: string]: {
            iconUrl: string,
            label: string,
            link: string,
        },
    } = {
        facebook: {
            label: "Facebook",
            link: "https://www.facebook.com/profile.php?id=61565899179933",
            iconUrl: "/images/ic_social_facebook.png",
        },
        instagram: {
            label: "Instagram",
            link: "https://www.instagram.com/customer_remote_jobs/",
            iconUrl: "/images/ic_social_instagram.png",
        },
        threads: {
            label: "Threads",
            link: "https://www.threads.net/@customer_remote_jobs",
            iconUrl: "/images/ic_social_threads.png",
        },
        bluesky: {
            label: "Bluesky",
            link: "https://marketingremote.bsky.social",
            iconUrl: "/images/ic_social_bluesky.png",
        },
        telegram: {
            label: "Telegram Community",
            link: "https://t.me/designremotejobs",
            iconUrl: "/images/ic_social_threads.png",
        },
    }
    static FIREBASE_CONFIG = this.ENVIRONMENT_DEV ?

        // DEV Config
        {
            apiKey: "AIzaSyCB2yrNkEHbpShTH_xwHOzcl2mXJT5i1K8",
            authDomain: "fortuly-qa-env.firebaseapp.com",
            projectId: "fortuly-qa-env",
            storageBucket: "fortuly-qa-env.appspot.com",
            messagingSenderId: "650128691290",
            appId: "1:650128691290:web:69f8a283090d23e0454863",
            measurementId: "G-V6XN6K51VZ"
        } :
        // PROD
        {
            apiKey: "AIzaSyCBmbPZKBaFj3P68UCxUM8Z43j2HqGHqBw",
            authDomain: "designremotejobs.firebaseapp.com",
            projectId: "designremotejobs",
            storageBucket: "designremotejobs.firebasestorage.app",
            messagingSenderId: "981147692584",
            appId: "1:981147692584:web:afd7839ca2215a4a08cf1d",
            measurementId: "G-7X3TR08GNC"
        };


    static IMAGEKIT_API = {
        publicKey : "public_OurCtnmj0jWqjVslk5YDfKE4aHQ=",
        privateKey : "private_vRUtPf6HGOj68f5rZZN8u9RHfiQ=",
        urlEndpoint : "https://ik.imagekit.io/fortulyapps",
    }

    static ONE_SIGNAL_API = "8ea62c2a-23ce-498f-a59e-bc2e7474d552";
}
