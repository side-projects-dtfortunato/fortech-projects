
export const PRODUCTION_GCLOUD_PROJECT = "designremotejobs";

export const WEBAPP_CONFIGS = {
    LOGO_URL: "https://www.designremotejobs.com/images/logo-white-bg.jpg",
    SITE_TAGLINE: "Find Graphic Design Remote Jobs",
    SITE_NAME: "designremotejobs.com",
    WEBSITE_URL: "https://designremotejobs.com",
    COMPANY_ADDRESS: "3030 Coimbra (Portugal)",
    COMPANY_EMAIL: "contact@designremotejobs.com",
    POST_PREFIX_URL: "https://www.designremotejobs.com/post/",
    PROFILE_PREFIX_URL: "https://www.designremotejobs.com/public-profile/",
    UNSUBSCRIBE_URL: `https://us-central1-${PRODUCTION_GCLOUD_PROJECT}.cloudfunctions.net/unsubscribeEmail?uid=`,

    CUSTOM_CONFIGS: {
        openPushInApp: true,
    } as any,

    // Account: contact@sidehustlify.com - Di.....oc7
    IMAGE_KIT_API: {
        publicKey : "TODO", // "public_QPb3OvUFUKRLd2VLZtV+ExCVDTo=",
        privateKey : "TODO", //"private_z60vq5B2MydsmNQ+c2dMsOG0RBg=",
        urlEndpoint : "TODO", //"https://ik.imagekit.io/sidehustlify",
    },

    STRIPE_API: {
        prod: "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",
        dev: "sk_test_51PueA3HT2hxH9IRDdlrmFSjPbHmjQ1An0PNkz74ZkAOw50c1MSK5nG0MGBWOvbm5RjkBDgG4VXnMDZ2yRBWJGtpA00wlrvUV9J",
        webhook_dev: "whsec_jySvwwZUSe3BLDFdJPYWn6c2vABfZbDd",
        webhook_prod: "whsec_3cPjLs9T9KRPevRChKEl2pHLSiOgoGX4",
    },

    ONESIGNAL_CONFIGS: {
        apiKey: "os_v2_app_r2tcykrdzzey7jm6xqxhi5gvkiz56hl7sa3emenizrxrfdofvdz62swuktigukyngt7t6m4bxltfj7kzbv7c2txfigyp3i6alttwi2i",
        appId: "8ea62c2a-23ce-498f-a59e-bc2e7474d552"
    },

    SOCIAL_NETWORK_API_KEY: {
        metaAccessToken: "EAASk2F8u4SIBO0vvd069R1cFpwtkOOsKtmjPKGLLuLKRysc7ZB3JYhcDOKgXvqu0KdCZAYvYknguEeV7cUpIoZAxvZCFnL8KwEPvzTGCnxLJZAtPqU0NgUIHL7IHBrQ2jQVb5GwwWbVfzG58LS9bd2b8T1XDMi4TovYRh9Q6LaZATFT6g06qVqVeIKnQ2DniOX8r775rYw",
        facebookPageId: "449251454949057",
        instagramId: "17841470882128234",
        threadsAPI: {
            clientId: "611270297923526",
            clientSecret: "a39f1878b5c816169d84769f8f0c04da",
            redirectUri: `https://us-central1-${PRODUCTION_GCLOUD_PROJECT}.cloudfunctions.net/threadsAPIHandleAuthCallback`,
        },
        redditAPI: {
            clientId: "4AipG2HfGPnuOhtXtOYEmA",
            clientSecret: "9swPagUhH079Yq3uzQjyimQssUYhow",
            username: "FortulyApps",
            password: "d****c7",
            subReddit: "",
        },
        bluesky: {
            username: "designremotejobs.bsky.social",
            password: "divadlooc7",
        },
        telegram: {
            channel: "@designremotejobs",
            botToken: "7592746257:AAHmQ4gYtaU1xs6nPry5mTluW1lpQzApOzs",
        }
    }
}