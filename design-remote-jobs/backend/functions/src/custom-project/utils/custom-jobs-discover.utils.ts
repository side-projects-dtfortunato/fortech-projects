import {
    JobDetailsModel,
    LinkedinSourceModel
} from "../../shareable-firebase-backend/base-projects/jobs-discover/shared-jobs-discover.data";


/**
 * Regions ids:
 * - UK: 101165590
 * - Australia: 101452733
 * - USA: 103644278
 * - EU: 91000002
 */
/*export const LINKEDIN_SOURCES: LinkedinSourceModel[] = [
    // USA
    {
        id: "1",
        category: "Graphic Design",
        region: "USA",
        url: "https://www.linkedin.com/jobs/search?keywords=Graphic Design&geoId=103644278&f_TPR=&f_WT=2&f_E=2%2C3%2C4%2C5&position=1&pageNum=0",
    },
    {
        id: "2",
        category: "UI UX Design",
        region: "USA",
        url: "https://www.linkedin.com/jobs/search?keywords=UI UX Design&geoId=103644278&f_TPR=&f_WT=2&f_E=2%2C3%2C4%2C5&position=1&pageNum=0",
    },
    {
        id: "3",
        category: "Brand Design",
        region: "USA",
        url: "https://www.linkedin.com/jobs/search?keywords=Brand Design&geoId=103644278&f_TPR=&f_WT=2&f_E=2%2C3%2C4%2C5&position=1&pageNum=0",
    },
    {
        id: "4",
        category: "Web Design",
        region: "USA",
        url: "https://www.linkedin.com/jobs/search?keywords=Web Design&geoId=103644278&f_TPR=&f_WT=2&f_E=2%2C3%2C4%2C5&position=1&pageNum=0",
    },
    {
        id: "5",
        category: "Product Design",
        region: "USA",
        url: "https://www.linkedin.com/jobs/search?keywords=Product Design&geoId=103644278&f_TPR=&f_WT=2&f_E=2%2C3%2C4%2C5&position=1&pageNum=0",
    },
    {
        id: "6",
        category: "3D Design",
        region: "USA",
        url: "https://www.linkedin.com/jobs/search?keywords=3D Design&geoId=103644278&f_TPR=&f_WT=2&f_E=2%2C3%2C4%2C5&position=1&pageNum=0",
    },
    {
        id: "7",
        category: "Illustration",
        region: "USA",
        url: "https://www.linkedin.com/jobs/search?keywords=Illustration&geoId=103644278&f_TPR=&f_WT=2&f_E=2%2C3%2C4%2C5&position=1&pageNum=0",
    },

    // UK
    {
        id: "8",
        category: "Graphic Design",
        region: "UK",
        url: "https://www.linkedin.com/jobs/search?keywords=Graphic Design&geoId=101165590&f_TPR=&f_WT=2&f_E=2%2C3%2C4%2C5&position=1&pageNum=0",
    },
    {
        id: "9",
        category: "UI UX Design",
        region: "UK",
        url: "https://www.linkedin.com/jobs/search?keywords=UI UX Design&geoId=101165590&f_TPR=&f_WT=2&f_E=2%2C3%2C4%2C5&position=1&pageNum=0",
    },
    {
        id: "10",
        category: "Brand Design",
        region: "UK",
        url: "https://www.linkedin.com/jobs/search?keywords=Brand Design&geoId=101165590&f_TPR=&f_WT=2&f_E=2%2C3%2C4%2C5&position=1&pageNum=0",
    },
    {
        id: "11",
        category: "Web Design",
        region: "UK",
        url: "https://www.linkedin.com/jobs/search?keywords=Web Design&geoId=101165590&f_TPR=&f_WT=2&f_E=2%2C3%2C4%2C5&position=1&pageNum=0",
    },
    {
        id: "12",
        category: "Product Design",
        region: "UK",
        url: "https://www.linkedin.com/jobs/search?keywords=Product Design&geoId=101165590&f_TPR=&f_WT=2&f_E=2%2C3%2C4%2C5&position=1&pageNum=0",
    },
    {
        id: "13",
        category: "3D Design",
        region: "UK",
        url: "https://www.linkedin.com/jobs/search?keywords=3D Design&geoId=101165590&f_TPR=&f_WT=2&f_E=2%2C3%2C4%2C5&position=1&pageNum=0",
    },
    {
        id: "14",
        category: "Illustration",
        region: "UK",
        url: "https://www.linkedin.com/jobs/search?keywords=Illustration&geoId=101165590&f_TPR=&f_WT=2&f_E=2%2C3%2C4%2C5&position=1&pageNum=0",
    },

    // EU
    {
        id: "15",
        category: "Graphic Design",
        region: "EU",
        url: "https://www.linkedin.com/jobs/search?keywords=Graphic Design&geoId=91000002&f_TPR=&f_WT=2&f_E=2%2C3%2C4%2C5&position=1&pageNum=0",
    },
    {
        id: "16",
        category: "UI UX Design",
        region: "EU",
        url: "https://www.linkedin.com/jobs/search?keywords=UI UX Design&geoId=91000002&f_TPR=&f_WT=2&f_E=2%2C3%2C4%2C5&position=1&pageNum=0",
    },
    {
        id: "17",
        category: "Brand Design",
        region: "EU",
        url: "https://www.linkedin.com/jobs/search?keywords=Brand Design&geoId=91000002&f_TPR=&f_WT=2&f_E=2%2C3%2C4%2C5&position=1&pageNum=0",
    },
    {
        id: "18",
        category: "Web Design",
        region: "EU",
        url: "https://www.linkedin.com/jobs/search?keywords=Web Design&geoId=91000002&f_TPR=&f_WT=2&f_E=2%2C3%2C4%2C5&position=1&pageNum=0",
    },
    {
        id: "19",
        category: "Product Design",
        region: "EU",
        url: "https://www.linkedin.com/jobs/search?keywords=Product Design&geoId=91000002&f_TPR=&f_WT=2&f_E=2%2C3%2C4%2C5&position=1&pageNum=0",
    },
    {
        id: "20",
        category: "3D Design",
        region: "EU",
        url: "https://www.linkedin.com/jobs/search?keywords=3D Design&geoId=91000002&f_TPR=&f_WT=2&f_E=2%2C3%2C4%2C5&position=1&pageNum=0",
    },
    {
        id: "21",
        category: "Illustration",
        region: "EU",
        url: "https://www.linkedin.com/jobs/search?keywords=Illustration&geoId=91000002&f_TPR=&f_WT=2&f_E=2%2C3%2C4%2C5&position=1&pageNum=0",
    },

    // Australia
    {
        id: "22",
        category: "Graphic Design",
        region: "Australia",
        url: "https://www.linkedin.com/jobs/search?keywords=Graphic Design&geoId=101452733&f_TPR=&f_WT=2&f_E=2%2C3%2C4%2C5&position=1&pageNum=0",
    },
    {
        id: "23",
        category: "UI UX Design",
        region: "Australia",
        url: "https://www.linkedin.com/jobs/search?keywords=UI UX Design&geoId=101452733&f_TPR=&f_WT=2&f_E=2%2C3%2C4%2C5&position=1&pageNum=0",
    },
    {
        id: "24",
        category: "Brand Design",
        region: "Australia",
        url: "https://www.linkedin.com/jobs/search?keywords=Brand Design&geoId=101452733&f_TPR=&f_WT=2&f_E=2%2C3%2C4%2C5&position=1&pageNum=0",
    },
    {
        id: "25",
        category: "Web Design",
        region: "Australia",
        url: "https://www.linkedin.com/jobs/search?keywords=Web Design&geoId=101452733&f_TPR=&f_WT=2&f_E=2%2C3%2C4%2C5&position=1&pageNum=0",
    },
    {
        id: "26",
        category: "Product Design",
        region: "Australia",
        url: "https://www.linkedin.com/jobs/search?keywords=Product Design&geoId=101452733&f_TPR=&f_WT=2&f_E=2%2C3%2C4%2C5&position=1&pageNum=0",
    },
    {
        id: "27",
        category: "3D Design",
        region: "Australia",
        url: "https://www.linkedin.com/jobs/search?keywords=3D Design&geoId=101452733&f_TPR=&f_WT=2&f_E=2%2C3%2C4%2C5&position=1&pageNum=0",
    },
    {
        id: "28",
        category: "Illustration",
        region: "Australia",
        url: "https://www.linkedin.com/jobs/search?keywords=Illustration&geoId=101452733&f_TPR=&f_WT=2&f_E=2%2C3%2C4%2C5&position=1&pageNum=0",
    },
]*/

export const ALLOWED_WORK_MODE = ["REMOTE"];

export const JOB_REGIONS = [
    {
        regionLabel: "EU",
        regionId: "91000002"
    },
    {
        regionLabel: "UK",
        regionId: "101165590",
    },
    {
        regionLabel: "Australia",
        regionId: "101452733",
    },
    {
        regionLabel: "USA",
        regionId: "103644278",
    }
] as {regionLabel: string, regionId: string}[];


export const JOB_CATEGORIES = [
    {
        jobCategoryLabel: "Graphic Design",
        jobCategoryQuery: "Graphic Design"
    },
    {
        jobCategoryLabel: "UI UX Design",
        jobCategoryQuery: "UI UX Design"
    },
    {
        jobCategoryLabel: "Brand Design",
        jobCategoryQuery: "Brand Design"
    },
    {
        jobCategoryLabel: "Web Design",
        jobCategoryQuery: "Web Design"
    },
    {
        jobCategoryLabel: "Product Design",
        jobCategoryQuery: "Product Design"
    },
    {
        jobCategoryLabel: "3D Design",
        jobCategoryQuery: "3D Design"
    },
    {
        jobCategoryLabel: "Illustration",
        jobCategoryQuery: "Illustration"
    }
]  as {jobCategoryLabel: string, jobCategoryQuery: string}[];

export class CustomJobsDiscoverUtils {

    static importJobsFromRemoteJobsHub(): JobDetailsModel[] {
        return [];
    }

    static getIsValidRule() {
        return "";
    }

}