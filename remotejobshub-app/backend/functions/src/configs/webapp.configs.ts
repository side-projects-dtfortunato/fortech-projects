
export const PRODUCTION_GCLOUD_PROJECT = "remotejobshub-app";

export const WEBAPP_CONFIGS = {
    LOGO_URL: "https://www.remotejobshub.app/images/logo-white-bg.jpg",
    SITE_TAGLINE: "RemoteJobsHub.app | Latest Remote Jobs & Work-From-Home Insights",
    SITE_NAME: "RemoteJobsHub.app",
    WEBSITE_URL: "https://www.remotejobshub.app",
    COMPANY_ADDRESS: "3030 Coimbra (Portugal)",
    COMPANY_EMAIL: "contact@remotejobshub.app",
    POST_PREFIX_URL: "https://www.remotejobshub.app/post/",
    PROFILE_PREFIX_URL: "https://www.remotejobshub.app/public-profile/",
    UNSUBSCRIBE_URL: `https://us-central1-${PRODUCTION_GCLOUD_PROJECT}.cloudfunctions.net/unsubscribeEmail?uid=`,

    CUSTOM_CONFIGS: {
        openPushInApp: true,
    } as any,

    // Account: contact@sidehustlify.com - Di.....oc7
    IMAGE_KIT_API: {
        publicKey : "TODO", // "public_QPb3OvUFUKRLd2VLZtV+ExCVDTo=",
        privateKey : "TODO", //"private_z60vq5B2MydsmNQ+c2dMsOG0RBg=",
        urlEndpoint : "TODO", //"https://ik.imagekit.io/sidehustlify",
    },

    STRIPE_API: {
        prod: "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",
        dev: "sk_test_51PueA3HT2hxH9IRDdlrmFSjPbHmjQ1An0PNkz74ZkAOw50c1MSK5nG0MGBWOvbm5RjkBDgG4VXnMDZ2yRBWJGtpA00wlrvUV9J",
        webhook_dev: "whsec_jySvwwZUSe3BLDFdJPYWn6c2vABfZbDd",
        webhook_prod: "whsec_1DTLaaRme2qQda1xZ3tPq2DLPfc0OIEr",
    },

    ONESIGNAL_CONFIGS: {
        apiKey: "NjI5NTA2YWQtYTUyYi00MTk3LTgyMGQtODUyZmY1N2Q4YjBi",
        appId: "1c657b68-f42e-40a9-a5d9-f08981387ab4"
    },

    SOCIAL_NETWORK_API_KEY: {
        metaAccessToken: "EAAHwAeOg8NoBOZBxVRZCZANV2Rxv8ZA9R3HPkNbTbETGBl1PPc0UEU2LZCCclXt0CMR9XOQrswovkw1YllxNeodC9OZC0rlaDjEwRJYIvxwT4HZBmxi3oPEvhPo67BIm9xXDCY39gyu8EqeAs8D6BSAI2eq1DDFpP7V3mutZCbK3zHSRePjCoZANdwFpyZAQPyuLMZD",
        facebookPageId: "417176938144443",
        instagramId: "17841468567577037",
        threadsAPI: {
            clientId: "893483479371664",
            clientSecret: "81e2c1702ba41af37e25c97ceb47029e",
            redirectUri: `https://us-central1-${PRODUCTION_GCLOUD_PROJECT}.cloudfunctions.net/threadsAPIHandleAuthCallback`,
        },
        redditAPI: {
            clientId: "4AipG2HfGPnuOhtXtOYEmA",
            clientSecret: "9swPagUhH079Yq3uzQjyimQssUYhow",
            username: "FortulyApps",
            password: "d****c7",
            subReddit: "",
        },
        bluesky: {
            username: "remotejobshub-app.bsky.social",
            password: "divadlooc7",
        },
        telegram: {
            channel: "@remotejobshub_app",
            botToken: "7226282675:AAEHcvn4uVDnJUt7-looJ31QdNKHo_lMrK0",
        }
    }
}