import * as functions from "firebase-functions";
import { OnesignalUtils } from "../../shareable-firebase-backend/utils/onesignal.utils";
import { getQuickResponse } from "../../shareable-firebase-backend/model-data/api-response.model";

export const testFunction = functions.https.onRequest(
    async (req, resp) => {

        await OnesignalUtils.sendPushNotificationToExternalUserIds(
            "Data Entry Assistant (100% Remote)",
            "Elitetownhires",
            "https://www.remotejobshub.app/job/elitetownhires-data-entry-assistant-100-remote",
            ["g64IFxcO03djGrXpM0NLfMym3AA3"],
        );
        resp.send(getQuickResponse(true, "Notification sent successfully"));
    });