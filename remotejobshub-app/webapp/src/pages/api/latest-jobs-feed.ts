import {NextApiRequest, NextApiResponse} from "next";
import {
    DailyPublishedArticlesModel
} from "../../components/react-shared-module/base-projects/articles-digest/data/model/DailyPublishedArticles.model";
import {FirebaseClientFirestoreUtils} from "../../components/react-shared-module/logic/shared-data/firebase.utils";
import {
    SharedFirestoreCollectionDB
} from "../../components/react-shared-module/logic/shared-data/datamodel/shared-firestore-collections";
import RootConfigs from "../../configs";
import {Feed} from "feed";
import {SharedRoutesUtils} from "../../components/react-shared-module/utils/shared-routes.utils";
import {
    DailyPublishedJobs, JobsDiscoverDataUtils
} from "../../components/react-shared-module/base-projects/jobs-discover/jobs-discover-data.model";

const ITEMS_PER_PAGE = 5;

// Function to encode special characters in text
function encodeSpecialChars(text: string): string {
    if (!text) return '';
    return text
        .replace(/&(?!amp;|lt;|gt;|quot;|apos;|#\d+;|#x[0-9a-fA-F]+;)/g, '&amp;')
        .replace(/[""]/g, '&quot;')
        .replace(/['′]/g, '&apos;')
        .replace(/[–—]/g, '-')
        .replace(/…/g, '...')
        .replace(/</g, '&lt;')
        .replace(/>/g, '&gt;');
}

// Function to sanitize URLs
function sanitizeUrl(url: string): string {
    if (!url) return '';
    // Ensure URLs are properly encoded while preserving already encoded characters
    return url.replace(/&(?!amp;|lt;|gt;|quot;|apos;|#\d+;|#x[0-9a-fA-F]+;)/g, '&amp;');
}

export default async function RssLatestJobsFeed(req: NextApiRequest, res: NextApiResponse) {
    const listPublishedDays: DailyPublishedJobs[] = await FirebaseClientFirestoreUtils
        .getDocumentsWithPagination(SharedFirestoreCollectionDB.JobsDiscoverProject.DailyPublishedJobs, ITEMS_PER_PAGE, "publishedDayId");
    let author = {
        name: RootConfigs.SITE_NAME,
        email: RootConfigs.CONTACT_EMAIL,
        link: RootConfigs.BASE_URL,
    }
    const feed = new Feed({
        title: RootConfigs.SITE_TITLE + " - Latest Remote Jobs",
        description: RootConfigs.SITE_DESCRIPTION,
        id: RootConfigs.BASE_URL,
        link: RootConfigs.BASE_URL,
        language: "en", // optional, used only in RSS 2.0, possible values: http://www.w3.org/TR/REC-html40/struct/dirlang.html#langcodes
        image: RootConfigs.BASE_URL + "/images/logo-512.png",
        favicon: RootConfigs.BASE_URL + "/favicon.ico",
        copyright: "All rights reserved " + new Date().getUTCFullYear()  + ", " + RootConfigs.SITE_NAME,
        updated: new Date(), // optional, default = today
        author: author
    });

    listPublishedDays.forEach((dailyPublished: DailyPublishedJobs) => {
        if (dailyPublished.listJobs) {
            Object.values(dailyPublished.listJobs).forEach((jobItem) => {
                const imageUrl: string = validateAndFixImageUrl(jobItem.companyThumbnailUrl!);
                feed.addItem({
                    title: jobItem.title,
                    link: RootConfigs.BASE_URL + SharedRoutesUtils.getJobDetailsPageUrl(jobItem.uid!),
                    id: jobItem.uid!,
                    image: imageUrl,
                    description: JobsDiscoverDataUtils.generateSEODescription(jobItem),
                    category: [{name: jobItem.category}],
                    published: new Date(jobItem.createdAt!),
                    date: new Date(jobItem.createdAt!),
                    author: [author],
                });
            });
        }
    });

    feed.addCategory(RootConfigs.SITE_TOPIC);

    res.status(200).setHeader('Content-Type', 'application/xml').send(feed.rss2());
}

function validateAndFixImageUrl(imageUrl: string): string {
    if (!imageUrl) return RootConfigs.BASE_URL + "/images/logo-white-bg.jpg";

    // Regular expression to match common image file extensions
    const imageRegex = /^https?:\/\/.*\.(jpg|jpeg|gif|png|tiff|bmp|aspx|JPEG|JPG|PNG)$/i;

    // Sanitize the URL before testing
    const sanitizedUrl = sanitizeUrl(imageUrl);

    if (imageRegex.test(sanitizedUrl)) {
        return sanitizedUrl;
    } else {
        return RootConfigs.BASE_URL + "/images/logo-white-bg.jpg";
    }
}