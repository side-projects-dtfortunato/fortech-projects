import { NextApiRequest, NextApiResponse } from "next";
import {
    DailyPublishedArticlesModel
} from "../../components/react-shared-module/base-projects/articles-digest/data/model/DailyPublishedArticles.model";
import { FirebaseClientFirestoreUtils } from "../../components/react-shared-module/logic/shared-data/firebase.utils";
import {
    SharedFirestoreCollectionDB
} from "../../components/react-shared-module/logic/shared-data/datamodel/shared-firestore-collections";
import RootConfigs from "../../configs";
import { Feed } from "feed";
import { SharedRoutesUtils } from "../../components/react-shared-module/utils/shared-routes.utils";

const ITEMS_PER_PAGE = 5;

// Function to encode special characters in text
function encodeSpecialChars(text: string): string {
    if (!text) return '';
    return text
        .replace(/&(?!amp;|lt;|gt;|quot;|apos;|#\d+;|#x[0-9a-fA-F]+;)/g, '&amp;')
        .replace(/[""]/g, '&quot;')
        .replace(/['′]/g, '&apos;')
        .replace(/[–—]/g, '-')
        .replace(/…/g, '...')
        .replace(/</g, '&lt;')
        .replace(/>/g, '&gt;');
}

// Function to sanitize URLs
function sanitizeUrl(url: string): string {
    if (!url) return '';
    // Ensure URLs are properly encoded while preserving already encoded characters
    return url.replace(/&(?!amp;|lt;|gt;|quot;|apos;|#\d+;|#x[0-9a-fA-F]+;)/g, '&amp;');
}

export default async function RssLatestArticlesFeed(req: NextApiRequest, res: NextApiResponse) {
    try {
        const listPublishedDays: DailyPublishedArticlesModel[] = await FirebaseClientFirestoreUtils
            .getDocumentsWithPagination(SharedFirestoreCollectionDB.ArticlesDigestProject.DailyPublishedArticles, ITEMS_PER_PAGE, "publishedDayId");

        const author = {
            name: encodeSpecialChars(RootConfigs.SITE_NAME),
            email: RootConfigs.CONTACT_EMAIL,
            link: sanitizeUrl(RootConfigs.BASE_URL),
        };

        const feed = new Feed({
            title: encodeSpecialChars(RootConfigs.SITE_TITLE + " - Recent Articles"),
            description: encodeSpecialChars(RootConfigs.SITE_DESCRIPTION),
            id: sanitizeUrl(RootConfigs.BASE_URL),
            link: sanitizeUrl(RootConfigs.BASE_URL),
            language: "en",
            image: sanitizeUrl(RootConfigs.BASE_URL + "/images/logo-512.png"),
            favicon: sanitizeUrl(RootConfigs.BASE_URL + "/favicon.ico"),
            copyright: encodeSpecialChars(`All rights reserved ${new Date().getUTCFullYear()}, ${RootConfigs.SITE_NAME}`),
            updated: new Date(),
            author: author,
        });

        listPublishedDays.forEach((dailyPublished: DailyPublishedArticlesModel) => {
            if (dailyPublished.listArticles) {
                Object.values(dailyPublished.listArticles).forEach((articleItem) => {

                    const imageUrl: string = validateAndFixImageUrl(articleItem.imageUrl!);

                    // Wrap description in CDATA if it contains HTML
                    let description = articleItem.aiGeneratedContent!.highlightsBullets![0];
                    description = description?.includes('<') ?
                        `<![CDATA[${articleItem.description}]]>` :
                        encodeSpecialChars(articleItem.description || '');

                    feed.addItem({
                        title: encodeSpecialChars(articleItem.title),
                        link: sanitizeUrl(RootConfigs.BASE_URL + SharedRoutesUtils.getArticleDetails(articleItem.uid!)),
                        id: articleItem.uid!,
                        image: sanitizeUrl(imageUrl),
                        description: description,
                        category: articleItem.aiGeneratedContent?.tags ?
                            articleItem.aiGeneratedContent.tags.map(tag => ({
                                name: encodeSpecialChars(tag)
                            })) : [],
                        published: new Date(articleItem.createdAt!),
                        date: new Date(articleItem.createdAt!),
                        author: [author],
                    });
                });
            }
        });

        feed.addCategory(encodeSpecialChars(RootConfigs.SITE_TOPIC));

        // Set proper content type and encoding headers
        res.setHeader('Content-Type', 'application/rss+xml; charset=utf-8');
        res.setHeader('Cache-Control', 'public, max-age=3600'); // Optional: Add caching headers

        const rssOutput = feed.rss2();
        res.status(200).send(rssOutput);
    } catch (error) {
        console.error('Error generating RSS feed:', error);
        res.status(500).json({ error: 'Error generating RSS feed' });
    }
}

function validateAndFixImageUrl(imageUrl: string): string {
    if (!imageUrl) return RootConfigs.BASE_URL + "/images/logo-white-bg.jpg";

    // Regular expression to match common image file extensions
    const imageRegex = /^https?:\/\/.*\.(jpg|jpeg|gif|png|tiff|bmp|aspx|JPEG|JPG|PNG)$/i;

    // Sanitize the URL before testing
    const sanitizedUrl = sanitizeUrl(imageUrl);

    if (imageRegex.test(sanitizedUrl)) {
        return sanitizedUrl;
    } else {
        return RootConfigs.BASE_URL + "/images/logo-white-bg.jpg";
    }
}