import CustomRootLayoutComponent from "../../components/app-components/root/CustomRootLayout.component";
import JobSubmissionForm
    from "../../components/react-shared-module/base-projects/jobs-discover/ui/job-submission-form.component";
import {
    JobDetailsModel
} from "../../components/react-shared-module/base-projects/jobs-discover/jobs-discover-data.model";
import {SharedBackendApi} from "../../components/react-shared-module/logic/shared-data/sharedbackend.api";
import {useRouter} from "next/router";
import RootConfigs from "../../configs";
import { PageHeadProps } from "../../components/react-shared-module/ui-components/root/RootLayout.component";
import { SharedRoutesUtils } from "../../components/react-shared-module/utils/shared-routes.utils";
import { UIHelper } from "../../components/ui-helper/UIHelper";

export default function SubmitJobPage() {
    const route = useRouter();
    
    const jobCategories = UIHelper.getJobsCategories();
    const categoriesText = jobCategories.slice(0, 3).join(', ') + ` and more`;

    const pageHeadParams: PageHeadProps = {
        title: `Post a Job | ${RootConfigs.SITE_NAME}`,
        description: `Post job opportunities in ${categoriesText} on ${RootConfigs.SITE_NAME}.`,
        imgUrl: "/images/logo.png",
        publishedTime: new Date().toString(),
        canonicalUrl: `${RootConfigs.BASE_URL}${SharedRoutesUtils.getSubmitJob()}`
    };

    return (
        <CustomRootLayoutComponent isIndexable={false} customBody={true} customBackgroundColor={"bg-slate-50"} pageHeadProps={pageHeadParams}>
            <JobSubmissionForm onSubmit={async (jobData: JobDetailsModel) => {
                const response = await SharedBackendApi.submitJobPost({jobPost: jobData});

                if (response.isSuccess && response.responseData.redirectUrl) {
                    await route.push(response.responseData.redirectUrl);

                    return response.isSuccess;
                } else {
                    return false;
                }
            }
            }/>
        </CustomRootLayoutComponent>
    )
}