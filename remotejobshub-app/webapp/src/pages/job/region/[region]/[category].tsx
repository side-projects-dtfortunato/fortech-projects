import { JobListModel, JobsDiscoverDataUtils } from "../../../../components/react-shared-module/base-projects/jobs-discover/jobs-discover-data.model";
import CustomRootLayoutComponent from "../../../../components/app-components/root/CustomRootLayout.component";
import JobRegionCategoryHeader from "../../../../components/react-shared-module/base-projects/jobs-discover/ui/job-region-category-header.component";
import JobListItemComponent from "../../../../components/react-shared-module/base-projects/jobs-discover/ui/job-list-item.component";
import { PageHeadProps } from "../../../../components/react-shared-module/ui-components/root/RootLayout.component";
import RootConfigs from "../../../../configs";
import EmptyListMessageComponent from "../../../../components/react-shared-module/ui-components/shared/EmptyListMessage.component";
import { UIHelper } from "../../../../components/ui-helper/UIHelper";
import { getLanguageLabel } from "../../../../components/react-shared-module/logic/language/language.helper";

export async function getStaticPaths() {
    const regions = UIHelper.getJobsFilterRegions();
    const categories = UIHelper.getJobsCategories();
    
    const paths = regions.flatMap(region => 
        categories.map(category => ({
            params: { region, category }
        }))
    );

    return {
        paths,
        fallback: true,
    }
}

export async function getStaticProps(context: any) {
    const { region, category } = context.params;

    let listJobs: JobListModel[] | undefined = await JobsDiscoverDataUtils.fetchFilteredJobs({
        category,
        remoteRegion: region,
    });

    if (!listJobs) {
        listJobs = [];
    }

    return {
        props: {
            listJobs,
            region,
            category,
        },
        revalidate: 60, // each 60 seconds
    };
}

interface JobByRegionAndCategoryProps {
    listJobs: JobListModel[];
    region: string;
    category: string;
}

export default function JobByRegionAndCategory({ listJobs, region, category }: JobByRegionAndCategoryProps) {
    function renderListJobs() {
        if (listJobs && listJobs.length > 0) {
            return listJobs.map((job) => (
                <JobListItemComponent key={job.uid!} jobData={job} />
            ));
        }
        
        return (
            <EmptyListMessageComponent 
                message={getLanguageLabel(
                    "JOBS_EMPTY_REGION_CATEGORY_MESSAGE",
                    `We don't have any ${category} remote jobs in ${region} at the moment. Please check back later.`
                ).replace("#CATEGORY", category).replace("#REGION", region)} 
            />
        );
    }

    const pageHeadProps: PageHeadProps = {
        title: getLanguageLabel("JOBS_REGION_CATEGORY_TITLE", `${category} Remote Jobs in ${region} | ${RootConfigs.SITE_NAME}`)
        .replace("#CATEGORY", category).replace("#REGION", region),
        description: generateSEODescription({
            region,
            category,
            jobCount: listJobs?.length,
        }),
        imgUrl: RootConfigs.BASE_URL + "/images/logo-white-bg.png",
        publishedTime: listJobs && listJobs.length > 0 
            ? new Date(listJobs[0].createdAt!).toString() 
            : new Date().toString(),
    }

    return (
        <CustomRootLayoutComponent pageHeadProps={pageHeadProps}>
            <JobRegionCategoryHeader region={region} category={category} />
            <div className='flex flex-col'>
                {renderListJobs()}
            </div>
        </CustomRootLayoutComponent>
    )
}

interface SEODescriptionParams {
    region: string;
    category: string;
    jobCount?: number;
    updateFrequency?: string;
}

function generateSEODescription({
    region,
    category,
    jobCount,
    updateFrequency = 'daily',
}: SEODescriptionParams): string {
    let description = getLanguageLabel("JOBS_REGION_CATEGORY_SEO_DESCRIPTION",
        `Find the latest ${category} remote jobs in ${region}.`)
        .replace("#CATEGORY", category).replace("#REGION", region);

    return description.slice(0, 160);
} 