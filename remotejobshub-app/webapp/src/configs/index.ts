
export default class RootConfigs {
    static ENVIRONMENT_DEV = false;
    static BASE_URL = "https://remotejobshub.app";
    static SITE_NAME = "RemoteJobsHub.app";
    static SITE_TITLE = "RemoteJobsHub.app | Latest Remote Jobs & Work-From-Home Insights";
    static SITE_DESCRIPTION = "Discover top remote job opportunities across various categories at Remote Jobs Hub. Stay informed with the latest news and articles on remote working trends, tips, and best practices. Your one-stop destination for finding your ideal remote career and mastering the work-from-home lifestyle.";
    static CURRENT_NICHE_SITE = RootConfigs.SITE_NAME;
    static CONTACT_EMAIL = "contact@remotejobshub.app";
    static SITE_TOPIC = "Remote Jobs";
    static THEME = {
        PRIMARY_COLOR: "blue",
    }
    static META_CONFIGS: any = {
        updateNicheBackend: false,
        disableGoogleSignin: true,
        disableCookies: true,
        disableAppStoreBanner: false,
        disableCommunitySeparator: true,
        enableJobSearch: true,
    }
    static APP_STORE_LINKS = {
        androidAppId: "remotejobshub.app",
        iosAppId: "6624295582",
    }
    static SOCIAL_NETWORKS: {
        [socialId: string]: {
            iconUrl: string,
            label: string,
            link: string,
        },
    } = {
        facebook: {
            label: "Facebook",
            link: "https://www.facebook.com/profile.php?id=61565189224753",
            iconUrl: "/images/ic_social_facebook.png",
        },
        instagram: {
            label: "Instagram",
            link: "https://www.instagram.com/remotejobshub.app/",
            iconUrl: "/images/ic_social_instagram.png",
        },
        threads: {
            label: "Threads",
            link: "https://www.threads.net/@remotejobshub.app/",
            iconUrl: "/images/ic_social_threads.png",
        },
        bluesky: {
            label: "Bluesky",
            link: "https://juniorremotejobs.bsky.social",
            iconUrl: "/images/ic_social_bluesky.png",
        },
        telegram: {
            label: "Telegram Community",
            link: "https://t.me/remotejobshub_app",
            iconUrl: "/images/ic_social_threads.png",
        },
    }
    static FIREBASE_CONFIG = this.ENVIRONMENT_DEV ?

        // DEV Config
        {
            apiKey: "AIzaSyCB2yrNkEHbpShTH_xwHOzcl2mXJT5i1K8",
            authDomain: "fortuly-qa-env.firebaseapp.com",
            projectId: "fortuly-qa-env",
            storageBucket: "fortuly-qa-env.appspot.com",
            messagingSenderId: "650128691290",
            appId: "1:650128691290:web:69f8a283090d23e0454863",
            measurementId: "G-V6XN6K51VZ"
        } :
        // PROD
        {
            apiKey: "AIzaSyCwmdzGlTgcovh0qkMDaknypWPz_EdVPBU",
            authDomain: "remotejobshub-app.firebaseapp.com",
            databaseURL: "https://remotejobshub-app-default-rtdb.firebaseio.com",
            projectId: "remotejobshub-app",
            storageBucket: "remotejobshub-app.appspot.com",
            messagingSenderId: "1017381093826",
            appId: "1:1017381093826:web:072228b9be177e38bf965b",
            measurementId: "G-NZD5HL9H9T"
        };


    static IMAGEKIT_API = {
        publicKey : "public_OurCtnmj0jWqjVslk5YDfKE4aHQ=",
        privateKey : "private_vRUtPf6HGOj68f5rZZN8u9RHfiQ=",
        urlEndpoint : "https://ik.imagekit.io/fortulyapps",
    }

    static ONE_SIGNAL_API = "1c657b68-f42e-40a9-a5d9-f08981387ab4";
}
