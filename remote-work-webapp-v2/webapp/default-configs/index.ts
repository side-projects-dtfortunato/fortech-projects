export enum RemoteWorkNicheSitesTypes {
    REMOTE_WORK = "REMOTE_WORK", REACTJOBS = "REACTJOBS",
}
export default class DefaultRootConfigs {
    static ENVIRONMENT_DEV = false;
    static BASE_URL = "https://remote-work.app";
    static SITE_NAME = "Remote-Work.app";
    static SITE_TITLE = "Remote Work App - Join the best community for Remote Workers and Remote Jobs";
    static SITE_DESCRIPTION = "Find the best Remote Jobs for IT, Finances, Design and other areas that allows to work remote. Join our community of remote workers.";
    static SOCIAL_NETWORKS: {
        [socialId: string]: {
            iconUrl: string,
            label: string,
            link: string,
        },
    } = {
        linkedin: {
            label: "LinkedIn",
            link: "https://www.linkedin.com/company/remote-work-app",
            iconUrl: "/images/ic_linkedin_white.png",
        },
        facebook: {
            label: "Facebook",
            link: "https://www.facebook.com/remoteworkdotapp",
            iconUrl: "/images/ic_social_facebook.png",
        },
        instagram: {
            label: "Instagram",
            link: "https://www.instagram.com/remote_work.app/",
            iconUrl: "/images/ic_social_instagram.png",
        },
        threads: {
            label: "Threads",
            link: "https://www.threads.net/@remote_work.app",
            iconUrl: "/images/ic_social_threads.png",
        },
        reddit: {
            label: "Reddit",
            link: "https://www.reddit.com/r/remoteworkapp/",
            iconUrl: "/images/ic_social_reddit.png",
        },
        telegram: {
            label: "Telegram",
            link: "https://t.me/remotework_app",
            iconUrl: "/images/ic_social_telegram.png",
        },
    }

    static CURRENT_NICHE_SITE: RemoteWorkNicheSitesTypes = RemoteWorkNicheSitesTypes.REMOTE_WORK;
    static META_CONFIGS: any = {
        disableGoogleSignin: true,
        defaultPostsAvailableNicheSites: [DefaultRootConfigs.CURRENT_NICHE_SITE],
        defaultJobsAvailableNicheSites: [DefaultRootConfigs.CURRENT_NICHE_SITE],
        defaultJobCategory: "fullstack",
        disableGoogleAds: false,
        enableJobSearch: true,
    }


    static FIREBASE_CONFIG = this.ENVIRONMENT_DEV ?

        // DEV Config
        {
            apiKey: "AIzaSyCB2yrNkEHbpShTH_xwHOzcl2mXJT5i1K8",
            authDomain: "fortuly-qa-env.firebaseapp.com",
            projectId: "fortuly-qa-env",
            storageBucket: "fortuly-qa-env.appspot.com",
            messagingSenderId: "650128691290",
            appId: "1:650128691290:web:69f8a283090d23e0454863",
            measurementId: "G-V6XN6K51VZ"
        } :
        // PROD
        {
            apiKey: "AIzaSyCgphdyrNi0Vd3Zm1eJYTCkYe6HQS1kzr0",
            authDomain: "remote-flex-jobs.firebaseapp.com",
            projectId: "remote-flex-jobs",
            storageBucket: "remote-flex-jobs.appspot.com",
            messagingSenderId: "689617790420",
            appId: "1:689617790420:web:3e71d3de5c33da65af6241",
            measurementId: "G-XNZFVLSQ5F"
        };



    static IMAGEKIT_API = {
        publicKey : "public_OurCtnmj0jWqjVslk5YDfKE4aHQ=",
        privateKey : "private_vRUtPf6HGOj68f5rZZN8u9RHfiQ=",
        urlEndpoint : "https://ik.imagekit.io/fortulyapps",
    }

}
