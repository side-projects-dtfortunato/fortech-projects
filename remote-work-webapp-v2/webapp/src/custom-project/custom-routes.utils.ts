import {SharedRoutesUtils} from "../components/react-shared-module/utils/shared-routes.utils";


export class CustomRoutesUtils {
    static getRemoteJobDetailsUrl(jobId: string) {
        return SharedRoutesUtils.getJobDetailsPageUrl(jobId);
    }
}