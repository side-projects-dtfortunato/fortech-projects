import 'firebase/compat/firestore';
import {JobPostDetailsModel, JobPostListInfoModel} from "./custom-data-model/job-post-details.model";
import {CustomFirestoreCollectionDB, CustomFirestoreDocsDB} from "./custom-firestore-collection-names";
import {
    callFunction,
    FirebaseClientFirestoreUtils,
    getAPIDocument
} from "../../components/react-shared-module/logic/shared-data/firebase.utils";
import {ApiResponse} from "../../components/react-shared-module/logic/shared-data/datamodel/base.model";
import {RemoteWorkNicheSitesUtils} from "./custom-data-model/remote-work-niche-sites";
import RootConfigs from "../../configs";
import {
    SharedFirestoreCollectionDB, SharedFirestoreDocsDB
} from "../../components/react-shared-module/logic/shared-data/datamodel/shared-firestore-collections";
import {BasePostDataModel} from "../../components/react-shared-module/logic/shared-data/datamodel/base-post-data.model";
import {
    DailyPublishedJobs,
    JobListModel
} from "../../components/react-shared-module/base-projects/jobs-discover/jobs-discover-data.model";


export class CustomBackendApi {
    static async getAllJobs(): Promise<JobPostListInfoModel[]> {
        let listJobs: { [jobId: string]: JobPostListInfoModel } | undefined = await getAPIDocument<{ [jobId: string]: JobPostDetailsModel } | undefined>(CustomFirestoreCollectionDB.CollectionDataCatalog, CustomFirestoreDocsDB.CollectionDataCatalog.JobsListCatalog);

        let listJobsArr: JobPostListInfoModel[] = listJobs ? Object.values(listJobs) : [];

        // Sort by date
        listJobsArr = listJobsArr.sort((j1, j2) => {
            if (!j1.publishedAt) return -1;
            if (!j2.publishedAt) return 1;
            return j2.publishedAt - j1.publishedAt
        });

        return listJobsArr.filter((job) => job.companyDetails);
    }

    static async getJobDetails(jobPostId: string): Promise<JobPostDetailsModel | undefined> {
        return await getAPIDocument(CustomFirestoreCollectionDB.CollectionJobPostDetails, jobPostId);
    }


    static async requestJobPostedVisited(jobPostId: string, updateType: "view" | "applied") {
        return await callFunction("requestJobPostedVisited", {
            jobPostId,
            updateType,
        });
    }

    static async publishRemoteJobFeed(jobPostId: string) {
        return await callFunction("publishRemoteJobFeed", {
            jobPostId,
        });
    }


    static async postRemoteJob(data: {
        jobPostDetails: JobPostDetailsModel
    }): Promise<ApiResponse<any>> {
        return await callFunction("postRemoteJob", {...data});
    }

    static async getRecentJobs(): Promise<JobListModel[]> {
        const listPublishedDays: DailyPublishedJobs[] = await FirebaseClientFirestoreUtils
            .getDocumentsWithPagination(SharedFirestoreCollectionDB.JobsDiscoverProject.DailyPublishedJobs, 2, "publishedDayId");

        let listRecentJobs: JobListModel[] = [];

        for (let publishJobsDay of listPublishedDays) {
            listRecentJobs = listRecentJobs.concat(Object.values(publishJobsDay.listJobs));
        }
        return listRecentJobs;
    }

    static async getRecentPosts(): Promise<BasePostDataModel[]> {
        let data: {[postId: string]: BasePostDataModel} | undefined = await getAPIDocument(SharedFirestoreCollectionDB.DataCatalog,
            SharedFirestoreDocsDB.DataCatalog.FilterRecent);

        // Filter Posts
        let listRecentPostsFiltered: BasePostDataModel[] = [];

        if (data && Object.keys(data).length > 0) {
            listRecentPostsFiltered = Object.values(data)
                .filter((postItem) => RemoteWorkNicheSitesUtils.isPostAvailableForCurrentSite(postItem) && postItem.title && postItem.title.length > 0);
        }


        return listRecentPostsFiltered;
    }


    static async importLinkedinJobPostDetails(data: {
        url: string,
        defaultJobCategory?: string,

    }): Promise<ApiResponse<any>> {
        return await callFunction("importLinkedinJobPostDetails", data);
    }
}