export const CustomFirestoreCollectionDB = {
    // Ingestion Collections
    ColIngestJobs: "IngestListJobsData",
    CompanyDetails: "CompanyDetails",

    CollectionJobPostDetails: "JobPostDetails",

    CollectionDataCatalog: "RemoteWorkDataCatalog"
}

export const CustomFirestoreDocsDB = {
    IngestListJobs: {
        DocIngestJobs: "IngestedListJobs",
        DocDigestedJobs: "DigestedListJobs",
    },
    CollectionDataCatalog: {
        CompaniesCatalog: "CompaniesCatalog",
        JobsListCatalog: "JobsListCatalog",
    },
}