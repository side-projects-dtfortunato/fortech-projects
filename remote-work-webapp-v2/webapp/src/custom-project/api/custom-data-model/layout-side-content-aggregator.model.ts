import {JobPostListInfoModel} from "./job-post-details.model";
import {
    BasePostDataModel
} from "../../../components/react-shared-module/logic/shared-data/datamodel/base-post-data.model";
import {CustomBackendApi} from "../custombackend.api";
import {
    JobListModel
} from "../../../components/react-shared-module/base-projects/jobs-discover/jobs-discover-data.model";

export interface LayoutSideContentAggregatorModel {
    recentJobs?: JobListModel[];
    recentPosts?: BasePostDataModel[];
}

const MAX_LIST_ITEMS = 5;

export class LayoutSideContentAggregatorUtils {
    static async getLayoutSideContentAggregatorData(props: {loadRecentJobs?: boolean, loadRecentPosts?: boolean}): Promise<LayoutSideContentAggregatorModel> {
        let aggData: LayoutSideContentAggregatorModel = {};

        if (props.loadRecentJobs) {
            aggData.recentJobs = (await CustomBackendApi.getRecentJobs())
                .sort((j1, j2) => j2.createdAt! - j1.createdAt!)
                .slice(0, MAX_LIST_ITEMS);
        }

        if (props.loadRecentPosts) {

            aggData.recentPosts = (await CustomBackendApi.getRecentPosts())
                .sort((p1, p2) => p2.publishedAt! - p1.publishedAt!)
                .slice(0, MAX_LIST_ITEMS);
        }

        return aggData;
    }

}