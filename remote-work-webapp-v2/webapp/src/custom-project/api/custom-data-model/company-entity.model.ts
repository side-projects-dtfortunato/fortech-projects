import {BaseModel} from "../../../components/react-shared-module/logic/shared-data/datamodel/base.model";
import {StorageFileModel} from "../../../components/react-shared-module/logic/shared-data/datamodel/storage-file.model";
import {LocationModel} from "../../../components/react-shared-module/logic/shared-data/datamodel/location.model";

export interface CatalogCompanyEntity extends BaseModel {
    companyLocation: LocationModel;
    companyName: string;
    companyLogo: StorageFileModel;
    companyThumbnailUrl?: string;
    hiringLocations?: {[locationId: string]: string};
    remoteRegionsAllowance?: {[regionKey: string]: boolean};
    industries?: string[];
    website: string;
}
