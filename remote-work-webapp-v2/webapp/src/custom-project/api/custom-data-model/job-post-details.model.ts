import {CatalogCompanyEntity} from "./company-entity.model";
import {BaseModel} from "../../../components/react-shared-module/logic/shared-data/datamodel/base.model";
import CommonUtils from "../../../components/react-shared-module/logic/commonutils";
import {FormOption} from "../../../components/react-shared-module/ui-components/form/RadioButton.component";


export enum RemoteZoneRestriction {
    WORLDWIDE = "WORLDWIDE",
    EUROPE = "EUROPE",
    ASIA = "ASIA",
    US = "US", UK = "UK", CANADA = "CANADA", SOUTH_AMERICA = "SOUTH_AMERICA", AFRICA = "AFRICA", AUSTRALIA = "AUSTRALIA", UNKNOWN = "UNKNOWN"
}

export enum JobPostStatus {
    DRAFT = "DRAFT",
    PENDING_PAYMENT = "PENDING_PAYMENT",
    PENDING_APPROVE = "PENDING_APPROVE",
    PUBLISHED = "PUBLISHED",
    EXPIRED = "EXPIRED",
    CANCELLED = "CANCELLED"
}

export enum JobExperienceLevel {
    ENTRY = "ENTRY",
    JUNIOR = "JUNIOR",
    MID = "MID",
    SENIOR = "SENIOR",
}

export enum JobApplicationType { URL= "URL", EMAIL = "EMAIL" }

export enum JobPostSource { ORIGINAL = "ORIGINAL", REMOTEOK = "REMOTEOK", WWR = "WWR" }

export enum JobCategories {fullstack = "fullstack", frontend = "frontend", backend = "backend", design = "design", devops = "devops", management_finance = "management_finance", product = "product", customer_support = "customer_support", salesmarketing = "salesmarketing", Other = "Other"}

export interface JobPostListInfoModel extends BaseModel {
    companyDetails: JobPostCompanyDetailsModel;
    positionTitle: string;
    jobSkills: JobSkills;
    remoteRegionsAllowance?: {
        [region: string]: boolean,
    };
    publishedAt?: number;
    jobCategory?: string;
    customZoneRestriction?: string;
    experienceLevel?: JobExperienceLevel;
}

export interface JobPostDetailsModel extends JobPostListInfoModel {
    positionDescription: string;
    descriptionContentType?: "HTML" | "MARKDOWN";
    expirationTime?: number;
    isFeature: boolean;
    applicationMethod: JobApplicationMethod;
    sourceUrl: string;
    sourceType: JobPostSource;
    rangeSalary?: JobRangeSalary;
    status?: JobPostStatus;
    stats?: JobPostStatsModel;
    jobCategory?: string;
    publisherUID?: string;
}

export interface JobPostStatsModel {
    views?: {[timestamp: string]: string}; // deprecated don't use it
    appliedBtn?: {[timestamp: string]: string}; // deprecated don't it
    numViews?: number;
    numApplications?: number;
}

export interface JobPostCompanyDetailsModel extends CatalogCompanyEntity {
    companyEmail?: string;
    description?: string;
    companyCareersLink?: string;
}

export interface JobSkills {
    jobCategory?: string;
    mainSkills: string[];
    secondarySkills: string[];
}

export interface JobApplicationMethod {
    url: string; // Can be an email
    applicationType: JobApplicationType;
}

export interface JobRangeSalary {
    minSalary: string;
    maxSalary: string;
    currency: string;
}

export class JobPostUtils {

    static getJobCategoryOptions():{[optionId: string]: FormOption} {
        let jobCategoryOptions: {[optionId: string]: FormOption} = {};

        for(let key in JobCategories){
            let label: any = key;

            switch (key.toString()) {
                case JobCategories.backend: label = "Backend Developer"; break;
                case JobCategories.customer_support: label = "Custom Support"; break;
                case JobCategories.design: label = "UX/UI Design"; break;
                case JobCategories.devops: label = "DevOps"; break;
                case JobCategories.frontend: label = "Frontend Developer"; break;
                case JobCategories.fullstack: label = "Fullstack Developer"; break;
                case JobCategories.management_finance: label = "Management & Finance"; break;
                case JobCategories.product: label = "Product Management"; break;
                case JobCategories.salesmarketing: label = "Sales/Marketing"; break;

                case JobCategories.Other:
                default:
                    label = "Other";
                    break;
            }
            jobCategoryOptions[key] = {
                optionId: key,
                label: label,
                displayOption: true,
            };
        }

        return jobCategoryOptions;
    }

    static getExperienceLevelsOptions():{[optionId: string]: FormOption} {
        let experienceLevelOptions: {[optionId: string]: FormOption} = {};

        for(let key in JobExperienceLevel){
            let label: any = key;

            switch (key.toString()) {
                case JobExperienceLevel.ENTRY: label = "Entry"; break;
                case JobExperienceLevel.JUNIOR: label = "Junior"; break;
                case JobExperienceLevel.MID: label = "Mid"; break;
                case JobExperienceLevel.SENIOR: label = "Senior"; break;
            }
            experienceLevelOptions[key] = {
                optionId: key,
                label: label,
                displayOption: true,
            };
        }

        return experienceLevelOptions;
    }

    static getRemoteRegionAllowance():{[optionId: string]: string} {
        let remoteRegionsAllowance: {[optionId: string]: string} = {};

        for(let key in RemoteZoneRestriction) {
            let label: string = key;

            switch (key) {
                case RemoteZoneRestriction.SOUTH_AMERICA: label = "🌎 LATAM"; break;
                case RemoteZoneRestriction.AUSTRALIA: label = "🇦🇺 Australia/Oceania"; break;
                case RemoteZoneRestriction.ASIA: label = "🌏 Asia"; break;
                case RemoteZoneRestriction.CANADA: label = "🇨🇦 Canada"; break;
                case RemoteZoneRestriction.US: label = "🇺🇸 USA"; break;
                case RemoteZoneRestriction.EUROPE: label = "🇪🇺 Europe"; break;
                case RemoteZoneRestriction.UK: label = "🇬🇧 UK"; break;
                case RemoteZoneRestriction.AFRICA: label = "🌍 Africa"; break;
                case RemoteZoneRestriction.WORLDWIDE:
                default:
                    label = "🌐 Worldwide"; break;
            }
            remoteRegionsAllowance[key] = label;
        }

        return remoteRegionsAllowance;
    }

    static generateCustomZoneRestrictionLabel(jobPostData: JobPostDetailsModel): JobPostDetailsModel {

        if (jobPostData.remoteRegionsAllowance) {
            jobPostData.customZoneRestriction = "Work from: ";
            let customLabel: string = "";
            let mapRegionLabels: {[optionId: string]: string} = this.getRemoteRegionAllowance();
            Object.keys(jobPostData.remoteRegionsAllowance).forEach((regionKey) => {
                if (jobPostData.remoteRegionsAllowance![regionKey] === true) {
                    if (customLabel) {
                        customLabel += "/ ";
                    }
                    customLabel += mapRegionLabels[regionKey];
                }
            });

            if (customLabel) {
                jobPostData.customZoneRestriction = "Work from: " + customLabel;
            }

        }
        return jobPostData;
    }



    static isJobPostDataValid(jobPostItem: JobPostDetailsModel): {errorMessage?: string, isValid: boolean} {
        let response: {errorMessage?: string, isValid: boolean} = {isValid: true};

        if (jobPostItem.positionTitle.length < 3) {
            response = {errorMessage: "Position title not valid", isValid: false};
        } else if (!CommonUtils.isValidHttpUrl(jobPostItem.applicationMethod.url)) {
            response = {errorMessage: "Invalid Application Link", isValid: false};
        } else if (!jobPostItem.jobSkills.jobCategory) {
            response = {errorMessage: "You didn't selected a job category", isValid: false};
        } else if (jobPostItem.positionDescription.length < 20) {
            response = {errorMessage: "Job description is too short", isValid: false};
        } else if (jobPostItem.companyDetails.companyName.length < 3) {
            response = {errorMessage: "Invalid company name", isValid: false};
        } else if (!CommonUtils.isValidHttpUrl(jobPostItem.companyDetails.website)) {
            response = {errorMessage: "Invalid company website", isValid: false};
        } else if (!CommonUtils.isValidHttpUrl(jobPostItem.companyDetails.companyLogo.fileUrl)) {
            response = {errorMessage: "Invalid company logo", isValid: false};
        } else if (!jobPostItem.experienceLevel) {
            response = {errorMessage: "Experience Level not selected.", isValid: false};
        }

        return response;
    }


}