
import {ProfessionalUserProfileModel} from "./professional-user-profile.model";
import {
    SharedCentralUserDataModel
} from "../../../components/react-shared-module/logic/shared-data/datamodel/shared-central-user-data.model";

export interface CustomCentralUserDataModel extends SharedCentralUserDataModel {
    proProfile?: ProfessionalUserProfileModel;
}