import {
    BasePostDataModel
} from "../../../components/react-shared-module/logic/shared-data/datamodel/base-post-data.model";
import RootConfigs from "../../../configs";
import {JobPostListInfoModel} from "./job-post-details.model";
import {SharedUIHelper} from "../../../components/react-shared-module/utils/shared-ui-helper";



export class RemoteWorkNicheSitesUtils {

    static isPostAvailableForCurrentSite(post: BasePostDataModel) {
        return SharedUIHelper.isPostAvailableForCurrentSite(post);
    }

    static isJobAvailableForCurrentSite(job: JobPostListInfoModel) {
        return true;
    }
}