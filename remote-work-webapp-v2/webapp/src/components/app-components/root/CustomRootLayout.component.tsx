import {FooterLinkItem} from "../../react-shared-module/ui-components/root/Footer.component";
import RootLayoutComponent, {PageHeadProps} from "../../react-shared-module/ui-components/root/RootLayout.component";
import Link from "next/link";
import {AiFillEdit} from "react-icons/ai";
import {useRouter} from "next/router";
import {CustomNavbarLinkData, UIHelper} from "../../ui-helper/UIHelper";
import LandingRecentJobsComponent from "../custom-landing-page/components/LandingRecentJobs.component";
import {
    LayoutSideContentAggregatorModel
} from "../../../custom-project/api/custom-data-model/layout-side-content-aggregator.model";
import LandingRecentPostsComponent from "../custom-landing-page/components/LandingRecentPosts.component";
import {useEffect, useState} from "react";
import {SharedRoutesUtils} from "../../react-shared-module/utils/shared-routes.utils";

export default function CustomRootLayoutComponent(props: {pageHeadProps?: PageHeadProps, children: any, customBody?: boolean,
    listFooterLinks?: FooterLinkItem[],
    isIndexable?: boolean, isLoading?: boolean, customNavbarItems?: any,
    leftChilds?: any, rigthChilds?: any, layoutSideContentData?: LayoutSideContentAggregatorModel, customBackgroundColor?: string, backPath?: string}) {
    const router = useRouter();
    const [isWebToApp, setIsWebToApp] = useState(router.query.isWebToApp === "true");


    useEffect(() => {
        // Access the query parameters from the URL
        const { isWebToApp } = router.query;

        // Check if the "isWebToApp" query parameter is set to "true"
        setIsWebToApp(isWebToApp === 'true');
    }, [router.query.isWebToApp]);

    function renderRigthSideContent() {
        if (props.layoutSideContentData) {
            let rigthSideContent: any[] = [];
            // leftSideContent.push(RootSideCategoryMenuComponent());
            rigthSideContent.push((
                <div className='flex w-full px-5 justify-center my-5'>
                    <Link href={SharedRoutesUtils.getSubmitJob()} className='btn btn-outline btn-secondary gap-'>{<AiFillEdit />} POST A JOB</Link>
                </div>
            ));

            if (props.rigthChilds) {
                rigthSideContent = rigthSideContent.concat(props.rigthChilds);
            }

            // Add Layout Side Content Data
            if (props.layoutSideContentData.recentJobs) {
                rigthSideContent.push((<LandingRecentJobsComponent listRecentJobs={props.layoutSideContentData.recentJobs} />))
            }
            if (props.layoutSideContentData.recentPosts) {
                rigthSideContent.push((<LandingRecentPostsComponent listRecentPosts={props.layoutSideContentData.recentPosts} />))
            }

            return (
                <div className='flex flex-col gap-5'>
                    {rigthSideContent}
                </div>
            )
        } else {
            return null;
        }

    }

    // Override Leftside content
    let customProps: any = {
        ...props,
        rigthChilds: renderRigthSideContent(),
    }

    function renderBottomNavigation() {
        const listNavTabs: CustomNavbarLinkData[] = UIHelper.getNavbarListLinks(router.asPath);

        function renderTabs() {
            return listNavTabs.map((navbarItem) => {
                return (
                    <Link href={navbarItem.link} key={navbarItem.link}
                          className={"hover:bg-red-100 " + (navbarItem.isActive ? "active bg-red-500 text-white" : "text-red-400")}>
                        <div className='flex flex-col items-center justify-center'>
                            {navbarItem.icon}
                            {navbarItem.label}
                        </div>
                    </Link>
                );
            });
        }

        return (
            <div className="flex lg:hidden btm-nav btm-nav-sm text-slate-400 bg-red-50 drop-shadow">
                {renderTabs()}
            </div>
        )
    }


    customProps.backgroundColor = props.customBackgroundColor ? props.customBackgroundColor : "bg-white";

    return (
        <>
            {RootLayoutComponent(customProps)}
            {renderBottomNavigation()}
        </>
    );
}