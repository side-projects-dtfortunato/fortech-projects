import {useState} from "react";
import ListHeadhlineSeparatorComponent
    from "../../../react-shared-module/ui-components/shared/ListHeadhlineSeparator.component";
import JobPostListItemComponent from "../../remote-jobs/JobPostListItem.component";
import Link from "next/link";
import {JobListModel} from "../../../react-shared-module/base-projects/jobs-discover/jobs-discover-data.model";
import {getRemoteJobsPageUrl} from "../../../../pages/jobs";

export default function LandingRecentJobsComponent(props: {listRecentJobs?: JobListModel[]}) {
    const [listRecentJobs, setListRecentJobs] = useState< JobListModel[] | undefined>(props.listRecentJobs);

    function renderListJobs() {
        return listRecentJobs?.map((jobItem) => {
            return (<JobPostListItemComponent key={jobItem.uid!} jobPostItem={jobItem} displayMode={"SHORT"}/>);
        });
    }

    if (listRecentJobs && listRecentJobs.length > 0) {
        return (
            <div className='flex flex-col gap-2'>
                <ListHeadhlineSeparatorComponent text={"Recent Jobs"} color={"#DB6179"} />
                {renderListJobs()}

                <div className='divider'>
                    <Link href={getRemoteJobsPageUrl()} title={"Show More"} className='btn btn-ghost btn-xs'>Show more jobs</Link>
                </div>
            </div>
        )
    } else {
        return (<></>);
    }

}