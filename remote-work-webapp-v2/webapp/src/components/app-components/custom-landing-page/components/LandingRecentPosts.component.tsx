import {useEffect, useState} from "react";
import LocalStorageDataManager from "../../../react-shared-module/logic/managers/LocalStorageData.manager";
import {RemoteWorkNicheSitesUtils} from "../../../../custom-project/api/custom-data-model/remote-work-niche-sites";
import ListHeadhlineSeparatorComponent
    from "../../../react-shared-module/ui-components/shared/ListHeadhlineSeparator.component";
import Link from "next/link";
import {BasePostDataModel} from "../../../react-shared-module/logic/shared-data/datamodel/base-post-data.model";
import {SharedRoutesUtils} from "../../../react-shared-module/utils/shared-routes.utils";

export default function LandingRecentPostsComponent(props: {listRecentPosts?: BasePostDataModel[]}) {
    const [listRecentPosts, setListRecentPosts] = useState< BasePostDataModel[] | undefined>(props.listRecentPosts);

    function renderListPosts() {
        return listRecentPosts?.map((post) => {
            return (
                <li className='flex flex-col bordered"' key={post.uid}>
                    <Link className='flex flex-col items-start' href={SharedRoutesUtils.getPostDetailsPageURL(post.uid!)}>
                        <span className='custom text-start'>{post.title}</span>
                        {post.listUsersInfos ? (<span className='subtitle'>{post.listUsersInfos[post.userCreatorId]?.name}</span>) : (<></>)}
                    </Link>
                </li>
            )
        });
    }

    if (listRecentPosts && listRecentPosts.length > 0) {
        // Colors Pallet for headlines colors: https://www.schemecolor.com/reading-headlines.php
        return (
            <div className='flex flex-col gap-2 w-full items-center'>
                <ListHeadhlineSeparatorComponent text={"Recent Posts"} color={"#080D4A"} />
                <ul className="list-item-bg drop-shadow menu bg-base-100 w-72">
                    {renderListPosts()}
                </ul>

                <div className='divider'>
                    <Link href={"/"} title={"Show More"} className='btn btn-ghost btn-xs'>Show more posts</Link>
                </div>
            </div>
        )
    } else {
        return (<></>);
    }

}