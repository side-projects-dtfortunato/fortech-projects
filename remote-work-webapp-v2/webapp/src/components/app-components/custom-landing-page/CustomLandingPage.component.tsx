import {BasePostDataModel} from "../../react-shared-module/logic/shared-data/datamodel/base-post-data.model";
import LandingTagPopularSidelistComponent
    from "../../react-shared-module/ui-components/landing-page-content/shared/LandingTagPopularSidelist.component";
import {
    UserShortProfileModel
} from "../../react-shared-module/logic/shared-data/datamodel/shared-public-userprofile.model";
import CustomRootLayoutComponent from "../root/CustomRootLayout.component";
import RootConfigs from "../../../configs";
import {SharedUtils} from "../../react-shared-module/utils/shared-utils";
import {getLanguageLabel} from "../../react-shared-module/logic/language/language.helper";
import {
    SharedLandingPageAggregatorsModel
} from "../../react-shared-module/logic/shared-data/datamodel/shared-landing-page-aggregators.model";
import FeedQuickPostFormComponent
    from "../../react-shared-module/ui-components/post-form/feed-post-form/FeedQuickPostFormComponent";
import ListPostsComponent from "../../react-shared-module/ui-components/posts/shared/ListPosts.component";
import NotFoundContentComponent from "../../react-shared-module/ui-components/shared/NotFoundContent.component";
import Link from "next/link";
import {getPostRemoteJobPageUrl} from "../../../pages/post-remote-job";
import {AiOutlineForm} from "react-icons/ai";
import {getCustomLanguageLabel} from "../../ui-helper/language/custom-language.helper";
import {PiArticleMediumFill} from "react-icons/pi";
import {getPublishPostPageUrl} from "../../../pages/publish-post";
import {PostTypes} from "../../react-shared-module/utils/shared-ui-helper";
import LandingRecentJobsComponent from "./components/LandingRecentJobs.component";
import {
    LayoutSideContentAggregatorModel
} from "../../../custom-project/api/custom-data-model/layout-side-content-aggregator.model";
import {useCentralUserDataState} from "../../react-shared-module/logic/global-hooks/root-global-state";
import EditProfileFormComponent from "../../react-shared-module/ui-components/edit-profile/EditProfileForm.component";

export default function CustomLandingPageComponent(props: {landingDataAgg: SharedLandingPageAggregatorsModel, layoutSideContentData?: LayoutSideContentAggregatorModel}) {
    const [centralUserData, setCentralUserData] = useCentralUserDataState();

    function renderListPosts() {
        if (props.landingDataAgg) {
            let listPosts: BasePostDataModel[] = props.landingDataAgg.recentPosts.filter((postItem) => {
                return !postItem.isFeatured;
            });

            return (
                <ListPostsComponent listPosts={listPosts} />
            )

        } else {
            return (<NotFoundContentComponent />)
        }
    }

    function renderOtherFormOptions() {
        return (
            <div className='flex flex-col gap-2 items-center'>
                <div className={'divider text-xs font-bold text-slate-400'}>OR</div>
                <div className='flex flew-wrap gap-2 items-center'>
                    <Link href={getPostRemoteJobPageUrl()} className='flex flex-row gap-2 btn btn-outline btn-xs'><AiOutlineForm /> {getCustomLanguageLabel("CUSTOM_POST_A_JOB_BTN")}</Link>
                    <Link href={getPublishPostPageUrl(PostTypes.ARTICLE_BLOCKS)} className='flex flex-row gap-2 btn btn-outline btn-xs'><PiArticleMediumFill /> {getCustomLanguageLabel("CUSTOM_WRITE_AN_ARTICLE")}</Link>
                </div>
            </div>
        )
    }

    function renderFeaturedPosts() {
        if (props.landingDataAgg) {
            let listPosts: BasePostDataModel[] = props.landingDataAgg.recentPosts.filter((postItem) => {
                return postItem.isFeatured;
            });

            if (listPosts && listPosts.length > 0) {
                return (
                    <ListPostsComponent listPosts={listPosts} />
                )
            } else {
                return (<></>)
            }


        } else {
            return (<></>)
        }
    }

    return (
        <CustomRootLayoutComponent
            layoutSideContentData={props.layoutSideContentData}
            pageHeadProps={{
                title: RootConfigs.SITE_TITLE,
                description: RootConfigs.SITE_DESCRIPTION,
                imgUrl: RootConfigs.BASE_URL + "/images/logo.png",
                jsonLdMarkup: SharedUtils.getSchemaMarkupSite(),
            }}>
            <div className='flex flex-col gap-3'>
                <FeedQuickPostFormComponent bottomComponents={renderOtherFormOptions()}/>
                {renderFeaturedPosts()}
                <div className='divider text-slate-400'>{getLanguageLabel("LANDING_PAGE_DIVIDER_PUBLICATIONS", "Publications")}</div>
                {renderListPosts()}
            </div>
        </CustomRootLayoutComponent>
    )

}


// Old Code to force the update of the username:
/*if (centralUserData && (!centralUserData.username || centralUserData.username.length === 0)) {
        return (
            <CustomRootLayoutComponent customBody={true} pageHeadProps={{
                title: RootConfigs.SITE_TITLE,
                description: RootConfigs.SITE_DESCRIPTION,
                imgUrl: RootConfigs.BASE_URL + "/images/logo.png",
                jsonLdMarkup: SharedUtils.getSchemaMarkupSite(),
            }}>
                <EditProfileFormComponent isNewUser={true} headerTitle={"Configure Your Account"} headerSubtitle={"Configure your account to publish new posts to the community"} />
            </CustomRootLayoutComponent>
        )
    } else {
        return (
            <CustomRootLayoutComponent
                layoutSideContentData={props.layoutSideContentData}
                pageHeadProps={{
                    title: RootConfigs.SITE_TITLE,
                    description: RootConfigs.SITE_DESCRIPTION,
                    imgUrl: RootConfigs.BASE_URL + "/images/logo.png",
                    jsonLdMarkup: SharedUtils.getSchemaMarkupSite(),
                }}>
                <div className='flex flex-col gap-3'>
                    <FeedQuickPostFormComponent bottomComponents={renderOtherFormOptions()}/>
                    {renderFeaturedPosts()}
                    <div className='divider text-slate-400'>{getLanguageLabel("LANDING_PAGE_DIVIDER_PUBLICATIONS", "Publications")}</div>
                    {renderListPosts()}
                </div>
            </CustomRootLayoutComponent>
        )
    }*/