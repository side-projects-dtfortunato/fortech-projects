import {JobPostListInfoModel} from "../../../custom-project/api/custom-data-model/job-post-details.model";
import JobPostListItemComponent from "./JobPostListItem.component";
import SearchFieldComponent, {
    SearchTagFilterData
} from "../../react-shared-module/ui-components/shared/SearchField.component";
import CommonUtils from "../../react-shared-module/logic/commonutils";
import {useState} from "react";
import ExpandCollapseButtonComponent
    from "../../react-shared-module/ui-components/shared/ExpandCollapseButton.component";

const MAX_INITIAL_JOBS = 15;

/**
 * @deprecated
 * @param props
 * @constructor
 */
export default function RemoteJobsListPage(props: { listJobs: JobPostListInfoModel[] }) {
    const listFilters = exportListTagsSortedPopularity(props.listJobs, 2);
    const [filteredListJobs, setFilteredListJobs] = useState(props.listJobs);
    const [isCollapsedList, setIsCollapsedList] = useState(true);

    function renderListJobs() {
        let listItems: any[] = [];

        /*if (filteredListJobs.length > 0) {
            let listJobs: JobPostListInfoModel[] = filteredListJobs.length > MAX_INITIAL_JOBS && isCollapsedList ? filteredListJobs.slice(0, MAX_INITIAL_JOBS) : filteredListJobs;

            listItems = listJobs.map((jobItem) => (
                <JobPostListItemComponent key={jobItem.uid!} jobPostItem={jobItem}/>));
        }*/

        return listItems;
    }

    function onSearch(query: string, selectedTags: string[]) {
        setFilteredListJobs(filterListJobs(query, selectedTags, props.listJobs));
    }

    function renderExpandCollapseBtn() {
        if (filteredListJobs.length > MAX_INITIAL_JOBS) {
            return (
                <ExpandCollapseButtonComponent collapsedLabel={"Show more jobs"}
                                               expandedLabel={"Show less"}
                                               isCollapsed={isCollapsedList} onClick={() => setIsCollapsedList(!isCollapsedList)} />
            )
        } else {
            return (<></>)
        }
    }

    return (
        <div className='flex flex-col gap-2'>
            <SearchFieldComponent onSearch={onSearch} filterTags={listFilters} hint={"Search for remote jobs..."}/>
            {renderListJobs()}
            {renderExpandCollapseBtn()}
        </div>
    )
}

function filterListJobs(query: string, selectedTags: string[], listJobs: JobPostListInfoModel[]) {
    return listJobs.filter((jobItem) => {
        if (query.length > 2) {
            if (!jobItem.positionTitle.toLowerCase().includes(query.toLowerCase())
                && !jobItem.companyDetails.companyName.toLowerCase().includes(query.toLowerCase())) {
                return false;
            }
        }

        if (selectedTags.length > 0) {
            let jobTags: string[] = exportTagsFromJob(jobItem);
            let hasTags: boolean = CommonUtils.isArrayIncludesOtherArray(jobTags, selectedTags);

            if (!hasTags) {
                return false;
            }
        }
        return true;
    });
}

function exportTagsFromJob(jobItem: JobPostListInfoModel) {
    let jobTags: string[] = [];

    if (!jobItem.jobSkills) {
        return jobTags;
    }

    if (jobItem.jobSkills && jobItem.jobSkills.jobCategory) {
        jobTags.push(jobItem.jobSkills.jobCategory!.toUpperCase());
    }

    jobItem.jobSkills.mainSkills.forEach((skill) => {
        jobTags.push(skill.toUpperCase());
    });
    jobItem.jobSkills.secondarySkills.forEach((skill) => {
        jobTags.push(skill.toUpperCase());
    });
    return jobTags;
}

function exportListTagsSortedPopularity(listJobs: JobPostListInfoModel[], minPopularity: number): SearchTagFilterData[] {
    let mapTags: {
        [tag: string]: {
            tag: string,
            counter: number,
        }
    } = {};

    function addTag(tag: string) {
        let tagUpperCase = tag.toUpperCase();
        if (mapTags[tagUpperCase]) {
            mapTags[tagUpperCase].counter++;
        } else {
            mapTags[tagUpperCase] = {
                tag: tagUpperCase,
                counter: 1,
            };
        }
    }

    listJobs.forEach((jobItem) => {
        let jobTags: string[] = exportTagsFromJob(jobItem);

        if (jobTags.length > 0) {
            jobTags.forEach((jobTag) => {
                addTag(jobTag);
            })
        }
    });

    return Object.values(mapTags)
        .filter((tag) => tag.counter > minPopularity)
        .sort((tag1, tag2) => tag2.counter - tag1.counter).map((tag) => {
        return {
            tagValue: tag.tag,
            tagLabel: tag.tag.replace("_", " ") + " (" + tag.counter + ")",
        };
    });
}