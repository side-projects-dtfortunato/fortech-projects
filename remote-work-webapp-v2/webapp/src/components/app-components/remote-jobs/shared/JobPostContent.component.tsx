import {JobPostDetailsModel} from "../../../../custom-project/api/custom-data-model/job-post-details.model";
import Link from "next/link";
import {renderCompanyLogo, renderJobTags} from "../JobPostListItem.component";
import {CustomBackendApi} from "../../../../custom-project/api/custombackend.api";
import GoogleAdsComponent, {AdsSlots} from "../../../react-shared-module/ui-components/ads/GoogleAds.component";
import BaseTextComponent from "../../../react-shared-module/ui-components/shared/BaseText.component";

export default function JobPostContentComponent(props: { jobPostDetails: JobPostDetailsModel }) {

    return (<>
        {renderJobPostDescriptionContainer(props.jobPostDetails)}
    </>);
}

function renderJobPostDescriptionContainer(jobDetails: JobPostDetailsModel) {

    /**
     * Scheme:
     * - Published x days agora
     * - Number of views and applications
     * - Position title
     * - Skills
     * - Description
     */
    return (
        <div className='flex flex-col w-full justify-center gap-2'>
            <div className='flex my-2 justify-center'>
                {renderSideDetailsContainer(jobDetails)}
            </div>
            <GoogleAdsComponent slot={AdsSlots.HORIZONTAL_ADS} />

            <article className="prose">
                <h1 className='mt-3'>{jobDetails.positionTitle}</h1>
            </article>

            <div className='flex flex-wrap mt-1'>
                {/*renderJobTags(jobDetails)*/}
            </div>

            <article className="my-4 flex prose">
                {
                    jobDetails.descriptionContentType === "MARKDOWN" ?
                        <BaseTextComponent text={jobDetails.positionDescription} />
                        : <div dangerouslySetInnerHTML={{__html: jobDetails.positionDescription}} />
                }

            </article>

            <div className='flex justify-center items-center'>
                {renderApplyButton(jobDetails)}
            </div>
        </div>
    )
}

function renderViewsAndApplications(jobDetails: JobPostDetailsModel) {
    let numberViews: number = 0;
    let numberApplications: number = 0;
    if (jobDetails.stats?.views) numberViews = Object.values(jobDetails.stats?.views).length + 1;
    if (jobDetails.stats?.appliedBtn) numberApplications = Object.values(jobDetails.stats?.appliedBtn).length;

    if (numberViews > 10 || numberApplications > 5) {
        return (
            <span className='text-xs text-slate-700 mt-2'
                  style={{fontSize: 14}}>👀 Views: {numberViews} - 🚀 Applications: {numberApplications}</span>
        )
    } else {
        return <></>
    }

}

function renderSideDetailsContainer(jobDetails: JobPostDetailsModel) {
    let listJobLocation: string = jobDetails.customZoneRestriction ? jobDetails.customZoneRestriction : "WORLDWIDE";

    return (
        <div className='flex flex-col justify-start items-center bg-neutral-50 drop-shadow px-5 py-4 w-full max-w-lg'>
            <div className='flex flex-row sm:flex-col items-center'>
                {/*renderCompanyLogo(jobDetails)*/}
                <div className='ml-2 sm:ml-0 flex flex-col items-start sm:items-center'>
                    <h4 className='text-center text-lg'>{jobDetails.companyDetails.companyName}</h4>
                    <div className='flex flex-row items-center mt-0 sm:mt-2 w-75'>
                        <img style={{height: 15, color: "#D2D2D2", fill: "#D2D2D2"}} src='/images/ic_location.png'/>
                        <span className='ml-1 font-xs text-slate-500'
                              style={{fontSize: 13}}>Location: {listJobLocation}</span>
                    </div>
                </div>
            </div>
            <div className='flex flex-col justify-center items-center mt-2 w-full'>
                {renderViewsAndApplications(jobDetails)}
                {renderApplyButton(jobDetails)}
            </div>
        </div>
    )
}

function renderApplyButton(jobDetails: JobPostDetailsModel) {
    return (
        <div className='flex flex-col justify-center items-center my-2 gap-2 w-full'>
            <Link href={jobDetails.applicationMethod.url} target='_blank'>
                <button className='btn text-white'
                        onClick={() => (CustomBackendApi.requestJobPostedVisited(jobDetails.uid as string, "applied"))}>🚀
                    Apply for this position
                </button>
            </Link>
            {/*<button className='btn btn-ghost flex flex-row gap-2 w-full' onClick={() => {
                CustomBackendApi.publishRemoteJobFeed(jobDetails.uid!);
            }
            }>
                <PiShareFat />
                Publish on Feed
            </button>*/}
            <Link href={"https://buy.stripe.com/fZe8yg6V78KKaUoaEF"} target='_blank' className='text-xs text-center'>Promote
                this job offer through our network</Link>
        </div>
    )
}