
import Image from "next/image";
import CommonUtils from "../../react-shared-module/logic/commonutils";
import {JobPostListInfoModel} from "../../../custom-project/api/custom-data-model/job-post-details.model";
import {CustomRoutesUtils} from "../../../custom-project/custom-routes.utils";
import JobSkillTagComponent from "./JobSkillTag.component";
import TagItemComponent from "../../react-shared-module/ui-components/shared/TagItem.component";
import Link from "next/link";
import {JobListModel} from "../../react-shared-module/base-projects/jobs-discover/jobs-discover-data.model";

export default function JobPostListItemComponent(props: {jobPostItem: JobListModel, isRecommended?: boolean, displayMode?: "FULL" | "SHORT"}) {
    let listJobLocation: string = props.jobPostItem.remoteRegion ? props.jobPostItem.remoteRegion : "WORLDWIDE";

    return (
        <Link href={CustomRoutesUtils.getRemoteJobDetailsUrl(props.jobPostItem.uid as string)} style={{textDecoration: "none"}} className='flex flex-col w-full p-3 list-item-bg drop-shadow hover:bg-slate-100' key={props.jobPostItem.uid}>
            <div className='flex mb-2 align-items-center'>
                {props.jobPostItem.createdAt ? (<span className="font-xs text-slate-500" style={{fontSize: 12}}>{"Published " + CommonUtils.getTimeAgo(props.jobPostItem.createdAt as number)}</span>) : (<></>) }
                <div className='flex grow justify-content-end'>
                    {props.isRecommended ? (
                        <div className="badge">Recommended for you</div>
                    ) : (<></>)}
                </div>
            </div>
            <div className='flex flex-row w-full align-items-start justify-content-start mb-2 gap-2'>
                {props.displayMode !== "SHORT" ? renderCompanyLogo(props.jobPostItem) : <></>}
                <div className='flex flex-col w-full whitespace-pre-wrap'>
                    <span className='font-xs mb-1 text-slate-500' style={{fontSize: 14}}>{props.jobPostItem.company}</span>
                    <span className='font-base font-bold mb-2  text-slate-700'>{props.jobPostItem.title}</span>
                    <div className='flex flex-row align-items-center'>
                        <Image height={15} width={15} style={{height: 15, color: "#D2D2D2", fill: "#D2D2D2"}} src='/images/ic_location.png' alt='icon location' />
                        <span className='ml-1 font-xs text-slate-500' style={{fontSize: 13}}>{listJobLocation}</span>
                    </div>
                </div>
            </div>
            <div className='flex flex-wrap mt-2'>
                {props.displayMode !== "SHORT" ? renderJobTags(props.jobPostItem) : <></>}
            </div>
        </Link>
    )
}

export function renderJobTags(jobPostItem: JobListModel) {
    let listTagsComponents: any[] = [];

    listTagsComponents.push(<TagItemComponent label={jobPostItem.category!} key={"category"} filled={true} />)
    jobPostItem.skills?.forEach((skill) => {

        listTagsComponents.push(<TagItemComponent label={skill} key={skill} filled={false} />);
    });
    return listTagsComponents;
}

export function renderCompanyLogo(jobPostItem: JobListModel) {

    let renderImage = (
        <div className='p-2'>
            <h2 className='text-black font-bold'>{jobPostItem.companyLogo}</h2>
        </div>
    );


    if (jobPostItem.companyLogo) {
        renderImage = <Image className='rounded' src={jobPostItem.companyLogo} style={{objectFit: "cover"}} onError={({ currentTarget }) => {
            currentTarget.onerror = null; // prevents looping
            currentTarget.src = "https://via.placeholder.com/128x128/FFF/000.png?text=" + jobPostItem.company.substr(0, 1);
        }} height={75} width={75} alt={jobPostItem.company}/>;
    }

    return (
        <div className='flex justify-content-center align-items-center rounded bg-white drop-shadow' style={{height: 75, width: 75}}>
            {renderImage}
        </div>
    )
}