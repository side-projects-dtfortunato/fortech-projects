export default function JobSkillTagComponent(props: {label: string}) {

    let tagLabel: string = props.label.replace("_", " ");
    return (
        <div className='flex px-1 py-1 border border-gray-900 mr-2 mb-2 rounded'>
            <span className='font-xs text-slate-500' style={{fontSize: 11}}>{tagLabel}</span>
        </div>

    )
}
