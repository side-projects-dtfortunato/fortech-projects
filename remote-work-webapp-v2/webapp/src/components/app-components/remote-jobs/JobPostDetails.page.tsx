import {useRouter} from "next/router";
import Link from "next/link";
import {JobPostDetailsModel} from "../../../custom-project/api/custom-data-model/job-post-details.model";
import CommonUtils from "../../react-shared-module/logic/commonutils";
import JobPostContentComponent from "./shared/JobPostContent.component";

let router;

export default function JobPostDetailsPageContent(props: { jobDetails: JobPostDetailsModel}) {
    router = useRouter();


    return (
        <div className='w-full lg:w-75'>
            <div className='flex flex-col w-full justify-center gap-2'>
                <Link href='/' style={{color: "#DD3444"}}>{"< Search more jobs"}</Link>
                <span className="text-xs text-slate-500 mt-4"
                      style={{fontSize: 13}}>{"Published " + CommonUtils.getTimeAgo(props.jobDetails.publishedAt as number)}</span>

                <JobPostContentComponent jobPostDetails={props.jobDetails}/>
            </div>
        </div>
    )
}
