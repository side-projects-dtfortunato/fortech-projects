import {
    BasePostDataModel,
    PostDataUtils
} from "../../react-shared-module/logic/shared-data/datamodel/base-post-data.model";
import {
    JobPostDetailsModel,
    JobPostListInfoModel
} from "../../../custom-project/api/custom-data-model/job-post-details.model";
import Link from "next/link";
import UserHeaderInfosComponent from "../../react-shared-module/ui-components/shared/UserHeaderInfos.component";
import JobPostListItemComponent from "./JobPostListItem.component";
import {CustomRoutesUtils} from "../../../custom-project/custom-routes.utils";

/**
 * @deprecated
 * @param props
 * @constructor
 */
export default function JobPublicationListItemComponent(props: {basePostItem: BasePostDataModel<JobPostListInfoModel, JobPostDetailsModel>}) {
    /*return (
        <Link className='list-item-bg drop-shadow p-4 flex flex-col gap-3 hover:bg-slate-100' href={CustomRoutesUtils.getRemoteJobDetailsUrl(props.basePostItem.uid!)}>
            {<UserHeaderInfosComponent userId={props.basePostItem.userCreatorId} isCreator={false} userInfos={PostDataUtils.getUserInfos(props.basePostItem, props.basePostItem.userCreatorId)} />}
            <span className='text-slate-400 text-xs'>Shared a remote job</span>
            <JobPostListItemComponent jobPostItem={props.basePostItem.shortenPostData as JobPostListInfoModel} />
        </Link>
    )*/
    return <div>DEPRECATED</div>
}