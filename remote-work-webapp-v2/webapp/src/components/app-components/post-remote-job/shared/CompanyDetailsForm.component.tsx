import {JobPostDetailsModel} from "../../../../custom-project/api/custom-data-model/job-post-details.model";
import InputTextComponent from "../../../react-shared-module/ui-components/form/InputText.component";


export default function CompanyDetailsFormComponent(props: { formJobPostData: JobPostDetailsModel, setFormJobPostData: (data: JobPostDetailsModel) => void }) {

    function onCompanyNameChanged(text: string) {
        props.setFormJobPostData({
            ...props.formJobPostData,
            companyDetails: {
                ...props.formJobPostData.companyDetails,
                companyName: text,
            },
        });
    }

    function onCompanyWebsiteChanged(url: string) {
        props.setFormJobPostData({
            ...props.formJobPostData,
            companyDetails: {
                ...props.formJobPostData.companyDetails,
                website: url,
            },
        });
    }

    function onCompanyEmailContact(emailContact: string) {
        props.setFormJobPostData({
            ...props.formJobPostData,
            companyDetails: {
                ...props.formJobPostData.companyDetails,
                companyEmail: emailContact,
            },
        });
    }

    function onCompanyLogoImageUrl(urlLogo: string) {
        props.setFormJobPostData({
            ...props.formJobPostData,
            companyDetails: {
                ...props.formJobPostData.companyDetails,
                companyLogo: {
                    storagePath: "",
                    fileUrl: urlLogo,
                },
            },
        });
    }

    return (
        <div className='flex flex-col gap-2'>
            <h4>Let us know a bit more about the hiring company</h4>
            <InputTextComponent topLabel={"* Company Name"} hint={"What is the name of the company that is hiring"}
                                onChanged={onCompanyNameChanged} defaultValue={props.formJobPostData.companyDetails.companyName} value={props.formJobPostData.companyDetails.companyName}/>
            <InputTextComponent topLabel={"* Company Website"}
                                hint={"Share the link of the company (can be the careers link)"}
                                onChanged={onCompanyWebsiteChanged} defaultValue={props.formJobPostData.companyDetails.website} value={props.formJobPostData.companyDetails.website}/>
            <InputTextComponent topLabel={"* Company logo URL"} hint={"Please, copy a link of the company logo"}
                                onChanged={onCompanyLogoImageUrl} defaultValue={props.formJobPostData.companyDetails.companyLogo.fileUrl} value={props.formJobPostData.companyDetails.companyLogo.fileUrl}/>
            <InputTextComponent topLabel={"Email Contact"} hint={"What is your email?"}
                                onChanged={onCompanyEmailContact} defaultValue={props.formJobPostData.companyDetails.companyEmail} value={props.formJobPostData.companyDetails.companyEmail}/>
        </div>
    )
}