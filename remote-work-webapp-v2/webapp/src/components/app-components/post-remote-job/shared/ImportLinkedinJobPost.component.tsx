import {JobPostDetailsModel} from "../../../../custom-project/api/custom-data-model/job-post-details.model";
import {useState} from "react";
import {
    RootGlobalStateActions,
    useAlertMessageGlobalState
} from "../../../react-shared-module/logic/global-hooks/root-global-state";
import {BsLinkedin} from "react-icons/bs";
import InputTextComponent from "../../../react-shared-module/ui-components/form/InputText.component";
import {CustomBackendApi} from "../../../../custom-project/api/custombackend.api";
import {response} from "express";
import RootConfigs from "../../../../configs";

export default function ImportLinkedinJobPostComponent(props: {onJobDataGenerated: (jobPostDetails: JobPostDetailsModel) => void}) {
    const [url, setUrl] = useState("");
    const [alertMessage, setAlertMessage] = useAlertMessageGlobalState();
    const [isLoading, setIsLoading] = useState(false);

    function onImportClicked() {
        setIsLoading(true);

        CustomBackendApi.importLinkedinJobPostDetails({url: url, defaultJobCategory: RootConfigs.META_CONFIGS.defaultJobCategory}).then((response) => {
            if (response.isSuccess && response.responseData) {
                props.onJobDataGenerated(response.responseData as JobPostDetailsModel);
                RootGlobalStateActions.displayAlertMessage({
                    alertType: "SUCCESS",
                    message: "Job details imported successfuly",
                    setAlertMessageState: setAlertMessage,
                });
            } else {
                RootGlobalStateActions.displayAlertMessage({
                    alertType: "ERROR",
                    message: "Something failed while importing the job. Please, try again...",
                    setAlertMessageState: setAlertMessage,
                });
            }

            setIsLoading(false);
        }).catch((err) => {
            RootGlobalStateActions.displayAlertMessage({
                alertType: "ERROR",
                message: err,
                setAlertMessageState: setAlertMessage,
            });
            setIsLoading(false);
        });
    }

    return (
        <div className='flex flex-col items-start gap-3'>
            <InputTextComponent topLabel={"Import directly from Linkedin"} hint={"Paste here the url of the job post to import"} onChanged={(text) => {setUrl(text)}} />
            <button className={'flex flex-row gap-2 btn btn-outline btn-sm ' + (isLoading ? "loading" : "")} disabled={isLoading } onClick={onImportClicked}><BsLinkedin /> Import from LinkedIn</button>
        </div>
    )
}

function validateLinkedInJobViewURL(url: string): boolean {
    const regex = /https:\/\/www.linkedin.com\/jobs\/view\/([^/]+)\/([^/]+)/;
    return regex.test(url);
}