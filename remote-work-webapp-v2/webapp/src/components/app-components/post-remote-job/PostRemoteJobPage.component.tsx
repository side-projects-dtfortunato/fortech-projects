import {useEffect, useState} from "react";
import {
    JobApplicationType,
    JobExperienceLevel,
    JobPostDetailsModel,
    JobPostSource,
    JobPostUtils,
    RemoteZoneRestriction
} from "../../../custom-project/api/custom-data-model/job-post-details.model";
import {CustomBackendApi} from "../../../custom-project/api/custombackend.api";
import {ApiResponse} from "../../react-shared-module/logic/shared-data/datamodel/base.model";
import {useRouter} from "next/router";
import InputTextComponent from "../../react-shared-module/ui-components/form/InputText.component";
import DropdownFormComponent from "../../react-shared-module/ui-components/form/DropdownForm.component";
import CompanyDetailsFormComponent from "./shared/CompanyDetailsForm.component";
import JobSkillTagComponent from "../remote-jobs/JobSkillTag.component";
import RichTextEditorComponent from "../../react-shared-module/ui-components/form/RichTextEditor.component";
import InputTagsFieldComponent from "../../react-shared-module/ui-components/form/InputTagsField.component";
import ImportLinkedinJobPostComponent from "./shared/ImportLinkedinJobPost.component";
import {BsLinkedin} from "react-icons/bs";
import JobPostContentComponent from "../remote-jobs/shared/JobPostContent.component";
import {BiError} from "react-icons/bi";

const InitialJobPostData: JobPostDetailsModel = {
    remoteRegionsAllowance: {
        [RemoteZoneRestriction.WORLDWIDE]: true,
    },
    jobSkills: {
        secondarySkills: [],
        mainSkills: [],
    },
    customZoneRestriction: "",
    positionTitle: "",
    companyDetails: {
        companyName: "",
        website: "",
        companyLocation: {},
        companyEmail: "",
        companyLogo: {
            fileUrl: "https://via.placeholder.com/128x128.png?text=Company",
            storagePath: "",
        },
    },
    sourceType: JobPostSource.ORIGINAL,
    sourceUrl: "Remote-Work.app",
    positionDescription: "",
    isFeature: false,
    applicationMethod: {
        applicationType: JobApplicationType.URL,
        url: "",
    },
};

export default function PostRemoteJobPageComponent() {
    const router = useRouter();
    const [formJobPostData, setFormJobPostData] = useState<JobPostDetailsModel>(InitialJobPostData);
    //const [formJobDescription, setFormJobDescription] = useState<string>(formJobPostData.positionDescription);
    const [errorMessage, setErrorMessage] = useState<string | undefined>();
    const [isAPILoading, setIsAPILoading] = useState<boolean>(false);

    let isFormDataValid: boolean = JobPostUtils.isJobPostDataValid(formJobPostData).isValid;

    /*useEffect(() => {
        setFormJobDescription(formJobPostData.positionDescription);
    }, [formJobPostData]);*/

    function onJobApplicationLink(url: string) {
        setErrorMessage(undefined);
        setFormJobPostData({
            ...formJobPostData,
            applicationMethod: {
                url: url,
                applicationType: JobApplicationType.URL,
            },
        });
    }

    function onJobTitleChanged(text: string) {
        setErrorMessage(undefined);
        setFormJobPostData({
            ...formJobPostData,
            positionTitle: text,
        });
    }

    function onJobDescriptionChanged(text: string) {
        setErrorMessage(undefined);
        if (formJobPostData.positionDescription !== text) {
            formJobPostData.positionDescription = text;
            setFormJobPostData({
                ...formJobPostData,
                positionDescription: text,
            });
        }

    }

    function onJobCategorySelection(jobCategory: string) {
        setErrorMessage(undefined);
        setFormJobPostData({
            ...formJobPostData,
            jobSkills: {
                ...formJobPostData.jobSkills,
                jobCategory: jobCategory,
            },
        });
    }

    function onJobExperienceLevel(experienceLevel: string) {
        setErrorMessage(undefined);
        setFormJobPostData({
            ...formJobPostData,
            experienceLevel: experienceLevel as JobExperienceLevel,
        });
    }

    function onJobSkillsChanged(text: string) {
        setErrorMessage(undefined);
        let splitSkills: string[] = text.split(";");
        splitSkills = splitSkills.filter((skillItem) => skillItem !== "" && skillItem !== " ");

        setFormJobPostData({
            ...formJobPostData,
            jobSkills: {
                ...formJobPostData.jobSkills,
                mainSkills: splitSkills
            },
        });
    }

    function onJobSkillsTagsChanged(tags: string[]) {
        setErrorMessage(undefined);
        let splitSkills: string[] = tags.map((tag) => tag.toUpperCase());
        splitSkills = splitSkills.filter((skillItem) => skillItem !== "" && skillItem !== " ");

        setFormJobPostData({
            ...formJobPostData,
            jobSkills: {
                ...formJobPostData.jobSkills,
                mainSkills: splitSkills
            },
        });
    }

    function onRemoteRegionsAllowance(data: { [region: string]: boolean, }) {
        setErrorMessage(undefined);

        setFormJobPostData(
            JobPostUtils.generateCustomZoneRestrictionLabel({
                ...formJobPostData,
                remoteRegionsAllowance: data,
            })
        );
    }

    async function onPublishRemoteJobClick() {
        let isFormValid: { errorMessage?: string, isValid: boolean } = JobPostUtils.isJobPostDataValid(formJobPostData);
        // Validate if that is valid
        if (!isFormValid.isValid) {
            // Display error message
            setErrorMessage(isFormValid.errorMessage);
        } else {
            setIsAPILoading(true);
            // API Call & redirect to success page
            let apiResponse: ApiResponse<any> = await CustomBackendApi.postRemoteJob({jobPostDetails: formJobPostData});

            if (!apiResponse.isSuccess) {
                setIsAPILoading(false);
                if (apiResponse.message) {
                    setErrorMessage(apiResponse.message);
                } else {
                    setErrorMessage("Some error occurred on trying to publish this job offer. Please, verify your form and try again.");
                }
            } else {
                router.push("/post-remote-job/success-publish");
            }
        }

    }

    function renderJobDescriptionForm(topLabel: string, hint: string, onChanged: (text: string) => void) {
        return (
            <div className="flex flex-col align-items-stretch w-full my-2 mr-2">
                <label className="label">
                    <span className="label-text">{topLabel}</span>
                </label>
                <div className='flex grow'>
                    <RichTextEditorComponent value={formJobPostData.positionDescription} onTextChanged={onChanged}/>
                </div>
            </div>
        )
    }

    function renderPreview() {
        return (
            <div className='flex flex-col bg-slate-100 p-5'>
                <h2 className='text-xl font-bold'>Job Post Preview</h2>
                <JobPostContentComponent jobPostDetails={formJobPostData} />
            </div>
        )
    }

    return (
        <div className='list-item-bg drop-shadow p-3 flex flex-col sm:flex-row gap-4'>
            <article className='prose flex flex-col gap-2'>
                <h2>💻🏡 Publish a remote job offer for FREE</h2>
                <span>Sharing a remote job in our community means reaching more than 2k registered users and still growing. Sharing a job offer will help others to find their dream remote job.</span>

                <div className='mt-5 divider'>
                    <span className='flex flex-row items-center text-slate-400 gap-2'><BsLinkedin/> Import From LinkedIn</span>
                </div>

                <ImportLinkedinJobPostComponent onJobDataGenerated={setFormJobPostData}/>

                <div className='mt-5 divider'>
                    <span className='text-slate-400'>JOB DETAILS</span>
                </div>

                <InputTextComponent topLabel={"* Job Title"} hint={"Describe the job position you are hiring"}
                                    onChanged={onJobTitleChanged} defaultValue={formJobPostData.positionTitle} value={formJobPostData.positionTitle}/>
                <InputTextComponent topLabel={"* Application link"}
                                    hint={"How can the candidate apply to this job position?"}
                                    onChanged={onJobApplicationLink} inputType={"url"} defaultValue={formJobPostData.applicationMethod.url} value={formJobPostData.applicationMethod.url}/>
                <div className='flex flex-wrap w-full gap-2 justify-between'>
                    <DropdownFormComponent label={"* Experience Level"} defaultLabel={"Pick the required experience"}
                                           currentSelectionKey={formJobPostData.experienceLevel}
                                           options={JobPostUtils.getExperienceLevelsOptions()}
                                           onOptionSelected={onJobExperienceLevel}/>
                    <DropdownFormComponent label={"* Job Category"} defaultLabel={"Pick the job category"}
                                           currentSelectionKey={formJobPostData.jobCategory}
                                           options={JobPostUtils.getJobCategoryOptions()}
                                           onOptionSelected={onJobCategorySelection}/>
                </div>

                <InputTagsFieldComponent defaultTags={formJobPostData.jobSkills.mainSkills}
                                         onTagsChanged={onJobSkillsTagsChanged} label={"Job Skills"}
                                         placeholder={"(Press Enter to add a skills)"}/>

                {renderJobDescriptionForm("* Job Description", "Describe all the details about this job offer", onJobDescriptionChanged)}
                {renderRemoteLocationAllowance(formJobPostData.remoteRegionsAllowance!, onRemoteRegionsAllowance)}

                <div className='mt-5 divider'>
                    <span className='text-slate-400'>COMPANY DETAILS</span>
                </div>

                <CompanyDetailsFormComponent formJobPostData={formJobPostData} setFormJobPostData={setFormJobPostData}/>

                <div className='flex flex-col justify-content-center align-items-center'>
                    <button
                        className={"btn " + (isFormDataValid ? "btn-success" : "btn-error btn-outline gap-2") + (isAPILoading ? " loading" : "")}
                        disabled={isAPILoading} onClick={onPublishRemoteJobClick}>{!isFormDataValid ? <BiError /> : <></>} PUBLISH REMOTE JOB
                    </button>
                    {errorMessage ? <span className={"flex items-center gap-2 my-2 text-red-700"}><BiError /> {errorMessage}</span> : <></>}
                </div>

            </article>

            {renderPreview()}
        </div>
    )
}

/**
 * @deprecated We use now the tags field
 * @param listSkills
 */
function renderListSkills(listSkills: string[]) {

    function renderListJobsSkills() {
        return listSkills.filter((skillItem) => skillItem.length > 0 && skillItem !== " ").map((skillItem) => {
            return (
                <div className='flex' key={skillItem}>
                    <JobSkillTagComponent label={skillItem} key={skillItem}/>
                </div>
            )
        });
    }

    return (
        <div className='flex flex-wrap w-full'>
            {renderListJobsSkills()}
        </div>
    )
}

function renderRemoteLocationAllowance(currentValue: { [region: string]: boolean }, onRemoteLocationAllowanceChanged: (data: { [region: string]: boolean, }) => void) {
    let isWorldwide: boolean = currentValue[RemoteZoneRestriction.WORLDWIDE] === true;

    function onIsWorldwideChanged(event: any) {
        isWorldwide = event.target.checked;
        if (isWorldwide) {
            onRemoteLocationAllowanceChanged({
                [RemoteZoneRestriction.WORLDWIDE]: true,
            });
        } else {
            onRemoteLocationAllowanceChanged({
                [RemoteZoneRestriction.WORLDWIDE]: false,
            });
        }
    }

    function onRegionAllowanceChanged(regionKey: string, isSelected: boolean) {
        // Update region selected
        onRemoteLocationAllowanceChanged({
            ...currentValue,
            [regionKey]: isSelected,
        });
    }

    function renderCheckboxRegions() {
        let renderItems: any[] = [];

        let mapRemoteRegions: { [optionId: string]: string } = JobPostUtils.getRemoteRegionAllowance();
        return Object.keys(mapRemoteRegions)
            .filter((regionKey) => regionKey !== RemoteZoneRestriction.WORLDWIDE && regionKey !== RemoteZoneRestriction.UNKNOWN)
            .map((regionKey) => {
                let isChecked: boolean = currentValue[regionKey] === true;
                return (
                    <div className="form-control" key={regionKey}>
                        <label className="label cursor-pointer">
                            <span className="label-text">{mapRemoteRegions[regionKey]}</span>
                            <input type="checkbox" checked={isChecked} className="checkbox"
                                   onChange={(event) => onRegionAllowanceChanged(regionKey, event.target.checked)}/>
                        </label>
                    </div>
                )
            });
    }

    return (
        <div className='flex flex-col'>
            <div className="form-control">
                <label className="cursor-pointer label">
                    <h5>🌎 Is this role open to work from anywhere in the world?</h5>
                    <input type="checkbox" className="checkbox" checked={isWorldwide} onChange={onIsWorldwideChanged}/>
                </label>
                {isWorldwide ? (<></>) : (
                    <div className='flex flex-col'>
                        <h3>Regions that allow to work remotely:</h3>
                        <div className='flex flex-wrap gap-2'>
                            {renderCheckboxRegions()}
                        </div>
                    </div>
                )}
            </div>
        </div>
    )
}

