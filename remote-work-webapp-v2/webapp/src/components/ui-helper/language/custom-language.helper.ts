

export const CustomLanguageLabels: {[labelId: string]: string} = {
    LANDING_PAGE_HEADER_JOIN_COMMUNITY_TITLE: "Join our community of Remote Workers",
    LANDING_PAGE_HEADER_JOIN_COMMUNITY_SUBTITLE: "To discover, share or discuss remote jobs.",
    LANDING_PAGE_DIVIDER_PUBLICATIONS: "RECENT PUBLICATIONS",
    CUSTOM_POST_A_JOB_BTN: "Post a job",
    CUSTOM_WRITE_AN_ARTICLE: "Write an article",
}

export function getCustomLanguageLabel(labelId: string): string | undefined {
    if (Object.keys(CustomLanguageLabels).includes(labelId)) {
        // @ts-ignore
        return CustomLanguageLabels[labelId]! as string;
    } else {
        return undefined;
    }
}