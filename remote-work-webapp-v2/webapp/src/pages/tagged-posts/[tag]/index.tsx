import {BasePostDataModel} from "../../../components/react-shared-module/logic/shared-data/datamodel/base-post-data.model";
import {getAPIDocument} from "../../../components/react-shared-module/logic/shared-data/firebase.utils";
import {
    SharedFirestoreCollectionDB,
    SharedFirestoreDocsDB
} from "../../../components/react-shared-module/logic/shared-data/datamodel/shared-firestore-collections";
import ListPostsComponent from "../../../components/react-shared-module/ui-components/posts/shared/ListPosts.component";
import CustomRootLayoutComponent from "../../../components/app-components/root/CustomRootLayout.component";
import LandingLoginSignupComponent
    from "../../../components/react-shared-module/ui-components/landing-page-content/shared/LandingLoginSignup.component";
import {getLanguageLabel} from "../../../components/react-shared-module/logic/language/language.helper";
import LandingPageSubmitPostComponent
    from "../../../components/react-shared-module/ui-components/landing-page-content/shared/LandingPageSubmitPost.component";

export async function getServerSideProps(context: any) {
    const tag: string = context.params.tag as string;

    const docId: string = SharedFirestoreDocsDB.DataCatalog.PrefixTagType + tag.toLowerCase();

    let listPosts: {[postId: string]: BasePostDataModel} | undefined= await getAPIDocument(SharedFirestoreCollectionDB.DataCatalog, docId);
    return {
        props: {
            listPosts: listPosts ? listPosts : {},
            tag,
            key: tag,
        },
    };
}

export default function TaggedPostsPage(props: {listPosts?: {[postId: string]: BasePostDataModel}, tag: string}) {
    let listPosts: BasePostDataModel[] = props.listPosts ? Object.values(props.listPosts) : [];

    return (
        <CustomRootLayoutComponent>
            <div className='flex flex-col py-5 gap-4 items-center'>
                <div className='flex flex-row gap-4 p-1 items-center'>
                    <h4 className='custom'>Posts about</h4>
                    <div className='rounded px-2 py-1 logo-bg-color'>
                        <h3 className='custom text-white'>#{props.tag.toUpperCase()}</h3>
                    </div>
                </div>
                <LandingPageSubmitPostComponent/>
                <div className='divider text-slate-400'>{getLanguageLabel("LANDING_PAGE_DIVIDER_PUBLICATIONS", "Publications")}</div>
                <ListPostsComponent listPosts={listPosts} hideStats={true}/>
            </div>
        </CustomRootLayoutComponent>
    )
}

export function getTaggedPostsPageURL(category: string) {
    return "/tagged-posts/" + category;
}