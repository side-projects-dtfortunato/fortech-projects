import {BasePostDataModel} from "../components/react-shared-module/logic/shared-data/datamodel/base-post-data.model";
import {getAPIDocument} from "../components/react-shared-module/logic/shared-data/firebase.utils";
import {
    SharedFirestoreCollectionDB,
    SharedFirestoreDocsDB
} from "../components/react-shared-module/logic/shared-data/datamodel/shared-firestore-collections";
import {
    LandingPageDataUtils,
    SharedLandingPageAggregatorsModel
} from "../components/react-shared-module/logic/shared-data/datamodel/shared-landing-page-aggregators.model";
import CustomLandingPageComponent from "../components/app-components/custom-landing-page/CustomLandingPage.component";
import {RemoteWorkNicheSitesUtils} from "../custom-project/api/custom-data-model/remote-work-niche-sites";
import {
    LayoutSideContentAggregatorModel, LayoutSideContentAggregatorUtils
} from "../custom-project/api/custom-data-model/layout-side-content-aggregator.model";

export async function getStaticProps() {
    // Load Recent Posts
    let listRecentPosts: {[postId: string]: BasePostDataModel} | undefined = await getAPIDocument(SharedFirestoreCollectionDB.DataCatalog, SharedFirestoreDocsDB.DataCatalog.FilterRecent);

    let layoutSideContentData: LayoutSideContentAggregatorModel = await LayoutSideContentAggregatorUtils.getLayoutSideContentAggregatorData({
        loadRecentJobs: true,
    });

    return {
        props: {
            landingDataAgg: LandingPageDataUtils.generateLandingPageAggregators(listRecentPosts ? Object.values(listRecentPosts).filter((post) => {
                return RemoteWorkNicheSitesUtils.isPostAvailableForCurrentSite(post);
            }) : []),
            layoutSideContentData,
        },
        revalidate: 30,
    }
}

export default function Home(props: {listRecentPosts?: {[postId: string]: BasePostDataModel}, landingDataAgg: SharedLandingPageAggregatorsModel, layoutSideContentData: LayoutSideContentAggregatorModel}) {

    return (
        <CustomLandingPageComponent landingDataAgg={props.landingDataAgg} layoutSideContentData={props.layoutSideContentData} />
    )
}
