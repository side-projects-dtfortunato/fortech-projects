import {
    BasePostDataModel,
    PostDataUtils
} from "../../components/react-shared-module/logic/shared-data/datamodel/base-post-data.model";
import {getAPIDocument} from "../../components/react-shared-module/logic/shared-data/firebase.utils";
import {
    SharedFirestoreCollectionDB,
    SharedFirestoreDocsDB
} from "../../components/react-shared-module/logic/shared-data/datamodel/shared-firestore-collections";
import {PostTypes} from "../../components/react-shared-module/utils/shared-ui-helper";
import RootConfigs from "../../configs";
import { Feed } from "feed";
import {SharedRoutesUtils} from "../../components/react-shared-module/utils/shared-routes.utils";
import {NextApiRequest, NextApiResponse} from "next";
import {JobPostListInfoModel} from "../../custom-project/api/custom-data-model/job-post-details.model";
import {CustomBackendApi} from "../../custom-project/api/custombackend.api";
import {RemoteWorkNicheSitesUtils} from "../../custom-project/api/custom-data-model/remote-work-niche-sites";
import {CustomRoutesUtils} from "../../custom-project/custom-routes.utils";
import {Enclosure} from "feed/lib/typings";

export default async function RssRecentJobs(req: NextApiRequest, res: NextApiResponse) {

    // Load list jobs
    let listJobs: JobPostListInfoModel[] = [];
    try {
        listJobs = await CustomBackendApi.getAllJobs();

        listJobs = listJobs.filter((jobItem) => {
            return RemoteWorkNicheSitesUtils.isJobAvailableForCurrentSite(jobItem);
        });
    } catch (e) {
        console.log(e);
    }

    var feed = new Feed({
        title: RootConfigs.SITE_TITLE,
        description: RootConfigs.SITE_DESCRIPTION,
        feed: "Recent Remote Jobs",
        image: RootConfigs.BASE_URL + "/images/logo-512.png",
        favicon: RootConfigs.BASE_URL + "/favicon.ico",
        id: RootConfigs.BASE_URL,
        language: "en",
        copyright: "All rights reserved " + new Date().getUTCFullYear()  + ", " + RootConfigs.SITE_NAME,
        link: RootConfigs.BASE_URL,
    });


    listJobs
        .sort((job1, job2) => job2.publishedAt! - job1.publishedAt!)
        .forEach((jobItem: JobPostListInfoModel) => {

        feed.addItem({
            id: jobItem.uid!,
            title: jobItem.positionTitle!,
            description: jobItem.companyDetails.companyName,
            link: RootConfigs.BASE_URL + "/" + CustomRoutesUtils.getRemoteJobDetailsUrl(jobItem.uid!),
            author: [{
                name: jobItem.companyDetails.companyName,
                link: jobItem.companyDetails.website,
            }],
            date: new Date(jobItem.publishedAt!),
            image: jobItem.companyDetails.companyLogo?.fileUrl.endsWith(".png") ? jobItem.companyDetails.companyLogo?.fileUrl : undefined
        });
    });
    res.status(200).setHeader('Content-Type', 'application/xml').send(feed.rss2());
};