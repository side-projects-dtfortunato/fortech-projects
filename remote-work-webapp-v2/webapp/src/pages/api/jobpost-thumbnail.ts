import {CanvasRenderingContext2D, createCanvas, loadImage, registerFont} from 'canvas';
import type {NextApiRequest, NextApiResponse} from 'next';
import Jimp from "jimp";
import path from 'path';
import {JobPostDetailsModel} from "../../custom-project/api/custom-data-model/job-post-details.model";
import RootConfigs from "../../configs";

export default async function handler(req: NextApiRequest, res: NextApiResponse) {
    // Helper function to clean up query parameter keys
    const cleanParamKey = (key: string): string => key.replace(/^amp;/, '');

    // Create an object with cleaned keys
    const cleanedQuery = Object.fromEntries(
        Object.entries(req.query).map(([key, value]) => [cleanParamKey(key), value])
    );

    let {
        companyLogo,
        companyName,
        positionTitle,
        companyWebsite,
        imageType
    } = {
        companyLogo: decodeURIComponent(cleanedQuery.companyLogo as string),
        companyName: decodeURIComponent(cleanedQuery.companyName as string),
        positionTitle: decodeURIComponent(cleanedQuery.positionTitle as string),
        companyWebsite: decodeURIComponent(cleanedQuery.companyWebsite as string),
        imageType: (cleanedQuery.imageType as string)?.toUpperCase() || 'SQUARE',
    };
    const tags = (decodeURIComponent(cleanedQuery.tags as string)).split(",");

    // Optimize image
    const optimizedImage = await generateJobPositionImage({
        positionTitle: positionTitle as string,
        companyLogo: companyLogo as string,
        tags: tags as string[],
        companyName: companyName as string,
        companyWebsite: companyWebsite as string,
        imageType: imageType as 'SQUARE' | 'RECTANGLE',
    });

    // Set response headers
    res.setHeader('Content-Type', 'image/png');
    res.setHeader('Cache-Control', 'public, max-age=31536000, immutable');

    // Send the image
    res.send(optimizedImage);
}

export async function generateJobPositionImage(jobOffer: {
    companyLogo: string;
    companyName: string;
    positionTitle: string;
    companyWebsite: string;
    tags: string[];
    imageType: 'SQUARE' | 'RECTANGLE';
}): Promise<any> {
    // Register the local font
    registerFont(path.join(process.cwd(), 'public', 'fonts', 'Helvetica.ttf'), {family: 'Helvetica'});

    let width = 1080, height = 1080; // Default
    if (jobOffer.imageType === 'SQUARE') {
        width = height = 1080;
    } else {
        // Rectangle (16:9 aspect ratio for URL preview)
        width = 1200;
        height = 630;
    }

    const canvas = createCanvas(width, height);
    const ctx = canvas.getContext('2d');

    // Set background
    ctx.fillStyle = '#F9F9FB';
    ctx.fillRect(0, 0, width, height);

    // Load and draw company logo in a circular background with shadow
    const logo = await loadImage(jobOffer.companyLogo);
    const logoSize = jobOffer.imageType === 'SQUARE' ? 120 : 80;
    let logoX = 70;
    let logoY = 70;

    // Draw shadow
    ctx.save();
    ctx.shadowColor = 'rgba(0, 0, 0, 0.2)';
    ctx.shadowBlur = 15;
    ctx.beginPath();
    ctx.arc(logoX + logoSize / 2, logoY + logoSize / 2, logoSize / 2, 0, Math.PI * 2);
    ctx.fillStyle = '#FFFFFF';
    ctx.fill();
    ctx.restore();

    // Clip the logo to a circle
    ctx.save();
    ctx.beginPath();
    ctx.arc(logoX + logoSize / 2, logoY + logoSize / 2, logoSize / 2, 0, Math.PI * 2);
    ctx.clip();
    ctx.drawImage(logo, logoX, logoY, logoSize, logoSize);
    ctx.restore();

    // Draw company name
    ctx.font = `bold ${jobOffer.imageType === 'SQUARE' ? 48 : 36}px Helvetica`;
    ctx.fillStyle = '#1a202c';
    ctx.fillText(jobOffer.companyName, logoX + logoSize + 30, logoY + 40);

    // Draw company website
    ctx.font = `${jobOffer.imageType === 'SQUARE' ? 24 : 18}px Helvetica`;
    ctx.fillStyle = '#718096';
    ctx.fillText(jobOffer.companyWebsite, logoX + logoSize + 30, logoY + 80);

    // Draw position title (centered, larger, and bold)
    ctx.font = `bold ${jobOffer.imageType === 'SQUARE' ? 80 : 60}px Helvetica`;
    ctx.fillStyle = '#1a202c';
    ctx.textAlign = 'center';
    wrapText(ctx, jobOffer.positionTitle, width / 2, jobOffer.imageType === 'SQUARE' ? 400 : 300, width - 140, jobOffer.imageType === 'SQUARE' ? 90 : 70);
    ctx.textAlign = 'left'; // Reset text alignment

    // Add "New Remote Job" banner with shadow and rounded corners
    if (jobOffer.imageType === 'SQUARE') {
        drawNewRemoteJobBanner(ctx, width);
    }

    // Draw tags
    ctx.font = `bold ${jobOffer.imageType === 'SQUARE' ? 28 : 24}px Helvetica`;
    let tagX = 70;
    let tagY = jobOffer.imageType === 'SQUARE' ? 700 : 450;
    jobOffer.tags.forEach(tag => {
        const tagWidth = ctx.measureText(tag).width + 40;
        if (tagX + tagWidth > width - 70) {
            tagX = 70;
            tagY += 60;
        }
        drawTag(ctx, tag, tagX, tagY);
        tagX += tagWidth + 20;
    });

    // Draw "Apply now" button
    if (jobOffer.imageType === 'SQUARE') {
        drawButton(ctx, 'Apply now', 70, 950, 200, 70);
    }

    // Load and draw the website logo
    await drawWebsiteLogo(ctx, width, height);

    // Convert canvas to buffer
    const buffer = await canvas.toBuffer();

    // Optimize image
    const optimizedImage = await Jimp.read(buffer)
        .then((image: any) => {
            return image
                .resize(width / 2, height / 2)
                .quality(80)
                .getBufferAsync(Jimp.MIME_JPEG);
        });
    return optimizedImage;
}

function wrapText(ctx: CanvasRenderingContext2D, text: string, x: number, y: number, maxWidth: number, lineHeight: number) {
    const words = text.split(' ');
    let line = '';

    for (const word of words) {
        const testLine = line + word + ' ';
        const metrics = ctx.measureText(testLine);
        const testWidth = metrics.width;
        if (testWidth > maxWidth && line !== '') {
            ctx.fillText(line, x, y);
            line = word + ' ';
            y += lineHeight;
        } else {
            line = testLine;
        }
    }
    ctx.fillText(line, x, y);
}

function drawNewRemoteJobBanner(ctx: CanvasRenderingContext2D, canvasWidth: number) {
    ctx.save();
    ctx.translate(canvasWidth - 250, 140);
    ctx.rotate(Math.PI / 4);

    // Add shadow
    ctx.shadowColor = 'rgba(0, 0, 0, 0.3)';
    ctx.shadowBlur = 10;
    ctx.shadowOffsetX = 3;
    ctx.shadowOffsetY = 3;

    // Draw rounded rectangle
    const bannerWidth = 300;
    const bannerHeight = 60;
    const cornerRadius = 10;

    ctx.beginPath();
    ctx.moveTo(cornerRadius, 0);
    ctx.lineTo(bannerWidth - cornerRadius, 0);
    ctx.quadraticCurveTo(bannerWidth, 0, bannerWidth, cornerRadius);
    ctx.lineTo(bannerWidth, bannerHeight - cornerRadius);
    ctx.quadraticCurveTo(bannerWidth, bannerHeight, bannerWidth - cornerRadius, bannerHeight);
    ctx.lineTo(cornerRadius, bannerHeight);
    ctx.quadraticCurveTo(0, bannerHeight, 0, bannerHeight - cornerRadius);
    ctx.lineTo(0, cornerRadius);
    ctx.quadraticCurveTo(0, 0, cornerRadius, 0);
    ctx.closePath();

    ctx.fillStyle = '#EEB2BE';
    ctx.fill();

    // Reset shadow for text
    ctx.shadowColor = 'transparent';
    ctx.shadowBlur = 0;
    ctx.shadowOffsetX = 0;
    ctx.shadowOffsetY = 0;

    ctx.fillStyle = '#093769';
    ctx.font = 'bold 24px Helvetica';
    ctx.fillText('NEW REMOTE JOB', 40, 40);
    ctx.restore();
}

function drawTag(ctx: CanvasRenderingContext2D, tag: string, x: number, y: number) {
    ctx.save();
    ctx.shadowColor = 'rgba(0, 0, 0, 0.2)';
    ctx.shadowBlur = 5;
    ctx.shadowOffsetX = 2;
    ctx.shadowOffsetY = 2;

    ctx.fillStyle = '#FFFFFF';
    const tagWidth = ctx.measureText(tag).width + 40;
    const tagHeight = 44;
    const radius = tagHeight / 2;

    ctx.beginPath();
    ctx.moveTo(x + radius, y);
    ctx.lineTo(x + tagWidth - radius, y);
    ctx.quadraticCurveTo(x + tagWidth, y, x + tagWidth, y + radius);
    ctx.lineTo(x + tagWidth, y + tagHeight - radius);
    ctx.quadraticCurveTo(x + tagWidth, y + tagHeight, x + tagWidth - radius, y + tagHeight);
    ctx.lineTo(x + radius, y + tagHeight);
    ctx.quadraticCurveTo(x, y + tagHeight, x, y + tagHeight - radius);
    ctx.lineTo(x, y + radius);
    ctx.quadraticCurveTo(x, y, x + radius, y);
    ctx.fill();

    ctx.shadowColor = 'transparent';
    ctx.fillStyle = '#333333';
    ctx.fillText(tag, x + 20, y + 30);
    ctx.restore();
}

function drawButton(ctx: CanvasRenderingContext2D, text: string, x: number, y: number, width: number, height: number) {
    ctx.save();
    ctx.shadowColor = 'rgba(0, 0, 0, 0.2)';
    ctx.shadowBlur = 5;
    ctx.shadowOffsetX = 2;
    ctx.shadowOffsetY = 2;

    ctx.fillStyle = '#FFFFFF';
    const radius = height / 2;

    ctx.beginPath();
    ctx.moveTo(x + radius, y);
    ctx.lineTo(x + width - radius, y);
    ctx.quadraticCurveTo(x + width, y, x + width, y + radius);
    ctx.lineTo(x + width, y + height - radius);
    ctx.quadraticCurveTo(x + width, y + height, x + width - radius, y + height);
    ctx.lineTo(x + radius, y + height);
    ctx.quadraticCurveTo(x, y + height, x, y + height - radius);
    ctx.lineTo(x, y + radius);
    ctx.quadraticCurveTo(x, y, x + radius, y);
    ctx.fill();

    ctx.shadowColor = 'transparent';
    ctx.font = 'bold 28px Helvetica';
    ctx.fillStyle = '#333333';
    const textWidth = ctx.measureText(text).width;
    ctx.fillText(text, x + (width - textWidth) / 2, y + height / 2 + 10);
    ctx.restore();
}

async function drawWebsiteLogo(ctx: CanvasRenderingContext2D, canvasWidth: number, canvasHeight: number) {
    const websiteLogo = await loadImage("https://www.remote-work.app/images/logo.png");

    const maxLogoWidth = 200;
    const maxLogoHeight = 80;

    const scaleX = maxLogoWidth / websiteLogo.width;
    const scaleY = maxLogoHeight / websiteLogo.height;
    const scale = Math.min(scaleX, scaleY);

    const logoWidth = websiteLogo.width * scale;
    const logoHeight = websiteLogo.height * scale;

    const logoX = canvasWidth - logoWidth - 20;
    const logoY = 20;

    ctx.drawImage(websiteLogo, logoX, logoY, logoWidth, logoHeight);
}

export function generateJobPostThumbnailUrl(jobPost: JobPostDetailsModel, imageType: 'SQUARE' | 'RECTANGLE' = 'SQUARE'): string {
    const skills = [
        ...jobPost.jobSkills.mainSkills ?? [],
        ...jobPost.jobSkills.secondarySkills ?? []
    ];

    const tags = skills.map(skill => encodeURIComponent(skill.toUpperCase())).join(',');

    const params = new URLSearchParams({
        positionTitle: jobPost.positionTitle,
        companyWebsite: encodeURIComponent(jobPost.customZoneRestriction ?? ''),
        companyLogo: jobPost.companyDetails.companyLogo.fileUrl ?? '',
        companyName: jobPost.companyDetails.companyName ?? '',
        tags,
        imageType
    });

    const url = `${RootConfigs.BASE_URL}/api/jobpost-thumbnail?${params.toString()}`;
    console.log(url);
    return url;
}