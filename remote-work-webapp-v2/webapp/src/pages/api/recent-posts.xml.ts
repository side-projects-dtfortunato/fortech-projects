import {
    BasePostDataModel,
    PostDataUtils
} from "../../components/react-shared-module/logic/shared-data/datamodel/base-post-data.model";
import {getAPIDocument} from "../../components/react-shared-module/logic/shared-data/firebase.utils";
import {
    SharedFirestoreCollectionDB,
    SharedFirestoreDocsDB
} from "../../components/react-shared-module/logic/shared-data/datamodel/shared-firestore-collections";
import {PostTypes} from "../../components/react-shared-module/utils/shared-ui-helper";
import RootConfigs from "../../configs";
import { Feed } from "feed";
import {SharedRoutesUtils} from "../../components/react-shared-module/utils/shared-routes.utils";
import {NextApiRequest, NextApiResponse} from "next";

export default async function RssRecentPosts(req: NextApiRequest, res: NextApiResponse) {

    let docResponse: {[postId: string]: BasePostDataModel} | undefined = await getAPIDocument(SharedFirestoreCollectionDB.DataCatalog, SharedFirestoreDocsDB.DataCatalog.FilterRecent);
    let listRecentPosts: BasePostDataModel[] = docResponse ? Object.values(docResponse)
        .filter((postItem: BasePostDataModel) => {
        return postItem.availableNicheSites?.includes(RootConfigs.CURRENT_NICHE_SITE)
            && (postItem.postType === PostTypes.ARTICLE_BLOCKS || postItem.postType === PostTypes.WRITE_ARTICLE)
    }).sort((post1, post2) => post2.publishedAt - post1.publishedAt) : [];

    var feed = new Feed({
        title: RootConfigs.SITE_TITLE,
        description: RootConfigs.SITE_DESCRIPTION,
        link: RootConfigs.BASE_URL,
        image: RootConfigs.BASE_URL + "/images/logo-512.png",
        favicon: RootConfigs.BASE_URL + "/favicon.ico",
        id: RootConfigs.BASE_URL,
        language: "en",
        copyright: "All rights reserved " + new Date().getUTCFullYear()  + ", " + RootConfigs.SITE_NAME,
    });


    listRecentPosts.forEach((postItem: BasePostDataModel) => {
        feed.addItem({
            id: postItem.uid!,
            title: postItem.title!,
            description: postItem.summary,
            link: RootConfigs.BASE_URL + "/" + postItem.uid!,
            author: [{
                name: PostDataUtils.getUserCreatorInfos(postItem)?.name,
                link: SharedRoutesUtils.getPublicProfilePageURL(postItem.userCreatorId),
                email: PostDataUtils.getUserCreatorInfos(postItem)?.email,
            }],
            date: new Date(postItem.publishedAt),
            image: postItem.thumbnailUrl,
        });
    });
    res.status(200).setHeader('Content-Type', 'application/xml').send(feed.rss2());
};