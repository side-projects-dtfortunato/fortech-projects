import SharedBookmarksPageContentComponent
    from "../../components/react-shared-module/ui-components/pages-content/SharedBookmarksPageContent.component";

export default function BookmarksPage() {

    return (
        <SharedBookmarksPageContentComponent />
    )
}


export function getBookmarksPageUrl() {
    return "/bookmarks";
}