import CustomRootLayoutComponent from "../../components/app-components/root/CustomRootLayout.component";
import PostRemoteJobPageComponent from "../../components/app-components/post-remote-job/PostRemoteJobPage.component";

export default function PostRemoteJobPage() {

    return (
        <CustomRootLayoutComponent isIndexable={false} customBody={true}>
            <div className='py-5'>
                <PostRemoteJobPageComponent />
            </div>
        </CustomRootLayoutComponent>
    )
}

export function getPostRemoteJobPageUrl() {
    return "/post-remote-job";
}