
import Link from "next/link";
import CustomRootLayoutComponent from "../../components/app-components/root/CustomRootLayout.component";
import RootConfigs from "../../configs";
import Image from "next/image";


export default function SuccessPublishPage() {
    return (
        <CustomRootLayoutComponent isIndexable={false} customBody={true} pageHeadProps={{title: "Remote Job Published", description: RootConfigs.SITE_DESCRIPTION}}>
            <div className='h-screen w-full flex justify-center items-center'>
                <div className='list-item-bg drop-shadow p-5'>
                    <article className='prose text-center'>
                        <div className='flex justify-center'>
                            <Image className='my-4' alt={"Checked"} src={"/images/ic_checked.png"} width={120} height={120}></Image>
                        </div>
                        <h2 className='text-center'>Remote Job published with success</h2>
                        <span className='text-center'>Thank you to publish this remote job offer in our platform. You can also get it promote it. Promoting a job offer will increase the number of applications and visibility.</span>
                        <div className='flex flex-row gap-2 justify-center my-4'>
                            <Link href={"/"} className='btn btn-ghost'>Back to home</Link>
                            <Link href={"https://buy.stripe.com/fZe8yg6V78KKaUoaEF"} target='_blank' className='btn btn-success'>🚀 Promote this job</Link>
                        </div>
                    </article>
                </div>
            </div>
        </CustomRootLayoutComponent>
    )
}