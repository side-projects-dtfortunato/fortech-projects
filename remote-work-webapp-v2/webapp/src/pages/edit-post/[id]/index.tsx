import {
    BasePostDataModel
} from "../../../components/react-shared-module/logic/shared-data/datamodel/base-post-data.model";
import {useRouter} from "next/router";
import {useCentralUserDataState} from "../../../components/react-shared-module/logic/global-hooks/root-global-state";
import {useEffect, useState} from "react";
import CustomRootLayoutComponent from "../../../components/app-components/root/CustomRootLayout.component";
import SpinnerComponent from "../../../components/react-shared-module/ui-components/shared/Spinner.component";
import {getAPIDocument} from "../../../components/react-shared-module/logic/shared-data/firebase.utils";
import {
    SharedFirestoreCollectionDB
} from "../../../components/react-shared-module/logic/shared-data/datamodel/shared-firestore-collections";
import NotFoundContentComponent
    from "../../../components/react-shared-module/ui-components/shared/NotFoundContent.component";
import {
    UserRole
} from "../../../components/react-shared-module/logic/shared-data/datamodel/shared-central-user-data.model";
import dynamic from "next/dynamic";
import {NextPageContext} from "next";
const EditPostPageComponent = dynamic(() => import("../../../components/react-shared-module/ui-components/edit-post/EditPostPage.component"), {
    ssr: false,
});


EditPostPage.getInitialProps = async (ctx: NextPageContext) => {
    const postId: string | string[] | undefined = ctx.query.id;
    let postData: BasePostDataModel | undefined;

    if (postId) {
        postData = await getAPIDocument<BasePostDataModel | undefined>(SharedFirestoreCollectionDB.PostsDetails, postId as string);
    }

    return { postData, postId }
}

export default function EditPostPage(props: {postId?: string, postData?: BasePostDataModel}) {
    const [centralUser, setCentralUserData] = useCentralUserDataState();
    const router = useRouter();

    function isAllowed() {
        if (!centralUser) {
            return false;
        }

        return centralUser.userRole === UserRole.ADMIN || centralUser.uid === props.postData?.userCreatorId;
    }


    if (!props.postData) {
        return (
            <CustomRootLayoutComponent isIndexable={false}>
                <NotFoundContentComponent/>
            </CustomRootLayoutComponent>
        )
    } else if (!isAllowed()) {
        return (<></>);
    } else {
        return (<CustomRootLayoutComponent isIndexable={false}>
            <EditPostPageComponent postData={props.postData}/>
        </CustomRootLayoutComponent>)
    }
}


export function getEditPostUrl(postId: string) {
    return "/edit-post/" + postId;
}