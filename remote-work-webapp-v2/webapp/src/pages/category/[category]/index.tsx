import {BasePostDataModel} from "../../../components/react-shared-module/logic/shared-data/datamodel/base-post-data.model";
import {getAPIDocument} from "../../../components/react-shared-module/logic/shared-data/firebase.utils";
import {
    SharedFirestoreCollectionDB,
    SharedFirestoreDocsDB
} from "../../../components/react-shared-module/logic/shared-data/datamodel/shared-firestore-collections";
import RootLayoutComponent, {
    PageHeadProps
} from "../../../components/react-shared-module/ui-components/root/RootLayout.component";
import ListPostsComponent from "../../../components/react-shared-module/ui-components/posts/shared/ListPosts.component";
import {CATEGORIES_OPTIONS, UIHelper} from "../../../components/ui-helper/UIHelper";
import RootConfigs from "../../../configs";
import CustomRootLayoutComponent from "../../../components/app-components/root/CustomRootLayout.component";
import LandingLoginSignupComponent
    from "../../../components/react-shared-module/ui-components/landing-page-content/shared/LandingLoginSignup.component";
import {getLanguageLabel} from "../../../components/react-shared-module/logic/language/language.helper";
import LandingPageSubmitPostComponent
    from "../../../components/react-shared-module/ui-components/landing-page-content/shared/LandingPageSubmitPost.component";

export async function getStaticPaths() {
    let listPaths: any[] = [];

    listPaths = Object.values(CATEGORIES_OPTIONS).map((categoryData) => {
        return {
            params: {
                category: categoryData.optionId,
            },
        }
    });

    return {
        paths: listPaths,
        fallback: true, // can also be true or 'blocking'
    }
}

export async function getStaticProps(context: any) {
    const category: string = context.params.category as string;
    const docId: string = SharedFirestoreDocsDB.DataCatalog.PrefixCategoryType + category;

    let listPosts: {[postId: string]: BasePostDataModel} | undefined= await getAPIDocument(SharedFirestoreCollectionDB.DataCatalog, docId);

    return {
        props: {
            listPosts: listPosts ? listPosts : {},
            category,
            key: category,
        },
        revalidate: (60 * 12),
    };
}

export default function CategoryPostsPage(props: {listPosts: BasePostDataModel[], category: string}) {
    let listPosts: BasePostDataModel[] = props.listPosts ? Object.values(props.listPosts) : [];


    const pageHeadProps: PageHeadProps = {
        title: RootConfigs.SITE_TITLE + " - Posts about " + props.category,
        description: RootConfigs.SITE_DESCRIPTION,
    };

    return (
        <CustomRootLayoutComponent pageHeadProps={pageHeadProps}>
            <div className='flex flex-col py-5 gap-4 items-center'>
                <div className='flex flex-row gap-4 p-1 items-center'>
                    <h4 className='custom'>Posts about</h4>
                    <div className='rounded px-2 py-1 logo-bg-color'>
                        <h3 className='custom text-white'>{UIHelper.getPostCategoryLabel(props.category)}</h3>
                    </div>
                </div>
                <LandingPageSubmitPostComponent/>
                <div className='divider text-slate-400'>{getLanguageLabel("LANDING_PAGE_DIVIDER_PUBLICATIONS", "Publications")}</div>
                <ListPostsComponent listPosts={listPosts}/>
            </div>
        </CustomRootLayoutComponent>
    )
}

export function getCategoryPostsPageURL(category: string) {
    return "/category/" + category;
}