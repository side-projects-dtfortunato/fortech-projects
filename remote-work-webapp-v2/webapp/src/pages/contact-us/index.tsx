import {useState} from "react";
import {
    RootGlobalStateActions,
    useAlertMessageGlobalState
} from "../../components/react-shared-module/logic/global-hooks/root-global-state";
import CustomRootLayoutComponent from "../../components/app-components/root/CustomRootLayout.component";
import InputTextComponent from "../../components/react-shared-module/ui-components/form/InputText.component";
import InputTextAreaComponent from "../../components/react-shared-module/ui-components/form/InputTextArea.component";
import {SharedBackendApi} from "../../components/react-shared-module/logic/shared-data/sharedbackend.api";
import {
    SharedFirestoreCollectionDB
} from "../../components/react-shared-module/logic/shared-data/datamodel/shared-firestore-collections";
import CommonUtils from "../../components/react-shared-module/logic/commonutils";

export interface ContactUsFormData {
    name: string,
    email: string,
    message: string,
    createdAt?: string,
}

export default function ContactUsPage() {
    const [contactUsForm, setContactUsFormData] = useState<ContactUsFormData>({name: "", email: "", message: ""});
    const [isLoading, setIsLoading] = useState(false);
    const [alertMessage, setAlertMessage] = useAlertMessageGlobalState();


    function onSendMessage() {
        contactUsForm.createdAt = new Date().toISOString();
        setIsLoading(true);
        SharedBackendApi.requestDocumentChanges({
            docData: contactUsForm,
            methodType: "CREATE",
            docId: Date.now().toString(),
            collectionName: SharedFirestoreCollectionDB.ContactUs,
        }).then((response) => {
            setIsLoading(false);
            setContactUsFormData({name: "", email: "", message: ""});

            RootGlobalStateActions.displayAlertMessage({
                message: "We received your message",
                setAlertMessageState: setAlertMessage,
                alertType: "SUCCESS"
            });
        });
    }

    function isFormValid() {
        return contactUsForm.name.length > 2 && CommonUtils.isValidEmail(contactUsForm.email) && contactUsForm.message.length > 2;
    }

    return (
        <CustomRootLayoutComponent>
            <div className={'list-item-bg custom flex flex-col gap-2 drop-shadow p-5'}>
                <h1 className='custom'>Contact Us</h1>
                <InputTextComponent topLabel={"Name"} hint={"What's your name?"} value={contactUsForm.name} onChanged={(text) => {
                    setContactUsFormData({
                        ...contactUsForm,
                        name: text,
                    });
                }
                }/>
                <InputTextComponent topLabel={"Email"} hint={"What's your email?"} inputType={"email"} value={contactUsForm.email} onChanged={(text) => {
                    setContactUsFormData({
                        ...contactUsForm,
                        email: text,
                    });
                }
                }/>
                <InputTextAreaComponent topLabel={"Message"} hint={"What is your message?"} value={contactUsForm.message} onChanged={(text) => {
                    setContactUsFormData({
                        ...contactUsForm,
                        message: text,
                    });
                }
                }/>

                <button className={"btn btn-primary btn-outline " + (isLoading ? "loading" : "")} disabled={isLoading || !isFormValid()} onClick={onSendMessage}>Send</button>
            </div>
        </CustomRootLayoutComponent>
    )

}