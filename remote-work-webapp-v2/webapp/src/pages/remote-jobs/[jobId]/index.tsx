import {
    JobPostDetailsModel
} from "../../../custom-project/api/custom-data-model/job-post-details.model";
import CustomRootLayoutComponent from "../../../components/app-components/root/CustomRootLayout.component";
import NotFoundContentComponent
    from "../../../components/react-shared-module/ui-components/shared/NotFoundContent.component";
import {
    LayoutSideContentAggregatorModel,
} from "../../../custom-project/api/custom-data-model/layout-side-content-aggregator.model";

export async function getStaticPaths() {
    let listPaths: { params: { jobId: string } }[] = [];

    return {
        paths: listPaths,
        fallback: true, // can also be true or 'blocking'
    }
}

export async function getStaticProps(context: any) {

    return {
        revalidate: 600, // 600 seconds - 10 minutes
    }
}

export default function jobDetails(props: {jobDetails?: JobPostDetailsModel, jobPostId: string, layoutSideContentData?: LayoutSideContentAggregatorModel}) {
    return (
        <CustomRootLayoutComponent layoutSideContentData={props.layoutSideContentData}>
            <NotFoundContentComponent />
        </CustomRootLayoutComponent>
    );

}