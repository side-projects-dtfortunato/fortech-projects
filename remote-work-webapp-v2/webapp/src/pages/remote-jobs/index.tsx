import {JobPostListInfoModel} from "../../custom-project/api/custom-data-model/job-post-details.model";
import CustomRootLayoutComponent from "../../components/app-components/root/CustomRootLayout.component";
import {CustomBackendApi} from "../../custom-project/api/custombackend.api";
import RemoteJobsListPage from "../../components/app-components/remote-jobs/RemoteJobsList.page";
import {RemoteWorkNicheSitesUtils} from "../../custom-project/api/custom-data-model/remote-work-niche-sites";
import LandingRecentPostsComponent
    from "../../components/app-components/custom-landing-page/components/LandingRecentPosts.component";
import {
    LayoutSideContentAggregatorModel,
    LayoutSideContentAggregatorUtils
} from "../../custom-project/api/custom-data-model/layout-side-content-aggregator.model";

const MAX_INITIAL_DISPLAY_LIST_ITEMS = 20;

export async function getStaticProps() {
    // Load list jobs
    let listJobs: JobPostListInfoModel[] | undefined;
    try {
        listJobs = await CustomBackendApi.getAllJobs();

        listJobs = listJobs.filter((jobItem) => {
            return RemoteWorkNicheSitesUtils.isJobAvailableForCurrentSite(jobItem);
        });
    } catch (e) {
        console.log(e);
    }

    let layoutSideContentData: LayoutSideContentAggregatorModel = await LayoutSideContentAggregatorUtils.getLayoutSideContentAggregatorData({
        loadRecentPosts: true,
    });

    return {
        props: {
            listJobs: listJobs ? listJobs : [],
            layoutSideContentData
        },
        revalidate: (60*5), // 5 mins
    }

}

export default function RemoteJobsPage(props: {listJobs: JobPostListInfoModel[], layoutSideContentData?: LayoutSideContentAggregatorModel}) {


    return (
        <CustomRootLayoutComponent layoutSideContentData={props.layoutSideContentData}>
            <RemoteJobsListPage listJobs={props.listJobs}/>
        </CustomRootLayoutComponent>
    )
}

function getRemoteJobsPageUrl() {
    return "/remote-jobs";
}