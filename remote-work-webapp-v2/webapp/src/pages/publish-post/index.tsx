import CustomRootLayoutComponent from "../../components/app-components/root/CustomRootLayout.component";
import {useAuthState} from "react-firebase-hooks/auth";
import {auth} from "../../components/react-shared-module/logic/shared-data/firebase.utils";
import {useRouter} from "next/router";
import SpinnerComponent from "../../components/react-shared-module/ui-components/shared/Spinner.component";
import AuthManager from "../../components/react-shared-module/logic/auth/auth.manager";
import {getLoginPageLink} from "../login";
import {PostTypes} from "../../components/react-shared-module/utils/shared-ui-helper";
import dynamic from "next/dynamic";
const PublishPostFormPageComponent = dynamic(() => import("../../components/react-shared-module/ui-components/post-form/page/PublishPostFormPage.component"), {
    ssr: false,
});

export default function PublishPostPage() {
    const [currentUser, isAuthLoading] = useAuthState(auth);
    const router = useRouter();
    let defaultPostType: PostTypes = PostTypes.ARTICLE_BLOCKS;

    if (router.query.posttype && Object.keys(PostTypes).includes(router.query.posttype as string)) {
        defaultPostType = router.query.posttype as PostTypes;
    }

    if (isAuthLoading) {
        return (
            <CustomRootLayoutComponent customBody={true}>
                <div className='flex flex-col w-full h-full items-center justify-center'>
                    <SpinnerComponent />
                </div>
            </CustomRootLayoutComponent>
        );
    } else if (!AuthManager.isUserLogged()) {
        // Redirect to login page
        router.push(getLoginPageLink());
        return (
            <CustomRootLayoutComponent customBody={true}>
                <div className='flex flex-col w-full h-full items-center justify-center'>
                    <SpinnerComponent />
                </div>
            </CustomRootLayoutComponent>
        );
    } else {
        return (
            <CustomRootLayoutComponent customBody={true}>
                <div className='flex flex-col m-2 mb-5 w-full max-w-3xl'>
                    <h2 className='text-2xl my-4 font-bold'>Submit a Post to our community</h2>
                    <PublishPostFormPageComponent defaultPostType={defaultPostType} showOnlyForm={true} />
                </div>
            </CustomRootLayoutComponent>
        )
    }
}

export function getPublishPostPageUrl(postTypeDefault?: PostTypes) {
    let url = "/publish-post";
    if (postTypeDefault) {
        url += "?posttype=" + postTypeDefault;
    }
    return url;
}