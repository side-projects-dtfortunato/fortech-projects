import {
    DailyPublishedJobs, JobListModel, JobsDiscoverDataUtils
} from "../../components/react-shared-module/base-projects/jobs-discover/jobs-discover-data.model";
import {FirebaseClientFirestoreUtils} from "../../components/react-shared-module/logic/shared-data/firebase.utils";
import {
    SharedFirestoreCollectionDB
} from "../../components/react-shared-module/logic/shared-data/datamodel/shared-firestore-collections";
import CustomRootLayoutComponent from "../../components/app-components/root/CustomRootLayout.component";
import JobListPageComponent
    from "../../components/react-shared-module/base-projects/jobs-discover/ui/job-list-page.component";
import NewsletterSignupListBannerComponent
    from "../../components/react-shared-module/ui-components/newsletter/shared/newsletter-signup-list-banner.component";
import NewsletterPopupComponent
    from "../../components/react-shared-module/ui-components/newsletter/shared/newsletter-popup.component";
import {
    LayoutSideContentAggregatorModel,
    LayoutSideContentAggregatorUtils
} from "../../custom-project/api/custom-data-model/layout-side-content-aggregator.model";


const ITEMS_PER_PAGE = 3;

export async function getStaticProps() {
    const listPublishedDays: DailyPublishedJobs[] = await FirebaseClientFirestoreUtils
        .getDocumentsWithPagination(SharedFirestoreCollectionDB.JobsDiscoverProject.DailyPublishedJobs, ITEMS_PER_PAGE, "publishedDayId");


    let layoutSideContentData: LayoutSideContentAggregatorModel = await LayoutSideContentAggregatorUtils.getLayoutSideContentAggregatorData({
        loadRecentPosts: true,
    });

    let filterListSkills: string[] = await JobsDiscoverDataUtils.getFiltersListSkills();


    let listTopJobs: JobListModel[] = [];

    // Get Featured Jobs
    const listFeaturedJobs: JobListModel[] = await FirebaseClientFirestoreUtils
        .getDocumentsMultipleFilteredWithPagination({
            collectionPath: SharedFirestoreCollectionDB.JobsDiscoverProject.JobDetails,
            pageSize: 10,
            fieldOrderBy: "createdAt",
            filters: [{ field: "publishPlanType", value: "FEATURED" }
            ]  ,
        });
    if (listFeaturedJobs.length > 0) {
        listTopJobs = listFeaturedJobs;
    }

    // Get Popular Jobs
    const popularJobsCount = 5 - listFeaturedJobs.length;
    if (popularJobsCount > 0) {
        const timeLimit = new Date().getTime() - 10 * 24 * 60 * 60 * 1000;

        let listPopularJobs: JobListModel[] = await FirebaseClientFirestoreUtils
        .getDocumentsMultipleFilteredWithPagination({
            collectionPath: SharedFirestoreCollectionDB.JobsDiscoverProject.JobDetails,
            pageSize: 40,
            fieldOrderBy: "stats.applyCounter"
        });
        listPopularJobs = listPopularJobs.map((job) => ({ ...job, isPopular: true }))
                        .sort((a, b) => a.createdAt! - b.createdAt!)
                        .filter((job) => job.createdAt! > timeLimit)
                        .slice(0, popularJobsCount);
        listTopJobs = [...listTopJobs, ...listPopularJobs];
    }


    return {
        props: {
            listPublishedDays,
            layoutSideContentData,
            filterListSkills,
            popularJobs: listTopJobs,
        },
        revalidate: 60*30, // 30 minutes
    }
}

export default function JobsPage(props: {listPublishedDays: DailyPublishedJobs[], layoutSideContentData?: LayoutSideContentAggregatorModel, filterListSkills?: string[], popularJobs?: JobListModel[]}) {

    return (
        <CustomRootLayoutComponent layoutSideContentData={props.layoutSideContentData}>

            <JobListPageComponent preloadedData={props.listPublishedDays} itemsPerPage={ITEMS_PER_PAGE} filterListSkills={props.filterListSkills} popularJobs={props.popularJobs} />

            <div className='flex flex-col items-center w-full'>
                <NewsletterSignupListBannerComponent title={"Subscribe our Newsletter"} />
            </div>
            {/* Popups */}
            <NewsletterPopupComponent />
        </CustomRootLayoutComponent>
    )
}


export function getRemoteJobsPageUrl() {
    return "/jobs";
}