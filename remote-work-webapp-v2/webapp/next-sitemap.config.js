/** @type {import('next-sitemap').IConfig} */
module.exports = {
    siteUrl: process.env.SITE_URL || 'https://www.remote-work.app',
    generateRobotsTxt: true, // (optional)
    exclude: ["/job/job-post-submission", "/login", "/login/recover-password", "/edit-post/*", "/edit-profile/user-profile", "/bookmarks", "/privacy-policy", "/profile/user-profile", "/eula"],
    // ...other options
}