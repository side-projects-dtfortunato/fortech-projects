import * as admin from "firebase-admin";

export class CustomUtils {
    static isEnvProduction() {
        return admin.instanceId().app.options.projectId === "remote-flex-jobs";
    }
}