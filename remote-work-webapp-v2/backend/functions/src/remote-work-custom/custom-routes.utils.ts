import {WEBAPP_CONFIGS} from "../configs/webapp.configs";

export class CustomRoutesUtils {

    static getJobPostDetailsUrl(jobPostUid: string) {
        return WEBAPP_CONFIGS.WEBSITE_URL + "/remote-jobs/" + jobPostUid;
    }

}