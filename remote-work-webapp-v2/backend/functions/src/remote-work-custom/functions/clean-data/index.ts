import {JobPostDetailsModel} from "../../data-model/job-post-details.model";
import {getDocumentData} from "../../../shareable-firebase-backend/utils/utils";
import * as functions from "firebase-functions";
import * as admin from "firebase-admin";
import {
    CustomFirestoreCollectionDB,
    CustomFirestoreDocsDB
} from "../../custom-firestore-collection-names";
import {EXPIRATION_TIME_30DAYS} from "../post-remote-job";
import {getQuickResponse} from "../../../shareable-firebase-backend/model-data/api-response.model";
import {SharedFirestoreCollectionDB} from "../../../shareable-firebase-backend/model-data/shared-firestore-collections";

export const scanExpiredPublishedJobs = functions.https.onRequest(async (request,
                                                                         response) => {
    const publishedJobPosts: {[uid: string]: JobPostDetailsModel} = await getDocumentData(CustomFirestoreCollectionDB.CollectionDataCatalog, CustomFirestoreDocsDB.CollectionDataCatalog.JobsListCatalog);

    const batch = admin.firestore().batch();

    // Check if some job post has already expired
    if (publishedJobPosts && Object.keys(publishedJobPosts).length > 0) {

        Object.keys(publishedJobPosts).forEach((jobUID) => {
            const jobPostData: JobPostDetailsModel = publishedJobPosts[jobUID];

            if (!jobPostData.expirationTime && jobPostData.publishedAt) {
                jobPostData.expirationTime = jobPostData.publishedAt + EXPIRATION_TIME_30DAYS;
            }

            // Check if has expired
            if (!jobPostData.expirationTime || jobPostData.expirationTime < Date.now()) {
                // Remove from published jobs and update status on job post details
                batch.set(admin.firestore().collection(CustomFirestoreCollectionDB.CollectionDataCatalog).doc(CustomFirestoreDocsDB.CollectionDataCatalog.JobsListCatalog), {[jobUID]: admin.firestore.FieldValue.delete()}, {merge: true});
                batch.delete(admin.firestore().collection(CustomFirestoreCollectionDB.CollectionJobPostDetails).doc(jobUID));
                batch.delete(admin.firestore().collection(SharedFirestoreCollectionDB.PostsDetails).doc(jobUID)); // Remove also from Post Details in case it was shared
            }
        });
    }

    batch.commit().then((res) => response.send(getQuickResponse(true)))
        .catch((err) => response.send(getQuickResponse(false, err)));

});