import * as functions from "firebase-functions";
import { getQuickResponse } from "../../shareable-firebase-backend/model-data/api-response.model";
import { OnesignalUtils } from "../../shareable-firebase-backend/utils/onesignal.utils";


export const testFunction = functions.https.onRequest(
    async (req, resp) => {

        /*await OnesignalUtils.sendPushNotificationToUserJobFilters("🚀 New Remote Job Alert!", 
            `💼 Position: HR Implementation Coordinator
🏢 Company: Devire
🌍 Location: EU 
📁 Category: HR
💰 Salary: €30,000 - €50,000
🛠️ Skills: HR`, "https://www.remote-work.app/job/devire-hr-implementation-coordinator", {category: "HR", remoteRegion: "EU"}, 1);*/

        resp.send(getQuickResponse(true, "Push Notification Sent"));

    });
