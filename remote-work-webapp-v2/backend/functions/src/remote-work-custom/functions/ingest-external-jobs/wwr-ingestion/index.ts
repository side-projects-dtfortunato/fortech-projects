import * as admin from 'firebase-admin';
import * as functions from 'firebase-functions';
import {IngestDataWWR, IngestDataWWRByCategory} from "../../../ingest-data/ingest-data-wwr";
import {IngestJobOffersModel, IngestSourceData} from "../../../backend-data-model/ingest-job-offers.model";
import {getDocumentData} from "../../../../shareable-firebase-backend/utils/utils";
import {CustomFirestoreCollectionDB, CustomFirestoreDocsDB} from "../../../custom-firestore-collection-names";
import {getQuickResponse} from "../../../../shareable-firebase-backend/model-data/api-response.model";

export const ingestDataFromWWR = functions.https.onRequest(async (request,
                                                                  response) => {
    let diigestedListJobDoc: { [jobUid: string]: IngestJobOffersModel } = await getDocumentData(CustomFirestoreCollectionDB.ColIngestJobs, CustomFirestoreDocsDB.IngestListJobs.DocDigestedJobs);

    if (!diigestedListJobDoc) {
        diigestedListJobDoc = {};
    }
    let listIngestCategoriesLoad: string[] = [];
    if (request.query["jobCategory"]) {
        listIngestCategoriesLoad.push(request.query["jobCategory"] as string);
    } else {
        listIngestCategoriesLoad = Object.keys(IngestDataWWRByCategory);
    }

    let filteredListJobs: { [jobUid: string]: IngestJobOffersModel } = {};

    // Get list of jobs from WWR
    for (let i = 0; i < listIngestCategoriesLoad.length; i++) {
        let ingestSourceKey = listIngestCategoriesLoad[i];

        // Get the ingestion source
        let ingestSource: IngestSourceData = IngestDataWWRByCategory[ingestSourceKey];
        if (ingestSource) {

            let listJobs: { [jobUid: string]: IngestJobOffersModel } = await IngestDataWWR.getListJobs(ingestSource);

            if (listJobs) {
                Object.keys(listJobs).forEach((jobUid: string) => {
                    if (!diigestedListJobDoc[jobUid]) {
                        filteredListJobs[jobUid] = listJobs[jobUid];
                    }
                });

            }
        }
    }

    if (filteredListJobs && Object.keys(filteredListJobs).length > 0) {
        // Save data
        let dbResponse = await admin.firestore().collection(CustomFirestoreCollectionDB.ColIngestJobs).doc(CustomFirestoreDocsDB.IngestListJobs.DocIngestJobs).set(filteredListJobs, {merge: true});

        response.send(getQuickResponse(true, {dbResponse}));
        return;
    }

    response.send(getQuickResponse(false, null, "Didnt found any job to ingest"));
});
