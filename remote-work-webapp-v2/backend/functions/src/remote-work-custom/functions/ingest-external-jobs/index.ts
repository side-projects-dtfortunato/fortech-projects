import * as functions from "firebase-functions";
import * as admin from "firebase-admin";
import {IngestJobOffersModel, IngestSourceType} from "../../backend-data-model/ingest-job-offers.model";
import {getDocumentData} from "../../../shareable-firebase-backend/utils/utils";
import {JobPostCompanyDetailsModel, JobPostDetailsModel, JobPostUtils} from "../../data-model/job-post-details.model";
import {CustomFirestoreCollectionDB, CustomFirestoreDocsDB} from "../../custom-firestore-collection-names";
import {IngestDataWWR} from "../../ingest-data/ingest-data-wwr";
import {CustomUtils} from "../../custom-utils";
import {StorageFileModel} from "../../../shareable-firebase-backend/model-data/storage-file.model";
import {ImagekitHelper} from "../../../shareable-firebase-backend/utils/imagekit.helper";
import {getQuickResponse} from "../../../shareable-firebase-backend/model-data/api-response.model";
import {NotificationsQueueManager} from "../../../shareable-firebase-backend/utils/notifications-queue.manager";
import {CustomRoutesUtils} from "../../custom-routes.utils";


const EXPIRATION_TIME_30DAYS = 2592000000; // 30 days

export const digestJobFromListIngestedJobs = functions.https.onRequest(async (request,
                                                               response) => {
    let allJobsToDigest: { [jobUid: string]: IngestJobOffersModel } = await getDocumentData(CustomFirestoreCollectionDB.ColIngestJobs, CustomFirestoreDocsDB.IngestListJobs.DocIngestJobs);
    let digestedListJobs: { [jobUid: string]: IngestJobOffersModel } = await getDocumentData(CustomFirestoreCollectionDB.ColIngestJobs, CustomFirestoreDocsDB.IngestListJobs.DocDigestedJobs);
    let catalogCompanies: { [companyUid: string]: JobPostCompanyDetailsModel} = await getDocumentData(CustomFirestoreCollectionDB.CollectionDataCatalog, CustomFirestoreDocsDB.CollectionDataCatalog.CompaniesCatalog);

    if (!catalogCompanies) {
        catalogCompanies = {};
    }

    // Find 3 jobs to digest
    const numbJobsToDigest: number = 3;
    let listJobsToDigestNow: { [jobUid: string]: IngestJobOffersModel } = {};
    let listJobKeys: string[] = Object.keys(allJobsToDigest);
    for (let i = 0; i < listJobKeys.length && Object.keys(listJobsToDigestNow).length <= numbJobsToDigest; i++) {
        let jobUid: string = listJobKeys[i];
        // Check if doesn't exists yet on digested jobs
        if (!digestedListJobs || !digestedListJobs[jobUid]) {
            listJobsToDigestNow[jobUid] = allJobsToDigest[jobUid];
        }
    }

    // Data to update on DB
    let listJobsDetails: {[jobUid: string]: JobPostDetailsModel} = {};
    const batch = admin.firestore().batch();

    // Digest jobs
    if (listJobsToDigestNow) {
        let keys = Object.keys(listJobsToDigestNow);
        for (let i = 0; i < keys.length; i++) {
            let ingestedJobData: IngestJobOffersModel = listJobsToDigestNow[keys[i]];

            // Digest data
            let jobPostDetails: JobPostDetailsModel | undefined;

            // Digest from the expected source
            switch (ingestedJobData.ingestSource) {
                case IngestSourceType.WWR:
                    jobPostDetails = await IngestDataWWR.digestJobPost(ingestedJobData);
                    break;
            }

            if (jobPostDetails) {
                listJobsDetails[jobPostDetails.uid as string] = jobPostDetails;
            }
        }
    }

    let removeIngestedData: any = {};
    // Save on DB and check if should also create a new company details
    for (let i = 0; i < Object.values(listJobsDetails).length; i++) {
        let jobItem: JobPostDetailsModel = Object.values(listJobsDetails)[i];
        // Create a job details
        batch.set(admin.firestore().collection(CustomFirestoreCollectionDB.CollectionJobPostDetails).doc(jobItem.uid as string), {...jobItem}, {merge: true});

        // Remove from ingest list
        removeIngestedData = {
            ...removeIngestedData,
            [jobItem.uid!]: admin.firestore.FieldValue.delete(),
        }

        // Register to dispatch a notification
        await NotificationsQueueManager.addNotificationItemToQueue({
            uid: jobItem.uid!,
            description: JobPostUtils.generateSocialPost(jobItem),
            link: CustomRoutesUtils.getJobPostDetailsUrl(jobItem.uid!),
            title: `[Remote Job] ${jobItem.positionTitle} @${jobItem.companyDetails.companyName} (${jobItem.customZoneRestriction})`,
            pushNotification: true,
            bgImageUrl: JobPostUtils.generateJobPostThumbnailUrl(jobItem, "SQUARE"),
            socialNetworks: ["THREADS", "INSTAGRAM", "FACEBOOK", "REDDIT"],
            category: "",
            socialPost: JobPostUtils.generateSocialPost(jobItem),
            registerTime: Date.now(),
            instagramOnlyBgImage: true,
        });

        // Check if should also generate a Company Details based on the digested job
        if (!catalogCompanies || !catalogCompanies[jobItem.companyDetails.uid as string]) {
            let companyDetails: JobPostCompanyDetailsModel = jobItem.companyDetails;
            if (jobItem.remoteRegionsAllowance) {
                companyDetails.remoteRegionsAllowance = {};
                Object.keys(jobItem.remoteRegionsAllowance).forEach((regionKey) => {
                    if (jobItem.remoteRegionsAllowance![regionKey]) {
                        companyDetails.remoteRegionsAllowance![regionKey] = true;
                    }
                });
            }

            if (jobItem.customZoneRestriction) {
                companyDetails.hiringLocations = {
                    [jobItem.customZoneRestriction]: jobItem.customZoneRestriction,
                };
            }

            // Upload Company logo
            if (CustomUtils.isEnvProduction() && companyDetails.companyLogo && companyDetails.companyLogo.fileUrl) {
                let companyLogoStorage: StorageFileModel | undefined = await ImagekitHelper.uploadImageFromUrl(companyDetails.companyLogo.fileUrl, companyDetails.uid as string, "remote-work/companies", [companyDetails.companyName!]);

                if (companyLogoStorage) {
                    companyDetails.companyLogo = companyLogoStorage;
                }
            }

            batch.set(admin.firestore().collection(CustomFirestoreCollectionDB.CompanyDetails).doc(jobItem.companyDetails.uid as string), {...companyDetails}, {merge: true});

            // Update it on local to avoid repeated uploads
            catalogCompanies[jobItem.companyDetails.uid as string] = companyDetails;

        } else {
            // Update the company details from our DB
            jobItem.companyDetails = catalogCompanies[jobItem.companyDetails.uid as string];
        }

    }

    // Remove digested jobs
    batch.set(admin.firestore().collection(CustomFirestoreCollectionDB.ColIngestJobs).doc(CustomFirestoreDocsDB.IngestListJobs.DocIngestJobs), removeIngestedData, {merge: true});
    if (listJobsToDigestNow) {
        // Update digested collection
        batch.set(admin.firestore().collection(CustomFirestoreCollectionDB.ColIngestJobs).doc(CustomFirestoreDocsDB.IngestListJobs.DocDigestedJobs), listJobsToDigestNow, {merge: true});
    }

    // Commit changes
    let dbResponse = await batch.commit();

    response.send(getQuickResponse(true, {dbResponse, listJobsDetails, listJobsToDigestNow}));
    return;
});


export const cleanOldDigestedRegistryJobs = functions.https.onRequest(async (request,
                                                                              response) => {
    let digestedListJobs: { [jobUid: string]: IngestJobOffersModel } = await getDocumentData(CustomFirestoreCollectionDB.ColIngestJobs, CustomFirestoreDocsDB.IngestListJobs.DocDigestedJobs);

    let listJobsToClean: { [jobUid: string]: any } = {};

    if (digestedListJobs) {
        Object.keys(digestedListJobs).forEach((jobItemUid) => {
            let digestedJob: IngestJobOffersModel = digestedListJobs[jobItemUid];

            if (digestedJob && (digestedJob.ingestedDate + EXPIRATION_TIME_30DAYS) < Date.now()) {
                // Digested job has expired
                listJobsToClean[jobItemUid] = admin.firestore.FieldValue.delete();
            }
        });
    }

    if (listJobsToClean && Object.keys(listJobsToClean).length > 0) {
        let batch = admin.firestore().batch();

        batch.set(admin.firestore().collection(CustomFirestoreCollectionDB.ColIngestJobs).doc(CustomFirestoreDocsDB.IngestListJobs.DocDigestedJobs), listJobsToClean, {merge: true});

        const dbResponse = await batch.commit();

        response.send(getQuickResponse(true, {dbResponse, listJobsToClean}));
    } else {
        response.send(getQuickResponse(false, {}, "Nothing to update"));
    }

});