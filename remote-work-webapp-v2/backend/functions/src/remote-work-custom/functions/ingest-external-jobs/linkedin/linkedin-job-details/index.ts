import * as functions from "firebase-functions";
import {getQuickResponse} from "../../../../../shareable-firebase-backend/model-data/api-response.model";
import {
    ExternalLinkedinJobDataUtils
} from "../../../../data-model/external-datamodel/external-linkedin-job-details.model";
import {Log} from "../../../../../shareable-firebase-backend/utils/log";
import {JobPostDetailsModel} from "../../../../data-model/job-post-details.model";
// import {JobApplicationType, JobPostDetailsModel, JobPostSource} from "../../../../data-model/job-post-details.model";


export const importLinkedinJobPostDetails = functions.https.onCall(
    (data: {url: string, defaultJobCategory: string},
     context) => {
        const userId = context && context.auth && context.auth.uid ? context.auth.uid : "anonymous";
        Log.onCall(data, userId);

        return new Promise(async (resolve, reject) => {
            let importedJobDetails: JobPostDetailsModel | undefined = await ExternalLinkedinJobDataUtils.parseLinkedinJobJsonToJobPostDetails(data.url as string);
            if (importedJobDetails && data.defaultJobCategory) {
                importedJobDetails.jobCategory = data.defaultJobCategory;
                importedJobDetails.jobSkills.jobCategory = data.defaultJobCategory;
            }

            resolve(getQuickResponse(true, importedJobDetails));
        });
    });


export const exportLinkedinJobJsonData = functions.https.onRequest(async (request,
                                                                          response) => {

    // const url = "https://www.linkedin.com/jobs/view/100%25-remote-front-end-react-developer-at-summit-human-capital-3737987995?refId=BlH271kaSqNv4Z4F4P5R6A%3D%3D&trackingId=PVYMQeuTWXFhECcNiZqdaQ%3D%3D&trk=public_jobs_topcard-title";

    if (!request.query && !request.query["url"]) {
        response.send(getQuickResponse(false, null, "Queries not found"));
        return;
    }

    const url = request.query["url"]; //  "https://www.linkedin.com/jobs/view/front-end-react-developer-at-experient-group-3619514684";

    // response.send(getQuickResponse(true, {body: await urlResponse.text()}));

    /*const scrapeResponse = await scrapeIt(url, {
        title: "h1.top-card-layout__title",
        description: "div.decorated-job-posting__details div.show-more-less-html__markup",
    });

    response.send(getQuickResponse(true, {data: scrapeResponse.data}));*/


    response.send(getQuickResponse(true, await ExternalLinkedinJobDataUtils.exportLinkedinJobJsonData(url as string)));
});

/**
 * Only used for testing purposes
 */
export const parseLinkedinJobJsonData = functions.https.onRequest(async (request,
                                                                          response) => {

    // const url = "https://www.linkedin.com/jobs/view/100%25-remote-front-end-react-developer-at-summit-human-capital-3737987995?refId=BlH271kaSqNv4Z4F4P5R6A%3D%3D&trackingId=PVYMQeuTWXFhECcNiZqdaQ%3D%3D&trk=public_jobs_topcard-title";

    if (!request.query && !request.query["url"]) {
        response.send(getQuickResponse(false, null, "Queries not found"));
        return;
    }

    const url = request.query["url"]; //  "https://www.linkedin.com/jobs/view/front-end-react-developer-at-experient-group-3619514684";

    response.send(getQuickResponse(true, await ExternalLinkedinJobDataUtils.parseLinkedinJobJsonToJobPostDetails(url as string)));
});




/*const jobParsedData = {
        title: dom.window.document.querySelector(
            'h1.top-card-layout__title'
        )?.textContent,
        description: removePrefixSuffixWhitespaceAndBreakLines(dom.window.document.querySelector(
            'div.decorated-job-posting__details div.show-more-less-html__markup'
        )?.innerHTML),
        companyName: removePrefixSuffixWhitespaceAndBreakLines(dom.window.document.querySelector('h4.top-card-layout__second-subline a.topcard__org-name-link')?.textContent),
        locationName: removePrefixSuffixWhitespaceAndBreakLines(dom.window.document.querySelector('h4.top-card-layout__second-subline span.topcard__flavor--bullet')?.textContent),
        experience: removePrefixSuffixWhitespaceAndBreakLines(dom.window.document.querySelector('li.description__job-criteria-item span.description__job-criteria-text')?.innerHTML),
        json: removePrefixSuffixWhitespaceAndBreakLines(dom.window.document.querySelector<HTMLScriptElement>(
            'script[type="application/ld+json"]'
        )?.textContent),
    };*/