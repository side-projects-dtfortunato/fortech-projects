import * as admin from 'firebase-admin';
import * as functions from 'firebase-functions';
import axios from "axios";
import {
    JobDetailsModel
} from "../../../../shareable-firebase-backend/base-projects/jobs-discover/shared-jobs-discover.data";
import {
    JobApplicationType,
    JobPostDetailsModel,
    JobPostSource,
    JobPostStatus, JobPostUtils
} from "../../../data-model/job-post-details.model";
import {CustomFirestoreCollectionDB} from "../../../custom-firestore-collection-names";
import {NotificationsQueueManager} from "../../../../shareable-firebase-backend/utils/notifications-queue.manager";
import {CustomRoutesUtils} from "../../../custom-routes.utils";

const URL = "https://us-central1-remotejobshub-app.cloudfunctions.net/getLatestJobsFunction"

export const fetchRemoteJobsHubJobs = functions.https.onRequest(
    async (req, resp) => {
        const response = await axios.get(URL);


        // Get List Jobs
        let importedListJobs: JobDetailsModel[] = response.data;

        // Parse to RemoteWork.app Data model
        let parsedJobs: JobPostDetailsModel[] = [];

        if (importedListJobs.length > 0) {

            parsedJobs = importedListJobs.map((importedJobData) => {
                let mainSkills: string[] = [];
                mainSkills = mainSkills.concat(convertJobTitleToTags(importedJobData.title));
                if (!mainSkills.includes(importedJobData.category!)) {
                    mainSkills = [importedJobData.category!].concat(mainSkills);
                }

                let secondarySkills: string[] = [importedJobData.experienceLevel, importedJobData.remoteRegion!]
                // Split job title
                return {
                    uid: importedJobData.uid!,
                    createdAt: Date.now(),
                    sourceType: JobPostSource.LINKEDIN,
                    jobCategory: importedJobData.category,
                    jobSkills: {
                        mainSkills: mainSkills,
                        jobCategory: importedJobData.category,
                        secondarySkills: secondarySkills,
                    },
                    isFeature: false,
                    jobTags: mainSkills.concat(secondarySkills),
                    updatedAt: Date.now(),
                    companyDetails: {
                        companyName: importedJobData.company,
                        website: importedJobData.companyUrl,
                        companyLogo: {
                            fileUrl: importedJobData.companyLogo,
                            storagePath: "",
                        },
                        companyThumbnailUrl: importedJobData.companyThumbnailUrl,
                        companyLocation: {
                        }
                    },
                    positionTitle: importedJobData.title,
                    positionDescription: importedJobData.description,
                    descriptionContentType: "MARKDOWN",
                    stats: {
                        numApplications: 0,
                        numViews: 0,
                        appliedBtn: {},
                        views: {}
                    },
                    applicationMethod: {
                        applicationType: JobApplicationType.URL,
                        url: importedJobData.applicationUrl,
                    },
                    customZoneRestriction: importedJobData.remoteRegion,
                    publishedAt: Date.now(),
                    status: JobPostStatus.PUBLISHED,
                    sourceUrl: "https://linkedin.com",
                    authorization: {
                        allWrite: false,
                        allRead: true,
                    },
                }
            });

            // Let's add to the datastore
            const batch = admin.firestore().batch();
            for (let jobItem of parsedJobs) {
                // Create a job details
                batch.set(admin.firestore().collection(CustomFirestoreCollectionDB.CollectionJobPostDetails).doc(jobItem.uid as string), jobItem, {merge: true});

                // Register to dispatch a notification
                await NotificationsQueueManager.addNotificationItemToQueue({
                    uid: jobItem.uid!,
                    description: JobPostUtils.generateSocialPost(jobItem),
                    link: CustomRoutesUtils.getJobPostDetailsUrl(jobItem.uid!),
                    title: `${jobItem.positionTitle} @${jobItem.companyDetails.companyName} (${jobItem.customZoneRestriction} Only)`,
                    pushNotification: true,
                    bgImageUrl: JobPostUtils.generateJobPostThumbnailUrl(jobItem, "SQUARE"),
                    socialNetworks: ["THREADS", "INSTAGRAM", "FACEBOOK", "REDDIT"],
                    category: jobItem.jobCategory,
                    socialPost: JobPostUtils.generateSocialPost(jobItem),
                    registerTime: Date.now(),
                    instagramOnlyBgImage: true,
                });
            }
            await batch.commit();

        }

        resp.send(parsedJobs);
    });

/**
 * Converts a job title into a list of tags/skills with capitalized first letters.
 * @param title The job title to process.
 * @returns An array of unique tags/skills extracted from the title, with capitalized first letters.
 */
export function convertJobTitleToTags(title: string): string[] {
    // Step 1: Convert to lowercase and replace parentheses and other punctuation with spaces
    let processedTitle = title.toLowerCase().replace(/[()[\]{}.,;:!?]/g, ' ');

    // Step 2: Split the title into words, preserving hyphenated words
    const words = processedTitle.match(/\b[\w-]+\b/g) || [];

    // Step 3: Filter out common words and very short terms
    const commonWords = new Set([
        'and', 'or', 'the', 'a', 'an', 'in', 'on', 'at', 'for', 'to', 'of', 'with',
        'job', 'position', 'opening', 'vacancy', 'role', 'opportunity',
    ]);

    const filteredWords = words.filter(word =>
        !commonWords.has(word) && word.length > 2
    );

    // Step 4: Capitalize the first letter of each word
    const capitalizedWords = filteredWords.map(word =>
        word.charAt(0).toUpperCase() + word.slice(1)
    );

    // Step 5: Remove duplicates and return the result
    return Array.from(new Set(capitalizedWords));
}