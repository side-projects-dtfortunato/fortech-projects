import {JobPostDetailsModel, JobPostSource, JobPostStatus, JobPostUtils} from "../../data-model/job-post-details.model";
import {getQuickResponse} from "../../../shareable-firebase-backend/model-data/api-response.model";
import * as functions from "firebase-functions";
import * as admin from "firebase-admin";
import {Log} from "../../../shareable-firebase-backend/utils/log";
import {BackendSharedUtils} from "../../../shareable-firebase-backend/utils/backend-shared-utils";
import {CustomFirestoreCollectionDB, CustomFirestoreDocsDB} from "../../custom-firestore-collection-names";
import {RemoteWorkNicheSitesTypes} from "../../data-model/remote-work-niche-sites";
import {NotificationsQueueManager} from "../../../shareable-firebase-backend/utils/notifications-queue.manager";
import {CustomRoutesUtils} from "../../custom-routes.utils";

export const EXPIRATION_TIME_30DAYS = 2592000000;

export const postRemoteJob = functions.https.onCall(
    (data: {jobPostDetails: JobPostDetailsModel,
         availableNicheSites: RemoteWorkNicheSitesTypes[]},
     context) => {
        // if (!context.auth || !context.auth.uid) return getResponse(1001);
        const userId = context && context.auth && context.auth.uid ? context.auth.uid : "";
        Log.onCall(data, userId);

        if (!data.jobPostDetails) {
            return getQuickResponse(false, data, "Invalid data on the request");
        }

        return new Promise(async (resolve, reject) => {

            // Create Job post request
            const batch = admin.firestore().batch();

            if (data.jobPostDetails) {

                // Generate a static UID
                let uid = data.jobPostDetails.positionTitle;
                uid = uid + " " + data.jobPostDetails.companyDetails.companyName;
                uid = uid + "-" + Date.now();

                // lowerCase
                uid = uid.toLowerCase();

                uid = BackendSharedUtils.string_to_slug(uid);

                const jobPostRequestData: JobPostDetailsModel = {
                    ...data.jobPostDetails,
                    uid: uid,
                    status: JobPostStatus.PUBLISHED,
                    isFeature: false,
                    publishedAt: Date.now(),
                    sourceType: JobPostSource.ORIGINAL,
                    stats: {
                        numViews: 0,
                        numApplications: 0,
                    },
                    expirationTime: (Date.now() + EXPIRATION_TIME_30DAYS),
                    publisherUID: userId,
                    availableNicheSites: data.availableNicheSites,
                };

                // Update DB
                batch.create(admin.firestore().collection(CustomFirestoreCollectionDB.CollectionJobPostDetails).doc(jobPostRequestData.uid!), jobPostRequestData);
                // Update Aggregator with new job post
                batch.set(admin.firestore().collection(CustomFirestoreCollectionDB.CollectionDataCatalog).doc(CustomFirestoreDocsDB.CollectionDataCatalog.JobsListCatalog), {[jobPostRequestData.uid!]: jobPostRequestData}, {merge: true});


                // Register to dispatch a notification
                await NotificationsQueueManager.addNotificationItemToQueue({
                    uid: jobPostRequestData.uid!,
                    description: JobPostUtils.generateSocialPost(jobPostRequestData),
                    link: CustomRoutesUtils.getJobPostDetailsUrl(jobPostRequestData.uid!),
                    title: `[Remote Job] ${jobPostRequestData.positionTitle} @${jobPostRequestData.companyDetails.companyName} (${jobPostRequestData.customZoneRestriction})`,
                    pushNotification: true,
                    bgImageUrl: JobPostUtils.generateJobPostThumbnailUrl(jobPostRequestData, "SQUARE"),
                    socialNetworks: ["THREADS", "INSTAGRAM", "FACEBOOK", "REDDIT"],
                    category: "",
                    socialPost: JobPostUtils.generateSocialPost(jobPostRequestData),
                    registerTime: Date.now(),
                    instagramOnlyBgImage: true,
                });
            }


            batch.commit()
                .then(async (res) => {
                    resolve(getQuickResponse(true));
                }).catch((err) => resolve(getQuickResponse(false, null, err.toString())));

        });

    });