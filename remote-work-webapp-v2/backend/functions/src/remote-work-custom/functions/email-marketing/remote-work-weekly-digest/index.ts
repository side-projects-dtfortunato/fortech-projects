import * as functions from "firebase-functions";
import {
    SharedNewsletterUserModel
} from "../../../../shareable-firebase-backend/model-data/frontend-model-data/shared-newsletter-user.model";
import {getDocumentData} from "../../../../shareable-firebase-backend/utils/utils";
import {
    SharedFirestoreCollectionDB,
    SharedFirestoreDocsDB
} from "../../../../shareable-firebase-backend/model-data/shared-firestore-collections";
import {getQuickResponse} from "../../../../shareable-firebase-backend/model-data/api-response.model";
import {
    BasePostDataModel
} from "../../../../shareable-firebase-backend/model-data/frontend-model-data/base-post-data.model";
import {
    EmailTemplateDigestUtils,
    ListItemParams,
    ListItemsContainer
} from "../../../../shareable-firebase-backend/utils/email-utils/email-template-digest-v2.utils";
import {WEBAPP_CONFIGS} from "../../../../configs/webapp.configs";
import {EmailDestinationModel} from "../../../../shareable-firebase-backend/utils/email-utils/email-sender-utils";
import {RealtimeDbUtils} from "../../../../shareable-firebase-backend/utils/realtime-db.utils";
import {JobPostListInfoModel} from "../../../data-model/job-post-details.model";
import {CustomFirestoreCollectionDB, CustomFirestoreDocsDB} from "../../../custom-firestore-collection-names";
import {BackendSharedUtils} from "../../../../shareable-firebase-backend/utils/backend-shared-utils";
import {PostShareRemoteJobType} from "../../../data-model/posts-model/post-share-remote-job.model";
import {EmailBulkDeliveryModel} from "../../../../shareable-firebase-backend/model-data/email-builk-delivery.model";
import {RemoteWorkNicheSitesTypes} from "../../../data-model/remote-work-niche-sites";

const BASE_URL = WEBAPP_CONFIGS.WEBSITE_URL;

export const generateEmailWeeklyDigest = functions.https.onRequest(async (request,
                                                                          response) => {

    // Get subscribed users
    let subscribedUsers: { [userId: string]: SharedNewsletterUserModel } | undefined = await RealtimeDbUtils.getData({
        collectionName: SharedFirestoreCollectionDB.NewsletterCatalog,
        docId: SharedFirestoreDocsDB.NewsletterCatalog.SubscribedUsers,
    });

    if (!subscribedUsers) {
        response.send(getQuickResponse(false, null, "No subscribers found"));
        return;
    }

    // Generate Digest items
    let listDigestPosts: ListItemsContainer[] = [];


    // Generate list posts
    let listPosts: { [postId: string]: BasePostDataModel } = await getDocumentData<{ [postId: string]: BasePostDataModel }>(SharedFirestoreCollectionDB.DataCatalog, SharedFirestoreDocsDB.DataCatalog.FilterRecent);
    let listRecentPostsEmailItems: ListItemParams[] = Object.values(listPosts)
        .filter((post) => {
            return post.postType !== PostShareRemoteJobType && BackendSharedUtils.isLessThanHoursAgo(post.publishedAt!, 24 * 7);
        })
        .sort((p1, p2) => p2.rankPts - p1.rankPts)
        .map((postItem) => {
            return EmailTemplateDigestUtils.generateListItemPost(postItem)
        });

    if (listRecentPostsEmailItems && listRecentPostsEmailItems.length > 0) {
        listDigestPosts.push({
            title: "Recent Posts",
            listItems: listRecentPostsEmailItems,
        });
    }

    // Generate Recent Jobs
    let listJobs: { [jobId: string]: JobPostListInfoModel } = await getDocumentData(CustomFirestoreCollectionDB.CollectionDataCatalog, CustomFirestoreDocsDB.CollectionDataCatalog.JobsListCatalog);

    if (listJobs) {
        let listRecentJobs: ListItemParams[] = Object.values(listJobs).filter((jobItem) => {
            return BackendSharedUtils.isLessThanHoursAgo(jobItem.publishedAt!, 24 * 7);
        }).sort((job1, job2) => job2.publishedAt! - job1.publishedAt!)
            .map((jobItem) => {
                return {
                    title: jobItem.positionTitle,
                    readMoreUrl: BASE_URL + "/remote-jobs/" + jobItem.uid!,
                    postedOn: new Date(jobItem.publishedAt!).toLocaleDateString(),
                    subtitle: jobItem.companyDetails.companyName,
                    summary: "📍 " + jobItem.customZoneRestriction ? jobItem.customZoneRestriction : "WORLDWIDE",
                    imageUrl: jobItem.companyDetails.companyLogo.fileUrl,
                }
            });

        if (listRecentJobs && listRecentJobs.length > 0) {
            listDigestPosts.push({
                title: "Recent Remote Jobs",
                listItems: listRecentJobs,
            });
        }
    }

    if (listDigestPosts.length === 0) {
        response.send(getQuickResponse(true, null, "Nothing to send"));
    }


    if (subscribedUsers && Object.keys(subscribedUsers).length > 0) {
        if (listDigestPosts && listDigestPosts.length >= 1) {
            // Send email
            const htmlContent: string = EmailTemplateDigestUtils.generateHtmlContent(
                {
                    logoUrl: WEBAPP_CONFIGS.LOGO_URL,
                    siteUrl: WEBAPP_CONFIGS.WEBSITE_URL,
                    headerTitle: "Weekly Digest",
                    headerSubtitle: "Here are the most recent jobs and posts published recently",
                    listItemsContainer: listDigestPosts,
                    footer: {
                        address: WEBAPP_CONFIGS.COMPANY_ADDRESS,
                        email: WEBAPP_CONFIGS.COMPANY_EMAIL,
                        unsubscribeLink: WEBAPP_CONFIGS.UNSUBSCRIBE_URL,
                    },
                    listSocialNetworks: [
                        {
                            link: "https://medium.com/@journeypreneur",
                            iconUrl: "https://www.iconpacks.net/icons/2/free-medium-icon-2177-thumb.png",
                        },
                        {
                            link: "https://twitter.com/journeypreneur_",
                            iconUrl: "https://cdn-icons-png.flaticon.com/512/124/124021.png",
                        },
                    ],
                }
            );

            if (subscribedUsers) {
                let destionationsList: EmailDestinationModel[] = Object.values(subscribedUsers)
                    .filter((subscribedUser) => {
                        return subscribedUser.userId && subscribedUser.email && (!subscribedUser.nicheSitesLoggedIn || subscribedUser.nicheSitesLoggedIn.includes(RemoteWorkNicheSitesTypes.REMOTE_WORK))
                    }).map((subscriptionItem) => {
                        return {
                            uid: subscriptionItem.userId,
                            email: subscriptionItem.email,
                            name: subscriptionItem.name ? subscriptionItem.name : subscriptionItem.email,
                        };
                    });

                // Store to later be delivered
                await RealtimeDbUtils.writeData({
                    collectionName: SharedFirestoreCollectionDB.RTEmailBulkToDeliver,
                    docId: Date.now().toString(),
                    data: {
                        bodyHtml: htmlContent,
                        destinationsEmail: destionationsList,
                        subject: "Weekly " + WEBAPP_CONFIGS.SITE_NAME + " Digest",
                        sentFrom: {
                            name: WEBAPP_CONFIGS.SITE_NAME,
                            email: "no-reply@" + WEBAPP_CONFIGS.SITE_NAME,
                        },
                        createdAt: Date.now(),
                    } as EmailBulkDeliveryModel,
                    merge: false,
                });
                response.send(getQuickResponse(true, null, "Weekly Digest planned to dispatch"));

                /*MailerSendUtils.sendEmail({
                    bodyHtml: htmlContent,
                    subject: "Daily " + WEBAPP_CONFIGS.SITE_NAME + " Digest",
                    sentFrom: {
                        name: WEBAPP_CONFIGS.SITE_NAME,
                        email: "no-reply@" + WEBAPP_CONFIGS.SITE_NAME,
                    },
                    destinationsEmail: destionationsList,
                }).then(async (res) => {
                    response.send(getQuickResponse(true, {
                        res,
                        destinationsCounter: destionationsList.length,
                    }));
                }).catch((err) => {
                    response.send(err);
                });*/
            } else {
                response.send(getQuickResponse(false, null, "Not enough posts to notify"));
                return;
            }

        } else {
            response.send(getQuickResponse(false, null, "Didn't found enough digested posts"));
            return;
        }


    } else {
        response.send(getQuickResponse(false, null, "No users to send digest"));
    }

});