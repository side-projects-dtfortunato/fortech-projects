import * as functions from "firebase-functions";
import * as admin from "firebase-admin";
import {getQuickResponse} from "../../../../shareable-firebase-backend/model-data/api-response.model";
import {CustomFirestoreCollectionDB} from "../../../custom-firestore-collection-names";
import {Log} from "../../../../shareable-firebase-backend/utils/log";

export const requestJobPostedVisited = functions.https.onCall(
    (data: {jobPostId: string, updateType: "view" | "applied"},
     context) => {
        const userId = context && context.auth && context.auth.uid ? context.auth.uid : "anonymous";
        Log.onCall(data, userId);


        return new Promise(async (resolve, reject) => {

            let batch = admin.firestore().batch();

            // add new data
            let dataToUpdate: any = {};
            if (data.updateType === "view") {
                dataToUpdate = {
                    stats: {
                        numViews: admin.firestore.FieldValue.increment(1),
                        views: {
                            [Date.now().toString()]: userId,
                        },
                    },
                };
            } else if (data.updateType === "applied") {
                dataToUpdate = {
                    stats: {
                        numApplications: admin.firestore.FieldValue.increment(1),
                        appliedBtn: {
                            [Date.now().toString()]: userId,
                        },
                    },
                };
            }

            if (dataToUpdate) {
                batch.set(admin.firestore().collection(CustomFirestoreCollectionDB.CollectionJobPostDetails).doc(data.jobPostId), dataToUpdate, {merge: true});

                batch.commit().then((res) => {
                    resolve(getQuickResponse(true, {res, dataToUpdate}));
                    return;
                }).catch((err) => {
                    resolve(getQuickResponse(false, {err, dataToUpdate}));
                    return;
                });
            } else {
                resolve(getQuickResponse(false, null, "Nothing to update"));
            }
        });
    });