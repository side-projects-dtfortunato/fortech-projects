import * as functions from "firebase-functions";
import * as admin from "firebase-admin";
import {CustomFirestoreCollectionDB, CustomFirestoreDocsDB} from "../../../custom-firestore-collection-names";
import {JobPostDetailsModel, JobPostListInfoModel} from "../../../data-model/job-post-details.model";
import {getQuickResponse} from "../../../../shareable-firebase-backend/model-data/api-response.model";
import {Log} from "../../../../shareable-firebase-backend/utils/log";

export const onJobPostDelete = functions
    .firestore
    .document(CustomFirestoreCollectionDB.CollectionJobPostDetails + '/{id}')
    .onDelete((data, context) => {
        Log.onDelete(data);
        let jobPostDetailsModel: JobPostDetailsModel;

        if (data && data.data()) {
            jobPostDetailsModel = {
                ...data.data() as JobPostDetailsModel,
                uid: data.id,
            };
        }

        return new Promise(async (resolve, reject) => {

            // Update Remote Jobs from catalogs
            const batch = admin.firestore().batch();

            // Remove from jobs catalog
            batch
                .set(admin.firestore().collection(CustomFirestoreCollectionDB.CollectionDataCatalog).doc(CustomFirestoreDocsDB.CollectionDataCatalog.JobsListCatalog), {
                    [jobPostDetailsModel.uid!]: admin.firestore.FieldValue.delete(),
                }, {merge: true});

            const dbResponse = await batch.commit();
            resolve(getQuickResponse(true, {dbResponse}));
        });
    });


export const onJobPostCreate = functions
    .firestore
    .document(CustomFirestoreCollectionDB.CollectionJobPostDetails + '/{id}')
    .onWrite((data, context) => {
        Log.onWrite(data);
        let jobPostDetailsModel: JobPostDetailsModel;

        if (data && data.after && data.after.data()) {
            jobPostDetailsModel = {
                ...data.after.data() as JobPostDetailsModel,
                uid: data.after.id,
            };
        } else {
            return;
        }

        return new Promise(async (resolve, reject) => {

            let batch = admin.firestore().batch();


            // Generate List Job Offer entity
            let listJobPostData: JobPostListInfoModel = {
                uid: jobPostDetailsModel.uid,
                createdAt: jobPostDetailsModel.createdAt,
                jobCategory: jobPostDetailsModel.jobCategory,
                jobSkills: jobPostDetailsModel.jobSkills,
                companyDetails: jobPostDetailsModel.companyDetails,
                positionTitle: jobPostDetailsModel.positionTitle,
                publishedAt: jobPostDetailsModel.publishedAt,
                updatedAt: jobPostDetailsModel.updatedAt,
                remoteRegionsAllowance: jobPostDetailsModel.remoteRegionsAllowance,
                customZoneRestriction: jobPostDetailsModel.customZoneRestriction,
                availableNicheSites: jobPostDetailsModel.availableNicheSites,
                originalNicheSitePublished: jobPostDetailsModel.originalNicheSitePublished,
            };

            // Add Job to the catalog from jobs catalog
            batch
                .set(admin.firestore().collection(CustomFirestoreCollectionDB.CollectionDataCatalog).doc(CustomFirestoreDocsDB.CollectionDataCatalog.JobsListCatalog), {
                    [jobPostDetailsModel.uid!]: listJobPostData,
                }, {merge: true});


            await batch.commit();

            /*
            // Deprecated:  Now is being used the NotificationQueueManager
            // const isCreated: boolean = !data.before.exists && data.after.exists;
            // Send Push Notification
            if (isCreated) {
                const jobPostUrl: string = CustomRoutesUtils.getJobPostDetailsUrl(jobPostDetailsModel.uid!);
                console.log("jobPostUrl: ", jobPostUrl);
                await OnesignalUtils
                .sendPushNotification("New Remote Job Published", jobPostDetailsModel.positionTitle, jobPostUrl);

                // Publish on social networks
                await JobPostUtils.socialNetworkPublish(jobPostDetailsModel);
            }*/

            resolve(getQuickResponse(true));

        });
    });