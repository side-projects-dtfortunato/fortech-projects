import * as functions from "firebase-functions";
import * as admin from "firebase-admin";
import {Log} from "../../../../shareable-firebase-backend/utils/log";
import {getQuickResponse, getResponse} from "../../../../shareable-firebase-backend/model-data/api-response.model";
import {JobPostDetailsModel, JobPostListInfoModel, JobPostUtils} from "../../../data-model/job-post-details.model";
import {getDocumentData} from "../../../../shareable-firebase-backend/utils/utils";
import {CustomFirestoreCollectionDB} from "../../../custom-firestore-collection-names";
import {
    BasePostDataModel, PostDataUtils
} from "../../../../shareable-firebase-backend/model-data/frontend-model-data/base-post-data.model";
import {
    SharedCentralUserDataModel
} from "../../../../shareable-firebase-backend/model-data/frontend-model-data/shared-central-user-data.model";
import {
    SharedFirestoreCollectionDB, SharedFirestoreDocsDB
} from "../../../../shareable-firebase-backend/model-data/shared-firestore-collections";
import {
    UserProfileUtils
} from "../../../../shareable-firebase-backend/model-data/frontend-model-data/shared-public-userprofile.model";
import {PostShareRemoteJobType} from "../../../data-model/posts-model/post-share-remote-job.model";

export const publishRemoteJobFeed = functions.https.onCall(
    (data: {jobPostId: string},
     context) => {
        if (!context.auth || !context.auth.uid) return getResponse(1001, undefined, "User must be logged in to execute this action");
        const userId: string = context && context.auth && context.auth.uid ? context.auth.uid : "";
        Log.onCall(data, userId);

        return new Promise(async (resolve, reject) => {

            // Get job details
            let jobPostDetails: JobPostDetailsModel = await getDocumentData(CustomFirestoreCollectionDB.CollectionJobPostDetails, data.jobPostId);

            if (!jobPostDetails) {
                resolve(getQuickResponse(false, null, "Job post not found"));
            }
            let jobPostListInfos: JobPostListInfoModel = JobPostUtils.convertJobPostDetailsToJobPostListInfos(jobPostDetails!);

            // Community Post
            let centralUserData: SharedCentralUserDataModel | undefined = await getDocumentData<SharedCentralUserDataModel>(SharedFirestoreCollectionDB.CentralUserData, userId);

            if (!centralUserData) {
                resolve(getQuickResponse(false, null, "User data not found"));
                return;
            }

            if (!centralUserData.publicProfile || !centralUserData.publicProfile.username || centralUserData.publicProfile.username.length === 0) {
                resolve(getQuickResponse(false, null, "You need to set a username to publish a post"));
                return;
            }

            let tags: string[] = [];
            if (jobPostDetails?.jobSkills.mainSkills) {
                tags = tags.concat(jobPostDetails?.jobSkills.mainSkills);
            }
            if (jobPostDetails?.jobSkills.secondarySkills) {
                tags = tags.concat(jobPostDetails?.jobSkills.secondarySkills);
            }

            // Initialize Post Data
            const postCategory: string = "Remote Job";
            let fullPostData: BasePostDataModel = {
                uid: data.jobPostId,
                responses: {
                    reactions: {},
                    comments: {},
                },
                listUsersInfos: {
                    [userId]: UserProfileUtils.generateUserShortProfile(centralUserData.publicProfile),
                },
                updatedAt: Date.now(),
                publishedAt: Date.now(),
                userCreatorId: userId,
                openViews: 0,
                rankPts: 0,
                numComments: 0,
                numReactions: 0,
                category: postCategory,
                detailsPostData: jobPostDetails,
                shortenPostData: jobPostListInfos,
                postType: PostShareRemoteJobType,
                createdAt: Date.now(),
                tags: tags,
                catalogDocs: [
                    SharedFirestoreDocsDB.DataCatalog.FilterRecent,
                    SharedFirestoreDocsDB.DataCatalog.PrefixCategoryType + postCategory,
                    SharedFirestoreDocsDB.DataCatalog.PrefixPostType + PostShareRemoteJobType]
            }

            const batch = admin.firestore().batch();

            // Add data in the Posts Collection
            batch.set(admin.firestore().collection(SharedFirestoreCollectionDB.PostsDetails).doc(fullPostData.uid!), {...fullPostData}, {merge: true});

            // Add Posts by tag
            if (fullPostData.tags) {
                let popularTagsIncrement: any = {};
                fullPostData.tags.forEach((tag) => {
                    popularTagsIncrement = {
                        ...popularTagsIncrement,
                        [tag.toLowerCase()]: admin.firestore.FieldValue.increment(1),
                    };
                    batch.set(admin.firestore().collection(SharedFirestoreCollectionDB.DataCatalog).doc(SharedFirestoreDocsDB.DataCatalog.PrefixTagType + tag.toLowerCase()), {[fullPostData.uid!]: PostDataUtils.convertPostToShortData(fullPostData)}, {merge: true});
                });
                batch.set(admin.firestore().collection(SharedFirestoreCollectionDB.DataCatalog).doc(SharedFirestoreDocsDB.DataCatalog.PopularTags), popularTagsIncrement, {merge: true});
            }

            const dbResponse = await batch.commit();
            resolve(getQuickResponse(true, {dbResponse}));
        });
    });