import {BaseModel} from "../../shareable-firebase-backend/model-data/base.model";

export enum IngestSourceType {WWR = "WWR"}

export interface IngestSourceData {
    jobCategory: string,
    url: string,
}

export interface IngestJobOffersModel extends BaseModel {
    jobTitle: string;
    originalJobUrl: string;
    jobCategory: string;
    companyName: string;
    ingestedDate: number;
    remoteLocation: string;
    ingestSource: IngestSourceType;
}

export interface DigestedJobOfferModel extends IngestJobOffersModel {
    digestedDate: number; // timestamp
}