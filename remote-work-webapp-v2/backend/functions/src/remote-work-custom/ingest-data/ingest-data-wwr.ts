const scrapeit = require("scrape-it")

import {IngestJobOffersModel, IngestSourceData, IngestSourceType} from "../backend-data-model/ingest-job-offers.model";
import {BackendSharedUtils} from "../../shareable-firebase-backend/utils/backend-shared-utils";
import {
    JobApplicationType,
    JobPostDetailsModel,
    JobPostSource,
    JobPostUtils, RemoteZoneRestriction
} from "../data-model/job-post-details.model";
import {RemoteWorkNicheSitesUtils} from "../data-model/remote-work-niche-sites";

export const IngestDataWWRByCategory: {[categoryId: string]: IngestSourceData} = {
    fullstack: {
        jobCategory: "fullstack",
        url: "https://weworkremotely.com/categories/remote-full-stack-programming-jobs#job-listings",
    },
    frontend: {
        jobCategory: "frontend",
        url: "https://weworkremotely.com/categories/remote-front-end-programming-jobs#job-listings",
    },
    backend: {
        jobCategory: "backend",
        url: "https://weworkremotely.com/categories/remote-back-end-programming-jobs#job-listings",
    },
    design: {
        jobCategory: "design",
        url: "https://weworkremotely.com/categories/remote-design-jobs#job-listings",
    },
    devops: {
        jobCategory: "devops",
        url: "https://weworkremotely.com/categories/remote-devops-sysadmin-jobs#job-listings",
    },
    management_finance: {
        jobCategory: "management_finance",
        url: "https://weworkremotely.com/categories/remote-management-and-finance-jobs#job-listings",
    },
    product: {
        jobCategory: "product",
        url: "https://weworkremotely.com/categories/remote-product-jobs",
    },
    customer_support: {
        jobCategory: "customer_support",
        url: "https://weworkremotely.com/categories/remote-customer-support-jobs#job-listings",
    },
    salesmarketing: {
        jobCategory: "salesmarketing",
        url: "https://weworkremotely.com/categories/remote-sales-and-marketing-jobs#job-listings",
    },
};

export class IngestDataWWR {

    static digestJobPost(ingestedJobPost: IngestJobOffersModel): Promise<JobPostDetailsModel | undefined>  {
        return new Promise(async (resolve, reject) => {
            let jobPostDetail: JobPostDetailsModel | undefined;

            let url: string = ingestedJobPost.originalJobUrl;
            if (!url.startsWith("https://")) {
                url = "https://weworkremotely.com" + ingestedJobPost.originalJobUrl;
            }

            let response: any = await scrapeit(url, {
                tags: {
                    listItem: "div.listing-header-container span.listing-tag",
                },
                description: {
                    selector: "section.job > div.listing-container",
                    how: "html",
                },
                companyData: {
                    selector: "div.company-card",
                    data: {
                        companyLogo: {
                            selector: "div.listing-logo img",
                            attr: "src",
                        },
                        companyLocation: {
                            selector: "h3",
                            eq: 0,
                        },
                        companyWebsite: {
                            selector: "h3 a",
                            attr: "href",
                        },
                        applicationUrl: {
                            selector: "a#job-cta-alt-2",
                            attr: "href",
                        },
                    },
                },
            });

            console.log("IngestDataWWR.digestJobPost.response:", response.data);

            if (response.data
                && response.data.tags
                && response.data.description
                && response.data.companyData
                && response.data.companyData.applicationUrl) {
                let companyUid: string = BackendSharedUtils.string_to_slug(ingestedJobPost.companyName.toLowerCase());
                jobPostDetail = {
                    uid: ingestedJobPost.uid,
                    companyDetails: {
                        uid: companyUid,
                        companyLogo: {
                            fileUrl: response.data.companyData.companyLogo as string,
                            storagePath: "",
                        },
                        companyLocation: {
                            cityName: response.data.companyData.companyLocation as string,
                        },
                        companyName: ingestedJobPost.companyName,
                        website: response.data.companyData.companyWebsite as string,
                        companyEmail: "",

                    },
                    positionTitle: ingestedJobPost.jobTitle,
                    jobCategory: ingestedJobPost.jobCategory,
                    customZoneRestriction: ingestedJobPost.remoteLocation,
                    jobSkills: {
                        mainSkills: [ingestedJobPost.jobCategory.toUpperCase()],
                        secondarySkills: response.data.tags as string[],
                        jobCategory: ingestedJobPost.jobCategory,
                    },
                    applicationMethod: {
                        applicationType: JobApplicationType.URL,
                        url: (response.data.companyData.applicationUrl as string).replace("weworkremotely", "remote-work_app"),
                    },
                    isFeature: false,
                    positionDescription: response.data.description as string,
                    publishedAt: Date.now(),
                    sourceType: JobPostSource.ORIGINAL,
                    sourceUrl: "https://remote-work.app",
                    stats: {
                        numApplications: 0,
                        numViews: 0,
                    },
                }

                // Update regions allowance
                jobPostDetail = this.convertCustomLocation(jobPostDetail);
                // Generate new custom restricted location
                jobPostDetail = JobPostUtils.generateCustomZoneRestrictionLabel(jobPostDetail);

                // Fill the list of available niche sites
                jobPostDetail.availableNicheSites = RemoteWorkNicheSitesUtils.generateJobListAvailableNicheSites(jobPostDetail);
            }

            resolve(jobPostDetail);
        });
    }

    static convertCustomLocation(jobPostDetail: JobPostDetailsModel) {
        let remoteRegionsAllowance: {
            [region: string]: boolean;
        } = {};

        if (jobPostDetail.customZoneRestriction) {
            const customLocation: string = jobPostDetail.customZoneRestriction.toLowerCase();


            // Check if anywhere in the world
            if (customLocation.includes("anywhere in the world")) {
                remoteRegionsAllowance = {
                    [RemoteZoneRestriction.WORLDWIDE.toString()]: true,
                };
            } else {
                // Region by region
                if (customLocation.includes("europe") || customLocation.includes("emea")) {
                    remoteRegionsAllowance[RemoteZoneRestriction.EUROPE.toString()] = true;
                }
                if (customLocation.includes("americas") || customLocation.includes("usa")) {
                    remoteRegionsAllowance[RemoteZoneRestriction.US.toString()] = true;
                }
                if (customLocation.includes("americas") || customLocation.includes("north america") || customLocation.includes("canada")) {
                    remoteRegionsAllowance[RemoteZoneRestriction.CANADA.toString()] = true;
                }
                if (customLocation.includes("asia")) {
                    remoteRegionsAllowance[RemoteZoneRestriction.ASIA.toString()] = true;
                }
                if (customLocation.includes("uk")) {
                    remoteRegionsAllowance[RemoteZoneRestriction.UK.toString()] = true;
                }
                if (customLocation.includes("oceania")) {
                    remoteRegionsAllowance[RemoteZoneRestriction.AUSTRALIA.toString()] = true;
                }
                if (customLocation.includes("latin america")) {
                    remoteRegionsAllowance[RemoteZoneRestriction.SOUTH_AMERICA.toString()] = true;
                }
            }

            // Update remote regions allowance
            jobPostDetail.remoteRegionsAllowance = remoteRegionsAllowance;
        }
        return jobPostDetail;
    }


    static getListJobs(ingestSource: IngestSourceData): Promise<{[jobUid: string]: IngestJobOffersModel}> {
        return new Promise(async (resolve, reject) => {
            let listIngestJobs: {[jobUid: string]: IngestJobOffersModel} = {};

            let response: any = await scrapeit(ingestSource.url, {
                listJobs: {
                    listItem: "section.jobs li > a",
                    data: {
                        title: {
                            selector: "span.title",
                        },
                        companyName: {
                            selector: "span.company",
                            eq: 0,
                        },
                        location: {
                            selector: "span.region",
                        },
                        link: {
                            attr: "href",
                        },
                    },
                },
            });

            console.log("response.data: ", response.data);

            if (response.data && response.data.listJobs) {
                let ingestedDate = new Date().getTime();
                response.data.listJobs.forEach((ingestItemData: any) => {
                    if (ingestItemData.title
                        && ingestItemData.companyName
                        && ingestItemData.location
                        && ingestItemData.link) {

                        let companyId: string = BackendSharedUtils.string_to_slug(ingestItemData.companyName);
                        let positionId: string = BackendSharedUtils.string_to_slug(ingestItemData.title);

                        let uid: string = companyId + "+" + ingestSource.jobCategory + "+" + positionId;
                        listIngestJobs[uid] = {
                            uid: uid,
                            companyName: ingestItemData.companyName,
                            ingestedDate: ingestedDate,
                            jobCategory: ingestSource.jobCategory,
                            jobTitle: ingestItemData.title,
                            originalJobUrl: ingestItemData.link,
                            remoteLocation: ingestItemData.location,
                            ingestSource: IngestSourceType.WWR,
                        };
                    }
                })
            }

            resolve(listIngestJobs);
            return;
        });
    }
}