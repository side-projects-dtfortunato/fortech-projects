import {LocationModel} from "../../shareable-firebase-backend/model-data/location.model";
import {StorageFileModel} from "../../shareable-firebase-backend/model-data/storage-file.model";
import {BaseModel} from "../../shareable-firebase-backend/model-data/base.model";

export interface CatalogCompanyEntity extends BaseModel {
    companyLocation: LocationModel;
    companyName?: string;
    companyLogo: StorageFileModel;
    companyThumbnailUrl?: string;
    hiringLocations?: {[locationId: string]: string};
    remoteRegionsAllowance?: {[regionKey: string]: boolean};
    industries?: string[];
    website?: string;
}
