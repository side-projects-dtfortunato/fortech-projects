export interface JobSkillModel {
    skill: string;
    expYears?: string;
}