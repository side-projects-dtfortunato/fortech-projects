import {BaseModel} from "../../shareable-firebase-backend/model-data/base.model";
import {CatalogCompanyEntity} from "./company-entity.model";
import {RemoteWorkNicheSitesTypes} from "./remote-work-niche-sites";
import { createCanvas, loadImage, CanvasRenderingContext2D } from 'canvas';
import {CustomRoutesUtils} from "../custom-routes.utils";
import {
    SocialNetworksPostUtils
} from "../../shareable-firebase-backend/base-projects/social-networks-autopost/SocialNetworksPost.utils";
import {BackendSharedUtils} from "../../shareable-firebase-backend/utils/backend-shared-utils";
import {WEBAPP_CONFIGS} from "../../configs/webapp.configs";


export enum RemoteZoneRestriction {
    WORLDWIDE = "WORLDWIDE",
    EUROPE = "EUROPE",
    ASIA = "ASIA",
    US = "US", UK = "UK", CANADA = "CANADA", SOUTH_AMERICA = "SOUTH_AMERICA", AFRICA = "AFRICA", AUSTRALIA = "AUSTRALIA", UNKNOWN = "UNKNOWN"
}

export enum JobPostStatus {
    DRAFT = "DRAFT",
    PENDING_PAYMENT = "PENDING_PAYMENT",
    PENDING_APPROVE = "PENDING_APPROVE",
    PUBLISHED = "PUBLISHED",
    EXPIRED = "EXPIRED",
    CANCELLED = "CANCELLED"
}

export enum JobExperienceLevel {
    ENTRY = "ENTRY",
    JUNIOR = "JUNIOR",
    MID = "MID",
    SENIOR = "SENIOR",
}

export enum JobApplicationType { URL= "URL", EMAIL = "EMAIL" }

export enum JobPostSource { ORIGINAL = "ORIGINAL", REMOTEOK = "REMOTEOK", WWR = "WWR", LINKEDIN = "LINKEDIN" }

export enum JobCategories {fullstack = "fullstack", frontend = "frontend", backend = "backend", design = "design", devops = "devops", management_finance = "management_finance", product = "product", customer_support = "customer_support", salesmarketing = "salesmarketing", Other = "Other"}

export interface JobPostListInfoModel extends BaseModel {
    companyDetails: JobPostCompanyDetailsModel;
    positionTitle: string;
    jobSkills: JobSkills;
    remoteRegionsAllowance?: {
        [region: string]: boolean,
    };
    jobTags?: string[];
    publishedAt?: number;
    jobCategory?: string;
    customZoneRestriction?: string;
    experienceLevel?: JobExperienceLevel;
    availableNicheSites?: RemoteWorkNicheSitesTypes[];
    originalNicheSitePublished?: RemoteWorkNicheSitesTypes;
}

export interface JobPostDetailsModel extends JobPostListInfoModel {
    positionDescription?: string;
    descriptionContentType?: "HTML" | "MARKDOWN";
    expirationTime?: number;
    isFeature?: boolean;
    applicationMethod: JobApplicationMethod;
    sourceUrl: string;
    sourceType: JobPostSource;
    baseSalary?: JobPostBaseSalary;
    status?: JobPostStatus;
    stats?: JobPostStatsModel;
    jobCategory?: string;
    publisherUID?: string;
}

export interface JobPostStatsModel {
    views?: {[timestamp: string]: string}; // deprecated don't use it
    appliedBtn?: {[timestamp: string]: string}; // deprecated don't it
    numViews?: number;
    numApplications?: number;
}

export interface JobPostCompanyDetailsModel extends CatalogCompanyEntity {
    companyEmail?: string;
    description?: string;
    companyCareersLink?: string;
}

export interface JobSkills {
    jobCategory?: string;
    mainSkills: string[];
    secondarySkills: string[];
}

export interface JobApplicationMethod {
    url: string; // Can be an email
    applicationType: JobApplicationType;
}

export interface JobPostBaseSalary {
    currency: "USD" | "EUR",
    value: {
        minValue: number,
        maxValue: number,
        unitText: "HOUR" | "YEAR",
    }
}

export class JobPostUtils {

    static getRemoteRegionAllowance():{[optionId: string]: string} {
        let remoteRegionsAllowance: {[optionId: string]: string} = {};

        for(let key in RemoteZoneRestriction) {
            let label: string = key;

            switch (key) {
                case RemoteZoneRestriction.SOUTH_AMERICA: label = "🌎 LATAM"; break;
                case RemoteZoneRestriction.AUSTRALIA: label = "🇦🇺 Australia/Oceania"; break;
                case RemoteZoneRestriction.ASIA: label = "🌏 Asia"; break;
                case RemoteZoneRestriction.CANADA: label = "🇨🇦 Canada"; break;
                case RemoteZoneRestriction.US: label = "🇺🇸 USA"; break;
                case RemoteZoneRestriction.EUROPE: label = "🇪🇺 Europe"; break;
                case RemoteZoneRestriction.UK: label = "🇬🇧 UK"; break;
                case RemoteZoneRestriction.AFRICA: label = "🌍 Africa"; break;
                case RemoteZoneRestriction.WORLDWIDE:
                default:
                    label = "🌐 Worldwide"; break;
            }
            remoteRegionsAllowance[key] = label;
        }

        return remoteRegionsAllowance;
    }

    static generateCustomZoneRestrictionLabel(jobPostData: JobPostDetailsModel): JobPostDetailsModel {

        if (jobPostData.remoteRegionsAllowance) {
            jobPostData.customZoneRestriction = "Work from: ";
            let customLabel: string = "";
            let mapRegionLabels: {[optionId: string]: string} = this.getRemoteRegionAllowance();
            Object.keys(jobPostData.remoteRegionsAllowance).forEach((regionKey) => {
                if (jobPostData.remoteRegionsAllowance![regionKey] === true) {
                    if (customLabel) {
                        customLabel += "/ ";
                    }
                    customLabel += mapRegionLabels[regionKey];
                }
            });

            if (customLabel) {
                jobPostData.customZoneRestriction = "Work from: " + customLabel;
            }

        }
        return jobPostData;
    }

    static convertJobPostDetailsToJobPostListInfos(jobPost: JobPostDetailsModel): JobPostListInfoModel {
        return {
            jobSkills: jobPost.jobSkills,
            jobCategory: jobPost.jobCategory,
            companyDetails: jobPost.companyDetails,
            uid: jobPost.uid,
            positionTitle: jobPost.positionTitle,
            customZoneRestriction: jobPost.customZoneRestriction,
            remoteRegionsAllowance: jobPost.remoteRegionsAllowance,
            publishedAt: jobPost.publishedAt,
            updatedAt: jobPost.updatedAt,
            createdAt: jobPost.createdAt,
        };
    }

    static generateSocialPost(jobPost: JobPostDetailsModel): string {
        const {
            companyDetails,
            positionTitle,
            jobSkills,
        } = jobPost;

        const companyName = companyDetails.companyName
        let skills = jobSkills.mainSkills.map(skill => skill.toUpperCase().replace("_", " ")).join(", ");
        skills += ", " +  jobPost.jobSkills.secondarySkills.map(skill => skill.toUpperCase()).join(", ");
        const regionRestrictions = jobPost.customZoneRestriction;
        let tags = jobSkills.mainSkills.map(skill => "#" + BackendSharedUtils.replaceAll(skill.toUpperCase(), " ", "")).join(" ");
        tags += ", " + jobPost.jobSkills.secondarySkills.map(skill => "#" + BackendSharedUtils.replaceAll(skill.toUpperCase(), " ", "")).join(" ")
        let salary = BackendSharedUtils.findMoneyValues(jobPost.jobSkills.secondarySkills)[0];

        // Additional hashtags related to remote work and job specifics
        const remoteWorkHashtags = "#RemoteWork #WorkFromHome #RemoteJobs #Hiring #JobOpportunity";
        const jobSpecificHashtags = `#${positionTitle.replace(/\s+/g, '')}`;


        const postMessage =
        `🚀 New Remote Job Alert!
💼 Position: ${positionTitle}
🏢 Company: ${companyName}
🌍 Location: ${regionRestrictions}
🛠️ Skills/Requirements: ${skills}
${salary ? "💵 Salary: " + salary : ""}
        
${tags}
${remoteWorkHashtags} ${jobSpecificHashtags}

        Apply now: ${CustomRoutesUtils.getJobPostDetailsUrl(jobPost.uid!)}
    `;

        return postMessage.trim();
    }

    static async generateJobPositionThumbnail(jobPost: JobPostDetailsModel): Promise<string> {
        let tags: string[] = [];
        tags = tags.concat(jobPost.jobSkills.mainSkills!.map((tag) => tag.toUpperCase()));
        tags = tags.concat(jobPost.jobSkills.secondarySkills!.map((tag) => tag.toUpperCase()));

        return await generateInstagramJobPositionImage({
            positionTitle: jobPost.positionTitle,
            companyWebsite: jobPost.customZoneRestriction!,
            companyLogo: jobPost.companyDetails.companyLogo.fileUrl,
            companyName: jobPost.companyDetails.companyName!,
            tags: tags,
        });
    }


    static async socialNetworkPublish(jobPost: JobPostDetailsModel) {
        let response = await SocialNetworksPostUtils.facebookPublishTextPost({
            message: this.generateSocialPost(jobPost),
            link: CustomRoutesUtils.getJobPostDetailsUrl(jobPost.uid!),
        });
        console.log("Facebook.response: ", response);

        response = await SocialNetworksPostUtils.threadsPublishPost({
            message: this.generateSocialPost(jobPost),
        });
        console.log("Threads.response: ", response);

        response = await SocialNetworksPostUtils.redditPostLink({
            text: this.generateSocialPost(jobPost),
            url: CustomRoutesUtils.getJobPostDetailsUrl(jobPost.uid!),
            title: `[Remote Job] ${jobPost.positionTitle} @${jobPost.companyDetails.companyName} (${jobPost.customZoneRestriction})`
        });
        console.log("Reddit.response: ", response);


        const instagramImage: any = await this.generateJobPositionThumbnail(jobPost);
        response = await SocialNetworksPostUtils.instagramPublishPhoto({
            uid: jobPost.uid!,
            message: this.generateSocialPost(jobPost),
            imageBase64: instagramImage,
        });
        console.log("Instagram.response: ", response);
    }

    static generateJobPostThumbnailUrl(jobPost: JobPostDetailsModel, imageType: 'SQUARE' | 'RECTANGLE' = 'SQUARE'): string {
        const skills = [
            ...jobPost.jobSkills.mainSkills ?? [],
            ...jobPost.jobSkills.secondarySkills ?? []
        ];

        const tags = skills.map(skill => encodeURIComponent(skill.toUpperCase())).join(',');

        const params = new URLSearchParams({
            positionTitle: jobPost.positionTitle,
            companyWebsite: encodeURIComponent(jobPost.customZoneRestriction ?? ''),
            companyLogo: jobPost.companyDetails.companyLogo.fileUrl ?? '',
            companyName: jobPost.companyDetails.companyName ?? '',
            tags,
            imageType
        });

        const url = `${WEBAPP_CONFIGS.WEBSITE_URL}/api/jobpost-thumbnail?${params.toString()}`;
        console.log(url);
        return url;
    }

}


/**
 * Instagram image
 * @param jobOffer
 */
export async function generateInstagramJobPositionImage(jobOffer: {
    companyLogo: string;
    companyName: string;
    positionTitle: string;
    companyWebsite: string;
    tags: string[];
}): Promise<string> {
    const size = 1080;
    const canvas = createCanvas(size, size);
    const ctx = canvas.getContext('2d');

    // Set background
    ctx.fillStyle = '#F9F9FB';
    ctx.fillRect(0, 0, size, size);

    // Load and draw company logo in a circular background with shadow
    const logo = await loadImage(jobOffer.companyLogo);
    const logoSize = 120;
    let logoX = 70;
    let logoY = 70;

    // Draw shadow
    ctx.save();
    ctx.shadowColor = 'rgba(0, 0, 0, 0.2)';
    ctx.shadowBlur = 15;
    ctx.beginPath();
    ctx.arc(logoX + logoSize/2, logoY + logoSize/2, logoSize/2, 0, Math.PI * 2);
    ctx.fillStyle = '#FFFFFF';
    ctx.fill();
    ctx.restore();

    // Clip the logo to a circle
    ctx.save();
    ctx.beginPath();
    ctx.arc(logoX + logoSize/2, logoY + logoSize/2, logoSize/2, 0, Math.PI * 2);
    ctx.clip();
    ctx.drawImage(logo, logoX, logoY, logoSize, logoSize);
    ctx.restore();

    // Draw company name
    ctx.font = 'bold 48px Helvetica';
    ctx.fillStyle = '#1a202c';
    ctx.fillText(jobOffer.companyName, logoX + logoSize + 30, logoY + 40);

    // Draw company website
    ctx.font = '24px Helvetica';
    ctx.fillStyle = '#718096';
    ctx.fillText(jobOffer.companyWebsite, logoX + logoSize + 30, logoY + 80);

    // Draw position title (centered, larger, and bold)
    ctx.font = 'bold 80px Helvetica';
    ctx.fillStyle = '#1a202c';
    ctx.textAlign = 'center';
    wrapText(ctx, jobOffer.positionTitle, size / 2, 400, size - 140, 90);
    ctx.textAlign = 'left'; // Reset text alignment

    // Add "New Remote Job" banner with shadow and rounded corners
    ctx.save();
    ctx.translate(size - 250, 140);
    ctx.rotate(Math.PI / 4);

    // Add shadow
    ctx.shadowColor = 'rgba(0, 0, 0, 0.3)';
    ctx.shadowBlur = 10;
    ctx.shadowOffsetX = 3;
    ctx.shadowOffsetY = 3;

    // Draw rounded rectangle
    const bannerWidth = 300;
    const bannerHeight = 60;
    const cornerRadius = 10;

    ctx.beginPath();
    ctx.moveTo(cornerRadius, 0);
    ctx.lineTo(bannerWidth - cornerRadius, 0);
    ctx.quadraticCurveTo(bannerWidth, 0, bannerWidth, cornerRadius);
    ctx.lineTo(bannerWidth, bannerHeight - cornerRadius);
    ctx.quadraticCurveTo(bannerWidth, bannerHeight, bannerWidth - cornerRadius, bannerHeight);
    ctx.lineTo(cornerRadius, bannerHeight);
    ctx.quadraticCurveTo(0, bannerHeight, 0, bannerHeight - cornerRadius);
    ctx.lineTo(0, cornerRadius);
    ctx.quadraticCurveTo(0, 0, cornerRadius, 0);
    ctx.closePath();

    ctx.fillStyle = '#EEB2BE';
    ctx.fill();

    // Reset shadow for text
    ctx.shadowColor = 'transparent';
    ctx.shadowBlur = 0;
    ctx.shadowOffsetX = 0;
    ctx.shadowOffsetY = 0;

    ctx.fillStyle = '#093769';
    ctx.font = 'bold 24px Helvetica';
    ctx.fillText('NEW REMOTE JOB', 40, 40);
    ctx.restore();

    // Draw tags
    ctx.font = 'bold 28px Helvetica';
    let tagX = 70;
    let tagY = 700;
    jobOffer.tags.forEach(tag => {
        const tagWidth = ctx.measureText(tag).width + 40;
        if (tagX + tagWidth > size - 70) {
            tagX = 70;
            tagY += 60;
        }
        drawTag(ctx, tag, tagX, tagY);
        tagX += tagWidth + 20;
    });

    // Draw "Apply now" button
    drawButton(ctx, 'Apply now', 70, 950, 200, 70);

    // Load the website logo
    const websiteLogo = await loadImage("https://www.remote-work.app/images/logo.png");

    // Define the maximum width and height for the logo area
    const maxLogoWidth = 200;
    const maxLogoHeight = 80;

    // Calculate the scaling factor to fit the logo within the defined area
    const scaleX = maxLogoWidth / websiteLogo.width;
    const scaleY = maxLogoHeight / websiteLogo.height;
    const scale = Math.min(scaleX, scaleY);

    // Calculate the new dimensions of the logo
    const logoWidth = websiteLogo.width * scale;
    const logoHeight = websiteLogo.height * scale;

    // Calculate the position to place the logo (top-right corner with some padding)
    logoX = size - logoWidth - 20;
    logoY = 20;

    // Draw the logo with the calculated dimensions and position
    ctx.drawImage(websiteLogo, logoX, logoY, logoWidth, logoHeight);

    // Convert canvas to base64
    return canvas.toDataURL('image/jpeg');
}

function wrapText(ctx: CanvasRenderingContext2D, text: string, x: number, y: number, maxWidth: number, lineHeight: number) {
    const words = text.split(' ');
    let line = '';

    for (const word of words) {
        const testLine = line + word + ' ';
        const metrics = ctx.measureText(testLine);
        const testWidth = metrics.width;
        if (testWidth > maxWidth && line !== '') {
            ctx.fillText(line, x, y);
            line = word + ' ';
            y += lineHeight;
        } else {
            line = testLine;
        }
    }
    ctx.fillText(line, x, y);
}

function drawTag(ctx: CanvasRenderingContext2D, tag: string, x: number, y: number) {
    ctx.save();
    ctx.shadowColor = 'rgba(0, 0, 0, 0.2)';
    ctx.shadowBlur = 5;
    ctx.shadowOffsetX = 2;
    ctx.shadowOffsetY = 2;

    ctx.fillStyle = '#FFFFFF';
    const tagWidth = ctx.measureText(tag).width + 40;
    const tagHeight = 44;
    const radius = tagHeight / 2;

    ctx.beginPath();
    ctx.moveTo(x + radius, y);
    ctx.lineTo(x + tagWidth - radius, y);
    ctx.quadraticCurveTo(x + tagWidth, y, x + tagWidth, y + radius);
    ctx.lineTo(x + tagWidth, y + tagHeight - radius);
    ctx.quadraticCurveTo(x + tagWidth, y + tagHeight, x + tagWidth - radius, y + tagHeight);
    ctx.lineTo(x + radius, y + tagHeight);
    ctx.quadraticCurveTo(x, y + tagHeight, x, y + tagHeight - radius);
    ctx.lineTo(x, y + radius);
    ctx.quadraticCurveTo(x, y, x + radius, y);
    ctx.fill();

    ctx.shadowColor = 'transparent';
    ctx.fillStyle = '#333333';
    ctx.fillText(tag, x + 20, y + 30);
    ctx.restore();
}

function drawButton(ctx: CanvasRenderingContext2D, text: string, x: number, y: number, width: number, height: number) {
    ctx.save();
    ctx.shadowColor = 'rgba(0, 0, 0, 0.2)';
    ctx.shadowBlur = 5;
    ctx.shadowOffsetX = 2;
    ctx.shadowOffsetY = 2;

    ctx.fillStyle = '#FFFFFF';
    const radius = height / 2;

    ctx.beginPath();
    ctx.moveTo(x + radius, y);
    ctx.lineTo(x + width - radius, y);
    ctx.quadraticCurveTo(x + width, y, x + width, y + radius);
    ctx.lineTo(x + width, y + height - radius);
    ctx.quadraticCurveTo(x + width, y + height, x + width - radius, y + height);
    ctx.lineTo(x + radius, y + height);
    ctx.quadraticCurveTo(x, y + height, x, y + height - radius);
    ctx.lineTo(x, y + radius);
    ctx.quadraticCurveTo(x, y, x + radius, y);
    ctx.fill();

    ctx.shadowColor = 'transparent';
    ctx.font = 'bold 28px Helvetica';
    ctx.fillStyle = '#333333';
    const textWidth = ctx.measureText(text).width;
    ctx.fillText(text, x + (width - textWidth) / 2, y + height / 2 + 10);
    ctx.restore();
}