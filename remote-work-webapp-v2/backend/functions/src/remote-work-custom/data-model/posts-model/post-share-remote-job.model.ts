import {JobPostListInfoModel} from "../job-post-details.model";
import {
    BasePostDataModel
} from "../../../shareable-firebase-backend/model-data/frontend-model-data/base-post-data.model";

export const PostShareRemoteJobType = "JobPostType";

export interface PostShareRemoteJobModel extends BasePostDataModel {
    jobPostInfo: JobPostListInfoModel;
}