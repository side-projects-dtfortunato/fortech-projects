import {
    JobApplicationType,
    JobExperienceLevel,
    JobPostBaseSalary,
    JobPostDetailsModel,
    JobPostSource,
    RemoteZoneRestriction
} from "../job-post-details.model";
import fetch from "node-fetch";
import {JSDOM} from "jsdom";
import {BackendSharedUtils} from "../../../shareable-firebase-backend/utils/backend-shared-utils";

var TurndownService = require('turndown');
var country = require('country-list-js');

var turndownService = new TurndownService();

export interface ExternalLinkedinJobDetailsModel {
    datePosted: string;
    description: string;
    employmentType: "CONTRACTOR" | "FULL_TIME" | "PART_TIME";
    hiringOrganization: Organization;
    image: string;
    industry: string;
    jobLocation: Place;
    skills: string;
    title: string;
    validThrough: string;
    educationRequirements: EducationalOccupationalCredential;
    experienceRequirements: OccupationalExperienceRequirements;
    jobLocationType: "TELECOMMUTE";
    applicantLocationRequirements: Country;
    baseSalary?: BaseSalary;

    // Custom
    customLocationName?: string;
    applicationLink?: string;
}

interface Organization {
    type: string;
    name: string;
    sameAs: string;
    logo: string;
}

interface Place {
    address: PostalAddress;
    latitude: number;
    longitude: number;
}

interface PostalAddress {
    addressCountry: string;
    addressLocality: string;
    addressRegion: string;
    streetAddress: string | null;
}

interface EducationalOccupationalCredential {
    credentialCategory: string;
}

interface OccupationalExperienceRequirements {
    monthsOfExperience: number; // 12 -> Entry / 12 - 24 -> Junior / 24 - 36 -> Mid / +36 -> Senior
}

interface Country {
    name: "United States";
}

interface BaseSalary extends JobPostBaseSalary {
}

export class ExternalLinkedinJobDataUtils {

    static async exportLinkedinJobJsonData(jobUrl: string): Promise<ExternalLinkedinJobDetailsModel | undefined> {
        /*let jobDetails: JobPostDetailsModel = {
            sourceUrl: jobUrl,
            sourceType: JobPostSource.LINKEDIN,
            applicationMethod: {
                applicationType: JobApplicationType.URL,
                url: "",
            },
        }; */

        // Export Data
        const urlResponse = await fetch(jobUrl);
        const html = await urlResponse.text();
        const dom = new JSDOM(html);

        // Parse Data
        const jsonData: string | undefined | null = this.removePrefixSuffixWhitespaceAndBreakLines(dom.window.document.querySelector<HTMLScriptElement>(
            'script[type="application/ld+json"]'
        )?.textContent);
        const locationName: string | undefined | null = this
            .removePrefixSuffixWhitespaceAndBreakLines(dom.window
                .document
                .querySelector('h4.top-card-layout__second-subline span.topcard__flavor--bullet')?.textContent);


        const companyLogoUrl: string | undefined | null = this
            .removePrefixSuffixWhitespaceAndBreakLines(dom.window
                .document
                .querySelector('div.top-card-layout__card img.artdeco-entity-image--square-5')?.getAttribute('src'));
        console.log("companyLogoUrl: ", companyLogoUrl);


        let applicationLink: string | undefined | null = this
            .removePrefixSuffixWhitespaceAndBreakLines(dom.window
                .document
                .querySelector('a.sign-up-modal__company_webiste')?.getAttribute('href'));

        if (applicationLink) {
            applicationLink = BackendSharedUtils.getQueryParamValue(applicationLink, "url");
        }

        let jsonParsedData: ExternalLinkedinJobDetailsModel | undefined = jsonData ? JSON.parse(jsonData) : undefined;

        if (jsonParsedData) {
            if (locationName) {
                jsonParsedData.customLocationName = locationName;
            }

            if (applicationLink) {
                jsonParsedData.applicationLink = BackendSharedUtils.addLinkSource(applicationLink);
            }
            if (jsonParsedData.hiringOrganization.logo) {
                jsonParsedData.hiringOrganization.logo = jsonParsedData.hiringOrganization.logo
                    .replace(/amp;/g, "");
            }
            if (jsonParsedData.image) {
                jsonParsedData.image = jsonParsedData.image
                    .replace(/amp;/g, "");
            }
        }


        return jsonParsedData;
    }

    static async parseLinkedinJobJsonToJobPostDetails(sourceUrl: string): Promise<JobPostDetailsModel | undefined> {
        const jsonData: ExternalLinkedinJobDetailsModel | undefined = await this.exportLinkedinJobJsonData(sourceUrl);

        if (!jsonData) {
            return undefined;
        }

        const jobPostDetails: JobPostDetailsModel = {
            uid: BackendSharedUtils.string_to_slug(jsonData.hiringOrganization.name) + "-" + BackendSharedUtils.string_to_slug(jsonData.title),
            sourceUrl: sourceUrl,
            sourceType: JobPostSource.LINKEDIN,
            applicationMethod: {
                applicationType: JobApplicationType.URL,
                url: jsonData.applicationLink ? jsonData.applicationLink : "",
            },
            companyDetails: {
                uid: BackendSharedUtils.string_to_slug(jsonData.hiringOrganization.name.toLowerCase()),
                companyName: jsonData.hiringOrganization.name,
                companyLogo: {
                    fileUrl: jsonData.hiringOrganization.logo,
                    storagePath: ""
                },
                website: jsonData.hiringOrganization.sameAs,
                companyLocation: {
                    cityName: jsonData.customLocationName,
                    countryName: jsonData.jobLocation.address.addressCountry,
                },
                industries: [jsonData.industry],
            },
            positionTitle: jsonData.title,
            positionDescription: turndownService.turndown(jsonData.description),
            publishedAt: Date.now(),
            updatedAt: Date.now(),
            stats: {
                numApplications: 0,
                numViews: 0,
            },
            jobSkills: {
                mainSkills: [],
                secondarySkills: [],
            },
            jobTags: [],
        };

        if (jsonData.employmentType) {
            jobPostDetails.jobTags?.push(BackendSharedUtils.convertWordsToUpperCaseAndReplaceUnderscoreByTrace(jsonData.employmentType));
        }

        // Check if can set the range salary
        if (jsonData.baseSalary) {
            jobPostDetails.baseSalary = jsonData.baseSalary;
        }

        if (jsonData.experienceRequirements) {
            if (jsonData.experienceRequirements.monthsOfExperience <= 12) {
                jobPostDetails.experienceLevel = JobExperienceLevel.ENTRY;
            } else if (jsonData.experienceRequirements.monthsOfExperience <= 24) {
                jobPostDetails.experienceLevel = JobExperienceLevel.JUNIOR;
            } else if (jsonData.experienceRequirements.monthsOfExperience <= 36) {
                jobPostDetails.experienceLevel = JobExperienceLevel.MID;
            } else {
                jobPostDetails.experienceLevel = JobExperienceLevel.SENIOR;
            }
        }

        // Set Remote Allowance
        if (jsonData.jobLocation.address.addressCountry === "US") {
            jobPostDetails.remoteRegionsAllowance = {
                [RemoteZoneRestriction.US]: true,
            };
        } else if (jsonData.jobLocation.address.addressCountry === "CA") {
            jobPostDetails.remoteRegionsAllowance = {
                [RemoteZoneRestriction.CANADA]: true,
            };
        } else if (jsonData.jobLocation.address.addressCountry === "UK") {
            jobPostDetails.remoteRegionsAllowance = {
                [RemoteZoneRestriction.UK]: true,
            };
        } else if (jsonData.jobLocation.address.addressCountry === "AU") {
            jobPostDetails.remoteRegionsAllowance = {
                [RemoteZoneRestriction.AUSTRALIA]: true,
            };
        } else if (jsonData.jobLocation && jsonData.jobLocation.address) {
            const coutryInfo: any = country.findByIso2(jsonData.jobLocation.address.addressCountry);

            switch (coutryInfo.continent) {
                case "South America":
                    jobPostDetails.remoteRegionsAllowance = {
                        [RemoteZoneRestriction.SOUTH_AMERICA]: true,
                    };
                    break;
                case "Europe":
                    jobPostDetails.remoteRegionsAllowance = {
                        [RemoteZoneRestriction.EUROPE]: true,
                    };
                    break;
                case "Asia":
                    jobPostDetails.remoteRegionsAllowance = {
                        [RemoteZoneRestriction.ASIA]: true,
                    };
                    break;
                case "Africa":
                    jobPostDetails.remoteRegionsAllowance = {
                        [RemoteZoneRestriction.AFRICA]: true,
                    };
                    break;
            }
        }

        return jobPostDetails;
    }


    static removePrefixSuffixWhitespaceAndBreakLines(text?: string | null): string | undefined | null {
        if (text) {
            return text.trim()
                .split("\n")
                .join("");
        } else {
            return text;
        }
    }

    static convertHTML(html: string): string {
        const regex = /&[a-z]+;/g;
        const replaceCallback: (substring: string, ...args: any[]) => string = (substring: string, ...args) => {
            const match = args[0];
            // @ts-ignore
            return {
                "&lt;": "<",
                "&gt;": ">",
                "&amp;": "&",
            }[match];
        }

        const convertedString = html.replace(regex, replaceCallback);

        return convertedString;
    }

}