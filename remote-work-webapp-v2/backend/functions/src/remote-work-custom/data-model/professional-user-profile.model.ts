import {BaseModel} from "../../shareable-firebase-backend/model-data/base.model";
import {LocationModel} from "../../shareable-firebase-backend/model-data/location.model";
import {JobSkillModel} from "./job-skill.model";


export enum UserGender { MALE = "MALE", FEMALE = "FEMALE", OTHER = "OTHER" }

export enum JobCategory { DEVELOPER = "DEVELOPER", QA = "QA", IT_TECHNICIAN = "IT_TECHNICIAN", PRODUCT_MANAGER = "PRODUCT_MANAGER", UIUX_DESIGNER = "UIUX_DESIGNER", MANAGER = "MANAGER", DIGITAL_MARKETING = "DIGITAL_MARKETING", OTHER = "OTHER"}

export interface ProfessionalUserProfileModel extends BaseModel {
    nationality?: string;
    location?: LocationModel;
    linkedinLink?: string;
    currentRole?: string;
    jobCategory?: JobCategory;
    listMainSkills?: {[skillName: string]: JobSkillModel};
    listJobSkills?: {[skillName: string]: JobSkillModel};
}


export interface UserLanguageExperience {
    language?: string;
    level?: string; // A: Basic user | B: Independent User | C: Proeficient user | Native
}

export interface ProfessionalSkillsMapModel {
    skillName: string;
    userWithSkill: {[userId: string]: string}; // years experience
}