import {
    SharedCentralUserDataModel
} from "../../shareable-firebase-backend/model-data/frontend-model-data/shared-central-user-data.model";
import {ProfessionalUserProfileModel} from "./professional-user-profile.model";

export interface CustomCentralUserDataModel extends SharedCentralUserDataModel {
    proProfile?: ProfessionalUserProfileModel;
}