import {JobPostDetailsModel} from "./job-post-details.model";
import {BackendSharedUtils} from "../../shareable-firebase-backend/utils/backend-shared-utils";

export enum RemoteWorkNicheSitesTypes {
    REMOTE_WORK = "REMOTE_WORK", REACTJOBS = "REACTJOBS",
}

export class RemoteWorkNicheSitesUtils {

    static generateJobListAvailableNicheSites(jobData: JobPostDetailsModel) {
        let listNicheSites: RemoteWorkNicheSitesTypes[] = [RemoteWorkNicheSitesTypes.REMOTE_WORK];


        // Check if should include in React Jobs
        if (jobData.positionTitle.toLowerCase().includes("react")
            || jobData.jobSkills.jobCategory?.toLowerCase() === "react"
            || BackendSharedUtils.hasStringInArray(jobData.jobSkills.mainSkills, "react")
            || BackendSharedUtils.hasStringInArray(jobData.jobSkills.secondarySkills, "react"))
        {
            listNicheSites.push(RemoteWorkNicheSitesTypes.REACTJOBS);
        }

        return listNicheSites;
    }
}