

export const PRODUCTION_GCLOUD_PROJECT = "remote-flex-jobs";

export const WEBAPP_CONFIGS = {
    LOGO_URL: "https://www.remote-work.app/images/logo-white-bg.jpg",
    SITE_NAME: "Remote-Work.app",
    SITE_TAGLINE: "Daily Remote Jobs",
    WEBSITE_URL: "https://remote-work.app",
    COMPANY_ADDRESS: "3030 Coimbra (Portugal)",
    COMPANY_EMAIL: "contact@remote-work.app",
    POST_PREFIX_URL: "https://www.remote-work.app/",
    PROFILE_PREFIX_URL: "https://www.remote-work.app/public-profile/",
    UNSUBSCRIBE_URL: "https://us-central1-remote-flex-jobs.cloudfunctions.net/unsubscribeEmail?uid=",

    MAILERSEND_API_KEY: "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIxIiwianRpIjoiODExYzQ3YTRhYmIyMjk5MDlhNGQ2ZGU1YzNmMTYyY2E5NmNjZmU4ZWQwMzg0M2U4OTkyNmU5YjE1ZjE1MDE1N2E2OTkwZTE1ZTU2ZmU0MjkiLCJpYXQiOjE2NzM2MTc5NzAuMTQzMjEsIm5iZiI6MTY3MzYxNzk3MC4xNDMyMTMsImV4cCI6NDgyOTI5MTU3MC4xMzgzMDMsInN1YiI6IjE2MDQ4Iiwic2NvcGVzIjpbImVtYWlsX2Z1bGwiLCJkb21haW5zX2Z1bGwiLCJhY3Rpdml0eV9mdWxsIiwiYW5hbHl0aWNzX2Z1bGwiLCJ0b2tlbnNfZnVsbCIsIndlYmhvb2tzX2Z1bGwiLCJ0ZW1wbGF0ZXNfZnVsbCIsInN1cHByZXNzaW9uc19mdWxsIiwic21zX2Z1bGwiLCJlbWFpbF92ZXJpZmljYXRpb25fZnVsbCIsImluYm91bmRzX2Z1bGwiLCJyZWNpcGllbnRzX2Z1bGwiLCJzZW5kZXJfaWRlbnRpdHlfZnVsbCJdfQ.FYvjd2BmU43sfibei4YoSijRGSXv44e--LEYaMH0ojmzH0HmnbNOyi0N6Uk15wqRxGnV40eoXg1OjMhXvBLduCxYJba6cclWZvQVl2cWC9ig9gGcO5WqDMHpXfdJQnvfGnSLJRNPLRV4WgiK-AtXKQoxYGhrqWNnc_cVsPurI3gHo8HL5XLm-9rMJzZ2Im88PD791LFWF7dEIEe48jrPqNV1rI8UV9dh-LkXhrzv4EoMgegBANYPKtvt7JMMSSM_4ba29uqLhPR754kD8Kz8pU0x9M-92iZXkqawWobO15DQH6-yAor-dRpmWT8E6pViHy11g_vkb5KB0iVo0hHegGr5tprKr5BZg5V7i3ufkiaGMWtIDPyjD1ENjVsxCbfHcSzMdY_7tUWpTSIjJg67dHRiCc1r6o_x8l6TI6OToUTk7LzhiFRWCIcJa9WVctU4Bv6MGYTz3aGIvaAq3EC7C4eDGAZ_SwDPArlE3odLiYCfErz4jMetlzdzVbucsTPwFvL0BGTdkzqTj1SrxSTNby8ACohdLlkBOxvklexxEB-ecycp8zn5pp8gMn6bVangSR3lU-yUnfztkAl5XZGvE1S8rpW5v61hQWehrUUcm7GQuMXRosNxOAZAuFMri26MSjWvYffzdkLXk0V78S07B-Fs4OH1xeXekoJKyvbbnbo",

    CUSTOM_CONFIGS: {
        openPushInApp: true,
    } as any,

    // Account: fortuly.apps@gmail.com
    IMAGE_KIT_API: {
        publicKey : "public_OurCtnmj0jWqjVslk5YDfKE4aHQ=",
        privateKey : "private_vRUtPf6HGOj68f5rZZN8u9RHfiQ=",
        urlEndpoint : "https://ik.imagekit.io/fortulyapps",
    },

    STRIPE_API: {
        prod: "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",
        dev: "sk_test_51PueA3HT2hxH9IRDdlrmFSjPbHmjQ1An0PNkz74ZkAOw50c1MSK5nG0MGBWOvbm5RjkBDgG4VXnMDZ2yRBWJGtpA00wlrvUV9J",
        webhook_dev: "whsec_jySvwwZUSe3BLDFdJPYWn6c2vABfZbDd",
        webhook_prod: "whsec_XpV1aj2xc2sTfdjzz2wvtymWnFUP9cuX",
    },
    ONESIGNAL_CONFIGS: {
        apiKey: "os_v2_app_qnx45mgo4ba5bbxvyx6l7txjj2b34lkzhi5e7iftd46q75m2uf6kx6iaiw3ssvxj536sfj5k6ydm4htnngml664aezk34g4i6hi2yxq",
        appId: "836fceb0-cee0-41d0-86f5-c5fcbfcee94e"
    },


    SOCIAL_NETWORK_API_KEY: {
        metaAccessToken: "EAAMJXHxyBloBO4EjZCrZAzIochfQjWjQrIHSA122sKGbBjpwptKBppkyC2zSwDXuyuZBEdsjf2w5zucMVKDTtTo3n2RWTZCHMe4j2MylIYb8de6FEOVQtRcXeqf9ZAf0pOdYzj13baljC4Pk2DUxBQXNfFFpyTZBXpYEFfCD5yj58MZCPnGEjAno8ksy8V0dQZDZD",
        facebookPageId: "105065652045969",
        instagramId: "17841450045997698",
        threadsAPI: {
            clientId: "493992756403362",
            clientSecret: "1a970417958869daff83b6645b39cd00",
            redirectUri: `https://us-central1-${PRODUCTION_GCLOUD_PROJECT}.cloudfunctions.net/threadsAPIHandleAuthCallback`,
        },
        redditAPI: {
            clientId: "4AipG2HfGPnuOhtXtOYEmA",
            clientSecret: "9swPagUhH079Yq3uzQjyimQssUYhow",
            username: "FortulyApps",
            password: "divadlooc7",
            subReddit: "remoteworkapp",
        },
        bluesky: {
            username: "remote-work-app.bsky.social",
            password: "divadlooc7",
        },
        telegram: {
            channel: "@remotework_app",
            botToken: "7222995131:AAGigJvl74gZS9GSSTIOtShcCi37CPHEw-8",
        }
    }
}