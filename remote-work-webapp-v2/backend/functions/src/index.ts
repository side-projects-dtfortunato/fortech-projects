import * as functions from 'firebase-functions';
import * as admin from 'firebase-admin';

import * as PushNotifsService from './shareable-firebase-backend/functions/push-notifs';
import * as DocumentChangeService from './shareable-firebase-backend/functions/document-change';
import * as PostsDataService from './shareable-firebase-backend/functions/posts-data';
import * as UserProfileService from './shareable-firebase-backend/functions/user-profile';
import * as UserFollowService from './shareable-firebase-backend/functions/user-follow';

import * as EmailMarketingService from './shareable-firebase-backend/functions/email-marketing';
import {reloadSearchableItems} from "./shareable-firebase-backend/functions/search";
import {removePostData} from './shareable-firebase-backend/functions/posts-data/remove-post';
import {setFeaturePost} from './shareable-firebase-backend/functions/posts-data/set-feature-post';

import {deleteAccount} from './shareable-firebase-backend/functions/auth/delete-account';
import {onCreateUser, onDeleteUser} from './shareable-firebase-backend/functions/auth';
import {testFunction} from './remote-work-custom/functions/test.function';
import {onContactUsCreated} from './shareable-firebase-backend/functions/contact-us/on-contactus-created.function';


// import {generateEmailDailyDigest} from './remote-work-custom/functions/email-marketing/daily-digest';
import {generateEmailWeeklyDigest} from './remote-work-custom/functions/email-marketing/remote-work-weekly-digest';
import {routineJobFunction} from './shareable-firebase-backend/functions/routine-jobs/routineJob.function';

import {exportLinkedinJobJsonData, parseLinkedinJobJsonData, importLinkedinJobPostDetails} from './remote-work-custom/functions/ingest-external-jobs/linkedin/linkedin-job-details';
import {threadsAPIHandleAuthCallback, threadsAPIGenerateAuthUrl} from './shareable-firebase-backend/base-projects/social-networks-autopost/functions/threadsapi.function'

// Shared Jobs Discovery
import {submitJobPost, submitJobPostStripeWebhook} from './shareable-firebase-backend/base-projects/jobs-discover/functions/submitJobPost.function'
import {digestImportedJobsList, fetchJobsList} from './shareable-firebase-backend/base-projects/jobs-discover/functions/fetchJobsList.function'
import {onJobUpdated} from './shareable-firebase-backend/base-projects/jobs-discover/functions/on-job-updated.function'
import {cleanOlderJobsFunction} from './shareable-firebase-backend/base-projects/jobs-discover/functions/cleanOlderJobs.function'
import {importJobsFromRemoteJobsHubFunction} from './shareable-firebase-backend/base-projects/jobs-discover/functions/importJobsFromRemoteJobsHub.function'
import {globalEmailDailyDigestGenerator} from './shareable-firebase-backend/functions/email-marketing/global-email-daily-digest-generator.function'



// Init App
admin.initializeApp(functions.config().firebase);
admin.firestore().settings({ ignoreUndefinedProperties: true });


// Push
export const updateFCMTokens = PushNotifsService.updateFCMTokens;


// User profile
export const updateUserProfile = UserProfileService.updateUserProfile;
export const onUpdatePublicUserProfile = UserProfileService.onUpdatePublicUserProfile;
export const checkUsernameValid = UserProfileService.checkUsernameValid;

// Document Change Service
export const requestDocumentChanges = DocumentChangeService.requestDocumentChanges;

// Posts Data Management
export const pushPostData = PostsDataService.pushPostData;
export const onPostDataUpdated = PostsDataService.onPostDataUpdated;
export const pushPostComment = PostsDataService.pushPostComment;
export const pushPostReaction = PostsDataService.pushPostReaction;
export const openViewPost = PostsDataService.openViewPost;
export const exportUrlMetadata = PostsDataService.exportUrlMetadata;
export const updatePostBookmark = PostsDataService.updatePostBookmark;

// User follow service
export const updateFollowingUser = UserFollowService.updateFollowingUser;

// Email Service
export const validateEmail = EmailMarketingService.validateEmail;
export const unsubscribeEmail = EmailMarketingService.unsubscribeEmail;


// Export functions
export {
    removePostData,
    reloadSearchableItems,


    // Email Daily Digest
    // generateEmailDailyDigest,
    generateEmailWeeklyDigest,
    routineJobFunction,

    // Set Post featured
    setFeaturePost,

    // Auth Functions
    onCreateUser,
    onDeleteUser,
    deleteAccount,

    exportLinkedinJobJsonData,
    parseLinkedinJobJsonData,
    importLinkedinJobPostDetails,

    threadsAPIHandleAuthCallback, threadsAPIGenerateAuthUrl,


    // Jobs Discovery Functions
    globalEmailDailyDigestGenerator,
    digestImportedJobsList, onJobUpdated, cleanOlderJobsFunction,
    submitJobPost, submitJobPostStripeWebhook, importJobsFromRemoteJobsHubFunction,
    fetchJobsList,

    testFunction, onContactUsCreated
}