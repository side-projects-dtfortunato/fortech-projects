import {
    JobDetailsModel,
    JobWorkModeType,
    LinkedinSourceModel
} from "../../shareable-firebase-backend/base-projects/jobs-discover/shared-jobs-discover.data";
import axios from "axios";


/**
 * Regions ids:
 * - UK: 101165590
 * - Australia: 101452733
 * - USA: 103644278
 * - EU: 91000002
 */
/*export const LINKEDIN_SOURCES: LinkedinSourceModel[] = [
    {
        id: "1",
        category: "Software Development",
        region: "USA",
        url: "https://www.linkedin.com/jobs/search?keywords=Software Engineer&geoId=103644278&f_TPR=&f_WT=2&f_E=2%2C3%2C4%2C5&position=1&pageNum=0",
    },
    {
        id: "2",
        category: "Software Development",
        region: "EU",
        url: "https://www.linkedin.com/jobs/search?keywords=Software Engineer&geoId=91000002&f_TPR=&f_WT=2&f_E=2%2C3%2C4%2C5&position=1&pageNum=0",
    },
    {
        id: "3",
        category: "Marketing",
        region: "EU",
        url: "https://www.linkedin.com/jobs/search?keywords=Marketing&geoId=91000002&f_TPR=&f_WT=2&f_E=2%2C3%2C4%2C5&position=1&pageNum=0",
    },
    {
        id: "4",
        category: "Marketing",
        region: "USA",
        url: "https://www.linkedin.com/jobs/search?keywords=Marketing&geoId=103644278&f_TPR=&f_WT=2&f_E=2%2C3%2C4%2C5&position=1&pageNum=0",
    },
    {
        id: "5",
        category: "Customer Service",
        region: "EU",
        url: "https://www.linkedin.com/jobs/search?keywords=Customer Service&geoId=91000002&f_TPR=&f_WT=2&f_E=2%2C3%2C4%2C5&position=1&pageNum=0",
    },
    {
        id: "6",
        category: "Customer Service",
        region: "USA",
        url: "https://www.linkedin.com/jobs/search?keywords=Customer Service&geoId=103644278&f_TPR=&f_WT=2&f_E=2%2C3%2C4%2C5&position=1&pageNum=0",
    },
    {
        id: "7",
        category: "Design",
        region: "EU",
        url: "https://www.linkedin.com/jobs/search?keywords=Design&geoId=91000002&f_TPR=&f_WT=2&f_E=2%2C3%2C4%2C5&position=1&pageNum=0",
    },
    {
        id: "8",
        category: "Design",
        region: "USA",
        url: "https://www.linkedin.com/jobs/search?keywords=Design&geoId=103644278&f_TPR=&f_WT=2&f_E=2%2C3%2C4%2C5&position=1&pageNum=0",
    },
    {
        id: "9",
        category: "Finance",
        region: "EU",
        url: "https://www.linkedin.com/jobs/search?keywords=Finance&geoId=91000002&f_TPR=&f_WT=2&f_E=2%2C3%2C4%2C5&position=1&pageNum=0",
    },
    {
        id: "10",
        category: "Finance",
        region: "USA",
        url: "https://www.linkedin.com/jobs/search?keywords=Finance&geoId=103644278&f_TPR=&f_WT=2&f_E=2%2C3%2C4%2C5&position=1&pageNum=0",
    },
    {
        id: "11",
        category: "HR",
        region: "EU",
        url: "https://www.linkedin.com/jobs/search?keywords=HR&geoId=91000002&f_TPR=&f_WT=2&f_E=2%2C3%2C4%2C5&position=1&pageNum=0",
    },
    {
        id: "12",
        category: "HR",
        region: "USA",
        url: "https://www.linkedin.com/jobs/search?keywords=HR&geoId=103644278&f_TPR=&f_WT=2&f_E=2%2C3%2C4%2C5&position=1&pageNum=0",
    },
    {
        id: "13",
        category: "Project Management",
        region: "EU",
        url: "https://www.linkedin.com/jobs/search?keywords=Project Manager&geoId=91000002&f_TPR=&f_WT=2&f_E=2%2C3%2C4%2C5&position=1&pageNum=0",
    },
    {
        id: "14",
        category: "Project Management",
        region: "USA",
        url: "https://www.linkedin.com/jobs/search?keywords=Project Manager&geoId=103644278&f_TPR=&f_WT=2&f_E=2%2C3%2C4%2C5&position=1&pageNum=0",
    },
    {
        id: "15",
        category: "Data Analysis",
        region: "EU",
        url: "https://www.linkedin.com/jobs/search?keywords=Data Analysis&geoId=91000002&f_TPR=&f_WT=2&f_E=2%2C3%2C4%2C5&position=1&pageNum=0",
    },
    {
        id: "16",
        category: "Data Analysis",
        region: "USA",
        url: "https://www.linkedin.com/jobs/search?keywords=Data Analysis&geoId=103644278&f_TPR=&f_WT=2&f_E=2%2C3%2C4%2C5&position=1&pageNum=0",
    },
    {
        id: "18",
        category: "Sales & Business",
        region: "EU",
        url: "https://www.linkedin.com/jobs/search?keywords=Sales Business&geoId=91000002&f_TPR=&f_WT=2&f_E=2%2C3%2C4%2C5&position=1&pageNum=0",
    },
    {
        id: "19",
        category: "Sales & Business",
        region: "USA",
        url: "https://www.linkedin.com/jobs/search?keywords=Sales Business&geoId=103644278&f_TPR=&f_WT=2&f_E=2%2C3%2C4%2C5&position=1&pageNum=0",
    },
    {
        id: "20",
        category: "Administrative",
        region: "EU",
        url: "https://www.linkedin.com/jobs/search?keywords=Administrative&geoId=91000002&f_TPR=&f_WT=2&f_E=2%2C3%2C4%2C5&position=1&pageNum=0",
    },
    {
        id: "21",
        category: "Administrative",
        region: "USA",
        url: "https://www.linkedin.com/jobs/search?keywords=Administrative&geoId=103644278&f_TPR=&f_WT=2&f_E=2%2C3%2C4%2C5&position=1&pageNum=0",
    },

    // Australia
    {
        id: "22",
        category: "Software Development",
        region: "Australia",
        url: "https://www.linkedin.com/jobs/search?keywords=Software Engineer&geoId=101452733&f_TPR=&f_WT=2&f_E=2%2C3%2C4%2C5&position=1&pageNum=0",
    },
    {
        id: "23",
        category: "Marketing",
        region: "Australia",
        url: "https://www.linkedin.com/jobs/search?keywords=Marketing&geoId=101452733&f_TPR=&f_WT=2&f_E=2%2C3%2C4%2C5&position=1&pageNum=0",
    },
    {
        id: "24",
        category: "Customer Service",
        region: "Australia",
        url: "https://www.linkedin.com/jobs/search?keywords=Customer Service&geoId=101452733&f_TPR=&f_WT=2&f_E=2%2C3%2C4%2C5&position=1&pageNum=0",
    },
    {
        id: "25",
        category: "Design",
        region: "Australia",
        url: "https://www.linkedin.com/jobs/search?keywords=Design&geoId=101452733&f_TPR=&f_WT=2&f_E=2%2C3%2C4%2C5&position=1&pageNum=0",
    },
    {
        id: "26",
        category: "Finance",
        region: "Australia",
        url: "https://www.linkedin.com/jobs/search?keywords=Finance&geoId=101452733&f_TPR=&f_WT=2&f_E=2%2C3%2C4%2C5&position=1&pageNum=0",
    },
    {
        id: "27",
        category: "HR",
        region: "Australia",
        url: "https://www.linkedin.com/jobs/search?keywords=HR&geoId=101452733&f_TPR=&f_WT=2&f_E=2%2C3%2C4%2C5&position=1&pageNum=0",
    },
    {
        id: "28",
        category: "Project Management",
        region: "Australia",
        url: "https://www.linkedin.com/jobs/search?keywords=Project Manager&geoId=101452733&f_TPR=&f_WT=2&f_E=2%2C3%2C4%2C5&position=1&pageNum=0",
    },
    {
        id: "29",
        category: "Data Analysis",
        region: "Australia",
        url: "https://www.linkedin.com/jobs/search?keywords=Data Analysis&geoId=101452733&f_TPR=&f_WT=2&f_E=2%2C3%2C4%2C5&position=1&pageNum=0",
    },
    {
        id: "30",
        category: "Sales & Business",
        region: "Australia",
        url: "https://www.linkedin.com/jobs/search?keywords=Sales Business&geoId=101452733&f_TPR=&f_WT=2&f_E=2%2C3%2C4%2C5&position=1&pageNum=0",
    },
    {
        id: "31",
        category: "Administrative",
        region: "Australia",
        url: "https://www.linkedin.com/jobs/search?keywords=Administrative&geoId=101452733&f_TPR=&f_WT=2&f_E=2%2C3%2C4%2C5&position=1&pageNum=0",
    },

    // UK
    {
        id: "32",
        category: "Software Development",
        region: "UK",
        url: "https://www.linkedin.com/jobs/search?keywords=Software Engineer&geoId=101165590&f_TPR=&f_WT=2&f_E=2%2C3%2C4%2C5&position=1&pageNum=0",
    },
    {
        id: "33",
        category: "Marketing",
        region: "UK",
        url: "https://www.linkedin.com/jobs/search?keywords=Marketing&geoId=101165590&f_TPR=&f_WT=2&f_E=2%2C3%2C4%2C5&position=1&pageNum=0",
    },
    {
        id: "34",
        category: "Customer Service",
        region: "UK",
        url: "https://www.linkedin.com/jobs/search?keywords=Customer Service&geoId=101165590&f_TPR=&f_WT=2&f_E=2%2C3%2C4%2C5&position=1&pageNum=0",
    },
    {
        id: "35",
        category: "Design",
        region: "UK",
        url: "https://www.linkedin.com/jobs/search?keywords=Design&geoId=101165590&f_TPR=&f_WT=2&f_E=2%2C3%2C4%2C5&position=1&pageNum=0",
    },
    {
        id: "36",
        category: "Finance",
        region: "UK",
        url: "https://www.linkedin.com/jobs/search?keywords=Finance&geoId=101165590&f_TPR=&f_WT=2&f_E=2%2C3%2C4%2C5&position=1&pageNum=0",
    },
    {
        id: "37",
        category: "HR",
        region: "UK",
        url: "https://www.linkedin.com/jobs/search?keywords=HR&geoId=101165590&f_TPR=&f_WT=2&f_E=2%2C3%2C4%2C5&position=1&pageNum=0",
    },
    {
        id: "38",
        category: "Project Management",
        region: "UK",
        url: "https://www.linkedin.com/jobs/search?keywords=Project Manager&geoId=101165590&f_TPR=&f_WT=2&f_E=2%2C3%2C4%2C5&position=1&pageNum=0",
    },
    {
        id: "39",
        category: "Data Analysis",
        region: "UK",
        url: "https://www.linkedin.com/jobs/search?keywords=Data Analysis&geoId=101165590&f_TPR=&f_WT=2&f_E=2%2C3%2C4%2C5&position=1&pageNum=0",
    },
    {
        id: "40",
        category: "Sales & Business",
        region: "UK",
        url: "https://www.linkedin.com/jobs/search?keywords=Sales Business&geoId=101165590&f_TPR=&f_WT=2&f_E=2%2C3%2C4%2C5&position=1&pageNum=0",
    },
    {
        id: "41",
        category: "Administrative",
        region: "UK",
        url: "https://www.linkedin.com/jobs/search?keywords=Administrative&geoId=101165590&f_TPR=&f_WT=2&f_E=2%2C3%2C4%2C5&position=1&pageNum=0",
    },
]*/



export const JOB_REGIONS = [
    {
        regionLabel: "EU",
        regionId: "91000002"    
    },
    {
        regionLabel: "UK",
        regionId: "101165590"
    },
    {
        regionLabel: "Australia",
        regionId: "101452733"
    },
    {
        regionLabel: "USA",
        regionId: "103644278"
    },
] as {regionLabel: string, regionId: string}[];

export const JOB_CATEGORIES = [
    {
        jobCategoryLabel: "Software Engineer",
        jobCategoryQuery: "Software Engineer",
    }, 
    {
        jobCategoryLabel: "Marketing",
        jobCategoryQuery: "Marketing",
    },
    {
        jobCategoryLabel: "Customer Service",
        jobCategoryQuery: "Customer Service",
    },
    {
        jobCategoryLabel: "Design",
        jobCategoryQuery: "Design",
    },
    {
        jobCategoryLabel: "Finance",
        jobCategoryQuery: "Finance",
    }, 
    {
        jobCategoryLabel: "HR",
        jobCategoryQuery: "HR",
    }, 
    {
        jobCategoryLabel: "Project Management",
        jobCategoryQuery: "Project Manager",
    },
    {
        jobCategoryLabel: "Data Analysis",
        jobCategoryQuery: "Data Analysis",
    }, 
    {
        jobCategoryLabel: "Sales & Business",
        jobCategoryQuery: "Sales Business",
    },
    {
        jobCategoryLabel: "Administrative",
        jobCategoryQuery: "Administrative",
    },
] as {jobCategoryLabel: string, jobCategoryQuery: string}[];


export const ALLOWED_WORK_MODE = ["REMOTE"] as JobWorkModeType[];


export class CustomJobsDiscoverUtils {


    static async importJobsFromRemoteJobsHub(): Promise<JobDetailsModel[]> {
        /*const listJobs: JobDetailsModel[] = (await axios.get(`https://us-central1-remotejobshub-app.cloudfunctions.net/getLatestJobsFunction`)).data;
        return listJobs;*/
        return [];
    }

    static getIsValidRule() {
        return undefined;
    }

}