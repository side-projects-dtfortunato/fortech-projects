import * as functions from "firebase-functions";
import {BackendSharedUtils} from "../../utils/backend-shared-utils";
import {getQuickResponse} from "../../model-data/api-response.model";
import {RealtimeDbUtils} from "../../utils/realtime-db.utils";
import {SharedFirestoreCollectionDB, SharedFirestoreDocsDB} from "../../model-data/shared-firestore-collections";
import {WEBAPP_CONFIGS} from "../../../configs/webapp.configs";
import {MailerSendUtils} from "../../utils/email-utils/email-sender-utils";

export const newsletterSubscribe = functions.https.onCall(
    async (data: { email: string },
     context) => {

        if (!BackendSharedUtils.isValidEmail(data.email) && await MailerSendUtils.verifyEmail(data.email)) {
            return getQuickResponse(false, null, "This Email is invalid");
        }

        const id: string = BackendSharedUtils.string_to_slug(data.email);
        await RealtimeDbUtils.writeData({
            collectionName: SharedFirestoreCollectionDB.NewsletterCatalog,
            docId: SharedFirestoreDocsDB.NewsletterCatalog.SubscribedUsers,
            subPaths: [BackendSharedUtils.string_to_slug(data.email)],
            data: {
                name: data.email,
                email: data.email,
                nicheSitesLoggedIn: [WEBAPP_CONFIGS.SITE_NAME],
                userId: id,
            },
            merge: true
        });

        return getQuickResponse(true);
    });