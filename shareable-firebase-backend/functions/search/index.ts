import * as functions from "firebase-functions";
import * as admin from "firebase-admin";
import {getQuickResponse} from "../../model-data/api-response.model";
import {BasePostDataModel, PostUserInfos} from "../../model-data/frontend-model-data/base-post-data.model";
import {SearchableItemModel} from "../../model-data/frontend-model-data/searchable-item.model";
import {SharedFirestoreCollectionDB, SharedFirestoreDocsDB} from "../../model-data/shared-firestore-collections";
const MAX_SEARCHABLE_ITEMS = 300;

export const reloadSearchableItems = functions.https.onRequest(async (request,
                                                                      response) => {
    let listPostItems: BasePostDataModel[] = [];
    let searchableItems: {[uid: string]: SearchableItemModel} = {};


    // Query all Posts based on the rank
    let query = await admin.firestore().collection(SharedFirestoreCollectionDB.PostsDetails)
        .limit(MAX_SEARCHABLE_ITEMS)
        .orderBy("rankPts", "desc").get();

    if (!query.empty) {
        listPostItems = query.docs.map((doc) => {
            return {...doc.data()} as BasePostDataModel;
        });
    }

    // Query most recent posts
    query = await admin.firestore().collection(SharedFirestoreCollectionDB.PostsDetails)
        .limit(30)
        .orderBy("updatedAt", "desc").get();
    if (!query.empty) {
        listPostItems = listPostItems.concat(query.docs.map((doc) => {
            return {...doc.data()} as BasePostDataModel;
        }));
    }

    // Generate list of user publishers
    if (listPostItems) {
        listPostItems.forEach((postItem) => {
            searchableItems = {
                ...searchableItems,
                [postItem.uid!]: {
                    uid: postItem.uid!,
                    title: postItem.title!,
                    rank: postItem.rankPts,
                    thumbnail: postItem.thumbnailUrl,
                    type: "POST",
                    tags: postItem.tags.concat(postItem.category),
                },
            };

            // Add User
            if (!Object.keys(searchableItems).includes(postItem.userCreatorId)) {
                let userInfos: PostUserInfos = postItem.listUsersInfos[postItem.userCreatorId];
                searchableItems = {
                    ...searchableItems,
                    [postItem.userCreatorId!]: {
                        uid: postItem.userCreatorId!,
                        title: userInfos.name,
                        subtitle: userInfos.subtitleLabel,
                        rank: 0,
                        thumbnail: userInfos.pictureUrl,
                        type: "USER",
                        tags: [userInfos.username!],
                    },
                };
            }
        });
    }

    // Update Searchable Doc
    const dbResponse = await admin.firestore().collection(SharedFirestoreCollectionDB.DataCatalog)
        .doc(SharedFirestoreDocsDB.DataCatalog.SearchableItems).set(searchableItems);

    response.send(getQuickResponse(true, {dbResponse}));
});