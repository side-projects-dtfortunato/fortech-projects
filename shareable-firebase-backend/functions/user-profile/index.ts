import {Log} from "../../utils/log";
import * as functions from "firebase-functions";
import * as admin from "firebase-admin";
import {getQuickResponse, getResponse} from "../../model-data/api-response.model";
import {MailerSendUtils} from "../../utils/email-utils/email-sender-utils";
import {SharedFirestoreCollectionDB, SharedFirestoreDocsDB} from "../../model-data/shared-firestore-collections";
import {SharedCentralUserDataModel} from "../../model-data/frontend-model-data/shared-central-user-data.model";
import {RealtimeDbUtils} from "../../utils/realtime-db.utils";
import {WEBAPP_CONFIGS} from "../../../configs/webapp.configs";

export const updateUserProfile = functions.https.onCall(
    (data: { centralUserData: SharedCentralUserDataModel},
     context) => {

        if (!context.auth || !context.auth.uid) return getResponse(1001, undefined, "User must be logged in to execute this action");
        const userId: string = context && context.auth && context.auth.uid ? context.auth.uid : "";
        Log.onCall(data, userId);


        return new Promise(async (resolve, reject) => {
            let batch = admin.firestore().batch();

            if (data.centralUserData.username && data.centralUserData.publicProfile && !data.centralUserData.publicProfile.name ) {
                data.centralUserData.publicProfile.name = data.centralUserData.username;
            }
            batch.set(admin.firestore().collection(SharedFirestoreCollectionDB.CentralUserData).doc(userId), data.centralUserData);

            const dbResponse = await batch.commit();

            resolve(getQuickResponse(true,  {dbResponse}));
        });
    });

export const onUpdatePublicUserProfile = functions
    .firestore
    .document(SharedFirestoreCollectionDB.CentralUserData + '/{userId}')
    .onWrite(async (data, context) => {

        // Update catalog data
        Log.onWrite(data);
        let centralUserData: SharedCentralUserDataModel | undefined;


        let isDeleted: boolean = data.after ? false : true;
        let userId: string = !isDeleted ? data.after.id : data.before.id;

        let batch = admin.firestore().batch();
        if (data && data.after.data()) {
            centralUserData = {
                ...data.after.data() as SharedCentralUserDataModel,
            };


            // Check if should validate email
            if (centralUserData.isEmailValid === undefined) {
                centralUserData = {
                    ...centralUserData,
                    isEmailValid: await MailerSendUtils.verifyEmail(centralUserData.email),
                };
            }

            // Check if should subscribe or unsubscribe newsletters
            if (centralUserData.isNewsletterSubscribed === false) {
                await RealtimeDbUtils.deleteData({
                    collectionName: SharedFirestoreCollectionDB.NewsletterCatalog,
                    docId: SharedFirestoreDocsDB.NewsletterCatalog.SubscribedUsers,
                    subPaths: [userId],
                });
                await RealtimeDbUtils.writeData({
                    collectionName: SharedFirestoreCollectionDB.NewsletterCatalog,
                    docId: SharedFirestoreDocsDB.NewsletterCatalog.UnsubscribedUsers,
                    data: {[userId]: true},
                    merge: true,
                });

            } else if (centralUserData.isEmailValid) {
                // Only subscribe if user email is valid to receive emails
                await RealtimeDbUtils.deleteData({
                    collectionName: SharedFirestoreCollectionDB.NewsletterCatalog,
                    docId: SharedFirestoreDocsDB.NewsletterCatalog.UnsubscribedUsers,
                    subPaths: [userId],
                });
                await RealtimeDbUtils.writeData({
                    collectionName: SharedFirestoreCollectionDB.NewsletterCatalog,
                    docId: SharedFirestoreDocsDB.NewsletterCatalog.SubscribedUsers,
                    subPaths: [userId],
                    data: {
                        name: centralUserData.publicProfile?.name ? centralUserData.publicProfile?.name : centralUserData.email,
                        userId: userId,
                        email: centralUserData.email,
                        nicheSitesLoggedIn: centralUserData.nicheSitesLoggedIn ? centralUserData.nicheSitesLoggedIn : [WEBAPP_CONFIGS.SITE_NAME],
                    },
                    merge: false,
                });

            }

            batch.set(admin.firestore().collection(SharedFirestoreCollectionDB.PublicUserProfile).doc(userId), {...centralUserData.publicProfile, uid: userId});

            const dbResponse = await batch.commit();
            console.log(dbResponse);
        } else {
            batch.delete(admin.firestore().collection(SharedFirestoreCollectionDB.PublicUserProfile).doc(userId));

            // Remove Newsletter Subscription
            await RealtimeDbUtils.deleteData({
                collectionName: SharedFirestoreCollectionDB.NewsletterCatalog,
                docId: SharedFirestoreDocsDB.NewsletterCatalog.SubscribedUsers,
                subPaths: [userId],
            });
            /*batch.set(admin.firestore().collection(SharedFirestoreCollectionDB.NewsletterCatalog).doc(SharedFirestoreDocsDB.NewsletterCatalog.SubscribedUsers), {
                [userId]: admin.firestore.FieldValue.delete(),
            }, {merge: true});*/

            const dbResponse = await batch.commit();
            console.log(dbResponse);
        }
    }
    );

export const checkUsernameValid = functions.https.onCall(
    (data: { username?: string},
     context) => {
        if (!context.auth || !context.auth.uid) return getQuickResponse(false, undefined, "User not logged in");
        const userId = context && context.auth && context.auth.uid ? context.auth.uid : "";
        Log.onCall(data, userId);
        console.log("UserId: ", userId);


        return new Promise(async (resolve, reject) => {
            if (!data.username || data.username.length < 5) {
                resolve(getQuickResponse(false, null, "Username is invalid or too short"));
            }

            const results = await admin.firestore().collection(SharedFirestoreCollectionDB.PublicUserProfile).where("username", "==", data.username).limit(1).get();

            if (results.empty) {
                resolve(getQuickResponse(true));
                return;
            } else {
                // Check if the result is from the current user

                if (userId === results.docs[0].id) {
                    resolve(getQuickResponse(true));
                    return;
                } else {
                    resolve(getQuickResponse(false, null, "Username already exists or is invalid. Please, try a different one"));
                    return;
                }
            }
        });
    });
