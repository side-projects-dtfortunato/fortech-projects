import * as functions from "firebase-functions";
import {getDocumentData} from "../../../shareable-firebase-backend/utils/utils";
import {
    SharedFirestoreCollectionDB,
    SharedFirestoreDocsDB
} from "../../../shareable-firebase-backend/model-data/shared-firestore-collections";
import {RealtimeDbUtils} from "../../../shareable-firebase-backend/utils/realtime-db.utils";
import {getQuickResponse} from "../../../shareable-firebase-backend/model-data/api-response.model";

export const migrateNewsletterSubscribersToRealtime = functions.https.onRequest(async (request,
                                                                              response) => {
    const newSubscribers: {[userId: string]: any} = await getDocumentData(SharedFirestoreCollectionDB.NewsletterCatalog, SharedFirestoreDocsDB.NewsletterCatalog.SubscribedUsers);
    const newUnSubscribers: {[userId: string]: any} = await getDocumentData(SharedFirestoreCollectionDB.NewsletterCatalog, SharedFirestoreDocsDB.NewsletterCatalog.UnsubscribedUsers);


    const finalSubscribedUsers: {[userId: string]: any} = {
        ...newSubscribers,
    };
    const finalUnsubscribedUsers: {[userId: string]: any} = {
        ...newUnSubscribers,
    };

    await RealtimeDbUtils.writeData({
        collectionName: SharedFirestoreCollectionDB.NewsletterCatalog,
        docId: SharedFirestoreDocsDB.NewsletterCatalog.SubscribedUsers,
        data: finalSubscribedUsers,
        merge: true,
    });
    await RealtimeDbUtils.writeData({
        collectionName: SharedFirestoreCollectionDB.NewsletterCatalog,
        docId: SharedFirestoreDocsDB.NewsletterCatalog.UnsubscribedUsers,
        data: finalUnsubscribedUsers,
        merge: true,
    });

    response.send(getQuickResponse(true));
});