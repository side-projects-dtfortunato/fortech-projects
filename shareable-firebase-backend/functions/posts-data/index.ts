import * as functions from "firebase-functions";
import {Log} from "../../utils/log";
import * as admin from "firebase-admin";
import {getQuickResponse, getResponse} from "../../model-data/api-response.model";
import {getDocumentData} from "../../utils/utils";
import {MetaData} from "metadata-scraper";
import {ImagekitHelper} from "../../utils/imagekit.helper";
import {
    BasePostDataModel,
    PostDataUtils,
    PushPostDataModel
} from "../../model-data/frontend-model-data/base-post-data.model";
import {SharedFirestoreCollectionDB, SharedFirestoreDocsDB} from "../../model-data/shared-firestore-collections";
import {UserProfileUtils} from "../../model-data/frontend-model-data/shared-public-userprofile.model";
import {SharedCentralUserDataModel} from "../../model-data/frontend-model-data/shared-central-user-data.model";
import {EmailDestinationModel, MailerSendUtils} from "../../utils/email-utils/email-sender-utils";
import {EmailTemplateDigestUtils} from "../../utils/email-utils/email-template-digest-v2.utils";
import {WEBAPP_CONFIGS} from "../../../configs/webapp.configs";
import {SharedNewsletterUserModel} from "../../model-data/frontend-model-data/shared-newsletter-user.model";
import {RealtimeDbUtils} from "../../utils/realtime-db.utils";
import {EmailBulkDeliveryModel} from "../../model-data/email-builk-delivery.model";
import {BasePostComment} from "../../model-data/base-post-comment.model";
const getMetaData = require('metadata-scraper')


export const pushPostData = functions.runWith({
    memory: "1GB",
    timeoutSeconds: 300,
}).https.onCall(
    (data: { postData: PushPostDataModel, isCreation: boolean, thumbnailBase?: string, isDev?: boolean },
     context) => {

        if (!context.auth || !context.auth.uid) return getResponse(1001, undefined, "User must be logged in to execute this action");
        const userId: string = context && context.auth && context.auth.uid ? context.auth.uid : "";
        Log.onCall(data, userId);


        return new Promise(async (resolve, reject) => {

            let fullPostData: BasePostDataModel;

            if (data.isCreation) {
                let centralUserData: SharedCentralUserDataModel | undefined = await getDocumentData<SharedCentralUserDataModel>(SharedFirestoreCollectionDB.CentralUserData, userId);

                if (!centralUserData) {
                    resolve(getQuickResponse(false, null, "User data not found"));
                    return;
                }

                if (!centralUserData.publicProfile || !centralUserData.publicProfile.username || centralUserData.publicProfile.username.length === 0) {
                    resolve(getQuickResponse(false, null, "You need to set a username to publish a post"));
                    return;
                }

                const postId: string = data.postData.uid ? data.postData.uid : admin.firestore().collection(SharedFirestoreCollectionDB.PostsDetails).doc().id;
                // If received thumbnailBase64, should upload it in ImageKit
                if (data.thumbnailBase) {
                    const response = await ImagekitHelper.uploadImageFromUrl(data.thumbnailBase, postId, data.isDev ? "articles-dev" : "articles", data.postData.tags);
                    if (response && response.fileUrl) {
                        data.postData.thumbnailUrl = response.fileUrl;
                    }
                }

                // Initialize Post Data
                fullPostData = {
                    ...data.postData as PushPostDataModel,
                    uid: postId,
                    responses: {
                        reactions: {},
                        comments: {},
                    },
                    listUsersInfos: {
                        [userId]: UserProfileUtils.generateUserShortProfile(centralUserData.publicProfile),
                    },
                    updatedAt: Date.now(),
                    publishedAt: Date.now(),
                    userCreatorId: userId,
                    openViews: 0,
                    rankPts: 0,
                    numComments: 0,
                    numReactions: 0,

                };
            } else {
                // Get Detailed data
                fullPostData = await getDocumentData(SharedFirestoreCollectionDB.PostsDetails, data.postData.uid!);

                if (!fullPostData) {
                    resolve(getQuickResponse(false, null, "Post not found"));
                    return;
                } else if (fullPostData.userCreatorId !== userId) {
                    resolve(getQuickResponse(false, null, "User not authorized to update this post"));
                    return;
                } else {
                    fullPostData = {
                        ...fullPostData,
                        ...data.postData,
                        updatedAt: Date.now(),
                    };
                }
            }

            if (fullPostData) {
                let batch = admin.firestore().batch();

                // Add data in the Posts Collection
                batch.set(admin.firestore().collection(SharedFirestoreCollectionDB.PostsDetails).doc(fullPostData.uid!), {...fullPostData}, {merge: true});

                // Add Posts by tag
                if (fullPostData.tags) {
                    let popularTagsIncrement: any = {};
                    fullPostData.tags.forEach((tag) => {
                        popularTagsIncrement = {
                            ...popularTagsIncrement,
                            [tag.toLowerCase()]: admin.firestore.FieldValue.increment(1),
                        };
                        batch.set(admin.firestore().collection(SharedFirestoreCollectionDB.DataCatalog).doc(SharedFirestoreDocsDB.DataCatalog.PrefixTagType + tag.toLowerCase()), {[fullPostData.uid!]: PostDataUtils.convertPostToShortData(fullPostData)}, {merge: true});
                    });
                    batch.set(admin.firestore().collection(SharedFirestoreCollectionDB.DataCatalog).doc(SharedFirestoreDocsDB.DataCatalog.PopularTags), popularTagsIncrement, {merge: true});
                }

                // Clean user drafts
                batch.delete(admin.firestore().collection(SharedFirestoreCollectionDB.DraftUserPosts).doc(userId));

                const dbResponse = await batch.commit();
                resolve(getQuickResponse(true, {dbResponse}));
            }
        });
    });


export const pushPostComment = functions.https.onCall(
    (data: { postId: string, replyCommentId?: string, message: string },
     context) => {
        if (!context.auth || !context.auth.uid) return getQuickResponse(false, undefined, "User not authorized");
        const userId = context && context.auth && context.auth.uid ? context.auth.uid : "";
        Log.onCall(data, userId);


        return new Promise(async (resolve, reject) => {

            let postData: BasePostDataModel = await getDocumentData(SharedFirestoreCollectionDB.PostsDetails, data.postId);

            let dataToUpdate: any = {};

            // Add user infos to list user infos
            if (!postData.listUsersInfos[userId]) {
                let userInfos: SharedCentralUserDataModel = await getDocumentData<SharedCentralUserDataModel>(SharedFirestoreCollectionDB.CentralUserData, userId);

                if (!userInfos.publicProfile || !userInfos.publicProfile.name || userInfos.publicProfile.name.length === 0) {
                    resolve(getQuickResponse(false, null, "You need to set a username before posting a comment"));
                    return;
                }
                dataToUpdate = {
                    listUsersInfos: {
                        [userId]: UserProfileUtils.generateUserShortProfile(userInfos.publicProfile!),
                    },
                };
            }


            let postCommentData: BasePostComment = {
                uid: userId + "_" + Date.now(),
                message: data.message,
                reactions: {},
                userCreatorId: userId,
                replies: {},
            };

            // Add Reply of a Comment
            if (data.replyCommentId) {
                // It's a reply of a comment
                if (postData.responses.comments[data.replyCommentId]) {
                    dataToUpdate = {
                        ...dataToUpdate,
                        responses: {
                            comments: {
                                [data.replyCommentId!]: {
                                    replies: {
                                        [postCommentData.uid!]: postCommentData,
                                    },
                                },
                            },
                        },
                    };
                }
            } else {
                // Add a comment
                dataToUpdate = {
                    ...dataToUpdate,
                    responses: {
                        comments: {
                            [postCommentData.uid!]: postCommentData,
                        },
                    },
                    numComments: admin.firestore.FieldValue.increment(1),
                };
            }

            // Update rank pts
            dataToUpdate = {
                ...dataToUpdate,
                rankPts: admin.firestore.FieldValue.increment(3),
            };

            // Update Data
            let batch = admin.firestore().batch();

            batch.set(admin.firestore().collection(SharedFirestoreCollectionDB.PostsDetails).doc(data.postId), dataToUpdate, {merge: true});

            // Email Notification
            let listUserIds: string[] = Object.keys(postData.listUsersInfos);
            if (listUserIds && listUserIds.length > 0) {
                let dailyDigestData: any = {};
                listUserIds.forEach((userId) => {
                    dailyDigestData = {
                        ...dailyDigestData,
                        [userId]: {
                            newCommentsPosts: {
                                [postData.uid!]: PostDataUtils.convertPostToShortData(postData),
                            },
                        },
                    };
                });

                // Schedule an email to be send soon
                const htmlContent: string = EmailTemplateDigestUtils.generateHtmlContent(
                    {
                        logoUrl: WEBAPP_CONFIGS.LOGO_URL,
                        siteUrl: WEBAPP_CONFIGS.WEBSITE_URL,
                        headerTitle: "Remote-Work.app",
                        headerSubtitle: "A post that you are following received a new comment",
                        listItemsContainer: [
                            {
                                title: "New Post Comment",
                                listItems: [
                                    EmailTemplateDigestUtils.generateListItemPost(postData),
                                ],
                            },
                        ],
                        footer: {
                            address: WEBAPP_CONFIGS.COMPANY_ADDRESS,
                            email: WEBAPP_CONFIGS.COMPANY_EMAIL,
                            unsubscribeLink: WEBAPP_CONFIGS.UNSUBSCRIBE_URL,
                        },
                        listSocialNetworks: [
                            {
                                link: WEBAPP_CONFIGS.WEBSITE_URL,
                                iconUrl: WEBAPP_CONFIGS.LOGO_URL,
                            },
                        ],
                    }
                );

                // Generate List of destinations
                let subscribedUsers: { [userId: string]: SharedNewsletterUserModel } | undefined = await RealtimeDbUtils.getData({
                    collectionName: SharedFirestoreCollectionDB.NewsletterCatalog,
                    docId: SharedFirestoreDocsDB.NewsletterCatalog.SubscribedUsers,
                });
                if (subscribedUsers) {
                    const listDestinationEmails: EmailDestinationModel[] = listUserIds
                        .filter((userId) => {
                            return Object.keys(subscribedUsers!).includes(userId);
                        })
                        .map((userId) => {
                            return {
                                uid: userId,
                                name: subscribedUsers![userId].name,
                                email: subscribedUsers![userId].email,
                            };
                        });

                    if (listDestinationEmails) {
                        // Store to later be delivered
                        await RealtimeDbUtils.writeData({
                            collectionName: SharedFirestoreCollectionDB.RTEmailBulkToDeliver,
                            docId: Date.now().toString(),
                            data: {
                                bodyHtml: htmlContent,
                                destinationsEmail: listDestinationEmails,
                                subject: WEBAPP_CONFIGS.SITE_NAME + ": New comment received",
                                sentFrom: {
                                    name: WEBAPP_CONFIGS.SITE_NAME,
                                    email: "no-reply@" + WEBAPP_CONFIGS.SITE_NAME,
                                },
                            } as EmailBulkDeliveryModel,
                            merge: false,
                        });
                    }
                }

            }


            const dbResponse = await batch.commit();

            resolve(getQuickResponse(true, {dbResponse}));
        });

    });


// Implement reaction to post

export const pushPostReaction = functions.https.onCall(
    (data: { postId: string, reactionType?: string, isToAdd: boolean },
     context) => {
        if (!context.auth || !context.auth.uid) return getQuickResponse(false, undefined, "User not authorized");
        const userId = context && context.auth && context.auth.uid ? context.auth.uid : "";
        Log.onCall(data, userId);


        return new Promise(async (resolve, reject) => {
            const postData: BasePostDataModel = await getDocumentData(SharedFirestoreCollectionDB.PostsDetails, data.postId);

            let dataToUpdate = {
                responses: {
                    reactions: {
                        [userId]: data.isToAdd ? data.reactionType : admin.firestore.FieldValue.delete(),
                    },

                },
                numReactions: admin.firestore.FieldValue.increment(data.isToAdd ? 1 : -1),
                rankPts: admin.firestore.FieldValue.increment(data.isToAdd ? 1 : -1),
            }


            // Update Data
            let batch = admin.firestore().batch();

            batch.set(admin.firestore().collection(SharedFirestoreCollectionDB.PostsDetails).doc(data.postId), dataToUpdate, {merge: true});

            // Notify creator

            // Generate List of destinations
            let subscribedUsers: { [userId: string]: SharedNewsletterUserModel } | undefined = await RealtimeDbUtils.getData({
                collectionName: SharedFirestoreCollectionDB.NewsletterCatalog,
                docId: SharedFirestoreDocsDB.NewsletterCatalog.SubscribedUsers,
            });
            if (subscribedUsers && Object.keys(subscribedUsers).includes(postData.userCreatorId)) {
                const listDestinationEmails: EmailDestinationModel[] = [{
                    email: subscribedUsers[postData.userCreatorId].email,
                    name: subscribedUsers[postData.userCreatorId].name,
                    uid: postData.userCreatorId,
                }];

                // Schedule an email to be send soon
                const htmlContent: string = EmailTemplateDigestUtils.generateHtmlContent(
                    {
                        logoUrl: WEBAPP_CONFIGS.LOGO_URL,
                        siteUrl: WEBAPP_CONFIGS.WEBSITE_URL,
                        headerTitle: "Remote-Work.app",
                        headerSubtitle: "Your Post Received an upvote",
                        listItemsContainer: [
                            {
                                title: "Upvoted Post",
                                listItems: [
                                    EmailTemplateDigestUtils.generateListItemPost(postData),
                                ],
                            },
                        ],
                        footer: {
                            address: WEBAPP_CONFIGS.COMPANY_ADDRESS,
                            email: WEBAPP_CONFIGS.COMPANY_EMAIL,
                            unsubscribeLink: WEBAPP_CONFIGS.UNSUBSCRIBE_URL,
                        },
                        listSocialNetworks: [
                            {
                                link: WEBAPP_CONFIGS.WEBSITE_URL,
                                iconUrl: WEBAPP_CONFIGS.LOGO_URL,
                            },
                        ],
                    }
                );

                if (listDestinationEmails) {
                    // Store to later be delivered
                    await MailerSendUtils.sendEmail({
                        bodyHtml: htmlContent,
                        destinationsEmail: listDestinationEmails,
                        subject: WEBAPP_CONFIGS.SITE_NAME + ": Your Post Was Upvoted",
                        sentFrom: {
                            name: WEBAPP_CONFIGS.SITE_NAME,
                            email: "no-reply@" + WEBAPP_CONFIGS.SITE_NAME,
                        },
                    });
                }
            }

            /*batch.set(admin.firestore().collection(SharedFirestoreCollectionDB.NewsletterCatalog).doc(SharedFirestoreDocsDB.NewsletterCatalog.DailyUserDigest), {
                [postData.userCreatorId]: {
                    newUpvotes: {
                        [data.postId]: PostDataUtils.convertPostToShortData(postData),
                    },
                },
            }, {merge: true});*/

            const dbResponse = await batch.commit();

            resolve(getQuickResponse(true, {dbResponse}));
        });

    });

export const updatePostBookmark = functions.https.onCall(
    (data: {postId: string, postData: BasePostDataModel, addBookmark: boolean},
     context) => {
        if (!context.auth || !context.auth.uid) return getQuickResponse(false, undefined, "User not logged in");
        const userId = context && context.auth && context.auth.uid ? context.auth.uid : "";
        Log.onCall(data, userId);


        return new Promise(async (resolve, reject) => {

            let batch = admin.firestore().batch();

            // User Bookmarks
            batch.set(admin.firestore().collection(SharedFirestoreCollectionDB.UserBookmarks).doc(userId), {
                posts: {
                    [data.postId]: data.addBookmark ? PostDataUtils.convertPostToShortData(data.postData) : admin.firestore.FieldValue.delete(),
                },
            }, {merge: true});

            // Post bookmarked by
            batch.set(admin.firestore().collection(SharedFirestoreCollectionDB.PostsDetails).doc(data.postId), {
                bookmarkedBy: {
                    [userId]: data.addBookmark ? true : admin.firestore.FieldValue.delete(),
                },
            }, {merge: true});


            const dbResponse = await batch.commit();

            resolve(getQuickResponse(true, {dbResponse}));
        });
    });


export const openViewPost = functions.https.onCall(
    (data: { postId: string },
     context) => {
        if (!context.auth || !context.auth.uid) return getQuickResponse(false, undefined, "User not authorized");
        const userId = context && context.auth && context.auth.uid ? context.auth.uid : "";
        Log.onCall(data, userId);


        return new Promise(async (resolve, reject) => {
            const dbResponse = await admin.firestore().collection(SharedFirestoreCollectionDB.PostsDetails).doc(data.postId).set({
                openViews: admin.firestore.FieldValue.increment(1),
                rankPts: admin.firestore.FieldValue.increment(0.05),
            }, {merge: true});

            resolve(getQuickResponse(true, {dbResponse}));
        });
    });

// Implement onCall when a post is updated to update the whole catalog data
export const onPostDataUpdated = functions
    .firestore
    .document(SharedFirestoreCollectionDB.PostsDetails + '/{postId}')
    .onWrite(async (data, context) => {

        // Update catalog data
        Log.onWrite(data);
        let postData: BasePostDataModel | undefined;


        let isDeleted: boolean = data.after ? false : true;
        let postId: string = !isDeleted ? data.after.id : data.before.id;

        if (data && data.after.data()) {
            postData = {
                ...(isDeleted ? data.before.data() : data.after.data()) as BasePostDataModel,
                uid: postId,
            };
        }

        // Update catalog data if required
        if (postData) {
            let batch = admin.firestore().batch();
            let shortenPostData: BasePostDataModel = {
                ...postData,
                detailsPostData: {},
                responses: {
                    ...postData.responses,
                    comments: {},
                    reactions: {},
                },
            };

            // Update catalog data
            if (postData.catalogDocs) {

                shortenPostData.catalogDocs?.forEach((catalogId: string) => {

                    // Update item into data catalog
                    batch.set(admin.firestore().collection(SharedFirestoreCollectionDB.DataCatalog).doc(catalogId), {
                        [shortenPostData.uid!]: isDeleted ? admin.firestore.FieldValue.delete() : shortenPostData,
                    }, {merge: true});

                });
            }

            // User creator catalog
            batch.set(admin.firestore().collection(SharedFirestoreCollectionDB.UserPostsCreated).doc(postData!.userCreatorId), {
                [shortenPostData.uid!]: isDeleted ? admin.firestore.FieldValue.delete() : shortenPostData,
            }, {merge: true});

            const dbResponse = await batch.commit();
            console.log(dbResponse);
        }

    });


export const exportUrlMetadata = functions.https.onCall(
    (data: { url: string },
     context) => {
        // if (!context.auth || !context.auth.uid) return getQuickResponse(false, undefined, "User not authorized");
        const userId = context && context.auth && context.auth.uid ? context.auth.uid : "";
        Log.onCall(data, userId);


        return new Promise(async (resolve, reject) => {
            getMetaData(data.url).then((res: MetaData) => {
                resolve(res);
            }).catch((err: any) => {
                resolve(undefined);
            });
        });
    });