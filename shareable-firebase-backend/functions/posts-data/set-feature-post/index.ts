import {getQuickResponse} from "../../../model-data/api-response.model";
import {Log} from "../../../utils/log";
import {
    SharedCentralUserDataModel,
    UserRole
} from "../../../model-data/frontend-model-data/shared-central-user-data.model";
import {getDocumentData} from "../../../utils/utils";
import {SharedFirestoreCollectionDB} from "../../../model-data/shared-firestore-collections";
import * as functions from "firebase-functions";
import * as admin from "firebase-admin";

export const setFeaturePost = functions.https.onCall(
    (data: { postId: string, isFeatured: boolean},
     context) => {
        if (!context.auth || !context.auth.uid) return getQuickResponse(false, undefined, "User not authorized");
        const userId = context && context.auth && context.auth.uid ? context.auth.uid : "";
        Log.onCall(data, userId);

        return new Promise(async (resolve, reject) => {
            const centralUserData: SharedCentralUserDataModel = await getDocumentData(SharedFirestoreCollectionDB.CentralUserData, userId);

            if (centralUserData.userRole !== UserRole.ADMIN) {
                resolve(getQuickResponse(false, null, "Not Authorized"));
            }

            await admin.firestore().collection(SharedFirestoreCollectionDB.PostsDetails).doc(data.postId).set({
                isFeatured: data.isFeatured,
            }, {merge: true});

            resolve(getQuickResponse(true));
            return;
        });
    });