import * as functions from "firebase-functions";
import * as admin from "firebase-admin";
import {getQuickResponse, getResponse} from "../../../model-data/api-response.model";
import {Log} from "../../../utils/log";
import {BasePostDataModel} from "../../../model-data/frontend-model-data/base-post-data.model";
import {getDocumentData} from "../../../utils/utils";
import {SharedFirestoreCollectionDB, SharedFirestoreDocsDB} from "../../../model-data/shared-firestore-collections";
import {
    SharedCentralUserDataModel,
    UserRole
} from "../../../model-data/frontend-model-data/shared-central-user-data.model";

export const removePostData = functions.https.onCall(
    (data: { postId: string},
     context) => {

        if (!context.auth || !context.auth.uid) return getResponse(1001, undefined, "User must be logged in to execute this action");
        const userId: string = context && context.auth && context.auth.uid ? context.auth.uid : "";
        Log.onCall(data, userId);


        return new Promise(async (resolve, reject) => {

            // Get Post details
            const postData: BasePostDataModel = await getDocumentData<BasePostDataModel>(SharedFirestoreCollectionDB.PostsDetails, data.postId);
            const centralUserData: SharedCentralUserDataModel = await getDocumentData<SharedCentralUserDataModel>(SharedFirestoreCollectionDB.CentralUserData, userId);

            if (postData) {

                // Check if user is allowed to delete this post
                if (centralUserData.userRole !== UserRole.ADMIN && postData.userCreatorId !== userId) {
                    // It's not allowed
                    resolve(getQuickResponse(false, null, "You are not allowed to remove this post."));
                    return;
                }

                // Remove all the references to this post
                let batch = admin.firestore().batch();

                // Add Posts by tag
                if (postData.tags) {
                    postData.tags.forEach((tag) => {
                        batch.set(admin.firestore().collection(SharedFirestoreCollectionDB.DataCatalog).doc(SharedFirestoreDocsDB.DataCatalog.PrefixTagType + tag.toLowerCase()), {[data.postId]: admin.firestore.FieldValue.delete()}, {merge: true});
                    });
                }

                // Remove from catalogs
                postData.catalogDocs?.forEach((catalogId: string) => {

                    // Update item into data catalog
                    batch.set(admin.firestore().collection(SharedFirestoreCollectionDB.DataCatalog).doc(catalogId), {
                        [data.postId]: admin.firestore.FieldValue.delete(),
                    }, {merge: true});

                });

                // Remove from user list posts
                batch.set(admin.firestore().collection(SharedFirestoreCollectionDB.UserPostsCreated).doc(postData!.userCreatorId), {
                    [data.postId]: admin.firestore.FieldValue.delete(),
                }, {merge: true});


                batch.commit().then((result) => {
                    resolve(getQuickResponse(true, result));
                }).catch((err) => {
                    resolve(getQuickResponse(false, err));
                });
            }
        });

    });