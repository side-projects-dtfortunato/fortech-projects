import * as functions from "firebase-functions";
import {Log} from "../../utils/log";
import * as admin from "firebase-admin";
import {getQuickResponse} from "../../model-data/api-response.model";
import {getDocumentData} from "../../utils/utils";
import {
    UserProfileUtils,
    UserShortProfileModel
} from "../../model-data/frontend-model-data/shared-public-userprofile.model";
import {
    SharedFirestoreCollectionDB,
    SharedFirestoreDocsDB
} from "../../model-data/shared-firestore-collections";
import {SharedCentralUserDataModel} from "../../model-data/frontend-model-data/shared-central-user-data.model";

export const updateFollowingUser = functions.https.onCall(
    (data: { userIdToFollow: string, startFollowing: boolean },
     context) => {
        if (!context.auth || !context.auth.uid) return getQuickResponse(false, undefined, "User not logged in");
        const userId = context && context.auth && context.auth.uid ? context.auth.uid : "";
        Log.onCall(data, userId);


        return new Promise(async (resolve, reject) => {
            let batch = admin.firestore().batch();

            if (data.startFollowing) {
                let userToFollow: SharedCentralUserDataModel = await getDocumentData(SharedFirestoreCollectionDB.CentralUserData, data.userIdToFollow);
                if (!userToFollow || !userToFollow.publicProfile) {
                    resolve(getQuickResponse(false, {}, "User to follow not found"));
                    return;
                }

                let userFollowing: SharedCentralUserDataModel = await getDocumentData(SharedFirestoreCollectionDB.CentralUserData, userId);
                if (!userFollowing || !userFollowing.publicProfile) {
                    resolve(getQuickResponse(false, {}, "User following not found"));
                    return;
                }

                let userToFollowProfile: UserShortProfileModel = UserProfileUtils.generateUserShortProfile(userToFollow!.publicProfile!);
                let userFollowingProfile: UserShortProfileModel = UserProfileUtils.generateUserShortProfile(userFollowing!.publicProfile!);
                // Add email
                userFollowingProfile.email = userFollowing.email;

                // Update db
                batch.set(admin.firestore().collection(SharedFirestoreCollectionDB.UserFollowing).doc(userId), {[data.userIdToFollow]: userToFollowProfile}, {merge: true});
                batch.set(admin.firestore().collection(SharedFirestoreCollectionDB.CentralUserData).doc(userId), {
                    usersFollowing: {
                        [data.userIdToFollow]: true,
                    },
                }, {merge: true});

                batch.set(admin.firestore().collection(SharedFirestoreCollectionDB.UserFollowers).doc(data.userIdToFollow), {[userId]: userFollowingProfile}, {merge: true});

                batch.set(admin.firestore().collection(SharedFirestoreCollectionDB.NewsletterCatalog)
                    .doc(SharedFirestoreDocsDB.NewsletterCatalog.DailyUserDigest), {
                    [data.userIdToFollow]: {
                        newFollowers: {
                            [userFollowing.uid!]: userFollowing,
                        },
                    }
                }, {merge: true});

            } else {
                // Remove from following
                batch.set(admin.firestore().collection(SharedFirestoreCollectionDB.UserFollowing).doc(userId), {[data.userIdToFollow]: admin.firestore.FieldValue.delete()}, {merge: true});
                batch.set(admin.firestore().collection(SharedFirestoreCollectionDB.CentralUserData).doc(userId), {
                    usersFollowing: {
                        [data.userIdToFollow]: admin.firestore.FieldValue.delete(),
                    },
                }, {merge: true});

                // Remove from followed
                batch.set(admin.firestore().collection(SharedFirestoreCollectionDB.UserFollowers).doc(userId), {[userId]: admin.firestore.FieldValue.delete()}, {merge: true});
            }

            const dbResponse = batch.commit();

            resolve(getQuickResponse(true, {dbResponse}));
        });
    });