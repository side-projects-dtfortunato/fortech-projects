import * as functions from "firebase-functions";
import {Log} from "../../utils/log";
import * as admin from "firebase-admin";
import {getQuickResponse} from "../../model-data/api-response.model";

export const requestDocumentChanges = functions.https.onCall(
    (data: {docData?: any, collectionName: string, docId?: string, methodType: "CREATE" | "MERGE" | "DELETE"},
     context) => {

        if (!context.auth || !context.auth.uid) return getQuickResponse(false, undefined, "User not authorized");
        const userId = context && context.auth && context.auth.uid ? context.auth.uid : "";
        Log.onCall(data, userId);


        return new Promise(async (resolve, reject) => {
            let batch = admin.firestore().batch();

            switch (data.methodType) {
                case "CREATE":
                    if (data.docData) {
                        batch.create(admin.firestore().collection(data.collectionName).doc(), data.docData);
                    } else {

                        resolve(getQuickResponse(false, null, "Doc data id was missing"));
                    }
                    break;
                case "DELETE":
                    if (data.docId) {
                        batch.delete(admin.firestore().collection(data.collectionName).doc(data.docId));
                    } else {
                        resolve(getQuickResponse(false, null, "Doc id was missing"));
                    }
                    break;
                case "MERGE":
                    if (data.docId && data.docData) {
                        batch.set(admin.firestore().collection(data.collectionName).doc(data.docId), data.docData, {merge: true});
                    } else {
                        resolve(getQuickResponse(false, null, "Doc id or doc data was missing"));
                    }
                    break;
            }

            const dbResponse = await batch.commit();

            resolve(getQuickResponse(true, {dbResponse}));
        });
    });