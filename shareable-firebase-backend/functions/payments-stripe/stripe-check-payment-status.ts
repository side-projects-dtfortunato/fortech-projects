import * as functions from "firebase-functions";
import {StripeUtils} from "../../utils/payments/stripe-utils";
import {BackendSharedUtils} from "../../utils/backend-shared-utils";
import {WEBAPP_CONFIGS} from "../../../configs/webapp.configs";
import {StripePaymentStatus} from "../../model-data/custom-stripe.model";

export const stripeCheckPaymentStatus = functions.https.onCall(
    async (data: { sessionId: string},
     context) => {

        const paymentStatus: StripePaymentStatus = await StripeUtils.confirmPayment({
            stripeApi:  BackendSharedUtils.isProjectProduction() ? WEBAPP_CONFIGS.STRIPE_API.prod : WEBAPP_CONFIGS.STRIPE_API.dev,
            sessionId: data.sessionId,
        });

        return paymentStatus;
    });