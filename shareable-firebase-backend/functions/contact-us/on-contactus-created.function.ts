import * as functions from "firebase-functions";
import {SharedFirestoreCollectionDB} from "../../model-data/shared-firestore-collections";
import {JobDetailsModel} from "../../base-projects/jobs-discover/shared-jobs-discover.data";
import {MailerSendUtils} from "../../utils/email-utils/email-sender-utils";
import {WEBAPP_CONFIGS} from "../../../configs/webapp.configs";

export const onContactUsCreated = functions
    .firestore
    .document(SharedFirestoreCollectionDB.ContactUs + '/{contactId}')
    .onCreate(async (data, context) => {

        let contactUsData: any | undefined = data.data();

        if (contactUsData) {
            await MailerSendUtils.sendEmail({
                subject: "ContactUs Received",
                sentFrom: {
                    name: WEBAPP_CONFIGS.SITE_NAME,
                    email: WEBAPP_CONFIGS.COMPANY_EMAIL,
                },
                bodyHtml: `You received the next email contact: \n\n ${JSON.stringify(contactUsData)}`,
                destinationsEmail: [
                    {
                        uid: "Admin",
                        email: "davidtfortunato@gmail.com",
                        name: "David",
                    },
                    {
                        uid: "Admin1",
                        email: "fortuly.apps@gmail.com",
                        name: "Fortuly Apps",
                    },
                ]
            });
        }
        return;
    });