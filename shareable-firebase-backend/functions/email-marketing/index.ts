import * as functions from "firebase-functions";
import * as admin from "firebase-admin";
import {EmailTemplateDigestUtils, ListItemsContainer} from "../../utils/email-utils/email-template-digest-v2.utils";
import {WEBAPP_CONFIGS} from "../../../configs/webapp.configs";
import {EmailDestinationModel, MailerSendUtils} from "../../utils/email-utils/email-sender-utils";
import {getDocumentData} from "../../utils/utils";
import {getQuickResponse} from "../../model-data/api-response.model";
import {
    SharedNewsletterDailyUserDigestModel,
    SharedNewsletterUserModel
} from "../../model-data/frontend-model-data/shared-newsletter-user.model";
import {SharedFirestoreCollectionDB, SharedFirestoreDocsDB} from "../../model-data/shared-firestore-collections";
import {BasePostDataModel} from "../../model-data/frontend-model-data/base-post-data.model";
import {SharedCentralUserDataModel} from "../../model-data/frontend-model-data/shared-central-user-data.model";
import {RealtimeDbUtils} from "../../utils/realtime-db.utils";


const PUBLISHED_TIME_AGO_MILLIS = 604800000; // 7 Days
const MAX_DIGESTED_LIST_POSTS = 10;
const BULK_SEND_MAX_LENGTH = 100; // Users
/**
 * @deprecated
 */
export const generateNewsletterSubscribersQueue = functions.https.onRequest(async (request,
                                                                                   response) => {
    // Get subscribed users
    let subscribedUsers: { [userId: string]: SharedNewsletterUserModel } | undefined = await RealtimeDbUtils.getData({
        collectionName: SharedFirestoreCollectionDB.NewsletterCatalog,
        docId: SharedFirestoreDocsDB.NewsletterCatalog.SubscribedUsers,
    });

    // Push those to the queue doc
    if (subscribedUsers) {
        const dbResponse = await admin.firestore().collection(SharedFirestoreCollectionDB.NewsletterCatalog).doc(SharedFirestoreDocsDB.NewsletterCatalog.DigestSenderUsersQueue).set(subscribedUsers);

        response.send(getQuickResponse(true, {dbResponse}));
        return;
    } else {
        response.send(getQuickResponse(false, null, "Newsletter subscribers not found"));
        return;
    }
});

/**
 * @deprecated
 */
export const sendEmailWeeklyDigest = functions.https.onRequest(async (request,
                                                                      response) => {

    // Get subscribed users
    let subscribedUsers: { [userId: string]: SharedNewsletterUserModel } | undefined = await getDocumentData<any>(SharedFirestoreCollectionDB.NewsletterCatalog, SharedFirestoreDocsDB.NewsletterCatalog.DigestSenderUsersQueue);

    if (!subscribedUsers) {
        response.send(getQuickResponse(false, null, "No subscribers found"));
        return;
    }


    // Filter only 100 users
    let subscribedKeys: string[] = Object.keys(subscribedUsers);
    if (subscribedUsers && subscribedKeys.length > BULK_SEND_MAX_LENGTH) {
        let filteredSubscribedUsers: { [userId: string]: SharedNewsletterUserModel } = {};

        for (let i = 0; i < BULK_SEND_MAX_LENGTH; i++) {
            filteredSubscribedUsers = {
                ...filteredSubscribedUsers,
                [subscribedUsers[subscribedKeys[i]].userId]: subscribedUsers[subscribedKeys[i]],
            };
        }

        subscribedUsers = filteredSubscribedUsers;
    }

    // Generate list jobs by category
    let listRecentPosts: { [postId: string]: BasePostDataModel } = await getDocumentData<{ [postId: string]: BasePostDataModel }>(SharedFirestoreCollectionDB.DataCatalog, SharedFirestoreDocsDB.DataCatalog.FilterRecent);
    let listPopularPosts: BasePostDataModel[] = [];

    // Generate popular posts
    Object.values(listRecentPosts)
        .sort((p1, p2) => p2.rankPts - p1.rankPts)
        .forEach((postItem) => {
            if (listPopularPosts.length <= MAX_DIGESTED_LIST_POSTS && postItem.publishedAt > (Date.now() - PUBLISHED_TIME_AGO_MILLIS)) {
                listPopularPosts.push(postItem);
            }
        });

    // Generate Digest items
    let listDigestPosts: ListItemsContainer[] = [];

    if (listPopularPosts.length > 2) {
        listDigestPosts.push({
            title: "Recent Popular Posts",
            listItems: listPopularPosts.map((postItem) => {
                return EmailTemplateDigestUtils.generateListItemPost(postItem);
            }),
        });
    }


    if (subscribedUsers && Object.keys(subscribedUsers).length > 0) {
        if (listDigestPosts && listDigestPosts.length >= 1) {
            // Send email
            const htmlContent: string = EmailTemplateDigestUtils.generateHtmlContent(
                {
                    logoUrl: WEBAPP_CONFIGS.LOGO_URL,
                    siteUrl: WEBAPP_CONFIGS.WEBSITE_URL,
                    headerTitle: "Weekly Digest",
                    headerSubtitle: "Popular recent posts shared through our community",
                    listItemsContainer: listDigestPosts,
                    footer: {
                        address: WEBAPP_CONFIGS.COMPANY_ADDRESS,
                        email: WEBAPP_CONFIGS.COMPANY_EMAIL,
                        unsubscribeLink: WEBAPP_CONFIGS.UNSUBSCRIBE_URL,
                    },
                    listSocialNetworks: [
                        {
                            link: "https://medium.com/@journeypreneur",
                            iconUrl: "https://www.iconpacks.net/icons/2/free-medium-icon-2177-thumb.png",
                        },
                        {
                            link: "https://twitter.com/journeypreneur_",
                            iconUrl: "https://cdn-icons-png.flaticon.com/512/124/124021.png",
                        },
                    ],
                }
            );

            if (subscribedUsers) {
                let destionationsList: EmailDestinationModel[] = Object.values(subscribedUsers).map((subscriptionItem) => {
                    return {
                        uid: subscriptionItem.userId,
                        name: subscriptionItem.name,
                        email: subscriptionItem.email,
                    };
                });

                // Remove users from queue to avoid duplication
                let updateQueue: any = {};
                Object.values(subscribedUsers).forEach((user) => {
                    updateQueue = {
                        ...updateQueue,
                        [user.userId]: admin.firestore.FieldValue.delete(),
                    };
                });
                const removedUsersQueueuResponse: any = await admin.firestore().collection(SharedFirestoreCollectionDB.NewsletterCatalog).doc(SharedFirestoreDocsDB.NewsletterCatalog.DigestSenderUsersQueue).set(updateQueue, {merge: true});

                MailerSendUtils.sendEmail({
                    bodyHtml: htmlContent,
                    subject: "Weekly " + WEBAPP_CONFIGS.SITE_NAME + " Digest",
                    sentFrom: {
                        name: WEBAPP_CONFIGS.SITE_NAME,
                        email: "no-reply@" + WEBAPP_CONFIGS.SITE_NAME,
                    },
                    destinationsEmail: destionationsList,
                }).then(async (res) => {
                    response.send(getQuickResponse(true, {
                        res,
                        removedUsersQueueuResponse,
                        destinationsCounter: destionationsList.length,
                    }));
                }).catch((err) => {
                    response.send(err);
                });
            } else {
                response.send(getQuickResponse(false, null, "Not enough posts to notify"));
                return;
            }

        } else {
            response.send(getQuickResponse(false, null, "Didn't found enough digested posts"));
            return;
        }


    } else {
        response.send(getQuickResponse(false, null, "No users to send digest"));
    }

});

/**
 * @deprecated
 */
export const sendEmailDailyUserDigest = functions.https.onRequest(async (request,
                                                                         response) => {
    let dailyUserDigest: { [userId: string]: SharedNewsletterDailyUserDigestModel } = await getDocumentData(SharedFirestoreCollectionDB.NewsletterCatalog, SharedFirestoreDocsDB.NewsletterCatalog.DailyUserDigest);

    if (dailyUserDigest && Object.keys(dailyUserDigest).length > 0) {
        let subscribedUsers: { [userId: string]: SharedNewsletterUserModel } = await RealtimeDbUtils.getData({
            collectionName: SharedFirestoreCollectionDB.NewsletterCatalog,
            docId: SharedFirestoreDocsDB.NewsletterCatalog.SubscribedUsers,
        });

        if (subscribedUsers) {
            const userIdsArr: string[] = Object.keys(dailyUserDigest);
            let dataBatchUpdate: any = {};

            for (let i = 0; i < userIdsArr.length && i < BULK_SEND_MAX_LENGTH; i++) {
                let userId: string | undefined = userIdsArr[i];
                if (Object.keys(subscribedUsers).includes(userId)) {
                    let digestData: SharedNewsletterDailyUserDigestModel = dailyUserDigest[userId];
                    let digestDailyHtml: string = EmailTemplateDigestUtils.generateDailyUserDigest(digestData);

                    if (digestDailyHtml && digestDailyHtml.length > 0) {
                        await MailerSendUtils.sendEmail({
                            destinationsEmail: [{
                                uid: userId,
                                name: subscribedUsers[userId].name,
                                email: subscribedUsers[userId].email,
                            }],
                            sentFrom: {
                                name: WEBAPP_CONFIGS.SITE_NAME,
                                email: WEBAPP_CONFIGS.COMPANY_EMAIL,
                            },
                            subject: WEBAPP_CONFIGS.SITE_NAME + " - Daily Digest for you",
                            bodyHtml: digestDailyHtml,
                        });


                    }
                }
                // Update DB
                dataBatchUpdate = {
                    ...dataBatchUpdate,
                    [userId]: admin.firestore.FieldValue.delete(),
                };
            }

            const dbResponse = await admin.firestore().collection(SharedFirestoreCollectionDB.NewsletterCatalog)
                .doc(SharedFirestoreDocsDB.NewsletterCatalog.DailyUserDigest).set(dataBatchUpdate, {merge: true});
            response.send(getQuickResponse(true, {dbResponse}));
            return;
        }
    }

    response.send(getQuickResponse(true, {}, "Daily digest was empty"));

});

/**
 * @deprecated
 */
export const importNewsletterSubscribers = functions.runWith({
    memory: "1GB",
    timeoutSeconds: 240,
}).https.onRequest(async (request,
                                                                            response) => {
    const centralDocsSnap = await admin.firestore().collection(SharedFirestoreCollectionDB.CentralUserData).get()
    const unsubscribedUsers: {[uid: string]: boolean} = await RealtimeDbUtils.getData({
        collectionName: SharedFirestoreCollectionDB.NewsletterCatalog,
        docId: SharedFirestoreDocsDB.NewsletterCatalog.UnsubscribedUsers,
    });
    let aggNewsletterSubscribed: { [userId: string]: SharedNewsletterUserModel } = {};
    let docs = centralDocsSnap.docs;
    let listUnsubscribedUids: string[] = Object.keys(unsubscribedUsers);

    const startIndex: number = 0;
    const maxImportedEmails: number = 500;
    for (let i = startIndex; i < docs.length; i++) {
        let doc = docs[i];
        let userCentralData: SharedCentralUserDataModel | undefined;

        if (doc.data()) {
            userCentralData = {
                ...doc.data() as SharedCentralUserDataModel,
            };


            console.log(`Verifying index ${i}/${docs.length} - email: ${userCentralData.email}`);
            if (!listUnsubscribedUids.includes(userCentralData.uid!) && userCentralData.email && userCentralData.isNewsletterSubscribed !== false && await MailerSendUtils.verifyEmail(userCentralData.email)) {
                aggNewsletterSubscribed = {
                    ...aggNewsletterSubscribed,
                    [doc.id]: {
                        userId: doc.id,
                        email: userCentralData.email,
                        name: userCentralData.publicProfile?.name ? userCentralData.publicProfile.name : userCentralData.email,
                    },
                };
            }
        }

        if ((i - startIndex) > maxImportedEmails) {
            break;
        }
    }

    // Add Central User Data
    await RealtimeDbUtils.writeData({
        collectionName: SharedFirestoreCollectionDB.NewsletterCatalog,
        docId: SharedFirestoreDocsDB.NewsletterCatalog.SubscribedUsers,
        data: aggNewsletterSubscribed,
        merge: true,
    });

    response.send(getQuickResponse(true, {
        allUsers: centralDocsSnap.size,
        usersImported: Object.keys(aggNewsletterSubscribed).length,
    }));
});

export const validateEmail = functions.https.onRequest(async (request,
                                                              response) => {

    const email: string = request.query.email as string;
    response.send(getQuickResponse(true, {email: email, result: await MailerSendUtils.verifyEmail(email)}));
});


export const unsubscribeEmail = functions.https.onRequest(async (request,
                                                                 response) => {
    const uid: string | undefined = request.query.uid as string;

    if (uid) {
        const batch = admin.firestore().batch();

        batch.set(admin.firestore().collection(SharedFirestoreCollectionDB.CentralUserData).doc(uid), {
            isNewsletterSubscribed: false,
        }, {merge: true});

        // Update Newsletter Database
        await RealtimeDbUtils.deleteData({
            collectionName: SharedFirestoreCollectionDB.NewsletterCatalog,
            docId: SharedFirestoreDocsDB.NewsletterCatalog.SubscribedUsers,
            subPaths: [uid]
        });
        await RealtimeDbUtils.writeData({
            collectionName: SharedFirestoreCollectionDB.NewsletterCatalog,
            docId: SharedFirestoreDocsDB.NewsletterCatalog.UnsubscribedUsers,
            subPaths: [uid],
            data: true,
            merge: true,
        });

        await batch.commit();
        response.send("You are unsubscribed from our newsletter. You can revert this by editing your user profile.");
    } else {
        response.send("User subscription not found. Please, contact us to help you with this issue: fortuly.apps@gmail.com");
    }
});