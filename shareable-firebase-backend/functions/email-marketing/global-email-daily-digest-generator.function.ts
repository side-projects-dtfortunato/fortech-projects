import * as functions from "firebase-functions";
import {SharedNewsletterUserModel} from "../../model-data/frontend-model-data/shared-newsletter-user.model";
import {RealtimeDbUtils} from "../../utils/realtime-db.utils";
import {SharedFirestoreCollectionDB, SharedFirestoreDocsDB} from "../../model-data/shared-firestore-collections";
import {getQuickResponse} from "../../model-data/api-response.model";
import {ArticleImportedModel} from "../../base-projects/articles-digest/data/model/ArticleImported.model";
import {getRecentDocuments} from "../../utils/utils";
import {JobDetailsModel} from "../../base-projects/jobs-discover/shared-jobs-discover.data";
import {CommunityCommentPostModel} from "../../base-projects/community/data/community-data.model";
import {generateEmailTemplate} from "../../email-templates/email-templates/email-digest-generator.utils";
import {WEBAPP_CONFIGS} from "../../../configs/webapp.configs";
import {BackendRouteUtils} from "../../utils/backend-route.utils";
import {EmailDestinationModel} from "../../utils/email-utils/email-sender-utils";
import {BackendSharedUtils} from "../../utils/backend-shared-utils";
import {EmailBulkDeliveryModel} from "../../model-data/email-builk-delivery.model";

export const globalEmailDailyDigestGenerator = functions.https.onRequest(async (request,
                                                                         response) => {
    // Get subscribed users
    let subscribedUsers: { [userId: string]: SharedNewsletterUserModel } | undefined = await RealtimeDbUtils.getData({
        collectionName: SharedFirestoreCollectionDB.NewsletterCatalog,
        docId: SharedFirestoreDocsDB.NewsletterCatalog.SubscribedUsers,
    });

    if (!subscribedUsers) {
        response.send(getQuickResponse(false, null, "No subscribers found"));
        return;
    }

    // Get content
    const listArticles: ArticleImportedModel[] = await getRecentDocuments(SharedFirestoreCollectionDB.ArticlesDigestProject.ArticlePublished,24, 10, "createdAt");
    const listJobs: JobDetailsModel[] = await getRecentDocuments(SharedFirestoreCollectionDB.JobsDiscoverProject.JobDetails,24*3, 30, "createdAt");
    const listCommunityPosts: CommunityCommentPostModel[] = await getRecentDocuments(SharedFirestoreCollectionDB.CommunityCommentPosts, 24, 12, "createdAt");

    if  (listJobs.length === 0 && listArticles.length === 0 && listCommunityPosts.length === 0) {
        response.send(getQuickResponse(false, null, "No content to digest"));
        return;
    }

    // Sort jobs
    listJobs.sort((a, b) => {
        // Featured jobs first (1 for featured, 0 for others)
        const aFeatured = a.publishPlanType === 'FEATURED' ? 1 : 0;
        const bFeatured = b.publishPlanType === 'FEATURED' ? 1 : 0;
        if (aFeatured !== bFeatured) return bFeatured - aFeatured;

        // Then by views counter (descending)
        const aViews = (a as any).stats?.viewsCounter || 0;
        const bViews = (b as any).stats?.viewsCounter || 0;
        if (aViews !== bViews) return bViews - aViews;

        // Finally by creation date (newest first)
        return (b.createdAt || 0) - (a.createdAt || 0);
    });

    // Generate Email Digest HTML
    const emailGenerator = generateEmailTemplate({
        introText: "Daily Digest",
        unsubscribeLink: WEBAPP_CONFIGS.UNSUBSCRIBE_URL,
        articles: listArticles,
        communityPosts: listCommunityPosts.map((communityItem) => {
            return {
                userName: communityItem.usersInfos[communityItem.userCreatorId].name,
                userAvatar: communityItem.usersInfos[communityItem.userCreatorId].pictureUrl,
                postText: communityItem.message,
                link: BackendRouteUtils.getCommunityDetailsPage(communityItem.uid!),
            };
        }),
        jobPositions: listJobs,
        socialNetworks: [],
    });

    let destionationsList: EmailDestinationModel[] = Object.values(subscribedUsers)
        .filter((subscribedUser) => {
            return subscribedUser.userId && BackendSharedUtils.isValidEmail(subscribedUser.email);
        }).map((subscriptionItem) => {
            return {
                uid: subscriptionItem.userId,
                email: subscriptionItem.email,
                name: subscriptionItem.name ? subscriptionItem.name : subscriptionItem.email,
            };
        });

    // Store to later be delivered
    await RealtimeDbUtils.writeData({
        collectionName: SharedFirestoreCollectionDB.RTEmailBulkToDeliver,
        docId: Date.now().toString(),
        data: {
            bodyHtml: emailGenerator.html,
            destinationsEmail: destionationsList,
            subject: emailGenerator.emailSubject,
            sentFrom: {
                name: WEBAPP_CONFIGS.SITE_NAME + " Digest",
                email: WEBAPP_CONFIGS.COMPANY_EMAIL,
            },
            createdAt: Date.now(),
        } as EmailBulkDeliveryModel,
        merge: false,
    });
    response.send(getQuickResponse(true, null, "Daily Digest planned to dispatch"));
});