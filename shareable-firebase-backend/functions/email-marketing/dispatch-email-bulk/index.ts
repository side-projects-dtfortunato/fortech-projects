import {RealtimeDbUtils} from "../../../utils/realtime-db.utils";
import {SharedFirestoreCollectionDB} from "../../../model-data/shared-firestore-collections";
import {EmailBulkDeliveryModel} from "../../../model-data/email-builk-delivery.model";
import {EmailDestinationModel, MailerSendUtils} from "../../../utils/email-utils/email-sender-utils";
import {BackendSharedUtils} from "../../../utils/backend-shared-utils";

const MAX_EMAILS_PER_BULK = 40;

/*export const dispatchEmailBulk = functions.https.onRequest(async (request,
                                                                     response) => {

    const allEmailBulks: {[id: string]: EmailBulkDeliveryModel} = await RealtimeDbUtils.getData({
        collectionName: SharedFirestoreCollectionDB.RTEmailBulkToDeliver
    });

    if (!allEmailBulks || Object.keys(allEmailBulks).length === 0) {
        response.send(getQuickResponse(true, null, "Nothing to dispatch"));
        return;
    }

    // Firstly clean the email bulk that are too old or empty of destinations
    for (const key of Object.keys(allEmailBulks)) {
        const value: EmailBulkDeliveryModel = allEmailBulks[key];
        if (!value.createdAt || BackendSharedUtils.isOlderThanXDays(new Date(value.createdAt), 6) || !value.destinationsEmail || value.destinationsEmail.length === 0) {
            // Should remove the email bulk
            await RealtimeDbUtils.deleteData({
                collectionName: SharedFirestoreCollectionDB.RTEmailBulkToDeliver,
                docId: key
            });

            delete allEmailBulks[key];
        }
    }

    const docId: string | undefined = Object.keys(allEmailBulks).find((key) => {
        return allEmailBulks[key].destinationsEmail !== undefined && allEmailBulks[key].destinationsEmail.length > 0;
    });

    if (!docId) {
        response.send(getQuickResponse(false, null, "Didn't found any Email Bulk to dispatch"));
        return;
    }
    const emailBulkData: EmailBulkDeliveryModel = allEmailBulks[docId] as EmailBulkDeliveryModel;

    let listDestinationsToDispatch: EmailDestinationModel[] = emailBulkData.destinationsEmail.slice(0, MAX_EMAILS_PER_BULK);
    emailBulkData.destinationsEmail = emailBulkData.destinationsEmail.filter((item) => !listDestinationsToDispatch.includes(item));

    MailerSendUtils.sendEmail({
        bodyHtml: emailBulkData.bodyHtml,
        subject: emailBulkData.subject,
        sentFrom: emailBulkData.sentFrom,
        destinationsEmail: listDestinationsToDispatch,
    }).then(async (res) => {

        // Update Email Bulk data on DB
        if (emailBulkData.destinationsEmail && emailBulkData.destinationsEmail.length > 0) {
            await RealtimeDbUtils.writeData({
                collectionName: SharedFirestoreCollectionDB.RTEmailBulkToDeliver,
                docId: docId,
                merge: false,
                data: emailBulkData,
            });
        } else {
            await RealtimeDbUtils.deleteData({
                collectionName: SharedFirestoreCollectionDB.RTEmailBulkToDeliver,
                docId: docId
            });
        }

        response.send(getQuickResponse(true, {
            res,
        }));
    }).catch((err) => {
        response.send(err);
    });

}); */

export async function dispatchEmailBulk() {
    const allEmailBulks: {[id: string]: EmailBulkDeliveryModel} = await RealtimeDbUtils.getData({
        collectionName: SharedFirestoreCollectionDB.RTEmailBulkToDeliver
    });

    if (!allEmailBulks || Object.keys(allEmailBulks).length === 0) {
        return;
    }

    // Firstly clean the email bulk that are too old or empty of destinations
    for (const key of Object.keys(allEmailBulks)) {
        const value: EmailBulkDeliveryModel = allEmailBulks[key];
        if (!value.createdAt || BackendSharedUtils.isOlderThanXDays(new Date(value.createdAt), 6) || !value.destinationsEmail || value.destinationsEmail.length === 0) {
            // Should remove the email bulk
            await RealtimeDbUtils.deleteData({
                collectionName: SharedFirestoreCollectionDB.RTEmailBulkToDeliver,
                docId: key
            });

            delete allEmailBulks[key];
        }
    }

    const docId: string | undefined = Object.keys(allEmailBulks).find((key) => {
        return allEmailBulks[key].destinationsEmail !== undefined && allEmailBulks[key].destinationsEmail.length > 0;
    });

    if (!docId) {
        //response.send(getQuickResponse(false, null, "Didn't found any Email Bulk to dispatch"));
        return;
    }
    const emailBulkData: EmailBulkDeliveryModel = allEmailBulks[docId] as EmailBulkDeliveryModel;

    let listDestinationsToDispatch: EmailDestinationModel[] = emailBulkData.destinationsEmail.slice(0, MAX_EMAILS_PER_BULK);
    emailBulkData.destinationsEmail = emailBulkData.destinationsEmail.filter((item) => !listDestinationsToDispatch.includes(item));

    if (listDestinationsToDispatch && listDestinationsToDispatch.length > 0) {
        try {
            await MailerSendUtils.sendEmail({
                bodyHtml: emailBulkData.bodyHtml,
                subject: emailBulkData.subject,
                sentFrom: emailBulkData.sentFrom,
                destinationsEmail: listDestinationsToDispatch,
            });
        } catch (e) {
            console.error(e);
        }
    }

    // Update Email Bulk data on DB
    if (emailBulkData.destinationsEmail && emailBulkData.destinationsEmail.length > 0) {
        await RealtimeDbUtils.writeData({
            collectionName: SharedFirestoreCollectionDB.RTEmailBulkToDeliver,
            docId: docId,
            merge: false,
            data: emailBulkData,
        });
    } else {
        await RealtimeDbUtils.deleteData({
            collectionName: SharedFirestoreCollectionDB.RTEmailBulkToDeliver,
            docId: docId
        });
    }
}