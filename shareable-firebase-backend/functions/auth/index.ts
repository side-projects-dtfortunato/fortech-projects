import * as functions from "firebase-functions";
import * as admin from "firebase-admin";
import {getQuickResponse} from "../../model-data/api-response.model";
import {MailerSendUtils} from "../../utils/email-utils/email-sender-utils";
import {
    SharedCentralUserDataModel,
    UserRole
} from "../../model-data/frontend-model-data/shared-central-user-data.model";
import {SharedFirestoreCollectionDB, SharedFirestoreDocsDB} from "../../model-data/shared-firestore-collections";
import {RealtimeDbUtils} from "../../utils/realtime-db.utils";
import {WEBAPP_CONFIGS} from "../../../configs/webapp.configs";
import {BackendSharedUtils} from "../../utils/backend-shared-utils";

/**
 * on Create user
 */
export const onCreateUser = functions.auth.user().onCreate((user) => {

    // Create User Container documents
    let batch = admin.firestore().batch();

    return new Promise(async (resolve, reject) => {

        // Generate a username from the email
        let tempUsername: string | undefined = user.displayName;
        if (!tempUsername && user.email) {
            tempUsername = user.email.substring(0, user.email.indexOf("@"));
        }

        if (tempUsername) {
            BackendSharedUtils.replaceAll(tempUsername, " ", "");
        } else {
            tempUsername = "User" + Date.now();
        }

        const centralUserData: SharedCentralUserDataModel = {
            uid: user.uid,
            userRole: UserRole.USER,
            username: tempUsername,
            email: user.email,
            isEmailValid: await MailerSendUtils.verifyEmail(user.email),
            publicProfile: {
                username: tempUsername,
                name: user.displayName,
                userPicture: user.photoURL ? {fileUrl: user.photoURL, storagePath: ""} : undefined,
                links: {},
                createdAt: Date.now(),
            },
            disableUsernameChange: false,
            isNewsletterSubscribed: true,
            nicheSitesLoggedIn: [WEBAPP_CONFIGS.SITE_NAME],
        };

        if (!centralUserData.isEmailValid) {
            await RealtimeDbUtils.writeData({
                collectionName: SharedFirestoreCollectionDB.NewsletterCatalog,
                docId: SharedFirestoreDocsDB.NewsletterCatalog.InvalidEmailUsers,
                data: {[user.uid]: user.email},
                merge: true,
            });
        }

        let totalUsers: number | undefined = await RealtimeDbUtils.getData({
            collectionName: SharedFirestoreCollectionDB.AdminRegisteredUsersCounterRTDB,
        });
        if (!totalUsers) {
            totalUsers = await getTotalUsers();
        }

        // Update DB with total users
        await RealtimeDbUtils.writeData({
            collectionName: SharedFirestoreCollectionDB.AdminRegisteredUsersCounterRTDB,
            data: totalUsers + 1,
            merge: false,
        });

        // Create Central User Data
        batch.set(admin
            .firestore()
            .collection(SharedFirestoreCollectionDB.CentralUserData)
            .doc(user.uid), centralUserData);

        //  Commit batch changes
        batch.commit()
            .then(res => resolve(getQuickResponse(true, res)))
            .catch(err => reject(getQuickResponse(false, err)));
    });

});


/**
 * on Delete user
 */
export const onDeleteUser = functions.auth.user().onDelete((user) => {

    // Create User Container documents
    let batch = admin.firestore().batch();

    return new Promise(async (resolve, reject) => {

        // Delete Central user Data
        batch.delete(admin
            .firestore()
            .collection(SharedFirestoreCollectionDB.CentralUserData)
            .doc(user.uid));

        // Delete Public Profile
        batch.delete(admin
            .firestore()
            .collection(SharedFirestoreCollectionDB.PublicUserProfile)
            .doc(user.uid));

        // Delete User Bookmarks
        batch.delete(admin
            .firestore()
            .collection(SharedFirestoreCollectionDB.UserBookmarks)
            .doc(user.uid));

        // Delete User Posts Created
        batch.delete(admin
            .firestore()
            .collection(SharedFirestoreCollectionDB.UserPostsCreated)
            .doc(user.uid));

        // Delete Newsletter Subscription
        await RealtimeDbUtils.deleteData({
            collectionName: SharedFirestoreCollectionDB.NewsletterCatalog,
            docId: SharedFirestoreDocsDB.NewsletterCatalog.SubscribedUsers,
            subPaths: [user.uid],
        });

        //  Commit batch changes
        batch.commit()
            .then(res => resolve(getQuickResponse(true, res)))
            .catch(err => reject(getQuickResponse(false, err)));
    });

});


async function getTotalUsers() {
    let userCount = 0;
    let nextPageToken;

    do {
        const listUsersResult: any = await admin.auth().listUsers(1000, nextPageToken);
        userCount += listUsersResult.users.length;
        nextPageToken = listUsersResult.pageToken;
    } while (nextPageToken);

    return userCount;
}