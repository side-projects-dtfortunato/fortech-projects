import * as functions from "firebase-functions";
import * as admin from "firebase-admin";
import {getQuickResponse} from "../../../model-data/api-response.model";
import {Log} from "../../../utils/log";

export const deleteAccount = functions.https.onCall(
    (data: {},
     context) => {

        if (!context.auth || !context.auth.uid) return getQuickResponse(false, undefined, "User not logged in");
        const userId = context && context.auth && context.auth.uid ? context.auth.uid : "";
        Log.onCall(data, userId);

        return new Promise(async (resolve, reject) => {
                // Delete User Account
                await admin.auth().deleteUser(userId);

                resolve(getQuickResponse(true));
        });
    });