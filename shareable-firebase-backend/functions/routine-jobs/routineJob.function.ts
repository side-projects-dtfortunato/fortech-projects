import * as functions from "firebase-functions";
import {RealtimeDBStatsEventModel} from "../../model-data/realtime-db-models/realtimedb-stats-event.model";
import {RealtimeDbUtils} from "../../utils/realtime-db.utils";
import {SharedRealtimeCollectionDB} from "../../model-data/shared-firestore-collections";
import * as admin from "firebase-admin";
import {dispatchEmailBulk} from "../email-marketing/dispatch-email-bulk";
import {getQuickResponse} from "../../model-data/api-response.model";
import {NotificationsQueueManager} from "../../utils/notifications-queue.manager";
import {isNumber} from "firebase-admin/lib/utils/validator";

export const routineJobFunction = functions.runWith({
    memory: "1GB",
    timeoutSeconds: 220,
}).https.onRequest(
    async (req, resp) => {

        // Get Stats Object Data to update parent docs
        const statsEventObject: {
            [docId: string]: RealtimeDBStatsEventModel,
        } | undefined = await RealtimeDbUtils.getData({
            collectionName: SharedRealtimeCollectionDB.StatsEventsName,
        });

        if (statsEventObject) {
            const listStatsEventsIds: string[] = Object.keys(statsEventObject);
            for (let statEventId of listStatsEventsIds) {
                const statEventObj: any = statsEventObject[statEventId];

                let statsDataItem: any = {};
                for (let statsDataKey of Object.keys(statEventObj)) {
                    if (statsDataKey !== "collectionName" && statsDataKey !== "docId") {
                        statsDataItem[statsDataKey] = admin.firestore.FieldValue.increment(statEventObj[statsDataKey]);
                    }
                }
                await admin.firestore().collection(statEventObj.collectionName).doc(statEventObj.docId).set({
                    stats: statsDataItem,
                }, {merge: true});
                await RealtimeDbUtils.deleteData({collectionName: SharedRealtimeCollectionDB.StatsEventsName, docId: statEventId});
            }
        }

        // Dispatch Notification from the queue
        await NotificationsQueueManager.dispatchNextNotificationItem();

        // Dispatch Email Bulk
        await dispatchEmailBulk();


        resp.send(getQuickResponse(true));
        return;
    });