import {JSDOM} from "jsdom";
import {UrlExtractDataUtils} from "../../utils/url-extract-data.utils";
import TurndownService from "turndown";
import {WEBAPP_CONFIGS} from "../../../configs/webapp.configs";
import {BaseModel} from "../../model-data/base.model";
import {BackendSharedUtils} from "../../utils/backend-shared-utils";
import getMetaData from 'metadata-scraper';
import {BackendRouteUtils} from "../../utils/backend-route.utils";
import {ArticleImportedModel} from "../articles-digest/data/model/ArticleImported.model";
import {OpenaiUtils} from "../../utils/openai/openai.utils";
import {RealtimeDbUtils} from "../../utils/realtime-db.utils";
import {SharedFirestoreCollectionDB} from "../../model-data/shared-firestore-collections";
import {FirebaseStorageUtils} from "../../utils/firebase-storage.utils";
import {ALLOWED_WORK_MODE, JOB_CATEGORIES, JOB_REGIONS} from "../../../custom-project/utils/custom-jobs-discover.utils";
import { NotificationQueueItemModel } from "../../utils/notifications-queue.manager";

export type JobWorkModeType = "HYBRID" | "REMOTE" | "ON-SITE";
export type JobRemoteRegion = "USA" | "EU" | "UK" | "Canada" | "Australia" | "WORLDWIDE" | string;
export type JobPublishPlanType = "IMPORTED" | "BASIC" | "FEATURED";

export interface LinkedinSourceModel {
    id: string,
    category: string,
    region: JobRemoteRegion,
    url: string,
}

export interface DailyPublishedJobs {
    publishedDayId: number,
    listJobs: {
        [jobId: string]: JobListModel,
    },
}

export interface JobListModel extends BaseModel {
    title: string;
    company: string;
    companyLogo: string;
    companyUrl?: string;
    companyThumbnailUrl?: string;
    location: string;
    link: string;
    workMode?: JobWorkModeType;
    remoteRegion?: JobRemoteRegion;
    category?: string;
    source?: string;
    publishPlanType?: JobPublishPlanType;
    publisherEmail?: string;
    skills?: string[];
}

export interface JobDetailsModel extends JobListModel {
    experienceLevel: string;
    description: string;
    applicationUrl: string;
    publishedDayId: number;
    salaryRange?: JobSalaryRange;
}

interface JobSalaryRange {
    min?: number;
    max?: number;
    currency?: string;
    isEstimated?: boolean;
}

export interface CompanyDataModel {
    uid: string;
    company: string;
    companyThumbnailUrl?: string;
    companyUrl?: string;
    companyLogo?: string;
}

export class SharedJobsDiscoverData {
    private static turndownService = new TurndownService({
        headingStyle: 'atx',
        codeBlockStyle: 'fenced',
        emDelimiter: '_',
        bulletListMarker: '*',
    });

    static getListLinkedinSources() {
        let listLinkedinSources: LinkedinSourceModel[] = [];


        JOB_REGIONS.forEach((region: any, regionIndex: any) => {
            JOB_CATEGORIES.forEach((jobCategory: any, jobCatIndex: any) => {
                let linkedinSourceUrl: string = `https://www.linkedin.com/jobs/search?keywords=${jobCategory.jobCategoryQuery}&geoId=${region.regionId}&f_TPR=r86400&f_E=2%2C3%2C4%2C5&position=1&pageNum=0` // f_TPR=r86400 is the last 24 hours

                // Add work mode
                let workModeParam: string = "";
                if (ALLOWED_WORK_MODE.length < 3 && ALLOWED_WORK_MODE.length > 0) {
                    ALLOWED_WORK_MODE.forEach((workMode: any) => {
                        workModeParam += `${workModeParam.length > 0 ? "%2C" : ""}`
                        switch (workMode) {
                            case "ON-SITE": workModeParam += "1"; break;
                            case "REMOTE": workModeParam += "2"; break;
                            case "HYBRID": workModeParam += "3"; break;
                        }
                    });
                    workModeParam = "&f_WT=" + workModeParam;
                }
                console.log("workModeParam: ", workModeParam);
                linkedinSourceUrl += workModeParam;


                listLinkedinSources.push({
                    url: linkedinSourceUrl,
                    category: jobCategory.jobCategoryLabel,
                    region: region.regionLabel,
                    id: `${regionIndex}${jobCatIndex}`,
                });
            });
        });

        return listLinkedinSources;
    }


    static async extractJobListings(url: string, remoteRegion?: JobRemoteRegion, category?: string): Promise<JobListModel[]> {

        let workMode: JobWorkModeType = "ON-SITE";
        if (url.includes("f_WT=2")) {
            workMode = "REMOTE";
        } else if (url.includes("f_WT=3")) {
            workMode = "HYBRID";
        }

        console.log("Extracting jobs from: ", url);

        let html: string | undefined = await this.extractJobListingHtml(url, 0);

        if (!html || html.length === 0) {
            return [];
        }
        const dom = new JSDOM(html);
        const document = dom.window.document;
        const jobCards = document.querySelectorAll('.base-card');

        const jobs: JobListModel[] = [];

        jobCards.forEach((card) => {
            const titleElem = card.querySelector('.base-search-card__title');
            const companyElem = card.querySelector('.base-search-card__subtitle');
            const companyUrlElem = companyElem!.querySelector('a.hidden-nested-link');
            const locationElem = card.querySelector('.job-search-card__location');
            const linkElem = card.querySelector('.base-card__full-link');
            const logoElem = card.querySelector('.search-entity-media img') as HTMLImageElement;

            const job: JobListModel = {
                title: titleElem?.textContent?.trim() || '',
                company: companyElem?.textContent?.trim() || '',
                companyUrl: this.normalizeLinkedInURL((companyUrlElem as HTMLAnchorElement)?.href) || '',
                location: locationElem?.textContent?.trim() || '',
                link: this.normalizeLinkedInURL((linkElem as HTMLAnchorElement)?.href) || '',
                companyLogo: logoElem?.src || '',
                workMode: workMode,
                remoteRegion: remoteRegion,
                category: category,
                createdAt: Date.now(),
                updatedAt: Date.now(),
                authorization: {
                    allWrite: false,
                    allRead: true,
                },
                source: "LinkedIn",
            };
            job.uid = BackendSharedUtils.string_to_slug(job.company) + "-" + BackendSharedUtils.string_to_slug(job.title);


            // Prevent adding jobs with only "***"
            if (!job.title.includes("****")) {
                jobs.push(job);
            }
        });


        return jobs;
    }


    static async extractJobListingHtml(url: string, attempt: number): Promise<string | undefined> {
        let html: string = "";

        try {
            html = await UrlExtractDataUtils.getHtmlWithElement(url, "jobs-search__results-list", "https://www.linkedin.com");

            /*if (!html || !html.includes('jobs-search__results-list')) {
                // If couldn't get the HTML code or the required element isn't found, try again
                if (attempt < 3) {
                    console.log("extractJobListingHtml.attempt: ", attempt);
                    // Introduce a random delay between 1 and 5 seconds
                    const delay = Math.floor(Math.random() * (5000 - 1000 + 1)) + 1000;
                    await new Promise(resolve => setTimeout(resolve, delay));
                    return await this.extractJobListingHtml(url, attempt + 1);
                } else {
                    return undefined;
                }
            }*/

        } catch (e: any) {
            console.error(e.message);
            /*if (attempt < 4) {
                // Introduce a random delay between 1 and 5 seconds in case of an error
                const delay = Math.floor(Math.random() * (5000 - 1000 + 1)) + 1000;
                await new Promise(resolve => setTimeout(resolve, delay));
                return await this.extractJobListingHtml(url, attempt + 1);
            } else {
                return undefined;
            }*/
        }
        return html;
    }

    static async extractJobDetailsHtml(url: string, attempt: number): Promise<string | undefined> {
        if (!url || url.length === 0) {
            return undefined;
        }
        let html: string = "";
        try {
            const htmlContent = await UrlExtractDataUtils.getLinkedInJobDetails(url);
            if (htmlContent) {
                html = htmlContent.html;
            }


            console.log("HTML has decorated-job-posting__details", html.includes("decorated-job-posting__details"));
            /* Removed Attempts

            if (!html || !html.includes("decorated-job-posting__details")) {
                // If couldn't get the HTML code, try again
                if (attempt < 0) {
                    return await this.extractJobDetailsHtml(url, attempt + 1);
                } else {
                    return undefined;
                }
            }*/

        } catch (e: any) {
            console.error(e.message);
            if (attempt < 4) {
                return await this.extractJobDetailsHtml(url, attempt + 1);
            }

        }
        return html;
    }

    static async extractJobDetails(url: string, jobListData?: JobListModel): Promise<JobDetailsModel | undefined> {
        try {

            let html = await this.extractJobDetailsHtml(url, 0);
            if (!html || html.length === 0) {
                return undefined;
            }

            const dom = new JSDOM(html);
            const document = dom.window.document;

            const jobDetails: JobDetailsModel = {
                title: this.getTextContent(document, '.top-card-layout__title'),
                company: this.getTextContent(document, '.topcard__org-name-link'),
                location: this.getTextContent(document, '.topcard__flavor--bullet'),
                description: this.getDescription(document),
                experienceLevel: this.getTextContent(document, '.description__job-criteria-text:nth-of-type(1)'),
                companyLogo: this.getImageSrc(document, 'img.artdeco-entity-image'),
                link: url,
                applicationUrl: this.getApplicationUrl(document),
                publishedDayId: BackendSharedUtils.getTodayDay(),
                ...jobListData,
            };

            // Check if the company data is already on our DB
            const companyData: CompanyDataModel | undefined = await SharedJobsDiscoverData.getCompanyData(jobDetails.company);
            if (companyData && companyData.companyLogo) {
                jobDetails.companyLogo = companyData.companyLogo;
                jobDetails.companyThumbnailUrl = companyData.companyThumbnailUrl;
                jobDetails.companyUrl = companyData.companyUrl;
            } else {
                jobDetails.companyLogo = this.getImageSrc(document, 'section.top-card-layout img.artdeco-entity-image');
            }

            // Get Company Thumbnail from their site
            if (jobDetails.companyUrl && !jobDetails.companyThumbnailUrl) {
                jobDetails.companyThumbnailUrl = await this.extractThumbnail(jobDetails.companyUrl);
            }

            if (!companyData) {
                // if the company data doesn't exist yet, should update it in the data basa
                await SharedJobsDiscoverData.saveCompanyData(jobDetails);
            }

            if (!jobDetails.applicationUrl) {
                // Use the job post link
                jobDetails.applicationUrl = jobDetails.link;
            }


            console.log("extractJobDetails:", jobDetails);

            return jobDetails;
        } catch (error) {
            console.error('Error extracting job details:', error);
            return undefined;
        }
    }

    static async extractJobDetailsLinkTest(url: string): Promise<JobDetailsModel | undefined> {
        try {

            let html = await this.extractJobDetailsHtml(url, 0);
            if (!html || html.length === 0) {
                return undefined;
            }

            const dom = new JSDOM(html);
            const document = dom.window.document;

            const jobDetails: JobDetailsModel = {
                title: this.getTextContent(document, '.top-card-layout__title'),
                company: this.getTextContent(document, '.topcard__org-name-link'),
                location: this.getTextContent(document, '.topcard__flavor--bullet'),
                description: this.getDescription(document),
                experienceLevel: this.getTextContent(document, '.description__job-criteria-text:nth-of-type(1)'),
                companyLogo: this.getImageSrc(document, 'img.artdeco-entity-image'),
                link: url,
                applicationUrl: this.getApplicationUrl(document),
                publishedDayId: BackendSharedUtils.getTodayDay(),
            };

            // Check if the company data is already on our DB
            jobDetails.companyLogo = this.getImageSrc(document, 'section.top-card-layout img.artdeco-entity-image');

            if (!jobDetails.applicationUrl) {
                // Use the job post link
                jobDetails.applicationUrl = jobDetails.link;
            }


            console.log("extractJobDetails:", jobDetails);

            return jobDetails;
        } catch (error) {
            console.error('Error extracting job details:', error);
            return undefined;
        }
    }

    private static getTextContent(document: Document, selector: string): string {
        const element = document.querySelector(selector);
        return element ? element.textContent?.trim() || '' : '';
    }

    private static getImageSrc(document: Document, selector: string): string {
        const element = document.querySelector(selector) as HTMLImageElement;
        return element ? element.src || '' : '';
    }

    private static getApplicationUrl(document: Document): string {
        const applyLink = document.querySelector('a.sign-up-modal__company_webiste') as HTMLAnchorElement;
        if (applyLink && applyLink.href) {
            const url = new URL(applyLink.href);
            const finalUrl = url.searchParams.get('url');
            if (finalUrl) {
                const decodedUrl = decodeURIComponent(finalUrl);
                const finalUrlObj = new URL(decodedUrl);
                finalUrlObj.searchParams.set('source', WEBAPP_CONFIGS.SITE_NAME);
                return finalUrlObj.toString();
            }
        }
        return '';
    }


    private static normalizeLinkedInURL(url: string): string {
        try {
            const urlObject = new URL(BackendSharedUtils.removeQueryParams(url));

            // Check if the hostname is a subdomain of linkedin.com
            if (urlObject.hostname.endsWith('.linkedin.com')) {
                // Replace the hostname with just 'linkedin.com'
                urlObject.hostname = 'linkedin.com';
            }

            return urlObject.toString();
        } catch (error) {
            // If the URL is invalid, return the original string
            console.error('Invalid URL:', error);
            return url;
        }
    }

    private static getDescription(document: Document): string {
        const descriptionElement = document.querySelector('.show-more-less-html__markup');
        if (!descriptionElement) return '';

        // Clone the element to avoid modifying the original DOM
        const clonedElement = descriptionElement.cloneNode(true) as HTMLElement;

        // Remove "Show more" and "Show less" buttons
        clonedElement.querySelectorAll('.show-more-less-button').forEach(button => button.remove());

        // Pre-process the HTML
        this.preProcessHTML(clonedElement);

        return this.convertHTMLtoMarkdown(clonedElement.innerHTML)
    }


    static async extractThumbnail(url: string): Promise<string | undefined> {
        try {
            const metadata: any = await getMetaData(url);

            // Check for og:image first
            if (metadata.og && metadata.og.image) {
                return metadata.og.image;
            }

            // Fallback to twitter:image
            if (metadata.twitter && metadata.twitter.image) {
                return metadata.twitter.image;
            }

            // If no social media images found, try to get any image from the page
            if (metadata.image) {
                return metadata.image;
            }

            // No thumbnail found
            return undefined;
        } catch (error) {
            console.error(`Error extracting thumbnail from ${url}:`, error);
            return undefined;
        }
    }


    public static convertHTMLtoMarkdown(html: string) {
        let markdown = this.turndownService.turndown(html);
        // Post-process the Markdown
        markdown = this.postProcessMarkdown(markdown);

        return markdown.trim();
    }

    private static preProcessHTML(element: HTMLElement): void {
        // Convert <br> tags to newlines
        element.querySelectorAll('br').forEach(br => br.replaceWith('\n'));

        // Ensure proper spacing for list items
        element.querySelectorAll('li').forEach(li => {
            if (!li.textContent?.trim().endsWith('.')) {
                li.innerHTML += '  '; // Add two spaces for line break in Markdown
            }
        });

        // Convert divs to paragraphs for better Markdown conversion
        element.querySelectorAll('div').forEach(div => {
            const p = document.createElement('p');
            p.innerHTML = div.innerHTML;
            div.replaceWith(p);
        });
    }

    private static postProcessMarkdown(markdown: string): string {
        // Fix double asterisks issue
        markdown = markdown.replace(/\*\*\s*\*\*/g, '');

        // Ensure proper line breaks between sections
        markdown = markdown.replace(/\n{3,}/g, '\n\n');

        // Fix list item spacing
        markdown = markdown.replace(/^\* /gm, '* ');

        // Improve heading formatting
        markdown = markdown.replace(/^(#+)\s*(.+?)\s*\1/gm, '$1 $2');

        return markdown;
    }


    static async isJobDetailsValid(jobData?: JobDetailsModel): Promise<boolean> {
        return jobData !== undefined
            && BackendSharedUtils.isValidHttpUrl(jobData?.applicationUrl)
            // && !jobData.applicationUrl.includes("linkedin.com/")
            && jobData.companyLogo !== undefined
            && jobData.companyLogo.length > 0
            && jobData.description !== undefined
            && jobData.description.length > 10;
    }

    static generateSocialPost(jobPost: JobDetailsModel): string {
        // Prepare salary information if it exists
        let salaryInfo = '';
        if (jobPost.salaryRange) {
            const { min, max, currency } = jobPost.salaryRange;
            if (min && max) {
                salaryInfo = `💰 Salary: ${this.getCurrencySymbol(currency!)}${min.toLocaleString()} - ${this.getCurrencySymbol(currency!)}${max.toLocaleString()}`;
            } else if (min) {
                salaryInfo = `💰 Salary: From ${this.getCurrencySymbol(currency!)}${min.toLocaleString()}`;
            } else if (max) {
                salaryInfo = `💰 Salary: Up to ${this.getCurrencySymbol(currency!)}${max.toLocaleString()}`;
            }
        }

        // Prepare skills information if it exists
        const skillsInfo = jobPost.skills && jobPost.skills.length > 0
            ? `🛠️ Skills: ${jobPost.skills.slice(0, 5).join(', ')}${jobPost.skills.length > 5 ? '...' : ''}`
            : '';

        const postMessage = `
🚀 New Remote Job Alert!
💼 Position: ${jobPost.title}
🏢 Company: ${jobPost.company}
🌍 Location: ${jobPost.remoteRegion || 'Remote'} ${jobPost.workMode === 'HYBRID' ? '(Hybrid)' : ''}
📁 Category: ${jobPost.category || 'Not specified'}
${salaryInfo}
${skillsInfo}
#RemoteJobs #RemoteWork #WorkFromHome #Hiring #JobOpportunity

Apply now: ${BackendRouteUtils.getJobPostDetailsPage(jobPost.uid!)}
`

        return postMessage.trim();
    }

    static generateSocialShortPost(jobPost: JobDetailsModel): string {
        // Prepare salary information if it exists
        let salaryInfo = '';
        if (jobPost.salaryRange) {
            const { min, max, currency } = jobPost.salaryRange;
            if (min && max) {
                salaryInfo = `💰 ${this.getCurrencySymbol(currency!)}${min.toLocaleString()} - ${this.getCurrencySymbol(currency!)}${max.toLocaleString()}`;
            } else if (min) {
                salaryInfo = `💰 ${this.getCurrencySymbol(currency!)}${min.toLocaleString()}`;
            } else if (max) {
                salaryInfo = `💰 ${this.getCurrencySymbol(currency!)}${max.toLocaleString()}`;
            }
        }

        const postMessage = `
🚀 New Remote Job Alert!
💼 ${jobPost.title}
🏢 ${jobPost.company}
🌍 ${jobPost.remoteRegion || 'Remote'} ${jobPost.workMode === 'HYBRID' ? '(Hybrid)' : ''}
📁 ${jobPost.category || 'Not specified'}
${salaryInfo}
#RemoteJobs #RemoteWork #WorkFromHome #Hiring #JobOpportunity

Apply now: ${BackendRouteUtils.getJobPostDetailsPage(jobPost.uid!)}
`

        return postMessage.trim();
    }


    static async digestJobDataWithAI(jobData: JobDetailsModel, isValidRule?: string): Promise<any> {
        console.log("digestJobDataWithAI with rule:", isValidRule);
        const jsonContent: string = JSON.stringify({
            ...jobData,
            workMode: null,
        });

        let assistantPrompt: string = `You are an assistant that reads and extract the Annual Job Salary Range from a Job Post, the required skills for the position (as tags) and based on the job description check if is a on-site, hybrid or remote position, from a JSON object. Try to find the salary range in job post, if doesn't exist, generate a estimated salary range based on the job requirements and location. Respond in the following JSON format (valid json format, without any prefix or suffix, just the JSON object): 
        { 
            minSalary: number, // minimum annual salary 
            maxSalary: number, // max annual salary 
            currency: string, // 3 letter currency symbol 
            isEstimated: boolean, // if the salary is just an estimated value or a value extracted from the job post 
            jobSkills: string[], // list of main required skills (technical/hard skills only) for the position 
            workMode: "HYBRID" | "REMOTE" | "ON-SITE", // verify based on the job description if is a hybrid, remote or on-site job position
            ${isValidRule ? "isValid: boolean // " + isValidRule : ""}
            searchableTokens: string[], // list of normalized (lowercase, no special characters and a word per token) searchable tokens for the job post based on the job title, company, location, category andskills
        }\n`;
        let userPrompt = `JSON Object with the Job Post details:\n${jsonContent}\n`

        console.log("assistantPrompt: ", assistantPrompt);
        try {
            const aiGeneratedContent: any = BackendSharedUtils.extractJsonObject(await OpenaiUtils.generateJsonContent(assistantPrompt, userPrompt));

            console.log("AiGeneratedContent: ", aiGeneratedContent);

            if (isValidRule && aiGeneratedContent.isValid === false) {

                console.log("job is not valid");
                return undefined;
            }

            // Update Job Post Data
            jobData.salaryRange = {
                currency: aiGeneratedContent.currency.toUpperCase(),
                max: aiGeneratedContent.maxSalary,
                min: aiGeneratedContent.minSalary,
                isEstimated: aiGeneratedContent.isEstimated,
            };
            if (aiGeneratedContent.jobSkills) {
                jobData.skills = aiGeneratedContent.jobSkills.map((skill: string) => skill.toUpperCase().replace(" SKILLS", ""));
            }

            if (aiGeneratedContent.workMode) {
                jobData.workMode = aiGeneratedContent.workMode;
            }

            if (aiGeneratedContent.searchableTokens) {
                jobData.searchableTokens = aiGeneratedContent.searchableTokens;
            }

            return jobData;
        } catch (e) {
            return undefined;
        }
    }

    static async generateJobSkills(jobData: JobDetailsModel): Promise<any> {
        const jsonContent: string = JSON.stringify({
            ...jobData,
            workMode: null,
        });

        let assistantPrompt: string = `You are an assistant that reads and extract the required skills for the position (as tags) from a JSON object. Respond in the following JSON format (valid json format, without any prefix or suffix, just the JSON object): 
        { 
            jobSkills: string[], // list of main required skills (technical/hard skills only) for the position 
        }\n`;
        let userPrompt = `JSON Object with the Job Post details:\n${jsonContent}\n`

        console.log("assistantPrompt: ", assistantPrompt);
        try {
            const aiGeneratedContent: any = BackendSharedUtils.extractJsonObject(await OpenaiUtils.generateJsonContent(assistantPrompt, userPrompt));

            console.log("AiGeneratedContent: ", aiGeneratedContent);


            // Update Job Post Data
            if (aiGeneratedContent.jobSkills) {
                jobData.skills = aiGeneratedContent.jobSkills.map((skill: string) => skill.toUpperCase().replace(" SKILLS", ""));
            }

            return jobData;
        } catch (e) {
            return undefined;
        }
    }


    static getCurrencySymbol = (currency: string): string => {
        switch (currency) {
            case 'AUD': return 'A$';
            case 'USD': return '$';
            case 'EUR': return '€';
            case 'GBP': return '£';
            default: return currency;
        }
    };

    static async getCompanyData(companyName: string): Promise<CompanyDataModel | undefined> {
        return await RealtimeDbUtils.getData({
            collectionName: SharedFirestoreCollectionDB.JobsDiscoverProject.CompanyDataRTDB,
            docId: BackendSharedUtils.string_to_slug(companyName),
        });
    }

    static async saveCompanyData(jobDetails: JobDetailsModel) {
        const uid = BackendSharedUtils.string_to_slug(jobDetails.company.toLowerCase());
        const companyData: CompanyDataModel = {
            uid: uid,
            company: jobDetails.company,
            companyLogo: jobDetails.companyLogo,
            companyUrl: jobDetails.companyUrl,
            companyThumbnailUrl: jobDetails.companyThumbnailUrl,
        };

        // Upload Company Logo
        if (jobDetails.companyLogo) {
            const apiResponse = await FirebaseStorageUtils.uploadImageFromUrl(jobDetails.companyLogo, `company/${uid}/companyLogo.jpg`);
            if (apiResponse.isSuccess && apiResponse.responseData?.fileUrl) {
                companyData.companyLogo = apiResponse.responseData.fileUrl;
                jobDetails.companyLogo = apiResponse.responseData.fileUrl;
            }
        }

        // Upload Company Thumbnail
        if (jobDetails.companyThumbnailUrl) {
            const apiResponse = await FirebaseStorageUtils.uploadImageFromUrl(jobDetails.companyThumbnailUrl, `company/${uid}/companyThumbnail.jpg`);
            if (apiResponse.isSuccess && apiResponse.responseData?.fileUrl) {
                companyData.companyThumbnailUrl = apiResponse.responseData.fileUrl;
                jobDetails.companyThumbnailUrl = apiResponse.responseData.fileUrl;
            }
        }

        await RealtimeDbUtils.writeData({
            collectionName: SharedFirestoreCollectionDB.JobsDiscoverProject.CompanyDataRTDB,
            docId: companyData.uid,
            data: companyData,
            merge: false,
        });
    }

    static generateJobPostNotification(jobPost: JobDetailsModel): NotificationQueueItemModel {
        return {
            uid: jobPost.uid!,
            description: SharedJobsDiscoverData.generateSocialPost(jobPost),
            link: BackendRouteUtils.getJobPostDetailsPage(jobPost.uid!),
            title: `${jobPost.title} @${jobPost.company} (${jobPost.remoteRegion})`,
            pushNotification: true,
            bgImageUrl: "",
            socialNetworks: ["THREADS", "FACEBOOK", "BLUESKY", "TELEGRAM"],
            category: jobPost.category,
            socialPost: SharedJobsDiscoverData.generateSocialPost(jobPost),
            socialShortPost: SharedJobsDiscoverData.generateSocialShortPost(jobPost),
            registerTime: Date.now(),
            instagramOnlyBgImage: true,
            type: "JOB_POST",
            extraData: {
                category: jobPost.category,
                remoteRegion: jobPost.remoteRegion,
            }
        }
    }


}