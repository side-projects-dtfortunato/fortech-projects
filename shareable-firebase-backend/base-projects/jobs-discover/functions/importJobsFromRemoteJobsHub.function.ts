import * as functions from "firebase-functions";
import * as admin from "firebase-admin";
import {CustomJobsDiscoverUtils} from "../../../../custom-project/utils/custom-jobs-discover.utils";
import {SharedFirestoreCollectionDB} from "../../../model-data/shared-firestore-collections";
import {NotificationsQueueManager} from "../../../utils/notifications-queue.manager";
import {SharedJobsDiscoverData} from "../shared-jobs-discover.data";
import {getQuickResponse} from "../../../model-data/api-response.model";

export const importJobsFromRemoteJobsHubFunction = functions.https.onRequest(
    async (req, resp) => {

        // Import list of jobs
        const listJobs = await CustomJobsDiscoverUtils.importJobsFromRemoteJobsHub();


        const batch = admin.firestore().batch();
        if (listJobs && listJobs.length > 0) {
            for (let jobDetails of listJobs) {
                // Save the Job Details in Firestore
                batch.set(admin.firestore()
                    .collection(SharedFirestoreCollectionDB.JobsDiscoverProject.JobDetails)
                    .doc(jobDetails.uid!), jobDetails, {merge: false});


                // Register to dispatch a notification
                // await NotificationsQueueManager.addNotificationItemToQueue(SharedJobsDiscoverData.generateJobPostNotification(jobDetails));
                // Removed this because supposedly it's already dispatched in the on-job-updated function
            }
        }


        await batch.commit();

        resp.send(getQuickResponse(true, listJobs));
        return;
    });