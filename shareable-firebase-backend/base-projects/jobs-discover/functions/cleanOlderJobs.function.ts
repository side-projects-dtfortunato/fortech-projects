import * as functions from 'firebase-functions';
import * as admin from 'firebase-admin';
import {SharedFirestoreCollectionDB} from "../../../model-data/shared-firestore-collections";
import {getQuickResponse} from "../../../model-data/api-response.model";


/**
 * TODO Call this function once a day
 */
export const cleanOlderJobsFunction = functions.https.onRequest(
    async (req, resp) => {

        // Calculate the date 20 days ago
        const twentyDaysAgo = new Date();
        twentyDaysAgo.setDate(twentyDaysAgo.getDate() - 20); // Remove jobs older than 20 days

        try {
            // Query for jobs older than 20 days
            const expiredJobsQuery = admin.firestore()
                .collection(SharedFirestoreCollectionDB.JobsDiscoverProject.JobDetails)
                .where('createdAt', '<', twentyDaysAgo.getTime())
                .limit(50);

            // Delete expired jobs in batches
            const snapshot = await expiredJobsQuery.get();

            if (snapshot.size === 0) {
                resp.send(getQuickResponse(false, null, "Nothing to remove"));
                return;
            }

            // Delete documents in a batch
            const batch = admin.firestore().batch();
            snapshot.docs.forEach((doc) => {
                batch.delete(doc.ref);
            });
            await batch.commit();

            console.log('Expired jobs removed successfully');
            resp.send(getQuickResponse(true));
        } catch (error) {
            console.error('Error removing expired jobs:', error);
            resp.send(getQuickResponse(false));
        }

    });