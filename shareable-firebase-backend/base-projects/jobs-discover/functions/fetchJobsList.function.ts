import * as functions from "firebase-functions";
import * as admin from "firebase-admin";
import {JobDetailsModel, JobListModel, LinkedinSourceModel, SharedJobsDiscoverData} from "../shared-jobs-discover.data";
import {RealtimeDbUtils} from "../../../utils/realtime-db.utils";
import {SharedFirestoreCollectionDB} from "../../../model-data/shared-firestore-collections";
import {BackendSharedUtils} from "../../../utils/backend-shared-utils";
import {
    ALLOWED_WORK_MODE,
    CustomJobsDiscoverUtils
} from "../../../../custom-project/utils/custom-jobs-discover.utils";
import {getQuickResponse} from "../../../model-data/api-response.model";


export const fetchJobsList = functions.runWith({
    memory: "1GB",
    timeoutSeconds: 120,
}).https.onRequest(
    async (req, resp) => {

        let loadedSources: undefined | LinkedinSourceModel[] = await RealtimeDbUtils.getData({
            collectionName: "JobLinkedinSourcedLoaded",
        });
        if (!loadedSources) {
            loadedSources = [];
        }
        // Next source to load
        let nextSourceToLoad: LinkedinSourceModel | undefined;

        // Find the next source to load
        for (const source of SharedJobsDiscoverData.getListLinkedinSources()) {
            if (!loadedSources.some(loadedSource => loadedSource.id === source.id)) {
                nextSourceToLoad = source;
                break;
            }
        }

        // If all sources have been loaded, start again from the beginning
        if (!nextSourceToLoad) {
            nextSourceToLoad = SharedJobsDiscoverData.getListLinkedinSources()[0];
            loadedSources = []; // Reset loaded sources
        }

        // Add the next source to load to loadedSources
        loadedSources.push(nextSourceToLoad);


        const jobsList = await SharedJobsDiscoverData.extractJobListings(nextSourceToLoad.url, nextSourceToLoad.region, nextSourceToLoad.category);
        let jobsListObj: { [jobId: string]: JobListModel } = {};
        if (jobsList && jobsList.length > 0) {
            let randomJobsList: JobListModel[] = BackendSharedUtils.getRandomItems(jobsList.slice(0, 50), 10)
            for (let jobItem of randomJobsList) {
                if (!(await RealtimeDbUtils.getData({
                    collectionName: SharedFirestoreCollectionDB.JobsDiscoverProject.DigestedJobsIndexRTDB,
                    docId: jobItem.uid!
                }))) {
                    // Only import if the job title + the company name wasn't digested already
                    jobsListObj = {
                        ...jobsListObj,
                        [jobItem.uid!]: jobItem,
                    };
                }
            }

            // Save Jobs List in a Doc
            await RealtimeDbUtils.writeData({
                collectionName: SharedFirestoreCollectionDB.JobsDiscoverProject.ImportedJobsListRTDB,
                data: jobsListObj,
                merge: true,
            });
        }

        // Update the loadedSources in the database
        await RealtimeDbUtils.writeData({
            collectionName: "JobLinkedinSourcedLoaded",
            data: loadedSources,
            merge: false,
        });


        // Send response
        resp.send(getQuickResponse(true, {jobs: Object.keys(jobsListObj).length}))
        return;
    }
);

export const digestImportedJobsList = functions.runWith({
    memory: "1GB",
    timeoutSeconds: 240,
}).https.onRequest(
    async (req, resp) => {
        try {
            // Get the list of jobs from the Realtime DB
            const importedJobs = await RealtimeDbUtils.getData({
                collectionName: SharedFirestoreCollectionDB.JobsDiscoverProject.ImportedJobsListRTDB
            }) as { [jobId: string]: JobListModel };

            if (!importedJobs) {

                resp.send(getQuickResponse(false, null, "No Imported Jobs Found"));
                return;
            }

            // Convert the object to an array and pick 1 random jobs
            const jobsArray = Object.values(importedJobs);
            const selectedJobs = getRandomJobs(jobsArray, 3);
            const extractedJobsSuccess: JobDetailsModel[] = [];

            const batch = admin.firestore().batch();
            let hasJobValid: boolean = false;
            for (const job of selectedJobs) {
                if (!hasJobValid) {
                    // Get the Job details
                    let jobDetails = await SharedJobsDiscoverData.extractJobDetails(job.link, job);

                    if (jobDetails) {
                        hasJobValid = true;

                        // Remove the job from the list no matter what
                        delete importedJobs[job.uid!];

                        if (jobDetails && await SharedJobsDiscoverData.isJobDetailsValid(jobDetails)) {
                            console.log("Valid JobDetails Extracted: ", jobDetails);

                            // Extract Salary Range from AI
                            jobDetails = await SharedJobsDiscoverData.digestJobDataWithAI(jobDetails, CustomJobsDiscoverUtils.getIsValidRule());
                            if (jobDetails && jobDetails.workMode && ALLOWED_WORK_MODE.includes(jobDetails.workMode)) {
                                extractedJobsSuccess.push(jobDetails);
                                // Save the Job Details in Firestore
                                batch.set(admin.firestore()
                                    .collection(SharedFirestoreCollectionDB.JobsDiscoverProject.JobDetails)
                                    .doc(job.uid!), jobDetails, {merge: false});
                            } else {
                                // Save it in the realtime index if is not valid
                                await RealtimeDbUtils.writeData({
                                    collectionName: SharedFirestoreCollectionDB.JobsDiscoverProject.DigestedJobsIndexRTDB,
                                    docId: job.uid!,
                                    data: {
                                        title: job.title,
                                        company: job.company,
                                        timestamp: Date.now(),
                                    },
                                    merge: false,
                                });
                            }
                        } else {
                            // Save it in the realtime index if couldn't
                            await RealtimeDbUtils.writeData({
                                collectionName: SharedFirestoreCollectionDB.JobsDiscoverProject.DigestedJobsIndexRTDB,
                                docId: job.uid!,
                                data: {
                                    title: job.title,
                                    company: job.company,
                                    timestamp: Date.now(),
                                },
                                merge: false,
                            });
                        }
                    }
                }

            }
            await batch.commit();

            // Clean up old jobs
            const updatedImportedJobs = Object.fromEntries(
                Object.entries(importedJobs).filter(([_, job]) => {
                    return BackendSharedUtils.isLessThanHoursAgo(job.createdAt!, 24);
                })
            );

            // Update the ImportedJobsListRTDB with cleaned up data
            await RealtimeDbUtils.writeData({
                collectionName: SharedFirestoreCollectionDB.JobsDiscoverProject.ImportedJobsListRTDB,
                data: updatedImportedJobs,
                merge: false, // Overwrite the entire object
            });

            resp.send(getQuickResponse(true, {
                message: 'Jobs processed and old entries cleaned up successfully',
                selectedJobs: selectedJobs,
                extractedJobsSuccess: extractedJobsSuccess,
            }));
            return;
        } catch (error) {
            resp.send(getQuickResponse(false, null, "Error processing imported jobs"));
            console.error('Error in digestImportedJobsList:', error);
            return;
        }
    }
);

function getRandomJobs(jobs: JobListModel[], count: number): JobListModel[] {
    const shuffled = [...jobs].sort(() => 0.5 - Math.random());
    return shuffled.slice(0, count);
}


const remoteWorkKeywords: string[] = [
    "remote",
    "work from home",
    "wfh",
    "telecommute",
    "virtual",
    "distributed team",
    "home-based",
    "anywhere",
    "location-independent",
    "flexible location",
    "digital nomad",
    "online position",
    "remote-first",
    "fully remote",
    "100% remote",
    "telework",
    "off-site",
    "hybrid",
    "geographic flexibility",
    "work from anywhere",
    "cloud-based position"
];

function isRemoteJob(title: string, description: string): boolean {
    const combinedText = (title + " " + description).toLowerCase();

    return remoteWorkKeywords.some(keyword => combinedText.includes(keyword));
}