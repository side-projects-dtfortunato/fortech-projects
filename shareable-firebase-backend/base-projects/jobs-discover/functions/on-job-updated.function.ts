import * as functions from "firebase-functions";
import {SharedFirestoreCollectionDB} from "../../../model-data/shared-firestore-collections";
import {Log} from "../../../utils/log";
import {JobDetailsModel, SharedJobsDiscoverData} from "../shared-jobs-discover.data";
import * as admin from "firebase-admin";
import {BackendSharedUtils} from "../../../utils/backend-shared-utils";
import {RealtimeDbUtils} from "../../../utils/realtime-db.utils";
import {NotificationsQueueManager} from "../../../utils/notifications-queue.manager";
import {BackendRouteUtils} from "../../../utils/backend-route.utils";

export const onJobUpdated = functions
    .firestore
    .document(SharedFirestoreCollectionDB.JobsDiscoverProject.JobDetails + '/{jobId}')
    .onWrite(async (data, context) => {
        // Update catalog data
        Log.onWrite(data);
        let jobDetailsData: JobDetailsModel | undefined = data.after.data() ? data.after.data() as JobDetailsModel : data.before.data() as JobDetailsModel;
        let hasRemoved: boolean = !data.after.exists;
        let hasCreated: boolean = !data.before.exists;

        // Update Published Day
        if (jobDetailsData) {
            await admin.firestore().collection(SharedFirestoreCollectionDB.JobsDiscoverProject.DailyPublishedJobs)
                .doc(jobDetailsData.publishedDayId!.toString()).set({
                    publishedDayId: jobDetailsData.publishedDayId!,
                    listJobs: {
                        [jobDetailsData.uid!]: hasRemoved ? admin.firestore.FieldValue.delete() : jobDetailsData,
                    }
                }, {merge: true});

            if (hasCreated) {
                // Save it in the realtime index
                await RealtimeDbUtils.writeData({
                    collectionName: SharedFirestoreCollectionDB.JobsDiscoverProject.DigestedJobsIndexRTDB,
                    docId: jobDetailsData.uid!,
                    data: {
                        title: jobDetailsData.title,
                        company: jobDetailsData.company,
                        timestamp: Date.now(),
                    },
                    merge: false,
                });

                // Save Skills counter
                if (jobDetailsData.skills && jobDetailsData.skills.length > 0) {
                    for (let skill of jobDetailsData.skills) {
                        await RealtimeDbUtils.writeData({
                            collectionName: SharedFirestoreCollectionDB.JobsDiscoverProject.JobSkillsCounterRTDB,
                            docId: skill.toUpperCase(),
                            data: admin.database.ServerValue.increment(1),
                            merge: false,
                        });
                    }
                }


                if (BackendSharedUtils.isProjectProduction()) {
                    // Register to dispatch a notification
                    await NotificationsQueueManager.addNotificationItemToQueue(SharedJobsDiscoverData.generateJobPostNotification(jobDetailsData));
                }

            } else {
                // Delete it in the realtime index
                await RealtimeDbUtils.deleteData({
                    collectionName: SharedFirestoreCollectionDB.JobsDiscoverProject.DigestedJobsIndexRTDB,
                    docId: jobDetailsData.uid!,
                });
            }
        }

    });