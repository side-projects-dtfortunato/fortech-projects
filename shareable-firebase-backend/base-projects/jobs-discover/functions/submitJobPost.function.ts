import * as functions from "firebase-functions";
import {CommentAttachedItemModel, CommunityParentRefs} from "../../community/data/community-data.model";
import {JobDetailsModel, SharedJobsDiscoverData} from "../shared-jobs-discover.data";
import {BackendSharedUtils} from "../../../utils/backend-shared-utils";
import * as admin from "firebase-admin";
import {SharedFirestoreCollectionDB} from "../../../model-data/shared-firestore-collections";
import {getQuickResponse} from "../../../model-data/api-response.model";
import {NotificationsQueueManager} from "../../../utils/notifications-queue.manager";
import {BackendRouteUtils} from "../../../utils/backend-route.utils";
import {WEBAPP_CONFIGS} from "../../../../configs/webapp.configs";
import {StripeUtils} from "../../../utils/payments/stripe-utils";
import {getDocumentData} from "../../../utils/utils";
import {AwsEmailServiceUtils} from "../../../utils/aws-email-service.utils";
import {RealtimeDbUtils} from "../../../utils/realtime-db.utils";

/**
 * @deprecated
 */
export const submitJobPost = functions.https.onCall(
    async (data: {jobPost: JobDetailsModel},
           context) => {

        const finalJobPostData: JobDetailsModel = {
            ...data.jobPost,
            link: "",
            authorization: {
                allRead: true,
                allWrite: true,
            },
            updatedAt: Date.now(),
            createdAt: Date.now(),
            publishedDayId: BackendSharedUtils.getTodayDay(),
            uid: BackendSharedUtils.string_to_slug(data.jobPost.company) + "-" + BackendSharedUtils.string_to_slug(data.jobPost.title),
            description: SharedJobsDiscoverData.convertHTMLtoMarkdown(data.jobPost.description),
        };

        // Add the job post to a pending payment collection
        // Function to generate the Checkout link and return it here
        // Webhook to receive the payment, publish the job post (if featured, add it to the Featured day), remove it from the waiting payment collection and send an email confirming the payment and the publication of the job post
        await admin.firestore()
            .collection(SharedFirestoreCollectionDB.JobsDiscoverProject.SubmitJobWaitingPayment)
            .doc(finalJobPostData.uid!).set(finalJobPostData, {merge: false});

        const sessionId: string = await createStripeCheckoutSession(finalJobPostData);

        if (sessionId) {
            return getQuickResponse(true, {redirectUrl: await StripeUtils.getSessionCheckoutUrl({
                    sessionId: sessionId,
                    stripeApi: StripeUtils.getStripeApiKey(),
                })});
        } else {
            return getQuickResponse(false);
        }
    });


async function createStripeCheckoutSession(jobPostData: JobDetailsModel) {

    // Define price based on the plan
    const price = jobPostData.publishPlanType === 'BASIC' ? 1000 : 10000; // in cents

    const stripe = require('stripe')(StripeUtils.getStripeApiKey());

    const session = await stripe.checkout.sessions.create({
        payment_method_types: ['card'],
        line_items: [
            {
                price_data: {
                    currency: 'usd',
                    product_data: {
                        name: `${WEBAPP_CONFIGS.SITE_NAME} Job Post`,
                        description: `${jobPostData.title} - ${jobPostData.publishPlanType} plan`,
                    },
                    unit_amount: price,
                },
                quantity: 1,
            },
        ],
        mode: 'payment',
        allow_promotion_codes: true,
        success_url: `${WEBAPP_CONFIGS.WEBSITE_URL}/job/job-post-submission?session_id={CHECKOUT_SESSION_ID}&result=success&jobId=${jobPostData.uid!}`,
        cancel_url: `${WEBAPP_CONFIGS.WEBSITE_URL}/job/job-post-submission?result=cancel`,
        client_reference_id: jobPostData.uid!,
    });

    return session.id;
}

export const submitJobPostStripeWebhook = functions.https.onRequest(async (req, res) => {
    const sig = req.headers['stripe-signature'];

    let event;
    const stripe = require('stripe')(StripeUtils.getStripeApiKey());
    const webhookKey: string = StripeUtils.getStripeWebhookKey();

    try {
        event = stripe.webhooks.constructEvent(req.rawBody.toString(), sig, webhookKey);
    } catch (err: any) {
        res.status(400).send(`Webhook Error: ${err.message}`);
        return;
    }

    if (event.type === 'checkout.session.completed') {
        const session = event.data.object;
        const jobPostId = session.client_reference_id;

        // Get Job Post Data
        let jobDetails: JobDetailsModel = await RealtimeDbUtils.getData({
            collectionName: SharedFirestoreCollectionDB.JobsDiscoverProject.SubmitJobWaitingPayment,
            docId: jobPostId
        });
        // old implementation const jobDetails: JobDetailsModel = await getDocumentData(SharedFirestoreCollectionDB.JobsDiscoverProject.SubmitJobWaitingPayment, jobPostId);
        jobDetails.publishedDayId = BackendSharedUtils.getTodayDay();

        // Generate job skills
        jobDetails = await SharedJobsDiscoverData.generateJobSkills(jobDetails);

        const batch = admin.firestore().batch();

        // Update job post in Firestore
        jobDetails.updatedAt = Date.now();
        batch.set(admin.firestore().collection(SharedFirestoreCollectionDB.JobsDiscoverProject.JobDetails).doc(jobPostId), jobDetails);
        // batch.delete(admin.firestore().collection(SharedFirestoreCollectionDB.JobsDiscoverProject.SubmitJobWaitingPayment).doc(jobPostId));
        await batch.commit();
        await RealtimeDbUtils.deleteData({
            collectionName: SharedFirestoreCollectionDB.JobsDiscoverProject.SubmitJobWaitingPayment,
            docId: jobPostId
        });

        // Perform any additional actions (e.g., send confirmation email, update user's account, etc.)
        if (jobDetails.publisherEmail) {
            const confirmationEmailContent = generateJobPostConfirmationEmail({
                jobDetails: jobDetails,
                socialNetworks: [],
            });

            await AwsEmailServiceUtils.sendDigestEmail({
                sentFromEmail: WEBAPP_CONFIGS.COMPANY_EMAIL,
                senderName: WEBAPP_CONFIGS.SITE_NAME,
                subject: confirmationEmailContent.emailSubject,
                recipientName: jobDetails.company,
                recipientEmail: jobDetails.publisherEmail,
                content: confirmationEmailContent.html,
            });
        }


        // Send a push notification
        await NotificationsQueueManager.addNotificationItemToQueue(SharedJobsDiscoverData.generateJobPostNotification(jobDetails));
    }

    res.json(getQuickResponse(true));
});



interface JobPostConfirmationEmailOptions {
    jobDetails: JobDetailsModel;
    socialNetworks: { name: string; url: string }[];
}

function generateJobPostConfirmationEmail(options: JobPostConfirmationEmailOptions): { html: string; emailSubject: string } {
    const { jobDetails, socialNetworks } = options;

    const jobLink = BackendRouteUtils.getJobPostDetailsPage(jobDetails.uid!);

    const html = `
    <!DOCTYPE html>
    <html lang="en">
    <head>
      <meta charset="UTF-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <title>Job Post Confirmation - ${WEBAPP_CONFIGS.SITE_NAME}</title>
    </head>
    <body style="font-family: Arial, sans-serif; line-height: 1.6; color: #444444; max-width: 600px; margin: 0 auto; padding: 20px;">
      <div style="text-align: center; margin-bottom: 20px;">
        <img src="${WEBAPP_CONFIGS.WEBSITE_URL + "/images/logo.png"}" alt="${WEBAPP_CONFIGS.SITE_NAME}" style="max-width: 200px;">
        <h1 style="font-size: 24px; font-weight: bold; color: #333333;">Job Post Confirmation</h1>
      </div>

      <div style="margin-bottom: 30px;">
        <p>Dear ${jobDetails.company},</p>
        <p>Thank you for submitting your job post on ${WEBAPP_CONFIGS.SITE_NAME}. We're excited to help you find the perfect candidate!</p>
        <p>Here's a summary of your job post:</p>
      </div>

      <div style="background-color: #f9f9f9; border-radius: 10px; padding: 20px; margin-bottom: 30px;">
        <h2 style="font-size: 20px; margin-bottom: 15px; color: #333333;">${jobDetails.title}</h2>
        <p><strong>Company:</strong> ${jobDetails.company}</p>
        <p><strong>Location:</strong> ${jobDetails.location}</p>
        <p><strong>Work Mode:</strong> ${jobDetails.workMode || 'Not specified'}</p>
        ${jobDetails.remoteRegion ? `<p><strong>Remote Region:</strong> ${jobDetails.remoteRegion}</p>` : ''}
        <p><strong>Category:</strong> ${jobDetails.category || 'Not specified'}</p>
        <p><strong>Experience Level:</strong> ${jobDetails.experienceLevel || 'Not specified'}</p>
        <p><a href="${jobLink}" style="display: inline-block; padding: 10px 20px; background-color: #007bff; color: #ffffff; text-decoration: none; border-radius: 5px;">View Job Post</a></p>
      </div>

      <div style="margin-bottom: 30px;">
        <p>Your Job Post will be published immediately</p>
        <p>If you need to make any changes or have any questions, please don't hesitate to contact our support team.</p>
      </div>

      <div style="text-align: center; font-size: 12px; color: #666666; margin-top: 30px;">
        <div style="margin-bottom: 10px;">
          ${socialNetworks.map(network => `
            <a href="${network.url}" style="display: inline-block; margin: 0 5px; color: #333333; text-decoration: none;">${network.name}</a>
          `).join('')}
        </div>
        <p>${WEBAPP_CONFIGS.COMPANY_ADDRESS}</p>
        <p>${WEBAPP_CONFIGS.COMPANY_EMAIL}</p>
      </div>
    </body>
    </html>
  `;

    return {
        html: html,
        emailSubject: `Job Post Confirmation: ${jobDetails.title} - ${WEBAPP_CONFIGS.SITE_NAME}`,
    };
}