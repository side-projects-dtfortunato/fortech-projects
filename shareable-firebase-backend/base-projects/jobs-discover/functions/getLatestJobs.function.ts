import * as functions from "firebase-functions";
import * as admin from "firebase-admin";
import {SharedFirestoreCollectionDB} from "../../../model-data/shared-firestore-collections";
import {JobDetailsModel, JobRemoteRegion} from "../shared-jobs-discover.data";

export const getLatestJobsFunction = functions.https.onRequest(
    async (req, resp) => {
        try {
            // Set CORS headers
            resp.set('Access-Control-Allow-Origin', '*');
            if (req.method === 'OPTIONS') {
                // Send response to OPTIONS requests
                resp.set('Access-Control-Allow-Methods', 'GET');
                resp.set('Access-Control-Allow-Headers', 'Content-Type');
                resp.set('Access-Control-Max-Age', '3600');
                resp.status(204).send('');
                return;
            }

            // Get query parameters
            const remoteRegion = req.query.remoteRegion as JobRemoteRegion | undefined;
            const category = req.query.category as string | undefined;

            // Calculate the timestamp for 24 hours ago
            const twentyFourHoursAgo = Date.now() - 24 * 60 * 60 * 1000;

            // Start building the query
            let query = admin.firestore().collection(SharedFirestoreCollectionDB.JobsDiscoverProject.JobDetails)
                .where('createdAt', '>=', twentyFourHoursAgo)
                .orderBy('createdAt', 'desc');

            // Add remoteRegion filter if provided
            if (remoteRegion) {
                query = query.where('remoteRegion', '==', remoteRegion);
            }

            // Add category filter if provided
            if (category) {
                query = query.where('category', '==', category);
            }

            // Execute the query with a limit
            const jobsSnapshot = await query.limit(15).get();

            const jobs = jobsSnapshot.docs.map(doc => {
                const data = doc.data() as JobDetailsModel;
                return {
                    uid: doc.id,
                    ...data,
                };
            });

            resp.status(200).json(jobs);
        } catch (error) {
            console.error('Error fetching jobs:', error);
            resp.status(500).send('Internal Server Error');
        }
    }
);