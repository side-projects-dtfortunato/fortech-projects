import * as functions from "firebase-functions";
import {getQuickResponse} from "../../../model-data/api-response.model";
import {Log} from "../../../utils/log";
import {
    SharedPublicUserprofileModel,
    UserShortProfileModel
} from "../../../model-data/frontend-model-data/shared-public-userprofile.model";
import {getDocumentData} from "../../../utils/utils";
import {SharedFirestoreCollectionDB, SharedFirestoreDocsDB} from "../../../model-data/shared-firestore-collections";
import {
    CommunityCommentPostModel,
    CommunityCommentReplyModel
} from "../data/community-data.model";
import * as admin from "firebase-admin";
import {EmailTemplateDigestUtils} from "../../../utils/email-utils/email-template-digest-v2.utils";
import {BackendSharedUtils} from "../../../utils/backend-shared-utils";
import {WEBAPP_CONFIGS} from "../../../../configs/webapp.configs";
import {SharedNewsletterUserModel} from "../../../model-data/frontend-model-data/shared-newsletter-user.model";
import {RealtimeDbUtils} from "../../../utils/realtime-db.utils";
import {EmailDestinationModel} from "../../../utils/email-utils/email-sender-utils";
import {EmailBulkDeliveryModel} from "../../../model-data/email-builk-delivery.model";
import { BackendRouteUtils } from "../../../utils/backend-route.utils";
import { OnesignalUtils } from "../../../utils/onesignal.utils";

export const communityReplyComment = functions.https.onCall(
    async (data: {comment: string, parentCommentId: string},
           context) => {
        if (!context.auth || !context.auth.uid) return getQuickResponse(false, undefined, "User not authorized");
        const userId = context && context.auth && context.auth.uid ? context.auth.uid : "";
        Log.onCall(data, userId);


        // Check if the user have the required infos
        const userProfile: SharedPublicUserprofileModel | undefined = await getDocumentData(SharedFirestoreCollectionDB.PublicUserProfile, userId);
        if (!userProfile || !userProfile.username || !userProfile.name) {
            return getQuickResponse(false, null, "Profile not completed");
        }

        // Get the parent comment
        const parentCommunityPost: CommunityCommentPostModel | undefined = await getDocumentData<CommunityCommentPostModel>(SharedFirestoreCollectionDB.CommunityCommentPosts, data.parentCommentId);
        if (!parentCommunityPost) {
            return getQuickResponse(false, null, "Comment post not found");
        }

        // Generate the reply comment
        const newReplyComment: CommunityCommentReplyModel = {
            uid: data.parentCommentId + "-" + Date.now(),
            parentCommentId: data.parentCommentId,
            createdAt: Date.now(),
            updatedAt: Date.now(),
            message: data.comment,
            reactions: {},
            userCreatorId: userId,
            authorization: {allRead: true, writeUIDs: [userId]},
        };

        // Check if the parent already had the new user infos
        if (!parentCommunityPost.usersInfos[userId]) {
            parentCommunityPost.usersInfos = {
                ...parentCommunityPost.usersInfos,
                [userId]: {
                    uid: userId,
                    name: userProfile.name,
                    username: userProfile.username,
                    pictureUrl: userProfile.userPicture ? userProfile.userPicture.fileUrl : "",
                    location: userProfile.location,
                    subtitleLabel: userProfile.publicSubtitle,
                } as UserShortProfileModel
            }
        }
        // add the new reply
        parentCommunityPost.replies = {
            ...parentCommunityPost.replies,
            [newReplyComment.uid!]: newReplyComment,
        };

        // Update the community post
        const batch = admin.firestore().batch();
        batch.set(admin.firestore().collection(SharedFirestoreCollectionDB.CommunityCommentPosts).doc(
            data.parentCommentId
        ), parentCommunityPost, {merge: true});

        // Increment comments counter if have a parent ref
        if (parentCommunityPost.parentRefIds) {
            batch.set(admin.firestore().collection(parentCommunityPost.parentRefIds.parentCollectionId).doc(parentCommunityPost.parentRefIds.parentDocId), {stats: {commentsCounter: admin.firestore.FieldValue.increment(1)}}, {merge: true});
        }
        await batch.commit();

        await dispatchNewCommentReplyEmailNotif(parentCommunityPost, newReplyComment);

        return getQuickResponse(true, parentCommunityPost);
    });



export async function dispatchNewCommentReplyEmailNotif(comment: CommunityCommentPostModel, newReplyComment: CommunityCommentReplyModel): Promise<void> {
    const html = EmailTemplateDigestUtils.generateNewCommentReplyNotification(comment.uid!);

    const emailContent = {
        html: html,
        emailTitle: "Your Comment Received a Reply",
    };


    // Get subscribed users
    let subscribedUsers: { [userId: string]: SharedNewsletterUserModel } | undefined = await RealtimeDbUtils.getData({
        collectionName: SharedFirestoreCollectionDB.NewsletterCatalog,
        docId: SharedFirestoreDocsDB.NewsletterCatalog.SubscribedUsers,
    });

    // Send push notification to subscribed users  
    await OnesignalUtils.sendPushNotificationToExternalUserIds(
        "New Comment Reply",
        `@${comment.usersInfos[newReplyComment.userCreatorId]?.username} said "${newReplyComment.message}"`,
        BackendRouteUtils.getCommunityDetailsPage(comment.uid!),
        Object.keys(comment.usersInfos)
    );

    let usersToDispatch: SharedNewsletterUserModel[] = Object.keys(comment.usersInfos)
        .filter((userId) => {
            if (userId !== newReplyComment.userCreatorId && subscribedUsers![userId]) // Avoid the creator of the new comment that originated this notification
                return true
            else
                return false
        }).map((userId) => subscribedUsers![userId]);

    if (usersToDispatch) {
        let destionationsList: EmailDestinationModel[] = Object.values(usersToDispatch)
            .filter((subscribedUser) => {
                return subscribedUser.userId && BackendSharedUtils.isValidEmail(subscribedUser.email);
            }).map((subscriptionItem) => {
                return {
                    uid: subscriptionItem.userId,
                    email: subscriptionItem.email,
                    name: subscriptionItem.name ? subscriptionItem.name : subscriptionItem.email,
                };
            });

        // Store to later be delivered
        await RealtimeDbUtils.writeData({
            collectionName: SharedFirestoreCollectionDB.RTEmailBulkToDeliver,
            docId: Date.now().toString(),
            data: {
                bodyHtml: emailContent.html,
                destinationsEmail: destionationsList,
                subject: emailContent.emailTitle,
                sentFrom: {
                    name: WEBAPP_CONFIGS.SITE_NAME,
                    email: "contact@" + WEBAPP_CONFIGS.SITE_NAME,
                },
                createdAt: Date.now(),
            } as EmailBulkDeliveryModel,
            merge: false,
        });
    }
}
