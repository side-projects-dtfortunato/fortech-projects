import * as functions from "firebase-functions";
import {getQuickResponse} from "../../../model-data/api-response.model";
import {Log} from "../../../utils/log";
import {
    SharedPublicUserprofileModel,
    UserShortProfileModel
} from "../../../model-data/frontend-model-data/shared-public-userprofile.model";
import {getDocumentData} from "../../../utils/utils";
import {
    SharedFirestoreCollectionDB,
    SharedRealtimeCollectionDB
} from "../../../model-data/shared-firestore-collections";
import * as admin from "firebase-admin";
import {BackendSharedUtils} from "../../../utils/backend-shared-utils";
import {
    CommentAttachedItemModel,
    CommunityCommentPostModel, CommunityParentRefs
} from "../data/community-data.model";
import {RealtimeDbUtils} from "../../../utils/realtime-db.utils";
import {NotificationsQueueManager} from "../../../utils/notifications-queue.manager";
import {WEBAPP_CONFIGS} from "../../../../configs/webapp.configs";

export const communityPostComment = functions.https.onCall(
    async (data: {comment: string, parentRefsId?: CommunityParentRefs,
               attachedItem?: CommentAttachedItemModel},
           context) => {
        if (!context.auth || !context.auth.uid) return getQuickResponse(false, undefined, "User not authorized");
        const userId = context && context.auth && context.auth.uid ? context.auth.uid : "";
        Log.onCall(data, userId);

        // Check if the user have the required infos
        const userProfile: SharedPublicUserprofileModel | undefined = await getDocumentData(SharedFirestoreCollectionDB.PublicUserProfile, userId);
        if (!userProfile || !userProfile.username || !userProfile.name) {
            return getQuickResponse(false, null, "Profile not completed");
        }



        // Add the user profile
        const generatedPostCommentData: CommunityCommentPostModel = {
            uid: admin.firestore().collection(SharedFirestoreCollectionDB.CommunityCommentPosts).doc().id,
            replies: {},
            pinned: false,
            createdAt: Date.now(),
            updatedAt: Date.now(),
            message: data.comment,
            reactions: {},
            userCreatorId: userId,
            usersInfos: {
                [userId]: {
                    uid: userId,
                    name: userProfile.name,
                    username: userProfile.username,
                    pictureUrl: userProfile.userPicture ? userProfile.userPicture.fileUrl : "",
                    location: userProfile.location,
                    subtitleLabel: userProfile.publicSubtitle,
                } as UserShortProfileModel
            },
            publishedDayId: BackendSharedUtils.getTodayDay(),
        };
        if (data.attachedItem) {
            generatedPostCommentData.attachedItem = data.attachedItem;
        }
        if (data.parentRefsId) {
            generatedPostCommentData.parentRefIds = data.parentRefsId;
        }


        const batch = admin.firestore().batch();
        batch.create(admin.firestore().collection(SharedFirestoreCollectionDB.CommunityCommentPosts).doc(generatedPostCommentData.uid!), generatedPostCommentData);

        // Increment comments counter if have a parent ref
        if (data.parentRefsId) {
            batch.set(admin.firestore().collection(data.parentRefsId.parentCollectionId).doc(data.parentRefsId.parentDocId), {stats: {commentsCounter: admin.firestore.FieldValue.increment(1)}}, {merge: true});
        }
        await batch.commit();

        // Update Realtime DB with the most recent community post
        await RealtimeDbUtils.writeData({
            collectionName: SharedRealtimeCollectionDB.CommunityMostRecentPosts,
            docId: generatedPostCommentData.uid,
            data: Date.now(),
            merge: false,
        });

        // Add Push Notification
        await NotificationsQueueManager.addNotificationItemToQueue({
            uid: generatedPostCommentData.uid!,
            socialPost: "",
            instagramOnlyBgImage: false,
            title: `${userProfile.username} published a new comment`,
            description: `\"${generatedPostCommentData.message}\"`,
            link: WEBAPP_CONFIGS.WEBSITE_URL + "/community/" + generatedPostCommentData.uid,
            pushNotification: true,
            socialNetworks: [],
        });

        return getQuickResponse(true, generatedPostCommentData);
    });