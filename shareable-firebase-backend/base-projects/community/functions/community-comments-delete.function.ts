import * as functions from "firebase-functions";
import * as admin from "firebase-admin";
import {getQuickResponse} from "../../../model-data/api-response.model";
import {Log} from "../../../utils/log";
import {getDocumentData} from "../../../utils/utils";
import {SharedFirestoreCollectionDB} from "../../../model-data/shared-firestore-collections";
import {CommunityCommentPostModel} from "../data/community-data.model";

export const communityCommentsDelete = functions.https.onCall(
    async (data: {parentCommentId: string, replyCommentId?: string},
           context) => {
        if (!context.auth || !context.auth.uid) return getQuickResponse(false, undefined, "User not authorized");
        const userId = context && context.auth && context.auth.uid ? context.auth.uid : "";
        Log.onCall(data, userId);


        // Get the parent comment
        const parentCommunityPost: CommunityCommentPostModel | undefined = await getDocumentData<CommunityCommentPostModel>(SharedFirestoreCollectionDB.CommunityCommentPosts, data.parentCommentId);
        if (!parentCommunityPost) {
            return getQuickResponse(false, null, "Comment post not found");
        }

        // Check if is the creator
        if (parentCommunityPost.userCreatorId !== userId || (data.replyCommentId && parentCommunityPost.replies[data.replyCommentId].userCreatorId !== userId)) {
            return getQuickResponse(false, null, "Not authorized");
        }

        const batch = admin.firestore().batch();
        if (data.replyCommentId) {
            // Just delete the respective comment
            batch.set(admin.firestore().collection(SharedFirestoreCollectionDB.CommunityCommentPosts).doc(data.parentCommentId), {
                replies: {
                    [data.replyCommentId]: admin.firestore.FieldValue.delete(),
                },
            }, {merge: true});
            if (parentCommunityPost.parentRefIds) {
                batch.set(admin.firestore().collection(parentCommunityPost.parentRefIds.parentCollectionId).doc(parentCommunityPost.parentRefIds.parentDocId), {stats: {commentsCounter: admin.firestore.FieldValue.increment(-1)}}, {merge: true});
            }
        } else {
            batch.delete(admin.firestore().collection(SharedFirestoreCollectionDB.CommunityCommentPosts).doc(data.parentCommentId));
            if (parentCommunityPost.parentRefIds) {
                let deletedCommentsCounter: number = 1 + Object.keys(parentCommunityPost.replies).length;
                batch.set(admin.firestore().collection(parentCommunityPost.parentRefIds.parentCollectionId).doc(parentCommunityPost.parentRefIds.parentDocId), {stats: {commentsCounter: admin.firestore.FieldValue.increment(-deletedCommentsCounter)}}, {merge: true});
            }
        }
        await batch.commit();

        return getQuickResponse(true);
    });