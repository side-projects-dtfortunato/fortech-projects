import * as functions from "firebase-functions";
import {RealtimeDbUtils} from "../../../utils/realtime-db.utils";
import {SharedRealtimeCollectionDB} from "../../../model-data/shared-firestore-collections";
import {BackendSharedUtils} from "../../../utils/backend-shared-utils";
import {getQuickResponse} from "../../../model-data/api-response.model";

/**
 * This job will remove the register of the most recent community posts that are older than 2 days
 */
export const communityDailyCleanupJob = functions.https.onRequest(
    async (req, resp) => {
        const recentPosts:  {[communityPostId: string]: number} = await RealtimeDbUtils.getData({collectionName: SharedRealtimeCollectionDB.CommunityMostRecentPosts});

        let postsRemoved: string[] = [];
        if (recentPosts && Object.keys(recentPosts).length > 0) {
            for (let itemKey of Object.keys(recentPosts)) {
                if (!BackendSharedUtils.isLessThanHoursAgo(recentPosts[itemKey], 48)) {
                    postsRemoved.push(itemKey);
                    await RealtimeDbUtils.deleteData({collectionName: SharedRealtimeCollectionDB.CommunityMostRecentPosts, docId: itemKey});
                }
            }
        }

        resp.send(getQuickResponse(true, {postsRemoved}));
    });