import * as functions from "firebase-functions";
import * as admin from "firebase-admin";
import {CommunityCommentPostModel} from "../data/community-data.model";
import {RealtimeDbUtils} from "../../../utils/realtime-db.utils";
import {
    SharedFirestoreCollectionDB,
    SharedRealtimeCollectionDB
} from "../../../model-data/shared-firestore-collections";
import {Log} from "../../../utils/log";

export const onCommunityCommentPostUpdateFunction = functions
    .firestore
    .document(SharedFirestoreCollectionDB.CommunityCommentPosts + '/{commentId}')
    .onWrite(async (data, context) => {
        // Update catalog data
        Log.onWrite(data);
        let postCommunityComment: CommunityCommentPostModel | undefined = data.after.data() ? data.after.data() as CommunityCommentPostModel : data.before.data() as CommunityCommentPostModel;
        let hasRemoved: boolean = !data.after.exists;

        if (postCommunityComment && !hasRemoved && postCommunityComment.publishedDayId) {
            await admin.firestore().collection(SharedFirestoreCollectionDB.CommunityDailyPublishes)
                .doc(postCommunityComment.publishedDayId!.toString()).set({
                    publishedDayId: postCommunityComment.publishedDayId,
                    listPosts: {
                        [postCommunityComment.uid!]: postCommunityComment
                    }
                }, {merge: true});

            // Update realtime data base in case of parent refs
            if (postCommunityComment.parentRefIds) {
                await RealtimeDbUtils.writeData({
                    collectionName: SharedRealtimeCollectionDB.CommunityParentCommentsAggregator,
                    docId: postCommunityComment.parentRefIds.parentCollectionId,
                    subPaths: [postCommunityComment.parentRefIds.parentDocId, postCommunityComment.uid!],
                    data: postCommunityComment,
                    merge: false,
                });
            }
        } else if (hasRemoved && postCommunityComment) {
            await admin.firestore().collection(SharedFirestoreCollectionDB.CommunityDailyPublishes)
                .doc(postCommunityComment.publishedDayId!.toString()).set({
                    listPosts: {
                        [postCommunityComment.uid!]:  admin.firestore.FieldValue.delete(),
                    }
                }, {merge: true});
            // Update realtime data base in case of parent refs
            if (postCommunityComment.parentRefIds) {
                await RealtimeDbUtils.deleteData({
                    collectionName: SharedRealtimeCollectionDB.CommunityParentCommentsAggregator,
                    docId: postCommunityComment.parentRefIds.parentCollectionId,
                    subPaths: [postCommunityComment.parentRefIds.parentDocId, postCommunityComment.uid!],
                });
            }
        }

    });