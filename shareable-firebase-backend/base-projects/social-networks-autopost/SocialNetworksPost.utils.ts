
import {WEBAPP_CONFIGS} from "../../../configs/webapp.configs";
import axios from "axios";
import {ApiResponse, getQuickResponse} from "../../model-data/api-response.model";
import {StorageFileModel} from "../../model-data/storage-file.model";
import {FirebaseStorageUtils} from "../../utils/firebase-storage.utils";
import {RealtimeDbUtils} from "../../utils/realtime-db.utils";
import {BskyAgent, RichText} from "@atproto/api";
var FB = require('fb');
const Reddit = require('reddit')


const fb = new FB.Facebook({
    version: "v20.0"
});

export class SocialNetworksPostUtils {

    static async facebookPublishTextPost(params: {message: string, link?: string}): Promise<any> {
        return new Promise((resolve, reject) => {
            fb.setAccessToken(WEBAPP_CONFIGS.SOCIAL_NETWORK_API_KEY.metaAccessToken);
            fb.api(`${WEBAPP_CONFIGS.SOCIAL_NETWORK_API_KEY.facebookPageId}/feed`, 'post', params, function (res: any) {
                if(!res || res.error) {
                    console.log(!res ? 'error occurred' : res.error);
                    resolve(!res ? 'error occurred' : res.error);
                    return;
                } else {
                }
                console.log('Post Id: ' + res.id);
                resolve('Post Id: ' + res.id);
                return;
            });
        });
    }

    static async instagramPublishPhoto(params: {uid: string, imageBase64?: string, message: string, imageUrl?: string}): Promise<any> {

        try {
            // Upload to storage bucket
            const tempImageFileName = params.uid + ".jpeg";
            let imageUrl: string = "";
            if (params.imageUrl) {
                imageUrl = params.imageUrl!;
            } else if (params.imageBase64){
                let imageResponse: ApiResponse<StorageFileModel> = await FirebaseStorageUtils.uploadImage({
                    base64Image: params.imageBase64,
                    fileName: tempImageFileName,
                });
                if (!imageResponse || !imageResponse.responseData || !imageResponse.isSuccess) {
                    return "Failed to upload image to firebase storage";
                }
                imageUrl = imageResponse.responseData.fileUrl!;
            } else {
                return "Failed to find an image to upload";
            }

            // Step 1: Upload the image
            const mediaResponse = await axios.post(
                `https://graph.facebook.com/v20.0/${WEBAPP_CONFIGS.SOCIAL_NETWORK_API_KEY.instagramId}/media`,
                {
                    image_url: imageUrl,
                    caption: params.message,
                    access_token: WEBAPP_CONFIGS.SOCIAL_NETWORK_API_KEY.metaAccessToken,
                }
            );

            const mediaId = mediaResponse.data.id;

            // Delete temp image
            await FirebaseStorageUtils.deleteImage({fileName: tempImageFileName});

            // Step 2: Publish the media
            await axios.post(
                `https://graph.facebook.com/v20.0/${WEBAPP_CONFIGS.SOCIAL_NETWORK_API_KEY.instagramId}/media_publish`,
                {
                    creation_id: mediaId,
                    access_token: WEBAPP_CONFIGS.SOCIAL_NETWORK_API_KEY.metaAccessToken,
                }
            );

            console.log('Post published successfully!');
            return "Post published successfully!";
        } catch (error: any) {
            console.error('Error publishing post:', error.response?.data || error.message);
            return error;
        }
    }

    static async threadsPublishPost(params: {message: string}) {

        try {
            const accessToken = await RealtimeDbUtils.getData({
                collectionName: 'threadsTokens',
                docId: "token"
            });
            const userId = await RealtimeDbUtils.getData({
                collectionName: 'threadsTokens',
                docId: "userId",
            });

            if (!accessToken) {
                return getQuickResponse(false, null, "Access token not found. Please authorize the app first.");
            }


            // First: POST /{threads-user-id}/threads
            const threadContainerPost = await axios.post(
                `https://graph.threads.net/v1.0/${userId}/threads`, null, {
                    params: {
                        media_type: "TEXT",
                        text: params.message,
                        access_token: accessToken!,
                    }
                }
            );
            if (!threadContainerPost || !threadContainerPost.data.id) {
                return getQuickResponse(false, threadContainerPost.data);
            }

            // Second : POST /{threads-user-id}/threads_publish
            const threadPost = await axios.post(
                `https://graph.threads.net/v1.0/${userId}/threads_publish`, null, {
                    params: {
                        creation_id: threadContainerPost.data.id,
                        access_token: accessToken!,
                    }
                }
            );

            return getQuickResponse(true, threadPost.data);
        } catch (error) {
            console.error('Error publishing post:', error);
            getQuickResponse(false, error, "Failed to publish post.");
            return getQuickResponse(false);
        }
    }


    static async linkedinGetOrganizationId() {
        const accessToken = await RealtimeDbUtils.getData({
            collectionName: 'linkedinTokens',
            docId: "accessToken"
        });

        if (!accessToken) {
            console.error("Not authenticated");
            return null;
        }

        try {
            const response = await axios.get('https://api.linkedin.com/v2/organizations', {
                headers: {
                    Authorization: `Bearer ${accessToken}`,
                    'Content-Type': 'application/json'
                }
            });

            const organizations = response.data.elements;
            console.log('Organizations:', organizations);

            if (organizations.length > 0) {
                const organizationId = organizations[0].id;
                console.log('Organization ID:', organizationId);
                return organizationId;
            } else {
                console.error('No organizations found.');
                return null;
            }
        } catch (error: any) {
            console.error('Error fetching organizations:', error.response?.data || error.message);
            return null;
        }
    }

    /**
     * Not being used
     * @deprecated
     * @param params
     */
    static async linkedinPublishPost(params: { text: string, link: string}) {
        const ORGANIZATION_ID = '103646751';
        const accessToken = await RealtimeDbUtils.getData({
            collectionName: 'linkedinTokens',
            docId: "accessToken"
        });

        if (!accessToken) {
            return getQuickResponse(false, null, "Not authenticated");
        }

        try {
            const postPayload = {
                "author": `urn:li:organization:${ORGANIZATION_ID}`,
                "commentary": "Sample text Post",
                "visibility": "PUBLIC",
                "distribution": {
                    "feedDistribution": "MAIN_FEED",
                    "targetEntities": [],
                    "thirdPartyDistributionChannels": []
                },
                "lifecycleState": "PUBLISHED",
                "isReshareDisabledByAuthor": false
                /*author: `urn:li:organization:${ORGANIZATION_ID}`,
                commentary: params.text,
                visibility: 'PUBLIC',
                content: {
                    article: {
                        source: params.link,
                        title: params.text,
                        description: params.text,
                    }
                }*/
            };

            console.log('Post Payload:', JSON.stringify(postPayload, null, 2));

            const postResponse = await axios.post('https://api.linkedin.com/rest/posts', postPayload, {
                headers: {
                    Authorization: `Bearer ${accessToken}`,
                    'Content-Type': 'application/json',
                    'X-Restli-Protocol-Version': '2.0.0',
                    "LinkedIn-Version": "202401",
                },
            });

            return getQuickResponse(true, postResponse.data);
        } catch (error: any) {
            console.log('Error Response:', error.response?.data || error.message);
            return getQuickResponse(false, error.response?.data || error.message, "Failed to publish post");
        }
    }

    static async redditPostLink(params: {title: string, url: string, text: string}) {
        const reddit = new Reddit({
            username: WEBAPP_CONFIGS.SOCIAL_NETWORK_API_KEY.redditAPI.username,
            password: WEBAPP_CONFIGS.SOCIAL_NETWORK_API_KEY.redditAPI.password,
            appId: WEBAPP_CONFIGS.SOCIAL_NETWORK_API_KEY.redditAPI.clientId,
            appSecret: WEBAPP_CONFIGS.SOCIAL_NETWORK_API_KEY.redditAPI.clientSecret,
            userAgent: `${WEBAPP_CONFIGS.SITE_NAME}/1.0.0 (${WEBAPP_CONFIGS.WEBSITE_URL})`
        });
        const res = await reddit.post('/api/submit', {
            sr: WEBAPP_CONFIGS.SOCIAL_NETWORK_API_KEY.redditAPI.subReddit,
            kind: 'link',
            resubmit: true,
            title: params.title,
            url: params.url,
        });
        return res;
    }


    static async blueskyPost(postData: {
        text: string;
        images?: {
            url: string;
            alt?: string;
        }[];
        tags?: string[];
    }) {
        try {

            // Validate required fields
            if (!postData.text) {
                return getQuickResponse(false, "Text content is required");
            }

            // Initialize Bluesky agent
            const agent = new BskyAgent({
                service: 'https://bsky.social'
            });

            // Login to Bluesky
            await agent.login({
                identifier: WEBAPP_CONFIGS.SOCIAL_NETWORK_API_KEY.bluesky.username,
                password: WEBAPP_CONFIGS.SOCIAL_NETWORK_API_KEY.bluesky.password
            });

            // Process the text content with RichText to handle mentions and links
            const richText = new RichText({ text: postData.text });
            await richText.detectFacets(agent); // This will automatically detect URLs and mentions

            // Add hashtags if provided
            let finalText = postData.text;
            if (postData.tags && postData.tags.length > 0) {
                const hashtags = postData.tags.map(tag => `#${tag.replace(/\s+/g, '')}`).join(' ');
                finalText = `${finalText}\n\n${hashtags}`;
            }

            // Prepare the post options
            const postOptions: any = {
                text: finalText,
                facets: richText.facets,
            };

            // Handle images if provided
            if (postData.images && postData.images.length > 0) {
                const uploads = await Promise.all(
                    postData.images.slice(0, 4).map(async (image) => {
                        const response = await fetch(image.url);
                        const blob = await response.blob();
                        return agent.uploadBlob(blob, {
                            encoding: 'image/jpeg',
                        });
                    })
                );

                postOptions.embed = {
                    $type: 'app.bsky.embed.images',
                    images: uploads.map((upload: any, index) => ({
                        alt: postData.images![index].alt || '',
                        image: upload.blob,
                    })),
                };
            }

            // Create the post
            const response = await agent.post(postOptions);

            return getQuickResponse(true, `Successfully posted to Bluesky. https://bsky.app/profile/${process.env.BLUESKY_USERNAME}/post/${response.uri.split('/').pop()}`);

        } catch (error) {
            console.error('Error posting to Bluesky:', error);
            return getQuickResponse(false, "Failed to post to Bluesky");
        }
    }

     /**
     * Publishes a message to a Telegram channel
     * @param text - The text content to publish
     */
     static async telegramPost(
        messageText: string
    ): Promise<void> {
        try {
            const telegramConfigs: any = WEBAPP_CONFIGS.SOCIAL_NETWORK_API_KEY["telegram"];

            if (!telegramConfigs) {
                return;
            }

            // Make the API request
            const response = await axios.post(
                `https://api.telegram.org/bot${telegramConfigs.botToken}/sendMessage`,
                {
                    chat_id: telegramConfigs.channel,
                    text: messageText,
                    parse_mode: 'HTML',
                    disable_web_page_preview: false
                }
            );
            console.log(response.data);
        } catch (error: any) {
            console.error(`Failed to post to Telegram: ${error.message}`);
        }
    }

}