import * as functions from 'firebase-functions';
import axios from 'axios';
import { Request, Response } from 'express';
import {RealtimeDbUtils} from "../../../utils/realtime-db.utils";
import {WEBAPP_CONFIGS} from "../../../../configs/webapp.configs";

export const threadsAPIGenerateAuthUrl = functions.https.onRequest((req: Request, res: Response) => {
    const authUrl = `https://www.threads.net/oauth/authorize?client_id=${WEBAPP_CONFIGS.SOCIAL_NETWORK_API_KEY.threadsAPI.clientId}&redirect_uri=${WEBAPP_CONFIGS.SOCIAL_NETWORK_API_KEY.threadsAPI.redirectUri}&scope=threads_basic,threads_content_publish&response_type=code`;
    res.redirect(authUrl);
});

export const threadsAPIHandleAuthCallback = functions.https.onRequest(async (req: Request, res: Response) => {
    const authCode = req.query.code as string;

    if (!authCode) {
        res.status(400).send('Authorization code is required.');
        return;
    }

    try {
        // Exchange authorization code for a short-lived access token
        const tokenResponse = await axios.post('https://graph.threads.net/oauth/access_token', null, {
            params: {
                client_id: WEBAPP_CONFIGS.SOCIAL_NETWORK_API_KEY.threadsAPI.clientId,
                client_secret: WEBAPP_CONFIGS.SOCIAL_NETWORK_API_KEY.threadsAPI.clientSecret,
                redirect_uri: WEBAPP_CONFIGS.SOCIAL_NETWORK_API_KEY.threadsAPI.redirectUri,
                code: authCode,
                grant_type: 'authorization_code'
            },
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            }
        });

        let accessToken = tokenResponse.data.access_token;

        // Retrieve Threads user ID
        const threadsUserIdResponse = await axios.get('https://graph.threads.net/v1.0/me', {
            params: {
                fields: 'id',
                access_token: accessToken
            }
        });

        const threadsUserId = threadsUserIdResponse.data.id;


        // Optionally, exchange short-lived token for a long-lived token if provided
        const longTokenResponse = await axios.get('https://graph.threads.net/access_token', {
            params: {
                grant_type: 'th_exchange_token',
                client_secret: WEBAPP_CONFIGS.SOCIAL_NETWORK_API_KEY.threadsAPI.clientSecret,
                access_token: accessToken
            },
        });

        accessToken = longTokenResponse.data.access_token;

        // Save long-lived access token and Threads user ID to Firestore
        await RealtimeDbUtils.writeData({
            collectionName: "threadsTokens",
            data: {
                token: accessToken,
                userId: threadsUserId
            },
            merge: false,
        });

        res.send('Authorization successful! Long-lived access token and user ID saved.');
    } catch (error: any) {
        console.error('Error exchanging authorization code for access token:', error);
        res.status(500).send(error.response.data);
    }
});
