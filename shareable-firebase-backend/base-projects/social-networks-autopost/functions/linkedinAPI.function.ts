import * as functions from 'firebase-functions';
import axios from 'axios';
import {Request, Response} from "express";
import {RealtimeDbUtils} from "../../../utils/realtime-db.utils";
import {PRODUCTION_GCLOUD_PROJECT} from "../../../../configs/webapp.configs";

// Replace with your LinkedIn app's credentials
const CLIENT_ID = '77lys40p1z309r';
const CLIENT_SECRET = 'U3qtyoxv0iNv8Oep';
const ORGANIZATION_ID = '103646751';
const REDIRECT_URI = `https://us-central1-${PRODUCTION_GCLOUD_PROJECT}.cloudfunctions.net/linkedinAPIHandleAuthCallback`;


export const linkedinAPIGenerateAuthUrl = functions.https.onRequest((req: Request, res: Response) => {
    const authUrl = `https://www.linkedin.com/oauth/v2/authorization?response_type=code&client_id=${CLIENT_ID}&redirect_uri=${REDIRECT_URI}&scope=w_member_social`;
    res.redirect(authUrl);
});

export const linkedinAPIHandleAuthCallback = functions.https.onRequest(async (req: Request, res: Response) => {
    const code = req.query.code;

    try {
        const response = await axios.post('https://www.linkedin.com/oauth/v2/accessToken', null, {
            params: {
                grant_type: 'authorization_code',
                code: code,
                redirect_uri: REDIRECT_URI,
                client_id: CLIENT_ID,
                client_secret: CLIENT_SECRET,
            },
        });

        let accessToken = response.data.access_token;
        await RealtimeDbUtils.writeData({
            collectionName: 'linkedinTokens',
            data: {
                accessToken: accessToken,
            },
            merge: false,
        });
        res.send('Authentication successful! You can now publish posts.');
    } catch (error) {
        console.error(error);
        res.status(500).send('Authentication failed');
    }
});

export const linkedinAPIPublish = functions.https.onRequest(async (req: Request, res: Response) => {
    const accessToken = await RealtimeDbUtils.getData({
        collectionName: 'linkedinTokens',
        docId: "accessToken"
    });

    if (!accessToken) {
        res.status(401).send('Not authenticated');
        return;
    }

    const { text, link } = req.body;

    try {
        const postResponse = await axios.post(`https://api.linkedin.com/v2/ugcPosts`, {
            author: `urn:li:organization:${ORGANIZATION_ID}`,
            lifecycleState: 'PUBLISHED',
            specificContent: {
                'com.linkedin.ugc.ShareContent': {
                    shareCommentary: {
                        text: text,
                    },
                    shareMediaCategory: 'ARTICLE',
                    media: [
                        {
                            status: 'READY',
                            description: {
                                text: text,
                            },
                            originalUrl: link,
                            title: {
                                text: text,
                            },
                        },
                    ],
                },
            },
            visibility: {
                'com.linkedin.ugc.MemberNetworkVisibility': 'PUBLIC',
            },
        }, {
            headers: {
                Authorization: `Bearer ${accessToken}`,
                'Content-Type': 'application/json',
                'x-li-format': 'json',
            },
        });

        res.send(postResponse.data);
        return;
    } catch (error) {
        console.error(error);
        res.status(500).send('Failed to publish post');
        return;
    }
});
