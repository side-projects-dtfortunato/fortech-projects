import {BackendSharedUtils} from "../../utils/backend-shared-utils";
import {ArticleImportedModel} from "./data/model/ArticleImported.model";
import {AIGeminiUtils} from "../../utils/aigemini.utils";
import {BackendRouteUtils} from "../../utils/backend-route.utils";
import {OpenaiUtils} from "../../utils/openai/openai.utils";
import {WEBAPP_CONFIGS} from "../../../configs/webapp.configs";
import { createHash } from 'crypto';


export class SharedArticlesDigestUtils {

    static async fetchGoogleNews(params: {
        searchTerm: string,
        searchType: "TOPIC" | "SEARCH",
        timeFrame: "" | "2h" | "4h" | "6h" | "12h" | "24h" | "7d",
        customLangGeoParams?: string,
        domainsBlackList?: string[]}): Promise<any> {

        const langGeoParams = params.customLangGeoParams ? params.customLangGeoParams : "hl=en-US&gl=US&ceid=US:en";
        const query = BackendSharedUtils.replaceAll(params.searchTerm, " ", "+");
        let domainsBlackListStr: string = "";
        if (params.domainsBlackList && params.domainsBlackList.length > 0) {
            params.domainsBlackList.forEach((domainBlacked) => {
                domainsBlackListStr += `+-site:${domainBlacked}`
            });
        }
        const whenParam: string = params.timeFrame ? "+when:" + params.timeFrame : "";
        let fetchUrl: string = `https://news.google.com/rss/search?${langGeoParams}&q=${query}${whenParam}${domainsBlackListStr}`;
        if (params.searchType === "TOPIC") {
            fetchUrl = `https://news.google.com/rss/topics/${params.searchTerm}?${langGeoParams}`;
        }
        console.log("Fetching: ", fetchUrl);
        const dataFetched: any = await BackendSharedUtils.fetchAndParseXML(fetchUrl, true);
        console.log("dataFetched: ", dataFetched);

        let dataItems: any[] | any | undefined = dataFetched.rss.channel.item;
        if (dataItems && !Array.isArray(dataItems)) {
            dataItems = [dataItems];
        }

        let listParsedArticles: ArticleImportedModel[] = [];

        if (dataItems) {

            for (let rssItem of dataItems) {
                let url: string = this.getPrettyUrl(rssItem["link"]);
                url = url.replace("rss/articles", "read");
                // let url: string = await this.getFinalUrl(rssItem["link"]);
                url = BackendSharedUtils.removeQueryParams(url);
                const pubDate: number = new Date(rssItem.pubDate).valueOf();
                const originalLinkId: string = this.generateArticleId(url);

                listParsedArticles.push({
                    uid: BackendSharedUtils.string_to_slug(rssItem.title.trim()) + "-" + pubDate,
                    originalLinkId: originalLinkId,
                    originalLink: url,
                    title: rssItem.title.trim(),
                    description: "",
                    publishedDate: pubDate,
                    imageUrl: rssItem["media:content"] ? rssItem["media:content"].url : undefined,
                    source: rssItem["source"]["_"],
                    createdAt: Date.now(),
                    updatedAt: Date.now(),
                    aiGeneratedContent: {
                        relevancy: 8,
                    },
                    upvotes: {},
                    stats: {
                        commentsCounter: 0,
                        viewsCounter: 0,
                    },
                } as ArticleImportedModel);
            }
        }
        return listParsedArticles;

    }

    static generateArticleId(url: string): string {
        // Use SHA-256 to create a hash of the URL
        const hash = createHash('sha256').update(url).digest('hex');

        // Return the first 20 characters of the hash
        // This should be unique enough for most purposes while staying well under Firebase's key length limit
        return hash.substring(0, 20);
    }

    /**
     * Copy from: https://github.com/lewisdonovan/google-news-scraper/blob/master/getPrettyUrl.js
     * @param url
     */
    static getPrettyUrl(url: string) {
        const base64Pattern = /articles\/([A-Za-z0-9+_\-\/=]+)/;
        const match = url.match(base64Pattern);

        if (match && match[1]) {
            const base64EncodedUrl = match[1].replace(/-/g, "+").replace(/_/g, "/");
            try {
                let decodedUrl = Buffer.from(base64EncodedUrl, "base64").toString("ascii");

                // Remove any trailing "R" if it's the last character
                decodedUrl = decodedUrl.replace(/R/, "");

                // Remove non-ASCII characters and split by potential delimiters
                const parts = decodedUrl.split(/[^\x20-\x7E]+/).filter(Boolean);

                // Regular expression to validate and extract URLs
                const urlPattern = /(https?:\/\/[^\s]+)/;
                let cleanedUrl = "";

                // Iterate over parts to find the first valid URL
                for (let part of parts) {
                    const urlMatch = part.match(urlPattern);
                    if (urlMatch && urlMatch[1]) {
                        cleanedUrl = urlMatch[1];
                        break; // Stop at the first match
                    }
                }

                if (cleanedUrl) {
                    // Log the cleaned URL in a well-formatted JSON
                    /*const output = {
                        originalUrl: url,
                        cleanedUrl: cleanedUrl
                    };*/

                    // console.log(JSON.stringify(output, null, 2));
                    return cleanedUrl;
                } else {
                    return url;
                }
            } catch (error: any) {
                console.error(
                    "Error decoding Base64 string:",
                    base64EncodedUrl,
                    "Original URL:",
                    url,
                    "Error:",
                    error.message,
                );
                return url;
            }
        } else {
            console.error("No Base64 segment found in the URL. Original URL:", url);
            return url;
        }
    }

    /*static async getFinalUrl(url: string): Promise<string> {
        let browser: any;
        try {
            browser = await puppeteer.launch({
                headless: true,
                args: ['--no-sandbox', '--disable-setuid-sandbox']
            });
            const page = await browser.newPage();

            // Set a reasonable timeout
            await page.setDefaultNavigationTimeout(15000);

            // Navigate to the URL
            await page.goto(url, { waitUntil: 'networkidle0' });

            // Check if we're on the consent page
            if (page.url().includes('consent.google.com')) {
                console.log('Detected consent page, attempting to proceed...');

                // Wait for the 'Agree' button to be available
                await page.waitForSelector('form[action^="https://consent.google.com"]', { timeout: 4000 });

                // Click the 'Agree' button
                await page.evaluate(() => {
                    const form = document.querySelector('form[action^="https://consent.google.com"]');
                    if (form instanceof HTMLFormElement) {
                        form.submit();
                    }
                });

                // Wait for navigation after consent
                await page.waitForNavigation({ waitUntil: 'networkidle0' });
            }

            // Wait for any additional redirects to complete
            await page.waitForNavigation({ waitUntil: 'networkidle0' }).catch(() => {});

            // Get the final URL
            let finalUrl = page.url();

            // If we're still on a Google domain, try to extract the actual news URL
            if (finalUrl.includes('news.google.com')) {
                const actualUrl = await page.evaluate(() => {
                    const link = document.querySelector('a[href^="https://news.google.com/articles/"]');
                    return link instanceof HTMLAnchorElement ? link.href : null;
                });
                if (actualUrl) {
                    finalUrl = actualUrl;
                }
            }

            return finalUrl;
        } catch (error) {
            console.error('Error getting final URL:', error);
            return url;
        } finally {
            if (browser) {
                await browser.close();
            }
        }
    }*/

    /*static async getFinalUrlAndHtml(url: string): Promise<{ finalUrl: string, html: string | null }> {
        let browser: any = null;
        try {
            browser = await puppeteer.launch({
                headless: true,
                args: ['--no-sandbox', '--disable-setuid-sandbox']
            });
            const page = await browser.newPage();

            // Set a reasonable timeout
            await page.setDefaultNavigationTimeout(10000);

            // Navigate to the URL
            await page.goto(url, { waitUntil: 'networkidle0' });

            // Check if we're on the consent page
            if (page.url().includes('consent.google.com')) {
                console.log('Detected consent page, attempting to proceed...');

                // Wait for the 'Agree' button to be available
                await page.waitForSelector('form[action^="https://consent.google.com"]', { timeout: 5000 });

                // Click the 'Agree' button
                await page.evaluate(() => {
                    const form = document.querySelector('form[action^="https://consent.google.com"]');
                    if (form instanceof HTMLFormElement) {
                        form.submit();
                    }
                });

                // Wait for navigation after consent
                await page.waitForNavigation({ waitUntil: 'networkidle0' });
            }

            // Wait for any additional redirects to complete
            await page.waitForNavigation({ waitUntil: 'networkidle0' }).catch(() => {});

            // Get the final URL
            let finalUrl = page.url();

            // If we're still on a Google domain, try to extract the actual news URL
            if (finalUrl.includes('news.google.com')) {
                const actualUrl = await page.evaluate(() => {
                    const link = document.querySelector('a[href^="https://news.google.com/articles/"]');
                    return link instanceof HTMLAnchorElement ? link.href : null;
                });
                if (actualUrl) {
                    finalUrl = actualUrl;
                    // Navigate to the actual URL
                    await page.goto(actualUrl, { waitUntil: 'networkidle0' });
                }
            }

            // Get the HTML content of the final page
            const html = await page.content();

            return { finalUrl, html };
        } catch (error) {
            console.error('Error getting final URL and HTML:', error);
            return { finalUrl: url, html: null };
        } finally {
            if (browser) {
                await browser.close();
            }
        }
    }*/


    static async generateAIContent(articleData: ArticleImportedModel, params: {topic: string, categoryList?: string, extraJsonParams?: string, otherArticles?: string[]}): Promise<any> {

        // Json content
        let jsonContentObj: any = {
            title: articleData.title,
            description: articleData.description,
            articleDetails: articleData.fullContent,
        };
        if (params.otherArticles) {
            jsonContentObj.otherArticlesTitles = params.otherArticles;
        }
        const jsonContent: string = JSON.stringify(jsonContentObj);

        let promptBuild: string = `You are an assistant that reads an article about ${params.topic} from a JSON string, translate to english and summarizes the content of the article. Respond in the following JSON format (valid json format, without any prefix or suffix, just the JSON object): 
        { 
            title: string, // rewrite and improve the title to be more clickable, focusing on capturing interest and curiosity 
            content: string, // rewrite the article content as if it was originally written by our website ${WEBAPP_CONFIGS.SITE_NAME} styled with markdown (use headings, bold the most relevant parts and keywords and add relevant images, links, iframes (as an html element) or tables (as a html elements) from the original content if relevant to the article, and do not add any example or fake image or content to the generated article content) and make it more summarized while keeping the most relevant and interesting parts (don't add any reference to the original source and any other news), and don't add any placeholder this should be a final version content 
            category: ${params.categoryList ? params.categoryList : "string"}, // select a unique concise and accurate category for the article 
            highlightsBullets: string[], // tl;dr list of key points of the article, bold the most relevant parts and keywords in each key point (do not add any prefix bullet to the strings) 
            socialNetworkPost: string, // create an engaging social post to publish this article, including relevant hashtags in keywords (do not add any CTA or placeholder for a link to the article) 
            tags: string[], // provide 5 relevant tags without hashtag and spaces 
            isContentValidAndRelevant: boolean, // true if the article content is not advertisement to promote external products or services, and is relevant (covers key points and main ideas), detailed (provides sufficient information), and interesting (engages the reader and holds their attention)
            ${params.extraJsonParams ? params.extraJsonParams : ""}
            ${params.otherArticles ? "hasSimilarArticle: boolean, // if there's already a similar article on the list of otherArticlesTitles " : ""}
        }\n`;
        promptBuild += `JSON Object with the article original content:\n${jsonContent}\n\n**TL;DR:**`
        // Old isContentValid content prompt: isContentValidAndRelevant

        try {
            const aiGeneratedContent: any = BackendSharedUtils.extractJsonObject(await AIGeminiUtils.generateAIContent(promptBuild));
            if (aiGeneratedContent && aiGeneratedContent.highlightsBullets) {
                aiGeneratedContent.highlightsBullets = aiGeneratedContent.highlightsBullets.map((bullet: string) => bullet.replace(/^- |^\* /, ''));
            }
            if (aiGeneratedContent.tags) {
                aiGeneratedContent.tags = aiGeneratedContent.tags.map((tag: string) => tag.replace(/[#\s]/g, "").toLowerCase());
            }

            // Update Article Data
            articleData.aiGeneratedContent.tags = aiGeneratedContent?.tags;
            articleData.aiGeneratedContent.summary = aiGeneratedContent?.content;
            articleData.aiGeneratedContent.highlightsBullets = aiGeneratedContent?.highlightsBullets;
            articleData.aiGeneratedContent.socialNetworkPost = aiGeneratedContent?.socialNetworkPost;
            articleData.aiGeneratedContent.category = aiGeneratedContent?.category;
            articleData.isArticleValid = aiGeneratedContent?.isContentValidAndRelevant;
            if (aiGeneratedContent.hasSimilarArticles === true) {
                articleData.isArticleValid = false;
            }
            articleData.title = aiGeneratedContent?.title ? aiGeneratedContent?.title! : articleData.title;
            articleData.uid = BackendSharedUtils.string_to_slug(articleData.title);

            return aiGeneratedContent;
        } catch (e) {
            return undefined;
        }
    }


    static async generateAIContentOpenAI(articleData: ArticleImportedModel, params: {topic: string, categoryList?: string, extraJsonParams?: string, otherArticles?: string[]}): Promise<any> {

        // Json content
        let jsonContentObj: any = {
            title: articleData.title,
            description: articleData.description,
            articleDetails: articleData.fullContent,
        };
        if (params.otherArticles) {
            jsonContentObj.otherArticlesTitles = params.otherArticles;
        }
        const jsonContent: string = JSON.stringify(jsonContentObj);

        let assistantPrompt: string = `You are an assistant that reads an article about ${params.topic} from a JSON string, translate to english and summarizes the content of the article. Respond in the following JSON format (valid json format, without any prefix or suffix, just the JSON object): 
        { 
            title: string, // rewrite and improve the title to be more clickable, focusing on capturing interest and curiosity 
            content: string, // rewrite the article content as it was written by our website ${WEBAPP_CONFIGS.WEBSITE_URL} styled with markdown (don't add any HTML element) (use headings, but not a title, bold the most relevant parts and keywords, images and iframes (if exists) and don't add a conclusion headline)  
            category: ${params.categoryList ? params.categoryList : "string"}, // select a unique concise and accurate category for the article 
            highlightsBullets: string[], // tl;dr list of key points of the article, bold the most relevant parts and keywords in each key point (do not add any prefix bullet to the strings) 
            socialNetworkPost: string, // create an engaging social post to publish this article, including relevant hashtags in keywords (do not add any CTA or placeholder for a link to the article) 
            tags: string[], // provide 5 relevant tags without hashtag and spaces 
            isContentValidAndRelevant: boolean, // true if the article content is not advertisement to promote external products or services, and is relevant to the audience, is detailed and constructive (provides sufficient information), and is interesting (engages the reader and holds their attention)
            ${params.extraJsonParams ? params.extraJsonParams : ""}
            ${params.otherArticles ? "hasSimilarArticle: boolean, // true if there's a highly similar or duplicated article story on the list of otherArticlesTitles (this is used to avoid to publish duplicated or too similar stories) " : ""}
        }\n`;
        let userPrompt = `JSON Object with the article original content:\n${jsonContent}\n\n**TL;DR:**`
        // Old isContentValid content prompt: isContentValidAndRelevant

        try {
            const aiGeneratedContent: any = BackendSharedUtils.extractJsonObject(await OpenaiUtils.generateJsonContent(assistantPrompt, userPrompt));

            console.log("aiGeneratedContent:", aiGeneratedContent);
            if (aiGeneratedContent && aiGeneratedContent.highlightsBullets) {
                aiGeneratedContent.highlightsBullets = aiGeneratedContent.highlightsBullets.map((bullet: string) => bullet.replace(/^- |^\* /, ''));
            }
            if (aiGeneratedContent.tags) {
                aiGeneratedContent.tags = aiGeneratedContent.tags.map((tag: string) => tag.replace(/[#\s]/g, "").toLowerCase());
            }

            // Update Article Data
            articleData.aiGeneratedContent.tags = aiGeneratedContent?.tags;
            articleData.aiGeneratedContent.summary = aiGeneratedContent?.content;
            articleData.aiGeneratedContent.highlightsBullets = aiGeneratedContent?.highlightsBullets;
            articleData.aiGeneratedContent.socialNetworkPost = aiGeneratedContent?.socialNetworkPost;
            articleData.aiGeneratedContent.category = aiGeneratedContent?.category;
            articleData.isArticleValid = aiGeneratedContent?.isContentValidAndRelevant;
            if (aiGeneratedContent.hasSimilarArticle === true) {
                articleData.isArticleValid = false;
            }
            articleData.title = aiGeneratedContent?.title ? aiGeneratedContent?.title! : articleData.title;
            articleData.uid = BackendSharedUtils.string_to_slug(articleData.title);

            return aiGeneratedContent;
        } catch (e) {
            return undefined;
        }
    }

    static async rankListArticles(listArticles: ArticleImportedModel[], contentTopic: string, otherArticlesTitles?: string[]): Promise<{listArticles: {articleUid: string, title: string, articleRelevancy: number, hasSimilarArticles: boolean}[]} | any | undefined> {
        const jsonContent: string = JSON.stringify({
            listArticles: listArticles.map((articleData) => {
                return {
                    articleUid: articleData.originalLinkId,
                    title: articleData.title,
                    description: articleData.description,
                    source: articleData.source,
                }
            }),
            otherArticlesTitles: otherArticlesTitles ? otherArticlesTitles : [],
        });

        let promptBuild: string = `Sort this list of the articles based on their relevancy and interest to the ${contentTopic} and respond in the next JSON Scheme: {listArticles: {articleUid: string, title: string, articleRelevancy: number // relevancy between 0 and 10 (10 is the most relevant article) of the article for Bitcoin investors, marketSentiment: \"bullish\"|\"bearish\"|\"neutral\", hasSimilarArticles: boolean // if some similar article already exists in the list of the param otherArticlesTitles}[]}`;
        promptBuild += ` Here's  the list of articles:\n${jsonContent}`

        try {
            let aiResponse: string = await AIGeminiUtils.generateAIContent(promptBuild);
            console.log("rankListArticles.aiResponse: ", aiResponse);
            // cleanup JSON
            return BackendSharedUtils.extractJsonObject(aiResponse);

            /*const aiGeneratedContent: any = JSON.parse(aiResponse);
            return aiGeneratedContent;*/
        } catch (e) {
            return undefined;
        }
    }


    static generateSocialNetworkPost(articleData: ArticleImportedModel) {
        let postMessage: string = "";

        if (articleData.aiGeneratedContent.socialNetworkPost) {
            postMessage = articleData.aiGeneratedContent.socialNetworkPost
        } else {
            postMessage = articleData.title + "\n\n";
            articleData.aiGeneratedContent.tags?.forEach((tag, index) => {
                if (index > 0) {
                    postMessage += " ";
                }
                postMessage += "#" + BackendSharedUtils.replaceAll(tag, " ", "");
            });
        }
        postMessage += "\n\nRead more: " + BackendRouteUtils.getArticleDetails(articleData.uid!);
        postMessage = BackendSharedUtils.replaceAll(postMessage, "*", "");
        return postMessage;
    }

    static removeDuplicateArticles(articles: ArticleImportedModel[]): ArticleImportedModel[] {
        return articles.filter((article, index, self) =>
            index === self.findIndex(t => t.originalLink === article.originalLink)
        );
    }


    /*static async getFinalUrlAndHtml(url: string): Promise<{ finalUrl: string, html: string | null }> {
        let browser: Browser | null = null;
        try {
            browser = await chromium.launch({
                headless: true,
                args: ['--no-sandbox', '--disable-setuid-sandbox']
            });
            const context = await browser.newContext();
            const page = await context.newPage();

            // Set a reasonable timeout
            page.setDefaultTimeout(15000);

            // Navigate to the URL
            await page.goto(url, { waitUntil: 'networkidle' });

            // Check if we're on the consent page
            if (page.url().includes('consent.google.com')) {
                console.log('Detected consent page, attempting to proceed...');

                // Wait for the 'Agree' button to be available
                await page.waitForSelector('form[action^="https://consent.google.com"]', { timeout: 6000 });

                // Click the 'Agree' button
                await page.evaluate(() => {
                    const form = document.querySelector('form[action^="https://consent.google.com"]');
                    if (form instanceof HTMLFormElement) {
                        form.submit();
                    }
                });

                // Wait for navigation after consent
                await page.waitForNavigation({ waitUntil: 'networkidle' });
            }

            // Wait for any additional redirects to complete
            await page.waitForLoadState('networkidle');

            // Get the final URL
            let finalUrl = page.url();

            // If we're still on a Google domain, try to extract the actual news URL
            if (finalUrl.includes('news.google.com')) {
                const actualUrl = await page.evaluate(() => {
                    const link = document.querySelector('a[href^="https://news.google.com/articles/"]');
                    return link instanceof HTMLAnchorElement ? link.href : null;
                });
                if (actualUrl) {
                    finalUrl = actualUrl;
                    // Navigate to the actual URL
                    await page.goto(actualUrl, { waitUntil: 'networkidle' });
                }
            }

            // Get the HTML content of the final page
            const html = await page.content();

            return { finalUrl, html };
        } catch (error) {
            console.error('Error getting final URL and HTML:', error);
            return { finalUrl: url, html: null };
        } finally {
            if (browser) {
                await browser.close();
            }
        }
    }*/
}