import {ArticleImportedModel} from "./ArticleImported.model";
import {BaseModel} from "../../../../model-data/base.model";

export interface DailyPublishedArticlesModel extends BaseModel{
    publishedDayId: number;
    listArticles: {
        [articleId: string]: ArticleImportedModel;
    }
    summary: string;
}