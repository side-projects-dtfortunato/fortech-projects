import {BaseModel} from "../../../../model-data/base.model";
import {SharedPostStatsModel} from "../../../../model-data/frontend-model-data/shared-post-stats.model";

export enum ArticleInterestTypes {VERY_LOW = "VERY_LOW", LOW = "LOW", NEUTRAL  = "NEUTRAL", HIGH = "HIGH", VERY_HIGH = "VERY_HIGH"};

export interface ArticleImportedModel extends BaseModel {
    originalLinkId: string;
    title: string;
    originalTitle: string;
    description: string;
    source: string;
    imageUrl?: string;
    originalLink: string;
    publishedDate: number;
    fullContent?: string; // Extracted content from the article
    aiGeneratedContent: {
        summary?: string;
        highlightsBullets?: string[];
        relevancy?: number;
        tags?: string[];
        hasSimilarArticles?: boolean;
        socialNetworkPost?: string;
        category?: string;
    };
    publishedDayId?: number;
    customHTMLContentElement?: string;
    upvotes: {
        [userId: string]: number,
    }; // User Ids: timestamp
    stats?: SharedPostStatsModel;
    isArticleValid?: boolean;
}

export interface ArticleGeneratedContent {
    summary: string;
    bullets: string[];
    relevancy: number;
    tags: string[];
}