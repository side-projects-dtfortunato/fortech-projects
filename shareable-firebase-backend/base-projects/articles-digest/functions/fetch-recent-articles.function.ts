import * as functions from "firebase-functions";
import * as admin from "firebase-admin";
import {ArticleImportedModel} from "../data/model/ArticleImported.model";
import {getQuickResponse} from "../../../model-data/api-response.model";
import {BackendSharedUtils} from "../../../utils/backend-shared-utils";
import {RealtimeDbUtils} from "../../../utils/realtime-db.utils";
import {
    SharedFirestoreCollectionDB,
    SharedRealtimeCollectionDB
} from "../../../model-data/shared-firestore-collections";
import {UrlExtractDataUtils} from "../../../utils/url-extract-data.utils";
import {ArticlesDigestUtils} from "../../../../custom-project/utils/articles-digest.utils";
import {DailyPublishedArticlesModel} from "../data/model/DailyPublishedArticles.model";
import {getDocumentsWithPagination} from "../../../utils/utils";
import {SharedArticlesDigestUtils} from "../shared-articles-digest.utils";

const MIN_ARTICLE_RELEVANCE: number = 7;

export const fetchRecentArticlesFunction = functions.runWith({
    memory: "1GB",
    timeoutSeconds: 500,
}).https.onRequest(
    async (req, resp) => {

        let recentArticles: ArticleImportedModel[] = await ArticlesDigestUtils.getMostRecentArticles();
        recentArticles = recentArticles.sort((a1, a2) => a2.publishedDate - a1.publishedDate);

        // Remove duplicated articles
        recentArticles = SharedArticlesDigestUtils.removeDuplicateArticles(recentArticles);

        let todayArticles: DailyPublishedArticlesModel | undefined = (await getDocumentsWithPagination(SharedFirestoreCollectionDB.ArticlesDigestProject.DailyPublishedArticles,1, "publishedDayId"))[0];


        // Get Temp Recent published Articles
        const tempRecentPublishedArticles: { [uid: string]: ArticleImportedModel } | undefined = await RealtimeDbUtils.getData({collectionName: SharedRealtimeCollectionDB.ArticlesDigestProject.TempRecentPublishedArticles});

        if (tempRecentPublishedArticles) {
            // Articles Published less than 2 hrs ago only
            recentArticles = recentArticles
                .filter((articleItem) => !tempRecentPublishedArticles[articleItem.originalLinkId!]);
        }

        const publishedDayId: number = BackendSharedUtils.getTodayDay();
        if (recentArticles && recentArticles.length > 0) {
            // recentArticles = recentArticles.sort((a1, a2) => a2.publishedDate - a1.publishedDate);
            recentArticles = recentArticles.slice(0, 2);

            // Extract relevancy of each article
            // recentArticles = await filterArticlesByRelevancy(recentArticles);

            for (let articleItem of recentArticles) {
                articleItem.publishedDayId = publishedDayId;

                // Get the fullcontent
                if (!articleItem.fullContent) {
                    try {
                        const extractResponse = await UrlExtractDataUtils.extractArticleContent(articleItem.originalLink, articleItem.customHTMLContentElement);
                        console.log(extractResponse);
                        if (extractResponse) {
                            articleItem.fullContent = extractResponse.content;
                            articleItem.description = extractResponse.description ? extractResponse.description : articleItem.description;
                            articleItem.imageUrl = extractResponse.image ? extractResponse.image : "";
                            articleItem.originalLink = extractResponse.finalUrl;
                        } else {
                            articleItem.isArticleValid = false;
                            break;
                        }

                    } catch (e) {
                        console.error(e);
                    }
                }

                if (articleItem.fullContent && articleItem.fullContent.length > 0 && BackendSharedUtils.isValidHttpUrl(articleItem.imageUrl) && await BackendSharedUtils.isImageLinkValid(articleItem.imageUrl!)) {
                    // Generate AI Content
                    await ArticlesDigestUtils.generateAIContent(articleItem, (todayArticles ? Object.values(todayArticles.listArticles).map((articleItem) => articleItem.title) : undefined));

                    // Generate random views
                    articleItem.stats = {
                        commentsCounter: 0,
                        viewsCounter: BackendSharedUtils.getRandomNumber(articleItem.aiGeneratedContent.relevancy! * 10, articleItem.aiGeneratedContent.relevancy! * 12),
                    }

                } else {
                    articleItem.isArticleValid = false;
                }

            }

        }


        // Update our Database
        for (let articleItem of recentArticles.filter((articleItem) => articleItem.aiGeneratedContent.relevancy && articleItem.aiGeneratedContent.relevancy > 0)) {
            // Clean up full content
            articleItem.fullContent = "";
            if (articleItem.aiGeneratedContent && articleItem.aiGeneratedContent.summary && articleItem.aiGeneratedContent.tags && articleItem.aiGeneratedContent.highlightsBullets
                && articleItem.aiGeneratedContent.relevancy && articleItem.aiGeneratedContent.relevancy >= MIN_ARTICLE_RELEVANCE && articleItem.isArticleValid) {

                await admin.firestore().collection(SharedFirestoreCollectionDB.ArticlesDigestProject.ArticlePublished).doc(articleItem.uid!).create(articleItem);
                await admin.firestore().collection(SharedFirestoreCollectionDB.ArticlesDigestProject.DailyPublishedArticles).doc(publishedDayId.toString()).set({
                    summary: "",
                    listArticles: {
                        [articleItem.uid!]: {
                            ...articleItem,
                            fullContent: "",
                        }
                    },
                    publishedDayId: publishedDayId,
                } as DailyPublishedArticlesModel, {merge: true});
            }
            await RealtimeDbUtils.writeData({
                collectionName: SharedRealtimeCollectionDB.ArticlesDigestProject.TempRecentPublishedArticles,
                docId: articleItem.originalLinkId!,
                data: {
                    ...articleItem,
                    imageUrl: "",
                    aiGeneratedContent: {
                        relevancy: articleItem.aiGeneratedContent.relevancy,
                    },
                },
                merge: true,
            });
        }


        // Remove Old Temporary Articles from the realtime database
        if (tempRecentPublishedArticles) {
            const tempArticlesToClean: ArticleImportedModel[] = Object
                .values(tempRecentPublishedArticles!)
                .filter((articleItem) => !BackendSharedUtils.isLessThanHoursAgo(articleItem.createdAt!, 24*30)); // clean 5 days old articles
            if (tempArticlesToClean && tempArticlesToClean.length > 0) {
                for (let articleItem of tempArticlesToClean) {
                    await RealtimeDbUtils.deleteData({
                        collectionName: SharedRealtimeCollectionDB.ArticlesDigestProject.TempRecentPublishedArticles,
                        docId: articleItem.originalLinkId!,
                    });
                }
            }
        }

        resp.send(getQuickResponse(true, {recentArticles: recentArticles}));
    });


export async function filterArticlesByRelevancy(recentArticles: ArticleImportedModel[]) {
    // Get today publishes
    const listRecentDailyPublishes: DailyPublishedArticlesModel[] = await getDocumentsWithPagination(SharedFirestoreCollectionDB.ArticlesDigestProject.DailyPublishedArticles, 1, "publishedDayId");
    const listTodayArticles: DailyPublishedArticlesModel | undefined = listRecentDailyPublishes && listRecentDailyPublishes.length > 0 ? listRecentDailyPublishes[0] : undefined;
    const otherArticlesTitles: string[] = listTodayArticles && listTodayArticles!.listArticles ? Object.values(listTodayArticles?.listArticles!).map((item: ArticleImportedModel) => item.title) : [];


    // Extract relevancy of each article
    let listRelevancyArticles: {listArticles: {articleUid: string, title: string, articleRelevancy: number, isRepeated: boolean}[]} = await ArticlesDigestUtils.rankListArticles(recentArticles, otherArticlesTitles);

    // Let's update each article based in its relevancy
    recentArticles.forEach((articleData) => {
        const articleRank: any = listRelevancyArticles.listArticles.find((article) => article.articleUid === articleData.originalLinkId);
        if (articleRank) {
            articleData.aiGeneratedContent.relevancy = articleRank.articleRelevancy;
            articleData.aiGeneratedContent.hasSimilarArticles = articleRank.hasSimilarArticles;
            (articleData.aiGeneratedContent as any).marketSentiment = articleRank.marketSentiment;
        }
    });

    return recentArticles.filter((articleItem) => articleItem.aiGeneratedContent.relevancy && articleItem.aiGeneratedContent.relevancy >= MIN_ARTICLE_RELEVANCE && articleItem.aiGeneratedContent.hasSimilarArticles !== true);
}