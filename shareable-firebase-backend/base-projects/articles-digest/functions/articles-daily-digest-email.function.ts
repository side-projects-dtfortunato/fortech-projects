import * as functions from "firebase-functions";
import {WEBAPP_CONFIGS} from "../../../../configs/webapp.configs";
import {EmailTemplateDigestUtils, ListItemsContainer} from "../../../utils/email-utils/email-template-digest-v2.utils";
import {BackendRouteUtils} from "../../../utils/backend-route.utils";
import {ArticleImportedModel} from "../data/model/ArticleImported.model";
import {getDocumentsWithPagination} from "../../../utils/utils";
import {SharedFirestoreCollectionDB, SharedFirestoreDocsDB} from "../../../model-data/shared-firestore-collections";
import {DailyPublishedArticlesModel} from "../data/model/DailyPublishedArticles.model";
import {BackendSharedUtils} from "../../../utils/backend-shared-utils";
import {SharedNewsletterUserModel} from "../../../model-data/frontend-model-data/shared-newsletter-user.model";
import {RealtimeDbUtils} from "../../../utils/realtime-db.utils";
import {getQuickResponse} from "../../../model-data/api-response.model";
import {EmailDestinationModel} from "../../../utils/email-utils/email-sender-utils";
import {EmailBulkDeliveryModel} from "../../../model-data/email-builk-delivery.model";

export const generateEmailDailyDigest = functions.https.onRequest(async (request,
                                                                         response) => {

    // Get subscribed users
    let subscribedUsers: { [userId: string]: SharedNewsletterUserModel } | undefined = await RealtimeDbUtils.getData({
        collectionName: SharedFirestoreCollectionDB.NewsletterCatalog,
        docId: SharedFirestoreDocsDB.NewsletterCatalog.SubscribedUsers,
    });

    if (!subscribedUsers) {
        response.send(getQuickResponse(false, null, "No subscribers found"));
        return;
    }



    if (subscribedUsers && Object.keys(subscribedUsers).length > 0) {
        // Send email
        const emailContent: {
            html: string,
            emailTitle: string,
        } | undefined = await generateDailyDigestHtml();

        if (!emailContent) {
            response.send(getQuickResponse(false));
            return;
        }

        if (subscribedUsers) {
            let destionationsList: EmailDestinationModel[] = Object.values(subscribedUsers)
                .filter((subscribedUser) => {
                    return subscribedUser.userId && BackendSharedUtils.isValidEmail(subscribedUser.email);
                }).map((subscriptionItem) => {
                    return {
                        uid: subscriptionItem.userId,
                        email: subscriptionItem.email,
                        name: subscriptionItem.name ? subscriptionItem.name : subscriptionItem.email,
                    };
                });

            // Store to later be delivered
            await RealtimeDbUtils.writeData({
                collectionName: SharedFirestoreCollectionDB.RTEmailBulkToDeliver,
                docId: Date.now().toString(),
                data: {
                    bodyHtml: emailContent.html,
                    destinationsEmail: destionationsList,
                    subject: emailContent.emailTitle,
                    sentFrom: {
                        name: WEBAPP_CONFIGS.SITE_NAME + " Daily Digest",
                        email: WEBAPP_CONFIGS.COMPANY_EMAIL,
                    },
                    createdAt: Date.now(),
                } as EmailBulkDeliveryModel,
                merge: false,
            });
            response.send(getQuickResponse(true, null, "Weekly Digest planned to dispatch"));

        } else {
            response.send(getQuickResponse(false, null, "Not enough posts to notify"));
            return;
        }


    } else {
        response.send(getQuickResponse(false, null, "No users to send digest"));
    }

});

export async function generateDailyDigestHtml(): Promise<{
    html: string,
    emailTitle: string,
} | undefined> {
    let listItemsContainer: ListItemsContainer[] = [];

    let listLastPublications: DailyPublishedArticlesModel[] = await getDocumentsWithPagination(SharedFirestoreCollectionDB.ArticlesDigestProject.DailyPublishedArticles, 2, "publishedDayId");

    let emailTitle: string = "";

    if (listLastPublications.length === 0 || listLastPublications[0].publishedDayId !== BackendSharedUtils.getTodayDay()) {
        // Couldn't found any startup
        return undefined;
    }

    // Get Today's Startups
    if (listLastPublications.length > 0) {
        const todayPublishedArticles: ListItemsContainer = {
            title: "Today Trending News",
            listItems: [],
        };
        Object
            .values(listLastPublications[0].listArticles)
            .sort((s1, s2) => s2.stats?.viewsCounter! - s1.stats?.viewsCounter!)
            .forEach((articleItem) => {
                if (todayPublishedArticles.listItems.length < 10) {
                    todayPublishedArticles.listItems.push({
                        title: articleItem.title,
                        subtitle: "👀 " + articleItem.stats?.viewsCounter,
                        summary: BackendSharedUtils.cleanMarkdown(articleItem.aiGeneratedContent.summary!).slice(0, 150) + "...",
                        postedOn: "",
                        readMoreUrl: BackendRouteUtils.getArticleDetails(articleItem.uid!),
                        imageUrl: articleItem.imageUrl
                    });
                }

                if (emailTitle.length < 100) {
                    if (emailTitle.length > 0) {
                        emailTitle += " | ";
                    }
                    emailTitle += articleItem.title;
                }
            });
        listItemsContainer.push(todayPublishedArticles);
    }

    // Get Yesterday Startups
    if (listLastPublications.length > 1) {
        const yesterdayArticles: ListItemsContainer = {
            title: "Yesterday Top News",
            listItems: [],
        };
        Object
            .values(listLastPublications[1].listArticles)
            .sort((s1, s2) => s2.stats?.viewsCounter! - s1.stats?.viewsCounter!)
            .forEach((articleItem: ArticleImportedModel) => {
                if (yesterdayArticles.listItems.length < 10) {
                    yesterdayArticles.listItems.push({
                        title: articleItem.title,
                        subtitle: "👀 " + articleItem.stats?.viewsCounter,
                        summary: BackendSharedUtils.cleanMarkdown(articleItem.aiGeneratedContent.summary!).slice(0, 150) + "...",
                        postedOn: "",
                        readMoreUrl: BackendRouteUtils.getArticleDetails(articleItem.uid!),
                        imageUrl: articleItem.imageUrl
                    });
                }
            });
        listItemsContainer.push(yesterdayArticles);
    }



    const html = EmailTemplateDigestUtils.generateHtmlContent({
        headerTitle: WEBAPP_CONFIGS.SITE_NAME + " - Daily Digest",
        headerSubtitle: WEBAPP_CONFIGS.SITE_TAGLINE,
        siteUrl: WEBAPP_CONFIGS.WEBSITE_URL,
        logoUrl: WEBAPP_CONFIGS.LOGO_URL,
        footer: {
            address: WEBAPP_CONFIGS.COMPANY_ADDRESS,
            email: WEBAPP_CONFIGS.COMPANY_EMAIL,
            unsubscribeLink: WEBAPP_CONFIGS.UNSUBSCRIBE_URL,
        },
        listSocialNetworks: [],
        listItemsContainer: listItemsContainer,
    });

    return {
        html: html,
        emailTitle: emailTitle,
    };
}