import * as functions from "firebase-functions";
import * as admin from "firebase-admin";
import {SharedFirestoreCollectionDB} from "../../../model-data/shared-firestore-collections";
import {Log} from "../../../utils/log";
import {ArticleImportedModel} from "../data/model/ArticleImported.model";
import {BackendSharedUtils} from "../../../utils/backend-shared-utils";
import {ArticlesDigestUtils} from "../../../../custom-project/utils/articles-digest.utils";

export const onArticleImportedChanged = functions
    .firestore
    .document(SharedFirestoreCollectionDB.ArticlesDigestProject.ArticlePublished + '/{articleId}')
    .onWrite(async (data, context) => {
        // Update catalog data
        Log.onWrite(data);
        let articleData: ArticleImportedModel | undefined = data.after.data() ? data.after.data() as ArticleImportedModel : data.before.data() as ArticleImportedModel;
        let hasRemoved: boolean = !data.after.exists;
        let hasCreated: boolean = !data.before.exists;
        console.log("onArticleImportedChanged.articleData: ", articleData);

        // Update Published Day
        if (articleData) {
            articleData.fullContent = "";
            await admin.firestore().collection(SharedFirestoreCollectionDB.ArticlesDigestProject.DailyPublishedArticles)
                .doc(articleData.publishedDayId!.toString()).set({
                    listArticles: {
                        [articleData.uid!]: hasRemoved ? admin.firestore.FieldValue.delete() : articleData,
                    }
                }, {merge: true});

            if (hasCreated && BackendSharedUtils.isProjectProduction()) {
                // Publish on social networks && Push notification
                await ArticlesDigestUtils.publishArticleSocialNetworks(articleData);

                // await OnesignalUtils.sendPushNotification(articleData.title, BackendSharedUtils.replaceAll(articleData.aiGeneratedContent.summary!, "*", ""), BackendRouteUtils.getArticleDetails(articleData.uid!));
            }
        }

    });