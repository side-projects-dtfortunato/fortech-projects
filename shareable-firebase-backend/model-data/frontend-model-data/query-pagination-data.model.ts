

export function initQueryPaginationDataModel<ItemsType>(itemsPerPage: number): QueryPaginationDataModel <ItemsType> {
    return {
        currentPage: 0,
        hasMorePages: true,
        itemsPerPage: itemsPerPage,
        listItems: [],
    };
}

export interface QueryPaginationDataModel <ItemsType> {
    listItems: ItemsType[];
    lastDoc?: any;
    itemsPerPage: number;
    currentPage: number;
    hasMorePages: boolean;
}