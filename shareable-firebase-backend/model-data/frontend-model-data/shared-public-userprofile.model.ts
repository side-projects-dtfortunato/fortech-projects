import {SharedCountryLocationModel} from "./shared-country-location.model";
import CommonUtilsNoDep from "./utils/CommonUtilsNoDep";
import {StorageFileModel} from "../storage-file.model";
import {BaseModel} from "../base.model";

export interface SharedPublicUserprofileModel extends BaseModel {
    username?: string;
    name?: string;
    userPicture?: StorageFileModel;
    publicSubtitle?: string;
    bioDescription?: string;
    location?: SharedCountryLocationModel;
    links?: {
        [index: string]: UserProfileLinkModel
    };
    createdAt?: number;
}

export interface UserShortProfileModel extends BaseModel{
    username?: string,
    name: string,
    pictureUrl?: string,
    subtitleLabel?: string,
    location?: SharedCountryLocationModel,
    email?: string,
}

export interface UserProfileLinkModel {
    label: string,
    link: string,
    iconUrl?: string,
}

export class UserProfileUtils {

    static allLinkValid(links?: {[index: string]: UserProfileLinkModel }) {
        let validLinks: boolean = true;

        if (links) {
            Object.values(links).forEach((linkData) => {
                if (!linkData.label || linkData.label.length === 0 || !CommonUtilsNoDep.isValidHttpUrl(linkData.link)) {
                    validLinks = false;
                }
            });
        }
        return validLinks;
    }

    static generateUserShortProfile(userProfile: SharedPublicUserprofileModel): UserShortProfileModel {
        return {
            uid: userProfile.uid,
            location: userProfile.location,
            name: userProfile.name ? userProfile.name : "",
            pictureUrl: userProfile.userPicture?.fileUrl,
            subtitleLabel: userProfile.publicSubtitle,
            username: userProfile.username,
        };
    }
}