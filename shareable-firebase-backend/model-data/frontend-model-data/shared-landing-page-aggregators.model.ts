import {BasePostDataModel} from "./base-post-data.model";
import {UserShortProfileModel} from "./shared-public-userprofile.model";

export interface SharedLandingPageAggregatorsModel {
    recentPosts: BasePostDataModel[],
    popularPosts: BasePostDataModel[],
    popularTagsPosts: {[tag: string]: {
            rank: number,
            tag: string,
            posts: BasePostDataModel[],
        }},
    popularWriters: {
        [userId: string]: {
            userId: string,
            rank: number,
            userInfos: UserShortProfileModel,
        },
    },
}

export class LandingPageDataUtils {

    static generateLandingPageAggregators(allPosts: BasePostDataModel[]): SharedLandingPageAggregatorsModel {
        let data: SharedLandingPageAggregatorsModel = {
            popularPosts: [],
            popularTagsPosts: {},
            popularWriters: {},
            recentPosts: []
        }

        // Recent posts
        data.recentPosts = allPosts.concat().sort((p1, p2) => {
            return p2.publishedAt - p1.publishedAt;
        });

        // Popular Posts
        data.popularPosts = allPosts.concat().sort((p1, p2) => {
            return p2.rankPts - p1.rankPts;
        });

        // Popular Writers
        allPosts.forEach((post) => {
           if (!Object.keys(data.popularWriters).includes(post.userCreatorId)) {
               data.popularWriters = {
                   ...data.popularWriters,
                   [post.userCreatorId]: {
                       rank: 0,
                       userInfos: {
                           ...post.listUsersInfos[post.userCreatorId],
                           uid: post.userCreatorId,
                       },
                       userId: post.userCreatorId,
                   },
               };
           }

            data.popularWriters[post.userCreatorId].rank += post.rankPts;
        });

        // Popular Tags
        data.popularPosts.forEach((post) => {
            post.tags.forEach((tag) => {
                if (!Object.keys(data.popularTagsPosts).includes(tag)) {
                    // Need to create an new ref for the tag, but check if reached the limit
                    if (Object.keys(data.popularTagsPosts).length > 8) {
                        return;
                    }

                    data.popularTagsPosts = {
                        ...data.popularTagsPosts,
                        [tag]: {
                            rank: 0,
                            tag: tag,
                            posts: [],
                        },
                    };
                }

                data.popularTagsPosts[tag].rank += post.rankPts;
                if (data.popularTagsPosts[tag].posts.length <= 8) {
                    data.popularTagsPosts[tag].posts.push(post);
                }

            });
        });

        return data;
    }
}