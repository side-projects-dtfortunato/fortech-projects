import {PostDetailsBaseModel} from "../base-post-data.model";

export interface PostFormArticleBlocksDetails extends PostDetailsBaseModel {
    editorJSData?: {
        time: number,
        version: string,
        blocks: EditorBlockModel[],
    },

    // listBlocks?: ArticleBlockBase[];
}

export interface EditorBlockModel {
    id: string,
    type: string,
    data: any,
}

export interface ArticleEditorImageFileBlockModel {
    url: string,
    isServerSaved: boolean,
    uid?: string,
}

/*

export enum ArticleBlockType {TEXT_CONTENT ="TEXT_CONTENT", EMBEDDED = "EMBEDDED"}

export interface ArticleBlockBase {
    type: ArticleBlockType;
}

export interface ArticleBlockTextContent extends ArticleBlockBase {
    title?: string;
    titleLink?: string;
    image?: StorageFileModel;
    text: string;
}

export interface ArticleBlockEmbedded extends ArticleBlockBase{
    title?: string
    htmlCode: string;
    text?: string;
}*/