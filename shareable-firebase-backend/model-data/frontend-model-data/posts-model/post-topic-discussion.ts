import {PostDetailsBaseModel} from "../base-post-data.model";

export interface PostTopicDiscussionDetailsModel extends PostDetailsBaseModel{
    bodyText: string,
}