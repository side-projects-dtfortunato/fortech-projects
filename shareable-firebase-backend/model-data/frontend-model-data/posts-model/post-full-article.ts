import {PostDetailsBaseModel} from "../base-post-data.model";

export interface PostFormFullArticleDetails extends PostDetailsBaseModel{
    bodyText: string,
}