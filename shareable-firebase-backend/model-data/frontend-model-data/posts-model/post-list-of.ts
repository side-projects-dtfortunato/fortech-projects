import {PostDetailsBaseModel} from "../base-post-data.model";

export interface PostListOfDetailsModel extends PostDetailsBaseModel{
    bodyText: string,
}