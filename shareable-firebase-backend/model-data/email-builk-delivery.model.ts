import {EmailDestinationModel} from "../utils/email-utils/email-sender-utils";

export interface EmailBulkDeliveryModel {
    destinationsEmail: EmailDestinationModel[],
    bodyHtml: string,
    subject: string,
    sentFrom: { name: string, email: string },
    createdAt?: number,
}