export interface RealtimeDBStatsEventModel {
    collectionName: string,
    docId: string,
    viewsCounter: number | any,
}