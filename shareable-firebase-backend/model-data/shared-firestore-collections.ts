export const SharedFirestoreCollectionDB = {
    // Profile
    CentralUserData: "CentralUserData",
    DataCatalog: "DataCatalog",
    UserPostsCreated: "UserPostsCreated",
    PostsDetails: "PostsDetails",
    PublicUserProfile: "PublicUserProfile",
    UserBookmarks: "UserBookmarks",
    UserFollowers: "UserFollowers",
    UserFollowing: "UserFollowing",
    NewsletterCatalog: "NewsletterCatalog",

    DraftUserPosts: "DraftUserPosts",

    // Realtime
    RTEmailBulkToDeliver: "EmailBulkToDeliver",
    PostCommentRoot: "PostCommentRoot",

    ContactUs: "ContactUs",

    // AI Imported Articles Project
    ArticlesDigestProject: {
        ArticlePublished: "ArticlePublished",
        DailyPublishedArticles: "DailyPublishedArticles",
    },
    CommunityCommentPosts: "CommunityCommentPosts",
    CommunityDailyPublishes: "CommunityDailyPublishes",
    CommunityReportedComments: "CommunityReportedComments",

    // Jobs Discover
    JobsDiscoverProject: {
        SubmitJobWaitingPayment: "SubmitJobWaitingPayment",
        JobDetails: "JobDetails",
        DailyPublishedJobs: "DailyPublishedJobs",
        DigestedJobsIndexRTDB: "DigestedJobsIndexRTDB",
        ImportedJobsListRTDB: "ImportedJobsList", // List of jobs that are waiting to get details
        JobSkillsCounterRTDB: "JobSkillsCounterRTDB",
        CompanyDataRTDB: "CompanyDataRTDB",
    },

    AdminRegisteredUsersCounterRTDB: "registeredUsersCounter"
}

export const SharedFirestoreDocsDB = {
    DataCatalog: {
        FilterRecent: "filter:recent",
        FilterPopular: "filter:popular",
        PrefixPostType: "type:",
        PrefixCategoryType: "category:",
        PrefixTagType: "tag:",
        PopularTags: "PopularTags",
        SearchableItems: "SearchableItems",
    },
    NewsletterCatalog: {
        SubscribedUsers: "SubscribedUsers",
        UnsubscribedUsers: "UnsubscribedUsers",
        DigestSenderUsersQueue: "DigestSenderUsersQueue",
        InvalidEmailUsers: "InvalidEmailUsers",
        DailyUserDigest: "DailyUserDigest",
    },
    RTEmailBulkToDeliver: {
        DailyDigest: "DailyDigest",
    },
}

export const SharedRealtimeCollectionDB = {
    ArticlesDigestProject: {
        TempRecentPublishedArticles: "TempRecentPublishedArticles",
    },
    StatsEventsName: "StatsEventsName",
    CommunityParentCommentsAggregator: "CommunityParentCommentsAggregator",
    CommunityMostRecentPosts: "CommunityMostRecentPosts",
}