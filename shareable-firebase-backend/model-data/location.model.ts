export interface LocationModel {
    cityName?: string;
    countryName?: string;
    cityCode?: string; // Universal Code
    countryCode?: string; // Universal Code
    timezone?: string; // timezone
    coordinations?: LocationCoordinates;
}

export interface LocationCoordinates {
    lng: string,
    lat: string
}