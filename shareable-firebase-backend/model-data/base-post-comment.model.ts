import {BaseModel} from "./base.model";

/**
 * @deprecated
 */
export interface BasePostComment extends BaseModel {
    userCreatorId: string;
    message: string;
    reactions: {
        [userId: string]: string, // Type of reaction
    },
    replies?: {
        [replyId: string]: BasePostComment,
    },
    pinned?: boolean,
}