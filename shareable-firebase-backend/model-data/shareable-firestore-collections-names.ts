export const ShareableFirestoreCollectionNames = {
    centralUserData: "CentralUserData",
    userFCMTokensMap: 'UserFCMTokensMap',
    subCollectionPagination:'Pagination', // This is only used for collection that handle Pagination (check updateCollectionPageContainer()

};