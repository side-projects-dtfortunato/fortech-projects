export interface StorageFileModel {
    fileUrl: string;
    storagePath: string;
}