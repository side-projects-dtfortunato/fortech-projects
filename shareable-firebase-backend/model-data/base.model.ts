import * as admin from "firebase-admin";

export interface BaseModel {
    uid?: string;
    createdAt?: number;
    updatedAt?: number;
    authorization?: {
        writeUIDs?: string[];
        readUIDs?: string[];
        allRead?: boolean;
        allWrite?: boolean;
    };
    searchableTokens?: string[];
}

export class BaseModelUtils {

    static initBaseModel(params: { collectionName: string, userId?: string, uid?: string}): BaseModel {
        return {
            uid: params.uid ? params.uid : admin.firestore().collection(params.collectionName).doc().id,
            createdAt: Date.now(),
            updatedAt: Date.now(),
            authorization: params.userId ? {
                writeUIDs: [params.userId],
                readUIDs: [params.userId],
            } : {allRead: true, allWrite: false},
        };
    }
}