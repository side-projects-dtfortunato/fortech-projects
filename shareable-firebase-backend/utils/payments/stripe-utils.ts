import {BackendSharedUtils} from "../backend-shared-utils";
import {StripePaymentStatus, StripeProductItemModel} from "../../model-data/custom-stripe.model";
import {WEBAPP_CONFIGS} from "../../../configs/webapp.configs";

export class StripeUtils {

    static getStripeApiKey() {
        return BackendSharedUtils.isProjectProduction() ? WEBAPP_CONFIGS.STRIPE_API.prod : WEBAPP_CONFIGS.STRIPE_API.dev
    }
    static getStripeWebhookKey() {
        return BackendSharedUtils.isProjectProduction() ? WEBAPP_CONFIGS.STRIPE_API.webhook_prod : WEBAPP_CONFIGS.STRIPE_API.webhook_dev
    }

    static async createCheckoutSession(props: { stripeApi: string, domain: string, listProducts: StripeProductItemModel[], metaData?: { [key: string]: string } }): Promise<string | undefined> {
        const stripe = require('stripe')(props.stripeApi);

        // Line Items
        const lineItems = props.listProducts.map((productItem) => {
            return {
                price_data: {
                    currency: productItem.currency,
                    product_data: {
                        name: productItem.productName,
                        description: productItem.productName,
                        image: productItem.productImageUrl,
                    },
                    unit_amount: productItem.price, // Amount in cents

                },
                quantity: 1,
            }
        });

        let metaParams: string = "";
        if (props.metaData && Object.keys(props.metaData).length > 0) {
            Object.keys(props.metaData).forEach((keyParam) => {
                metaParams += metaParams.length === 0 ? "?" : "&";
                metaParams += keyParam + "=" + BackendSharedUtils.string_to_slug(props.metaData![keyParam]);
            });
        }

        const session = await stripe.checkout.sessions.create({
            line_items: lineItems,
            mode: 'payment',
            success_url: `${props.domain}/payment/success` + metaParams,
            cancel_url: `${props.domain}/payment/cancel` + metaParams,
            automatic_tax: {enabled: true},
            allow_promotion_codes: true,
        });
        if (session && session.id) {
            return session.id;
        } else {
            return undefined;
        }
    }

    static async confirmPayment(props: { stripeApi: string, sessionId: string }): Promise<StripePaymentStatus> {
        // Initialize Stripe API client
        const stripe = require('stripe')(props.stripeApi);

        // Retrieve the checkout session details
        const session = await stripe.checkout.sessions.retrieve(props.sessionId);
        console.log("confirmPayment.session:", session);
        console.log("confirmPayment.session.payment_status:", session.payment_status);

        switch (session.payment_status) {
            case 'paid':
                return StripePaymentStatus.CONFIRMED;
            case 'requires_capture':
                return StripePaymentStatus.WAITING_PAYMENT;
            default:
                return StripePaymentStatus.CANCELLED;
        }
    }

    static async getSessionCheckoutUrl(props: { stripeApi: string, sessionId: string }): Promise<string | undefined> {
        // Initialize Stripe API client
        const stripe = require('stripe')(props.stripeApi);

        const session = await stripe.checkout.sessions.retrieve(props.sessionId);
        if (session && session.url) {
            return session.url;
        }
        return undefined
    }
}