import {RealtimeDbUtils} from "./realtime-db.utils";
import {OnesignalUtils} from "./onesignal.utils";
import {SocialNetworksPostUtils} from "../base-projects/social-networks-autopost/SocialNetworksPost.utils";
import {BackendImageUtils} from "./backend-image.utils";
import {WEBAPP_CONFIGS} from "../../configs/webapp.configs";
import { BackendSharedUtils } from "./backend-shared-utils";

const NOTIFICATION_QUEUE_COLLECTIONID = "NotificationsQueue"

export type SocialNetworksTypes = "REDDIT" | "FACEBOOK" | "INSTAGRAM" | "THREADS" | "BLUESKY" | "TELEGRAM";

export interface NotificationQueueItemModel {
    uid: string;
    title: string,
    description: string;
    link: string,
    socialPost: string;
    socialShortPost?: string;
    bgImageUrl?: string;
    category?: string;
    pushNotification: boolean;
    socialNetworks: SocialNetworksTypes[];
    registerTime?: number;
    instagramOnlyBgImage?: boolean;
    type?: "JOB_POST" | "GENERIC_NOTIFICATION";
    extraData?: any;
}

export class NotificationsQueueManager {

    static async addNotificationItemToQueue(notificationItem: NotificationQueueItemModel) {
        notificationItem.registerTime = Date.now();
        await RealtimeDbUtils.writeData({
            collectionName: NOTIFICATION_QUEUE_COLLECTIONID,
            docId: notificationItem.uid,
            data: notificationItem,
            merge: false,
        });
        console.log("NotificationsQueueManager.addNotificationItemToQueue: ", notificationItem);
    }

    static async dispatchNextNotificationItem() {
        // Get queue
        let notificationsQueue: { [uid: string]: NotificationQueueItemModel } = await RealtimeDbUtils.getData({
            collectionName: NOTIFICATION_QUEUE_COLLECTIONID
        });

        // Get the most recent
        let notificationItemToDispatch: NotificationQueueItemModel | undefined;
        if (notificationsQueue && Object.values(notificationsQueue).length > 0) {
            notificationItemToDispatch = Object.values(notificationsQueue).sort((n1, n2) => n1.registerTime! - n2.registerTime!)[0];
        }

        console.log("NotificationsQueueManager.dispatchNextNotificationItem: ", notificationItemToDispatch);

        // Dispatch Push Notification
        if (notificationItemToDispatch) {

            // Remove From the QUEUE
            await RealtimeDbUtils.deleteData({
                collectionName: NOTIFICATION_QUEUE_COLLECTIONID,
                docId: notificationItemToDispatch.uid,
            });

            if (notificationItemToDispatch.pushNotification) {
                switch(notificationItemToDispatch.type) {
                    case "JOB_POST":
                        await OnesignalUtils.sendPushNotificationToUserJobFilters(notificationItemToDispatch.title, notificationItemToDispatch.description, notificationItemToDispatch.link, notificationItemToDispatch.extraData);
                        break;
                    case "GENERIC_NOTIFICATION":
                    default:
                        await OnesignalUtils.sendPushNotification(notificationItemToDispatch.title, notificationItemToDispatch.description, notificationItemToDispatch.link);
                        break;
                }
            }

            if (notificationItemToDispatch.socialNetworks && notificationItemToDispatch.socialNetworks.length > 0) {
                for (let socialNetworkType of notificationItemToDispatch.socialNetworks) {

                    if (BackendSharedUtils.getRandomNumber(0, 100) > 50) {
                        continue; // Skip 50% of the time
                    }

                    try {
                        switch (socialNetworkType) {
                            case "FACEBOOK":
                                if (WEBAPP_CONFIGS.SOCIAL_NETWORK_API_KEY.metaAccessToken.length > 0) {
                                    await SocialNetworksPostUtils.facebookPublishTextPost({
                                        message: notificationItemToDispatch.socialPost,
                                        link: notificationItemToDispatch.link,
                                    });
                                }
                                break;
                            case "THREADS":
                                if (WEBAPP_CONFIGS.SOCIAL_NETWORK_API_KEY.threadsAPI.clientSecret.length > 0) {
                                    await SocialNetworksPostUtils.threadsPublishPost({
                                        message: notificationItemToDispatch.socialPost,
                                    });
                                }
                                break;
                            case "BLUESKY":
                                if (WEBAPP_CONFIGS.SOCIAL_NETWORK_API_KEY.bluesky.username.length > 0) {
                                    const blueskyResponse = await SocialNetworksPostUtils.blueskyPost({
                                        text: notificationItemToDispatch.socialShortPost ? notificationItemToDispatch.socialShortPost : notificationItemToDispatch.socialPost,
                                    });
                                    console.log(blueskyResponse);
                                }
                                break;
                            /*case "REDDIT":
                                if (WEBAPP_CONFIGS.SOCIAL_NETWORK_API_KEY.redditAPI.clientSecret.length > 0) {
                                    await SocialNetworksPostUtils.redditPostLink({
                                        title: notificationItemToDispatch.title,
                                        url: notificationItemToDispatch.link,
                                        text: notificationItemToDispatch.socialPost,
                                    });
                                }
                                break;*/
                            case "INSTAGRAM":
                                if (WEBAPP_CONFIGS.SOCIAL_NETWORK_API_KEY.instagramId.length > 0) {
                                    try {
                                        let instagramResponse: any;
                                        if (notificationItemToDispatch.instagramOnlyBgImage) {
                                            instagramResponse = await SocialNetworksPostUtils.instagramPublishPhoto({
                                                uid: notificationItemToDispatch.uid!,
                                                imageUrl: notificationItemToDispatch.bgImageUrl,
                                                message: notificationItemToDispatch.socialPost,
                                            });
                                        } else {
                                            const imageBase64: string = await BackendImageUtils.generateThumbnail({
                                                title: notificationItemToDispatch.title,
                                                bgImageUrl: notificationItemToDispatch.bgImageUrl!,
                                                category: notificationItemToDispatch.category,
                                            });
                                            instagramResponse = await SocialNetworksPostUtils.instagramPublishPhoto({
                                                uid: notificationItemToDispatch.uid!,
                                                imageBase64: imageBase64,
                                                message: notificationItemToDispatch.socialPost,
                                            });
                                        }
                                        console.log(instagramResponse);

                                    } catch (e: any) {
                                        console.log("Error publishing instagram post: ", e);
                                    }
                                }
                                break;
                            case "TELEGRAM":
                                if (WEBAPP_CONFIGS.SOCIAL_NETWORK_API_KEY["telegram"] && WEBAPP_CONFIGS.SOCIAL_NETWORK_API_KEY["telegram"].channel) {
                                    await SocialNetworksPostUtils.telegramPost(notificationItemToDispatch.socialPost);
                                }
                                break;
                        }
                    } catch (e: any) {
                        console.error(e.messsage);
                    }
                }
            }
        }
    }
}