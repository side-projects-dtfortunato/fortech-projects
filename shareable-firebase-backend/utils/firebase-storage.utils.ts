import * as admin from "firebase-admin";
import { ApiResponse, getQuickResponse } from "../model-data/api-response.model";
import { StorageFileModel } from "../model-data/storage-file.model";
import fetch from 'node-fetch';

export class FirebaseStorageUtils {

    static async uploadImage(params: { base64Image: string, fileName: string }): Promise<ApiResponse<any>> {
        const { base64Image, fileName } = params;

        if (!base64Image || !fileName) {
            return getQuickResponse(false);
        }

        try {

            // Decode the base64 image data
            const buffer = Buffer.from(base64Image.split(',')[1], 'base64');
            const bucket = admin.storage().bucket();
            const filePath: string = `images/${fileName}`;
            const file = bucket.file(filePath);

            // Upload the file to Firebase Storage
            await file.save(buffer, {
                metadata: {
                    contentType: 'image/jpeg',
                },
                public: true,
            });

            // Make the file publicly accessible
            await file.makePublic();

            // Get the public URL of the uploaded image
            const imageUrl = `https://storage.googleapis.com/${bucket.name}/${filePath}`;
            return getQuickResponse(true, {
                storagePath: filePath,
                fileUrl: imageUrl,
            } as StorageFileModel);
        } catch (error) {
            console.error('Error uploading image:', error);
            return getQuickResponse(false, null, 'Failed to upload image.');
        }
    }

    /**
     * Uploads an image to Firebase Storage from a URL using Firebase Admin SDK
     * @param imageUrl - The URL of the image to upload
     * @param storagePath - The path in Firebase Storage where the image should be stored
     * @returns Promise with the download URL of the uploaded image
     */
    static async uploadImageFromUrl(imageUrl: string, storagePath: string): Promise<ApiResponse<StorageFileModel>> {
        try {
            // Get storage bucket
            const bucket = admin.storage().bucket();

            // Fetch the image
            const response = await fetch(imageUrl);
            if (!response.ok) {
                throw new Error(`Failed to fetch image: ${response.statusText}`);
            }

            // Convert to buffer
            const imageBuffer = await response.buffer();

            // Create write stream for upload
            const file = bucket.file(storagePath);
            const writeStream = file.createWriteStream({
                metadata: {
                    contentType: response.headers.get('content-type') || 'image/jpeg'
                }
            });

            // Return promise that resolves with download URL after upload
            return new Promise((resolve, reject) => {
                writeStream.on('error', (error) => {
                    console.error('Error uploading image:', error);
                    reject(error);
                });

                writeStream.on('finish', async () => {
                    try {
                        // Make the file publicly accessible
                        await file.makePublic();

                        // Get the public URL
                        const publicUrl = `https://storage.googleapis.com/${bucket.name}/${file.name}`;
                        console.log('Image uploaded successfully');
                        resolve(getQuickResponse(true, {
                            storagePath: storagePath,
                            fileUrl: publicUrl,
                        } as StorageFileModel));
                    } catch (error) {

                        reject(getQuickResponse(false, error));
                    }
                });

                // Write buffer to stream
                writeStream.end(imageBuffer);
            });

        } catch (error) {
            console.error('Error in upload process:', error);
            throw error;
        }
    }

    static async deleteImage(params: { fileName: string }): Promise<ApiResponse<any>> {
        const { fileName } = params;

        if (!fileName) {
            return getQuickResponse(false);
        }

        const bucket = admin.storage().bucket();
        const file = bucket.file(`images/${fileName}`);

        try {
            await file.delete();
            return getQuickResponse(true);
        } catch (error) {
            console.error('Error deleting image:', error);
            return getQuickResponse(false, null, 'Failed to delete image.');
        }
    }
}
