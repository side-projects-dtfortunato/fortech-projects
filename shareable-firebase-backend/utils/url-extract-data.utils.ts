import * as cheerio from 'cheerio';
import axios from "axios";
import { JSDOM } from 'jsdom';
import { Readability } from '@mozilla/readability';
import fetch from 'node-fetch';
import {ApiResponse} from "../model-data/api-response.model";
import {BackendSharedUtils} from "./backend-shared-utils";
import getMetaData from "metadata-scraper";

export type UrlExtractDataServer = "HEROKU" | "RENDER";

interface FinalUrlAndHtmlResult {
    inputUrl: string;
    finalUrl?: string;
    html: string;
}

const ARTICLE_CONTENT_ELEMENT_LIST = [
    "article",
    "div.ArticleBody-articleBody",
    "div.article__body",
    "div.entry-content",
    "div #article-body",
    ".body-content",
    ".news-container",
    ".uk-container",
    ".main-container",
    "div.post-page__item",
    "div.content",
    "main",
]

export class UrlExtractDataUtils {


    static async extractArticleContent(url: string, customHTMLElement?: string, server?: UrlExtractDataServer): Promise<{
        title: string;
        content: string;
        textContent: string;
        image?: string;
        description?: string;
        finalUrl: string;
    } | undefined> {
        // Check if the url is the final url
        let finalUrl = url;
        let html: string;

        if (finalUrl.startsWith("https://news.google.com")) {
            const urlResolverResult: FinalUrlAndHtmlResult | undefined = await this.getFinalUrlAndHtml(url, 0, server);
            if (!urlResolverResult) {
                return undefined;
            }
            if (urlResolverResult.finalUrl) {
                finalUrl = urlResolverResult.finalUrl;
            }
            html = urlResolverResult.html;
        } else {
            const response = await fetch(finalUrl);
            html = await response.text();
        }

        console.log("finalUrl:", finalUrl);


        // Extracted content
        let extractedContent: any = await this.extractArticleContentFromMozillaReader(html, finalUrl);

        if (!extractedContent) {
            console.log("Failed to extract from Mozilla. Will try to extract it manually")
            extractedContent = await this.extractArticleContentFromHTMLElement(html, customHTMLElement);
        }

        if (extractedContent) {
            return {
                ...extractedContent,
                finalUrl
            };
        } else {
            return undefined;
        }

    }



    static async getFinalUrlAndHtml(url: string, attempt: number, server?: UrlExtractDataServer): Promise<FinalUrlAndHtmlResult | undefined> {
        let apiUrl = this.getServerDomain() + '/getFinalUrlAndHtml';

        try {
            let response: ApiResponse<FinalUrlAndHtmlResult> = (await axios.get<ApiResponse<FinalUrlAndHtmlResult>>(apiUrl, {
                params: { url },
            })).data;
            if (!response.isSuccess) {
                // Try Again
                response = (await axios.get<ApiResponse<FinalUrlAndHtmlResult>>(apiUrl, {
                    params: { url },
                })).data;
            }

            return response.responseData;
        } catch (error) {
            console.error('Error fetching final URL and HTML:', error);
            if (attempt < 2) {
                return await this.getFinalUrlAndHtml(url, attempt + 1);
            } else {
                return undefined;
            }
        }
    }

    static async extractArticleContentFromMozillaReader(html: string, url: string): Promise<{
        title: string,
        content: string,
        image?: string,
        description?: string,
    } | undefined> {
        // Use JSDOM to create a DOM
        const dom = new JSDOM(html, { url: url });
        const document = dom.window.document;

        // Use Readability to parse the article
        const reader = new Readability(document);
        const article = reader.parse();

        if (!article) {
            return undefined;
        }

        // Extract image (first try og:image, then first image in content)
        let image = document.querySelector('meta[property="og:image"]')?.getAttribute('content') || undefined;

        if (!image && article.content) {
            const contentDOM = new JSDOM(article.content);
            image = contentDOM.window.document.querySelector('img')?.getAttribute('src') || undefined;
        }

        // Extract description
        let description = document.querySelector('meta[name="description"]')?.getAttribute('content') ||
            document.querySelector('meta[property="og:description"]')?.getAttribute('content') ||
            article.excerpt ||
            '';

        const articleContent = await this.removeBrokenImages(article.content);

        return {
            title: article.title,
            content: articleContent,
            // textContent: article.textContent,
            image,
            description,
        };
    }

    static async extractArticleContentFromHTMLElement(html: string, customHtmlContentElement?: string): Promise<{
        title: string;
        content: string;
        image?: string;
        description?: string;
    } | undefined> {

        // Parse the HTML using Cheerio
        const $ = cheerio.load(html);

        // Remove all script and style elements
        $('script, style').remove();

        // Identify elements (replace with your selectors)
        const title = $('h1').first().text().trim() || $('meta[property="og:title"]').attr('content') || '';

        let content = "";
        let contentElement: cheerio.Cheerio | null = null;

        // Use a custom content selector if provided
        const elementsToTry = customHtmlContentElement ? [customHtmlContentElement, ...ARTICLE_CONTENT_ELEMENT_LIST] : ARTICLE_CONTENT_ELEMENT_LIST;
        for (let elementAttempt of elementsToTry) {
            contentElement = $(elementAttempt);
            console.log(`Trying selector: ${elementAttempt}, Found content: ${contentElement.length > 0 ? 'Yes' : 'No'}`);
            if (contentElement.length > 0) {
                break;
            }
        }

        if (contentElement && contentElement.length > 0) {
            // Remove potential duplicate title from content
            contentElement.find('h1').remove();

            // Process images
            contentElement.find('img').each((_, elem) => {
                const imgElement = $(elem);
                const src = imgElement.attr('src');
                const alt = imgElement.attr('alt') || '';
                if (src) {
                    imgElement.replaceWith(`<img src="${src}" alt="${alt}">`);
                }
            });

            // Get the HTML content
            content = contentElement.html() || "";

            // Clean content (replace multiple spaces and newlines)
            content = content.replace(/\s\s+/g, ' ').replace(/\n+/g, '\n').trim();
        }

        // Extract thumbnail URL (example selector, adjust as needed)
        const image = $('meta[property="og:image"]').attr('content') || $('img').first().attr('src');

        // Extract SEO description (example selector, adjust as needed)
        const description = $('meta[name="description"]').attr('content') || $('meta[property="og:description"]').attr('content') || '';

        return { title, content, image, description };
    }

    static async getHtmlContent(url: string, server?: UrlExtractDataServer): Promise<FinalUrlAndHtmlResult> {
        let apiUrl = this.getServerDomain() + '/getHtmlContent';

        try {
            const response = await axios.get<FinalUrlAndHtmlResult>(apiUrl, {
                params: { url },
                timeout: 30000, // 30 seconds timeout
            });

            return response.data;
        } catch (error) {
            if (axios.isAxiosError(error)) {
                if (error.response) {
                    // The request was made and the server responded with a status code
                    // that falls out of the range of 2xx
                    console.error('Error response data:', error.response.data);
                    console.error('Error response status:', error.response.status);
                } else if (error.request) {
                    // The request was made but no response was received
                    console.error('No response received');
                } else {
                    // Something happened in setting up the request that triggered an Error
                    console.error('Error message:', error.message);
                }
            }

            return {
                html: "",
                inputUrl: url,
            };
        }
    }

    static async getHtmlWithElement(url: string, htmlElement: string, preloadUrl?: string): Promise<string> {
        let apiUrl = this.getServerDomain() + '/getHtmlWithElement';

        try {
            const response = await axios.get<string>(apiUrl, {
                params: { url, htmlElement, preloadUrl },
                timeout: 30000, // 30 seconds timeout
            });

            return response.data;
        } catch (error) {
            if (axios.isAxiosError(error)) {
                if (error.response) {
                    // The request was made and the server responded with a status code
                    // that falls out of the range of 2xx
                    console.error('Error response data:', error.response.data);
                    console.error('Error response status:', error.response.status);
                } else if (error.request) {
                    // The request was made but no response was received
                    console.error('No response received');
                } else {
                    // Something happened in setting up the request that triggered an Error
                    console.error('Error message:', error.message);
                }
            }
            // Handle non-Axios errors
            return "";
        }
    }

    static async getLinkedInJobDetails(url: string): Promise<any> {
        let apiUrl = this.getServerDomain() + '/getLinkedInJobDetails';

        if (!url || url.length === 0) {
            return {}
        }
        try {
            const response = await axios.get<FinalUrlAndHtmlResult>(apiUrl, {
                params: { url },
                timeout: 40000, // 30 seconds timeout
            });

            return response.data;
        } catch (error) {
            if (axios.isAxiosError(error)) {
                if (error.response) {
                    // The request was made and the server responded with a status code
                    // that falls out of the range of 2xx
                    console.error('Error response data:', error.response.data);
                    console.error('Error response status:', error.response.status);
                } else if (error.request) {
                    // The request was made but no response was received
                    console.error('No response received');
                } else {
                    // Something happened in setting up the request that triggered an Error
                    console.error('Error message:', error.message);
                }
            }
            return undefined;
        }
    }

    /*
    // OLD FUNCTION TO EXTRACT THE ARTICLE CONTENT MANUALLY
    static async extractArticleContent(url: string, customHtmlContentElement?: string): Promise<{
        title: string;
        content: string;
        image?: string;
        description?: string;
        finalUrl: string,
    }> {
        // Check if the url is the final url
        let finalUrl = url;
        let html;
        if (finalUrl.startsWith("https://news.google.com")) {
            const urlResolverResult: FinalUrlAndHtmlResult = await this.getFinalUrlAndHtml(url);
            if (urlResolverResult.finalUrl) {
                finalUrl = urlResolverResult.finalUrl;
            }
            html = urlResolverResult.html;
        }
        console.log("finalUrl:", finalUrl);

        if (!html) {
            const response = await fetch(finalUrl);
            html = await response.text();
        }

        // Parse the HTML using Cheerio
        const $ = cheerio.load(html);

        // Remove all script and style elements
        $('script, style').remove();

        // Identify elements (replace with your selectors)
        const title = $('h1').first().text().trim() || $('meta[property="og:title"]').attr('content') || '';

        let content = "";
        let contentElement: cheerio.Cheerio | null = null;

        // Use a custom content selector if provided
        const elementsToTry = customHtmlContentElement ? [customHtmlContentElement, ...ARTICLE_CONTENT_ELEMENT_LIST] : ARTICLE_CONTENT_ELEMENT_LIST;
        for (let elementAttempt of elementsToTry) {
            contentElement = $(elementAttempt);
            console.log(`Trying selector: ${elementAttempt}, Found content: ${contentElement.length > 0 ? 'Yes' : 'No'}`);
            if (contentElement.length > 0) {
                break;
            }
        }

        if (contentElement && contentElement.length > 0) {
            // Remove potential duplicate title from content
            contentElement.find('h1').remove();

            // Process images
            contentElement.find('img').each((_, elem) => {
                const imgElement = $(elem);
                const src = imgElement.attr('src');
                const alt = imgElement.attr('alt') || '';
                if (src) {
                    imgElement.replaceWith(`<img src="${src}" alt="${alt}">`);
                }
            });

            // Get the HTML content
            content = contentElement.html() || "";

            // Clean content (replace multiple spaces and newlines)
            content = content.replace(/\s\s+/g, ' ').replace(/\n+/g, '\n').trim();
        }

        // Extract thumbnail URL (example selector, adjust as needed)
        const image = $('meta[property="og:image"]').attr('content') || $('img').first().attr('src');

        // Extract SEO description (example selector, adjust as needed)
        const description = $('meta[name="description"]').attr('content') || $('meta[property="og:description"]').attr('content') || '';

        return { title, content, image, description, finalUrl };
    }*/

    /*static async extractArticleContent(url: string, customHtmlContentElement?: string): Promise<{
        title: string;
        content: string;
        image?: string;
        description?: string;
        finalUrl: string,
    }> {
        // Check if the url is the final url
        let finalUrl = url;
        let html;
        if (finalUrl.startsWith("https://news.google.com")) {
            const urlResolverResult: FinalUrlAndHtmlResult = await this.getFinalUrlAndHtml(url);
            // const extractedData: { finalUrl: string, html: string | null } = await SharedArticlesDigestUtils.getFinalUrlAndHtml(url);
            if (urlResolverResult.finalUrl) {
                finalUrl = urlResolverResult.finalUrl;
            }

            html = urlResolverResult.html;
        }
        console.log("finalUrl:", finalUrl);

        if (!html) {
            const response = await fetch(finalUrl);
            html = await response.text();
        }

        // Parse the HTML using Cheerio
        const $ = cheerio.load(html);

        // Remove all script and style elements
        $('script, style').remove();

        // Identify elements (replace with your selectors)
        const title = $('h1').first().text().trim(); // Adjust selector for title
        let content = "";

        // Use a custom content selector if provided
        const elementsToTry = customHtmlContentElement ? [customHtmlContentElement, ...ARTICLE_CONTENT_ELEMENT_LIST] : ARTICLE_CONTENT_ELEMENT_LIST;
        for (let elementAttempt of elementsToTry) {
            const elementContent = $(elementAttempt).text().trim();
            console.log(`Trying selector: ${elementAttempt}, Found content: ${elementContent ? 'Yes' : 'No'}`); // Debug statement
            if (elementContent && elementContent.length > 0) {
                content = elementContent;
                break;
            }
        }

        // Remove potential duplicate title from content
        content = content.replace(new RegExp(title, 'gi'), ''); // Replace all instances (gi flag)

        // Clean content (replace \n and extra spaces)
        content = content.replace(/\s\s+/g, ' ').replace(/\n/g, ''); // Replace multiple spaces and \n

        // Extract thumbnail URL (example selector, adjust as needed)
        const image = $('meta[property="og:image"]').attr('content') || $('img').first().attr('src');

        // Extract SEO description (example selector, adjust as needed)
        const description = $('meta[name="description"]').attr('content') || '';

        return { title, content, image, description, finalUrl };
    }*/

    static async removeBrokenImages(htmlString: string): Promise<string> {
        const dom = new JSDOM(htmlString);
        const document = dom.window.document;
        const images = document.getElementsByTagName('img');

        const checkImage = async (img: HTMLImageElement): Promise<boolean> => {
            // Check if the image URL contains "/ad-placeholder/"
            if (img.src.includes('/ad-placeholder/')) {
                return false;
            }

            try {
                const response = await axios.head(img.src);
                return response.status === 200;
            } catch (error) {
                return false;
            }
        };

        const promises = Array.from(images).map(async (img) => {
            const isValid = await checkImage(img);
            if (!isValid) {
                img.parentNode?.removeChild(img);
            }
        });

        await Promise.all(promises);

        return dom.serialize();
    }

    static async extractThumbnail(url: string): Promise<string | undefined> {
        try {
            const metadata: any = await getMetaData(url);

            // Check for og:image first
            if (metadata.og && metadata.og.image) {
                return metadata.og.image;
            }

            // Fallback to twitter:image
            if (metadata.twitter && metadata.twitter.image) {
                return metadata.twitter.image;
            }

            // If no social media images found, try to get any image from the page
            if (metadata.image) {
                return metadata.image;
            }

            // No thumbnail found
            return undefined;
        } catch (error) {
            console.error(`Error extracting thumbnail from ${url}:`, error);
            return undefined;
        }
    }

    static getServerDomain() {
        return "https://fortulyapps.up.railway.app";
        /*const serverSelection: number = BackendSharedUtils.getRandomNumber(1, 2);
        switch (serverSelection) {
            case 1: return "https://fortuly-server.onrender.com";
            case 2:
            default:
                return "https://fortulyapps.up.railway.app";
        }*/
    }
}