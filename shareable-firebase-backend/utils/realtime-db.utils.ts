import * as admin from "firebase-admin";


export class RealtimeDbUtils {

    static async writeData(params: {collectionName: string, docId?: string, subPaths?:string[], data: any, merge: boolean}): Promise<void> {
        const db = admin.database();
        let path = params.collectionName;

        if (params.docId) {
            path += "/" + params.docId
        }
        if (params.subPaths) {
            params.subPaths.forEach((subPath) => {
                path = path + "/" + subPath;
            })
        }

        let finalData = params.data;
        if (params.merge) {
            const existingData: any = await this.getData(params);
            if (existingData) {
                finalData = {
                    ...existingData,
                    ...finalData,
                };
            }
        }

        return db.ref(path).set(finalData);
    }

    static deleteData(params: {collectionName: string, docId: string, subPaths?:string[]}): Promise<void> {
        const db = admin.database();
        let path = params.collectionName + "/" + params.docId;
        if (params.subPaths) {
            params.subPaths.forEach((subPath) => {
                path = path + "/" + subPath;
            })
        }
        return db.ref(path).remove();
    }

    static async getData(params: {collectionName: string, docId?: string, subPaths?:string[]}): Promise<any> {
        const db = admin.database();
        let path = params.collectionName;

        if (params.docId) {
            path += "/" + params.docId;
        }
        if (params.subPaths) {
            params.subPaths.forEach((subPath) => {
                path = path + "/" + subPath;
            })
        }

        return (await db.ref(path).once("value")).val();
    }

    static async getFirstChild(params: {collectionName: string, docId: string, subPaths?:string[]}): Promise<any> {
        const data = await this.getData(params);

        if (data && Object.keys(data).length > 0) {
            return Object.values(data)[0];
        } else {
            return null;
        }
    }
}