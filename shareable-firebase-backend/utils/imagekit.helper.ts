import {WEBAPP_CONFIGS} from "../../configs/webapp.configs";

var ImageKit = require("imagekit");

import {StorageFileModel} from "../model-data/storage-file.model";

const imagekit = new ImageKit(WEBAPP_CONFIGS.IMAGE_KIT_API);

export class ImagekitHelper {


    static async uploadImageFromUrl(data: string, uid: string, folderPath: string, tags: string[]): Promise<StorageFileModel | undefined> {
        const response = await imagekit.upload({
            file : data, //required
            fileName : uid + ".png",   //required
            tags: tags,
            folder: folderPath,
            overwriteFile: true,
        });

        if (response) {
            return {
                storagePath: response.fileId,
                fileUrl: response.url,
            };
        } else {
            return undefined;
        }
    }

}