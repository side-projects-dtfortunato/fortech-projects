import {INLINE_VARS} from "./shared/template-inlinevars";
import {WEBAPP_CONFIGS} from "../../../configs/webapp.configs";
import {SharedNewsletterDailyUserDigestModel} from "../../model-data/frontend-model-data/shared-newsletter-user.model";
import {SharedPublicUserprofileModel} from "../../model-data/frontend-model-data/shared-public-userprofile.model";
import {BasePostDataModel} from "../../model-data/frontend-model-data/base-post-data.model";
import {BackendRouteUtils} from "../backend-route.utils";


export interface ListItemParams {
    imageUrl?: string,
    postedOn: string,
    title: string,
    subtitle?: string,
    summary?: string,
    readMoreUrl: string,
    rightSideImageUrl?: string,
}

export interface ListItemsContainer {
    title: string,
    listItems: ListItemParams[]
}

export interface EmailTemplateParams {
    logoUrl: string,
    siteUrl: string,
    headerTitle: string,
    headerSubtitle: string,
    listItemsContainer: ListItemsContainer[],
    listSocialNetworks: { link: string, iconUrl: string }[],
    footer: {
        address: string,
        email: string,
        unsubscribeLink: string,
    },
}

export class EmailTemplateDigestUtils {

    static generateNewFollowerHtmlContent(userInfos: SharedPublicUserprofileModel, title: string, subtitle: string, unsubscribeLink: string) {
        let userName: string | undefined = userInfos.name ? userInfos.name : userInfos.username!;
        if (!userName) {
            userName = "Anonymous";
        }
        return this.generateHtmlContent({
            headerTitle: title,
            headerSubtitle: subtitle,
            logoUrl: WEBAPP_CONFIGS.LOGO_URL,
            siteUrl: WEBAPP_CONFIGS.WEBSITE_URL,
            listItemsContainer: [
                {
                    title: title,
                    listItems: [
                        {
                            title: userName!,
                            readMoreUrl: WEBAPP_CONFIGS.POST_PREFIX_URL + "public-profile/" + (userInfos.username ? "@" + userInfos.username : userInfos.uid),
                            postedOn: "",
                            summary: "Started to follow you",
                            imageUrl: userInfos.userPicture?.fileUrl ? userInfos.userPicture?.fileUrl : "https://www.sidehustlify.com/images/ic_user.png",
                            subtitle: userInfos.publicSubtitle,
                        },
                    ]
                }
            ],
            footer: {
                address: WEBAPP_CONFIGS.COMPANY_ADDRESS,
                email: WEBAPP_CONFIGS.COMPANY_EMAIL,
                unsubscribeLink: unsubscribeLink,
            },
            listSocialNetworks: [
                {
                    link: "https://medium.com/@journeypreneur",
                    iconUrl: "https://www.iconpacks.net/icons/2/free-medium-icon-2177-thumb.png",
                },
                {
                    link: "https://twitter.com/journeypreneur_",
                    iconUrl: "https://cdn-icons-png.flaticon.com/512/124/124021.png",
                },
            ]
        })
    }

    static generatePostNotifHtmlContent(postItem: BasePostDataModel, title: string, subtitle: string, unsubscribeLink: string) {
        return this.generateHtmlContent({
            headerTitle: title,
            headerSubtitle: subtitle,
            logoUrl: WEBAPP_CONFIGS.LOGO_URL,
            siteUrl: WEBAPP_CONFIGS.WEBSITE_URL,
            listItemsContainer: [
                {
                    title: title,
                    listItems: [
                        {
                            title: postItem.title!,
                            readMoreUrl: WEBAPP_CONFIGS.POST_PREFIX_URL + postItem.uid,
                            postedOn: new Date(postItem.publishedAt).toLocaleString(),
                            summary: postItem.summary,
                            imageUrl: postItem.thumbnailUrl,
                            subtitle: postItem.listUsersInfos[postItem.userCreatorId].username ? "Published by: " + postItem.listUsersInfos[postItem.userCreatorId].username : "",
                        },
                    ]
                }
            ],
            footer: {
                address: WEBAPP_CONFIGS.COMPANY_ADDRESS,
                email: WEBAPP_CONFIGS.COMPANY_EMAIL,
                unsubscribeLink: unsubscribeLink,
            },
            listSocialNetworks: [
                {
                    link: "https://medium.com/@journeypreneur",
                    iconUrl: "https://www.iconpacks.net/icons/2/free-medium-icon-2177-thumb.png",
                },
                {
                    link: "https://twitter.com/journeypreneur_",
                    iconUrl: "https://cdn-icons-png.flaticon.com/512/124/124021.png",
                },
            ]
        })
    }

    static generateListItemPost(postItem: BasePostDataModel) {
        return {
            title: postItem.title ? postItem.title : "",
            readMoreUrl: WEBAPP_CONFIGS.POST_PREFIX_URL + postItem.uid,
            postedOn: new Date(postItem.publishedAt).toLocaleString(),
            summary: postItem.summary ? postItem.summary : "",
            imageUrl: postItem.thumbnailUrl,
            subtitle: postItem.listUsersInfos[postItem.userCreatorId].username ? "Published by: " + postItem.listUsersInfos[postItem.userCreatorId].username : "",
        };
    }

    static generateDailyUserDigest(dailyDigestData: SharedNewsletterDailyUserDigestModel) {
        let listItems: ListItemsContainer[] = [];

        if (dailyDigestData.newFollowers && Object.keys(dailyDigestData.newFollowers).length > 0) {
            listItems.push({
                title: "New followers",
                listItems: Object.values(dailyDigestData.newFollowers).map((userData) => {
                    return {
                        title: userData.username ? userData.username : userData.name,
                        readMoreUrl: WEBAPP_CONFIGS.PROFILE_PREFIX_URL + userData.uid,
                        postedOn: "",
                        summary: "New follower",
                        imageUrl: userData.pictureUrl,
                        subtitle: userData.subtitleLabel,
                    }
                }),
            })
        }
        // New upvotes Posts
        if (dailyDigestData.newUpvotes && Object.keys(dailyDigestData.newUpvotes).length > 0) {
            listItems.push({
                title: "Your posts were upvoted",
                listItems: Object.values(dailyDigestData.newUpvotes).map((postData) => {
                    return this.generateListItemPost(postData);
                }),
            })
        }
        // New Comments Posts
        if (dailyDigestData.newCommentsPosts && Object.keys(dailyDigestData.newCommentsPosts).length > 0) {
            listItems.push({
                title: "New Comments",
                listItems: Object.values(dailyDigestData.newCommentsPosts).map((postData) => {
                    return this.generateListItemPost(postData);
                }),
            })
        }
        // Following Posts
        if (dailyDigestData.followingPosts && Object.keys(dailyDigestData.followingPosts).length > 0) {
            listItems.push({
                title: "New posts from following publishers",
                listItems: Object.values(dailyDigestData.followingPosts).map((postData) => {
                    return this.generateListItemPost(postData);
                }),
            })
        }


        if (listItems.length > 0) {
            return this.generateHtmlContent({
                headerTitle: "Daily Digest",
                headerSubtitle: "We aggregate all relevant updates for you",
                logoUrl: WEBAPP_CONFIGS.LOGO_URL,
                siteUrl: WEBAPP_CONFIGS.WEBSITE_URL,
                listItemsContainer: listItems,
                footer: {
                    address: WEBAPP_CONFIGS.COMPANY_ADDRESS,
                    email: WEBAPP_CONFIGS.COMPANY_EMAIL,
                    unsubscribeLink: WEBAPP_CONFIGS.UNSUBSCRIBE_URL,
                },
                listSocialNetworks: [
                    {
                        link: "https://medium.com/@journeypreneur",
                        iconUrl: "https://www.iconpacks.net/icons/2/free-medium-icon-2177-thumb.png",
                    },
                    {
                        link: "https://twitter.com/journeypreneur_",
                        iconUrl: "https://cdn-icons-png.flaticon.com/512/124/124021.png",
                    },
                ]
            });
        } else {
            return "";
        }
    }

    static generateHtmlContent(params: EmailTemplateParams) {
        let htmlTemplate: string = "" + EMAIL_BODY_CONTENT_TEMPLATE;
        let bodyFullContent: string = "";

        // Fill header
        bodyFullContent = bodyFullContent + HEADER_LOGO_TEMPLATE
            .replace(INLINE_VARS.HEADER_LOGO_URL, params.logoUrl)
            .replace(INLINE_VARS.WEBSITE_URL, params.siteUrl);

        // Header Title
        bodyFullContent = bodyFullContent + HEADER_TITLE_TEMPLATE
            .replace(INLINE_VARS.HEADER_TITLE, params.headerTitle)
            .replace(INLINE_VARS.HEADER_SUBTITLE, params.headerSubtitle);


        // Body Lists Containers

        params.listItemsContainer.forEach((containerItem => {
            // List Title
            bodyFullContent = bodyFullContent + BODY_LIST_TITLE_COMPONENT_TEMPLATE.replace(INLINE_VARS.BODY_LIST_TITLE, containerItem.title);

            // List Items
            containerItem.listItems.forEach((listItem) => {
                bodyFullContent = bodyFullContent + this.getListItemComponent(listItem);
            });
        }));

        // Social Networks
        let socialNetworkList: string = "";
        params.listSocialNetworks.forEach((item) => {
            socialNetworkList = socialNetworkList + BODY_BOTTOM_SOCIALNETWORK_ITEM_TEMPLATE
                .replace(INLINE_VARS.BODY_SOCIALNETWORK_ITEM_LINK, item.link)
                .replace(INLINE_VARS.BODY_SOCIALNETWORK_ITEM_ICONURL, item.iconUrl);
        });
        bodyFullContent = bodyFullContent + BODY_BOTTOM_SOCIALNETWORKS_CONTAINER_TEMPLATE.replace(INLINE_VARS.BODY_SOCIALNETWORKS_CONTAINER, socialNetworkList);

        // Footer
        bodyFullContent = bodyFullContent + FOOTER_CONTAINER_TEMPLATE
            .replace(INLINE_VARS.FOOTER_UNSUBSCRIBE_URL, params.footer.unsubscribeLink)
            .replace(INLINE_VARS.FOOTER_COMPANY_ADDRESS, params.footer.address)
            .replace(INLINE_VARS.FOOTER_COMPANY_EMAIL, params.footer.email);

        return htmlTemplate.replace(INLINE_VARS.EMAIL_BODY_CONTENT, bodyFullContent);
    }

    static getListItemComponent(params: ListItemParams) {
        if (params.imageUrl) {
            let htmlContent = BODY_LIST_ITEM_COMPONENT_TEMPLATE.replace(INLINE_VARS.ITEM_IMAGE_URL, params.imageUrl)
                .replace(INLINE_VARS.ITEM_POSTED_ON, params.postedOn)
                .replace(INLINE_VARS.ITEM_TITLE, params.title)
                .replace(INLINE_VARS.ITEM_SUBTITLE, params.subtitle ? params.subtitle : "")
                .replace(INLINE_VARS.ITEM_SUMMARY, params.summary ? params.summary : "")
                .replace(INLINE_VARS.ITEM_READ_MORE_URL, params.readMoreUrl)
                .replace(INLINE_VARS.ITEM_READ_MORE_URL, params.readMoreUrl);

            if (params.rightSideImageUrl) {
                htmlContent = htmlContent.replace(INLINE_VARS.ITEM_RIGHT_CONTAINER, BODY_LIST_RIGHT_IMAGE);
                htmlContent = htmlContent.replace(INLINE_VARS.ITEM_READ_MORE_URL, params.readMoreUrl);
                htmlContent = htmlContent.replace(INLINE_VARS.ITEM_RIGHT_IMAGE_URL, params.rightSideImageUrl);
            } else {
                htmlContent = htmlContent.replace(INLINE_VARS.ITEM_RIGHT_CONTAINER, "");
            }
            return htmlContent;
        } else {
            if (params.subtitle || params.summary) {
                return BODY_LIST_ITEM_NO_IMAGE_COMPONENT_TEMPLATE
                    .replace(INLINE_VARS.ITEM_POSTED_ON, params.postedOn)
                    .replace(INLINE_VARS.ITEM_TITLE, params.title)
                    .replace(INLINE_VARS.ITEM_SUBTITLE, params.subtitle ? params.subtitle : "")
                    .replace(INLINE_VARS.ITEM_SUMMARY, params.summary ? params.summary : "")
                    .replace(INLINE_VARS.ITEM_READ_MORE_URL, params.readMoreUrl);
            } else {
                return BODY_LIST_ITEM_ONLY_TITLE_TEMPLATE
                    .replace(INLINE_VARS.ITEM_TITLE, params.title)
                    .replace(INLINE_VARS.ITEM_READ_MORE_URL, params.readMoreUrl);
            }
        }
    }


    static generateNewCommentReplyNotification(commentId: string) {
        return `<!DOCTYPE html>
                <html>
                <head>
                    <style>
                        .email-container {
                            max-width: 600px;
                            margin: auto;
                            padding: 20px;
                            font-family: Arial, sans-serif;
                        }
                        .header-logo img {
                            max-width: 100%;
                            height: auto;
                        }
                        .content {
                            margin-top: 20px;
                        }
                        .cta-button {
                            display: block;
                            width: max-content;
                            margin: 20px auto;
                            padding: 10px 20px;
                            text-align: center;
                            text-decoration: none;
                            background-color: #F2A900; /* Updated CTA color */
                            color: white;
                            border-radius: 5px;
                        }
                        .footer {
                            margin-top: 30px;
                            font-size: 12px;
                            text-align: center;
                        }
                        .footer a {
                            color: #008CBA;
                            text-decoration: none;
                        }
                    </style>
                </head>
                <body>
                    <div class="email-container">
                        <div class="header-logo">
                            <!-- Link added to the logo -->
                            <a href="${WEBAPP_CONFIGS.WEBSITE_URL}">
                                <img src="${WEBAPP_CONFIGS.WEBSITE_URL}/images/logo.png" alt="${WEBAPP_CONFIGS.SITE_NAME} Logo">
                            </a>
                        </div>
                        <div class="content">
                            <p>Hello,</p>
                            <p>Your comment on ${WEBAPP_CONFIGS.SITE_NAME} has received a new reply! You can view the reply by clicking on the link below:</p>
                            <!-- Replace '#' with the actual link to the comment -->
                            <a href="${BackendRouteUtils.getCommunityDetailsPage(commentId)}" class="comment-link">View Comment</a>
                        </div>
                        <!-- Replace '#' with the actual link for the CTA -->
                        <a href="${WEBAPP_CONFIGS.WEBSITE_URL}" class="cta-button">Go to ${WEBAPP_CONFIGS.SITE_NAME}</a>
                        <div class="footer">
                            <p>If you wish to unsubscribe from these notifications, please click <a href="${INLINE_VARS.FOOTER_UNSUBSCRIBE_URL}">here</a>.</p>
                            <p>Contact us at <a href="mailto:${WEBAPP_CONFIGS.COMPANY_EMAIL}">${WEBAPP_CONFIGS.COMPANY_EMAIL}</a></p>
                            <!-- Add actual links to your social networks -->
                            <!-- <p>Follow us on <a href="#">Twitter</a>, <a href="#">Facebook</a>, and <a href="#">Instagram</a></p> -->
                        </div>
                    </div>
                </body>
                </html>`;
    }

}

/**** PRO TIP: Convert HTML to EMAIL HTML: https://templates.mailchimp.com/resources/inline-css/ ***/

const HEADER_LOGO_TEMPLATE = "<table class=\"wrapper\" role=\"module\" data-type=\"image\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"table-layout: fixed;-webkit-font-smoothing: antialiased;-webkit-text-size-adjust: 100%;-moz-text-size-adjust: 100%;-ms-text-size-adjust: 100%;width: 100% !important;\">\n" +
    "    <tr>\n" +
    "        <td style=\"font-size:6px;line-height:10px;background-color:#fff;padding:20px 0px 20px 0px;\" valign=\"top\" align=\"center\">\n" +
    "            <a href=\"" + INLINE_VARS.WEBSITE_URL + "\" style=\"color: #333333;text-decoration: none;\"><img width=\"300\" class=\"max-width\" style=\"display:block;color:#000;text-decoration:none;font-family:Helvetica, arial, sans-serif;font-size:16px;max-width:50% !important;width:50%;height:auto !important;\" alt=\"" + WEBAPP_CONFIGS.SITE_NAME + "\" src=\"" + INLINE_VARS.HEADER_LOGO_URL + "\" border=\"0\"></a>\n" +
    "        </td>\n" +
    "    </tr>\n" +
    "</table>";

const HEADER_TITLE_TEMPLATE = "<table class=\"module\" role=\"module\" data-type=\"text\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"table-layout: fixed;\">\n" +
    "    <tr>\n" +
    "        <td style=\"background-color:#FFF;padding:17px 0px 12px 0px;line-height:24px;text-align:inherit;\" height=\"100%\" valign=\"top\" bgcolor=\"#FFF\">\n" +
    "            <div style=\"text-align: center;font-family: arial,helvetica,sans-serif;font-size: 16px;\">\n" +
    "                <span style=\"font-size:20px;\"><strong>" + INLINE_VARS.HEADER_TITLE + "</strong></span>\n" +
    "            </div>\n" +
    "\n" +
    "            <div style=\"text-align: center;font-family: arial,helvetica,sans-serif;font-size: 16px;\">\n" +
    "                <span style=\"color: rgb(51, 51, 51); font-family: arial, helvetica, sans-serif; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; text-align: center;\">" + INLINE_VARS.HEADER_SUBTITLE + "</span>\n" +
    "            </div>\n" +
    "        </td>\n" +
    "    </tr>\n" +
    "</table>";

const BODY_LIST_ITEM_COMPONENT_TEMPLATE = "<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class=\"mcnButtonBlock\" width=\"100%\" style=\"background-color: #FFFFFF\">\n" +
    "    <tbody class=\"mcnButtonBlockOuter\">\n" +
    "        <tr>\n" +
    "            <td style=\"width: 60px; padding-top:15px;padding-right:0px;padding-bottom:15px;padding-left:15px;border-collapse:collapse;color:#606060;font-family:Helvetica;font-size:15px;line-height:150%;text-align:left;border-bottom: 1px solid #EAEAEA\" valign=\"top\">\n" +
    "                <span class=\"sg-image\">\n" +
    "                    <a href=\"" + INLINE_VARS.ITEM_READ_MORE_URL + "\" style=\"color: #333333;text-decoration: none;\"><img class=\"service-logo\" style=\"width: 60px; border-radius: 3px; border: 1px solid #ebebeb; height: 60px;\" src=\"" + INLINE_VARS.ITEM_IMAGE_URL + "\"></a>\n" +
    "                </span>\n" +
    "            </td>\n" +
    "            <td style=\"padding-top:14px;padding-right:18px;padding-bottom:14px;padding-left:18px;border-collapse:collapse;color:#606060;font-family:Helvetica;font-size:15px;line-height:150%;text-align:left;border-bottom: 1px solid #EAEAEA\" valign=\"top\">\n" +
    "                <a style=\"color: #000000; font-weight: bold; text-decoration:none;\" href=\"" + INLINE_VARS.ITEM_READ_MORE_URL + "\">" + INLINE_VARS.ITEM_TITLE + "</a>\n" + INLINE_VARS.ITEM_RIGHT_CONTAINER +
    "                <div style=\"font-size: 11px;line-height: 15px;color: grey;font-family: arial,helvetica,sans-serif;\">\n" +
    "                    " + INLINE_VARS.ITEM_SUBTITLE + "\n" +
    "                </div>\n" +
    "                " + INLINE_VARS.ITEM_SUMMARY + "\n" +
    "            </td>\n" +
    "        </tr>\n" +
    "    </tbody>\n" +
    "</table>";

const BODY_LIST_RIGHT_IMAGE = `<div style="font-size: 11px;line-height: 15px;color: grey;font-family: arial,helvetica,sans-serif;">
                    <a href="${INLINE_VARS.ITEM_READ_MORE_URL}" style="color: #333333; text-decoration: none; font-size: 20px; vertical-align: top; float: right;">
                        <img class="service-logo" style="width: 45px; border-radius: 3px; height: 45px;" src="${INLINE_VARS.ITEM_RIGHT_IMAGE_URL}"/>
                    </a>
                </div>`;

const BODY_LIST_ITEM_NO_IMAGE_COMPONENT_TEMPLATE = "<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class=\"mcnButtonBlock\" width=\"100%\" style=\"background-color: #FFFFFF\">\n" +
    "    <tbody class=\"mcnButtonBlockOuter\">\n" +
    "        <tr>\n" +
    "            <td style=\"padding-top:14px;padding-right:18px;padding-bottom:14px;padding-left:18px;border-collapse:collapse;color:#606060;font-family:Helvetica;font-size:15px;line-height:150%;text-align:left;border-bottom: 1px solid #EAEAEA\" valign=\"top\">\n" +
    "                <a style=\"color: #333333; font-weight: bold; text-decoration:none;\" href=\"" + INLINE_VARS.ITEM_READ_MORE_URL + "\">" + INLINE_VARS.ITEM_TITLE + "</a>\n" +
    "                <div style=\"font-size: 11px;line-height: 15px;color: grey;font-family: arial,helvetica,sans-serif;\">\n" +
    "                    " + INLINE_VARS.ITEM_SUBTITLE + "\n" +
    "                </div>\n" +
    "                " + INLINE_VARS.ITEM_SUMMARY + "\n" +
    "            </td>\n" +
    "        </tr>\n" +
    "    </tbody>\n" +
    "</table>";

const BODY_LIST_ITEM_ONLY_TITLE_TEMPLATE = "<div style=\"text-align: center;font-family: arial,helvetica,sans-serif;font-size: 16px;\"><u><a href=\"" + INLINE_VARS.ITEM_READ_MORE_URL + "\" style=\"color: #000000;text-decoration: none;\"><span style=\"color:#000000;\"><strong> " + INLINE_VARS.ITEM_TITLE + "</strong></span></a></u></span></div>";

const BODY_LIST_TITLE_COMPONENT_TEMPLATE = "<table class=\"module\" role=\"module\" data-type=\"text\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"table-layout: fixed;\">\n" +
    "                                                            <tr>\n" +
    "                                                                <td style=\"background-color:#ffffff;padding:18px 18px 18px 18px;line-height:22px;text-align:inherit;\" height=\"100%\" valign=\"top\" bgcolor=\"#ffffff\">\n" +
    "                                                                    <h1 style=\"color: #333333;\"><span style=\"font-size:28px;\">" + INLINE_VARS.BODY_LIST_TITLE + "</span></h1>\n" +
    "                                                                </td>\n" +
    "                                                            </tr>\n" +
    "                                                        </table>";


const BODY_BOTTOM_SOCIALNETWORKS_CONTAINER_TEMPLATE = "<table class=\"module\" role=\"module\" data-type=\"social\" align=\"center\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"table-layout: fixed;\">\n" +
    "    <tbody>\n" +
    "        <tr>\n" +
    "            <td valign=\"top\" style=\"padding:0px 0px 0px 0px;font-size:6px;line-height:10px;\">\n" +
    "                <table align=\"center\">\n" +
    "                    <tbody>\n" +
    "                        <tr>\n" +
    "                            " + INLINE_VARS.BODY_SOCIALNETWORKS_CONTAINER + "\n" +
    "                        </tr>\n" +
    "                    </tbody>\n" +
    "                </table>\n" +
    "            </td>\n" +
    "        </tr>\n" +
    "    </tbody>\n" +
    "</table>";

const BODY_BOTTOM_SOCIALNETWORK_ITEM_TEMPLATE = "<td style=\"padding: 0px 5px;\">\n" +
    "    <a role=\"social-icon-link\" href=\"" + INLINE_VARS.BODY_SOCIALNETWORK_ITEM_LINK + "\" target=\"_blank\" alt=\"LinkedIn\" data-nolink=\"false\" style=\"-webkit-border-radius: 2px;-moz-border-radius: 2px;border-radius: 2px;display: inline-block;background-color: transparent;color: #333333;text-decoration: none;\">\n" +
    "        <img role=\"social-icon\" alt=\"LinkedIn\" title=\"LinkedIn \" height=\"30\" width=\"30\" style=\"height: 30px, width: 30px\" src=\"" + INLINE_VARS.BODY_SOCIALNETWORK_ITEM_ICONURL + "\">\n" +
    "    </a>\n" +
    "</td>";

const FOOTER_CONTAINER_TEMPLATE = "<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" align=\"center\" width=\"100%\" role=\"module\" data-type=\"columns\" data-version=\"2\" style=\"padding:48px 34px 17px 34px;background-color:#021531;box-sizing:border-box;\" bgcolor=\"#021531\">\n" +
    "    <tr role=\"module-content\">\n" +
    "        <td height=\"100%\" valign=\"top\">\n" +
    "            <table width=\"266.000\" style=\"width:266.000px;border-spacing:0;border-collapse:collapse;margin:0px 0px 0px 0px;\" cellpadding=\"0\" cellspacing=\"0\" align=\"left\" border=\"0\" bgcolor=\"#021531\" class=\"column column-0 of-2\n" +
    "                empty\">\n" +
    "                <tr>\n" +
    "                    <td style=\"padding:0px;margin:0px;border-spacing:0;\">\n" +
    "\n" +
    "                        <table class=\"module\" role=\"module\" data-type=\"text\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"table-layout: fixed;\">\n" +
    "                            <tr>\n" +
    "                                <td style=\"padding:0px 0px 0px 0px;background-color:#021531;\" height=\"100%\" valign=\"top\" bgcolor=\"#021531\">\n" +
    "                                    <div style=\"font-size: 10px;line-height: 150%;margin: 0px;font-family: arial,helvetica,sans-serif;\">\n" +
    "                                        <a target='_blank' href=\"" + INLINE_VARS.FOOTER_UNSUBSCRIBE_URL + "\" style=\"color: #021531;text-decoration: none;\"><span style=\"color:#FFFFFF;\">Unsubscribe</span></a>\n" +
    "                                    </div>\n" +
    "                                </td>\n" +
    "                            </tr>\n" +
    "                        </table>\n" +
    "\n" +
    "                    </td>\n" +
    "                </tr>\n" +
    "            </table>\n" +
    "\n" +
    "            <table width=\"266.000\" style=\"width:266.000px;border-spacing:0;border-collapse:collapse;margin:0px 0px 0px 0px;\" cellpadding=\"0\" cellspacing=\"0\" align=\"left\" border=\"0\" bgcolor=\"#021531\" class=\"column column-1 of-2\n" +
    "                empty\">\n" +
    "                <tr>\n" +
    "                    <td style=\"padding:0px;margin:0px;border-spacing:0;\">\n" +
    "\n" +
    "                        <table class=\"module\" role=\"module\" data-type=\"text\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"table-layout: fixed;\">\n" +
    "                            <tr>\n" +
    "                                <td style=\"padding:0px 0px 0px 0px;background-color:#021531;\" height=\"100%\" valign=\"top\" bgcolor=\"#021531\">\n" +
    "                                    <div style=\"font-size: 10px;line-height: 150%;margin: 0px;text-align: right;font-family: arial,helvetica,sans-serif;\">\n" +
    "                                        <span style=\"color:#ffffff;\">" + INLINE_VARS.FOOTER_COMPANY_EMAIL + "</span>\n" +
    "                                    </div>\n" +
    "                                    <div style=\"font-size: 10px;line-height: 150%;margin: 0px;text-align: right;font-family: arial,helvetica,sans-serif;\">\n" +
    "                                        <span style=\"color:#ffffff;\">" + INLINE_VARS.FOOTER_COMPANY_ADDRESS + "</span>\n" +
    "                                    </div>\n" +
    "                                </td>\n" +
    "                            </tr>\n" +
    "                        </table>\n" +
    "                    </td>\n" +
    "                </tr>\n" +
    "            </table>\n" +
    "        </td>\n" +
    "    </tr>\n" +
    "</table>";


const EMAIL_BODY_CONTENT_TEMPLATE = "\n" +
    "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Strict//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd\">\n" +
    "<html data-editor-version=\"2\" class=\"sg-campaigns\" xmlns=\"http://www.w3.org/1999/xhtml\">\n" +
    "<head>\n" +
    "    <meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\">\n" +
    "    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1\"><!--[if !mso]><!-->\n" +
    "    <meta http-equiv=\"X-UA-Compatible\" content=\"IE=Edge\"><!--<![endif]-->\n" +
    "    <!--[if (gte mso 9)|(IE)]>\n" +
    "    <xml>\n" +
    "        <o:OfficeDocumentSettings>\n" +
    "            <o:AllowPNG/>\n" +
    "            <o:PixelsPerInch>96</o:PixelsPerInch>\n" +
    "        </o:OfficeDocumentSettings>\n" +
    "    </xml>\n" +
    "    <![endif]-->\n" +
    "    <!--[if (gte mso 9)|(IE)]>\n" +
    "    <style type=\"text/css\">\n" +
    "        body {width: 600px;margin: 0 auto;}\n" +
    "        table {border-collapse: collapse;}\n" +
    "        table, td {mso-table-lspace: 0pt;mso-table-rspace: 0pt;}\n" +
    "        img {-ms-interpolation-mode: bicubic;}\n" +
    "    </style>\n" +
    "    <![endif]-->\n" +
    "\n" +
    "\n" +
    "    <!--user entered Head Start-->\n" +
    "\n" +
    "    <!--End Head user entered-->\n" +
    "</head>\n" +
    "<body style=\"font-family: arial,helvetica,sans-serif;font-size: 16px;color: #333333;\">\n" +
    "<center class=\"wrapper\" data-link-color=\"#333333\" data-body-style=\"font-size: 16px; font-family: arial,helvetica,sans-serif; color: #333333; background-color: #f5f8fa;\">\n" +
    "    <div class=\"webkit\" style=\"font-family: arial,helvetica,sans-serif;font-size: 16px;\">\n" +
    "        <table cellpadding=\"0\" cellspacing=\"0\" border=\"0\" width=\"100%\" class=\"wrapper\" bgcolor=\"#f5f8fa\" style=\"table-layout: fixed;-webkit-font-smoothing: antialiased;-webkit-text-size-adjust: 100%;-moz-text-size-adjust: 100%;-ms-text-size-adjust: 100%;width: 100% !important;\">\n" +
    "            <tr>\n" +
    "                <td valign=\"top\" bgcolor=\"#f5f8fa\" width=\"100%\">\n" +
    "                    <table width=\"100%\" role=\"content-container\" class=\"outer\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\">\n" +
    "                        <tr>\n" +
    "                            <td width=\"100%\">\n" +
    "                                <table width=\"100%\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\">\n" +
    "                                    <tr>\n" +
    "                                        <td>\n" +
    "                                            <table width=\"100%\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\" style=\"width: 100%; max-width:600px;\" align=\"center\">\n" +
    "                                                <tr>\n" +
    "                                                    <td role=\"modules-container\" style=\"padding: 0px 0px 0px 0px; color: #333333; text-align: left;\" bgcolor=\"#f5f8fa\" width=\"100%\" align=\"left\">\n" +
    "                                                        " + INLINE_VARS.EMAIL_BODY_CONTENT + "\n" +
    "                                                    </td>\n" +
    "                                                </tr>\n" +
    "                                            </table>\n" +
    "                                        </td>\n" +
    "                                    </tr>\n" +
    "                                </table>\n" +
    "                            </td>\n" +
    "                        </tr>\n" +
    "                    </table>\n" +
    "                </td>\n" +
    "            </tr>\n" +
    "        </table>\n" +
    "    </div>\n" +
    "</center>\n" +
    "</body>\n" +
    "</html>"