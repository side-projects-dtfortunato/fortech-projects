import {WEBAPP_CONFIGS} from "../../../configs/webapp.configs";
import {AwsEmailServiceUtils} from "../aws-email-service.utils";
import {RealtimeDbUtils} from "../realtime-db.utils";
import {SharedFirestoreCollectionDB} from "../../model-data/shared-firestore-collections";
import {ApiResponse} from "../../model-data/api-response.model";
import axios from "axios";

var unirest = require('unirest');


export interface EmailDestinationModel {
    uid: string,
    email: string,
    name?: string,
}


export class MailerSendUtils {
    static async sendEmail(params: { destinationsEmail: EmailDestinationModel[], bodyHtml: string, subject: string, sentFrom: { name: string, email: string} }) {

        let counterSent: number = 0;
        for (let destinationEmail of params.destinationsEmail) {
            if (counterSent === 12) {
                // Wait 1 second because of the 14 emails limit per second
                await new Promise(resolve => setTimeout(resolve, 1200));
                counterSent = 0;
            }
            counterSent ++;

            // Send email
            try {
                const awsResponse = await AwsEmailServiceUtils.sendDigestEmail({
                    recipientEmail: destinationEmail.email,
                    recipientName: destinationEmail.name ? destinationEmail.name : "",
                    senderName: params.sentFrom.name,
                    sentFromEmail: params.sentFrom.email,
                    content: params.bodyHtml.replace(WEBAPP_CONFIGS.UNSUBSCRIBE_URL, WEBAPP_CONFIGS.UNSUBSCRIBE_URL + destinationEmail.uid),
                    subject: params.subject,
                });
                if (!awsResponse.isSuccess) {
                    console.error(awsResponse);
                }

            } catch (e) {
                console.error(e);
            }
        }

        /*let listEmailParams: EmailParams[] = [];

        for (let i = 0; i < params.destinationsEmail.length; i++) {
            const destionationItem: EmailDestinationModel = params.destinationsEmail[i];

            const sentFrom = new Sender(params.sentFrom.email, params.sentFrom.name);

            const recipients: Recipient[] = [new Recipient(destionationItem.email, destionationItem.name)];

            const emailParams = new EmailParams()
                .setFrom(sentFrom)
                .setTo(recipients)
                .setSubject(params.subject)
                .setHtml(params.bodyHtml.replace(WEBAPP_CONFIGS.UNSUBSCRIBE_URL, WEBAPP_CONFIGS.UNSUBSCRIBE_URL + destionationItem.uid));

            listEmailParams.push(emailParams);
        }

        if (listEmailParams.length > 0) {
            if (listEmailParams.length === 1) {
                const response = await mailerSend.email.send(listEmailParams[0]);
                return response;
            } else {
                const response = await mailerSend.email.sendBulk(listEmailParams);
                return response;
            }
        }*/

        return null;
    }

    /**
     * Project: https://parse-dashboard.back4app.com/apps/134dece9-6f43-4faf-b643-3d9832f95ec5/cloud_code
     */
    static async verifyEmail(email?: string): Promise<boolean> {
        if (!email) {
            return false;
        }
        const url = "https://fortulyapps.up.railway.app/verifyEmail";
        let response: ApiResponse<{isEmailValid: boolean}> = (await axios.get<ApiResponse<{isEmailValid: boolean}>>(url, {
            params: { email },
        })).data;
        return response.responseData ? response.responseData?.isEmailValid : true;
    }
}


