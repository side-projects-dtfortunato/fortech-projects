import * as OneSignal from '@onesignal/node-onesignal';
import {WEBAPP_CONFIGS} from "../../configs/webapp.configs";

// npm install @onesignal/node-onesignal --save
const configs = OneSignal.createConfiguration({
    userAuthKey: WEBAPP_CONFIGS.ONESIGNAL_CONFIGS.appId,
    restApiKey: WEBAPP_CONFIGS.ONESIGNAL_CONFIGS.apiKey,
});
const client = new OneSignal.DefaultApi(configs);

export class OnesignalUtils {
    static async sendPushNotification(
        notificationTitle: string,
        notificationMessage: string,
        dataOpenUrl: string,
        date?: Date // Optional parameter for scheduling
    ) {
        if (!client) {
            return;
        }

        // Create a notification object with the required properties
        const notification = new OneSignal.Notification();
        notification.app_id = WEBAPP_CONFIGS.ONESIGNAL_CONFIGS.appId;
        notification.headings = { en: notificationTitle };
        notification.contents = { en: notificationMessage };
        notification.data = {
            openUrl: dataOpenUrl,
        };

        if (WEBAPP_CONFIGS.CUSTOM_CONFIGS.openPushInApp === false) {
            notification.url = dataOpenUrl;
        }
        notification.included_segments = ["All"];

        // If a date is provided, set the send_after property
        if (date) {
            notification.send_after = date.toISOString();
        }

        // Send the notification using the client object
        try {
            const response = await client.createNotification(notification);
            console.log('Notification sent successfully:', response);
        } catch (error) {
            console.error('Error sending notification:', error);
        }
    }

    static async sendPushNotificationToExternalUserIds(title: string,
        message: string,
        dataOpenUrl: string,
        externalUserIds: string[]) {
        if (!client) {
            return;
        }

        const notification = new OneSignal.Notification();
        notification.app_id = WEBAPP_CONFIGS.ONESIGNAL_CONFIGS.appId;
        notification.headings = { 
            en: title,
        };
        notification.contents = {
            en: message,
        };
        notification.data = {
            openUrl: dataOpenUrl,
        };
        if (WEBAPP_CONFIGS.CUSTOM_CONFIGS.openPushInApp === false) {
            notification.url = dataOpenUrl;
        }
        
        // Set the include_external_user_ids property
        // notification.include_external_user_ids = externalUserIds;
        notification.include_aliases = {
            external_id: externalUserIds,
        };
        notification.target_channel = "push";

        try {
            const response = await client.createNotification(notification);
            console.log('Notification sent successfully to external users:', response);
        } catch (error) {
            console.error('Error sending notification to external users:', error);
        }
    }

    static async sendPushNotificationToUserJobFilters(
        title: string,
        message: string,
        dataOpenUrl: string,
        filters: {category?: string, remoteRegion?: string},
        probabilityToAllUsers: number = 0.5
    ) {
        const headers = {
            'Content-Type': 'application/json',
            'Authorization': `Basic ${WEBAPP_CONFIGS.ONESIGNAL_CONFIGS.apiKey}`,
        };

        const filterArray: any[] = [];

        if (filters.category && filters.remoteRegion) {
            // Group 1: Users with both matching tags
            filterArray.push(
                {"field": "tag", "key": "category", "relation": "=", "value": filters.category},
                {"operator": "AND"},
                {"field": "tag", "key": "remoteRegion", "relation": "=", "value": filters.remoteRegion},

                {"operator": "OR"},

                // Group 2: Users with matching category but no remoteRegion tag
                {"field": "tag", "key": "category", "relation": "=", "value": filters.category},
                {"operator": "AND"},
                {"field": "tag", "key": "remoteRegion", "relation": "not_exists"},

                {"operator": "OR"},

                // Group 3: Users with matching remoteRegion but no category tag
                {"field": "tag", "key": "category", "relation": "not_exists"},
                {"operator": "AND"},
                {"field": "tag", "key": "remoteRegion", "relation": "=", "value": filters.remoteRegion},
            );

            // Add probability to send to all users
            if (Math.random() < probabilityToAllUsers) {
                filterArray.push(
                    {"operator": "OR"},

                    // Group 4: Users with no filters selected
                    {"field": "tag", "key": "category", "relation": "not_exists"},
                    {"operator": "AND"},
                    {"field": "tag", "key": "remoteRegion", "relation": "not_exists"},
                );
            }
        }

        const notificationPayload: any = {
            app_id: WEBAPP_CONFIGS.ONESIGNAL_CONFIGS.appId,
            headings: { en: title },
            contents: { en: message },
            data: { openUrl: dataOpenUrl },
        };

        if (WEBAPP_CONFIGS.CUSTOM_CONFIGS.openPushInApp === false) {
            notificationPayload.url = dataOpenUrl;
        }

        // If filters are provided, use them. Otherwise, target all subscribed users
        if (filterArray.length > 0) {
            notificationPayload.filters = filterArray;
        } else {
            notificationPayload.included_segments = ["Subscribed Users"];
        }

        try {
            const response = await fetch('https://onesignal.com/api/v1/notifications', {
                method: 'POST',
                headers: headers,
                body: JSON.stringify(notificationPayload)
            });

            const responseData = await response.json();

            if (!response.ok) {
                throw new Error(`HTTP error! status: ${response.status}, message: ${JSON.stringify(responseData)}`);
            }

            console.log('REST API - Filtered notification sent successfully:', responseData);
            return responseData;
        } catch (error) {
            console.error('REST API - Error sending filtered notification:', error);
            throw error;
        }
    }

    static async sendPushNotificationToExternalUserIdsREST(
        title: string,
        message: string,
        dataOpenUrl: string,
        externalUserIds: string[]
    ) {
        const headers = {
            'Content-Type': 'application/json',
            'Authorization': `Basic ${WEBAPP_CONFIGS.ONESIGNAL_CONFIGS.apiKey}`,
        };

        const notificationPayload: any = {
            app_id: WEBAPP_CONFIGS.ONESIGNAL_CONFIGS.appId,
            headings: { en: title },
            contents: { en: message },
            data: { openUrl: dataOpenUrl },
            include_aliases: {
                external_id: externalUserIds
            },
            target_channel: "push"
        };

        if (WEBAPP_CONFIGS.CUSTOM_CONFIGS.openPushInApp === false) {
            notificationPayload.url = dataOpenUrl;
        }

        try {
            const response = await fetch('https://onesignal.com/api/v1/notifications', {
                method: 'POST',
                headers: headers,
                body: JSON.stringify(notificationPayload)
            });

            const responseData = await response.json();

            if (!response.ok) {
                throw new Error(`HTTP error! status: ${response.status}, message: ${JSON.stringify(responseData)}`);
            }

            console.log('REST API - Notification sent successfully to external users:', responseData);
            return responseData;
        } catch (error) {
            console.error('REST API - Error sending notification to external users:', error);
            throw error;
        }
    }
}
