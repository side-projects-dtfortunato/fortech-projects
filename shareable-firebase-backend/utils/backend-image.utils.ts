import axios from 'axios';
import { createCanvas, loadImage, Canvas } from 'canvas';
import {WEBAPP_CONFIGS} from "../../configs/webapp.configs";

interface ThumbnailProps {
    title: string;
    bgImageUrl: string;
    category?: string;
}

export class BackendImageUtils {

    static async downloadImage(url: string): Promise<Buffer> {
        const response = await axios({
            url,
            method: 'GET',
            responseType: 'arraybuffer',
        });
        return Buffer.from(response.data, 'binary');
    }

    static async generateThumbnail({ title, bgImageUrl, category = "" }: ThumbnailProps): Promise<string> {
        const canvas: Canvas = createCanvas(1080, 1080);
        const context = canvas.getContext('2d');

        const logoUrl = WEBAPP_CONFIGS.WEBSITE_URL + '/images/logo-white.png';

        // Download images
        const bgImageBuffer = await this.downloadImage(bgImageUrl);
        const logoImageBuffer = await this.downloadImage(logoUrl);

        // Load images from buffers
        const backgroundImage = await loadImage(bgImageBuffer);
        const logoImage = await loadImage(logoImageBuffer);

        const { width, height } = canvas;

        // Center crop the background image
        const imgWidth = backgroundImage.width;
        const imgHeight = backgroundImage.height;
        const aspectRatio = imgWidth / imgHeight;
        let srcX = 0, srcY = 0, srcWidth = imgWidth, srcHeight = imgHeight;

        if (aspectRatio > 1) {
            // Landscape image
            srcWidth = imgHeight;
            srcX = (imgWidth - imgHeight) / 2;
        } else if (aspectRatio < 1) {
            // Portrait image
            srcHeight = imgWidth;
            srcY = (imgHeight - imgWidth) / 2;
        }

        context.drawImage(backgroundImage, srcX, srcY, srcWidth, srcHeight, 0, 0, width, height);

        // Add a gradient overlay from bottom to top
        const gradient = context.createLinearGradient(0, height, 0, 0);
        gradient.addColorStop(0, 'rgba(0, 0, 0, 0.7)');
        gradient.addColorStop(1, 'rgba(0, 0, 0, 0)');
        context.fillStyle = gradient;
        context.fillRect(0, 0, width, height);

        // Set text properties for the title
        const titleFontSize = 80;
        const categoryFontSize = 60;
        context.font = `bold ${titleFontSize}px 'Roboto', sans-serif`;
        context.textAlign = 'center';

        const maxWidth = width - 40; // Only horizontal margin
        const lineHeight = titleFontSize + 10;
        const x = width / 2;
        let y = height / 2 + lineHeight;

        // Function to wrap text and paint 50% of lines at the bottom with color
        const wrapText = (context: any, text: string, x: number, y: number, maxWidth: number, lineHeight: number): number => {
            const words = text.split(' ');
            let line = '';
            const lines: string[] = [];

            for (let n = 0; n < words.length; n++) {
                const testLine = line + words[n] + ' ';
                const metrics = context.measureText(testLine);
                const testWidth = metrics.width;
                if (testWidth > maxWidth && n > 0) {
                    lines.push(line);
                    line = words[n] + ' ';
                } else {
                    line = testLine;
                }
            }
            lines.push(line);

            // Paint 50% of lines at the bottom with color
            const coloredLinesCount = Math.ceil(lines.length / 2);

            for (let k = 0; k < lines.length; k++) {
                const textY = y - (lines.length - 1 - k) * lineHeight;
                if (k >= lines.length - coloredLinesCount) {
                    // Bottom lines painted with color
                    context.fillStyle = '#FCB249';
                } else {
                    // Top lines painted with white
                    context.fillStyle = 'white';
                }
                context.fillText(lines[k], x, textY);
            }

            return lines.length * lineHeight;
        };

        const titleHeight = wrapText(context as any, title, x, y, maxWidth, lineHeight);
        y += titleHeight / 2;

        // Set text properties for the category
        context.font = `lighter ${categoryFontSize}px 'Roboto', sans-serif`;
        context.fillStyle = 'white';

        // Draw the category text below the title
        context.fillText(category, x, y + lineHeight);

        // Draw the logo in the top right corner, maintaining aspect ratio
        const logoMaxHeight = 80;
        const logoAspectRatio = logoImage.width / logoImage.height;
        const logoWidth = logoMaxHeight * logoAspectRatio;
        const logoHeight = logoMaxHeight;

        const logoX = width - logoWidth - 20;
        const logoY = 20;

        context.drawImage(logoImage, logoX, logoY, logoWidth, logoHeight);

        // Return the canvas as a base64 string
        return canvas.toDataURL('image/jpeg');
    }




}
