import {WEBAPP_CONFIGS} from "../../configs/webapp.configs";

export class BackendRouteUtils {

    static getArticleDetails(articleId: string) {
        return WEBAPP_CONFIGS.WEBSITE_URL + "/article/" + articleId;
    }

    static getCommunityPage() {
        return WEBAPP_CONFIGS.WEBSITE_URL + "/community";
    }

    static getCommunityDetailsPage(commentId: string) {
        return WEBAPP_CONFIGS.WEBSITE_URL + "/community/" + commentId;
    }

    static getJobPostDetailsPage(jobId: string) {
        return WEBAPP_CONFIGS.WEBSITE_URL + "/job/" + jobId;
    }

    static getJobCategoryPage(category: string) {
        return WEBAPP_CONFIGS.WEBSITE_URL + "/job/category/" + category;
    }
}