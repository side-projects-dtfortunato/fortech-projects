import {GoogleGenerativeAI} from "@google/generative-ai";

export class AIGeminiUtils {
    static genAI = new GoogleGenerativeAI("AIzaSyAfuoqxQj5uIWGZMNUWvCojIiLXbI0p-8g");
    static model = this.genAI.getGenerativeModel({
        model: "gemini-1.5-flash" // "gemini-1.0-pro-latest"
    });


    static async generateAIContent(prompt: string): Promise<string> {
        const result = await this.model.generateContent(prompt);
        const response = await result.response;
        let text = response.text().trim();
        return text;
    }

    static async summarizeJsonArticle(jsonContent: string): Promise<{summary: string, highlightsBullets: string[], relevancy: number, tags: string[]} | undefined> {
        let promptBuild: string = "You are an assistant that reads an article from a JSON string and summarize the content of the article and respond in the next JSON Scheme (without any prefix or suffix): {summary: string // summary of the article with the most important things of the article content, highlightsBullets: string[] // list of bullets with the highlights of the article, relevancy: number // give a relevancy value regarding the content of the article, in a scale of 1 to 10, where 10 is the maximum of relevancy, tags: string[] // 5 tags without any spaces}\n";
        promptBuild += `Summarize the following article in this JSON object:\n${jsonContent}\n\n**TL;DR:**`

        const result = await this.model.generateContent(promptBuild);
        const response = await result.response;

        try {
            return JSON.parse(response.text().trim());
        } catch (e) {
            return undefined;
        }
    }

    static async tagJsonContent(jsonContent: string): Promise<{tags: string[]}> {
        let promptBuild: string = "You are an assistant that reads an article from a JSON string and generate tags related with the content and respond in the next JSON Scheme (without any prefix or suffix): {tags: string[] // sorted by relevancy}\n";
        promptBuild += `Generated tags to categorize this content: \n${jsonContent}\n\n**TL;DR:**`

        const result = await this.model.generateContent(promptBuild);
        const response = await result.response;
        try {
            return JSON.parse(response.text().trim());
        } catch (e) {
            return {tags: []};
        }
    }

}