import * as AWS from 'aws-sdk';
import {getQuickResponse} from "../model-data/api-response.model";
import {WEBAPP_CONFIGS} from "../../configs/webapp.configs";

const SES_CONFIG = {
    accessKeyId: "AKIA5CBDRNO6FPCAC65P",
    secretAccessKey: "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",
    region: "eu-north-1",
};

const AWS_SES = new AWS.SES(SES_CONFIG);



export class AwsEmailServiceUtils {
    static async sendDigestEmail(params: {
        recipientEmail: string,
        recipientName: string,
        subject: string,
        content: string,
        senderName: string,
        sentFromEmail?: string,
    }) {
        const domain = WEBAPP_CONFIGS.WEBSITE_URL.replace("https://", "");
        const sentFrom: string = params.sentFromEmail ? params.sentFromEmail : `no-reply@${domain}`;
        const emailParams: any = {
            Source: `"${params.senderName}" <${sentFrom}>`,
            Destination: {
                ToAddresses: [`"${params.recipientName}" <${params.recipientEmail}>`],
            },
            ReplyToAddresses: [`"${params.senderName}" <contact@${domain}>`],
            Message: {
                Subject: { Data: params.subject },
                Body: { Html: { Data: params.content } },
            },
        };

        try {
            const result = await AWS_SES.sendEmail(emailParams).promise();
            console.log("AWS Result: ", result);
            console.log('Digest email sent successfully:', result.MessageId);
            return getQuickResponse(true, result.MessageId, "Digest email sent successfully");
        } catch (error) {
            console.error('Error sending digest email:', error);
            return getQuickResponse(false, null, "Error sending digest email");
        }
    }
}
