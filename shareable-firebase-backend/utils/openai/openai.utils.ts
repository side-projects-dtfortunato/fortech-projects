import OpenAI from "openai";

const openai = new OpenAI({apiKey: "sk-proj-yXuh9z7RX18btdFpwR5BT3BlbkFJwt9LZYv3okTdSL63RHwj"});

export class OpenaiUtils {

    static async summarizeArticle(articleContent: string): Promise<any> {
        const response = await openai.chat.completions.create({
            messages: [
                { role: "system", content: "You are an assistant that reads an article from a JSON object and summarize the content of the article and respond it as an JSON object (only, to directly parse the response string) with this scheme: {summary: string, relevancy: number // give a relevancy value in a scale of 1 to 10 where 10 is the maximum of relevancy, tags: string[] // 5 max}"},
                { role: "user", content: `Summarize the following article in this JSON object:\n${articleContent}\n\n**TL;DR:**`}],
            model: "gpt-3.5-turbo-0125",
            max_tokens: 500,
            temperature: 0.7,
            response_format: { type: "json_object"},
            });

        const summary = response.choices[0].message.content!.trim(); // Extract and format summary

        return summary;
    }

    static async generateJsonContent(assistantPrompt: string, userPrompt: string): Promise<any> {
        const response = await openai.chat.completions.create({
            messages: [
                { role: "system", content: assistantPrompt},
                { role: "user", content: userPrompt}],
            model: "gpt-4o-mini",
            temperature: 0.6,
            response_format: { type: "json_object"},
            });

        const summary = response.choices[0].message.content!.trim(); // Extract and format summary

        return summary;
    }
}