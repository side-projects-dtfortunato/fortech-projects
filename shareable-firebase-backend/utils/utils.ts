import * as admin from "firebase-admin";
import {BaseModel} from "../model-data/base.model";

export async function getDocumentData<T extends BaseModel>(collectionId: string, docId: string, forceUidOnObject?: boolean): Promise<T> {
    let dataDoc: any = null;
    const docRef = await admin.firestore().collection(collectionId).doc(docId).get();

    if (docRef && docRef.exists) {
        dataDoc = {
            ...docRef.data() as T,
        };
        if (forceUidOnObject) {
            dataDoc["uid"] = docId;
        }
    }

    return dataDoc;
}

export async function getAllDocuments<T>(collectionName: string): Promise<T[]> {
    const querySnapshot = await admin.firestore()
        .collection(collectionName).get();

    if (querySnapshot.empty) {
        return [];
    } else {
        return querySnapshot.docs.map((doc) => doc.data() as T);
    }
}

export async function getDocumentsWithPagination(
    collectionPath: string,
    pageSize: number,
    fieldOrderBy: string
): Promise<any[]> {
    try {
        const documents = await admin.firestore()
            .collection(collectionPath)
            .orderBy(fieldOrderBy, "desc")
            .limit(pageSize).get();

        return documents.empty ? [] : documents.docs.map((doc) => doc.data());
    } catch (error) {
        console.error('Error fetching documents:', error);
        // Handle error (e.g., show a notification to the user)
        return [];
    }
}

export async function getRecentDocuments(
    collectionPath: string,
    hoursAgo: number,
    pageSize: number,
    fieldOrderBy: string = 'createdAt'
): Promise<any[]> {
    try {
        // Calculate the timestamp for X hours ago
        const xHoursAgo = Date.now() - hoursAgo * 60 * 60 * 1000;

        const documents = await admin.firestore()
            .collection(collectionPath)
            .where('createdAt', '>', xHoursAgo)
            .orderBy(fieldOrderBy, 'desc')
            .limit(pageSize)
            .get();

        return documents.empty ? [] : documents.docs.map((doc) => doc.data());
    } catch (error) {
        console.error('Error fetching recent documents:', error);
        // Handle error (e.g., show a notification to the user)
        return [];
    }
}

export async function getDocumentDataFromPath<T extends BaseModel>(path: { collectionId: string, docId: string }[], forceUidOnObject?: boolean): Promise<T> {
    let dataDoc: any = null;
    let docCursor: any;

    for (let i = 0; i < path.length; i++) {
        const pathItem = path[i];
        if (!docCursor) {
            docCursor = admin.firestore().collection(pathItem.collectionId).doc(pathItem.docId).get();
        } else {
            docCursor = docCursor.collection(pathItem.collectionId).doc(pathItem.docId);
        }
    }

    let docRef: any = await docCursor.get();

    if (docRef && docRef.exists) {
        dataDoc = {
            ...docRef.data() as T,
        };
        if (forceUidOnObject) {
            dataDoc["uid"] = docRef.id;
        }
    }

    return dataDoc;
}

export function shuffleArray(array: any[]) {
    array.sort(() => Math.random() - 0.5);
}

export function getRandomString(length: number): string {
    let randomChars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    let result: string = '';
    for (var i = 0; i < length; i++) {
        result += randomChars.charAt(Math.floor(Math.random() * randomChars.length));
    }
    return result;
}




