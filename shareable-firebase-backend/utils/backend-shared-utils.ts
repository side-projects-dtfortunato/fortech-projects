import axios from 'axios';
import * as xml2js from 'xml2js';
import * as admin from "firebase-admin";
import {PRODUCTION_GCLOUD_PROJECT, WEBAPP_CONFIGS} from "../../configs/webapp.configs";
import {getColorFromURL, getPaletteFromURL, Palette} from "color-thief-node";
import * as https from "https";
import * as http from "http";


export class BackendSharedUtils {

    static getDomain(url: string, subdomain: boolean = false): string {
        // Remove protocol and www (if present)
        url = url.replace(/(https?:\/\/)?(www\.)?/i, '');

        // Split by slashes to get the domain
        const parts = url.split('/');
        const domain = parts[0];

        // Optionally include subdomain
        if (subdomain) {
            return domain;
        } else {
            // Remove subdomain (if any)
            const domainParts = domain.split('.');
            return domainParts.slice(-2).join('.');
        }
    }

    static string_to_slug(str: string) {
        let slugStr = str.replace(/^\s+|\s+$/g, ''); // trim
        slugStr = slugStr.toLowerCase();

        // remove accents, swap ñ for n, etc
        var from = "àáäâèéëêìíïîòóöôùúüûñç·/_,:;";
        var to = "aaaaeeeeiiiioooouuuunc------";
        for (var i = 0, l = from.length; i < l; i++) {
            slugStr = slugStr.replace(new RegExp(from.charAt(i), 'g'), to.charAt(i));
        }

        slugStr = slugStr.replace(/[^a-z0-9 -]/g, '') // remove invalid chars
            .replace(/\s+/g, '-') // collapse whitespace and replace by -
            .replace(/-+/g, '-'); // collapse dashes

        return slugStr;
    }

    static slug_to_title(slug: string) {
        var words = slug.split('-');

        for (var i = 0; i < words.length; i++) {
            var word = words[i];
            words[i] = word.charAt(0).toUpperCase() + word.slice(1);
        }

        return words.join(' ');
    }

    static isLessThanHoursAgo(dateInMilliseconds: number, hoursAgo: number): boolean {
        const now = Date.now();
        const date = new Date(dateInMilliseconds);
        const delta = now - date.getTime();

        return delta <= hoursAgo * 60 * 60 * 1000; // 24 hours in milliseconds
    }

    static isDateLessThanHoursAgo(date: Date, hoursAgo: number): boolean {
        const now = Date.now();
        const delta = now - date.getTime();

        return delta <= hoursAgo * 60 * 60 * 1000; // 24 hours in milliseconds
    }

    static hasStringInArray(arrValues: string[], valueToFind: string) {
        return arrValues.find((listItem) => {
            if (listItem.toLowerCase().includes(valueToFind)) {
                return true;
            } else {
                return;
            }
        }) !== undefined;
    }

    static convertWordsToUpperCaseAndReplaceUnderscoreByTrace(words: string): string {
        const wordsArray = words.split(" ");
        const convertedWords = wordsArray.map((word) => {
            const convertedWord = word.charAt(0).toUpperCase() + word.slice(1);
            return convertedWord.replace("_", "-");
        });
        return convertedWords.join(" ");
    }

    static getQueryParamValue(url: string, param: string): string | null {
        const queryParams = new URLSearchParams(new URL(url).search);
        return queryParams.get(param);
    }

    static async loadImageToBase64(imageUrl: string): Promise<string> {
        try {
            const response = await axios.get(imageUrl, {responseType: 'arraybuffer'});
            const data = Buffer.from(response.data, 'binary').toString('base64');
            return `data:${response.headers['content-type']};base64,${data}`;
        } catch (error) {
            console.error('Error loading image:', error);
            throw error;
        }
    }

    static replaceQParamOnURL(originalUrl: string, qparam: string, newValue: string) {
        try {
            if (!originalUrl.startsWith("http")) {
                originalUrl = "https://" + originalUrl;
            }
            let href: URL = new URL(originalUrl);
            href.searchParams.set(qparam, newValue);
            return href.toString();
        } catch (e) {
            return originalUrl;
        }
    }

    static addLinkSource(originalUrl: string) {
        let updatedUrl: string = this.replaceQParamOnURL(originalUrl, "utm_source", WEBAPP_CONFIGS.SITE_NAME);
        updatedUrl = this.replaceQParamOnURL(updatedUrl, "ref", WEBAPP_CONFIGS.SITE_NAME);
        return updatedUrl;
    }

    static isProjectProduction(): boolean {
        return PRODUCTION_GCLOUD_PROJECT === process.env.GCLOUD_PROJECT;
    }

    static async deleteFolderInStorage(folderPath: string): Promise<void> {
        const bucket = admin.storage().bucket(); // If you use a custom bucket, pass its name as a parameter

        try {
            const [files] = await bucket.getFiles({prefix: folderPath});

            const deletePromises = files.map(file => file.delete());
            await Promise.all(deletePromises);

            console.log(`All files in folder '${folderPath}' have been deleted.`);
        } catch (error) {
            console.error('Error deleting files from storage: ', error);
        }
    }

    static isOlderThanXDays(inputDate: Date, x: number): boolean {
        const xDaysAgo = new Date();
        xDaysAgo.setDate(xDaysAgo.getDate() - x);
        return inputDate < xDaysAgo;
    }

    static isValidHttpUrl(urlStr?: string) {
        if (!urlStr) {
            return false;
        }
        let url: URL | undefined;
        try {
            url = new URL(urlStr);
        } catch (_) {
            return false;
        }
        return url.protocol === "http:" || url.protocol === "https:";
    }

    static async extractColorsFromImage(imageUrl: string): Promise<{ primaryColor: string, backgroundColor: string }> {
        const primaryColor: Palette = await getColorFromURL(imageUrl);
        const palette: Palette[] = await getPaletteFromURL(imageUrl, 5);

        // Find the most different color from the primary color in the palette
        // This is a simple heuristic that can be improved
        let maxDiff = 0;
        let backgroundColor = primaryColor;
        for (const color of palette) {
            // Calculate the Euclidean distance between the two colors
            const diff = Math.sqrt(
                Math.pow(color[0] - primaryColor[0], 2) +
                Math.pow(color[1] - primaryColor[1], 2) +
                Math.pow(color[2] - primaryColor[2], 2)
            );
            // Update the maxDiff and backgroundColor if the difference is larger
            if (diff > maxDiff) {
                maxDiff = diff;
                backgroundColor = color;
            }
        }

        // Return the primary and background colors as hex strings
        return {
            primaryColor: this.rgbToHex(primaryColor),
            backgroundColor: this.rgbToHex(backgroundColor),
        };
    }

    static rgbToHex(rgb: number[]) {
        return (
            '#' +
            rgb
                .map((x) => {
                    const hex = x.toString(16);
                    return hex.length === 1 ? '0' + hex : hex;
                })
                .join('')
        );
    }

    static compareArrays(array1: string[], array2: string[]): number {
        // Convert the arrays to sets to remove duplicates and make comparison easier
        let set1 = new Set(array1);
        let set2 = new Set(array2);

        // Find the intersection of the two sets (i.e., the common elements)
        let commonElements = new Set([...set1].filter(x => set2.has(x)));

        // Return the number of common elements
        return commonElements.size;
    }

    static isValidEmail(emailStr?: string) {
        if (emailStr && /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(emailStr)) {
            return true;
        }
        return false;
    }

    static getTodayDay() {
        return Math.floor(Date.now() / 86400000)
    }

    static async getRedirectedUrl(originalUrl: string): Promise<string | undefined> {
        return new Promise((resolve, reject) => {
            console.log("getRedirectedUrl: " + originalUrl);
            const client = originalUrl.startsWith('https') ? https : http;
            try {
                client.get(originalUrl, response => {
                    if (response.statusCode && response.statusCode >= 300 && response.statusCode < 400 && response.headers.location) {
                        resolve(this.getRedirectedUrl(response.headers.location));
                    } else {
                        resolve(originalUrl);
                    }
                }).on('error', () => {
                    resolve(undefined);
                });
            } catch (e) {
                resolve(undefined);
            }
        });
    }

    static getRandomNumber(min: number, max: number): number {
        return Math.floor(Math.random() * (max - min + 1)) + min;
    }

    static extractDomain(url: string): string {
        const domain = url.match(/:\/\/(www\.)?(.[^/:]+)/i);
        return domain ? domain[2] : '';
    }

    static formatTags(tagString: string): string {
        return tagString
            .split(' ')
            .map(word => word.charAt(0).toUpperCase() + word.slice(1).toLowerCase())
            .join('');
    }

    static async fetchAndParseXML(url: string, useProxy?: boolean): Promise<any> {
        console.log("fetchAndParseXML.url:", url);
        try {

            const response = await fetch(url, {
                method: 'GET',
                headers: {
                    // 'Content-Type': 'application/rss+xml',
                    'Access-Control-Allow-Origin': '*'
                }
            });
            if (!response.ok) {
                if (useProxy) {
                    // Fetch the XML data from the URL with CORS enabled
                    const proxyUrl = new URL(WEBAPP_CONFIGS.WEBSITE_URL + "/api/proxy");
                    proxyUrl.searchParams.append("url", url);
                    return await this.fetchAndParseXML(proxyUrl.toString(), false);
                } else {
                    throw new Error(`HTTP error! status: ${response.status}`);
                }
            }
            const xmlData = await response.text();

            // Parse the XML data to JSON
            const parser = new xml2js.Parser({ explicitArray: false, mergeAttrs: true });
            const result = await parser.parseStringPromise(xmlData);

            return result;
        } catch (error) {
            console.error('Error fetching or parsing XML:', error);
            throw error;
        }
    }

    static removeQueryParams(url: string): string {
        // Create a URL object from the string
        const urlObject = new URL(url);

        // Set the search property to an empty string to remove query parameters
        urlObject.search = '';

        // Return the modified URL string
        return urlObject.toString();
    }

    static extractJsonObject(input: string): Record<string, unknown> | null {
        try {
            // Find the first occurrence of '{' (start of the JSON object)
            const startIndex = input.indexOf('{');
            if (startIndex === -1) {
                return null; // No JSON object found
            }

            // Find the last occurrence of '}' (end of the JSON object)
            const endIndex = input.lastIndexOf('}');
            if (endIndex === -1) {
                return null; // Invalid JSON object (no closing bracket)
            }

            // Extract the JSON object substring
            const jsonString = input.substring(startIndex, endIndex + 1);

            // Parse the JSON string into an object
            const jsonObject = JSON.parse(jsonString);

            return jsonObject;
        } catch (error) {
            console.error('Error parsing JSON:', error);
            return null; // Invalid JSON format
        }
    }

    static replaceAll(input: string, target: string, replacement: string): string {
        let result = "";
        let index = 0;

        while (true) {
            const targetIndex = input.indexOf(target, index);
            if (targetIndex === -1) {
                result += input.substring(index);
                break;
            }
            result += input.substring(index, targetIndex);
            result += replacement;
            index = targetIndex + target.length;
        }

        return result;
    }

    static cleanMarkdown(inputText: string): string {
        let text = inputText;
        // Remove code blocks
        text = text.replace(/```[\s\S]*?```/g, '');
        text = text.replace(/`[^`]*`/g, '');

        // Remove headers
        text = text.replace(/^#+\s+/gm, '');

        // Remove images
        text = text.replace(/!\[.*?\]\(.*?\)/g, '');

        // Remove links
        text = text.replace(/\[([^\]]+)\]\(.*?\)/g, '$1');

        // Remove emphasis (bold, italic, strikethrough)
        text = text.replace(/(\*\*|__)(.*?)\1/g, '$2');
        text = text.replace(/(\*|_)(.*?)\1/g, '$2');
        text = text.replace(/~~(.*?)~~/g, '$1');

        // Remove blockquotes
        text = text.replace(/^>+\s+/gm, '');

        // Remove horizontal rules
        text = text.replace(/^(-\s*?|\*\s*?|_\s*?){3,}$/gm, '');

        // Remove unordered lists
        text = text.replace(/^\s*[-+*]\s+/gm, '');

        // Remove ordered lists
        text = text.replace(/^\s*\d+\.\s+/gm, '');

        // Remove tables
        text = text.replace(/^\|.*?\|$/gm, '');
        text = text.replace(/^\|\s*:?-+:?\s*\|(?:\s*:?-+:?\s*\|)*$/gm, '');

        // Remove HTML tags
        text = text.replace(/<[^>]*>/g, '');

        return text;
    }

    static async isImageLinkValid(url: string): Promise<boolean> {

        return new Promise((resolve) => {
            if (url.includes("googleusercontent")) {
                resolve(false);
                return;
            }
            try {
                const parsedUrl = new URL(url);
                const protocol = parsedUrl.protocol === 'https:' ? https : http;

                const req = protocol.get(url, (res) => {
                    const { statusCode } = res;
                    const contentType = res.headers['content-type'];

                    let error;
                    if (statusCode !== 200) {
                        error = new Error('Request Failed.\n' + `Status Code: ${statusCode}`);
                    } else if (!contentType?.startsWith('image/')) {
                        error = new Error('Invalid content-type.\n' + `Expected image/* but received ${contentType}`);
                    }

                    if (error) {
                        res.resume(); // Consume response data to free up memory
                        resolve(false);
                        return;
                    }

                    res.once('data', () => {
                        // We only need to check if we can start receiving data
                        res.destroy(); // Close the connection
                        resolve(true);
                    });

                    res.on('error', () => {
                        resolve(false);
                    });
                });

                req.on('error', () => {
                    resolve(false);
                });

                req.end();
            } catch (error) {
                resolve(false);
            }
        });
    }

    static findMoneyValues(strings: string[]): string[] {
        const moneyRegex = /(?:^\$|^\€|\$|\€|\bUSD\b|\bEUR\b)/;

        return strings.filter(str => moneyRegex.test(str));
    }

    static removeBracketedContent(input: string): string {
        // Regular expression to match content within square brackets
        const regex = /\[.*?\]/g;

        // Replace all matches with an empty string
        return input.replace(regex, '');
    }

    static checkProbability(probability: number): boolean {
        // Ensure the probability is within the valid range
        if (probability < 0 || probability > 100) {
            throw new Error("Probability must be between 0 and 100");
        }

        // Generate a random number between 1 and 100
        const randomNumber = Math.floor(Math.random() * 100) + 1;

        // Check if the random number is within the probability range
        return randomNumber <= probability;
    }

    static async isValidUrl(url?: string): Promise<boolean> {
        try {
            if (!url) {
                return false;
            }

            // Check if the URL is valid
            new URL(url);

            // Make a HEAD request to check if the URL is accessible
            const response = await axios.head(url, {
                timeout: 5000, // 5 seconds timeout
                validateStatus: (status) => status < 400, // Consider any status less than 400 as success
            });

            return true;
        } catch (error) {
            if (axios.isAxiosError(error)) {
                console.error(`Error checking URL ${url}: ${error.message}`);
            } else {
                console.error(`Invalid URL format: ${url}`);
            }
            return false;
        }
    }

    static isValidUrlFormat(url: string): boolean {
        const urlRegex = /^(https?:\/\/)?((([a-zA-Z0-9\-]+\.)+[a-zA-Z]{2,})|localhost)(:[0-9]{1,5})?(\/.*)?$/;
        return urlRegex.test(url);
    }

    /**
     * Extracts a random subset of items from an array
     * @param array The source array to extract items from
     * @param count The number of items to extract. If larger than array length, returns full array
     * @returns A new array containing the randomly selected items
     */
    static getRandomItems<T>(array: T[], count: number): T[] {
        // Handle edge cases
        if (!array || array.length === 0) return [];
        if (count >= array.length) return [...array];
        if (count <= 0) return [];

        // Create a copy of the array to avoid modifying the original
        const shuffled = [...array];

        // Fisher-Yates shuffle algorithm, but only up to the number of items we need
        for (let i = 0; i < count; i++) {
            const remainingItems = shuffled.length - i;
            const randomIndex = i + Math.floor(Math.random() * remainingItems);

            // Swap current element with random element
            [shuffled[i], shuffled[randomIndex]] = [shuffled[randomIndex], shuffled[i]];
        }

        // Return the first 'count' items
        return shuffled.slice(0, count);
    }
}