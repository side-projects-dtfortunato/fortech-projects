interface EmailParams {
    title: string;
    subtitle: string;
    logoSrc: string; // URL to the logo image
    message: string;
    ctaLabel: string;
    ctaLink: string; // URL for the CTA button
    unsubscribeLink: string;
    companyName: string;
    companyAddress: string;
}

export function createEmailTemplateSimpleMessage(params: EmailParams): string {
    const {title, subtitle, logoSrc, message, ctaLabel, ctaLink, unsubscribeLink, companyName, companyAddress} = params;

    return `
<!DOCTYPE html>
<html>
<head>
    <title>${title}</title>
    <style>
        body { font-family: Arial, sans-serif; }
        .container { width: 100%; max-width: 600px; margin: auto; padding: 20px; }
        .logo { text-align: center; }
        .title { font-size: 24px; text-align: center; }
        .subtitle { font-size: 18px; text-align: center; color: #555; }
        .content { margin-top: 20px; text-align: left; }
        .cta-button { display: block; width: max-content; background-color: #160747; color: #FFFFFF; padding: 10px 20px; text-align: center; text-decoration: none; border-radius: 5px; margin: auto; }
        .footer { margin-top: 30px; font-size: 12px; text-align: center; color: #999; }
        .footer a { color: #999; }
    </style>
</head>
<body>
    <div class="container">
        <div class="logo">
            <img src="${logoSrc}" alt="Logo">
        </div>
        <h1 class="title">${title}</h1>
        <h2 class="subtitle">${subtitle}</h2>
        <div class="content">
            <p>${message}</p>
        </div>
        <a href="${ctaLink}" class="cta-button">${ctaLabel}</a>
        <div class="footer">
            <p>
                You are receiving this email because you opted in at our website. To unsubscribe, please <a href="${unsubscribeLink}">click here</a>.
            </p>
            <p>${companyName}, ${companyAddress}</p>
        </div>
    </div>
</body>
</html>
`;
}