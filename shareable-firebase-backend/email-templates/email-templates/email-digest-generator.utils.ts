import {ArticleImportedModel} from "../../base-projects/articles-digest/data/model/ArticleImported.model";
import {JobDetailsModel, JobListModel} from "../../base-projects/jobs-discover/shared-jobs-discover.data";
import {WEBAPP_CONFIGS} from "../../../configs/webapp.configs";
import {BackendRouteUtils} from "../../utils/backend-route.utils";
import { BackendSharedUtils } from "../../utils/backend-shared-utils";

interface CommunityPost {
    userAvatar?: string;
    userName: string;
    postText: string;
    link: string;
}

interface EmailTemplateOptions {
    introText: string;
    articles: ArticleImportedModel[];
    jobPositions: JobDetailsModel[];
    communityPosts: CommunityPost[];
    unsubscribeLink: string;
    socialNetworks: { name: string; url: string }[];
}

function generateEmailTemplate(options: EmailTemplateOptions): {html: string, emailSubject: string} {
    const {
        introText,
        articles,
        jobPositions,
        communityPosts,
        unsubscribeLink,
        socialNetworks,
    } = options;

    const html = `
    <!DOCTYPE html>
    <html lang="en">
    <head>
      <meta charset="UTF-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <title>${introText} - Newsletter</title>
      <style>
        @import url('https://fonts.googleapis.com/css2?family=Inter:wght@400;500;600;700&display=swap');
      </style>
    </head>
    <body style="font-family: 'Inter', Arial, sans-serif; line-height: 1.5; color: #4B5563; max-width: 600px; margin: 0 auto; padding: 16px; background-color: #F9FAFB;">
      <div style="background-color: white; border-radius: 12px; overflow: hidden; box-shadow: 0 4px 6px rgba(0, 0, 0, 0.05);">
        <!-- Header -->
        <div style="background-color: #FFFFFF; padding: 24px; text-align: center; border-bottom: 1px solid #E5E7EB;">
          <img src="${WEBAPP_CONFIGS.WEBSITE_URL + "/images/logo.png"}" alt="${WEBAPP_CONFIGS.SITE_NAME}" style="max-width: 160px;">
        </div>
        
        <!-- Content -->
        <div style="padding: 24px 20px;">
          <div style="margin-bottom: 20px; text-align: center;">
            <h1 style="font-size: 20px; font-weight: 700; color: #111827; margin-top: 0; margin-bottom: 8px;">${introText}</h1>
            <p style="color: #6B7280; font-size: 15px; margin-top: 0;">Here's your personalized digest of the latest updates.</p>
          </div>

          ${generateSection('Latest Articles', articles, generateArticleItem)}
          ${generateJobsByCategory(jobPositions)}
          ${generateSection('Community Posts', communityPosts, generateCommunityPostItem)}
        </div>
        
        <!-- Footer -->
        <div style="padding: 16px; background-color: #F3F4F6; text-align: center; font-size: 13px; color: #6B7280; border-top: 1px solid #E5E7EB;">
          <div style="margin-bottom: 12px;">
            ${socialNetworks.map(network => `
              <a href="${network.url}" style="display: inline-block; margin: 0 8px; color: #4B5563; text-decoration: none; font-weight: 500;">${network.name}</a>
            `).join('')}
          </div>
          <p style="margin: 6px 0;">${WEBAPP_CONFIGS.COMPANY_ADDRESS}</p>
          <p style="margin: 6px 0;">${WEBAPP_CONFIGS.COMPANY_EMAIL}</p>
          <p style="margin: 12px 0 0;">
            <a href="${unsubscribeLink}" style="color: #4B5563; text-decoration: none; border-bottom: 1px solid #D1D5DB; padding-bottom: 2px;">Unsubscribe</a>
          </p>
        </div>
      </div>
    </body>
    </html>
  `;

    return {
        html: html,
        emailSubject: generateEmailSubject(options),
    }
}

function generateSection(title: string, items: any[], itemGenerator: (item: any) => string): string {
    if (items.length === 0) return '';

    return `
    <div style="margin-bottom: 28px;">
      <h2 style="font-size: 18px; font-weight: 600; margin-bottom: 16px; color: #111827; padding-bottom: 8px; border-bottom: 1px solid #E5E7EB;">${title}</h2>
      ${items.map(item => itemGenerator(item)).join('')}
    </div>
  `;
}

function generateArticleItem(article: ArticleImportedModel): string {
    const articleLink: string = BackendRouteUtils.getArticleDetails(article.uid!);
    const socialNetworkText = article.aiGeneratedContent?.socialNetworkPost || article.title || 'Read more about this article';
    
    return `
    <div style="margin-bottom: 20px; background-color: #F9FAFB; border-radius: 8px; overflow: hidden; box-shadow: 0 1px 3px rgba(0,0,0,0.05);">
      <a href="${articleLink}" style="text-decoration: none; display: block;">
        <img src="${article.imageUrl || 'https://placeholder.com/600x300'}" alt="${article.title}" style="width: 100%; height: 140px; object-fit: cover;">
      </a>
      <div style="padding: 14px;">
        <div style="font-weight: 600; margin-bottom: 6px; font-size: 16px; color: #111827;">
          <a href="${articleLink}" style="color: #111827; text-decoration: none;">${article.title}</a>
        </div>
        <div style="font-size: 14px; color: #4B5563; line-height: 1.4;">
          <a href="${articleLink}" style="color: #4B5563; text-decoration: none;">${BackendSharedUtils.replaceAll(truncateText(socialNetworkText, 120), '*', '')}</a>
        </div>
        <div style="margin-top: 10px;">
          <a href="${articleLink}" style="display: inline-block; color: #2563EB; font-size: 14px; font-weight: 500; text-decoration: none;">Read more →</a>
        </div>
      </div>
    </div>
  `;
}

function generateJobsByCategory(jobs: JobDetailsModel[]): string {
    if (jobs.length === 0) return '';
    
    // Group jobs by category
    const jobsByCategory: {[category: string]: JobDetailsModel[]} = {};
    
    // Sort jobs, putting featured jobs first within each category
    const sortedJobs = [...jobs].sort((a, b) => {
      // Featured jobs come first
      if (a.publishPlanType === 'FEATURED' && b.publishPlanType !== 'FEATURED') return -1;
      if (a.publishPlanType !== 'FEATURED' && b.publishPlanType === 'FEATURED') return 1;
      return 0;
    });
    
    // Group by category
    sortedJobs.forEach(job => {
        const category = job.category || 'Other';
        if (!jobsByCategory[category]) {
            jobsByCategory[category] = [];
        }
        jobsByCategory[category].push(job);
    });
    
    // Generate HTML for each category
    let html = `
    <div style="margin-bottom: 28px;">
      <h2 style="font-size: 18px; font-weight: 600; margin-bottom: 16px; color: #111827; padding-bottom: 8px; border-bottom: 1px solid #E5E7EB;">Job Opportunities</h2>
    `;
    
    // Sort categories alphabetically but ensure "Other" is last if present
    const sortedCategories = Object.keys(jobsByCategory).sort((a, b) => {
        if (a === 'Other') return 1;
        if (b === 'Other') return -1;
        return a.localeCompare(b);
    });
    
    sortedCategories.forEach(category => {
        const categoryLink = category !== 'Other' ? BackendRouteUtils.getJobCategoryPage(category) : '';
        
        html += `
        <div style="margin-bottom: 18px;">
            <h3 style="font-size: 15px; color: #4B5563; margin: 0 0 12px 0; font-weight: 600; display: inline-block; border-bottom: 2px solid #3B82F6; padding-bottom: 4px;">
                ${categoryLink ? 
                    `<a href="${categoryLink}" style="color: #4B5563; text-decoration: none;">
                       ${category}
                       <span style="display: inline-block; margin-left: 4px; font-size: 12px; color: #3B82F6;">→</span>
                     </a>` : 
                    category}
            </h3>
            <div style="display: grid; grid-template-columns: 1fr; gap: 10px;">
                ${jobsByCategory[category].map(job => generateJobItem(job)).join('')}
            </div>
        </div>
        `;
    });
    
    html += `</div>`;
    return html;
}

function generateJobItem(job: JobDetailsModel): string {
    const jobLink: string = BackendRouteUtils.getJobPostDetailsPage(job.uid!);
    const isFeatured = job.publishPlanType === 'FEATURED';
    
    return `
    <div style="background-color: white; border-radius: 8px; padding: 12px; position: relative; box-shadow: 0 1px 2px rgba(0,0,0,0.05); border: 1px solid #E5E7EB; ${isFeatured ? 'border-left: 3px solid #3B82F6;' : ''}">
      ${isFeatured ? `
        <div style="position: absolute; top: -6px; right: 10px; background: linear-gradient(90deg, #60a5fa, #3b82f6); color: white; padding: 3px 8px; border-radius: 12px; font-size: 11px; font-weight: 500; box-shadow: 0 2px 4px rgba(59, 130, 246, 0.2);">
          ⭐ Featured
        </div>` : ''}
      <div style="display: flex; gap: 12px;">
        <a href="${jobLink}" style="text-decoration: none; flex-shrink: 0;">
          <img src="${job.companyLogo}" alt="${job.company}" style="width: 42px; height: 42px; border-radius: 6px; object-fit: cover; box-shadow: 0 1px 2px rgba(0,0,0,0.1);">
        </a>
        <div style="min-width: 0; flex-grow: 1; display: flex; flex-direction: column; justify-content: center;">
          <div style="font-weight: 600; color: #111827; white-space: normal; word-break: break-word; font-size: 15px; margin-bottom: 2px; line-height: 1.3;">
            <a href="${jobLink}" style="color: #111827; text-decoration: none;">
              ${job.title}${isFeatured ? ' 🔥' : ''}
            </a>
          </div>
          <div style="font-size: 13px; color: #6B7280; display: flex; align-items: center;">
            <a href="${jobLink}" style="color: #6B7280; text-decoration: none; margin-right: 6px;">${job.company}</a>
            <span style="color: #9CA3AF; margin: 0 4px;">•</span>
            <span>${job.location}</span>
            ${job.workMode && typeof job.workMode === 'string' ? `<span style="color: #9CA3AF; margin: 0 4px;">•</span><span>${job.workMode}</span>` : ''}
          </div>
        </div>
        <a href="${jobLink}" style="display: inline-flex; align-items: center; justify-content: center; flex-shrink: 0; text-decoration: none; color: #3B82F6; font-size: 16px; font-weight: 500; margin-left: auto;">
          <span style="border: 1px solid #3B82F6; border-radius: 50%; width: 24px; height: 24px; display: flex; align-items: center; justify-content: center;">→</span>
        </a>
      </div>
    </div>
  `;
}

function generateCommunityPostItem(post: CommunityPost): string {
    const avatarContent = post.userAvatar
        ? `<img src="${post.userAvatar}" alt="${post.userName}" style="width: 42px; height: 42px; border-radius: 100%; object-fit: cover;">`
        : `<div style="width: 42px; height: 42px; border-radius: 100%; background-color: #3B82F6; display: flex; align-items: center; justify-content: center; color: white; font-size: 18px; font-weight: 600;">
             ${post.userName.charAt(0).toUpperCase()}
           </div>`;

    return `
    <div style="margin-bottom: 16px; background-color: white; border-radius: 8px; padding: 14px; box-shadow: 0 1px 2px rgba(0,0,0,0.05); border: 1px solid #E5E7EB;">
      <div style="display: flex; align-items: center; margin-bottom: 10px;">
        <a href="${post.link}" style="text-decoration: none; margin-right: 12px; flex-shrink: 0;">
          ${avatarContent}
        </a>
        <div>
          <div style="font-weight: 600; margin-bottom: 1px; color: #111827; font-size: 14px;">
            <a href="${post.link}" style="color: #111827; text-decoration: none;">${post.userName}</a>
          </div>
          <div style="font-size: 12px; color: #6B7280;">Community Member</div>
        </div>
      </div>
      <div style="font-size: 14px; line-height: 1.5; color: #4B5563; padding: 10px; border-radius: 8px; margin-bottom: 10px; border: 1px solid #E5E7EB;">
        <a href="${post.link}" style="color: #4B5563; text-decoration: none;">${truncateText(post.postText, 150)}</a>
      </div>
      <div style="text-align: right;">
        <a href="${post.link}" style="display: inline-block; color: #3B82F6; font-size: 13px; font-weight: 500; text-decoration: none;">View post →</a>
      </div>
    </div>
  `;
}

function generateEmailSubject(options: EmailTemplateOptions): string {
    const { articles, jobPositions, communityPosts } = options;

    // Helper function to get random item from array
    const getRandomItem = <T>(arr: T[]): T => arr[Math.floor(Math.random() * arr.length)];

    // Generate subject based on content
    if (articles.length > 0 && jobPositions.length > 0 && communityPosts.length > 0) {
        // All types of content are present
        return `${options.introText}: 📰 Latest Articles, 💼 Job Opportunities, and 💬 Community Updates`;
    } else if (jobPositions.length > 0) {
        // Focus on job opportunities
        const randomJob = getRandomItem(jobPositions);
        const jobCount = jobPositions.length;
        return `${options.introText}: 💼 ${jobCount} New Job Opportunities, including ${randomJob.title} at ${randomJob.company}`;
    } else if (articles.length > 0) {
        // Focus on articles
        const randomArticle = getRandomItem(articles);
        return `${options.introText}: 📰 Hot Off the Press: "${truncateText(randomArticle.title, 80)}" and More`;
    } else if (communityPosts.length > 0) {
        // Focus on community posts
        const postCount = communityPosts.length;
        return `${options.introText}: 💬 ${postCount} New Community Posts - Join the Conversation!`;
    } else {
        // Fallback subject if no specific content
        return `Your ${options.introText}`;
    }
}

function truncateText(text: string, maxLength: number): string {
    if (text.length <= maxLength) return text;
    return text.substring(0, maxLength - 3) + '...';
}

export { generateEmailTemplate, EmailTemplateOptions };