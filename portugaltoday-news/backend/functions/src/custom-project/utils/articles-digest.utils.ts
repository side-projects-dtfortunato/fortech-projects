import {
    ArticleImportedModel
} from "../../shareable-firebase-backend/base-projects/articles-digest/data/model/ArticleImported.model";
import {BackendSharedUtils} from "../../shareable-firebase-backend/utils/backend-shared-utils";
import {BackendRouteUtils} from "../../shareable-firebase-backend/utils/backend-route.utils";
import {
    SharedArticlesDigestUtils
} from "../../shareable-firebase-backend/base-projects/articles-digest/shared-articles-digest.utils";
import {NotificationsQueueManager} from "../../shareable-firebase-backend/utils/notifications-queue.manager";



const ARTICLES_CONTENT_TOPIC = "News about Portugal (translate all the content to english)";
const CATEGORIES_LIST = "News | Local | Politics | Economy | Culture | Sports | Tourism | Technology | Health | Education | Environment | International | Society | Business | Lifestyle | Science | Crime | Entertainment | Expats"

export class ArticlesDigestUtils {

    static SOURCES_BLACKLIST: string[] = ["DIÁRIO DE NOTÍCIAS"];
    // static SOURCES_BLACKLIST_DOMAINS = ["bloomberg.com", "bitcoin.com", "youtube.com"]
    static SOURCES_BLACKLIST_DOMAINS = []

    static async fetchGoogleNews(searchTerm: string, searchType: "TOPIC" | "SEARCH", customLangGeoParams?: string, filterOlderThanHours?: number, maxArticles?: number): Promise<any> {
        let listArticles: ArticleImportedModel[] = await SharedArticlesDigestUtils.fetchGoogleNews({
            searchTerm: searchTerm,
            searchType: searchType,
            timeFrame: "",
            domainsBlackList: this.SOURCES_BLACKLIST_DOMAINS,
            customLangGeoParams: customLangGeoParams
        });

        listArticles = listArticles
            .filter((googleArticle) => {
                return !this.SOURCES_BLACKLIST.includes(googleArticle.source.toUpperCase())
            });
        // filter by the most relevant and recent articles
        listArticles = listArticles.slice(0, maxArticles ? maxArticles : 5); // Top 4 articles
        if (filterOlderThanHours) {
            listArticles = listArticles.filter((googleArticle) => BackendSharedUtils.isLessThanHoursAgo(googleArticle.publishedDate, filterOlderThanHours));
        }

        return listArticles;
    }


    static async getMostRecentArticles(): Promise<ArticleImportedModel[]> {
        let listParsedArticles: ArticleImportedModel[] = [];

        // Fetch Google News
        listParsedArticles = listParsedArticles.concat(await this.fetchGoogleNews("CAAqJggKIiBDQkFTRWdvSkwyMHZNREZuWnpnMEVnVndkQzFRVkNnQVAB", "TOPIC", "hl=pt-PT&gl=PT&ceid=PT", 24, 5)); // Topic: Algarve (PT)
        listParsedArticles = listParsedArticles.concat(await this.fetchGoogleNews("CAAqJQgKIh9DQkFTRVFvSUwyMHZNRFJzYkdJU0JYQjBMVkJVS0FBUAE", "TOPIC", "hl=pt-PT&gl=PT&ceid=PT", 24, 5)); // Topic: Lisboa (PT)
        listParsedArticles = listParsedArticles.concat(await this.fetchGoogleNews("CAAqJQgKIh9DQkFTRVFvSUwyMHZNRFZ5TkhjU0JYQjBMVkJVS0FBUAE", "TOPIC", "hl=pt-PT&gl=PT&ceid=PT", 12, 4)); // Topic: Portugal (PT)
        listParsedArticles = listParsedArticles.concat(await this.fetchGoogleNews("CAAqIQgKIhtDQkFTRGdvSUwyMHZNRFZ5TkhjU0FtVnVLQUFQAQ", "TOPIC", "hl=en-US&gl=US&ceid=US", 24, 2)); // Topic: Portugal (EN)
        listParsedArticles = listParsedArticles.concat(await this.fetchGoogleNews("CAAqJQgKIh9DQkFTRVFvSUwyMHZNRFZ5TkhjU0JXVnVMVWRDS0FBUAE", "TOPIC", "hl=en-GB&gl=GB&ceid=GB", 24, 2)); // Topic: Portugal (EN:UK)

        return listParsedArticles;
    }

    static async generateAIContent(articleData: ArticleImportedModel, otherArticles?: string[]): Promise<{title: string, category: string, content: string, highlightsBullets: string[], tags: string[], socialNetworkPost: string, isArticleContentValid: boolean} | undefined> {
        const generatedAIContent = await SharedArticlesDigestUtils.generateAIContentOpenAI(articleData, {
            topic: ARTICLES_CONTENT_TOPIC,
            categoryList: CATEGORIES_LIST,
            otherArticles: otherArticles,
        });
        if (generatedAIContent) {
            (articleData.aiGeneratedContent as any).marketSentiment = generatedAIContent?.marketSentiment;
        }
        return generatedAIContent;
    }



    static async rankListArticles(listArticles: ArticleImportedModel[], otherArticlesTitles?: string[]): Promise<{listArticles: {articleUid: string, title: string, articleRelevancy: number, hasSimilarArticles: boolean}[]} | any | undefined> {
        return SharedArticlesDigestUtils.rankListArticles(listArticles, ARTICLES_CONTENT_TOPIC, otherArticlesTitles);
    }

    static generateSocialNetworkPost(articleData: ArticleImportedModel) {
        let postMessage: string = "";

        if (articleData.aiGeneratedContent.socialNetworkPost) {
            postMessage = articleData.aiGeneratedContent.socialNetworkPost
        } else {
            postMessage = articleData.title + "\n\n";
            articleData.aiGeneratedContent.tags?.forEach((tag, index) => {
                if (index > 0) {
                    postMessage += " ";
                }
                postMessage += "#" + BackendSharedUtils.replaceAll(tag, " ", "");
            });
        }
        postMessage += "\n\nRead more: " + BackendRouteUtils.getArticleDetails(articleData.uid!);
        postMessage = BackendSharedUtils.replaceAll(postMessage, "*", "");
        return postMessage;
    }

    static async publishArticleSocialNetworks(articleData: ArticleImportedModel) {

        // Add Notification to the queue
        await NotificationsQueueManager.addNotificationItemToQueue({
            uid: articleData.uid!,
            socialPost: this.generateSocialNetworkPost(articleData),
            category: articleData.aiGeneratedContent!.category ? articleData.aiGeneratedContent!.category : articleData.aiGeneratedContent!.tags![0],
            title: BackendSharedUtils.replaceAll(articleData.title, "*", ""),
            link: BackendRouteUtils.getArticleDetails(articleData.uid!),
            socialNetworks: ["REDDIT", "FACEBOOK", "INSTAGRAM", "THREADS", "BLUESKY", "TELEGRAM"],
            bgImageUrl: articleData.imageUrl!,
            pushNotification: true,
            description: BackendSharedUtils.cleanMarkdown(articleData.aiGeneratedContent.summary!),
        });
    }
}