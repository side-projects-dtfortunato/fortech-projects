import {
    JobDetailsModel,
    LinkedinSourceModel
} from "../../shareable-firebase-backend/base-projects/jobs-discover/shared-jobs-discover.data";
import axios from "axios";


/**
 * Regions ids:
 * - UK: 101165590
 * - Australia: 101452733
 * - USA: 103644278
 * - EU: 91000002
 */
export const LINKEDIN_SOURCES: LinkedinSourceModel[] = []


export class CustomJobsDiscoverUtils {


    static async importJobsFromRemoteJobsHub() {
        const category = "Customer%20Service";
        const listJobs: JobDetailsModel[] = (await axios.get(`https://us-central1-remotejobshub-app.cloudfunctions.net/getLatestJobsFunction?category=${category}`)).data;
        return listJobs;
    }

    static getIsValidRule() {
        return undefined;
    }

}