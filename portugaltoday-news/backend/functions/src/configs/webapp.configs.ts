
export const PRODUCTION_GCLOUD_PROJECT = "portugaltodaynews";

export const WEBAPP_CONFIGS = {
    LOGO_URL: "https://www.portugaltoday.news/images/logo-white-bg.jpg",
    SITE_TAGLINE: "Top Portugal Daily News",
    SITE_NAME: "PortugalToday.news",
    WEBSITE_URL: "https://portugaltoday.news",
    COMPANY_ADDRESS: "3030 Coimbra (Portugal)",
    COMPANY_EMAIL: "contact@portugaltoday.news",
    POST_PREFIX_URL: "https://www.portugaltoday.news/post/",
    PROFILE_PREFIX_URL: "https://www.portugaltoday.news/public-profile/",
    UNSUBSCRIBE_URL: `https://us-central1-${PRODUCTION_GCLOUD_PROJECT}.cloudfunctions.net/unsubscribeEmail?uid=`,

    MAILERSEND_API_KEY: "mlsn.af84b198b5d05a63aa0f1a69cf807d9b70279803b6d0327f0a165e0ed33745e4",

    CUSTOM_CONFIGS: {
        openPushInApp: true,
    } as any,

    // Account: contact@sidehustlify.com - Di.....oc7
    IMAGE_KIT_API: {
        publicKey : "TODO", // "public_QPb3OvUFUKRLd2VLZtV+ExCVDTo=",
        privateKey : "TODO", //"private_z60vq5B2MydsmNQ+c2dMsOG0RBg=",
        urlEndpoint : "TODO", //"https://ik.imagekit.io/sidehustlify",
    },

    STRIPE_API: {
        prod: "",
        dev: "",
        webhook_prod: "",
        webhook_dev: "",
    },

    ONESIGNAL_CONFIGS: {
        apiKey: "YWVhOWYzNzEtNWQ0MC00YWI2LWE2NmYtN2EzMTBlMGFiY2Jk",
        appId: "09bdb23b-1224-4bdf-84d8-fcd1b189deb0"
    },

    SOCIAL_NETWORK_API_KEY: {
        metaAccessToken: "EAALIaURaLj0BO9BrBBs6EMiID5CSjIS3ocIGD4mmlPnAyCkFusxZCmmqN7Gn0T5kBnHZBqIJ3UkXHr9M2d6pR59MjsJZBCrxyfJXhGYfuLNjzh3jGZCzCZAu4la6YohfLE1wQoSdSbZAVjTiUOM8Yj8JYVbgxggj1CubrynBpSvNfdT0Ku6U1wQ5SzZCTYqUCcZD",
        facebookPageId: "390879197435311",
        instagramId: "17841468123462679",
        threadsAPI: {
            clientId: "499525875799244",
            clientSecret: "53c889e8cb6cb437acc932a7d9f80491",
            redirectUri: `https://us-central1-${PRODUCTION_GCLOUD_PROJECT}.cloudfunctions.net/threadsAPIHandleAuthCallback`,
        },
        redditAPI: {
            clientId: "4AipG2HfGPnuOhtXtOYEmA",
            clientSecret: "9swPagUhH079Yq3uzQjyimQssUYhow",
            username: "FortulyApps",
            password: "divadlooc7",
            subReddit: "PortugalNewsToday",
        },
        bluesky: {
            username: "portugaltodaynews.bsky.social",
            password: "divadlooc7",
        },
        telegram: {
            channel: "@portugaltodaynews",
            botToken: "8069198770:AAEVL8ianEHHtWzhEIjxVJGlj6dGqRQDKtQ",
        }
    }
}