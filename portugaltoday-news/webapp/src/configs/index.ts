
export default class RootConfigs {
    static ENVIRONMENT_DEV = false;
    static BASE_URL = "https://www.portugaltoday.news";
    static SITE_NAME = "PortugalToday.news";
    static SITE_TITLE = "Portugal Today News - Daily Top News of Portugal for Expats";
    static SITE_DESCRIPTION = "PortugalToday.news: Your daily source for the latest Portuguese news in English. Stay informed about politics, economy, culture, and current events shaping Portugal. Timely, accurate, and easy to understand coverage for expats, travelers, and Portugal enthusiasts worldwide.";
    static CURRENT_NICHE_SITE = RootConfigs.SITE_NAME;
    static CONTACT_EMAIL = "contact@portugaltoday.news";
    static SITE_TOPIC = "Portugal Breaking News";
    static THEME = {
        PRIMARY_COLOR: "red",
    }
    static META_CONFIGS: any = {
        updateNicheBackend: false,
        disableGoogleSignin: true,
        disableCookies: true,
        disableAppStoreBanner: false,
    }
    static APP_STORE_LINKS = {
        androidAppId: "portugaltoday.news",
        iosAppId: "6538716025",
    }
    static SOCIAL_NETWORKS: {
        [socialId: string]: {
            iconUrl: string,
            label: string,
            link: string,
        },
    } = {
        facebook: {
            label: "Facebook",
            link: "https://www.facebook.com/profile.php?id=61562471464838",
            iconUrl: "/images/ic_social_facebook.png",
        },
        instagram: {
            label: "Instagram",
            link: "https://www.instagram.com/portugaltoday.news/",
            iconUrl: "/images/ic_social_instagram.png",
        },
        threads: {
            label: "Threads",
            link: "https://www.threads.net/@portugaltoday.news/",
            iconUrl: "/images/ic_social_threads.png",
        },
        bluesky: {
            label: "Bluesky",
            link: "https://portugaltodaynews.bsky.social",
            iconUrl: "/images/ic_social_bluesky.png",
        },
        telegram: {
            label: "Telegram Community",
            link: "https://t.me/portugaltodaynews",
            iconUrl: "/images/ic_social_threads.png",
        },
    }
    static FIREBASE_CONFIG = this.ENVIRONMENT_DEV ?

        // DEV Config - // Change it
        {
            apiKey: "AIzaSyCB2yrNkEHbpShTH_xwHOzcl2mXJT5i1K8",
            authDomain: "fortuly-qa-env.firebaseapp.com",
            projectId: "fortuly-qa-env",
            storageBucket: "fortuly-qa-env.appspot.com",
            messagingSenderId: "650128691290",
            appId: "1:650128691290:web:69f8a283090d23e0454863",
            measurementId: "G-V6XN6K51VZ"
        } :
        // PROD
        {
            apiKey: "AIzaSyBBt4WM_5mRHY7fAkqv6luD-8wo7VH_k7c",
            authDomain: "portugaltodaynews.firebaseapp.com",
            projectId: "portugaltodaynews",
            storageBucket: "portugaltodaynews.appspot.com",
            messagingSenderId: "327719301475",
            appId: "1:327719301475:web:dc32218f3e81655d6ea356",
            measurementId: "G-4JBV63YYDB"
        };


    static IMAGEKIT_API = {
        publicKey : "public_OurCtnmj0jWqjVslk5YDfKE4aHQ=",
        privateKey : "private_vRUtPf6HGOj68f5rZZN8u9RHfiQ=",
        urlEndpoint : "https://ik.imagekit.io/fortulyapps",
    }

    static ONE_SIGNAL_API = "09bdb23b-1224-4bdf-84d8-fcd1b189deb0";
}
