import SharedPrivacyPolicyPageContent
    from "../components/react-shared-module/ui-components/pages-content/SharedPrivacyPolicyPageContent";

export default function PrivacyPolicyPage() {
    return (
        <SharedPrivacyPolicyPageContent />
    )
}

export function getPrivacyPolicyPage() {
    return "/privacy-policy";
}