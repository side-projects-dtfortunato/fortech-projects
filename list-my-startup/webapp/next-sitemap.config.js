/** @type {import('next-sitemap').IConfig} */
module.exports = {
    siteUrl: process.env.SITE_URL || 'https://www.listmystartup.app',
    generateRobotsTxt: true, // (optional)
    exclude: ["/login", "/login/recover-password", "/edit-post/*", "/edit-profile/user-profile", "/bookmarks", "/submit-startup",
        "/submit-startup/pending/*", "/privacy-policy"],
    // ...other options
}