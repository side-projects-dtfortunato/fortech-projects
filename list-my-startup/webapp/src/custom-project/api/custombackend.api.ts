import {
    callFunction,
    ClientRealtimeDbUtils, db
} from "../../components/react-shared-module/logic/shared-data/firebase.utils";
import {StartupDetailsModel} from "../data/model/startup-details.model";
import {ApiResponse} from "../../components/react-shared-module/logic/shared-data/datamodel/base.model";
import {ImagekitHelper} from "../../components/react-shared-module/utils/imagekit.helper";
import CommonUtils from "../../components/react-shared-module/logic/commonutils";
import {StorageFileModel} from "../../components/react-shared-module/logic/shared-data/datamodel/storage-file.model";
import {CustomFirestoreCollectionDB, CustomRealtimeCollectionDB} from "../data/custom-firestore-collection-names";
import {getDocs, limit, orderBy, startAfter} from "firebase/firestore";
import {collection, query, where} from "@firebase/firestore";


export class CustomBackendApi {

    static async submitStartup(data: { startupData: StartupDetailsModel, creatorComment?: string}): Promise<ApiResponse<any>> {
        // Upload Images first
        data.startupData.uid = CommonUtils.getDomainNameFromUrl(data.startupData.link);
        const folderPath: string = CommonUtils.string_to_slug(data.startupData.uid + "/");
        console.log("submitStartup.startupData:", data.startupData);
        console.log("Folder Path:", folderPath);

        // Upload Logo
        data.startupData.media.logo = await ImagekitHelper.uploadImageFromUrl(data.startupData.media.logo!.tempBase64!, data.startupData.uid + "-logo", folderPath, []);

        // Upload Images
        if (data.startupData.media.screenshots.length > 0) {
            for (let i = 0; i < data.startupData.media.screenshots.length; i++) {
                const imageData: StorageFileModel = data.startupData.media.screenshots[i];
                const imageResponse = await ImagekitHelper.uploadImageFromUrl(imageData.tempBase64!, data.startupData.uid + "-image-" + i, folderPath, []);
                if (imageResponse){
                    data.startupData.media.screenshots[i] = {
                        fileUrl: imageResponse.fileUrl,
                        storagePath: imageResponse.storagePath,
                    };
                }
            }
        }

        // Update Database with Categories
        if (data.startupData.categories) {
            let dbCategoriesData: {[cat: string]: number} | undefined = await ClientRealtimeDbUtils.getData({
                collectionName: CustomRealtimeCollectionDB.SubmitStartupCategories,
            });
            if (!dbCategoriesData) {
                dbCategoriesData = {};
            }
            data.startupData.categories.forEach((cat)  => {
                if (Object.keys(dbCategoriesData!).includes(cat)) {
                    dbCategoriesData![cat] = dbCategoriesData![cat] + 1;
                } else {
                    dbCategoriesData![cat] = 1;
                }
            });
            await ClientRealtimeDbUtils.writeData({
                collectionName: CustomRealtimeCollectionDB.SubmitStartupCategories,
                data: dbCategoriesData,
                merge: false,
            });
        }

        return await callFunction("submitStartup", data);
    }

    static async getCategoryStartups(props: {category: string, lastDocumentId?: string}): Promise<StartupDetailsModel[]> {
        const constraints = [
            orderBy("updatedAt", "desc"), // Replace 'id' with your actual document ID field
            where("categories", "array-contains", props.category),
            limit(10),
        ];
        if (props.lastDocumentId) {
            // @ts-ignore
            constraints.push(startAfter(lastDocumentId!));
        }
        const q = query(
            collection(db, CustomFirestoreCollectionDB.StartupDetails),
            ...constraints
        );

        const querySnapshot = await getDocs(q);
        const documents = querySnapshot.docs.map((doc) => doc.data() as StartupDetailsModel);

        return documents;
    }
}