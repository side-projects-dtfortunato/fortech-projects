import React from 'react';
import { motion } from 'framer-motion';
import { Rocket, Globe, Users, TrendingUp, ChevronRight } from 'lucide-react';
import Link from 'next/link';
import { CustomRoutesUtils } from '../../utils/custom-routes.utils';

// @ts-ignore
const FeatureCard = ({ icon: Icon, title }) => (
    <motion.div
        className="flex items-center gap-2 bg-white/90 px-4 py-2 rounded-lg shadow-sm"
        whileHover={{ y: -2, scale: 1.02 }}
        transition={{ type: "spring", stiffness: 400 }}
    >
        <Icon className="w-4 h-4 text-orange-500" />
        <span className="text-sm font-medium text-gray-700">{title}</span>
    </motion.div>
);

const SubmitStartupBanner = () => {
    return (
        <div className="w-full max-w-4xl mx-auto px-4">
            <motion.div
                initial={{ opacity: 0, y: 20 }}
                animate={{ opacity: 1, y: 0 }}
                className="relative overflow-hidden"
            >
                {/* Main Container */}
                <div className="rounded-3xl bg-gradient-to-br from-orange-50 via-white to-orange-100">
                    <div className="p-8 md:p-12">

                        {/* Content */}
                        <div className="flex flex-col items-center text-center gap-6 max-w-2xl mx-auto">
                            <motion.div
                                initial={{ opacity: 0, y: -20 }}
                                animate={{ opacity: 1, y: 0 }}
                                transition={{ delay: 0.2 }}
                                className="space-y-4"
                            >
                                <div className="inline-flex items-center gap-2 bg-orange-100 px-4 py-2 rounded-full">
                                    <Rocket className="w-4 h-4 text-orange-500" />
                                    <span className="text-sm font-medium text-orange-700">Launch Your Success Story</span>
                                </div>

                                <h2 className="text-3xl md:text-4xl font-bold text-gray-900">
                                    Ready to Scale Your
                                    <span className="text-orange-500"> Startup</span>?
                                </h2>

                                <p className="text-gray-600 text-lg">
                                    Join our curated platform and connect with investors, customers,
                                    and opportunities that can take your startup to the next level.
                                </p>
                            </motion.div>

                            {/* Features Grid */}
                            <motion.div
                                initial={{ opacity: 0 }}
                                animate={{ opacity: 1 }}
                                transition={{ delay: 0.4 }}
                                className="grid grid-cols-2 sm:grid-cols-4 gap-3 w-full"
                            >
                                <FeatureCard icon={Globe} title="Global Exposure" />
                                <FeatureCard icon={Users} title="Investor Network" />
                                <FeatureCard icon={TrendingUp} title="Growth Analytics" />
                                <FeatureCard icon={Users} title="Founder Community" />
                            </motion.div>

                            {/* CTA Button */}
                            <Link href={CustomRoutesUtils.getSubmitStartupUrl()}>
                                <motion.div
                                    whileHover={{ scale: 1.02 }}
                                    whileTap={{ scale: 0.98 }}
                                    className="group inline-flex items-center justify-center gap-2 px-8 py-4 bg-orange-500 hover:bg-orange-600 text-white rounded-xl font-semibold shadow-lg hover:shadow-xl transition-all duration-200 cursor-pointer"
                                >
                                    List Your Startup Now
                                    <ChevronRight className="w-5 h-5 group-hover:translate-x-1 transition-transform" />
                                </motion.div>
                            </Link>
                        </div>
                    </div>
                </div>

                {/* Subtle background decoration */}
                <div className="absolute inset-0 -z-10">
                    <div className="absolute top-0 left-0 w-32 h-32 bg-orange-200 rounded-full blur-3xl opacity-20" />
                    <div className="absolute bottom-0 right-0 w-32 h-32 bg-orange-300 rounded-full blur-3xl opacity-20" />
                </div>
            </motion.div>
        </div>
    );
};

export default SubmitStartupBanner;