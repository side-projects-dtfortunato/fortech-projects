import {useDisclosure} from "@chakra-ui/hooks";
import {
    Modal,
    ModalBody,
    ModalCloseButton,
    ModalContent,
    ModalFooter,
    ModalHeader,
    ModalOverlay
} from "@chakra-ui/modal";
import {useEffect, useState} from "react";
import InputTextComponent from "../../../components/react-shared-module/ui-components/form/InputText.component";
import UserProfilePictureUpload
    from "../../../components/react-shared-module/ui-components/edit-profile/shared/UserProfilePictureUpload.component";
import {
    useCentralUserDataState
} from "../../../components/react-shared-module/logic/global-hooks/root-global-state";
import SpinnerComponent from "../../../components/react-shared-module/ui-components/shared/Spinner.component";
import Link from "next/link";
import {CustomProjectUtils} from "../../utils/custom-project.utils";
import InlineLabelSeparatorComponent
    from "../../../components/react-shared-module/ui-components/shared/inline-label-separator.component";
import {SharedBackendApi} from "../../../components/react-shared-module/logic/shared-data/sharedbackend.api";

export default function ProfileEditorPopupComponent(props: {displayPopup: boolean, onPopupClosed: () => void}) {
    const { isOpen, onOpen, onClose } = useDisclosure();
    const [centralUserData, setCentralUserData] = useCentralUserDataState();
    const [isLoading, setIsLoading] = useState(false);
    const [errorMessage, setErrorMessage] = useState("");

    useEffect(() => {
        if (props.displayPopup) {
            onOpen();
        }
    }, [props.displayPopup]);

    function renderEditorForm() {
        if (centralUserData) {
            return (
                <div className='flex flex-col gap-5 items-center'>
                    <InputTextComponent disabled={isLoading} value={centralUserData.username} topLabel={"Username"} hint={"Choose a unique username"} onChanged={(value) => {
                        setErrorMessage("");
                        setCentralUserData(
                            {
                                ...centralUserData,
                                username: value,
                                publicProfile: {
                                    ...centralUserData!.publicProfile,
                                    username: value,
                                },
                            }
                        );
                    }} />
                    <div className='flex flex-col gap-1'>
                        <h4 className='custom'>Upload your public picture</h4>
                        <UserProfilePictureUpload disabled={isLoading} centralUserData={centralUserData!}
                                                  setCentralUserData={setCentralUserData}/>
                    </div>

                    <span className='text-red-600 mt-5 w-full text-center'>{errorMessage}</span>
                </div>
            )
        } else {
            return (<></>);
        }

    }

    async function onSaveProfile() {
        setIsLoading(true);
        setErrorMessage("");

        // Check if the new username is valid
        let checkUsernameResponse = await SharedBackendApi.checkUsernameValid({username: centralUserData?.username});

        if (!checkUsernameResponse.isSuccess) {
            setErrorMessage(checkUsernameResponse.message!);
            setIsLoading(false);
            return;
        }

        const apiResponse = await SharedBackendApi.updateUserProfile({centralUserData: centralUserData!});

        if (apiResponse.isSuccess) {
            onClose();
        } else {
            setErrorMessage("Some error occurred, please try again or contact our support team.");
        }

        setIsLoading(false);

    }


    return (
        <div className='flex flex-col'>
            <Modal isOpen={isOpen} onClose={onClose} size={"xl"} closeOnEsc={false} closeOnOverlayClick={false}>
                <ModalOverlay />
                <ModalContent>
                    <ModalHeader>
                        <InlineLabelSeparatorComponent title={"Complete your profile"} subtitle={"Before you submit your startup, please provide us some additional info about you."} />
                    </ModalHeader>
                    <ModalBody>
                        {isLoading ?
                            <SpinnerComponent/> : <></>}
                        {renderEditorForm()}
                    </ModalBody>

                    <ModalFooter className='flex flex-row gap-2'>
                        <Link href={"/"} className='btn btn-ghost'>Cancel</Link>
                        <button className='btn btn-success' disabled={!CustomProjectUtils.isProfileValidToSubmitStartup(centralUserData?.publicProfile) || isLoading} onClick={onSaveProfile}>Save Changes</button>
                    </ModalFooter>
                </ModalContent>
            </Modal>
        </div>
    )
}