import Link from "next/link";
import {CustomRoutesUtils} from "../../utils/custom-routes.utils";

export default function StartupCategoryLabelComponent(props: { categoryLabel: string }) {
    return (
        <Link href={CustomRoutesUtils.getCategoryStartupsUrl(props.categoryLabel)} className="badge badge-ghost text-slate-600 text-xs text-slate-600 hover:bg-orange-100">{props.categoryLabel}</Link>
    )
}