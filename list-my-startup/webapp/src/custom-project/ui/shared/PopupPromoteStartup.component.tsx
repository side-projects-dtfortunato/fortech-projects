import {StartupDetailsModel} from "../../data/model/startup-details.model";
import {useDisclosure} from "@chakra-ui/hooks";
import {useAuthState} from "react-firebase-hooks/auth";
import {auth, ClientRealtimeDbUtils} from "../../../components/react-shared-module/logic/shared-data/firebase.utils";
import {Modal, ModalBody, ModalContent, ModalFooter, ModalHeader, ModalOverlay} from "@chakra-ui/modal";
import InlineLabelSeparatorComponent
    from "../../../components/react-shared-module/ui-components/shared/inline-label-separator.component";
import Link from "next/link";
import {useEffect, useState} from "react";
import {Radio, RadioGroup} from "@chakra-ui/radio";
import {Stack} from "@chakra-ui/react";
import {CustomRealtimeCollectionDB} from "../../data/custom-firestore-collection-names";
import AuthManager from "../../../components/react-shared-module/logic/auth/auth.manager";

const WEEK_PROMOTION_LINK = "https://buy.stripe.com/fZe4iA7yhfOBaKkfYZ";
const MONTH_PROMOTION_LINK = "https://buy.stripe.com/5kA2as19T9qd8CcfZ0";

enum PromotionPlan { FREE = "FREE", WEEK_PROMOTION = "WEEK_PROMOTION", MONTH_PROMOTION = "MONTH_PROMOTION"}

export default function PopupPromoteStartupComponent(props: {startupData: StartupDetailsModel}) {
    const { isOpen, onOpen, onClose } = useDisclosure();
    const [currentUser, isAuthLoading] = useAuthState(auth);
    const [selectedPlan, setSelectedPlan] = useState<PromotionPlan>(PromotionPlan.FREE);

    useEffect(() => {
        // Check if should display the popup
        if (props.startupData.creatorUid && props.startupData.creatorUid.length > 0 && AuthManager.getUserId() === props.startupData.creatorUid) {
            ClientRealtimeDbUtils.getData({
                collectionName: CustomRealtimeCollectionDB.SponsoredStartupsClicked,
                docId: props.startupData.uid!
            }).then((timestamp: number | undefined) => {
                if (!timestamp && !isOpen) {
                    onOpen();
                }
            });
        }
    }, [isAuthLoading]);



    function renderSelectorItem(title: any, description: string, promotionPlan: PromotionPlan) {
        const isSelected: boolean = selectedPlan === promotionPlan;

        return (
            <div className={`flex flex-row gap-2 p-5 rounded border
            ${isSelected ? "border-orange-200" : "border-slate-200"}`}>
                <Radio colorScheme='orange' value={promotionPlan}>
                    <div className='flex flex-col gap-2 ml-2'>
                        <span className='flex font-bold text-lg'>{title}</span>
                        <span className='text-base'>{description}</span>
                    </div>
                </Radio>

            </div>
        )
    }

    function renderListOptions() {
        return (
            <RadioGroup defaultValue={PromotionPlan.WEEK_PROMOTION}>
                <Stack spacing={5} direction='column'>
                    {/*renderSelectorItem("FREE Promotion", "Your app will be listed for free in the next day of your submission", PromotionPlan.FREE)*/}
                    {renderSelectorItem("🚀 1 Week Exclusive Sponsor - $50", "Get your startup promoted on multiple premium and exclusive spots and on the top of our Daily Newsletter for 1 WEEK.", PromotionPlan.WEEK_PROMOTION)}
                    {renderSelectorItem((<>🚀🚀 1 Month Exclusive Sponsor - <s>$200</s> $125</>), "Get your startup promoted on multiple premium and exclusive spots and on the top of our Daily Newsletter for 1 Month.", PromotionPlan.MONTH_PROMOTION)}
                </Stack>
            </RadioGroup>
        )
    }

    async function onBuyBtnClicked() {
        await ClientRealtimeDbUtils.writeData({
            collectionName: CustomRealtimeCollectionDB.SponsoredStartupsClicked,
            docId: props.startupData.uid!,
            data: Date.now(),
            merge: false,
        });
        onClose();
    }

    let selectedPlanLink: string = WEEK_PROMOTION_LINK;
    switch (selectedPlan) {
        case PromotionPlan.WEEK_PROMOTION:
            selectedPlanLink = WEEK_PROMOTION_LINK;
            break;
        case PromotionPlan.MONTH_PROMOTION:
            selectedPlanLink = MONTH_PROMOTION_LINK;
            break;
    }

    return (
        <Modal isOpen={isOpen} onClose={onClose} size={"xl"} closeOnEsc={false} closeOnOverlayClick={false}>
            <ModalOverlay />
            <ModalContent>
                <ModalHeader>
                    <InlineLabelSeparatorComponent title={"Get more traffic"} subtitle={"Select on of our sponsorship options to become our exclusive sponsor and get more traffic instantly"} />
                </ModalHeader>
                <ModalBody>
                    {renderListOptions()}
                </ModalBody>

                <ModalFooter className='flex flex-row gap-2'>
                    <button className='btn btn-xs btn-ghost' onClick={() => {
                        onClose();
                    }}>Skip for now</button>

                    <Link className='btn btn-success' href={selectedPlanLink} target={"_blank"} onClick={onBuyBtnClicked}>Get Promoted</Link>
                </ModalFooter>
            </ModalContent>
        </Modal>
    )
}