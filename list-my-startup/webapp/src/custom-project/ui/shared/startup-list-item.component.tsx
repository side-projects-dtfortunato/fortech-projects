import Link from "next/link";
import {StartupDetailsModel} from "../../data/model/startup-details.model";
import {CustomRoutesUtils} from "../../utils/custom-routes.utils";
import StartupCategoryLabelComponent from "./StartupCategoryLabel.component";

export default function StartupListItemComponent(props: {listStartup: StartupDetailsModel}) {

    function renderTitle() {
        return <span className='text-slate-700 text-start font-light flex-wrap'>
            <span className='font-bold text-black'>{props.listStartup.name}</span> - {props.listStartup.tagline}</span>
    }

    function renderCategories() {
        let listCat: any[] = props.listStartup.categories.map((cat, index) => {
            return (
                <StartupCategoryLabelComponent categoryLabel={cat} key={index} />
            )
        });

        return (
            <div className='flex flex-wrap gap-x-2'>
                {listCat}
            </div>
        )
    }

    return (
        <div className='flex flex-row gap-5 items-center rounded-xl hover:bg-gradient-to-tr from-orange-50 to-transparent'>
            <img className='flex rounded-xl min-w-16 w-16 sm:min-w-20 sm:w-20' src={props.listStartup.media.logo?.fileUrl!} alt={props.listStartup.name} />
            <Link className='flex flex-col gap-1 grow' href={CustomRoutesUtils.getStartupDetailsUrl(props.listStartup.uid!)}>
                {renderTitle()}
                {renderCategories()}
            </Link>
        </div>
    )
}