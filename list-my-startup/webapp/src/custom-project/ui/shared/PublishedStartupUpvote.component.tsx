import {PublishedStartupListModel} from "../../data/model/published-startup-list.model";
import {useAuthState} from "react-firebase-hooks/auth";
import {
    auth,
    FirebaseClientFirestoreUtils
} from "../../../components/react-shared-module/logic/shared-data/firebase.utils";
import {useEffect, useState} from "react";
import AuthManager from "../../../components/react-shared-module/logic/auth/auth.manager";
import {BiSolidUpArrow} from "react-icons/bi";
import {CustomFirestoreCollectionDB, CustomFirestoreDocsDB} from "../../data/custom-firestore-collection-names";
import {useRouter} from "next/router";
import {SharedRoutesUtils} from "../../../components/react-shared-module/utils/shared-routes.utils";
import {deleteField} from "@firebase/firestore";
import {StartupDetailsUtils} from "../../data/model/startup-details.model";

export default function PublishedStartupUpvoteComponent(props: { listedStartup: PublishedStartupListModel, style: "SMALL" | "NORMAL", hideVotes?: boolean}) {
    const [currentUser, isAuthLoading] = useAuthState(auth);
    const [hasUpvoted, setHasUpvoted] = useState(Object.keys(props.listedStartup.upvotes).includes(AuthManager.getUserId() ? AuthManager.getUserId()! : ""));
    const router = useRouter();

    useEffect(() => {
        if (AuthManager.getUserId()) {
            setHasUpvoted(Object.keys(props.listedStartup.upvotes).includes(AuthManager.getUserId()!))
        }
    }, [currentUser]);

    async function onUpvoteClick() {
        // Backend API
        if (AuthManager.getUserId()) {
            let updateUserVote: boolean = !hasUpvoted;


            if (updateUserVote) {
                props.listedStartup.upvotes = {
                    ...props.listedStartup.upvotes,
                    [AuthManager.getUserId()!]: Date.now(),
                };
            } else {
                delete props.listedStartup.upvotes[AuthManager.getUserId()!];
            }
            setHasUpvoted(updateUserVote);

            const result = await FirebaseClientFirestoreUtils.updatePathDocument(
                [CustomFirestoreCollectionDB.StartupDetails, CustomFirestoreDocsDB.StartupDetails.PublicPublished],
                [props.listedStartup.uid!, props.listedStartup.dailyPublicationId!], {
                    upvotes: {
                        [AuthManager.getUserId()!]: updateUserVote ? Date.now() : deleteField(),
                    },
                });
            if (!result) {
                setHasUpvoted(!updateUserVote);
            }
        } else {
            // If not logged in, then redirect him to the login page
            router.push(SharedRoutesUtils.getLoginPageUrl());
        }

    }

    if (props.style === "SMALL") {
        return (<button className={"btn btn-primary " + (hasUpvoted ? "text-white" : "btn-outline")}
                        onClick={onUpvoteClick}>
            <div className='flex flex-col gap-1 items-center'>
                <BiSolidUpArrow/> {props.hideVotes ? "-" : StartupDetailsUtils.getTotalPublishedStartup(props.listedStartup)}
            </div>
        </button>)
    } else {
        return (<button className={"btn btn-sm btn-primary items-center " + (hasUpvoted ? "text-white" : "btn-outline")}
                        onClick={onUpvoteClick}>
            <BiSolidUpArrow/> {props.hideVotes ? "-" : StartupDetailsUtils.getTotalPublishedStartup(props.listedStartup) + (hasUpvoted ? " Upvoted" : " Upvote")}
        </button>)
    }

}