import {useState} from "react";
import {DailyPublishedStartupsModel} from "../../data/model/daily-published-startups.model";
import PublishedStartupsListContainerComponent
    from "../published-startups-list/published-startups-list-container.component";
import InfiniteScroll from 'react-infinite-scroll-component';
import SpinnerComponent from "../../../components/react-shared-module/ui-components/shared/Spinner.component";
import {FirebaseClientFirestoreUtils} from "../../../components/react-shared-module/logic/shared-data/firebase.utils";
import {CustomFirestoreCollectionDB} from "../../data/custom-firestore-collection-names";
import NewsletterSignupListBannerComponent
    from "../../../components/react-shared-module/ui-components/newsletter/shared/newsletter-signup-list-banner.component";
import {
    BasePostDataModel
} from "../../../components/react-shared-module/logic/shared-data/datamodel/base-post-data.model";
import {
    PostFormArticleBlocksDetails
} from "../../../components/react-shared-module/logic/shared-data/datamodel/posts-model/post-article-blocks";
import InlineLabelSeparatorComponent
    from "../../../components/react-shared-module/ui-components/shared/inline-label-separator.component";
import BlogHorizontalCarouselComponent
    from "../../../components/react-shared-module/ui-components/blog/shared/blog-horizontal-carousel.component";
import {PublishedStartupListModel} from "../../data/model/published-startup-list.model";
import {
    ArticleImportedModel
} from "../../../components/react-shared-module/base-projects/articles-digest/data/model/ArticleImported.model";
import ArticlesHorizontalCarouselComponent
    from "../../../components/react-shared-module/base-projects/articles-digest/ui-components/shared/articles-horizontal-carousel.component";

export default function PublishedDailyStartupsPaginationPage(props: {preloadedData: DailyPublishedStartupsModel[], itemsPerPage: number, listRecentArticles: ArticleImportedModel[]}) {
    const [listPublishedDays, setListPublishedDays] = useState<DailyPublishedStartupsModel[]>(props.preloadedData);
    const [hasMorePages, setHasMorePages] = useState(props.preloadedData.length >= props.itemsPerPage);


    async function loadNextPage() {
        // BackendAPI
        let lastDocId: string | undefined = listPublishedDays.length > 0 ? listPublishedDays[listPublishedDays.length - 1].publishedDayId : undefined;
        const response: any[] = await FirebaseClientFirestoreUtils
            .getDocumentsWithPagination(CustomFirestoreCollectionDB.DailyLaunches, props.itemsPerPage, "publishedDayId", lastDocId);

        if (response && response.length > 0) {
            setListPublishedDays(listPublishedDays.concat(response));
            setHasMorePages(response.length === props.itemsPerPage); // If the response has the same number of items per page, means that has possible more pages
        } else {
            setHasMorePages(false);
        }
    }

    function renderBlogPostsCarousel() {
        if (props.listRecentArticles && props.listRecentArticles.length > 0) {
            return (
                <div className='flex flex-col gap-2 w-full max-w-max'>
                    <InlineLabelSeparatorComponent title={"Recent Articles"} />
                    <ArticlesHorizontalCarouselComponent listPosts={props.listRecentArticles} />
                </div>
            )
        } else {
            return (
                <></>
            )
        }

    }

    function renderDailyPublishedStartupsContainers() {
        return listPublishedDays.filter((listItem) => listItem.listStartups).map((listItem, index) => {
            return (
                <div key={index} className='flex flex-col gap-5'>
                    <PublishedStartupsListContainerComponent key={listItem.publishedDayId} listStartups={Object.values(listItem.listStartups)} publishedDay={Number(listItem.publishedDayId)} hideVotes={listItem.hideVotes} linkable={true} />
                    {
                        index === 0 ? renderBlogPostsCarousel() : <></>
                    }
                    {
                        index === 0 || index === 4 ? <NewsletterSignupListBannerComponent /> : <></>
                    }
                </div>
            )
        })
    }

    return (
        <InfiniteScroll next={loadNextPage} hasMore={hasMorePages} loader={<SpinnerComponent/>} dataLength={listPublishedDays.length}>
            {renderDailyPublishedStartupsContainers()}
        </InfiniteScroll>
    )
}