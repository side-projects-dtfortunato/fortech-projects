import InlineLabelSeparatorComponent
    from "../../../../components/react-shared-module/ui-components/shared/inline-label-separator.component";
import StartupCategoryLabelComponent from "../../shared/StartupCategoryLabel.component";

export default function LandingPopularCategoriesComponent(props: {categories?: {[category: string]: number}, maxLength: number}) {

    function renderCategories() {
        if (!props.categories) {
            return <></>;
        }
        return Object.keys(props.categories!)
            .sort((c1, c2) => props.categories![c2] - props.categories![c1])
            .filter((c, index) => index < props.maxLength)
            .map((category) => <StartupCategoryLabelComponent key={category} categoryLabel={category} />)
    }

    if (props.categories) {
        return (
            <div className='flex flex-col gap-2'>
                <InlineLabelSeparatorComponent title={"Popular Categories"} />
                <div className='flex flex-wrap gap-3'>
                    {renderCategories()}
                </div>
            </div>
        )
    } else {
        return <></>
    }

}