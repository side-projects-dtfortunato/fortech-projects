import {PublishedStartupListModel} from "../../data/model/published-startup-list.model";
import PublishedStartupHeaderComponent from "./shared/published-startup-header.component";
import PublishedStartupSimilarListComponent from "./shared/published-startup-similar-list.component";
import PostCommentsRootComponent
    from "../../../components/react-shared-module/base-projects/community/ui/post-comments-root.component";
import {
    SharedFirestoreCollectionDB
} from "../../../components/react-shared-module/logic/shared-data/datamodel/shared-firestore-collections";
import RootConfigs from "../../../configs";
import {CustomRoutesUtils} from "../../utils/custom-routes.utils";
import {CustomFirestoreCollectionDB} from "../../data/custom-firestore-collection-names";

export default function PublishedStartupListDetailsPage(props: {startupListDetails: PublishedStartupListModel}) {

    return (
        <div className='flex flex-col gap-10 p-5 sm:my-10'>
            <PublishedStartupHeaderComponent startupListDetails={props.startupListDetails} startupData={props.startupListDetails.startup} />
            <div id={"comments"} className='mt-5'>
                <PostCommentsRootComponent
                    communityParentRefs={{parentCollectionId: CustomFirestoreCollectionDB.StartupDetails, parentDocId: props.startupListDetails.startup.uid + "-" + props.startupListDetails.dailyPublicationId}}
                    attachedItem={{title: props.startupListDetails.startup.name, link: RootConfigs.BASE_URL + CustomRoutesUtils.getStartupListedDetails(props.startupListDetails.startup.uid!, props.startupListDetails.dailyPublicationId!), thumbnailUrl: props.startupListDetails.startup.media.logo?.fileUrl!}}/>
            </div>
            <PublishedStartupSimilarListComponent startupData={props.startupListDetails.startup} />
        </div>
    )
}