import React, { useState, useCallback } from 'react';
import ReactPlayer from 'react-player';
import Slider from 'react-slick';
import { motion, AnimatePresence } from 'framer-motion';
import { ChevronLeft, ChevronRight, Play, X } from 'lucide-react';
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";

interface MediaGalleryProps {
    videoUrl?: string;
    imageUrls: string[];
    aspectRatio?: 'square' | 'video' | '4:3';
    autoplaySpeed?: number;
}

interface ImagePopupProps {
    images: string[];
    currentIndex: number;
    onClose: () => void;
    onNavigate: (index: number) => void;
}

const isValidUrl = (url: string): boolean => {
    try {
        new URL(url);
        return true;
    } catch {
        return false;
    }
};

const updateImageDimensions = (url: string, width: string, height: string): string => {
    try {
        const urlObj = new URL(url);
        urlObj.searchParams.set('w', width);
        urlObj.searchParams.set('h', height);
        return urlObj.toString();
    } catch {
        return url;
    }
};

const ImagePopup: React.FC<ImagePopupProps> = ({ images, currentIndex, onClose, onNavigate }) => {
    return (
        <motion.div
            initial={{ opacity: 0 }}
            animate={{ opacity: 1 }}
            exit={{ opacity: 0 }}
            className="fixed inset-0 z-50 flex items-center justify-center bg-black bg-opacity-90"
            onClick={onClose}
        >
            <button
                className="absolute top-4 right-4 text-white hover:text-gray-300 transition-colors"
                onClick={onClose}
            >
                <X size={24} />
            </button>

            <button
                className="absolute left-4 top-1/2 -translate-y-1/2 text-white hover:text-gray-300 transition-colors"
                onClick={(e) => {
                    e.stopPropagation();
                    onNavigate(currentIndex - 1 < 0 ? images.length - 1 : currentIndex - 1);
                }}
            >
                <ChevronLeft size={32} />
            </button>

            <motion.img
                key={images[currentIndex]}
                initial={{ opacity: 0, scale: 0.9 }}
                animate={{ opacity: 1, scale: 1 }}
                exit={{ opacity: 0, scale: 0.9 }}
                transition={{ duration: 0.2 }}
                src={updateImageDimensions(images[currentIndex], '1200', '800')}
                className="max-w-[90vw] max-h-[90vh] object-contain"
                onClick={(e) => e.stopPropagation()}
                loading="eager"
            />

            <button
                className="absolute right-4 top-1/2 -translate-y-1/2 text-white hover:text-gray-300 transition-colors"
                onClick={(e) => {
                    e.stopPropagation();
                    onNavigate(currentIndex + 1 >= images.length ? 0 : currentIndex + 1);
                }}
            >
                <ChevronRight size={32} />
            </button>
        </motion.div>
    );
};

const MediaGallery: React.FC<MediaGalleryProps> = ({
                                                       videoUrl,
                                                       imageUrls,
                                                       aspectRatio = 'video',
                                                       autoplaySpeed = 5000,
                                                   }) => {
    const [isPopupOpen, setIsPopupOpen] = useState(false);
    const [currentImageIndex, setCurrentImageIndex] = useState(0);
    const [isLoading, setIsLoading] = useState(true);

    const getAspectRatioClass = () => {
        switch (aspectRatio) {
            case 'square': return 'aspect-square';
            case '4:3': return 'aspect-4/3';
            default: return 'aspect-video';
        }
    };

    const handleImageClick = useCallback((index: number) => {
        setCurrentImageIndex(index);
        setIsPopupOpen(true);
    }, []);

    const validImageUrls = imageUrls.filter(isValidUrl);

    const sliderSettings = {
        dots: true,
        infinite: true,
        centerMode: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed,
        pauseOnHover: true,
        adaptiveHeight: true,
        nextArrow: <ChevronRight className="text-gray-800 w-6 h-6" />,
        prevArrow: <ChevronLeft className="text-gray-800 w-6 h-6" />,
        arrows: true,
        responsive: [
            {
                breakpoint: 640,
                settings: {
                    centerMode: false,
                    arrows: false
                }
            }
        ]
    };

    const renderSlide = (content: React.ReactNode, key: string) => (
        <div key={key} className="px-2">
            <div className={`relative overflow-hidden rounded-lg shadow-lg ${getAspectRatioClass()} bg-gray-100`}>
                {content}
            </div>
        </div>
    );

    const renderGallery = () => {
        const slides: React.ReactNode[] = [];

        if (videoUrl && isValidUrl(videoUrl)) {
            slides.push(
                renderSlide(
                    <div className="relative w-full h-full bg-black">
                        <ReactPlayer
                            url={videoUrl}
                            width="100%"
                            height="100%"
                            controls
                            playIcon={<Play className="w-12 h-12 text-white" />}
                            light
                            playing={false}
                        />
                    </div>,
                    videoUrl
                )
            );
        }

        validImageUrls.forEach((imageUrl, index) => {
            const optimizedImageUrl = updateImageDimensions(imageUrl, '600', '600');
            slides.push(
                renderSlide(
                    <motion.div
                        initial={{ opacity: 0 }}
                        animate={{ opacity: 1 }}
                        transition={{ duration: 0.3 }}
                        className="w-full h-full cursor-pointer group"
                        onClick={() => handleImageClick(index)}
                    >
                        <img
                            src={optimizedImageUrl}
                            alt=""
                            className="w-full h-full object-cover transition-transform duration-300 group-hover:scale-105"
                            onLoad={() => setIsLoading(false)}
                            loading="lazy"
                        />
                    </motion.div>,
                    imageUrl
                )
            );
        });

        return slides;
    };

    return (
        <div className="relative w-full">
            <div className={`w-full ${isLoading ? 'animate-pulse' : ''}`}>
                <Slider {...sliderSettings}>
                    {renderGallery()}
                </Slider>
            </div>

            <AnimatePresence>
                {isPopupOpen && (
                    <ImagePopup
                        images={validImageUrls}
                        currentIndex={currentImageIndex}
                        onClose={() => setIsPopupOpen(false)}
                        onNavigate={setCurrentImageIndex}
                    />
                )}
            </AnimatePresence>
        </div>
    );
};

export default MediaGallery;