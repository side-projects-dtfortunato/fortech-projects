import {StartupDetailsModel} from "../../../data/model/startup-details.model";
import InlineLabelSeparatorComponent
    from "../../../../components/react-shared-module/ui-components/shared/inline-label-separator.component";
import StartupListItemComponent from "../../shared/startup-list-item.component";

export default function PublishedStartupSimilarListComponent(props: {startupData: StartupDetailsModel}) {

    if (!props.startupData.similarStartups || props.startupData.similarStartups.length === 0) {
        return <></>;
    } else {
        return (
            <div className='flex flex-col gap-4 w-full'>
                <InlineLabelSeparatorComponent title={"Related Startups"} />
                {props.startupData.similarStartups.map((startupItem) => {
                    return <StartupListItemComponent key={startupItem.uid!} listStartup={startupItem} />
                })}
            </div>
        )
    }
}