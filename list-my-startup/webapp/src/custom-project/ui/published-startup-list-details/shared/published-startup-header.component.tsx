import {PublishedStartupListModel, PublishedUserInfos} from "../../../data/model/published-startup-list.model";
import Image from "next/image";
import Link from "next/link";
import BaseAuthorInfosComponent
    from "../../../../components/react-shared-module/ui-components/shared/BaseAuthorInfos.component";
import BaseTextComponent from "../../../../components/react-shared-module/ui-components/shared/BaseText.component";
import PublishedStartupMediaGalleryComponent from "./published-startup-media-gallery.component";
import BaseRootContainerComponent
    from "../../../../components/react-shared-module/ui-components/shared/BaseRootContainer.component";
import PublishedStartupUpvoteComponent from "../../shared/PublishedStartupUpvote.component";
import StartupCategoryLabelComponent from "../../shared/StartupCategoryLabel.component";
import {StartupDetailsModel} from "../../../data/model/startup-details.model";
import {CustomRoutesUtils} from "../../../utils/custom-routes.utils";
import CommonUtils from "../../../../components/react-shared-module/logic/commonutils";
import RootConfigs from "../../../../configs";
import Linkify from "react-linkify";
import GoogleAdsInFeedComponent
    from "../../../../components/react-shared-module/ui-components/ads/GoogleAdsInFeed.component";


export default function PublishedStartupHeaderComponent(props: { startupData: StartupDetailsModel, startupListDetails?: PublishedStartupListModel }) {

    function renderStartupTitle() {
        const tags: any[] = props.startupData.tags.map((tag) => {
            return (
                <span key={tag} className="text-slate-800 text-sm">#{tag}</span>
            )
        });
        const titleElement: any = props.startupListDetails ?
            <Link className='hover:text-orange-600' href={CustomRoutesUtils.getStartupDetailsUrl(props.startupData.uid!)}><h1
                className='text-xl sm:text-2xl font-bold'>{props.startupData.name}</h1></Link> :
            <h1 className='text-xl sm:text-2xl font-bold'>{props.startupData.name}</h1>;


        return (
            <div className='flex flex-col sm:flex-row  gap-5 justify-start items-center sm:items-start w-full'>
                <img className='flex rounded-xl min-w-20 w-20 sm:min-w-24 sm:w-24 min-h-20 h-20 sm:min-h-24 sm:h-24 object-cover' src={props.startupData.media.logo?.fileUrl!} alt={props.startupData.name} />
                <div className='flex-col gap-3 items-center sm:items-start grow'>
                    {titleElement}
                    <h2 className='text-xl sm:text-2xl font-light text-slate-400'>{props.startupData.tagline}</h2>
                    <div className='flex flex-wrap gap-2'>
                        {tags}
                    </div>
                </div>
                {renderVisitUpvoteBtns()}
            </div>
        )
    }

    function renderVisitUpvoteBtns() {
        const visitUrl: string = CommonUtils.replaceQParamOnURL(props.startupData.link, "ref", RootConfigs.SITE_NAME);

        return (
            <div className='flex flex-row sm:flex-col gap-5'>
                <Link href={visitUrl} className='btn btn-sm btn-outline btn-secondary' target={"_blank"}>VISIT
                    STARTUP</Link>
                {
                    props.startupListDetails ?
                        <PublishedStartupUpvoteComponent listedStartup={props.startupListDetails}
                                                         style={"NORMAL"}/> : <></>
                }
            </div>
        )
    }

    function renderDescription() {
        return (
            <BaseTextComponent text={props.startupData.description}
                               extraClassName={"w-full my-5 text-center sm:text-start"}/>
        )
    }

    function renderCategories() {
        let listCat: any[] = props.startupData.categories.map((cat) => {
            return (
                <StartupCategoryLabelComponent categoryLabel={cat} key={cat}/>
            )
        });

        return (
            <div className='flex flex-wrap gap-2 gap-y-2 items-center'>
                <span className='text-base text-slate-500'>Categories: </span>
                {listCat}
            </div>
        )
    }

    function renderCreatedBy() {

        if (props.startupData.isAutoImport === true) {
            return (<></>);
        }
        let creatorProfile: PublishedUserInfos | undefined = props.startupData.creatorUserInfos;

        if (props.startupListDetails) {
            creatorProfile = props.startupListDetails.relatedUserInfos[props.startupData.creatorUid];
        }

        if (creatorProfile) {
            return (
                <div className='flex flex-row gap-2 items-center my-5'>
                    <BaseTextComponent text={"Created By"}/>
                    <BaseAuthorInfosComponent pictureUrl={creatorProfile.photoUrl} name={creatorProfile.name!}
                                              subtitle={"@" + creatorProfile.username}/>
                </div>
            )
        } else {
            return <></>
        }

    }

    function renderGallery() {
        return (
            <PublishedStartupMediaGalleryComponent
                imageUrls={props.startupData.media.screenshots.map((file) => file.fileUrl)}
                videoUrl={props.startupData.media.videoLink}/>
        )
    }

    return (
        <div className="flex flex-col gap-3 items-center sm:items-center">
            <BaseRootContainerComponent>{renderStartupTitle()}</BaseRootContainerComponent>
            <BaseRootContainerComponent>{renderDescription()}</BaseRootContainerComponent>
            <BaseRootContainerComponent>{renderCategories()}</BaseRootContainerComponent>
            <BaseRootContainerComponent>{renderCreatedBy()}</BaseRootContainerComponent>
            <BaseRootContainerComponent extraLarge={true}>{renderGallery()}</BaseRootContainerComponent>
            <GoogleAdsInFeedComponent />
        </div>
    )
}