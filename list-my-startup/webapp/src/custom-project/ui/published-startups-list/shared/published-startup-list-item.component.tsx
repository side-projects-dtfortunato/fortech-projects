import {PublishedStartupListModel} from "../../../data/model/published-startup-list.model";
import PublishedStartupUpvoteComponent from "../../shared/PublishedStartupUpvote.component";
import Link from "next/link";
import {CustomRoutesUtils} from "../../../utils/custom-routes.utils";
import StartupCategoryLabelComponent from "../../shared/StartupCategoryLabel.component";

export default function PublishedStartupListItemComponent(props: {listStartup: PublishedStartupListModel, hideVotes?: boolean}) {

    function renderTitle() {
        return <span className='text-slate-700 text-start font-light flex-wrap'>
            <span className='font-bold text-black'>{props.listStartup.startup.name}</span> - {props.listStartup.startup.tagline}</span>
    }

    function renderCategories() {
        let listCat: any[] = props.listStartup.startup.categories.map((cat, index) => {
            return (
                <StartupCategoryLabelComponent categoryLabel={cat} key={index} />
            )
        });

        return (
            <div className='flex flex-wrap gap-x-2'>
                {listCat}
            </div>
        )
    }

    const startupLink: string = CustomRoutesUtils.getStartupListedDetails(props.listStartup.startup.uid!, props.listStartup.dailyPublicationId!);

    return (
        <div className='flex flex-row gap-5 items-center rounded-xl hover:bg-gradient-to-tr from-orange-50 to-transparent'>
            <Link href={startupLink}>
                <img className='flex rounded-xl min-w-16 w-16 sm:min-w-20 sm:w-20 min-h-16 h-16 sm:min-h-20 sm:h-20 object-cover' src={props.listStartup.startup.media.logo?.fileUrl!} alt={props.listStartup.startup.name} />
            </Link>
            <div className='flex flex-col gap-1 grow'>
                <Link href={startupLink}>
                    {renderTitle()}
                </Link>
                {renderCategories()}
            </div>
            <PublishedStartupUpvoteComponent listedStartup={props.listStartup} style={"SMALL"} hideVotes={props.hideVotes} />
        </div>
    )
}