import {PublishedStartupListModel} from "../../data/model/published-startup-list.model";
import LandingPageSecondaryTitleComponent
    from "../../../components/react-shared-module/ui-components/saas-landing-page/shared/landing-page-secondary-title.component";
import PublishedStartupListItemComponent from "./shared/published-startup-list-item.component";
import InlineLabelSeparatorComponent
    from "../../../components/react-shared-module/ui-components/shared/inline-label-separator.component";
import CommonUtils from "../../../components/react-shared-module/logic/commonutils";
import {StartupDetailsUtils} from "../../data/model/startup-details.model";
import Link from "next/link";
import {CustomRoutesUtils} from "../../utils/custom-routes.utils";

export default function PublishedStartupsListContainerComponent(props: {listStartups: PublishedStartupListModel[], publishedDay: number, maxToDisplay?: number, hideVotes?: boolean, linkable?: boolean}) {
    let dateStr: string = "";
    let publishedDate: Date = new Date(CommonUtils.convertDaytimestampToMillis(props.publishedDay));

    // Is Today
    if (props.publishedDay === CommonUtils.getTodayDay()) {
        dateStr = "Launching Today";
    } else if (props.publishedDay === (CommonUtils.getTodayDay() - 1)) {
        dateStr = "Launched Yesterday";
    } else {
        dateStr = "Launched " + CommonUtils.getTimeAgo(publishedDate.getTime());
    }

    function renderTitle() {
        if (props.linkable) {
            return (
                <Link href={CustomRoutesUtils.getPublishedStartupsDaily(props.publishedDay.toString())} className='hover:text-orange-400'>
                    <InlineLabelSeparatorComponent title={"Best Startups " + dateStr} />
                </Link>
            )
        } else {
            return (
                <InlineLabelSeparatorComponent title={"Best Startups " + dateStr} />
            )
        }
    }

    return (
        <div className='flex flex-col gap-4 px-5 py-10 sm:px-0'>
            {renderTitle()}
            {props.listStartups
                .sort((s1, s2) => StartupDetailsUtils.getTotalPublishedStartup(s2) - StartupDetailsUtils.getTotalPublishedStartup(s1))
                .map((listStartupItem) => <PublishedStartupListItemComponent key={listStartupItem.uid!} listStartup={listStartupItem} hideVotes={props.hideVotes} />)}
        </div>
    )
}