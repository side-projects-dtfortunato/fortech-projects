import {
    BasePostDataModel
} from "../../../components/react-shared-module/logic/shared-data/datamodel/base-post-data.model";
import {useEffect, useState} from "react";
import {SharedBackendApi} from "../../../components/react-shared-module/logic/shared-data/sharedbackend.api";
import {PageHeadProps} from "../../../components/react-shared-module/ui-components/root/RootLayout.component";
import RootConfigs from "../../../configs";
import {SharedUtils} from "../../../components/react-shared-module/utils/shared-utils";
import BlogPostListItemComponent
    from "../../../components/react-shared-module/ui-components/blog/shared/blog-post-list-item.component";
import CustomRootLayoutComponent from "../../../components/app-components/root/CustomRootLayout.component";
import BlogPostContentComponent
    from "../../../components/react-shared-module/ui-components/blog/content/blog-post-content.component";
import {PostTypes} from "../../../components/react-shared-module/utils/shared-ui-helper";
import LandingPageGetListedBannerComponent from "../shared/LandingPageGetListedBanner.component";
import {CustomRoutesUtils} from "../../utils/custom-routes.utils";

export default function PostDetailsPageContentCustomComponent(props: {postData?: BasePostDataModel, listSimilarPosts?: BasePostDataModel[]}) {
    const [postData, setPostData] = useState(props.postData);
    let hasPushedViewEvent: boolean = false;

    useEffect(() => {
        // Update views
        if (props.postData?.uid && !hasPushedViewEvent) {
            hasPushedViewEvent = true;
            SharedBackendApi.pushPostPageView({postId: props.postData?.uid});
        }
    });

    const pageHeadProps: PageHeadProps = {
        title: props.postData?.title ? props.postData?.title : RootConfigs.SITE_TITLE,
        description: props.postData?.summary ? props.postData.summary : RootConfigs.SITE_DESCRIPTION,
        publishedTime: new Date(props.postData?.publishedAt!).toUTCString(),
        imgUrl: props.postData?.thumbnailUrl,
        jsonLdMarkup: props.postData ? SharedUtils.getSchemaMarkupFromPost(props.postData) : undefined,
    };


    function renderSimilarPosts() {
        if (!props.listSimilarPosts || props.listSimilarPosts?.length === 0) {
            return <></>;
        }

        return (
            <div className='flex w-full justify-center items-center'>
                <div className='flex flex-col mt-5 gap-5 w-full max-w-3xl'>
                    <div className='divider'/>
                    <h3 className='text-xl'>Related Articles</h3>
                    {props.listSimilarPosts.map((post) => <BlogPostListItemComponent key={postData?.uid!} postData={postData!} type={"normal_list"} />)}
                </div>
            </div>
        )
    }

    const isIndexable: boolean = props.postData?.postType !== PostTypes.SHARE_LINK;
    return (
        <CustomRootLayoutComponent pageHeadProps={pageHeadProps} isIndexable={isIndexable} customBody={true}>
            <div className='w-full flex flex-col gap-5'>
                <BlogPostContentComponent postDetails={props.postData!} />
                <LandingPageGetListedBannerComponent />
                {renderSimilarPosts()}
            </div>
        </CustomRootLayoutComponent>
    )
}