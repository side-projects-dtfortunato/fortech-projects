import {StartupCategories, StartupDetailsModel, StartupDetailsUtils} from "../../../data/model/startup-details.model";
import BaseStepFormContainerComponent
    from "../../../../components/react-shared-module/ui-components/shared/BaseStepFormContainer.component";
import InputTextComponent from "../../../../components/react-shared-module/ui-components/form/InputText.component";
import InputTextAreaComponent
    from "../../../../components/react-shared-module/ui-components/form/InputTextArea.component";
import InputListLinksFormComponent, {
    ListLinkFormItemModel
} from "../../../../components/react-shared-module/ui-components/form/InputListLinksForm.component";
import CommonUtils from "../../../../components/react-shared-module/logic/commonutils";
import {MultiSelect} from "react-multi-select-component";
import InputTagsFieldComponent
    from "../../../../components/react-shared-module/ui-components/form/InputTagsField.component";
import UploadImageComponent from "../../../../components/react-shared-module/ui-components/form/UploadImage.component";
import UploadMultipleImagesComponent
    from "../../../../components/react-shared-module/ui-components/form/UploadMultipleImages.component";
import InlineLabelSeparatorComponent
    from "../../../../components/react-shared-module/ui-components/shared/inline-label-separator.component";
import {useEffect, useState} from "react";
import {CustomBackendApi} from "../../../api/custombackend.api";
import {
    RootGlobalStateActions,
    useAlertMessageGlobalState
} from "../../../../components/react-shared-module/logic/global-hooks/root-global-state";
import SpinnerComponent from "../../../../components/react-shared-module/ui-components/shared/Spinner.component";
import {useRouter} from "next/router";
import {CustomRoutesUtils} from "../../../utils/custom-routes.utils";
import {CustomProjectUtils} from "../../../utils/custom-project.utils";
import {ClientRealtimeDbUtils} from "../../../../components/react-shared-module/logic/shared-data/firebase.utils";
import {CustomRealtimeCollectionDB} from "../../../data/custom-firestore-collection-names";

export default function SubmitStartupFormMainInfosComponent(props: { startupData: StartupDetailsModel, onDataChanged: (startupData: StartupDetailsModel) => void, onSubmitStartupClicked: () => void }) {
    const [isLoading, setIsLoading] = useState(false);
    const [alertMessage, setAlertMessage] = useAlertMessageGlobalState();
    const [listCategories, setListCategories] = useState(StartupCategories);
    const router = useRouter();

    useEffect(() => {
        // Load Categories
        ClientRealtimeDbUtils.getData({
            collectionName: CustomRealtimeCollectionDB.SubmitStartupCategories,
        }).then((response) => {
            if (response) {
                Object.keys(response).forEach((cat) => {
                    if (!CommonUtils.checkArrayIncludesNoCaseSensitive(listCategories, cat)) {
                        listCategories.push(cat);
                    }
                });
                setListCategories([...listCategories]);
            }
        });
    }, []);



    async function onSubmitStartupClicked() {
        setIsLoading(true);
        const apiResponse = await CustomBackendApi.submitStartup({
            startupData: props.startupData,
        });

        if (apiResponse.isSuccess) {
            RootGlobalStateActions.displayAlertMessage({
                alertType: "SUCCESS",
                message: "Congrats, your Startup was successfully submitted",
                setAlertMessageState: setAlertMessage,
            });
            router.replace(CustomRoutesUtils.getPendingStartupUrl(apiResponse.responseData.uid));
        } else {
            RootGlobalStateActions.displayAPIResponseError({
                apiResponse: apiResponse,
                setAlertMessageState: setAlertMessage,
            });
            setIsLoading(false);
        }
        props.onDataChanged(StartupDetailsUtils.initStartupDataObj());

    }

    function onFieldChanged(fieldPath: string, text: string) {
        // @ts-ignore
        props.startupData[fieldPath] = text;
        props.onDataChanged(props.startupData);
    }

    function renderMainInfos() {
        return (
            <BaseStepFormContainerComponent title={"📝 Main Infos"}
                                            subtitle={"Tell us more about your startup"}>
                <div className='flex flex-col gap-5 justify-start items-start'>
                    <InputTextComponent topLabel={"Name of the Startup"}
                                        hint={"Tell us the name of your Startup/Product"}
                                        onChanged={(value) => onFieldChanged("name", value)} inputType={"text"}
                                        maxLength={25} value={props.startupData.name}/>

                    <InputTextComponent topLabel={"Tagline"} hint={"Concise and descriptive tagline for the startup"}
                                        onChanged={(value) => onFieldChanged("tagline", value)} inputType={"text"}
                                        maxLength={40} value={props.startupData.tagline}/>
                    <InputTextAreaComponent topLabel={"Description of the startup"}
                                            hint={"Short  description of your startup/product"}
                                            onChanged={(value) => onFieldChanged("description", value)} maxLength={260}
                                            inputType={"text"} value={props.startupData.description}/>
                </div>
            </BaseStepFormContainerComponent>
        )
    }

    function renderOtherLinks() {
        const defaultLinksList: ListLinkFormItemModel[] = [{
            link: props.startupData.link,
            label: props.startupData.name,
            disable: true,
        }];
        props.startupData.otherLinks.forEach((link) => {
            defaultLinksList.push({
                link: link.link,
                label: link.label,
                isValid: CommonUtils.isValidHttpUrl(link.link),
            });
        });

        return (
            <BaseStepFormContainerComponent title={"🔗 Other Links"}
                                            subtitle={"Add more links related to your product (App Stores, Social Pages, etc..)"}>
                <div className='flex flex-col gap-5 justify-start items-start'>
                    <InputListLinksFormComponent defaultLinks={defaultLinksList}
                                                 onListValidLinksChanged={(listValidLinks) => {
                                                     props.startupData.otherLinks = listValidLinks
                                                         .filter((link) => link.link !== props.startupData.link)
                                                         .map((link) => {
                                                             return {label: link.label, link: link.link}
                                                         });
                                                 }
                                                 }/>
                </div>
            </BaseStepFormContainerComponent>
        )
    }

    function renderProductCategories() {
        const disableMoreCats: boolean = props.startupData.categories.length >= 3;
        return (
            <BaseStepFormContainerComponent title={"🔎 Categories"}
                                            subtitle={"To let your product be discovered easily, select the categories of your product"}>
                <div className='flex flex-col gap-5 justify-start items-start w-full'>
                    <div className='flex flex-col w-full h-full items-stretch z-1 overscroll-auto overflow-y-visible'>
                        <span className='text-sm font-bold'>Startup Categories</span>
                        <span className='text-xs font-light mb-5 mt-1'>Up to 3 categories</span>
                        <MultiSelect
                            options={listCategories.sort((o1, o2) => o1.localeCompare(o2)).map((option) => {
                                return {
                                    key: option,
                                    value: option,
                                    label: option,
                                    disabled: disableMoreCats && !props.startupData.categories.includes(option)
                                }
                            })}
                            value={props.startupData.categories.map((option) => {
                                return {key: option, value: option, label: option}
                            })}
                            onChange={(selectedOptions: any[]) => {
                                props.startupData.categories = selectedOptions.map((option: any) => option.value);
                                props.onDataChanged(props.startupData);
                            }
                            }
                            hasSelectAll={false}
                            labelledBy="Select 3 categories for your Startup"

                        />
                    </div>
                    <InputTagsFieldComponent defaultTags={props.startupData.tags} onTagsChanged={(tags) => {
                        props.startupData.tags = tags;
                    }
                    } label={"Tags"} placeholder={"Enter to add a new tag. Max 4 tags"}
                                             disabled={props.startupData.tags.length >= 4}/>
                </div>
            </BaseStepFormContainerComponent>
        )
    }

    function renderMedia() {
        return (
            <BaseStepFormContainerComponent title={"📸 Media"}
                                            subtitle={"Let's choose some pictures and videos to demonstrate your product"}>
                <div className='flex flex-col gap-16'>
                    <UploadImageComponent size={75} title={"Startup Logo"} onImageBase64={(base64Image) => {
                        if (base64Image) {
                            props.startupData.media.logo = {
                                tempBase64: base64Image,
                                fileUrl: "",
                                storagePath: "",
                            };
                        }
                    }
                    }/>

                    <div className='flex flex-col gap-2'>
                        <InlineLabelSeparatorComponent title={"Gallery"}
                                                       subtitle={"Upload some screenshots of your startup/product."}/>
                        <UploadMultipleImagesComponent onSelectedImagesChanged={(selectedImages) => {
                            props.startupData.media.screenshots = selectedImages;
                            props.onDataChanged(props.startupData);
                        }} maxImages={4}/>
                    </div>

                    <div className='flex flex-col gap-2'>
                        <InlineLabelSeparatorComponent title={"Video"}
                                                       subtitle={"(Optional) Upload an optional video from YouTube or Loom to show people how to use your product or share your maker story. Video helps you connect with viewers."}/>
                        <InputTextComponent topLabel={"Video URL"} hint={"Link to your Startup Video"} inputType={"url"}
                                            onChanged={(value) => {
                                                props.startupData.media.videoLink = value;
                                                props.onDataChanged(props.startupData);
                                            }
                                            }/>
                    </div>
                </div>
            </BaseStepFormContainerComponent>
        )
    }

    function renderSubmitBtn() {
        return (
            <div className='flex my-5 w-full justify-center'>
                <button className='btn btn-primary' onClick={onSubmitStartupClicked} disabled={!CustomProjectUtils.isSubmitStartupValidToSubmit(props.startupData)}>Submit Startup</button>
            </div>
        )
    }

    if (isLoading) {
        return (
            <div className='flex flex-col gap-5 justify-center w-full my-20'>
                <h3 className='text-xl font-bold text-center'>{"We are submitting your startup, please be patient..."}</h3>
                <SpinnerComponent/>
            </div>
        )
    } else {
        return (
            <div className='flex flex-col'>
                {renderMainInfos()}
                <div className='divider'/>
                {renderOtherLinks()}
                <div className='divider'/>
                {renderProductCategories()}
                <div className='divider'/>
                {renderMedia()}
                <div className='divider'/>
                {renderSubmitBtn()}
            </div>
        )
    }
}