import {StartupDetailsModel} from "../../../data/model/startup-details.model";
import InputTextComponent from "../../../../components/react-shared-module/ui-components/form/InputText.component";
import {useState} from "react";
import CommonUtils from "../../../../components/react-shared-module/logic/commonutils";
import BaseStepFormContainerComponent
    from "../../../../components/react-shared-module/ui-components/shared/BaseStepFormContainer.component";

export default function SubmitStartupFormInitialUrlComponent(props: { startupData: StartupDetailsModel, onDataChanged: (startupData: StartupDetailsModel) => void, onGetStartedClicked: () => void }) {
    const [isGetStartedEnabled, setIsGetStartedEnabled] = useState(CommonUtils.isValidHttpUrl(props.startupData.link));

    function onUrlChanged(url: string) {
        props.startupData.link = url.trim();
        // Check if the link is starting by https, otherwise should add it
        if (!props.startupData.link.startsWith("https://") && !props.startupData.link.startsWith("http://")) {
            props.startupData.link = "https://" + props.startupData.link;
        }
        props.onDataChanged(props.startupData);
        setIsGetStartedEnabled(CommonUtils.isValidHttpUrl(props.startupData.link));
    }

    return (
        <BaseStepFormContainerComponent title={"Link Your Startup"}
                                        subtitle={"Let's start by sharing the link to your startup."}>
            <div className='flex flex-col gap-5 justify-start items-start'>
                <InputTextComponent topLabel={"Link to the startup"} hint={"e.g.: www.listmyproduct.app"}
                                    onChanged={onUrlChanged} inputType={"url"} value={props.startupData.link} onEnterPressed={props.onGetStartedClicked}/>
                <button className='btn btn-sm btn-primary btn-outline' disabled={!isGetStartedEnabled}
                        onClick={props.onGetStartedClicked}>Get Started</button>
            </div>
        </BaseStepFormContainerComponent>
    )
}