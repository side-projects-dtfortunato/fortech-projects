import CustomRootLayoutComponent from "../../../components/app-components/root/CustomRootLayout.component";
import {useEffect, useState} from "react";
import SubmitStartupFormInitialUrlComponent from "./steps/submit-startup-form-initial-url.component";
import {StartupDetailsModel, StartupDetailsUtils} from "../../data/model/startup-details.model";
import PageHeaderTitleComponent
    from "../../../components/react-shared-module/ui-components/shared/PageHeaderTitle.component";
import SubmitStartupFormMainInfosComponent from "./steps/submit-startup-form-main-infos.component";
import {useAuthState} from "react-firebase-hooks/auth";
import {auth} from "../../../components/react-shared-module/logic/shared-data/firebase.utils";
import AuthManager from "../../../components/react-shared-module/logic/auth/auth.manager";
import LoginRootComponent from "../../../components/react-shared-module/ui-components/login/LoginRoot.component";
import {useCentralUserDataState} from "../../../components/react-shared-module/logic/global-hooks/root-global-state";
import ProfileEditorPopupComponent from "../shared/ProfileEditorPopup.component";
import {CustomProjectUtils} from "../../utils/custom-project.utils";
import {Modal, ModalContent, ModalOverlay} from "@chakra-ui/modal";
import Link from "next/link";


// Inspired in: https://shuffle.dev/editor?project=a7f14ec929a762ab58302746a31fbcea04dff770&tutorial=1

type SubmitStartupFormStep = "INITIAL_URL" | "MAIN_INFOS"
export default function SubmitStartupFormPage() {
    const [formStep, setFormStep] = useState<SubmitStartupFormStep>("INITIAL_URL");
    const [startupFormData, setStartupFormData] = useState<StartupDetailsModel>(StartupDetailsUtils.initStartupDataObj());
    const [currentUser, isAuthLoading] = useAuthState(auth);
    const [centralUserData, setCentralUserData] = useCentralUserDataState();

    const [displayProfileEditorPopup, setDisplayEditorPopup] = useState(false);

    useEffect(() => {
        if (centralUserData && !CustomProjectUtils.isProfileValidToSubmitStartup(centralUserData.publicProfile)) {
            setDisplayEditorPopup(true);
        }
    }, [centralUserData]);



    function onDataChanged(startupData: StartupDetailsModel) {
        setStartupFormData({
            ...startupFormData,
        });
    }

    function renderCentralContent() {
        switch (formStep) {
            case "INITIAL_URL":
                return (
                    <SubmitStartupFormInitialUrlComponent startupData={startupFormData} onDataChanged={onDataChanged} onGetStartedClicked={() => setFormStep("MAIN_INFOS")}/>
                )
            case "MAIN_INFOS":
            default:
                return (<SubmitStartupFormMainInfosComponent startupData={startupFormData} onDataChanged={onDataChanged} onSubmitStartupClicked={() => {}}/>);
        }
    }


    function renderLoginModal() {
        const displayModal = !isAuthLoading && !AuthManager.isUserLogged();
        return (
            <Modal isOpen={displayModal} onClose={() => {
            }
            }  closeOnEsc={false} closeOnOverlayClick={false}>
                <ModalOverlay />
                <ModalContent>
                    <div className='flex flex-col items-center gap-2'>
                        <PageHeaderTitleComponent title={"🚀 Submit Your Startup"} subtitle={"Before you submit your startup, please create an account."}/>
                        <LoginRootComponent autoBack={false} />

                    </div>
                </ModalContent>
            </Modal>
        )
    }

    return (
        <CustomRootLayoutComponent customBody={true} isIndexable={false}>
            <section className="bg-white py-4 px-2 md:px-10 min-h-screen">
                <div className="container px-4 mx-auto">
                    <PageHeaderTitleComponent title={"🚀 Submit Your Startup"} subtitle={"Stumbled upon an exciting product that you think deserves attention? Or perhaps you’ve created something unique and want to share it with the world? You’ve come to the perfect place. Just sit back and follow the process."}/>
                    <div className='divider'></div>
                    {renderCentralContent()}
                    <ProfileEditorPopupComponent displayPopup={displayProfileEditorPopup} onPopupClosed={() => {}} />
                    {renderLoginModal()}
                </div>
            </section>
        </CustomRootLayoutComponent>
    )
    /*if (!isAuthLoading && !AuthManager.isUserLogged()) {
        return (
            <CustomRootLayoutComponent customBody={true} isIndexable={false}>
                <section className="bg-white py-4 px-2 md:px-10 min-h-screen">
                    <div className="flex flex-col gap-5 container px-4 mx-auto">
                        <PageHeaderTitleComponent title={"🚀 Submit Your Startup"} subtitle={"Before you submit your startup, please create an account."}/>

                        <LoginRootComponent autoBack={false} />
                    </div>
                </section>
            </CustomRootLayoutComponent>
        )
    } else {
        return (
            <CustomRootLayoutComponent customBody={true} isIndexable={false}>
                <section className="bg-white py-4 px-2 md:px-10 min-h-screen">
                    <div className="container px-4 mx-auto">
                        <PageHeaderTitleComponent title={"🚀 Submit Your Startup"} subtitle={"Stumbled upon an exciting product that you think deserves attention? Or perhaps you’ve created something unique and want to share it with the world? You’ve come to the perfect place. Just sit back and follow the process."}/>
                        <div className='divider'></div>
                        {renderCentralContent()}
                        <ProfileEditorPopupComponent displayPopup={displayProfileEditorPopup} onPopupClosed={() => {}} />
                    </div>
                </section>
            </CustomRootLayoutComponent>
        )
    }*/
}