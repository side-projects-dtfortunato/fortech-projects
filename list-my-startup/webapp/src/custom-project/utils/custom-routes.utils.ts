
export class CustomRoutesUtils {

    static getSubmitStartupUrl() {
        return "/submit-startup";
    }
    static getStartupListedDetails(startupId: string, publishedDayId: string) {
        return "/startup/" + startupId + "/listed/" + publishedDayId;
    }

    static getStartupDetailsUrl(startupId: string) {
        return "/startup/" + startupId
    }

    static getPublishedStartupsDaily(publishedDayId: string) {
        return "/published-startups-daily/" + publishedDayId;
    }

    static getPendingStartupUrl(startupId: string) {
        return this.getSubmitStartupUrl() + "/pending/" + startupId;
    }

    static getCategoryStartupsUrl(categoryLabel: string) {
        return "/category/" + categoryLabel.replaceAll(" ", "+");
    }
}