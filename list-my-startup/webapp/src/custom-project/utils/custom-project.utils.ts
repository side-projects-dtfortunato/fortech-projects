import {
    SharedPublicUserprofileModel
} from "../../components/react-shared-module/logic/shared-data/datamodel/shared-public-userprofile.model";
import {StartupDetailsModel} from "../data/model/startup-details.model";
import CommonUtils from "../../components/react-shared-module/logic/commonutils";

export class CustomProjectUtils {

    static isProfileValidToSubmitStartup(profileData?: SharedPublicUserprofileModel) {
        return profileData && profileData.username !== undefined && profileData.username.length > 3;
    }

    static isSubmitStartupValidToSubmit(startupData: StartupDetailsModel) {
        return CommonUtils.isValidHttpUrl(startupData.link)
            && startupData.tagline.length > 5
            && startupData.name.length > 2
            && startupData.media.logo?.tempBase64
            && startupData.media.screenshots.length > 0
            && startupData.categories.length > 0
            && startupData.description.length > 5
    }
}