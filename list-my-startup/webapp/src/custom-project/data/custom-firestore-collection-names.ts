export const CustomFirestoreCollectionDB = {
    StartupDetails: "StartupDetails",
    PendingToLaunch: "PendingToLaunch",
    DailyLaunches: "DailyLaunches",
}

export const CustomRealtimeCollectionDB = {
    SubmitStartupCategories: "SubmitStartupCategories",
    SponsoredStartupsClicked: "SponsoredStartupsClicked",
}

export const CustomFirestoreDocsDB = {
    StartupDetails: {
        PublicPublished: "PublicPublished",
    },
}