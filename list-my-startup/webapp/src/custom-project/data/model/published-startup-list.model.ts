
import {StartupDetailsModel} from "./startup-details.model";
import {BaseModel} from "../../../components/react-shared-module/logic/shared-data/datamodel/base.model";

export interface PublishedStartupListModel extends BaseModel {
    startup: StartupDetailsModel;
    upvotes: {
        [userId: string]: number,
    }; // User Ids: timestamp
    boostVotes?: number,
    ranks?: {
        dayRank?: number;
        weekRank?: number;
    };
    relatedUserInfos: {
        [userId: string]: PublishedUserInfos,
    };
    comments: {
        [commentId: string]: PublishCommentModel,
    };
    dailyPublicationId?: string;
}

export interface PublishedUserInfos {
    userId: string;
    name: string;
    username: string;
    photoUrl?: string;
}

export interface PublishCommentModel {
    uid: string;
    userId: string;
    message: string;
    publishedAt: number;
    reactions: {
        [userId: string]: string, // Type of reaction
    },
    replies?: {
        [replyId: string]: PublishCommentModel,
    },
    pinned?: boolean,
}