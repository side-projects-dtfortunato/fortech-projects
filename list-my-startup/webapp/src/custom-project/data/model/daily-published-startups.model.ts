import {PublishedStartupListModel} from "./published-startup-list.model";

export interface DailyPublishedStartupsModel {
    publishedDayId: string;
    hideVotes?: boolean;
    listStartups: {
        [startupId: string]: PublishedStartupListModel;
    }
}