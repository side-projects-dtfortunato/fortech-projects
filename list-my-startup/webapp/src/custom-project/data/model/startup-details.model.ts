
import {BaseModel} from "../../../components/react-shared-module/logic/shared-data/datamodel/base.model";
import {PublishedStartupListModel, PublishedUserInfos} from "./published-startup-list.model";
import {StorageFileModel} from "../../../components/react-shared-module/logic/shared-data/datamodel/storage-file.model";

export const StartupCategories = [
    "Technology",
    "Career",
    "Healthcare",
    "Finance",
    "Education",
    "E-commerce",
    "Real Estate",
    "Entertainment",
    "Food & Beverage",
    "Travel & Tourism",
    "Transportation",
    "Agriculture",
    "Environment",
    "Social Impact",
    "Fashion & Beauty",
    "Sports",
    "Gaming",
    "Artificial Intelligence",
    "Blockchain",
    "Virtual Reality",
    "Augmented Reality",
    "Internet of Things",
    "Other"
];

export interface StartupDetailsModel extends BaseModel {
    name: string;
    tagline: string;
    description: string;
    link: string;
    media: {
        logo?: StorageFileModel;
        videoLink?: string;
        screenshots: StorageFileModel[];
    };
    otherLinks: OtherLink[];
    tags: string[];
    categories: string[];
    creatorUid: string;
    creatorUserInfos?: PublishedUserInfos;
    similarStartups?: StartupDetailsModel[];
    isAutoImport?: boolean;
}

export interface OtherLink {
    label: string;
    link: string;
}

export class StartupDetailsUtils {
    static initStartupDataObj(): StartupDetailsModel {
        return {
            link: "",
            name: "",
            tags: [],
            media: {
                screenshots: [],
            },
            tagline: "",
            categories: [],
            otherLinks: [],
            description: "",
            creatorUid: "",
        };
    }

    static getTotalPublishedStartup(publishedStartup: PublishedStartupListModel): number {
        let upvotes: number = Object.keys(publishedStartup.upvotes).length;
        if (publishedStartup.boostVotes) {
            upvotes += publishedStartup.boostVotes;
        }
        return upvotes;
    }
}