import {
    PageHeadProps
} from "../../../components/react-shared-module/ui-components/root/RootLayout.component";
import {BasePostDataModel} from "../../../components/react-shared-module/logic/shared-data/datamodel/base-post-data.model";
import {getAPIDocument} from "../../../components/react-shared-module/logic/shared-data/firebase.utils";
import {
    SharedFirestoreCollectionDB,
    SharedFirestoreDocsDB
} from "../../../components/react-shared-module/logic/shared-data/datamodel/shared-firestore-collections";
import {
    SharedPublicUserprofileModel,
} from "../../../components/react-shared-module/logic/shared-data/datamodel/shared-public-userprofile.model";
import PublicProfilePageContent
    from "../../../components/react-shared-module/ui-components/public-profile/PublicProfilePageContent.component";
import PublicProfileListPostsComponent
    from "../../../components/react-shared-module/ui-components/public-profile/PublicProfileListPosts.component";
import {SharedBackendApi} from "../../../components/react-shared-module/logic/shared-data/sharedbackend.api";
import NotFoundContentComponent
    from "../../../components/react-shared-module/ui-components/shared/NotFoundContent.component";
import RootConfigs from "../../../configs";
import CustomRootLayoutComponent from "../../../components/app-components/root/CustomRootLayout.component";
import {SharedUtils} from "../../../components/react-shared-module/utils/shared-utils";
import {StartupDetailsModel} from "../../../custom-project/data/model/startup-details.model";


export async function getStaticPaths() {
    let listRecentPosts: {[postId: string]: BasePostDataModel} | undefined = await getAPIDocument(SharedFirestoreCollectionDB.DataCatalog, SharedFirestoreDocsDB.DataCatalog.FilterRecent);
    let listPaths: any[] = [];
    let mapRecentUsers: {[userId: string]: boolean} = {};

    if (listRecentPosts) {
        Object.values(listRecentPosts).forEach((postItem) => {
           if (postItem.listUsersInfos[postItem.userCreatorId].username) {
               mapRecentUsers = {
                   ...mapRecentUsers,
                   ["@" + postItem.listUsersInfos[postItem.userCreatorId].username]: true,
               }
           } else {
               mapRecentUsers = {
                   ...mapRecentUsers,
                   [postItem.userCreatorId]: true,
               }
           }
        });
    }

    if (mapRecentUsers) {
        listPaths = Object.keys(mapRecentUsers).map((userId) => {
            return {
                params: {
                    id: userId,
                },
            }
        });
    }

    return {
        paths: listPaths,
        fallback: true, // can also be true or 'blocking'
    }
}

export async function getStaticProps(context: any) {
    const pageId: string = context.params.id as string;
    let publicUserProfile: SharedPublicUserprofileModel | undefined;
    let userId: string | undefined

    // Check if should search for UID or Username
    if (pageId && pageId.startsWith("@")) {
        let userName: string = pageId.replace("@", "");
        publicUserProfile = await SharedBackendApi.getPublicProfileByUsername({username: userName});
        userId = publicUserProfile?.uid;
    } else {
        userId = pageId;
        publicUserProfile = await getAPIDocument(SharedFirestoreCollectionDB.PublicUserProfile, userId);
    }

    return {
        props: {
            publicUserProfile: publicUserProfile ? publicUserProfile : null,
            key: userId,
        },
        revalidate: 60 * 60, // each 60 minutes
    };
}

export default function PublicProfilePage(props: { publicUserProfile?: SharedPublicUserprofileModel}) {


    const pageHeadProps: PageHeadProps = {
        title: props.publicUserProfile?.name + " - " + props.publicUserProfile?.publicSubtitle,
        description: props.publicUserProfile?.publicSubtitle ? props.publicUserProfile?.publicSubtitle : RootConfigs.SITE_DESCRIPTION,
        imgUrl: props.publicUserProfile?.userPicture?.fileUrl,
        publishedTime: props.publicUserProfile?.createdAt ? new Date(props.publicUserProfile?.createdAt).toUTCString() : undefined,
        jsonLdMarkup: props.publicUserProfile ? SharedUtils.getSchemaMarkupFromUser(props.publicUserProfile) : <></>
    };

    if (props.publicUserProfile) {
        return (
            <CustomRootLayoutComponent pageHeadProps={pageHeadProps}>
                <div className='flex flex-col gap-5 py-5'>
                    <PublicProfilePageContent userProfile={props.publicUserProfile} />
                    {/* TODO List the published startups*/}
                </div>
            </CustomRootLayoutComponent>
        )
    } else {
        return (
            <CustomRootLayoutComponent>
                <NotFoundContentComponent />
            </CustomRootLayoutComponent>
        )
    }

}

export function getPublicProfilePageURL(userId: string, username?: string) {
    let pageId: string = userId;
    if (username && username.length > 0) {
        pageId = "@" + username;
    }
    return "/public-profile/" + pageId;
}