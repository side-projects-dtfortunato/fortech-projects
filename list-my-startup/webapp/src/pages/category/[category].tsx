import {StartupDetailsModel} from "../../custom-project/data/model/startup-details.model";
import CustomRootLayoutComponent from "../../components/app-components/root/CustomRootLayout.component";
import EmptyListMessageComponent
    from "../../components/react-shared-module/ui-components/shared/EmptyListMessage.component";
import StartupListItemComponent from "../../custom-project/ui/shared/startup-list-item.component";
import {CustomBackendApi} from "../../custom-project/api/custombackend.api";
import {
    ClientRealtimeDbUtils,
} from "../../components/react-shared-module/logic/shared-data/firebase.utils";
import {
    CustomRealtimeCollectionDB
} from "../../custom-project/data/custom-firestore-collection-names";
import {CustomRoutesUtils} from "../../custom-project/utils/custom-routes.utils";
import {PageHeadProps} from "../../components/react-shared-module/ui-components/root/RootLayout.component";
import RootConfigs from "../../configs";

export async function getStaticPaths() {
    const listCategories: {[category: string]: number} = await ClientRealtimeDbUtils.getData({collectionName: CustomRealtimeCollectionDB.SubmitStartupCategories});

    let paths: string[] = [];
    if (listCategories) {
        Object.keys(listCategories).forEach((category) => {
            paths.push(CustomRoutesUtils.getCategoryStartupsUrl(category.toLowerCase()));
        });
    }

    return {
        paths: paths,
        fallback: true, // can also be true or 'blocking'
    }
}

export async function getStaticProps(context: any) {
    let category: string = context.params.category as string;

    category = category.replaceAll("+", " ");

    let listStartups: StartupDetailsModel[] = await CustomBackendApi.getCategoryStartups({category: category});

    if (!listStartups) {
        listStartups = [];
    }
    return {
        props: {
            categoryLabel: category,
            listStartups: listStartups,
        },
        revalidate: 86400, // every day
    };
}

export default function CategoryListStartupsPage(props: {categoryLabel: string, listStartups: StartupDetailsModel[]}) {

    function renderListStartups() {
        if (props.listStartups && props.listStartups.length > 0) {
            return props.listStartups.map((startup) => {
                return <StartupListItemComponent listStartup={startup} key={startup.uid!} />
            });
        } else {
            return (
                <EmptyListMessageComponent message={"We couldn't find any startup in this category"} />
            )
        }
    }

    const pageHeadProps: PageHeadProps = {
        title: props.categoryLabel + " Startups - " + RootConfigs.SITE_TITLE,
        description: RootConfigs.SITE_DESCRIPTION,
        imgUrl: props.listStartups && props.listStartups.length > 0 && props.listStartups[0].media ? props.listStartups[0].media!.logo!.fileUrl : "/images/logo-512.png",
        publishedTime: new Date().toISOString(),
    }

    return (
        <CustomRootLayoutComponent pageHeadProps={pageHeadProps}>
            <div className='flex flex-col my-5 min-h-screen p-5'>
                <span className='flex flex-col sm:flex-row text-3xl items-center'>
                    <span className='p-2 bg-orange-400 rounded-xl text-white font-bold'>{props.categoryLabel}</span>
                    <span className='p-2 text-slate-800'>Startups</span>
                </span>
                <div className='divider'/>
                <div className='flex flex-col gap-10'>
                    {renderListStartups()}
                </div>
            </div>
        </CustomRootLayoutComponent>
    )
}