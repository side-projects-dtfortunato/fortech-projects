import AdminOnlyRootComponent from "../../components/react-shared-module/ui-components/admin/admin-only-root.component";
import {useState} from "react";
import {useRouter} from "next/router";
import {PushPostDataModel} from "../../components/react-shared-module/logic/shared-data/datamodel/base-post-data.model";
import {
    PostFormArticleBlocksDetails
} from "../../components/react-shared-module/logic/shared-data/datamodel/posts-model/post-article-blocks";
import {SharedBackendApi} from "../../components/react-shared-module/logic/shared-data/sharedbackend.api";
import {CustomRoutesUtils} from "../../custom-project/utils/custom-routes.utils";
import dynamic from "next/dynamic";
import CustomRootLayoutComponent from "../../components/app-components/root/CustomRootLayout.component";
import {SharedRoutesUtils} from "../../components/react-shared-module/utils/shared-routes.utils";
const PostFormArticleBlocksComponent = dynamic(() => import("../../components/react-shared-module/ui-components/post-form/PostFormArticleBlocks.component"), {
    ssr: false,
});

export default function NewArticlePage() {
    const [isLoading, setIsLoading] = useState(false);
    const router = useRouter();

    async function onSubmitClicked(postData: PushPostDataModel<any, PostFormArticleBlocksDetails>) {
        setIsLoading(true);
        await SharedBackendApi.pushPostData({
            postData: postData,
            isCreation: true,
            thumbnailBase: postData.thumbnailUrl,
        });
        setIsLoading(false);

        await router.push(SharedRoutesUtils.getBlogPostDetailsPageURL(postData.uid!));
    }

    return (
        <AdminOnlyRootComponent>
            <CustomRootLayoutComponent>
                <PostFormArticleBlocksComponent postType={"ARTICLE_BLOCKS"} onSubmitClicked={onSubmitClicked}/>
            </CustomRootLayoutComponent>
        </AdminOnlyRootComponent>
    )
}