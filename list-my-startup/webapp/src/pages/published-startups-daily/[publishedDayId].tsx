import {PublishedStartupListModel} from "../../custom-project/data/model/published-startup-list.model";
import {
    FirebaseClientFirestoreUtils,
    getAPIDocument
} from "../../components/react-shared-module/logic/shared-data/firebase.utils";
import {CustomFirestoreCollectionDB} from "../../custom-project/data/custom-firestore-collection-names";
import CustomRootLayoutComponent from "../../components/app-components/root/CustomRootLayout.component";
import PublishedStartupsListContainerComponent
    from "../../custom-project/ui/published-startups-list/published-startups-list-container.component";
import {DailyPublishedStartupsModel} from "../../custom-project/data/model/daily-published-startups.model";
import {PageHeadProps} from "../../components/react-shared-module/ui-components/root/RootLayout.component";
import {StartupDetailsUtils} from "../../custom-project/data/model/startup-details.model";
import {CustomRoutesUtils} from "../../custom-project/utils/custom-routes.utils";
import CommonUtils from "../../components/react-shared-module/logic/commonutils";
import RootConfigs from "../../configs";
import {useCentralUserDataState} from "../../components/react-shared-module/logic/global-hooks/root-global-state";
import {
    UserRole
} from "../../components/react-shared-module/logic/shared-data/datamodel/shared-central-user-data.model";
import NewsletterPopupComponent
    from "../../components/react-shared-module/ui-components/newsletter/shared/newsletter-popup.component";

// Use startic props
export async function getStaticPaths() {
    // Get last 5 days of startups
    const listPublishedDays: DailyPublishedStartupsModel[] = await FirebaseClientFirestoreUtils
        .getDocumentsWithPagination(CustomFirestoreCollectionDB.DailyLaunches, 5, "publishedDayId");

    let paths: string[] = [];
    listPublishedDays
        .filter((listItem) => listItem.listStartups)
        .forEach((publishDay) => {
        if (publishDay.publishedDayId) {
            paths.push(CustomRoutesUtils.getPublishedStartupsDaily(publishDay.publishedDayId));
        }
    });

    return {
        paths: paths,
        fallback: true, // can also be true or 'blocking'
    }
}

export async function getStaticProps(context: any) {
    const publishedDayId: string = context.params.publishedDayId as string;

    let publishedStartupList: DailyPublishedStartupsModel | undefined = await getAPIDocument<DailyPublishedStartupsModel>(CustomFirestoreCollectionDB.DailyLaunches, publishedDayId);

    return {
        props: {
            publishedStartupList,
            publishedDayId,
        },
        revalidate: 60*10, // each 60 seconds
    };
}

export default function PublishedStartupsDailyPage(props: {publishedStartupList?: DailyPublishedStartupsModel, publishedDayId: string}) {
    const [centralUserData, setCentralUserData] = useCentralUserDataState();

    if (!props.publishedStartupList) {
        return (<></>);
    }

    if (!props.publishedStartupList.listStartups) {
        props.publishedStartupList.listStartups = {};
    }

    const topStartups: PublishedStartupListModel[] = Object
        .values(props.publishedStartupList.listStartups)
        .sort((s1, s2) => StartupDetailsUtils.getTotalPublishedStartup(s2) - StartupDetailsUtils.getTotalPublishedStartup(s1))
        .slice(0, 5);

    let title: string = "Top Startups Launches of the Day: ";
    let description: string = "Top voted startups launches of the day: ";

    topStartups.forEach((startup) => {
        title += startup.startup.name + "; ";
        description += startup.startup.name + " - " + startup.startup.description + "; ";
    });


    const pageHeadProps: PageHeadProps = {
        title: title,
        description: description,
        imgUrl: RootConfigs.LOGO_URL,
        publishedTime: new Date(topStartups[0].createdAt!).toISOString(),
    }

    function onCopyTitleToClipboard() {
        navigator.clipboard.writeText(title);
    }

    function onCopyDescriptionToClipboard() {
        let text: string = "";

        text += "Daily Top Startups:"

        let tags: Set<string> = new Set<string>();

        topStartups.forEach((startup) => {
            text += "\n" + startup.startup.name;
            text += ": " + startup.startup.tagline;
            text += " - Upvotes: " + StartupDetailsUtils.getTotalPublishedStartup(startup);

            startup.startup.categories.forEach((category) => {
                tags.add("#" + CommonUtils.string_to_slug(category).replaceAll("-", ""));
            });
        });

        // tags
        text += "\n\n";
        Array.from(tags).forEach((tag) => {
            text += tag + " ";
        });

        // Add Link
        text += "\n" + RootConfigs.BASE_URL + CustomRoutesUtils.getPublishedStartupsDaily(props.publishedDayId);

        navigator.clipboard.writeText(text);
    }

    function renderAdminCopyToClipboard() {
        if (centralUserData && centralUserData.userRole === UserRole.ADMIN) {
            return (
                <div className={"flex"}>
                    <button className='btn' onClick={onCopyTitleToClipboard}>Copy Title to Clipboard</button>
                    <button className='btn' onClick={onCopyDescriptionToClipboard}>Copy Description to Clipboard</button>
                </div>
            )
        } else {
            return (<></>)
        }
    }

    return (
        <CustomRootLayoutComponent pageHeadProps={pageHeadProps}>
            {renderAdminCopyToClipboard()}
            <PublishedStartupsListContainerComponent listStartups={Object.values(props.publishedStartupList.listStartups)} publishedDay={Number(props.publishedStartupList.publishedDayId)} linkable={false} hideVotes={props.publishedStartupList.hideVotes} />

            {/* Popups */}
            <NewsletterPopupComponent />
        </CustomRootLayoutComponent>
    )
}