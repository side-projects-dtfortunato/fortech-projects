import {PublishedStartupListModel} from "../../../custom-project/data/model/published-startup-list.model";
import CustomRootLayoutComponent from "../../../components/app-components/root/CustomRootLayout.component";
import NotFoundContentComponent
    from "../../../components/react-shared-module/ui-components/shared/NotFoundContent.component";
import {DailyPublishedStartupsModel} from "../../../custom-project/data/model/daily-published-startups.model";
import {getAPIDocument} from "../../../components/react-shared-module/logic/shared-data/firebase.utils";
import {CustomFirestoreCollectionDB} from "../../../custom-project/data/custom-firestore-collection-names";
import PublishedStartupHeaderComponent
    from "../../../custom-project/ui/published-startup-list-details/shared/published-startup-header.component";
import BannerMessageComponent
    from "../../../components/react-shared-module/ui-components/shared/BannerMessage.component";
import PopupConfirmationMessageComponent
    from "../../../components/react-shared-module/ui-components/shared/PopupConfirmationMessage.component";
import {CiCircleCheck} from "react-icons/ci";
import PopupPromoteStartupComponent from "../../../custom-project/ui/shared/PopupPromoteStartup.component";

export async function getServerSideProps(context: any) {
    const startupId: string = context.params.startupId as string;

    let startupData: PublishedStartupListModel | undefined = await getAPIDocument<PublishedStartupListModel>(CustomFirestoreCollectionDB.PendingToLaunch, startupId);

    return {
        props: {
            startupData,
        },
    };
}

export default function PendingStartupSubmitPage(props: {startupData?: PublishedStartupListModel}) {

    if (!props.startupData) {
        return (
            <CustomRootLayoutComponent>
                <NotFoundContentComponent />
            </CustomRootLayoutComponent>
        )
    } else {
        return (
            <CustomRootLayoutComponent isIndexable={false}>
                {/* Popup*/}
                <PopupConfirmationMessageComponent icon={<CiCircleCheck size={50} color={"#00AA00"}/>} message={"Congratulations, your startup was submitted successfully"} durationSeconds={5} />

                <PopupPromoteStartupComponent startupData={props.startupData.startup} />

                <div className='flex flex-col gap-5 items-center'>
                    <BannerMessageComponent message={"Your startup will be published tomorrow."} bannerType={"in_progress"} />
                    <PublishedStartupHeaderComponent startupListDetails={props.startupData} startupData={props.startupData.startup} />
                </div>
            </CustomRootLayoutComponent>
        )
    }

}