import '../styles/globals.css'
import Script from 'next/script';
import type {AppProps} from 'next/app'
import RootConfigs from "../configs";
import { ChakraProvider } from '@chakra-ui/react'

export default function App({Component, pageProps}: AppProps) {

    return (
        <>
            <Script src="https://cdn.tailwindcss.com"></Script>
            {/** Set the proper google tag manager **/}
            <Script
                id="gtm_0"
                src={"https://www.googletagmanager.com/gtag/js?id=" + RootConfigs.FIREBASE_CONFIG.measurementId}
                strategy="afterInteractive"
            />
            <Script id="google-analytics" strategy="afterInteractive">
                {
                    `
              window.dataLayer = window.dataLayer || [];
              function gtag(){window.dataLayer.push(arguments);}
              gtag('js', new Date());
        
              gtag('config', '${RootConfigs.FIREBASE_CONFIG.measurementId}');
            `}
            </Script>

            <Script
                async
                src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js?client=ca-pub-5492791201497132"
                strategy="afterInteractive"
                crossOrigin={"anonymous"}
            />

            <ChakraProvider>
                <Component {...pageProps} />
            </ChakraProvider>
        </>
    )
}
