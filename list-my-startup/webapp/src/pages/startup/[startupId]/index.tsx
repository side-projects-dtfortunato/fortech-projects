import {StartupDetailsModel} from "../../../custom-project/data/model/startup-details.model";
import {PublishedStartupListModel} from "../../../custom-project/data/model/published-startup-list.model";
import {
    CustomFirestoreCollectionDB,
    CustomFirestoreDocsDB
} from "../../../custom-project/data/custom-firestore-collection-names";
import {
    FirebaseClientFirestoreUtils,
    getAPIDocument
} from "../../../components/react-shared-module/logic/shared-data/firebase.utils";
import CustomRootLayoutComponent from "../../../components/app-components/root/CustomRootLayout.component";
import {PageHeadProps} from "../../../components/react-shared-module/ui-components/root/RootLayout.component";
import RootConfigs from "../../../configs";
import PublishedStartupHeaderComponent
    from "../../../custom-project/ui/published-startup-list-details/shared/published-startup-header.component";
import PublishedStartupListItemComponent
    from "../../../custom-project/ui/published-startups-list/shared/published-startup-list-item.component";
import EmptyListMessageComponent
    from "../../../components/react-shared-module/ui-components/shared/EmptyListMessage.component";
import InlineLabelSeparatorComponent
    from "../../../components/react-shared-module/ui-components/shared/inline-label-separator.component";
import {DailyPublishedStartupsModel} from "../../../custom-project/data/model/daily-published-startups.model";
import {CustomRoutesUtils} from "../../../custom-project/utils/custom-routes.utils";
import PublishedStartupSimilarListComponent
    from "../../../custom-project/ui/published-startup-list-details/shared/published-startup-similar-list.component";
import PopupPromoteStartupComponent from "../../../custom-project/ui/shared/PopupPromoteStartup.component";
import PostCommentsRootComponent
    from "../../../components/react-shared-module/base-projects/community/ui/post-comments-root.component";
import {
    SharedFirestoreCollectionDB
} from "../../../components/react-shared-module/logic/shared-data/datamodel/shared-firestore-collections";
import {SharedRoutesUtils} from "../../../components/react-shared-module/utils/shared-routes.utils";

export async function getStaticPaths() {
    // Get last 5 days of startups
    const listStartups: StartupDetailsModel[] = await FirebaseClientFirestoreUtils.getDocumentsWithPagination(CustomFirestoreCollectionDB.StartupDetails, 100, "createdAt");

    let paths: string[] = [];
    listStartups.forEach((startupData) => {
        if (startupData.media.logo && startupData.media.logo.fileUrl && startupData.media.screenshots.length > 0) {
            paths.push(CustomRoutesUtils.getStartupDetailsUrl(startupData.uid!));
        }
    });

    return {
        paths: paths,
        fallback: true, // can also be true or 'blocking'
    }
}

export async function getStaticProps(context: any) {
    let startupId: string = context.params.startupId as string;


    let startupData: StartupDetailsModel | undefined = await getAPIDocument<StartupDetailsModel>(CustomFirestoreCollectionDB.StartupDetails, startupId);

    let listPublications: PublishedStartupListModel[] = [];

    if (startupData) {
        listPublications = await FirebaseClientFirestoreUtils
            .getDocsFromSubCollection({
                collection: CustomFirestoreCollectionDB.StartupDetails,
                docId: startupId,
                subCollection: CustomFirestoreDocsDB.StartupDetails.PublicPublished,
            }, 4, "dailyPublicationId");

    }


    return {
        props: {
            startupData: startupData,
            listPublications: listPublications,
        },
        revalidate: 86400, // every day
    };
}

export default function StartupDetailsPage(props: { startupData?: StartupDetailsModel, listPublications?: PublishedStartupListModel[] }) {

    function renderListPublications() {

        if (props.listPublications && props.listPublications.length > 0) {
            return (
                <div className='flex flex-col gap-5 my-10'>
                    {props.listPublications
                        .sort((s1, s2) => Number(s2.dailyPublicationId) - Number(s1.dailyPublicationId))
                        .map((publishedStartupItem) => {
                            return (<PublishedStartupListItemComponent key={publishedStartupItem.uid!}
                                                                       listStartup={publishedStartupItem}/>);
                        })}
                </div>
            );
        } else {
            return (
                <div className='flex w-full px-5 mb-10'>
                    <EmptyListMessageComponent message={"Didn't find any publication of this startup"}/>
                </div>
            )
        }

    }

    if (props.startupData) {
        let pageHeadProps: PageHeadProps = {
            title: props.startupData.name + " - " + RootConfigs.SITE_TITLE,
            description: props.startupData.tagline,
            imgUrl: props.startupData.media.screenshots[0].fileUrl,
            publishedTime: new Date(props.startupData.createdAt!).toISOString(),
        }
        return (
            <CustomRootLayoutComponent pageHeadProps={pageHeadProps}>
                <PopupPromoteStartupComponent startupData={props.startupData} />
                <div className='flex flex-col sm:my-10 p-5'>
                    <PublishedStartupHeaderComponent startupData={props.startupData}/>
                    <div className='flex w-full mt-10'>
                        <InlineLabelSeparatorComponent title={"Recent Publications"}/>
                    </div>
                    {renderListPublications()}
                    <div className='flex mt-10'>
                        <PublishedStartupSimilarListComponent startupData={props.startupData} />
                    </div>
                </div>
            </CustomRootLayoutComponent>
        )
    }

}