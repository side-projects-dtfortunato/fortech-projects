import {
    FirebaseClientFirestoreUtils,
    getAPIDocumentSubCol
} from "../../../../../components/react-shared-module/logic/shared-data/firebase.utils";
import {
    CustomFirestoreCollectionDB,
    CustomFirestoreDocsDB
} from "../../../../../custom-project/data/custom-firestore-collection-names";
import {PublishedStartupListModel} from "../../../../../custom-project/data/model/published-startup-list.model";
import CustomRootLayoutComponent from "../../../../../components/app-components/root/CustomRootLayout.component";
import NotFoundContentComponent
    from "../../../../../components/react-shared-module/ui-components/shared/NotFoundContent.component";
import PublishedStartupListDetailsPage
    from "../../../../../custom-project/ui/published-startup-list-details/published-startup-list-details.page";
import {DailyPublishedStartupsModel} from "../../../../../custom-project/data/model/daily-published-startups.model";
import {CustomRoutesUtils} from "../../../../../custom-project/utils/custom-routes.utils";
import {PageHeadProps} from "../../../../../components/react-shared-module/ui-components/root/RootLayout.component";
import RootConfigs from "../../../../../configs";
import PopupPromoteStartupComponent from "../../../../../custom-project/ui/shared/PopupPromoteStartup.component";
import NewsletterPopupComponent
    from "../../../../../components/react-shared-module/ui-components/newsletter/shared/newsletter-popup.component";


export async function getStaticPaths() {
    // Get last 5 days of startups
    const listPublishedDays: DailyPublishedStartupsModel[] = await FirebaseClientFirestoreUtils
        .getDocumentsWithPagination(CustomFirestoreCollectionDB.DailyLaunches, 5, "publishedDayId");

    let paths: string[] = [];
    listPublishedDays.filter((item) => item.listStartups)
        .forEach((publishDay) => {
        if (Object.keys(publishDay.listStartups).length > 0) {
            Object.keys(publishDay.listStartups)
                .filter((startupId) => publishDay.listStartups[startupId].startup.media.screenshots && publishDay.listStartups[startupId].startup.media.screenshots.length > 0)
                .forEach((startupId) => {
                paths.push(CustomRoutesUtils.getStartupListedDetails(startupId, publishDay.publishedDayId));
            });
        }
    });

    return {
        paths: paths,
        fallback: true, // can also be true or 'blocking'
    }
}

export async function getStaticProps(context: any) {
    const startupId: string = context.params.startupId as string;
    const publishedDailyId: string = context.params.publishedDailyId as string;

    let publishedStartupList: PublishedStartupListModel | undefined = await getAPIDocumentSubCol<PublishedStartupListModel>([CustomFirestoreCollectionDB.StartupDetails,
        CustomFirestoreDocsDB.StartupDetails.PublicPublished], [startupId, publishedDailyId]);

    return {
        props: {
            publishedStartupList,
        },
        revalidate: 60, // each 60 seconds
    };
}

export default function PublishedStartupListRoot(props: {publishedStartupList?: PublishedStartupListModel}) {


    if (!props.publishedStartupList) {
        return (
            <CustomRootLayoutComponent>
                <NotFoundContentComponent />
            </CustomRootLayoutComponent>
        )
    } else {
        let pageHeadProps: PageHeadProps = {
            title: props.publishedStartupList.startup.name + " - " + RootConfigs.SITE_TITLE,
            description: props.publishedStartupList.startup.tagline,
            imgUrl: props.publishedStartupList.startup.media.screenshots[0].fileUrl,
            publishedTime: new Date(props.publishedStartupList.createdAt!).toISOString()
        }

        return (
            <CustomRootLayoutComponent pageHeadProps={pageHeadProps}>
                <PopupPromoteStartupComponent startupData={props.publishedStartupList.startup} />
                <PublishedStartupListDetailsPage startupListDetails={props.publishedStartupList} />
            </CustomRootLayoutComponent>
        )
    }
}