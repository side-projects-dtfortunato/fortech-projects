import {
    ClientRealtimeDbUtils,
    FirebaseClientFirestoreUtils, getAPIDocument,
} from "../components/react-shared-module/logic/shared-data/firebase.utils";
import CustomRootLayoutComponent from "../components/app-components/root/CustomRootLayout.component";
import {
    CustomFirestoreCollectionDB,
    CustomRealtimeCollectionDB
} from "../custom-project/data/custom-firestore-collection-names";
import {DailyPublishedStartupsModel} from "../custom-project/data/model/daily-published-startups.model";
import PublishedDailyStartupsPaginationPage
    from "../custom-project/ui/published-daily-startups-pagination/published-daily-startups-pagination.page";
import NotFoundContentComponent from "../components/react-shared-module/ui-components/shared/NotFoundContent.component";
import LandingPageGetListedBannerComponent from "../custom-project/ui/shared/LandingPageGetListedBanner.component";
import LandingPopularCategoriesComponent
    from "../custom-project/ui/categories/shared/landing-popular-categories.component";
import {
    SharedFirestoreCollectionDB,
    SharedFirestoreDocsDB
} from "../components/react-shared-module/logic/shared-data/datamodel/shared-firestore-collections";
import {PostTypes} from "../components/react-shared-module/utils/shared-ui-helper";
import Link from "next/link";
import {SharedRoutesUtils} from "../components/react-shared-module/utils/shared-routes.utils";
import ListHeadhlineSeparatorComponent
    from "../components/react-shared-module/ui-components/shared/ListHeadhlineSeparator.component";
import {CustomRoutesUtils} from "../custom-project/utils/custom-routes.utils";
import NewsletterPopupComponent
    from "../components/react-shared-module/ui-components/newsletter/shared/newsletter-popup.component";
import {
    DailyPublishedArticlesModel
} from "../components/react-shared-module/base-projects/articles-digest/data/model/DailyPublishedArticles.model";
import {
    ArticleImportedModel
} from "../components/react-shared-module/base-projects/articles-digest/data/model/ArticleImported.model";
import ArticleDetailsLatestArticlesComponent
    from "../components/react-shared-module/base-projects/articles-digest/ui-components/shared/article-details-latest-articles.component";


const ITEMS_PER_PAGE = 4;

export async function getStaticProps() {
    const listPublishedDays: DailyPublishedStartupsModel[] = await FirebaseClientFirestoreUtils
        .getDocumentsWithPagination(CustomFirestoreCollectionDB.DailyLaunches, ITEMS_PER_PAGE, "publishedDayId");

    const categories: {[category: string]: number} | undefined = await ClientRealtimeDbUtils.getData({
        collectionName: CustomRealtimeCollectionDB.SubmitStartupCategories,
    });

    /*let docRecentPosts: {[postId: string]: BasePostDataModel} | undefined = await getAPIDocument(SharedFirestoreCollectionDB.DataCatalog, SharedFirestoreDocsDB.DataCatalog.FilterRecent);
    let listBlogArticles: BasePostDataModel<any, PostFormArticleBlocksDetails>[] = [];

    if (docRecentPosts && Object.keys(docRecentPosts).length > 0) {
        Object.values(docRecentPosts)
            .filter((postData) => postData.postType === PostTypes.ARTICLE_BLOCKS)
            .forEach((postData) => {
                if (listBlogArticles.length < 10) {
                    listBlogArticles.push(postData);
                }
            })
    }*/
    const listPublishedArticlesDays: DailyPublishedArticlesModel[] = await FirebaseClientFirestoreUtils
        .getDocumentsWithPagination(SharedFirestoreCollectionDB.ArticlesDigestProject.DailyPublishedArticles, 2, "publishedDayId");
    let recentArticles: ArticleImportedModel[] = [];
    if (listPublishedArticlesDays) {
        listPublishedArticlesDays.forEach((publishDay) => {
            if (recentArticles.length < 5 && publishDay.listArticles) {
                recentArticles = recentArticles.concat(Object.values(publishDay.listArticles!));
            }
        });
    }

    return {
        props: {
            listPublishedDays,
            categories,
            recentArticles,
        },
        revalidate: 30,
    }
}

export default function Home(props: {listPublishedDays?: DailyPublishedStartupsModel[],
    categories?: {[category: string]: number}, recentArticles: ArticleImportedModel[]}) {

    function renderSideArticles() {
        return <ArticleDetailsLatestArticlesComponent latestArticles={props.recentArticles}/>;
        /*
        if (props.listBlogArticles && props.listBlogArticles.length > 0) {
            let listItems = props.listBlogArticles?.map((post) => {
                return (
                    <li className='flex flex-col bordered"' key={post.uid}>
                        <Link className='flex flex-col items-start' href={SharedRoutesUtils.getPostDetailsPageURL(post.uid!)}>
                            <span className='custom text-start'>{post.title}</span>
                            {post.listUsersInfos ? (<span className='subtitle'>{post.summary}</span>) : (<></>)}
                        </Link>
                    </li>
                )
            });

            return (
                <div className='flex flex-col gap-2 w-full items-center'>
                    <ListHeadhlineSeparatorComponent text={"Recent Articles"} color={"#333333"} />
                    <ul className="list-item-bg drop-shadow menu bg-base-100 w-72">
                        {listItems}
                    </ul>

                    <div className='divider'>
                        <Link href={SharedRoutesUtils.getBlogPageUrl()} title={"Show More"} className='btn btn-ghost btn-xs'>Show more posts</Link>
                    </div>
                </div>
            )
        } else {
            return undefined;
        }*/
    }

    if (props.listPublishedDays) {
        return (
            <CustomRootLayoutComponent rigthChilds={renderSideArticles()}>
                <LandingPageGetListedBannerComponent />
                <div className='mt-10 px-2'>
                    <LandingPopularCategoriesComponent categories={props.categories} maxLength={25} />
                </div>
                <PublishedDailyStartupsPaginationPage preloadedData={props.listPublishedDays} itemsPerPage={ITEMS_PER_PAGE} listRecentArticles={props.recentArticles} />

                {/* Popups */}
                <NewsletterPopupComponent />
            </CustomRootLayoutComponent>
        )
    } else {
        return (
            <CustomRootLayoutComponent>
                <NotFoundContentComponent />
            </CustomRootLayoutComponent>
        )
    }

}
