import {FooterLinkItem} from "../../react-shared-module/ui-components/root/Footer.component";
import RootLayoutComponent, {PageHeadProps} from "../../react-shared-module/ui-components/root/RootLayout.component";
import {UIHelper} from "../../ui-helper/UIHelper";

export default function CustomRootLayoutComponent(props: {pageHeadProps?: PageHeadProps, children: any, customBody?: boolean,
    listFooterLinks?: FooterLinkItem[],
    isIndexable?: boolean, isLoading?: boolean, customNavbarItems?: any,
    leftChilds?: any, rigthChilds?: any, headerBanner?: any, customBackgroundColor?: string}) {



    function renderLeftSideContent() {
        let leftSideContent: any[] = [];

        if (props.leftChilds) {
            leftSideContent = leftSideContent.concat(props.leftChilds);
        }

        return (
            <div className='flex flex-col gap-3'>
                {leftSideContent}
            </div>
        )
    }

    // Override Leftside content
    let customProps: any = {
        ...props,
        leftChilds: renderLeftSideContent(),
        backgroundColor: "#FFFFFF"
    }


    customProps.backgroundColor = props.customBackgroundColor ? props.customBackgroundColor : "bg-white";

    return (
        <>
            {RootLayoutComponent(customProps)}
            <div className='flex sm:hidden'>
                {UIHelper.renderNavigationHeaderBottomMenu()}
            </div>
        </>
    );
}