
export default class RootConfigs {
    static ENVIRONMENT_DEV = false;
    static BASE_URL = "https://www.listmystartup.app";
    static LOGO_URL = "https://listmystartup.app/images/logo-white-bg.jpg";
    static SITE_NAME = "ListMyStartup.app";
    static SITE_TITLE = "List My Startup - The Best Daily Startup Ideas";
    static SITE_DESCRIPTION = "Discover and explore innovative startups on ListMyStartup.app. List your startup for free or opt for our premium service to publish your startup across multiple directories. Join us in fostering the next generation of entrepreneurship.";
    static CURRENT_NICHE_SITE = RootConfigs.SITE_NAME;
    static CONTACT_EMAIL = "contact@listmystartup.app";
    static SITE_TOPIC = "Startups";
    static THEME = {
        PRIMARY_COLOR: "orange",
    }
    static META_CONFIGS: any = {
        disableGoogleSignin: false,
        disableNichSites: true,
        updateNicheBackend: false,
    }

    static APP_STORE_LINKS = {
        androidAppId: "listmystartup.app",
        iosAppId: "6479720082",
    }
    static SOCIAL_NETWORKS: {
        [socialId: string]: {
            iconUrl: string,
            label: string,
            link: string,
        },
    } = {
        facebook: {
            label: "Facebook",
            link: "https://www.facebook.com/profile.php?id=61561725846376",
            iconUrl: "/images/ic_social_facebook.png",
        },
        instagram: {
            label: "Instagram",
            link: "https://www.instagram.com/listmystartup.app/",
            iconUrl: "/images/ic_social_instagram.png",
        },
        threads: {
            label: "Threads",
            link: "https://www.threads.net/@listmystartup.app",
            iconUrl: "/images/ic_social_threads.png",
        },
        reddit: {
            label: "Reddit",
            link: "https://www.reddit.com/r/ListMyStartup/",
            iconUrl: "/images/ic_social_reddit.png",
        },
    }
    static FIREBASE_CONFIG = this.ENVIRONMENT_DEV ?

        // DEV Config - // TODO Change it
        {
            apiKey: "AIzaSyCB2yrNkEHbpShTH_xwHOzcl2mXJT5i1K8",
            authDomain: "fortuly-qa-env.firebaseapp.com",
            projectId: "fortuly-qa-env",
            storageBucket: "fortuly-qa-env.appspot.com",
            messagingSenderId: "650128691290",
            appId: "1:650128691290:web:69f8a283090d23e0454863",
            measurementId: "G-V6XN6K51VZ"
        } :
        // PROD
        {
            apiKey: "AIzaSyDH_CcF7WF4e9kBMHrjzufynJf-y9xU62s",
            authDomain: "listmystartup-app.firebaseapp.com",
            databaseURL: "https://listmystartup-app-default-rtdb.firebaseio.com",
            projectId: "listmystartup-app",
            storageBucket: "listmystartup-app.appspot.com",
            messagingSenderId: "816608351963",
            appId: "1:816608351963:web:c7c18ca396e59f6ad21efe",
            measurementId: "G-9VZFQ4KVXW"
        };

    static IMAGEKIT_API = {
        publicKey : "public_iWUm+7xKzICBvC8mse87J3l8pQk=",
        privateKey : "private_/7D3S4qLgNDfYIa//nJj0yrOtCo=",
        urlEndpoint : "https://ik.imagekit.io/listmystartup",
    }
    static ONE_SIGNAL_API = "67feb43b-b72a-4fd0-bd39-f58bff3fce01"
}
