import * as functions from 'firebase-functions';
import * as admin from 'firebase-admin';

import * as AuthService from './shareable-firebase-backend/functions/auth'
import * as UserProfileService from './shareable-firebase-backend/functions/user-profile';
import * as DocumentChangeService from './shareable-firebase-backend/functions/document-change';

import * as EmailMarketingService from './shareable-firebase-backend/functions/email-marketing';
import {submitStartup} from './custom-project/functions/publish-startup/submit-startup.function';
import {publishPendingStartupsJob, onStartupPublicPublishedChanged, displayTodayVotes} from './custom-project/functions/publish-startup/publish-pending-startups.function';
import {submitPublicationCommentFunction} from './custom-project/functions/publish-startup/submit-publication-comment.function';
import {testFunction} from './custom-project/functions/publish-startup/test-function';
import {autoImportTopProductHuntProducts} from './custom-project/functions/publish-startup/auto-import-top-product-hunt-products.function';
import {importBetalistStartupsFunction} from './custom-project/functions/publish-startup/import-betalist-startups.function';


import {newsletterSubscribe} from './shareable-firebase-backend/functions/newsletter/newsletter-subscribe';
import {generateEmailDailyDigest} from './custom-project/functions/email-marketing/email-daily-digest.function';
import {dispatchEmailBulk} from './shareable-firebase-backend/functions/email-marketing/dispatch-email-bulk/index';
import * as PostsDataService from './shareable-firebase-backend/functions/posts-data';
import {fetchRecentArticlesFunction} from './shareable-firebase-backend/base-projects/articles-digest/functions/fetch-recent-articles.function'
import {onArticleImportedChanged} from './shareable-firebase-backend/base-projects/articles-digest/functions/on-article-updated.function'
import {onCommunityCommentPostUpdateFunction} from './shareable-firebase-backend/base-projects/community/functions/on-community-comment-update.function'
import {communityPostComment} from './shareable-firebase-backend/base-projects/community/functions/post-community-comment.function'
import {communityCommentsDelete} from './shareable-firebase-backend/base-projects/community/functions/community-comments-delete.function'
import {communityReplyComment} from './shareable-firebase-backend/base-projects/community/functions/reply-community-comment.function'
import {communityDailyCleanupJob} from './shareable-firebase-backend/base-projects/community/functions/community-daily-cleanup-job.function'
import {routineJobFunction} from './shareable-firebase-backend/functions/routine-jobs/routineJob.function'
import {threadsAPIHandleAuthCallback, threadsAPIGenerateAuthUrl} from './shareable-firebase-backend/base-projects/social-networks-autopost/functions/threadsapi.function'


// Init App
admin.initializeApp(functions.config().firebase);
admin.firestore().settings({ ignoreUndefinedProperties: true });

// Auth
export const onCreateUser = AuthService.onCreateUser;


// Document Change Service
export const requestDocumentChanges = DocumentChangeService.requestDocumentChanges;

// User profile
export const updateUserProfile = UserProfileService.updateUserProfile;
export const onUpdatePublicUserProfile = UserProfileService.onUpdatePublicUserProfile;
export const checkUsernameValid = UserProfileService.checkUsernameValid;

export const validateEmail = EmailMarketingService.validateEmail;
export const unsubscribeEmail = EmailMarketingService.unsubscribeEmail;

// Posts Data Management
export const pushPostData = PostsDataService.pushPostData;
export const onPostDataUpdated = PostsDataService.onPostDataUpdated;
export const pushPostComment = PostsDataService.pushPostComment;
export const pushPostReaction = PostsDataService.pushPostReaction;
export const openViewPost = PostsDataService.openViewPost;

// Export functions
export {
    // List My Startup
    submitStartup, publishPendingStartupsJob, displayTodayVotes,
    onStartupPublicPublishedChanged,
    submitPublicationCommentFunction,

    testFunction,
    autoImportTopProductHuntProducts, // FIXME remove this function
    importBetalistStartupsFunction,

    newsletterSubscribe,

    generateEmailDailyDigest,
    dispatchEmailBulk,

    // Articles Digest
    fetchRecentArticlesFunction, onArticleImportedChanged,
    // Community Functions
    onCommunityCommentPostUpdateFunction, communityPostComment, communityCommentsDelete, communityReplyComment, communityDailyCleanupJob,
    routineJobFunction,
    // Social Networks
    threadsAPIHandleAuthCallback, threadsAPIGenerateAuthUrl
}