import * as functions from "firebase-functions";
import {StartupImportUtils} from "../../utils/startup-import.utils";


export const testFunction = functions.https.onRequest(
    async (req, resp) => {
        resp.send(await StartupImportUtils.getTodayBetaListStartups());

        // resp.send(await BackendSharedUtils.fetchAndParseXML("https://feeds.feedburner.com/BetaList"));
    });
