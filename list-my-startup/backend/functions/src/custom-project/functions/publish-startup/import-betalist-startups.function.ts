import * as functions from "firebase-functions";
import {StartupDetailsModel, StartupDetailsUtils} from "../../data/model/startup-details.model";
import * as admin from "firebase-admin";
import {BackendSharedUtils} from "../../../shareable-firebase-backend/utils/backend-shared-utils";
import {RealtimeDbUtils} from "../../../shareable-firebase-backend/utils/realtime-db.utils";
import {CustomRealtimeCollectionDB} from "../../data/custom-firestore-collection-names";
import {getQuickResponse} from "../../../shareable-firebase-backend/model-data/api-response.model";
import {StartupImportUtils} from "../../utils/startup-import.utils";

export const importBetalistStartupsFunction = functions.runWith({
    memory: "1GB",
    timeoutSeconds: 500,
}).https.onRequest(
    async (req, resp) => {
        let importedStartupList: StartupDetailsModel[] = await StartupImportUtils.getTodayBetaListStartups();


        // Submit Startups
        const batch = admin.firestore().batch();


        for (let startupData of importedStartupList) {

            StartupDetailsUtils.submitStartupPendingList(
                startupData,
                undefined,
                BackendSharedUtils.getRandomNumber(10, 40),
                batch,
            );
        }

        const batchResp = await batch.commit();

        // Update Categories
        let dbAvailableCategories: {[cat: string]: number} | undefined = await RealtimeDbUtils.getData(
            {
                collectionName: CustomRealtimeCollectionDB.SubmitStartupCategories,
            }
        )
        if (!dbAvailableCategories) {
            dbAvailableCategories = {};
        }
        importedStartupList.forEach((startupData) => {
            startupData.categories.forEach((category) => {
                if (Object.keys(dbAvailableCategories!).includes(category)) {
                    dbAvailableCategories![category] = dbAvailableCategories![category] + 1;
                } else {
                    dbAvailableCategories![category] = 1;
                }
            });
        });
        await RealtimeDbUtils.writeData({
            collectionName: CustomRealtimeCollectionDB.SubmitStartupCategories,
            data: dbAvailableCategories,
            merge: false,
        });


        resp.send(getQuickResponse(true, batchResp));
    });