import * as functions from "firebase-functions";
import * as admin from "firebase-admin";
import {ProductHuntProductItemModel, ScrapeUtils} from "../../data/scraper/scrape.utils";
import {StartupDetailsModel, StartupDetailsUtils} from "../../data/model/startup-details.model";
import {
    SharedPublicUserprofileModel
} from "../../../shareable-firebase-backend/model-data/frontend-model-data/shared-public-userprofile.model";
import {getDocumentData} from "../../../shareable-firebase-backend/utils/utils";
import {SharedFirestoreCollectionDB} from "../../../shareable-firebase-backend/model-data/shared-firestore-collections";
import {PublishedUserInfos} from "../../data/model/published-startup-list.model";
import {RealtimeDbUtils} from "../../../shareable-firebase-backend/utils/realtime-db.utils";
import {CustomRealtimeCollectionDB} from "../../data/custom-firestore-collection-names";
import {getQuickResponse} from "../../../shareable-firebase-backend/model-data/api-response.model";
import {CustomUtils} from "../../custom.utils";
import {BackendSharedUtils} from "../../../shareable-firebase-backend/utils/backend-shared-utils";
import {AIGeminiUtils} from "../../../shareable-firebase-backend/utils/aigemini.utils";

const MAX_DAILY_IMPORTED_PRODUCTS = 6;

const EDITORCHOICE_UID = BackendSharedUtils.isProjectProduction() ? "ZOmOCW38xBOoeZRVUIjvqR3UxWy1" : "eLa0AJZMNsdAsZzvKEdMFUviCK82";


export const autoImportTopProductHuntProducts = functions.runWith({
    memory: "1GB",
    timeoutSeconds: 500,
}).https.onRequest(
    async (req, resp) => {

        let importedListProducts: ProductHuntProductItemModel[] = await ScrapeUtils.getProductHuntListTodayProducts();
        let importedStartupList: StartupDetailsModel[] = [];


        // Get Public User Infos
        const creatorUserInfos: SharedPublicUserprofileModel = await getDocumentData(SharedFirestoreCollectionDB.PublicUserProfile, EDITORCHOICE_UID);
        const publishedCreatorUserInfos: PublishedUserInfos = {
            userId: EDITORCHOICE_UID,
            name: creatorUserInfos.name!,
            photoUrl: creatorUserInfos.userPicture?.fileUrl,
            username: creatorUserInfos.username!
        };

        // Submit Startups
        const batch = admin.firestore().batch();


        for (let i = 0; i < MAX_DAILY_IMPORTED_PRODUCTS && i < importedListProducts.length; i++) {
            let productItem: ProductHuntProductItemModel | undefined = importedListProducts[i];

            productItem = await ScrapeUtils.completeProductDetails(productItem);

            if (productItem) {
                const startupData: StartupDetailsModel = {
                    categories: productItem.categories,
                    name: productItem.name,
                    creatorUid: EDITORCHOICE_UID,
                    description: productItem.description!,
                    tagline: productItem.tagline,
                    media: {
                        logo: {
                            storagePath :"",
                            fileUrl: productItem.logoUrl,
                        },
                        screenshots: productItem.screenshots,
                    },
                    link: productItem.productUrl,
                    updatedAt: Date.now(),
                    otherLinks: [],
                    tags: [],
                    createdAt: Date.now(),
                    isAutoImport: true,
                };
                startupData.uid = StartupDetailsUtils.generateStartupUid(startupData);

                // Generate tags
                const generatedTags: {tags:string[]} | undefined = await AIGeminiUtils.tagJsonContent(JSON.stringify({
                    startupTitle: startupData.name,
                    startupDescription: startupData.description,
                    startupTagline: startupData.tagline,
                }));
                if (generatedTags && generatedTags.tags) {
                    startupData.tags = generatedTags.tags.map((tag) => BackendSharedUtils.formatTags(tag));
                }

                // Get Similar Startups
                startupData.similarStartups = await CustomUtils.findSimilarStartups(startupData);

                StartupDetailsUtils.submitStartupPendingList(
                    startupData,
                    publishedCreatorUserInfos,
                    Math.round(productItem.originalVotes * 0.2),
                    batch,
                );
                importedStartupList.push(startupData);
            }
        }

        const batchResp = await batch.commit();

        // Update Categories
        let dbAvailableCategories: {[cat: string]: number} | undefined = await RealtimeDbUtils.getData(
            {
                collectionName: CustomRealtimeCollectionDB.SubmitStartupCategories,
            }
        )
        if (!dbAvailableCategories) {
            dbAvailableCategories = {};
        }
        importedStartupList.forEach((startupData) => {
            startupData.categories.forEach((category) => {
                if (Object.keys(dbAvailableCategories!).includes(category)) {
                    dbAvailableCategories![category] = dbAvailableCategories![category] + 1;
                } else {
                    dbAvailableCategories![category] = 1;
                }
            });
        });
        await RealtimeDbUtils.writeData({
            collectionName: CustomRealtimeCollectionDB.SubmitStartupCategories,
            data: dbAvailableCategories,
            merge: false,
        });


        resp.send(getQuickResponse(true, batchResp));
    });