import * as functions from "firebase-functions";
import * as admin from "firebase-admin";
import {StartupDetailsModel, StartupDetailsUtils} from "../../data/model/startup-details.model";
import {getQuickResponse, getResponse} from "../../../shareable-firebase-backend/model-data/api-response.model";
import {Log} from "../../../shareable-firebase-backend/utils/log";
import {CustomFirestoreCollectionDB} from "../../data/custom-firestore-collection-names";
import {getDocumentData} from "../../../shareable-firebase-backend/utils/utils";
import {BaseModelUtils} from "../../../shareable-firebase-backend/model-data/base.model";
import {PublishedUserInfos
} from "../../data/model/published-startup-list.model";
import {
    SharedPublicUserprofileModel
} from "../../../shareable-firebase-backend/model-data/frontend-model-data/shared-public-userprofile.model";
import {SharedFirestoreCollectionDB} from "../../../shareable-firebase-backend/model-data/shared-firestore-collections";
import {CustomUtils} from "../../custom.utils";
import {BackendSharedUtils} from "../../../shareable-firebase-backend/utils/backend-shared-utils";

export const submitStartup = functions.https.onCall(
    async (data: { startupData: StartupDetailsModel, creatorComment?: string},
           context) => {

        if (!context.auth || !context.auth.uid) return getResponse(1001, undefined, "User must be logged in to execute this action");
        const userId: string = context && context.auth && context.auth.uid ? context.auth.uid : "";
        Log.onCall(data, userId);

        const batch = admin.firestore().batch();

        // Create Startup Details
        const startupUid: string = data.startupData.uid ? data.startupData.uid : StartupDetailsUtils.generateStartupUid(data.startupData);

        // Get Public User Infos
        const userInfos: SharedPublicUserprofileModel = await getDocumentData(SharedFirestoreCollectionDB.PublicUserProfile, userId);

        const startupData: StartupDetailsModel = {
            ...BaseModelUtils.initBaseModel({
                collectionName: CustomFirestoreCollectionDB.StartupDetails,
                uid: startupUid,
            }),
            ...data.startupData,
            uid: startupUid,
            creatorUid: userId,
        };
        const publishedUserInfos: PublishedUserInfos = {
            userId: userId,
            photoUrl: userInfos.userPicture?.fileUrl ? userInfos.userPicture?.fileUrl : "",
            username: userInfos.username!,
            name: userInfos.name!,
        }

        // Get Similar Startups
        startupData.similarStartups = await CustomUtils.findSimilarStartups(startupData);

        // Submit Startup
        StartupDetailsUtils.submitStartupPendingList(
            startupData,
            publishedUserInfos,
            BackendSharedUtils.getRandomNumber(70, 100),
            batch,
            data.creatorComment,
        );

        await batch.commit();

        return getQuickResponse(true, data.startupData);
    });