import * as functions from "firebase-functions";
import * as admin from "firebase-admin";
import {getQuickResponse, getResponse} from "../../../shareable-firebase-backend/model-data/api-response.model";
import {Log} from "../../../shareable-firebase-backend/utils/log";
import {
    SharedPublicUserprofileModel
} from "../../../shareable-firebase-backend/model-data/frontend-model-data/shared-public-userprofile.model";
import {getDocumentData} from "../../../shareable-firebase-backend/utils/utils";

import {SharedFirestoreCollectionDB} from "../../../shareable-firebase-backend/model-data/shared-firestore-collections";
import {CustomFirestoreCollectionDB} from "../../data/custom-firestore-collection-names";
import {PublishCommentModel, PublishedUserInfos} from "../../data/model/published-startup-list.model";

export const submitPublicationCommentFunction = functions.https.onCall(
    async (data: {publishedDailyId: string, startupId: string, comment: string, replyCommentId?: string},
           context) => {

        if (!context.auth || !context.auth.uid) return getResponse(1001, undefined, "User must be logged in to execute this action");
        const userId: string = context && context.auth && context.auth.uid ? context.auth.uid : "";
        Log.onCall(data, userId);

        // Get Public User Infos
        const userInfos: SharedPublicUserprofileModel = await getDocumentData<SharedPublicUserprofileModel>(SharedFirestoreCollectionDB.PublicUserProfile, userId);

        if (!userInfos || !userInfos.username) {
            return getQuickResponse(false, null, "User profile is not completed");
        }

        const batch = admin.firestore().batch();

        let dataToUpdate: any = {
            relatedUserInfos: {
                [userId]: {
                    userId: userId,
                    photoUrl: userInfos.userPicture?.fileUrl,
                    username: userInfos.username,
                    name: userInfos.name,
                } as PublishedUserInfos
            },
        };

        const commentData: PublishCommentModel = {
            uid: userId + "_" + Date.now(),
            message: data.comment,
            replies: {},
            userId: userId,
            reactions: {},
            publishedAt: Date.now(),
        };


        if (data.replyCommentId) {
            dataToUpdate = {
                ...dataToUpdate,
                comments: {
                    [data.replyCommentId]: {
                        replies: {
                            [commentData.uid]: commentData,
                        },
                    },
                },
            };
        } else {
            dataToUpdate = {
                ...dataToUpdate,
                comments: {
                    [commentData.uid]: commentData
                },
            };
        }
        batch.set(admin.firestore().collection(CustomFirestoreCollectionDB.DailyLaunches).doc(data.publishedDailyId), {
            [data.startupId]: dataToUpdate
        }, {merge: true});

        return getQuickResponse(true);
    });