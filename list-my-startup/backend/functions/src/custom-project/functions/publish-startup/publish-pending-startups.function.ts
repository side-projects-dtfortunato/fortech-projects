import * as functions from "firebase-functions";
import * as admin from "firebase-admin";
import {PublishedStartupListModel, PublishedStartupListStatus} from "../../data/model/published-startup-list.model";
import {getAllDocuments, getDocumentsWithPagination} from "../../../shareable-firebase-backend/utils/utils";
import {CustomFirestoreCollectionDB, CustomFirestoreDocsDB} from "../../data/custom-firestore-collection-names";
import {getQuickResponse} from "../../../shareable-firebase-backend/model-data/api-response.model";
import {OnesignalUtils} from "../../../shareable-firebase-backend/utils/onesignal.utils";
import {DailyPublishedStartupsModel} from "../../data/model/daily-published-startups.model";
import {BackendSharedUtils} from "../../../shareable-firebase-backend/utils/backend-shared-utils";
import {WEBAPP_CONFIGS} from "../../../configs/webapp.configs";

export const publishPendingStartupsJob = functions.https.onRequest(
    async (req, resp) => {
        // Get Day Id
        const dailyPublishedId: string = BackendSharedUtils.getTodayDay().toString(); // Convert milliseconds to days

        // Get the list of pending projects
        let listPendingStartups: PublishedStartupListModel[] = await getAllDocuments(CustomFirestoreCollectionDB.PendingToLaunch);

        const batch = admin.firestore().batch();

        if (listPendingStartups && listPendingStartups.length > 0) {
            let mapPublishDailyStartups: {
                [startupId: string]: PublishedStartupListModel,
            } = {};
            listPendingStartups.forEach((publishedStartup: PublishedStartupListModel) => {
                publishedStartup.dailyPublicationId = dailyPublishedId;

                mapPublishDailyStartups = {
                    ...mapPublishDailyStartups,
                    [publishedStartup.uid!]: publishedStartup,
                };
                batch.delete(admin.firestore().collection(CustomFirestoreCollectionDB.PendingToLaunch).doc(publishedStartup.uid!));

                publishedStartup.status = PublishedStartupListStatus.PUBLISHED;

                // Add it to Startup Published List
                batch.set(admin.firestore().collection(CustomFirestoreCollectionDB.StartupDetails).doc(publishedStartup.uid!), publishedStartup.startup, {merge: true});
                batch.set(admin.firestore().collection(CustomFirestoreCollectionDB.StartupDetails)
                    .doc(publishedStartup.uid!).collection(CustomFirestoreDocsDB.StartupDetails.PublicPublished).doc(dailyPublishedId!), publishedStartup);

            });

            // Start by hiding votes
            batch.set(admin.firestore().collection(CustomFirestoreCollectionDB.DailyLaunches)
                .doc(dailyPublishedId), {
                publishedDayId: dailyPublishedId,
                hideVotes: true,
            }, {merge: true});

            await batch.commit();

            if (BackendSharedUtils.isProjectProduction()) {
                await OnesignalUtils.sendPushNotification("🚀 Fresh Startups Just Landed!", "Vote now for today's hottest new startup!", WEBAPP_CONFIGS.WEBSITE_URL);
            }

            resp.send(getQuickResponse(true, mapPublishDailyStartups, "Daily Launches Completed"));
        } else {
            resp.send(getQuickResponse(false, null, "Didn't found any Startup pending to launch"));
        }

    });

export const onStartupPublicPublishedChanged = functions
    .firestore
    .document(CustomFirestoreCollectionDB.StartupDetails + '/{startupId}/'
        + CustomFirestoreDocsDB.StartupDetails.PublicPublished + '/{dailyPublishedId}')
    .onWrite(async (data, context) => {

        // Update DailyLaunches
        const batch = admin.firestore().batch();

        if (data.after.exists) {
            batch.set(admin.firestore().collection(CustomFirestoreCollectionDB.DailyLaunches)
                .doc(context.params["dailyPublishedId"]), {
                publishedDayId: context.params["dailyPublishedId"],
                listStartups: {
                    [context.params["startupId"]]: data.after.data()
                },
            }, {merge: true});
        } else {
            // Probably was deleted
            batch.set(admin.firestore().collection(CustomFirestoreCollectionDB.DailyLaunches)
                .doc(context.params["dailyPublishedId"]), {
                listStartups: {
                    [context.params["startupId"]]: admin.firestore.FieldValue.delete(),
                },
            }, {merge: true});
        }

        await batch.commit();

        return getQuickResponse(true);
    });


export const displayTodayVotes = functions.https.onRequest(
    async (req, resp) => {

        const dailyPublished: DailyPublishedStartupsModel[] = await getDocumentsWithPagination(CustomFirestoreCollectionDB.DailyLaunches, 1, "publishedDayId");
        const dailyPublishedId: string = dailyPublished[0].publishedDayId;

        if (dailyPublished[0].hideVotes === false) {
            // Start by hiding votes
            const batch = admin.firestore().batch();
            batch.set(admin.firestore().collection(CustomFirestoreCollectionDB.DailyLaunches)
                .doc(dailyPublishedId), {
                publishedDayId: dailyPublishedId,
                hideVotes: false,
            }, {merge: true});

            await batch.commit();
        }


        resp.send(getQuickResponse(true));
    });
