import * as functions from "firebase-functions";
import {
    SharedNewsletterUserModel
} from "../../../shareable-firebase-backend/model-data/frontend-model-data/shared-newsletter-user.model";
import {RealtimeDbUtils} from "../../../shareable-firebase-backend/utils/realtime-db.utils";
import {
    SharedFirestoreCollectionDB,
    SharedFirestoreDocsDB
} from "../../../shareable-firebase-backend/model-data/shared-firestore-collections";
import {getQuickResponse} from "../../../shareable-firebase-backend/model-data/api-response.model";
import {
    EmailTemplateDigestUtils,
    ListItemsContainer
} from "../../../shareable-firebase-backend/utils/email-utils/email-template-digest-v2.utils";
import {
    BasePostDataModel
} from "../../../shareable-firebase-backend/model-data/frontend-model-data/base-post-data.model";
import {getDocumentData, getDocumentsWithPagination} from "../../../shareable-firebase-backend/utils/utils";
import {
    PostFormArticleBlocksDetails
} from "../../../shareable-firebase-backend/model-data/frontend-model-data/posts-model/post-article-blocks";
import {WEBAPP_CONFIGS} from "../../../configs/webapp.configs";
import {DailyPublishedStartupsModel} from "../../data/model/daily-published-startups.model";
import {CustomFirestoreCollectionDB} from "../../data/custom-firestore-collection-names";
import {CustomRoutesUtils} from "../../custom-routes.utils";
import {StartupDetailsUtils} from "../../data/model/startup-details.model";
import {EmailBulkDeliveryModel} from "../../../shareable-firebase-backend/model-data/email-builk-delivery.model";
import {EmailDestinationModel} from "../../../shareable-firebase-backend/utils/email-utils/email-sender-utils";
import {BackendSharedUtils} from "../../../shareable-firebase-backend/utils/backend-shared-utils";

export const generateEmailDailyDigest = functions.https.onRequest(async (request,
                                                                          response) => {

    // Get subscribed users
    let subscribedUsers: { [userId: string]: SharedNewsletterUserModel } | undefined = await RealtimeDbUtils.getData({
        collectionName: SharedFirestoreCollectionDB.NewsletterCatalog,
        docId: SharedFirestoreDocsDB.NewsletterCatalog.SubscribedUsers,
    });

    if (!subscribedUsers) {
        response.send(getQuickResponse(false, null, "No subscribers found"));
        return;
    }



    if (subscribedUsers && Object.keys(subscribedUsers).length > 0) {
        // Send email
        const emailContent: {
            html: string,
            emailTitle: string,
        } | undefined = await generateDailyDigestHtml();

        if (!emailContent) {
            response.send(getQuickResponse(false));
            return;
        }

        if (subscribedUsers) {
            let destionationsList: EmailDestinationModel[] = Object.values(subscribedUsers)
                .filter((subscribedUser) => {
                    return subscribedUser.userId && BackendSharedUtils.isValidEmail(subscribedUser.email);
                }).map((subscriptionItem) => {
                    return {
                        uid: subscriptionItem.userId,
                        email: subscriptionItem.email,
                        name: subscriptionItem.name ? subscriptionItem.name : subscriptionItem.email,
                    };
                });

            // Store to later be delivered
            await RealtimeDbUtils.writeData({
                collectionName: SharedFirestoreCollectionDB.RTEmailBulkToDeliver,
                docId: Date.now().toString(),
                data: {
                    bodyHtml: emailContent.html,
                    destinationsEmail: destionationsList,
                    subject: emailContent.emailTitle,
                    sentFrom: {
                        name: WEBAPP_CONFIGS.SITE_NAME + " Daily Digest",
                        email: "contact@" + WEBAPP_CONFIGS.SITE_NAME,
                    },
                    createdAt: Date.now(),
                } as EmailBulkDeliveryModel,
                merge: false,
            });
            response.send(getQuickResponse(true, null, "Weekly Digest planned to dispatch"));

        } else {
            response.send(getQuickResponse(false, null, "Not enough posts to notify"));
            return;
        }


    } else {
        response.send(getQuickResponse(false, null, "No users to send digest"));
    }

});


async function generateDailyDigestHtml(): Promise<{
    html: string,
    emailTitle: string,
} | undefined> {
    let listItemsContainer: ListItemsContainer[] = [];
    // Get Most Recent Article
    const dayInMillis: number = 86400000;
    let docRecentPosts: {[postId: string]: BasePostDataModel} | undefined = await getDocumentData<any>(SharedFirestoreCollectionDB.DataCatalog, SharedFirestoreDocsDB.DataCatalog.FilterRecent);
    let listArticles: BasePostDataModel[] = Object.values(docRecentPosts ? docRecentPosts : {}).filter((postData) => postData.postType === "ARTICLE_BLOCKS" && postData.publishedAt > (Date.now() - dayInMillis)) as BasePostDataModel<any, PostFormArticleBlocksDetails>[]
    if (listArticles.length > 0) {
        listItemsContainer.push({
            title: "Recent Articles",
            listItems: listArticles.map((postItem) => {
                return {
                    title: postItem.title!,
                    subtitle: postItem.summary ? postItem.summary : "",
                    imageUrl: postItem.thumbnailUrl,
                    postedOn: "",
                    readMoreUrl: WEBAPP_CONFIGS.WEBSITE_URL + "/" + postItem.uid!,
                };
            }),
        });
    }

    let listLastPublications: DailyPublishedStartupsModel[] = await getDocumentsWithPagination(CustomFirestoreCollectionDB.DailyLaunches, 2, "publishedDayId");

    let emailTitle: string = "";

    if (listLastPublications.length === 0 || listLastPublications[0].publishedDayId !== BackendSharedUtils.getTodayDay().toString()) {
        // Couldn't found any startup
        return undefined;
    }

    // Get Today's Startups
    if (listLastPublications.length > 0) {
        const todayPublishedStartups: ListItemsContainer = {
            title: "Today New Startup",
            listItems: [],
        };
        Object
            .values(listLastPublications[0].listStartups)
            .sort((s1, s2) => StartupDetailsUtils.getTotalVotesPublishedStartup(s2) - StartupDetailsUtils.getTotalVotesPublishedStartup(s1))
            .map((startupItem) => {
                todayPublishedStartups.listItems.push({
                    title: startupItem.startup.name,
                    subtitle: "",
                    summary: startupItem.startup.tagline,
                    postedOn: "",
                    readMoreUrl: CustomRoutesUtils.getStartupListedDetails(startupItem.uid!, startupItem.dailyPublicationId!),
                    imageUrl: startupItem.startup.media.logo?.fileUrl,
                    rightSideImageUrl: "https://www.listmystartup.app/images/ic_upvote_btn.png",
                });
                if (emailTitle.length < 100) {
                    if (emailTitle.length > 0) {
                        emailTitle += " | ";
                    }
                    emailTitle += startupItem.startup.tagline;
                }
            });
        listItemsContainer.push(todayPublishedStartups);
    }

    // Get Yesterday Startups
    if (listLastPublications.length > 0) {
        const yesterdayStartups: ListItemsContainer = {
            title: "Yesterday Top Startups",
            listItems: [],
        };
        Object
            .values(listLastPublications[1].listStartups)
            .sort((s1, s2) => StartupDetailsUtils.getTotalVotesPublishedStartup(s2) - StartupDetailsUtils.getTotalVotesPublishedStartup(s1))
            .map((startupItem) => {
                yesterdayStartups.listItems.push({
                    title: startupItem.startup.name,
                    subtitle: "▲ Votes: " + StartupDetailsUtils.getTotalVotesPublishedStartup(startupItem),
                    summary: startupItem.startup.tagline,
                    postedOn: "",
                    readMoreUrl: CustomRoutesUtils.getStartupListedDetails(startupItem.uid!, startupItem.dailyPublicationId!),
                    imageUrl: startupItem.startup.media.logo?.fileUrl,
                    rightSideImageUrl: "https://www.listmystartup.app/images/ic_upvote_btn.png",
                });
            });
        listItemsContainer.push(yesterdayStartups);
    }



    const html = EmailTemplateDigestUtils.generateHtmlContent({
        headerTitle: WEBAPP_CONFIGS.SITE_NAME + " - Daily Digest",
        headerSubtitle: WEBAPP_CONFIGS.SITE_TAGLINE,
        siteUrl: WEBAPP_CONFIGS.WEBSITE_URL,
        logoUrl: WEBAPP_CONFIGS.LOGO_URL,
        footer: {
            address: WEBAPP_CONFIGS.COMPANY_ADDRESS,
            email: WEBAPP_CONFIGS.COMPANY_EMAIL,
            unsubscribeLink: WEBAPP_CONFIGS.UNSUBSCRIBE_URL,
        },
        listSocialNetworks: [],
        listItemsContainer: listItemsContainer,
    });

    return {
        html: html,
        emailTitle: emailTitle,
    };
}