import {StartupDetailsModel, StartupDetailsUtils} from "../data/model/startup-details.model";
import {
    SharedPublicUserprofileModel
} from "../../shareable-firebase-backend/model-data/frontend-model-data/shared-public-userprofile.model";
import {getDocumentData} from "../../shareable-firebase-backend/utils/utils";
import {SharedFirestoreCollectionDB} from "../../shareable-firebase-backend/model-data/shared-firestore-collections";
import {BackendSharedUtils} from "../../shareable-firebase-backend/utils/backend-shared-utils";
import scrapeIt, {scrapeHTML} from "scrape-it";
import {UrlExtractDataUtils} from "../../shareable-firebase-backend/utils/url-extract-data.utils";
import axios from "axios";
import {WEBAPP_CONFIGS} from "../../configs/webapp.configs";
import {AIGeminiUtils} from "../../shareable-firebase-backend/utils/aigemini.utils";




export class StartupImportUtils {


    static async getTodayBetaListStartups() {
        let listImportedStartups: StartupDetailsModel[] = [];

        // Fetch RSS data
        let betalistData = await BackendSharedUtils.fetchAndParseXML("https://feeds.feedburner.com/BetaList");

        if (betalistData && betalistData.feed && betalistData.feed.entry) {
            for (let betalistItem of betalistData.feed.entry) {
                if (BackendSharedUtils.isDateLessThanHoursAgo(new Date(betalistItem.published as string), 24)) {
                    // Import Startup Details
                    const startupDetails = await this.fetchBetalistStartupDetails(betalistItem.id);

                    if (startupDetails) {
                        listImportedStartups.push(startupDetails);
                    }
                }
            }
        }
        return listImportedStartups;
    }


    static async fetchBetalistStartupDetails(url: string) {

        const urlStartupDetails: string = url + "/visit";
        console.log("urlStartupDetails: ", urlStartupDetails);
        const response = await axios.get(urlStartupDetails, {
            maxRedirects: 10, // Limit maximum redirects to prevent infinite loops
            validateStatus: (status) => status < 400, // Accept all successful status codes
        });

        if (!response.request.res.responseUrl) {
            return null;
        }
        let startupLink: string = response.request.res.responseUrl;
        startupLink = BackendSharedUtils.replaceQParamOnURL(startupLink, "ref", WEBAPP_CONFIGS.SITE_NAME);


        let scrapeResponse = await scrapeIt<any>(url, {
            title: {
                selector: "h1.text-4xl",
                eq: 0
            },
            tagline: {
                selector: "h2.text-3xl",
                eq: 0
            },
            description: {
                selector: "div.text-lg",
                eq: 0,
            },
            categories: {
                listItem: "a[class^='block rounded-full px-3 py-1 border text-sm whitespace-nowrap hover:bg-gray-100 hover:border-gray-300']",
            },
            listImages: {
                listItem: "div.h-52",
                data: {
                    imageUrl: {
                        selector: "img",
                        attr: "src",
                    }
                }
            },
        });

        console.log(scrapeResponse.data);

        if (scrapeResponse.data && scrapeResponse.data.title && scrapeResponse.data.tagline && scrapeResponse.data.listImages) {
            let logo: string | undefined = await UrlExtractDataUtils.extractThumbnail(startupLink);

            let startupDetails: StartupDetailsModel = {
                categories: scrapeResponse.data.categories,
                name: scrapeResponse.data.title,
                tagline: scrapeResponse.data.tagline,
                link: startupLink,
                media: {
                    logo: {
                        storagePath: "",
                        fileUrl: logo ? logo : scrapeResponse.data.listImages[0].imageUrl,
                    },
                    screenshots: scrapeResponse.data.listImages.map((imgUrl: any) => {
                        return {
                            storagePath: "",
                            fileUrl: imgUrl.imageUrl,
                        };
                    })
                },
                tags: [],
                description: scrapeResponse.data.description,
                isAutoImport: true,
                createdAt: Date.now(),
                updatedAt: Date.now(),
                authorization: {
                    allRead: true,
                    allWrite: false,
                },
                otherLinks: [],
            };
            startupDetails.uid = StartupDetailsUtils.generateStartupUid(startupDetails);

            // Generate tags
            const generatedTags: {tags:string[]} | undefined = await AIGeminiUtils.tagJsonContent(JSON.stringify({
                startupTitle: startupDetails.name,
                startupDescription: startupDetails.description,
                startupTagline: startupDetails.tagline,
            }));
            if (generatedTags && generatedTags.tags) {
                startupDetails.tags = generatedTags.tags.map((tag) => BackendSharedUtils.formatTags(tag));
            }

            return startupDetails;
        }

        return undefined;
    }
}