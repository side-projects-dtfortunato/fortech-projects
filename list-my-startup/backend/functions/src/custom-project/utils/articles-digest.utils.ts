import {
    ArticleImportedModel
} from "../../shareable-firebase-backend/base-projects/articles-digest/data/model/ArticleImported.model";
import {BackendSharedUtils} from "../../shareable-firebase-backend/utils/backend-shared-utils";
import {AIGeminiUtils} from "../../shareable-firebase-backend/utils/aigemini.utils";
import {OpenaiUtils} from "../../shareable-firebase-backend/utils/openai/openai.utils";
import {BackendRouteUtils} from "../../shareable-firebase-backend/utils/backend-route.utils";
import {
    SharedArticlesDigestUtils
} from "../../shareable-firebase-backend/base-projects/articles-digest/shared-articles-digest.utils";
import {NotificationsQueueManager} from "../../shareable-firebase-backend/utils/notifications-queue.manager";

const ARTICLES_CONTENT_TOPIC = "Startups and Entrepreneurship";


export class ArticlesDigestUtils {

    static SOURCES_BLACKLIST_DOMAINS = []


    static async fetchGoogleNews(searchTerm: string, searchType: "SEARCH" | "TOPIC"): Promise<any> {
        let listArticles: ArticleImportedModel[] = await SharedArticlesDigestUtils.fetchGoogleNews({
            searchTerm: searchTerm,
            timeFrame: "",
            domainsBlackList: this.SOURCES_BLACKLIST_DOMAINS,
            searchType: searchType,
        });

        // filter by the most relevant and recent articles
        listArticles = listArticles.slice(0, 10); // Only get the top 10 articles
        listArticles = listArticles.filter((googleArticle) => BackendSharedUtils.isLessThanHoursAgo(googleArticle.publishedDate, 24));

        return listArticles;
    }

    static async getMostRecentArticles(): Promise<ArticleImportedModel[]> {
        let listParsedArticles: ArticleImportedModel[] = [];
        listParsedArticles = listParsedArticles.concat(await this.fetchGoogleNews("CAAqIQgKIhtDQkFTRGdvSUwyMHZNREp1ZDNFU0FtVnVLQUFQAQ", "TOPIC")); // Entrepreneurship
        listParsedArticles = listParsedArticles.concat(await this.fetchGoogleNews("CAAqIQgKIhtDQkFTRGdvSUwyMHZNR2d3TVdZU0FtVnVLQUFQAQ", "TOPIC")); // Startups

        return listParsedArticles;
    }

    static async generateAIContent(articleData: ArticleImportedModel, otherArticles?: string[]): Promise<{title: string, category: string, content: string, highlightsBullets: string[], tags: string[], socialNetworkPost: string, isArticleContentValid: boolean} | undefined> {
        return await SharedArticlesDigestUtils.generateAIContentOpenAI(articleData, {
            topic: ARTICLES_CONTENT_TOPIC,
            otherArticles: otherArticles,
        });
    }

    static async rankListArticles(listArticles: ArticleImportedModel[], otherArticlesTitles?: string[]): Promise<{listArticles: {articleUid: string, title: string, articleRelevancy: number, marketSentiment: "bullish"|"bearish"|"neutral", hasSimilarArticles: boolean}[]} | any | undefined> {
        return SharedArticlesDigestUtils.rankListArticles(listArticles, ARTICLES_CONTENT_TOPIC, otherArticlesTitles);
    }

    static generateSocialNetworkPost(articleData: ArticleImportedModel) {
        return SharedArticlesDigestUtils.generateSocialNetworkPost(articleData);
    }

    static async publishArticleSocialNetworks(articleData: ArticleImportedModel) {

        // Add Notification to the queue
        await NotificationsQueueManager.addNotificationItemToQueue({
            uid: articleData.uid!,
            socialPost: this.generateSocialNetworkPost(articleData),
            category: articleData.aiGeneratedContent!.category ? articleData.aiGeneratedContent!.category : articleData.aiGeneratedContent!.tags![0],
            title: BackendSharedUtils.replaceAll(articleData.title, "*", ""),
            link: BackendRouteUtils.getArticleDetails(articleData.uid!),
            socialNetworks: ["REDDIT", "FACEBOOK", "INSTAGRAM", "THREADS"],
            bgImageUrl: articleData.imageUrl!,
            pushNotification: true,
            description: BackendSharedUtils.cleanMarkdown(articleData.aiGeneratedContent.summary!),
        });

    }
}