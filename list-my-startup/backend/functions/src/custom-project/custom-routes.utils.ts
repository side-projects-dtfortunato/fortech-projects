import {WEBAPP_CONFIGS} from "../configs/webapp.configs";

export class CustomRoutesUtils {

    static getSubmitStartupUrl() {
        return WEBAPP_CONFIGS.WEBSITE_URL + "/submit-startup";
    }
    static getStartupListedDetails(startupId: string, publishedDayId: string) {
        return WEBAPP_CONFIGS.WEBSITE_URL + "/startup/" + startupId + "/listed/" + publishedDayId;
    }

    static getStartupDetailsUrl(startupId: string) {
        return WEBAPP_CONFIGS.WEBSITE_URL + "/startup/" + startupId
    }

    static getPublishedStartupsDaily(publishedDayId: string) {
        return WEBAPP_CONFIGS.WEBSITE_URL + "/published-startups-daily/" + publishedDayId;
    }

    static getPendingStartupUrl(startupId: string) {
        return WEBAPP_CONFIGS.WEBSITE_URL + this.getSubmitStartupUrl() + "/pending/" + startupId;
    }

    static getCategoryStartupsUrl(categoryLabel: string) {
        return WEBAPP_CONFIGS.WEBSITE_URL + "/category/" + categoryLabel.replace(" ", "+");
    }
}