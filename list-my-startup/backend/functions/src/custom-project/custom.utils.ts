import {StartupDetailsModel} from "./data/model/startup-details.model";
import * as admin from "firebase-admin";
import {CustomFirestoreCollectionDB} from "./data/custom-firestore-collection-names";
import {BackendSharedUtils} from "../shareable-firebase-backend/utils/backend-shared-utils";

export class CustomUtils {

    static async findSimilarStartups(startupData: StartupDetailsModel): Promise<StartupDetailsModel[]> {
        let listSimilarStartups: StartupDetailsModel[] = [];
        const maxSimilarStartups: number = 4;
        const db = admin.firestore();
        // First try to find another startup with exactly same categories
        let query = db.collection(CustomFirestoreCollectionDB.StartupDetails).where('categories', '==', startupData.categories);
        let querySnapshot = await query.get();
        querySnapshot.forEach((doc) => {
            if (doc.id !== startupData.uid && listSimilarStartups.length < maxSimilarStartups) { // Avoid adding the same startup
                listSimilarStartups.push(doc.data() as StartupDetailsModel);
            }
        });

        // Then try to find the startups with the most same categories, if necessary
        if (listSimilarStartups.length < maxSimilarStartups) {
            query = db.collection(CustomFirestoreCollectionDB.StartupDetails)
                .orderBy("updatedAt", "desc")
                .limit(20)
                .where('categories', 'array-contains-any', startupData.categories);
            querySnapshot = await query.get();

            let listOtherSimilarStartups: StartupDetailsModel[] = [];
            querySnapshot.forEach((doc) => {
                if (doc.id !== startupData.uid && !listSimilarStartups.some(s => s.uid === doc.id)) { // Avoid adding duplicates
                    listOtherSimilarStartups.push(doc.data() as StartupDetailsModel);
                }
            });

            // Now sort by number of equal categories
            listOtherSimilarStartups = listOtherSimilarStartups.sort((s1, s2) => {
                return BackendSharedUtils.compareArrays(s1.categories!, s2.categories);
            });

            for (let similarStartup of listOtherSimilarStartups) {
                if (listSimilarStartups.length >= maxSimilarStartups) break; // Stop if we already have enough startups

                listSimilarStartups.push(similarStartup);
            }

            /*
            for (let category of startupData.categories) {
                if (listSimilarStartups.length >= maxSimilarStartups) break; // Stop if we already have enough startups

                query = db.collection(CustomFirestoreCollectionDB.StartupDetails).where('categories', 'array-contains', category);
                querySnapshot = await query.get();
                querySnapshot.forEach((doc) => {
                    if (doc.id !== startupData.uid && !listSimilarStartups.some(s => s.uid === doc.id)) { // Avoid adding duplicates
                        listSimilarStartups.push(doc.data() as StartupDetailsModel);
                    }
                });
            }*/
        }

        return listSimilarStartups.map((startup) => {
            return {
                ...startup,
                similarStartups: [],
            };
        });
    }


}