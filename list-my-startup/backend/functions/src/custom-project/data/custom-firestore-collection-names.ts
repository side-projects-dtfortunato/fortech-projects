export const CustomFirestoreCollectionDB = {
    StartupDetails: "StartupDetails",
    PendingToLaunch: "PendingToLaunch",
    DailyLaunches: "DailyLaunches",
}

export const CustomRealtimeCollectionDB = {
    SubmitStartupCategories: "SubmitStartupCategories",
}

export const CustomFirestoreDocsDB = {
    StartupDetails: {
        PublicPublished: "PublicPublished",
        AggregatorStartupPublications: "AggregatorStartupPublications"
    },
}