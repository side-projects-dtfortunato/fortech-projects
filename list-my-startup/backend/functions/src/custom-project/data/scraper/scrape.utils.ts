import {StorageFileModel} from "../../../shareable-firebase-backend/model-data/storage-file.model";
import scrapeIt, {scrapeHTML} from "scrape-it";
import {BackendSharedUtils} from "../../../shareable-firebase-backend/utils/backend-shared-utils";
import {UrlExtractDataUtils} from "../../../shareable-firebase-backend/utils/url-extract-data.utils";
import axios from 'axios';
import { JSDOM } from 'jsdom';

// const getMetaData = require('metadata-scraper')


interface NextJSData {
    props: any;
    page: string;
    query: any;
    buildId: string;
    [key: string]: any;
}

interface ProductNextJSData {
    __typename: string;
    id: string;
    slug: string;
    name: string;
    tagline: string;
    description: string;
    [key: string]: any;
}

export interface ProductHuntProductItemModel {
    name: string,
    tagline: string,
    logoUrl: string,
    categories: string[],
    detailsLink: string,
    productUrl: string,
    description?: string,
    screenshots: StorageFileModel[],
    videoLink?: string,
    originalVotes: number,
}

export class ScrapeUtils {
    static async getProductHuntListTodayProducts(): Promise<any> {
        let listProducts: ProductHuntProductItemModel[] = [];
        let htmlContentResult = await UrlExtractDataUtils.getHtmlContent("https://www.producthunt.com/all");
        let scrapeResponse = await scrapeHTML<any>(htmlContentResult.html, {
            listProducts: {
                // listItem: "#__next > div.mt-8.md\\:mt-10.styles_container__eS_WB > main > div > div > div > div:nth-child(2)",
                listItem: "div [data-test=\"homepage-section-0\"] div.styles_item__Dk_nz",
                data: {
                    upvotes: {
                        selector: "div.styles_voteCountItem__zwuqk",
                    },
                    title: {
                        selector: "div.styles_titleItem__bCaNQ",
                    },
                    logo: {
                        selector: "img.styles_mediaThumbnail__NCzNO",
                        attr: "src",
                    },
                    detailsLink: {
                        selector: "a",
                        attr: "href",
                    },
                    productUrl: {
                        selector: "a",
                        attr: "href",
                    }
                }
            }
        });

        console.log(scrapeResponse);

        if (scrapeResponse) {
            scrapeResponse.listProducts
                .filter((item: any) => item.logo && item.productUrl && item.detailsLink)
                .map((item: any) => {
                    listProducts.push({
                        logoUrl: item.logo,
                        detailsLink: item.detailsLink,
                        productUrl: item.productUrl,
                        categories: [],
                        name: item.title,
                        tagline: "",
                        screenshots: [],
                        originalVotes: item.upvotes ? Number(item.upvotes) : 0,
                    });
                })
        }

        return listProducts;
    }

    static async completeProductDetails(productItem: ProductHuntProductItemModel): Promise<ProductHuntProductItemModel | undefined> {


        let htmlContentResult = await UrlExtractDataUtils.getHtmlContent("https://www.producthunt.com" + productItem.detailsLink);
        let scrapeResponse = await scrapeHTML<any>(htmlContentResult.html, {
            name: {
                selector: "h1.styles_title__O2bMP",
                eq: 0
            },
            tagline: {
                selector: "h2.styles_tagline__Mhn2j",
                eq: 0
            },
            description: {
                selector: "div.styles_htmlText__eYPgj",
                eq: 0
            },
            categories: {
                listItem: "a.styles_reset__0clCw div.styles_topicItem__zLFg_ span",
            },
            listImages: {
                listItem: "div[class^='styles_galleryContainer__'] button",
                data: {
                    imageUrl: {
                        selector: "img[class^='styles_image__']",
                        attr: "src",
                    }
                }
            },
        });

        const websiteUrl = this.extractWebsiteUrl(htmlContentResult.html);
        productItem.productUrl = websiteUrl ? websiteUrl : "https://producthunt.com" + productItem.productUrl;


        // Increase logo size
        productItem.logoUrl = productItem.logoUrl.replace("&w=48&h=48", "&w=250&h=250");
        if (scrapeResponse && scrapeResponse.listImages) {
            // Update Product Item Data
            productItem.name = scrapeResponse.name;
            productItem.tagline = scrapeResponse.tagline;
            productItem.description = scrapeResponse.description;
            productItem.categories = scrapeResponse.categories;
            productItem.screenshots = scrapeResponse.listImages
                .filter((imageObj: { imageUrl: string }) => {
                    return BackendSharedUtils.isValidHttpUrl(imageObj.imageUrl);
                })
                .map((imageObj: { imageUrl: string }) => {
                    let updateImageSize: string = BackendSharedUtils.replaceQParamOnURL(imageObj.imageUrl, "w", "600");
                    updateImageSize = BackendSharedUtils.replaceQParamOnURL(updateImageSize, "h", "600");
                    return {
                        fileUrl: updateImageSize,
                        storagePath: "",
                    } as StorageFileModel
                });

            return productItem;
        } else {
            return undefined;
        }
    }

    static extractWebsiteUrl(htmlContent: string): string | null {
        try {
            // Create a DOM from the HTML content
            const dom = new JSDOM(htmlContent);
            const document = dom.window.document;

            // Get all text content from the document
            const textContent = document.documentElement.textContent || '';

            // Find JSON-like content using regex
            const jsonPattern = /{[^{}]*websiteUrl[^{}]*}/g;
            const matches = textContent.match(jsonPattern);

            if (!matches) {
                return null;
            }

            // Look for websiteUrl in each potential JSON match
            for (const match of matches) {
                try {
                    // Try to clean up the JSON string
                    const cleanedJson = match
                        .replace(/(\w+):/g, '"$1":') // Add quotes to keys
                        .replace(/'/g, '"') // Replace single quotes with double quotes
                        .replace(/,\s*}/g, '}') // Remove trailing commas
                        .replace(/,\s*]/g, ']'); // Remove trailing commas in arrays

                    const parsed = JSON.parse(cleanedJson);

                    if (parsed.websiteUrl) {
                        return parsed.websiteUrl;
                    }
                } catch (e) {
                    // Continue to next match if parsing fails
                    continue;
                }
            }

            // If no valid JSON with websiteUrl is found, try regex approach
            const urlPattern = /websiteUrl"?\s*:\s*"([^"]+)"/;
            const urlMatch = textContent.match(urlPattern);

            if (urlMatch && urlMatch[1]) {
                return urlMatch[1];
            }

            return null;
        } catch (error) {
            console.error('Error extracting website URL:', error);
            return null;
        }
    }


}