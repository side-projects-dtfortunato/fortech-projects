import {PublishedStartupListModel} from "./published-startup-list.model";
import {BaseModel} from "../../../shareable-firebase-backend/model-data/base.model";

export interface DailyPublishedStartupsModel extends BaseModel{
    publishedDayId: string;
    hideVotes?: boolean;
    listStartups: {
        [startupId: string]: PublishedStartupListModel;
    }
}