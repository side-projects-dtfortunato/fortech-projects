import {BaseModel, BaseModelUtils} from "../../../shareable-firebase-backend/model-data/base.model";
import {StorageFileModel} from "../../../shareable-firebase-backend/model-data/storage-file.model";
import {firestore} from "firebase-admin";
import WriteBatch = firestore.WriteBatch;
import {CustomFirestoreCollectionDB} from "../custom-firestore-collection-names";
import * as admin from "firebase-admin";
import {
    PublishCommentModel,
    PublishedStartupListModel,
    PublishedStartupListStatus, PublishedUserInfos
} from "./published-startup-list.model";
import {BackendSharedUtils} from "../../../shareable-firebase-backend/utils/backend-shared-utils";

export interface StartupDetailsModel extends BaseModel {
    name: string;
    tagline: string;
    description: string;
    link: string;
    media: {
        logo?: StorageFileModel;
        videoLink?: string;
        screenshots: StorageFileModel[];
    };
    otherLinks: OtherLink[];
    tags: string[];
    categories: string[];
    creatorUid?: string;
    similarStartups?: StartupDetailsModel[];
    isAutoImport?: boolean;
}

export interface OtherLink {
    label: string;
    link: string;
}


export class StartupDetailsUtils {

    static generateStartupUid(startupData: StartupDetailsModel) {
        /*let startupUid: string = new URL(startupData.link).hostname;

        if (startupUid.toLowerCase().startsWith("www.")) {
            startupUid = startupUid.toLowerCase().replace("www.", "");
        }
        startupUid = startupUid.toLowerCase().replace(".", "-");
        startupUid += "-" + BackendSharedUtils.string_to_slug(startupData.name); */
        return BackendSharedUtils.string_to_slug(startupData.name) + "-" + BackendSharedUtils.string_to_slug(startupData.tagline);
    }

    static submitStartupPendingList(startupData: StartupDetailsModel,
                                          creatorUserInfos: PublishedUserInfos | undefined,
                                          boostVotes: number,
                                          batch: WriteBatch, creatorCommentMesssage?: string) {


        startupData.updatedAt = Date.now();

        // Add it to the pending collection
        const publishedStartupListData: PublishedStartupListModel = {
            ...BaseModelUtils.initBaseModel({
                collectionName: CustomFirestoreCollectionDB.PendingToLaunch,
                uid: startupData.uid,
            }),
            status: PublishedStartupListStatus.PENDING_LIST,
            startup: startupData,
            comments: {},
            upvotes: {},
            boostVotes: boostVotes,
            ranks: {},
            relatedUserInfos: {
            },
        };
        if (creatorUserInfos) {
            publishedStartupListData.relatedUserInfos = {
                ...publishedStartupListData.relatedUserInfos,
                [creatorUserInfos.userId]: creatorUserInfos,
            }
        }

        // Check if should add  the creator Comment
        if (creatorCommentMesssage && creatorUserInfos) {
            const creatorComment: PublishCommentModel = {
                uid: creatorUserInfos.userId + "_" + Date.now(),
                message: creatorCommentMesssage!,
                publishedAt: Date.now(),
                reactions: {},
                replies: {},
                pinned: true,
                userId: creatorUserInfos.userId,
            };
            publishedStartupListData.comments = {
                ...publishedStartupListData.comments,
                [creatorComment.uid]: creatorComment,
            };
        }

        batch.set(admin.firestore().collection(CustomFirestoreCollectionDB.PendingToLaunch).doc(startupData.uid!), publishedStartupListData, {merge: true});
    }



    static getTotalVotesPublishedStartup(publishedStartup: PublishedStartupListModel): number {
        let upvotes: number = Object.keys(publishedStartup.upvotes).length;
        if (publishedStartup.boostVotes) {
            upvotes += publishedStartup.boostVotes;
        }
        return upvotes;
    }
}