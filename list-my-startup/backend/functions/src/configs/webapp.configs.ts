
export const PRODUCTION_GCLOUD_PROJECT = "listmystartup-app";
export const WEBAPP_CONFIGS = {
    LOGO_URL: "https://www.ListMyStartup.app/images/logo-white-bg.jpg",
    SITE_NAME: "ListMyStartup.app",
    SITE_TAGLINE: "Discover new startups every day",
    WEBSITE_URL: "https://ListMyStartup.app",
    COMPANY_ADDRESS: "3030 Coimbra (Portugal)",
    COMPANY_EMAIL: "contact@ListMyStartup.app",
    POST_PREFIX_URL: "https://www.ListMyStartup.app/post/",
    PROFILE_PREFIX_URL: "https://www.ListMyStartup.com/public-profile/",
    UNSUBSCRIBE_URL: "https://us-central1-listmystartup-app.cloudfunctions.net/unsubscribeEmail?uid=",

    MAILERSEND_API_KEY: "mlsn.664da241f139a0ce7f1ade28f8a215de5c4cf857cc053270280c9f4b62ee912b",

    CUSTOM_CONFIGS: {
        openPushInApp: true,
    } as any,

    // Account: contact@sidehustlify.com - Di.....oc7
    IMAGE_KIT_API: {
        publicKey : "public_iWUm+7xKzICBvC8mse87J3l8pQk=",
        privateKey : "private_/7D3S4qLgNDfYIa//nJj0yrOtCo=",
        urlEndpoint : "https://ik.imagekit.io/listmystartup",
    },

    STRIPE_API: {
        prod: "",
        dev: "",
        webhook_prod: "",
        webhook_dev: "",
    },

    ONESIGNAL_CONFIGS: {
        apiKey: "Y2U4MzAwYTgtMmE2OC00YzI3LTkzODItMmJmN2VkNmQ0ZjZj",
        appId: "67feb43b-b72a-4fd0-bd39-f58bff3fce01"
    },



    SOCIAL_NETWORK_API_KEY: {
        metaAccessToken: "EAAMahDU79HkBOx1ASkgxLAAaZC5OtZBexUnkkr9lBqE3e5QIwRGcZAI6HvOaau0szNBYp9E2NXAgkCwMjUc5gVZByFGLZC9pXTZAZBS5FrZCHrRcHtLm9gWoo4ZBLWtGm2vubmZCQXnshnm4lhO4NXFwIC7USdjeu9b5yeFycrUuCGdZB4M0qkrozTnAqUnOGP1w0AZD",
        facebookPageId: "380252778498281",
        instagramId: "17841468017891939",
        threadsAPI: {
            clientId: "1846345442539329",
            clientSecret: "469d1bc46bdaa081bf80e03b0bb99d6c",
            redirectUri: `https://us-central1-${PRODUCTION_GCLOUD_PROJECT}.cloudfunctions.net/threadsAPIHandleAuthCallback`,
        },
        redditAPI: {
            clientId: "4AipG2HfGPnuOhtXtOYEmA",
            clientSecret: "9swPagUhH079Yq3uzQjyimQssUYhow",
            username: "FortulyApps",
            password: "divadlooc7",
            subReddit: "ListMyStartup",
        },
        telegram: {
            channel: "",
        },
        bluesky: {
            username: "",
            password: "",
        }
    }
}