/** @type {import('next-sitemap').IConfig} */
module.exports = {
    siteUrl: process.env.SITE_URL || 'https://webtoapp.app',
    generateRobotsTxt: true, // (optional)
    exclude: ["/login", "/login/recover-password", "/edit-post/*", "/edit-profile/user-profile", "/bookmarks", "/studio", "/studio/*", "/admin", "/payment/*", "/privacy-policy"],
    // ...other options
}