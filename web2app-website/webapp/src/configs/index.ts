
export default class RootConfigs {
    static ENVIRONMENT_DEV = false;
    static BASE_URL = "https://webtoapp.app";
    static CONTACT_EMAIL = "contact@webtoapp.app";
    static SITE_NAME = "WebToApp.app";
    static SITE_TITLE = "Web To App - Convert any website into a native app for iOS and Android";
    static SITE_DESCRIPTION = "WebToApp is a Saas platform that lets you convert any website into a native app for iOS and Android. You can customize the style, features, and functionality of your app with no coding required. Try it for free today!";
    static CURRENT_NICHE_SITE = RootConfigs.SITE_NAME;
    static SITE_TOPIC = "Web To App";
    static THEME = {
        PRIMARY_COLOR: "red"
    }
    static META_CONFIGS: any = {
        disableGoogleSignin: false,
        disableNichSites: true,
        updateNicheBackend: false,
        disablePromptUsernameUpdate: false,
    }
    static SOCIAL_NETWORKS: {
        [socialId: string]: {
            iconUrl: string,
            label: string,
            link: string,
        },
    } = {
        medium: {
            label: "WebToApp",
            link: "https://webtoapp.app",
            iconUrl: "https://webtoapp.app/images/logo.png",
        },
    }
    static ONE_SIGNAL_API: string = "";
    static FIREBASE_CONFIG = this.ENVIRONMENT_DEV ?

        // DEV Config
        { // fortuly-qa Keys
            apiKey: "AIzaSyCB2yrNkEHbpShTH_xwHOzcl2mXJT5i1K8",
            authDomain: "fortuly-qa-env.firebaseapp.com",
            projectId: "fortuly-qa-env",
            storageBucket: "fortuly-qa-env.appspot.com",
            messagingSenderId: "650128691290",
            appId: "1:650128691290:web:69f8a283090d23e0454863",
            measurementId: "G-V6XN6K51VZ"
        }
         :
        // PROD - TODO
        {
            apiKey: "AIzaSyBZqghpIkF2pYlO0D5T2raiTt2B362-mog",
            authDomain: "web2app-maker.firebaseapp.com",
            projectId: "web2app-maker",
            storageBucket: "web2app-maker.appspot.com",
            messagingSenderId: "390353665455",
            appId: "1:390353665455:web:4167b4d8ed82e30e57035a",
            measurementId: "G-QSVS18K2X5"
        };


    static IMAGEKIT_API = {
        publicKey:"public_pi/TTr2Kyoqlfd9HUol6GO3ohNY=",
        privateKey:"private_P/RtYhyLuWFIgUGbFPEzuc+xK+c=",
        urlEndpoint:"https://ik.imagekit.io/WebToApp"
    };


    static APP_STORE_LINKS = {
        androidAppId: "",
        iosAppId: "",
    }
    static DISABLE_PAYMENTS = false;
}
