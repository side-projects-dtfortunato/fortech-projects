import CustomRootLayoutComponent from "../../components/app-components/root/CustomRootLayout.component";
import {useEffect, useState} from "react";
import {useRouter} from "next/router";
import {
    StripePaymentStatus
} from "../../components/react-shared-module/logic/shared-data/datamodel/custom-stripe.model";
import Lottie from "lottie-react";
import {LottieMapIconsUtils} from "../../components/react-shared-module/ui-components/lottie/lottie-map-icons.utils";
import {CustomRoutesUtils} from "../../custom-project/custom-routes.utils";

export default function PaymentSuccessPage(props: {projectId: string}) {
    const [paymentStatus, setPaymentStatus] = useState<StripePaymentStatus>(StripePaymentStatus.WAITING_PAYMENT);
    const router = useRouter();

    function renderIcon() {
        switch (paymentStatus) {

            case StripePaymentStatus.CONFIRMED:
                return (
                    <div style={{maxWidth: 125, maxHeight: 125}}>
                        <Lottie animationData={LottieMapIconsUtils.lottiePaymentConfirmedIcon} loop={false} width={75} height={75} />
                    </div>
                );

            case StripePaymentStatus.WAITING_PAYMENT:
            default:
                return (
                    <div style={{maxWidth: 125, maxHeight: 125}}>
                        <Lottie animationData={LottieMapIconsUtils.lottie_waiting_payment} loop={true} width={75} height={75} />
                    </div>
                )
        }
    }

    useEffect(() => {
        setTimeout(() => {
            setPaymentStatus(StripePaymentStatus.CONFIRMED);
        }, 3000);
        setTimeout(() => {
            router.replace(CustomRoutesUtils.getProjectDetailsURL(props.projectId));
        }, 6000);
    }, []);


    return (
        <CustomRootLayoutComponent isIndexable={false}>
            <div className='flex flex-col min-h-72 w-full list-item-bg drop-shadow my-32 gap-10 justify-center items-center'>
                {renderIcon()}
                <h1 className='custom'>{paymentStatus === StripePaymentStatus.WAITING_PAYMENT ? "Verifying Subscription..." : "Congratulations, your subscription was validated."}</h1>
            </div>
        </CustomRootLayoutComponent>
    )
}

PaymentSuccessPage.getInitialProps = async (context: any) => {
    const projectId: string = context.query.projectId as string;

    return {
        projectId,
    };
}

export function getPaymentSuccessPageUrl(projectId: string) {
    return "/payment/success?projectId=" + projectId;
}