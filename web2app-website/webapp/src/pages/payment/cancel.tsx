import CustomRootLayoutComponent from "../../components/app-components/root/CustomRootLayout.component";
import {useRouter} from "next/navigation";
import {useEffect} from "react";
import {CustomRoutesUtils} from "../../custom-project/custom-routes.utils";

export default function PaymentCancelPage(props: {projectId: string}) {
    const router = useRouter();


    useEffect(() => {
        router.replace(CustomRoutesUtils.getProjectDetailsURL(props.projectId));
    }, []);

    return (
        <CustomRootLayoutComponent isIndexable={false}>
            <div className='flex flex-col min-h-72 w-full list-item-bg drop-shadow my-32 gap-10 justify-center items-center'>
                <span className="loading loading-spinner loading-lg"></span>
                <h1 className='custom'>Cancelling payment...</h1>
            </div>
        </CustomRootLayoutComponent>
    )
}

PaymentCancelPage.getInitialProps = async (context: any) => {
    const projectId: string = context.query.projectId as string;

    return {
        projectId,
    };
}