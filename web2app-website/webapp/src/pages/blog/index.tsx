import {
    BasePostDataModel,
    PostDataUtils
} from "../../components/react-shared-module/logic/shared-data/datamodel/base-post-data.model";
import {getAPIDocument} from "../../components/react-shared-module/logic/shared-data/firebase.utils";
import {
    SharedFirestoreCollectionDB,
    SharedFirestoreDocsDB
} from "../../components/react-shared-module/logic/shared-data/datamodel/shared-firestore-collections";
import {
    PostFormArticleBlocksDetails
} from "../../components/react-shared-module/logic/shared-data/datamodel/posts-model/post-article-blocks";
import {PostTypes} from "../../components/react-shared-module/utils/shared-ui-helper";
import CustomRootLayoutComponent from "../../components/app-components/root/CustomRootLayout.component";
import BlogLatestPostsContainerComponent
    from "../../components/react-shared-module/ui-components/blog/shared/blog-latest-posts-container.component";
import LogoComponent from "../../components/react-shared-module/ui-components/shared/Logo.component";
import BlogPostListItemComponent
    from "../../components/react-shared-module/ui-components/blog/shared/blog-post-list-item.component";

export async function getStaticProps(context: any) {
    let docRecentPosts: {[postId: string]: BasePostDataModel} | undefined = await getAPIDocument(SharedFirestoreCollectionDB.DataCatalog, SharedFirestoreDocsDB.DataCatalog.FilterRecent);


    return {
        props: {
            initialListPosts: Object.values(docRecentPosts ? docRecentPosts : {}).filter((postData) => postData.postType === PostTypes.ARTICLE_BLOCKS) as BasePostDataModel<any, PostFormArticleBlocksDetails>[],
        },
        revalidate: 60, // each 60 seconds
    };
}

export default function BlogPage(props: {initialListPosts: BasePostDataModel<any, PostFormArticleBlocksDetails>[]}) {

    function renderRemainingBlogPosts() {
        return props.initialListPosts.slice(4)
            .map((post) => <BlogPostListItemComponent key={post.uid!} postData={post} type={"normal_list"} />);
    }

    return (
        <CustomRootLayoutComponent backgroundColor={"#FFFFFF"}>
            <BlogLatestPostsContainerComponent listPosts={props.initialListPosts} seeMoreArticle={false} />
            <div className='flex flex-col gap-4 my-5'>
                {renderRemainingBlogPosts()}
            </div>
        </CustomRootLayoutComponent>
    )
}