import {
    BasePostDataModel,
    PostDataUtils
} from "../../components/react-shared-module/logic/shared-data/datamodel/base-post-data.model";
import {getAPIDocument} from "../../components/react-shared-module/logic/shared-data/firebase.utils";
import {
    SharedFirestoreCollectionDB,
    SharedFirestoreDocsDB
} from "../../components/react-shared-module/logic/shared-data/datamodel/shared-firestore-collections";
import {PostTypes} from "../../components/react-shared-module/utils/shared-ui-helper";
import PostDetailsPageContentCustomComponent
    from "../../components/app-components/post-details/PostDetailsPageContent.custom.component";

const MAX_SIMILAR_POSTS = 5;

export async function getStaticPaths() {
    let listRecentPosts: {[postId: string]: BasePostDataModel} | undefined = await getAPIDocument(SharedFirestoreCollectionDB.DataCatalog, SharedFirestoreDocsDB.DataCatalog.FilterRecent);
    let listPaths: any[] = [];

    if (listRecentPosts) {
        listPaths = Object.values(listRecentPosts)
            .filter((item) => item.postType !== PostTypes.SHARE_LINK)
            .map((postItem) => {
            return {
                params: {
                    postId: postItem.uid!,
                },
            }
        });
    }

    return {
        paths: listPaths,
        fallback: true, // can also be true or 'blocking'
    }
}

export async function getStaticProps(context: any) {
    const postId: string = context.params.postId as string;
    let postData: BasePostDataModel | undefined = await getAPIDocument(SharedFirestoreCollectionDB.PostsDetails, postId);
    let docRecentPosts: {[postId: string]: BasePostDataModel} | undefined = await getAPIDocument(SharedFirestoreCollectionDB.DataCatalog, SharedFirestoreDocsDB.DataCatalog.FilterRecent);
    let listSimilarPosts: BasePostDataModel[] = [];
    // Display similar posts
    if (postData && docRecentPosts) {
        listSimilarPosts = PostDataUtils.generateListSimilarPosts(postData, docRecentPosts);

        if (listSimilarPosts.length > MAX_SIMILAR_POSTS) {
            listSimilarPosts = listSimilarPosts.slice(0, MAX_SIMILAR_POSTS);
        }
    }

    /*let layoutSideContentData: LayoutSideContentAggregatorModel = await LayoutSideContentAggregatorUtils.getLayoutSideContentAggregatorData({
        loadRecentJobs: true,
    });*/

    return {
        props: {
            postData,
            listSimilarPosts,
            key: postId,
        },
        revalidate: 60, // each 60 seconds
    };
}

export default function PostDetailsPage(props: {postData?: BasePostDataModel, listSimilarPosts?: BasePostDataModel[]}) {
    if (props.postData) {
        return <PostDetailsPageContentCustomComponent postData={props.postData} listSimilarPosts={props.listSimilarPosts} />
    } else {
        return <></>
    }

}

export function getPostDetailsPageURL(postId: string) {
    return "/" + postId;
}
