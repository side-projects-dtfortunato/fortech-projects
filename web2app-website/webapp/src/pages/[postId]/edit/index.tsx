import {
    BasePostDataModel
} from "../../../components/react-shared-module/logic/shared-data/datamodel/base-post-data.model";
import {getAPIDocument} from "../../../components/react-shared-module/logic/shared-data/firebase.utils";
import {
    SharedFirestoreCollectionDB
} from "../../../components/react-shared-module/logic/shared-data/datamodel/shared-firestore-collections";
import {
    PostFormArticleBlocksDetails
} from "../../../components/react-shared-module/logic/shared-data/datamodel/posts-model/post-article-blocks";
import AdminOnlyRootComponent
    from "../../../components/react-shared-module/ui-components/admin/admin-only-root.component";
import CustomRootLayoutComponent from "../../../components/app-components/root/CustomRootLayout.component";
import NotFoundContentComponent
    from "../../../components/react-shared-module/ui-components/shared/NotFoundContent.component";
import dynamic from "next/dynamic";
const EditPostPageComponent = dynamic(() => import("../../../components/react-shared-module/ui-components/edit-post/EditPostPage.component"), {
    ssr: false,
});

export default function EditPostPage(props: {postDetails: BasePostDataModel<any, PostFormArticleBlocksDetails> | undefined}) {


    if (props.postDetails) {
        return (
            <AdminOnlyRootComponent>
                <CustomRootLayoutComponent>
                    <EditPostPageComponent postData={props.postDetails}/>
                </CustomRootLayoutComponent>
            </AdminOnlyRootComponent>
        )
    } else {
        <CustomRootLayoutComponent>
            <NotFoundContentComponent  />
        </CustomRootLayoutComponent>
    }

}

EditPostPage.getInitialProps = async (context: any) => {
    const postId: string = context.query.postId as string;
    let postDetails: BasePostDataModel<any, PostFormArticleBlocksDetails> | undefined = await getAPIDocument<BasePostDataModel<any, PostFormArticleBlocksDetails> | undefined>(SharedFirestoreCollectionDB.PostsDetails, postId);

    return {
        postDetails: postDetails
    };
}