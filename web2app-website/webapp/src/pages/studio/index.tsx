import {ProjectDetailsModel} from "../../custom-project/data/model/project-details.model";
import {useAuthState} from "react-firebase-hooks/auth";
import {auth} from "../../components/react-shared-module/logic/shared-data/firebase.utils";
import DashboardPageComponent from "../../custom-project/ui/dashboard/DashboardPage.component";
import AuthManager from "../../components/react-shared-module/logic/auth/auth.manager";
import LoginRootComponent from "../../components/react-shared-module/ui-components/login/LoginRoot.component";
import {CustomBackendApi} from "../../custom-project/api/custombackend.api";
import CustomRootLayoutComponent from "../../components/app-components/root/CustomRootLayout.component";

export default function StudioDashboard(props: {listProjects?: ProjectDetailsModel[] | undefined}) {
    const [currentUser, isAuthLoading] = useAuthState(auth);

    let rootContent = <></>;

    if (isAuthLoading) {
        rootContent = (
            <div className='flex w-full h-full items-center justify-center'>
                <span className="loading loading-spinner loading-lg"></span>
            </div>
        )
    } else if (AuthManager.isUserLogged()) {
        rootContent = (
            <div className='flex flex-col w-full gap-5 items-center'>
                <img width={250} src={"/images/logo.png"}/>
                <DashboardPageComponent listProjects={props.listProjects} />
                <div className='flex w-full justify-center'>
                    <button className='btn btn-ghost text-red-500' onClick={() => AuthManager.signOut()}>Logout</button>
                </div>
            </div>
        )
    } else {
        rootContent = (
            <LoginRootComponent autoBack={false} />
        )
    }


    return (
        <CustomRootLayoutComponent>
            <div className='flex justify-center w-full min-h-96 py-10'>
                {rootContent}
            </div>
        </CustomRootLayoutComponent>
    )
}

StudioDashboard.getInitialProps = async (context: any) => {

    // Load list of user projects
    let listProjects: ProjectDetailsModel[] = await CustomBackendApi.getUserListProjects();

    return {
        listProjects: listProjects,
        isLoggedIn: AuthManager.isUserLogged(),
    };
}


