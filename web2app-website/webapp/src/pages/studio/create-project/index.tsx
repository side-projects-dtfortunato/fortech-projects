import CreateProjectRootComponent from "../../../custom-project/ui/create-project/CreateProjectRoot.component";
import CustomRootLayoutComponent from "../../../components/app-components/root/CustomRootLayout.component";

export default function CreateProjectPage(props: {url?: string, demoId?: string}) {

    return (
        <CustomRootLayoutComponent customBody={true}>
            <div className='flex justify-center w-full min-h-96 py-10'>
                <CreateProjectRootComponent url={props.url} demoId={props.demoId}/>
            </div>
        </CustomRootLayoutComponent>
    )
}

CreateProjectPage.getInitialProps = async (context: any) => {
    let url: string | undefined = context.query.url;
    let demoId: string | undefined = context.query.demoId;

    if (url !== undefined && !url.startsWith("http")) {
        url = "https://" + url;
    }

    return {
        url,
        demoId,
    };
}

export function getCreateProjectPageUrl() {
    return "/studio/create-project";
}