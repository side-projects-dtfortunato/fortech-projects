import {useState} from "react";
import {
    ProjectDetailsModel, ProjectDetailsUtils,
    ProjectSubscriptionPlan
} from "../../../../custom-project/data/model/project-details.model";
import {
    RootGlobalStateActions,
    useAlertMessageGlobalState
} from "../../../../components/react-shared-module/logic/global-hooks/root-global-state";
import {useAuthState} from "react-firebase-hooks/auth";
import {auth} from "../../../../components/react-shared-module/logic/shared-data/firebase.utils";
import {CustomBackendApi} from "../../../../custom-project/api/custombackend.api";
import ProjectDetailsIOSBuildComponent
    from "../../../../custom-project/ui/project-details/content/ProjectDetailsIOSBuild.component";
import ProjectDetailsAndroidBuildComponent
    from "../../../../custom-project/ui/project-details/content/ProjectDetailsAndroidBuild.component";
import CreateProjectStepPreviewAppComponent
    from "../../../../custom-project/ui/create-project/steps/CreateProjectStepPreviewApp.component";
import ProjectDetailsSettingsComponent
    from "../../../../custom-project/ui/project-details/content/ProjectDetailsSettings.component";
import ProjectDetailsSubscriptionPlanComponent
    from "../../../../custom-project/ui/project-details/content/ProjectDetailsSubscriptionPlan.component";
import ProjectDetailsExternalIntegrationsComponent
    from "../../../../custom-project/ui/project-details/content/ProjectDetailsExternalIntegrations.component";
import CreateProjectStepInitialComponent
    from "../../../../custom-project/ui/create-project/steps/CreateProjectStepInitial.component";
import {FaRocket} from "react-icons/fa";
import Link from "next/link";
import CustomRootLayoutComponent from "../../../../components/app-components/root/CustomRootLayout.component";
import SpinnerComponent from "../../../../components/react-shared-module/ui-components/shared/Spinner.component";
import AuthManager from "../../../../components/react-shared-module/logic/auth/auth.manager";
import ProjectDetailsSideMenuComponent from "../../../../custom-project/ui/side-menu/ProjectDetailsSideMenu.component";
import NotFoundContentComponent
    from "../../../../components/react-shared-module/ui-components/shared/NotFoundContent.component";
import {getLanguageLabel} from "../../../../components/react-shared-module/logic/language/language.helper";
import {CustomRoutesUtils} from "../../../../custom-project/custom-routes.utils";
import ProjectDetailsBuildPlanComponent
    from "../../../../custom-project/ui/project-details/content/ProjectDetailsBuildPlan.component";

export enum ProjectDetailsPageMenuOptions {
    APP_THEME = "APP_THEME",
    ANDROID_BUILD = "ANDROID_BUILD",
    IOS_BUILD = "IOS_BUILD",
    APP_PREVIEW = "APP_PREVIEW",
    SETTINGS = "SETTINGS",
    EXTERNAL_INTEGRATIONS = "EXTERNAL_INTEGRATIONS",
    SUBSCRIPTION = "SUBSCRIPTION",

}

const QParamNavKey = "nav";

export default function ProjectDetailsPage(props: { projectDetails?: ProjectDetailsModel, menuSelectedOption: ProjectDetailsPageMenuOptions }) {
    const [projectDetails, setProjectDetails] = useState(props.projectDetails);
    const [hasDataChanged, setHasDataChanged] = useState(false);
    const [isLoading, setIsLoading] = useState(false);
    const [alertMessage, setAlertMessage] = useAlertMessageGlobalState();
    const [currentUser, isAuthLoading] = useAuthState(auth);

    function onProjectUpdated(projectDetails: ProjectDetailsModel) {
        setHasDataChanged(true);
        setProjectDetails({
            ...projectDetails,
        });
    }

    function onSaveProjectDetails() {
        setIsLoading(true);
        CustomBackendApi.web2appSaveProjectData({projectData: projectDetails!}).then((response) => {
            setIsLoading(false);
            setHasDataChanged(false);
            if (response.isSuccess) {
                RootGlobalStateActions.displayAlertMessage({
                    alertType: "SUCCESS",
                    message: "Project saved with success",
                    setAlertMessageState: setAlertMessage,
                });
                setProjectDetails({
                    ...projectDetails!,
                    hasChangesPublished: false,
                });
            } else {
                RootGlobalStateActions.displayAPIResponseError({
                    apiResponse: response,
                    setAlertMessageState: setAlertMessage,
                });
            }
        });
    }

    async function onPublishChanges() {
        setIsLoading(true);
        if (hasDataChanged) {
            await CustomBackendApi.web2appSaveProjectData({projectData: projectDetails!});
            setHasDataChanged(false);
        }
        await CustomBackendApi.projectPublishChanges({projectId: projectDetails?.uid!});
        projectDetails!.hasChangesPublished = true;
        setProjectDetails({
            ...projectDetails!,
        });
        setIsLoading(false);
    }

    function renderContent() {
        let withBgContainer: boolean = true;
        let content: any;
        switch (props.menuSelectedOption) {
            case ProjectDetailsPageMenuOptions.ANDROID_BUILD:
                content = (<ProjectDetailsAndroidBuildComponent projectData={projectDetails!} onProjectUpdated={onProjectUpdated} />);
                break;

            case ProjectDetailsPageMenuOptions.IOS_BUILD:
                content = (<ProjectDetailsIOSBuildComponent projectData={projectDetails!} onProjectUpdated={onProjectUpdated} />);
                break;

            case ProjectDetailsPageMenuOptions.APP_PREVIEW:
                content = (<CreateProjectStepPreviewAppComponent projectData={projectDetails!} onProjectUpdated={onProjectUpdated} />);
                break;

            case ProjectDetailsPageMenuOptions.SETTINGS:
                content = (<ProjectDetailsSettingsComponent projectDetails={projectDetails!} onProjectUpdated={onProjectUpdated} />);
                break;

            case ProjectDetailsPageMenuOptions.SUBSCRIPTION:
                withBgContainer = false;
                content = (<ProjectDetailsSubscriptionPlanComponent projectDetails={projectDetails!} onProjectUpdated={onProjectUpdated} />);
                break;

            case ProjectDetailsPageMenuOptions.EXTERNAL_INTEGRATIONS:
                content = (<ProjectDetailsExternalIntegrationsComponent projectData={projectDetails!} onProjectUpdated={onProjectUpdated} />);
                break;

            case ProjectDetailsPageMenuOptions.APP_THEME:
            default:
                content = (<CreateProjectStepInitialComponent projectData={projectDetails!} onProjectUpdated={onProjectUpdated} autoImportFromUrl={false}/>);
                break;
        }

        if (withBgContainer) {
            return (
                <div className='list-item-bg border drop-shadow p-5'>
                    {content}
                </div>
            )
        } else {
            return content;
        }
    }

    function renderPublishButton() {
        const publishButtonDisabled: boolean = projectDetails?.hasChangesPublished === true || isLoading
        return (
            <button className={'btn btn-primary text-white rounded-full ' + (isLoading ? "loading":"")} disabled={publishButtonDisabled} onClick={onPublishChanges}><FaRocket/> Publish Changes</button>
        )
    }
    function renderBottomButtons() {
        if (isLoading) {
            return (
                <div className='flex w-full justify-end'>
                    <span className={"loading loading-spinner loading-lg"}/>
                </div>
            );
        } else {
            let listBtns: any[] = [];

            if (props.menuSelectedOption === ProjectDetailsPageMenuOptions.SUBSCRIPTION) {
                return <></>;
            }

            if (props.menuSelectedOption !== ProjectDetailsPageMenuOptions.APP_PREVIEW) {
                listBtns.push((<Link className='btn btn-outline btn-success' href={CustomRoutesUtils.getProjectDetailsURL(props.projectDetails?.uid!, ProjectDetailsPageMenuOptions.APP_PREVIEW)} >{getLanguageLabel("WEB2APP_PAGE_PREVIEW_APP")}</Link>))
            }

            listBtns.push(<button className='btn btn-primary' disabled={!hasDataChanged || isLoading} onClick={onSaveProjectDetails}>Save</button> )

            listBtns.push(renderPublishButton());

            return (
                <div className='flex gap-2 justify-end'>
                    {listBtns}
                </div>
            )
        }

    }

    const sideMenuItemsComponent: any = <ProjectDetailsSideMenuComponent projectId={props.projectDetails!.uid!} selectedMenuOption={props.menuSelectedOption} publishButton={renderPublishButton()} />;

    if (isAuthLoading) {
        return (
            <CustomRootLayoutComponent isIndexable={false}>
                <div className='w-full h-screen'>
                    <SpinnerComponent />
                </div>
            </CustomRootLayoutComponent>
        );
    } else if (props.projectDetails && props.projectDetails.userId === AuthManager.getUserId()) {
        if (ProjectDetailsUtils.hasProjectAnySubscription(props.projectDetails)) {
            return (
                <CustomRootLayoutComponent customNavbarItems={sideMenuItemsComponent} leftChilds={sideMenuItemsComponent}>
                    <div className='flex flex-col gap-2 min-h-72 w-full'>
                        {renderBottomButtons()}
                        {renderContent()}
                    </div>
                </CustomRootLayoutComponent>
            )
        } else {
            return (
                <CustomRootLayoutComponent>
                    <div className='flex flex-col gap-2 min-h-72 w-full'>
                        <ProjectDetailsBuildPlanComponent projectDetails={props.projectDetails} onProjectUpdated={onProjectUpdated} />
                    </div>
                </CustomRootLayoutComponent>
            )
        }

    } else {
        return (
            <CustomRootLayoutComponent>
                <NotFoundContentComponent/>
            </CustomRootLayoutComponent>
        )
    }

}

ProjectDetailsPage.getInitialProps = async (context: any) => {
    const projectId: string = context.query.projectId as string;

    let menuSelectedOption: ProjectDetailsPageMenuOptions | undefined = context.query[QParamNavKey];

    if (!menuSelectedOption) {
        menuSelectedOption = ProjectDetailsPageMenuOptions.APP_THEME;
    }

    // Load Project details
    let projectDetails: ProjectDetailsModel | undefined = await CustomBackendApi.getProjectDetailsData(projectId);

    return {
        projectDetails: projectDetails,
        menuSelectedOption: menuSelectedOption,
    };
}