import Link from "next/link";
import {useState} from "react";
import {useRouter} from "next/router";
import {CustomRoutesUtils} from "../../../../custom-project/custom-routes.utils";
import {ProjectDetailsModel} from "../../../../custom-project/data/model/project-details.model";
import {
    RootGlobalStateActions,
    useAlertMessageGlobalState
} from "../../../../components/react-shared-module/logic/global-hooks/root-global-state";
import {useAuthState} from "react-firebase-hooks/auth";
import {auth} from "../../../../components/react-shared-module/logic/shared-data/firebase.utils";
import {CustomBackendApi} from "../../../../custom-project/api/custombackend.api";
import CustomRootLayoutComponent from "../../../../components/app-components/root/CustomRootLayout.component";
import SpinnerComponent from "../../../../components/react-shared-module/ui-components/shared/Spinner.component";
import AuthManager from "../../../../components/react-shared-module/logic/auth/auth.manager";
import ProjectDetailsSideMenuComponent from "../../../../custom-project/ui/side-menu/ProjectDetailsSideMenu.component";
import {ProjectDetailsPageMenuOptions} from "./index";
import NotFoundContentComponent
    from "../../../../components/react-shared-module/ui-components/shared/NotFoundContent.component";

export default function DeleteProject(props: {projectDetails: ProjectDetailsModel}) {
    const [alertMessage, setAlertMessage] = useAlertMessageGlobalState();
    const [currentUser, isAuthLoading] = useAuthState(auth);
    const [isLoading, setIsLoading] = useState(false);
    const router = useRouter();

    function onDeleteClicked() {
        setIsLoading(true);
        CustomBackendApi.deleteProject({projectId: props.projectDetails.uid!})
            .then((apiResponse) => {
                setIsLoading(false);
            if (apiResponse.isSuccess){
                router.push("/");
            } else {
                RootGlobalStateActions.displayAPIResponseError({
                    apiResponse: apiResponse,
                    setAlertMessageState: setAlertMessage,
                });
            }
        });
    }

    function renderContent() {
        return (
            <div className='flex flex-col gap-5 m-10 items-center'>
                <h2 className='custom text-center'>Are you sure you want to remove this project?</h2>
                <span className='text-center text-slate-400 text-sm'>You cannot revert this action.</span>
                <div className='flex flex-wrap gap-5'>
                    {isLoading ? <></> : <Link className='btn btn-outline' href={CustomRoutesUtils.getProjectDetailsURL(props.projectDetails.uid!, ProjectDetailsPageMenuOptions.SETTINGS)}>Cancel</Link>}
                    <button className={'btn btn-error text-white ' + (isLoading ? "loading" : "")} disabled={isLoading} onClick={onDeleteClicked}>Yes delete this project</button>
                </div>
            </div>
        )
    }

    if (isAuthLoading) {
        return (
            <CustomRootLayoutComponent isIndexable={false}>
                <div className='w-full h-screen'>
                    <SpinnerComponent />
                </div>
            </CustomRootLayoutComponent>
        );
    } else if (props.projectDetails && props.projectDetails.userId === AuthManager.getUserId()) {
        return (
            <CustomRootLayoutComponent leftChilds={<ProjectDetailsSideMenuComponent projectId={props.projectDetails.uid!} selectedMenuOption={ProjectDetailsPageMenuOptions.SETTINGS} />}>
                <div className='flex flex-col gap-2 justify-center py-20'>
                    <div className='list-item-bg border drop-shadow p-5'>
                        {renderContent()}
                    </div>
                </div>
            </CustomRootLayoutComponent>
        )
    } else {
        return (
            <CustomRootLayoutComponent>
                <NotFoundContentComponent/>
            </CustomRootLayoutComponent>
        )
    }
}

DeleteProject.getInitialProps = async (context: any) => {
    const projectId: string = context.query.projectId as string;

    // Load Project details
    let projectDetails: ProjectDetailsModel | undefined = await CustomBackendApi.getProjectDetailsData(projectId);

    return {
        projectDetails: projectDetails,
    };
}

export function getDeleteProjectUrl(projectId: string) {
    return "/project/" + projectId + "/delete-project";
}