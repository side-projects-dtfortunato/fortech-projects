import CustomRootLayoutComponent from "../../components/app-components/root/CustomRootLayout.component";
import LogoComponent from "../../components/react-shared-module/ui-components/shared/Logo.component";
import PricingShortenWebToAppComponent from "../../custom-project/ui/pricing/shared/PricingShortenWebToApp.component";


export async function getStaticProps(context: any) {

    return {
        props: {},
        revalidate: 60, // each 60 seconds
    };
}

export default function PricingPage() {

    return (
        <CustomRootLayoutComponent>
            <div className='flex flex-col item-center gap-1 p-5 my-5'>
                <LogoComponent />
                <PricingShortenWebToAppComponent />
            </div>
        </CustomRootLayoutComponent>
    )
}