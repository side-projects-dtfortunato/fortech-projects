import CustomFaqComponent from "../../custom-project/ui/landing-page/shared/custom-faq.component";
import CustomRootLayoutComponent from "../../components/app-components/root/CustomRootLayout.component";

export async function getStaticProps(context: any) {

    return {
        props: {},
        revalidate: 60, // each 60 seconds
    };
}

export default function FAQPage() {
    return (
        <CustomRootLayoutComponent>
            <CustomFaqComponent isLandingPage={false} />
        </CustomRootLayoutComponent>
    )
}