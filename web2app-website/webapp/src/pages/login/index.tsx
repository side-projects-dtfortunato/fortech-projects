import CustomRootLayoutComponent from "../../components/app-components/root/CustomRootLayout.component";
import LoginRootComponent from "../../components/react-shared-module/ui-components/login/LoginRoot.component";


export default function LoginPage() {

    return (
        <CustomRootLayoutComponent customBody>
            <div className='w-full h-full'>
                <div className='flex h-screen w-full justify-center items-start mt-10 p-2'>
                    <LoginRootComponent />
                </div>
            </div>
        </CustomRootLayoutComponent>
    )
}

export function getLoginPageLink() {
    return "/login";
}