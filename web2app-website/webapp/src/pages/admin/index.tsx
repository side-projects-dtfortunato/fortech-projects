import AdminManagementPageComponent from "../../custom-project/ui/admin/admin-management-page.component";
import AdminOnlyRootComponent from "../../components/react-shared-module/ui-components/admin/admin-only-root.component";

export default function AdminPage() {
    return (
        <AdminOnlyRootComponent>
            <AdminManagementPageComponent />
        </AdminOnlyRootComponent>
    )
}