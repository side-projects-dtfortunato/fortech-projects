import {ProjectDetailsModel} from "../custom-project/data/model/project-details.model";
import LandingPageComponent from "../custom-project/ui/landing-page/landing-page.component";
import {BasePostDataModel} from "../components/react-shared-module/logic/shared-data/datamodel/base-post-data.model";
import {getAPIDocument} from "../components/react-shared-module/logic/shared-data/firebase.utils";
import {
    SharedFirestoreCollectionDB,
    SharedFirestoreDocsDB
} from "../components/react-shared-module/logic/shared-data/datamodel/shared-firestore-collections";
import {PostTypes} from "../components/react-shared-module/utils/shared-ui-helper";
import {
    PostFormArticleBlocksDetails
} from "../components/react-shared-module/logic/shared-data/datamodel/posts-model/post-article-blocks";

export async function getStaticProps(context: any) {
    let docRecentPosts: {[postId: string]: BasePostDataModel} | undefined = await getAPIDocument(SharedFirestoreCollectionDB.DataCatalog, SharedFirestoreDocsDB.DataCatalog.FilterRecent);


    return {
        props: {
            initialListPosts: Object.values(docRecentPosts ? docRecentPosts : {}).filter((postData) => postData.postType === PostTypes.ARTICLE_BLOCKS) as BasePostDataModel<any, PostFormArticleBlocksDetails>[],
        },
        revalidate: 60 * 30, // each 30 minutes
    };
}

export default function Home(props: {initialListPosts: BasePostDataModel<any, PostFormArticleBlocksDetails>[]}) {
    return (
        <LandingPageComponent initialListPosts={props.initialListPosts} />
    )
}



