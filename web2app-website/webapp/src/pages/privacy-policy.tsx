import SharedPrivacyPolicyPageContent
    from "../components/react-shared-module/ui-components/pages-content/SharedPrivacyPolicyPageContent";
import {SharedUtils} from "../components/react-shared-module/utils/shared-utils";
import {CustomRoutesUtils} from "../custom-project/custom-routes.utils";
import {useEffect} from "react";

export default function PrivacyPolicyPage() {

    useEffect(() => {
        return () => {
            SharedUtils.openExternalUrl(CustomRoutesUtils.getWikiDocsUrl() + "/about/privacy-policy");
        };
    }, []);

    return(
        <div></div>
    )
}

export function getPrivacyPolicyPage() {
    return "/privacy-policy";
}