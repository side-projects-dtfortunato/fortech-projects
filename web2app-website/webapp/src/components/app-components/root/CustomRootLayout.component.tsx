import {FooterLinkItem} from "../../react-shared-module/ui-components/root/Footer.component";
import RootLayoutComponent, {PageHeadProps} from "../../react-shared-module/ui-components/root/RootLayout.component";
import {useAuthState} from "react-firebase-hooks/auth";
import {auth} from "../../react-shared-module/logic/shared-data/firebase.utils";
import SpinnerComponent from "../../react-shared-module/ui-components/shared/Spinner.component";

export default function CustomRootLayoutComponent(props: {pageHeadProps?: PageHeadProps, children: any, customBody?: boolean,
    listFooterLinks?: FooterLinkItem[],
    isIndexable?: boolean, isLoading?: boolean, customNavbarItems?: any,
    leftChilds?: any, rigthChilds?: any, isAuthRequired?: boolean, backgroundColor?: string}) {
    const [currentUser, isAuthLoading] = useAuthState(auth);

    function renderLeftSideContent() {
        let leftSideContent: any[] = [];

        if (props.leftChilds) {
            leftSideContent = leftSideContent.concat(props.leftChilds);
        }

        return (
            <div className='flex flex-col gap-3'>
                {leftSideContent}
            </div>
        )
    }

    function renderContent() {
        return (
            <div className='flex flex-row gap-6 w-full m-2 mb-20 justify-center'>
                {props.customBody ? <></> :
                    <div className='hidden md:flex'>{renderLeftSideContent()}</div>}
                <div className='w-full max-w-6xl justify-self-center'>
                    {props.isAuthRequired && isAuthLoading ? <SpinnerComponent/> : props.children}

                </div>
            </div>
        )
    }

    // Override Leftside content
    let customProps: any = {
        ...props,
        customBody: true,
        children: renderContent(),
    }

    return RootLayoutComponent(customProps);
}