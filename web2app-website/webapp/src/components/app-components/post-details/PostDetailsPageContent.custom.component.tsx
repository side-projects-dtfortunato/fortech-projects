import {BasePostDataModel} from "../../react-shared-module/logic/shared-data/datamodel/base-post-data.model";
import {useEffect, useState} from "react";
import {SharedBackendApi} from "../../react-shared-module/logic/shared-data/sharedbackend.api";
import PostDetailsLinkContentComponent
    from "../../react-shared-module/ui-components/posts/post-details/PostDetailsLinkContent.component";
import PostDetailsListOfContentComponent
    from "../../react-shared-module/ui-components/posts/post-details/PostDetailsListOfContent.component";
import PostDetailsDiscussionTopicContentComponent
    from "../../react-shared-module/ui-components/posts/post-details/PostDetailsDiscussionTopicContent.component";
import {PostTypes} from "../../react-shared-module/utils/shared-ui-helper";
import PostDetailsArticleContentComponent
    from "../../react-shared-module/ui-components/posts/post-details/PostDetailsArticleContent.component";
import PostDetailsArticleBlocksContentComponent
    from "../../react-shared-module/ui-components/posts/post-details/PostDetailsArticleBlocksContent.component";
import PostDetailsProductContentComponent
    from "../../react-shared-module/ui-components/posts/post-details/PostDetailsProductContent.component";
import PostDetailsQuickPostContentComponent
    from "../../react-shared-module/ui-components/posts/post-details/PostDetailsQuickPostContent.component";
import NotFoundContentComponent from "../../react-shared-module/ui-components/shared/NotFoundContent.component";
import ListPostsComponent from "../../react-shared-module/ui-components/posts/shared/ListPosts.component";
import SideSimilarListPostsComponent
    from "../../react-shared-module/ui-components/landing-page-content/shared/SideSimilarListPosts.component";
import {PageHeadProps} from "../../react-shared-module/ui-components/root/RootLayout.component";
import RootConfigs from "../../../configs";
import {SharedUtils} from "../../react-shared-module/utils/shared-utils";
import CustomRootLayoutComponent from "../root/CustomRootLayout.component";
import BlogPostContentComponent from "../../react-shared-module/ui-components/blog/content/blog-post-content.component";
import BlogPostListItemComponent
    from "../../react-shared-module/ui-components/blog/shared/blog-post-list-item.component";


export default function PostDetailsPageContentCustomComponent(props: {postData?: BasePostDataModel, listSimilarPosts?: BasePostDataModel[]}) {
    const [postData, setPostData] = useState(props.postData);
    let hasPushedViewEvent: boolean = false;

    useEffect(() => {
        // Update views
        if (props.postData?.uid && !hasPushedViewEvent) {
            hasPushedViewEvent = true;
            SharedBackendApi.pushPostPageView({postId: props.postData?.uid});
        }
    });

    const pageHeadProps: PageHeadProps = {
        title: props.postData?.title ? props.postData?.title : RootConfigs.SITE_TITLE,
        description: props.postData?.summary ? props.postData.summary : RootConfigs.SITE_DESCRIPTION,
        publishedTime: new Date(props.postData?.publishedAt!).toUTCString(),
        imgUrl: props.postData?.thumbnailUrl,
        jsonLdMarkup: props.postData ? SharedUtils.getSchemaMarkupFromPost(props.postData) : undefined,
    };


    function renderSimilarPosts() {
        if (!props.listSimilarPosts || props.listSimilarPosts?.length === 0) {
            return <></>;
        }

        return (
            <div className='flex flex-col mt-5 gap-5'>
                <div className='divider'/>
                <h3 className='text-xl'>Related Articles</h3>
                {props.listSimilarPosts.map((post) => <BlogPostListItemComponent key={postData?.uid!} postData={postData!} type={"normal_list"} />)}
            </div>
        )
    }

    const isIndexable: boolean = props.postData?.postType !== PostTypes.SHARE_LINK;
    return (
        <CustomRootLayoutComponent backgroundColor={"#FFFFFF"} pageHeadProps={pageHeadProps} isIndexable={isIndexable} customBody={true}>
            <div className='w-full flex flex-col gap-2'>
                <BlogPostContentComponent postDetails={props.postData!} />
                {renderSimilarPosts()}
            </div>
        </CustomRootLayoutComponent>
    )
}