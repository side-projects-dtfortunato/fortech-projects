

export const CustomLanguageLabels: {[labelId: string]: string} = {
    LANDING_PAGE_HEADER_JOIN_COMMUNITY_TITLE: "Join our community of Side Hustlers",
    LANDING_PAGE_HEADER_JOIN_COMMUNITY_SUBTITLE: "To discover, share or discuss side hustles ideas and results. Also, share your own side hustles with our community of Side Hustlers",
    WEB2APP_PAGE_APP_STYLE: "App Style",
    WEB2APP_PAGE_ANDROID_BUILD: "Android Build",
    WEB2APP_PAGE_IOS_BUILD: "iOS Build",
    WEB2APP_PAGE_PREVIEW_APP: "App Preview",
    WEB2APP_PAGE_EXTERNAL_INTEGRATIONS: "External Integrations",
    LANDING_PAGE_TOP_TITLE: "Convert your Website into a Mobile App",
    LANDING_PAGE_TOP_SUBTITLE: "in just 5 minutes",
    LANDING_PAGE_TOP_DESCRIPTION: "Create an iOS and/or Android native app converting your website. No-code or other technical expertise is required to use our platform.",


}

export function getCustomLanguageLabel(labelId: string): string | undefined {
    if (Object.keys(CustomLanguageLabels).includes(labelId)) {
        // @ts-ignore
        return CustomLanguageLabels[labelId]! as string;
    } else {
        return undefined;
    }
}