import {BaseModel} from "../../../components/react-shared-module/logic/shared-data/datamodel/base.model";
import {ProjectBottomNavbarItem, ProjectExternalIntegrations, ProjectSubscriptionPlan} from "./project-details.model";
import {StorageFileModel} from "../../../components/react-shared-module/logic/shared-data/datamodel/storage-file.model";


export interface ProjectPublishedAppStyleModel extends BaseModel {
    title?: string;
    url?: string;
    bottomNavbar: ProjectBottomNavbarItem[];
    iconColor?: string;
    navbarBgColor?: string;
    logo?: StorageFileModel;
    subscriptionPlan?: ProjectSubscriptionPlan;
    externalIntegrations?: ProjectExternalIntegrations;
    askFilesPermission?: boolean;
    hasOAuthSignin?: boolean;
}