import {ProjectAndroidConfigsModel, ProjectBuildStatusType, ProjectDetailsModel} from "./project-details.model";
import {BaseModel} from "../../../components/react-shared-module/logic/shared-data/datamodel/base.model";
import {StorageFileModel} from "../../../components/react-shared-module/logic/shared-data/datamodel/storage-file.model";
import {
    StripePaymentStatus
} from "../../../components/react-shared-module/logic/shared-data/datamodel/custom-stripe.model";


export interface ProjectAndroidBuildRequestModel extends BaseModel {
    projectId: string;
    buildConfigs: ProjectAndroidConfigsModel;
    buildReleaseFile?: StorageFileModel;
    status: ProjectBuildStatusType;
    displayMessage?: string;
    projectSnapshot: ProjectDetailsModel;
}