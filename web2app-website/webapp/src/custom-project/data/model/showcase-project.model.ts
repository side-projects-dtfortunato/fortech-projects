export interface ShowcaseProjectModel {
    appName: string,
    description: string,
    tags: string[],
    logo: string,
    images: string[],
    links: {
        website: string,
        playstore?: string,
        appstore?: string,
    }
}

export const CustomShowcaseListData: ShowcaseProjectModel[] = [
    {
        appName: "List My Startup",
        description: "Discover new startups everyday and to inspire you with the next big idea",
        tags: ["Community", "Startup", "Entrepreneurship"],
        logo: "https://firebasestorage.googleapis.com/v0/b/web2app-maker.appspot.com/o/projects%2Flist-my-startup-1710881280447%2Flogo?alt=media&token=aa5bedac-92ea-48de-a58e-805d43ce805c",
        images: [
            "https://play-lh.googleusercontent.com/-Fsy2oiyp2zTUNh8s9mYA-ePPmH7ikupWS4f8QwrdT9F5Rn2N7unhHxAPqyCcQPmZ7o=w5120-h2880-rw",
            "https://play-lh.googleusercontent.com/r60tfv0sLD8d2dtc15wm-5iMrYDXWf_2wpVN1KulTH27sMY0-zar_ZAHeRxF2329L7w=w5120-h2880-rw",
            "https://play-lh.googleusercontent.com/idyHC4r_HCwvKkrCxHvwgU6ALOIP415z0eDEaJrgucxzJeLACnw1UKW7-v6mZPJOeFzg=w5120-h2880-rw",
        ],
        links: {
            website: "https://listmystartup.app",
            appstore: "https://apps.apple.com/pt/app/list-my-startup-startups-hub/id6479720082",
            playstore: "https://play.google.com/store/apps/details?id=listmystartup.app"
        },
    },
    {
        appName: "Remote Work",
        description: "Find the best Remote Jobs for IT, Finances, Design and other areas that allows to work remote. Join our community of remote workers.",
        tags: ["Community", "Career", "Jobs"],
        logo: "https://firebasestorage.googleapis.com/v0/b/web2app-maker.appspot.com/o/sm0oiOOO9CbhtLy5DIbQ7v0BhjL2%2Fprojects%2Fremote-workapp-1706525526624%2Flogo?alt=media&token=3edfd32d-e3aa-4cbd-8b8f-ef1ed9db705d",
        images: [
            "https://play-lh.googleusercontent.com/70jIqM_n0Lo7EZXMThojqyL_AN5M8xZBtL61Bb5PtcAO6kpeBn2c3_RRXj96IT1nF9Sn=w526-h296-rw",
            "https://play-lh.googleusercontent.com/jsbux2wJR9J0nBemkbzedKRjM_HE9WvwO3p66WJMb9s-BmfsDoUB_seac6McEiAKJ0I=w526-h296-rw",
            "https://play-lh.googleusercontent.com/qPaC5PYDAKtxmnkaHnlQmNZjAj3YfR4K-6VQlc7umfrLuZtq2QzomAN2SpF6PkwG-F8=w526-h296-rw",
        ],
        links: {
            website: "https://remote-work.app",
            appstore: "https://apps.apple.com/us/app/remote-work-app-remote-jobs/id1557905686",
            playstore: "https://play.google.com/store/apps/details?id=com.fortuly.remotework"
        },
    },
    {
        appName: "Football Tips Today",
        description: "Football Tips Today it's a simple to use football bet tips prediction app to recommend you the best Football bet opportunities of the day",
        tags: ["Sports", "Gambling", "Entertainment"],
        logo: "https://firebasestorage.googleapis.com/v0/b/web2app-maker.appspot.com/o/sm0oiOOO9CbhtLy5DIbQ7v0BhjL2%2Fprojects%2Ffootball-tips-1704225944676%2Flogo?alt=media&token=25b0324e-2edd-427f-bd4e-8e5d9a908fcc",
        images: [
            "https://play-lh.googleusercontent.com/-Ec6AqEvtSiGn2zpsfzq2M-IPqp3wlF8svUFEbsq0F7kr8n81xhLdh2SK5N4Vi9OTMt3=w526-h296-rw",
            "https://play-lh.googleusercontent.com/AVBALbINgBomVFzItKA8bv31Bft0cz3AFtZIN93UHep4MnszEvqM5x5QfUu2B2y1P-E=w526-h296-rw",
            "https://play-lh.googleusercontent.com/HE2tyPLo8DfqRk-M2aA_gZavMNLHD4pNWqG3AxJY9IWtwJmag98SRB02ZtXUFSmD40I=w526-h296-rw"
        ],
        links: {
            website: "https://footballtips.app",
            appstore: "https://apps.apple.com/us/app/football-tips-today/id6475490250",
            playstore: "https://play.google.com/store/apps/details?id=com.fortuly.footballtips",
        },
    },
    {
        appName: "React Remote Jobs",
        description: "Discover new remote jobs for React developers.",
        tags: ["Career", "Tech", "Jobs", "Community"],
        logo: "https://play-lh.googleusercontent.com/YbBf246TAmglt2_m5fDJNmKDQDPEG9Bwbr9dt0f9gclLtCylsTL017C05MVC-R7M9Zw=w240-h480-rw",
        images: [
            "https://play-lh.googleusercontent.com/ed0aYcX4i1NmJ6C_G3RQw34pe3CLOv9laJTGhpO-HZ2i4U8DbWwiBurF5k0mNlMHzA=w526-h296-rw",
            "https://play-lh.googleusercontent.com/wnAxPVAInTc9aMiQUwOo85Z3r1PRP1NZbiDBN4xT-InFS42t6yT2ZRFPni2RbK9iag=w526-h296-rw",
            "https://play-lh.googleusercontent.com/O-NXUUz7PergI5YbNPUNuoJCiq_UfZNI11Exe819lOUuFL68huiv6FSeDDnn4LYU-A=w526-h296-rw"
        ],
        links: {
            website: "https://reactremotejobs.com",
            playstore: "https://play.google.com/store/apps/details?id=com.web2app.reactjobs",
            appstore: "https://apps.apple.com/us/app/react-remote-jobs/id6470238140",
        }
    }
]