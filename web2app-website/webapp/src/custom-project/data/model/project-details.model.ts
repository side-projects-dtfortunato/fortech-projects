import {ApiResponse, BaseModel} from "../../../components/react-shared-module/logic/shared-data/datamodel/base.model";
import {StorageFileModel} from "../../../components/react-shared-module/logic/shared-data/datamodel/storage-file.model";
import CommonUtils from "../../../components/react-shared-module/logic/commonutils";
import {ProjectIOSBuildRequestModel} from "./project-ios-build-request.model";
import {ProjectAndroidBuildRequestModel} from "./project-android-build-request.model";
import {uploadFileStoragePromise} from "../../../components/react-shared-module/logic/shared-data/firebase.utils";
import {CustomBackendApi} from "../../api/custombackend.api";
import {
    StripePaymentStatus
} from "../../../components/react-shared-module/logic/shared-data/datamodel/custom-stripe.model";
import {ProjectPublishedAppStyleModel} from "./project-published-app-style.model";
import AuthManager from "../../../components/react-shared-module/logic/auth/auth.manager";

export enum ProjectBuildStatusType {
    IN_PROGRESS = "IN_PROGRESS", BUILD_COMPLETED = "BUILD_COMPLETED",
    BUILD_FAILED = "BUILD_FAILED", BUILD_CANCELLED = "BUILD_CANCELLED"
}

export enum ProjectPlatformTypes {ANDROID = "ANDROID", IOS = "IOS"}

export enum ProjectSubscriptionPlan {
    FREE_PLAN = "FREE_PLAN",
    ANDROID_ONLY = "ANDROID_ONLY",
    IOS_ONLY = "IOS_ONLY",
    ANDROID_AND_IOS = "ANDROID_AND_IOS",
}


export function initProjectDetailsModel(props?: {url?: string}): ProjectDetailsModel {
    return {
        title: "",
        url: props?.url,
        androidConfigs: {},
        iosConfigs: {},
        bottomNavbar: [],
        uiStyle: {
            iconColor: "#1B237E",
            navbarBgColor: "#FFFFFF"
        },
        settings: {
            contactEmail: "",
            receiveEmailAlerts: true,
        },
        hasChangesPublished: false,
        appConfigs: {
            hasOAuthSignin: false,
            askFilesPermission: false,
        },
        subscription: {
            planType: ProjectSubscriptionPlan.FREE_PLAN,
            updatedAt: Date.now(),
        },
        hasBuilt: false,
    };
}


export interface ProjectDetailsModel extends BaseModel {
    title?: string;
    url?: string;
    logo?: StorageFileModel;
    androidConfigs: ProjectAndroidConfigsModel;
    iosConfigs: ProjectIOSConfigsModel;
    bottomNavbar: ProjectBottomNavbarItem[];
    uiStyle: {
        iconColor: string,
        navbarBgColor: string,
    };
    appConfigs?: {
        askFilesPermission?: boolean,
        hasOAuthSignin?: boolean,
    };
    userId?: string;

    lastBuilds?: {
        IOS?: ProjectIOSBuildRequestModel;
        ANDROID?: ProjectAndroidBuildRequestModel;
    };
    inProgressBuilds?: {
        IOS?: ProjectIOSBuildRequestModel;
        ANDROID?: ProjectAndroidBuildRequestModel;
    };
    settings: {
        contactEmail: string;
        receiveEmailAlerts: boolean;
    };
    subscription?: {
        planType?: ProjectSubscriptionPlan,
        updatedAt?: number,
    };
    publishedData?: ProjectPublishedAppStyleModel;
    hasChangesPublished?: boolean;
    externalIntegrations?: ProjectExternalIntegrations;
    hasBuilt?: boolean;
}

export interface ProjectAndroidConfigsModel {
    appId?: string;
    keyStore?: StorageFileModel;
    keyStorePassword?: string;
    keyStoreAlias?: string;
    isEditable?: boolean;
    versionCode?: number;
    versionName?: string;
}

export interface ProjectIOSConfigsModel {
    appId?: string;
    provisioningProfileCertificate?: StorageFileModel;
    distributionCertificateFile?: StorageFileModel;
    isEditable?: boolean;
    versionCode?: number;
    versionName?: string;
}

export interface ProjectBottomNavbarItem {
    label?: string;
    url?: string;
    icon?: StorageFileModel;
}

export interface ProjectExternalIntegrations {
    oneSignalAppId?: string;
}

export class ProjectDetailsUtils {

    static isProjectStepValid(projectData: ProjectDetailsModel, currentStep: number) {
        console.log("CurrentStep: ", currentStep);
        switch (currentStep) {
            case 1:
                return AuthManager.isUserLogged();

            case 0:
            default:
                return projectData.title
                    && projectData.title.length > 2
                    && CommonUtils.isValidHttpUrl(projectData.url!)
                    && projectData.logo && projectData.logo.fileUrl;
            /*case 1:
                return projectData.androidConfigs.appId
                    && projectData.androidConfigs.appId.length > 2 && projectData.androidConfigs.appId.includes(".")
                    && projectData.androidConfigs.keyStoreAlias && projectData.androidConfigs.keyStoreAlias.length > 2
                    && projectData.androidConfigs.keyStorePassword && projectData.androidConfigs.keyStorePassword.length > 2;
            case 2:
                return projectData.androidConfigs.appId
                    && projectData.androidConfigs.appId.length > 3
                    && projectData.iosConfigs.provisioningProfileCertificate; */
        }

        return true;
    }

    static isProjectAndroidConfigsValid(projectData: ProjectDetailsModel) {
        return projectData.androidConfigs.appId
            && projectData.androidConfigs.appId.length > 2 && projectData.androidConfigs.appId.includes(".")
            && projectData.androidConfigs.keyStoreAlias && projectData.androidConfigs.keyStoreAlias.length > 2
            && projectData.androidConfigs.keyStorePassword && projectData.androidConfigs.keyStorePassword.length > 2;
    }

    static isProjectIOSConfigsValid(projectData: ProjectDetailsModel) {
        return projectData.iosConfigs.appId
            && projectData.iosConfigs.appId.length > 2 && projectData.iosConfigs.appId.includes(".");
            //&& projectData.iosConfigs.distributionCertificateFile && projectData.iosConfigs.distributionCertificateFile.fileUrl.startsWith("http")
            //&& projectData.iosConfigs.provisioningProfileCertificate && projectData.iosConfigs.provisioningProfileCertificate.fileUrl.startsWith("http");
    }

    static getProjectStorageFolderPath(projectData: ProjectDetailsModel) {
        return "projects/" + projectData.uid! + "/";
    }

    static async uploadKeystoreFile(projectData: ProjectDetailsModel, file: File): Promise<ProjectDetailsModel> {
        let filePath: string = this.getProjectStorageFolderPath(projectData) + "android-keystore";
        // Add file extension
        filePath += CommonUtils.getFileExtension(file);
        const fileUrl: string = await uploadFileStoragePromise(file, filePath);
        const storageFile: StorageFileModel = {
            fileUrl: fileUrl,
            storagePath: filePath,
        };
        const apiResponse: ApiResponse<ProjectDetailsModel> = await CustomBackendApi.updatedAndroidKeystoreProject({
            projectId: projectData.uid!,
            fileData: storageFile
        });

        return apiResponse.responseData!;
    }

    static async uploadProvisioningProfileCertificate(projectData: ProjectDetailsModel, file: File): Promise<ProjectDetailsModel> {
        let filePath: string = this.getProjectStorageFolderPath(projectData) + "provisioning-profile-cert";
        // Add file extension
        filePath += CommonUtils.getFileExtension(file);
        const fileUrl: string = await uploadFileStoragePromise(file, filePath);
        const storageFile: StorageFileModel = {
            fileUrl: fileUrl,
            storagePath: filePath,
        };
        const apiResponse: ApiResponse<ProjectDetailsModel> = await CustomBackendApi.updateiOSProvisioningProfileCert({
            projectId: projectData.uid!,
            fileData: storageFile
        });

        return apiResponse.responseData!;
    }

    static async updateiOSDistributionCert(projectData: ProjectDetailsModel, file: File): Promise<ProjectDetailsModel> {
        let filePath: string = this.getProjectStorageFolderPath(projectData) + "distribution-cert";
        // Add file extension
        filePath += CommonUtils.getFileExtension(file);
        const fileUrl: string = await uploadFileStoragePromise(file, filePath);
        const storageFile: StorageFileModel = {
            fileUrl: fileUrl,
            storagePath: filePath,
        };
        const apiResponse: ApiResponse<ProjectDetailsModel> = await CustomBackendApi.updateiOSDistributionCert({
            projectId: projectData.uid!,
            fileData: storageFile
        });

        return apiResponse.responseData!;
    }

    static async uploadAndroidBuildAppPackageFile(androidBuildRequest: ProjectAndroidBuildRequestModel, file: File): Promise<ProjectAndroidBuildRequestModel> {
        let filePath: string = this.getProjectStorageFolderPath(androidBuildRequest.projectSnapshot)
            + "/android-builds/" + androidBuildRequest.buildConfigs.versionName!
            + "-" + androidBuildRequest.buildConfigs.versionCode + "-"
            + androidBuildRequest.uid!;
        // Add file extension
        filePath += CommonUtils.getFileExtension(file);
        const fileUrl: string = await uploadFileStoragePromise(file, filePath);
        const storageFile: StorageFileModel = {
            fileUrl: fileUrl,
            storagePath: filePath,
        };
        androidBuildRequest.buildReleaseFile = storageFile;

        return androidBuildRequest;
    }

    static getKeytoolTerminalCommand(androidBuildRequest:ProjectAndroidBuildRequestModel) {
        return "keytool -genkey -v -keystore web2app-" + CommonUtils.string_to_slug(androidBuildRequest.buildConfigs.keyStoreAlias!) + ".keystore -alias " + androidBuildRequest.buildConfigs.keyStoreAlias + " -keyalg RSA -sigalg SHA1withRSA -keysize 2048 -validity 10000";
    }

    static getAndroidKeyProperties(androidBuildRequest: ProjectAndroidBuildRequestModel) {
        return "storePassword=" + androidBuildRequest.buildConfigs.keyStorePassword
            + "\nkeyPassword=" + androidBuildRequest.buildConfigs.keyStorePassword
            + "\nkeyAlias=" + androidBuildRequest.buildConfigs.keyStoreAlias
            + "\nstoreFile=./" + "android-keystore.jks";
    }

    static getFlutterAppConfigs(projectDetails: ProjectDetailsModel) {
        let appConfigsStr: string = "";

        appConfigsStr += "static var APP_TITLE = \"" + projectDetails.title + "\";";
        appConfigsStr += "\nstatic var APP_ID = \"" + projectDetails.uid! + "\";";
        appConfigsStr += "\nstatic var APP_WEB_URL = \"" + projectDetails.url! + "\";";
        appConfigsStr += "\nstatic var APP_PRIMARY_COLOR = convertToMaterialColor(\"" + projectDetails.uiStyle.iconColor! + "\");";

        // Bottom Navbar
        appConfigsStr += "\n\n static List<TabPageData> NAVBAR_TABS = [";
        if (projectDetails.bottomNavbar.length > 0) {
            projectDetails.bottomNavbar.forEach((bottomItem) => {
                appConfigsStr += "\nTabPageData(\"" + bottomItem.label +"\", \"" + bottomItem.icon?.fileUrl! + "\", \"" + bottomItem.url! + "\"),";
            });
        }
        appConfigsStr += "\n];";

        return appConfigsStr;
    }

    static isInProgressAndroidBuild(projectDetails: ProjectDetailsModel) {
        return projectDetails.inProgressBuilds;
    }
    static isInProgressIOSBuild(projectDetails: ProjectDetailsModel) {
        return projectDetails.inProgressBuilds;
    }

    static hasProjectAnySubscription(projectDetails: ProjectDetailsModel): boolean {
        return projectDetails.subscription !== undefined
            && (projectDetails.subscription.planType === ProjectSubscriptionPlan.IOS_ONLY
                || projectDetails.subscription.planType === ProjectSubscriptionPlan.ANDROID_ONLY
                || projectDetails.subscription.planType === ProjectSubscriptionPlan.ANDROID_AND_IOS)
    }

    static isProjectWithSubscription(projectDetails: ProjectDetailsModel, platformType: ProjectPlatformTypes) {
        switch (platformType) {
            case ProjectPlatformTypes.IOS:
                return projectDetails.subscription
                    && (projectDetails.subscription.planType === ProjectSubscriptionPlan.ANDROID_AND_IOS || projectDetails.subscription.planType === ProjectSubscriptionPlan.IOS_ONLY);
            case ProjectPlatformTypes.ANDROID:
                return projectDetails.subscription
                    && (projectDetails.subscription.planType === ProjectSubscriptionPlan.ANDROID_ONLY || projectDetails.subscription.planType === ProjectSubscriptionPlan.ANDROID_AND_IOS);
        }

        return false;
    }
}