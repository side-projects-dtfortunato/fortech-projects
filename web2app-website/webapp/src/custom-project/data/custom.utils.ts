

export class CustomUtils {

    static convertToAndroidAppId(text: string): string {
        // Remove leading and trailing whitespaces
        let appId = text.trim();

        // Ensure the app ID is lowercase
        appId = appId.toLowerCase();

        // Add a default package if the app ID is empty
        if (appId.length === 0) {
            appId = 'app.webtoapp.';
        }

        return appId;
    }

}