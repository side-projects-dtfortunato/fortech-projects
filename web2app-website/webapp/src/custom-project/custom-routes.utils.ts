import {getCreateProjectPageUrl} from "../pages/studio/create-project";
import {ProjectDetailsPageMenuOptions} from "../pages/studio/project/[projectId]";
import CommonUtils from "../components/react-shared-module/logic/commonutils";

export class CustomRoutesUtils {

    static getStudioDashboardUrl() {
        return "/studio";
    }
    static getCreateProjectPageUrl(props?: {defaultUrl?: string, demoId?: string}) {
        let url = "/studio/create-project";

        if (props?.defaultUrl) {
            url += "?url=" + props.defaultUrl;
        } else if  (props?.demoId) {
            url += "?demoId=" + props.demoId;
        }

        return url;
    }

    static getPricingPageUrl() {
        return "/pricing";
    }

    static getDeleteProjectUrl(projectId: string) {
        return "/studio/project/" + projectId + "/delete-project";
    }

    static getProjectDetailsURL(projectId: string, menuSelectedOption?: ProjectDetailsPageMenuOptions) {
        let qparams = "";

        if (menuSelectedOption) {
            qparams = "?nav=" + menuSelectedOption;
        }

        return "/studio/project/" + projectId + qparams;
    }

    static getLoginPageLink() {
        return "/login";
    }

    static getAdminPageUrl() {
        return "/admin";
    }



    static getWikiDocsUrl() {
        return "https://docs.webtoapp.app";
    }

    static getWikiDocsiOSBuildGuide() {
        return this.getWikiDocsUrl() + "/ios-guide/ios-app-config-step-by-step-guide";
    }

    static getWikiDociOSDistributionCertificate() {
        return this.getWikiDocsUrl() + "/ios-guide/how-to-generate-a-distribution-certificate-for-ios-app-development";
    }

    static getWikiDociOSProvisioningCertificate() {
        return this.getWikiDocsUrl() + "/ios-guide/how-to-generate-a-provisioning-profile-certificate";
    }

    static getBlogPageUrl() {
        return "/blog";
    }
    static getBlogPostDetailsPageURL(postId: string) {
        return "/" + postId;
    }
    static getFAQPage() {
        return "/faq"
    }
}