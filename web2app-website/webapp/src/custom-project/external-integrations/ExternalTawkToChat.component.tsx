import Script from "next/script";

export default function ExternalTawkToChatComponent(props: {channelType?: "POPUP" | "EMBED"}) {
    return (
        <Script id="Tawk-chat" type="text/javascript">
            {
                `
              var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
                (function(){
                var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
                s1.async=true;
                s1.src='https://embed.tawk.to/65944b3f8d261e1b5f4e6f70/1hj5kbs7r';
                s1.charset='UTF-8';
                s1.setAttribute('crossorigin','*');
                s0.parentNode.insertBefore(s1,s0);
                })();
            `}

        </Script>
    )
}