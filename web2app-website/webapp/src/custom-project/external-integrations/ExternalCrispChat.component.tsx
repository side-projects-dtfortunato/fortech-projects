import {useEffect} from "react";

export default function ExternalCrispChatComponent() {
    return (
        <div dangerouslySetInnerHTML={{__html: "<script type=\"text/javascript\">window.$crisp=[];window.CRISP_WEBSITE_ID=\"70c119e0-67e7-4f29-ac58-ee4baf0acc4c\";(function(){d=document;s=d.createElement(\"script\");s.src=\"https://client.crisp.chat/l.js\";s.async=1;d.getElementsByTagName(\"head\")[0].appendChild(s);})();</script>"}} />
    )
}