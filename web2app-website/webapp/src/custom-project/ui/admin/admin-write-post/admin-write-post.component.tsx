import {useState} from "react";
import {useRouter} from "next/router";
import {
    PushPostDataModel
} from "../../../../components/react-shared-module/logic/shared-data/datamodel/base-post-data.model";
import {
    PostFormArticleBlocksDetails
} from "../../../../components/react-shared-module/logic/shared-data/datamodel/posts-model/post-article-blocks";
import {SharedBackendApi} from "../../../../components/react-shared-module/logic/shared-data/sharedbackend.api";
import {CustomRoutesUtils} from "../../../custom-routes.utils";
import dynamic from "next/dynamic";
const PostFormArticleBlocksComponent = dynamic(() => import("../../../../components/react-shared-module/ui-components/post-form/PostFormArticleBlocks.component"), {
    ssr: false,
});

export default function AdminWritePostComponent() {
    const [isLoading, setIsLoading] = useState(false);
    const router = useRouter();
    async function onSubmitClicked(postData: PushPostDataModel<any, PostFormArticleBlocksDetails>) {
        setIsLoading(true);
        await SharedBackendApi.pushPostData({
            postData: postData,
            isCreation: true,
            thumbnailBase: postData.thumbnailUrl,
        });
        setIsLoading(false);

        await router.push(CustomRoutesUtils.getBlogPostDetailsPageURL(postData.uid!));
    }

    return (
        <PostFormArticleBlocksComponent postType={"ARTICLE_BLOCKS"} onSubmitClicked={onSubmitClicked}></PostFormArticleBlocksComponent>
    )
}