import CustomRootLayoutComponent from "../../../components/app-components/root/CustomRootLayout.component";
import AdminManagementAndroidBuildRequestsComponent
    from "./admin-android-build-requests/admin-management-android-build-requests.component";
import {useState} from "react";
import AdminManagementIOSBuildRequestsComponent
    from "./admin-ios-build-requests/admin-management-ios-build-requests.component";
import AdminDraftDemoProjectFormComponent from "./admin-draft-demo/admin-draft-demo-project-form.component";
import AdminListDemoProjectsComponent from "./admin-draft-demo/admin-list-demo-projects.component";
import AdminWritePostComponent from "./admin-write-post/admin-write-post.component";
import AdminCreateBuildManuallyComponent from "./admin-create-build/admin-create-build-manually.component";

export default function AdminManagementPageComponent() {
    const [tabSelectedIndex, setTabSelectedIndex] = useState(-1);

    function renderTabContent() {
        switch (tabSelectedIndex) {
            case 1:
                return <AdminManagementIOSBuildRequestsComponent />;
            case 2:
                return <AdminDraftDemoProjectFormComponent />;
            case 3:
                return <AdminListDemoProjectsComponent />;
            case 4:
                return <AdminWritePostComponent />;
            case 5:
                return <AdminCreateBuildManuallyComponent />;
            case 0:
                return <AdminManagementAndroidBuildRequestsComponent />;
            default:
                return <></>
        }
    }

    function renderTabButton(index: number, label: string) {
        if (index === tabSelectedIndex) {
            return <button className={"btn btn-primary"} onClick={() => setTabSelectedIndex(index)}>{label}</button>
        } else {
            return <button className={"btn btn-primary btn-outline"} onClick={() => setTabSelectedIndex(index)}>{label}</button>
        }
    }

    function renderTabs() {
        return (
            <div className='flex flex-wrap gap-2 justify-center w-full'>
                {renderTabButton(0, "Android Requests")}
                {renderTabButton(1, "iOS Requests")}
                {renderTabButton(2, "Create Draft Demo")}
                {renderTabButton(3, "List Demo Projects")}
                {renderTabButton(4, "Write Blog Post")}
                {renderTabButton(5, "Create Project Build")}
            </div>
        )
    }

    return (
        <CustomRootLayoutComponent isIndexable={false}>
            <div className='flex flex-col gap-10 mt-10'>
                {renderTabs()}

                <div className='w-full h-min-screen'>
                    {renderTabContent()}
                </div>
            </div>
        </CustomRootLayoutComponent>
    )
}