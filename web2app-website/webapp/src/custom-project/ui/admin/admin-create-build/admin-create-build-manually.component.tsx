import InputTextComponent from "../../../../components/react-shared-module/ui-components/form/InputText.component";
import {useState} from "react";
import {
    RootGlobalStateActions,
    useAlertMessageGlobalState
} from "../../../../components/react-shared-module/logic/global-hooks/root-global-state";
import {CustomBackendApi} from "../../../api/custombackend.api";
import SpinnerComponent from "../../../../components/react-shared-module/ui-components/shared/Spinner.component";

export default function AdminCreateBuildManuallyComponent() {
    const [projectIdFormValue, setProjectIdFormValue] = useState("");
    const [isLoading, setIsLoading] = useState(false);
    const [alertMessage, setAlertMessage] = useAlertMessageGlobalState();

    async function onCreateAndroidBuild() {
        setIsLoading(true);
        // API Request
        const apiResponse = await CustomBackendApi.web2appProjectAndroidBuildRequest({
            projectId: projectIdFormValue,
        });

        if (apiResponse.isSuccess) {
            RootGlobalStateActions.displayAlertMessage({
                alertType: "SUCCESS",
                message: "Android Build created successfully",
                setAlertMessageState: setAlertMessage,
            });
        } else {
            RootGlobalStateActions.displayAPIResponseError({
                apiResponse: apiResponse,
                setAlertMessageState: setAlertMessage,
            });
        }
        setIsLoading(false);
    }

    async function onCreateiOSBuild() {
        setIsLoading(true);
        // API Request
        const apiResponse = await CustomBackendApi.web2appProjectIOSBuildRequest({
            projectId: projectIdFormValue,
        });

        if (apiResponse.isSuccess) {
            RootGlobalStateActions.displayAlertMessage({
                alertType: "SUCCESS",
                message: "iOS Build created successfully",
                setAlertMessageState: setAlertMessage,
            });
        } else {
            RootGlobalStateActions.displayAPIResponseError({
                apiResponse: apiResponse,
                setAlertMessageState: setAlertMessage,
            });
        }
        setIsLoading(false);
    }

    async function onPublishChanges() {
        setIsLoading(true);
        // API Request
        const apiResponse = await CustomBackendApi.projectPublishChanges({
            projectId: projectIdFormValue,
        });

        if (apiResponse.isSuccess) {
            RootGlobalStateActions.displayAlertMessage({
                alertType: "SUCCESS",
                message: "Changes Published",
                setAlertMessageState: setAlertMessage,
            });
        } else {
            RootGlobalStateActions.displayAPIResponseError({
                apiResponse: apiResponse,
                setAlertMessageState: setAlertMessage,
            });
        }
        setIsLoading(false);
    }

    return (
        <div className="flex flex-col gap-5 items-center">
            <InputTextComponent topLabel={"Project Id"} hint={"Write the respective project Id"} onChanged={setProjectIdFormValue} value={projectIdFormValue} />
            {isLoading ? <SpinnerComponent /> : <></>}
            <div className='flex flex-row gap-2'>
                <button className='btn btn-outline btn-secondary' onClick={onCreateAndroidBuild}>Create Android Build</button>
                <button className='btn btn-outline btn-secondary' onClick={onCreateiOSBuild}>Create iOS Build</button>
                <button className='btn btn-outline btn-secondary' onClick={onPublishChanges}>Publish Changes</button>
            </div>
        </div>
    )
}