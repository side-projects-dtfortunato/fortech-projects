import {ProjectDetailsModel, ProjectDetailsUtils} from "../../../data/model/project-details.model";
import CopyToClipboardButtonComponent
    from "../../../../components/react-shared-module/ui-components/shared/CopyToClipboardButton.component";
import Link from "next/link";
import {FaImage} from "react-icons/fa";

export default function AdminCommonBuildStepsComponent(props: {
    projectDetails: ProjectDetailsModel,
    version: string,
    versionCode: number,
}) {
    const versionLabel: string = "version: " + props.version + "+" + props.versionCode;
    return (
        <div className='flex flex-col gap-2'>
            <h3 className='custom'>Multi Platform Configs:</h3>
            <div className='flex flex-wrap items-center rounded-lg bg-slate-200 p-2 gap-2'>
                <span>#WEBTOAPP_APP_TITLE to <b>{props.projectDetails.title!}</b></span>
                <CopyToClipboardButtonComponent text={props.projectDetails.title!} />
            </div>
            <div className="flex items-center rounded-lg bg-slate-200 p-2">
                <span>AppDataManager.APP_ID to <b>{props.projectDetails.uid!}</b></span>
                <CopyToClipboardButtonComponent text={props.projectDetails.uid!} />
            </div>
            <div className="flex items-center rounded-lg bg-slate-200 p-2">
                <span>AppDataManager.DEFAULT_JSON with Default JSON</span>
                <CopyToClipboardButtonComponent text={JSON.stringify(props.projectDetails.publishedData)} />
            </div>
            <div className="flex items-center rounded-lg bg-slate-200 p-2">
                <span>version: 1.0.0+1 → <b>{versionLabel}</b></span>
                <CopyToClipboardButtonComponent text={versionLabel} />
            </div>
            <div className="flex items-center rounded-lg bg-slate-200 p-2">
                <span>Copy logo to <b>assets/images/logo.png</b></span>
            </div>
            <Link className='btn btn-sm btn-outline' href={props.projectDetails.logo?.fileUrl!} target={"_blank"}><FaImage/> Download Logo</Link>

        </div>
    )
}