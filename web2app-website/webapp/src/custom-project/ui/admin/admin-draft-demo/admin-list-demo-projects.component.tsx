import {useEffect, useState} from "react";
import {ProjectDetailsModel} from "../../../data/model/project-details.model";
import {ClientRealtimeDbUtils} from "../../../../components/react-shared-module/logic/shared-data/firebase.utils";
import {CustomRealtimeCollectionDB} from "../../../data/custom-firestore-collection-names";
import {CustomBackendApi} from "../../../api/custombackend.api";
import Link from "next/link";
import {CustomRoutesUtils} from "../../../custom-routes.utils";
import SpinnerComponent from "../../../../components/react-shared-module/ui-components/shared/Spinner.component";

export default function AdminListDemoProjectsComponent() {
    const [listDemoProjects, setListDemoProjects] = useState<ProjectDetailsModel[]>([]);
    const [isLoading, setIsLoading] = useState(false);

    useEffect(() => {
        fetchDemoProjects();
    }, []);

    async function fetchDemoProjects() {
        setIsLoading(true);
        // Get list projects from RealTime DB
        const mapDemoProjects: { [uid: string]: ProjectDetailsModel } | undefined = await ClientRealtimeDbUtils.getData({
            collectionName: CustomRealtimeCollectionDB.DraftDemoProjects,
        });

        if (mapDemoProjects) {
            setListDemoProjects(Object.values(mapDemoProjects));
        }

        setIsLoading(false);
    }

    async function onDeleteDemoProject(projectData: ProjectDetailsModel) {
        setIsLoading(true);

        await CustomBackendApi.adminDeleteDraftDemoProject({projectUid: projectData.uid!});
        await fetchDemoProjects();

        setIsLoading(false);
    }

    function renderListItem(projectData: ProjectDetailsModel) {
        return (
            <Link className='flex list-item-bg rounded-xl p-4 flex flex-col drop-shadow'
                  href={CustomRoutesUtils.getCreateProjectPageUrl({demoId: projectData.uid!})}>
                <h2 className='text-sm'>{projectData.title}</h2>
                <button className='btn btn-xs btn-ghost text-red-400'
                        onClick={() => onDeleteDemoProject(projectData)}>Delete Demo
                </button>
            </Link>
        )
    }

    function renderListDemos() {

        return (
            <div className='flex flex-wrap gap-5'>
                {listDemoProjects.map((item) => renderListItem(item))}
            </div>
        )
    }

    return (
        <div className='flex flex-col gap-5'>
            {isLoading ? <SpinnerComponent /> : <></>}
            {renderListDemos()}
        </div>
    )
}