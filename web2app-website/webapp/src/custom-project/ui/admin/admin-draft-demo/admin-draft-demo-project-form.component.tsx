import CreateProjectStepInitialComponent from "../../create-project/steps/CreateProjectStepInitial.component";
import {useState} from "react";
import {
    initProjectDetailsModel,
    ProjectDetailsModel,
    ProjectDetailsUtils
} from "../../../data/model/project-details.model";
import CreateProjectStepPreviewAppComponent from "../../create-project/steps/CreateProjectStepPreviewApp.component";
import {
    RootGlobalStateActions,
    useAlertMessageGlobalState,
    useIsLoadingGlobalState
} from "../../../../components/react-shared-module/logic/global-hooks/root-global-state";
import {CustomBackendApi} from "../../../api/custombackend.api";
import InputTextComponent from "../../../../components/react-shared-module/ui-components/form/InputText.component";
import {useRouter} from "next/router";
import {CustomRoutesUtils} from "../../../custom-routes.utils";
import {ApiResponse} from "../../../../components/react-shared-module/logic/shared-data/datamodel/base.model";

export default function AdminDraftDemoProjectFormComponent() {
    const [projectData, setProjectData] = useState(initProjectDetailsModel());
    const [isLoading, setIsLoading] = useIsLoadingGlobalState();
    const [alertMessage, setAlertMessage] = useAlertMessageGlobalState();
    const router = useRouter();


    function onProjectUpdated(projectDataChanges: ProjectDetailsModel) {
        setProjectData({
            ...projectDataChanges,
        });
    }

    async function onCreateProjectCreate() {
        setIsLoading(true);

        const apiResponse: ApiResponse<ProjectDetailsModel> = await CustomBackendApi.adminDraftDemoProject({
            projectData: projectData,
        });
        if (apiResponse.isSuccess) {
            router.push(CustomRoutesUtils.getCreateProjectPageUrl({
                demoId: apiResponse.responseData?.uid,
            }));
        } else {
            RootGlobalStateActions.displayAPIResponseError({
                apiResponse: apiResponse,
                setAlertMessageState: setAlertMessage,
            });
        }

        setIsLoading(false);
    }

    return (
        <div className='flex flex-col w-full list-item-bg my-10 p-5 gap-5'>
            <h1 className='text-center text-2xl font-bold'>Draft Demo Project</h1>
            <div className='flex flex-col sm:flex-row gap-4 justify-between'>
                <div className='flex flex-col gap-4'>
                    <CreateProjectStepInitialComponent projectData={projectData} onProjectUpdated={onProjectUpdated} autoImportFromUrl={true} />
                    <InputTextComponent topLabel={"Contact email"} hint={"Company email"} onChanged={(text) => projectData.settings.contactEmail = text} inputType={"email"} />
                </div>

                <CreateProjectStepPreviewAppComponent projectData={projectData} onProjectUpdated={onProjectUpdated} />
            </div>
            <button className='btn btn-primary btn-outline' onClick={onCreateProjectCreate} disabled={!ProjectDetailsUtils.isProjectStepValid(projectData, 0)}>Create Project</button>
        </div>
    )
}