import {useEffect, useState} from "react";
import {ProjectAndroidBuildRequestModel} from "../../../data/model/project-android-build-request.model";
import {CustomBackendApi} from "../../../api/custombackend.api";
import {
    RootGlobalStateActions,
    useAlertMessageGlobalState
} from "../../../../components/react-shared-module/logic/global-hooks/root-global-state";
import SpinnerComponent from "../../../../components/react-shared-module/ui-components/shared/Spinner.component";
import {ProjectIOSBuildRequestModel} from "../../../data/model/project-ios-build-request.model";
import AdminIOSBuildRequestListItemComponent from "./admin-ios-build-request-list-item.component";

export default function AdminManagementIOSBuildRequestsComponent() {
    const [listBuildIOSRequests, setListBuildIOSRequests] = useState<ProjectIOSBuildRequestModel[]>([]);
    const [isLoading, setIsLoading] = useState(true);
    const [alertMessage, setAlertMessage] = useAlertMessageGlobalState();

    useEffect(() => {
        fetchIOSBuildRequests();
    }, []);

    function fetchIOSBuildRequests() {
        setIsLoading(true);
        CustomBackendApi.adminGetiOSBuildRequests({}).then((apiResponse) => {
            if (apiResponse.isSuccess) {
                setListBuildIOSRequests(apiResponse.responseData as ProjectIOSBuildRequestModel[]);
            } else {
                RootGlobalStateActions.displayAPIResponseError({
                    apiResponse: apiResponse,
                    setAlertMessageState: setAlertMessage,
                });
            }
            setIsLoading(false);
        });
    }

    function renderListBuildRequests() {
        return listBuildIOSRequests.map((iosBuildRequest) => {
            return (<AdminIOSBuildRequestListItemComponent key={iosBuildRequest.uid!} iosBuildRequestModel={iosBuildRequest} onRefreshList={onRefreshIOSBuildList} />);
        });
    }

    function onRefreshIOSBuildList() {
        fetchIOSBuildRequests();
    }

    if (isLoading) {
        return (
            <div className='flex w-full h-full justify-center items-center'>
                <SpinnerComponent />
            </div>
        )
    } else {
        return (
            <div className='flex flex-col gap-2'>
                {renderListBuildRequests()}
            </div>
        )
    }

}