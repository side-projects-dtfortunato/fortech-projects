import {JSONTree} from "react-json-tree";
import InputTextComponent from "../../../../components/react-shared-module/ui-components/form/InputText.component";
import {ProjectDetailsModel, ProjectDetailsUtils} from "../../../data/model/project-details.model";
import Link from "next/link";
import {CustomBackendApi} from "../../../api/custombackend.api";
import CopyToClipboardButtonComponent
    from "../../../../components/react-shared-module/ui-components/shared/CopyToClipboardButton.component";
import {ProjectIOSBuildRequestModel} from "../../../data/model/project-ios-build-request.model";
import {FaCloudDownloadAlt} from "react-icons/fa";
import AdminCommonBuildStepsComponent from "../shared/admin-common-build-steps.component";
import React, { useState } from 'react';

export default function AdminIOSBuildRequestListItemComponent(props: { iosBuildRequestModel: ProjectIOSBuildRequestModel, onRefreshList: () => void}) {
    const [iosBuildRequest, setIOSBuildRequest] = useState<ProjectIOSBuildRequestModel>(props.iosBuildRequestModel);
    const [isLoading, setIsLoading] = useState(false);

    async function onCompleteBuildClicked() {
        setIsLoading(true);

        await CustomBackendApi.adminCompleteIOSBuildRequest({
            buildRequestId: iosBuildRequest.uid!,
            projectId: iosBuildRequest.projectId,
            message: iosBuildRequest.displayMessage!,
        });
        setIsLoading(false);
        props.onRefreshList();
    }

    async function onCancelBuildClicked() {
        setIsLoading(true);
        await CustomBackendApi.adminAndroidRejectBuild({
            buildRequestId: iosBuildRequest.uid!,
            message: iosBuildRequest.displayMessage!,
            projectId: iosBuildRequest.projectId,
        });
        setIsLoading(false);
        props.onRefreshList();

    }

    function renderGenerationCommands() {
        return (
            <div className='flex flex-col gap-2'>
                <AdminCommonBuildStepsComponent projectDetails={props.iosBuildRequestModel!.projectSnapshot!} version={props.iosBuildRequestModel.buildConfigs.versionName!} versionCode={props.iosBuildRequestModel.buildConfigs.versionCode!} />
                <div className="flex items-center rounded-lg bg-slate-200 p-2">
                    <p className="text-sm mx-2">
                        &quot;app.webtoapp&quot; to <b>{props.iosBuildRequestModel.buildConfigs.appId!}</b>
                    </p>
                    <CopyToClipboardButtonComponent text={props.iosBuildRequestModel.buildConfigs.appId!} />
                </div>
                <div className="flex items-center rounded-lg bg-slate-200 p-2">
                    <p className="text-sm mx-2">
                        AppConfigs:
                    </p>
                    <CopyToClipboardButtonComponent text={ProjectDetailsUtils.getFlutterAppConfigs(iosBuildRequest.projectSnapshot!)} />
                </div>
                <div className="flex flex-col gap-2 rounded-lg bg-slate-200 p-2">
                    <span className='flex flex-wrap gap-2'>- Copy logo to <b>assets/launcher/ic_ios_launcher.jpg</b></span>
                    <span>- Run: <b>flutter packages pub run flutter_launcher_icons:main</b> <CopyToClipboardButtonComponent text={"flutter packages pub run flutter_launcher_icons:main"} /></span>
                    <span>- Run: <b>flutter build ios</b> <CopyToClipboardButtonComponent text={"flutter build ios"} /></span>
                </div>
            </div>
        );
    }

    function renderDownloadCertificates() {
        if (iosBuildRequest.buildConfigs!.distributionCertificateFile!) {
            return (
                <div className='flex flex-col gap-2'>
                    <h3 className='custom'>Distribution Certificate:</h3>
                    <Link href={iosBuildRequest.buildConfigs!.distributionCertificateFile!.fileUrl} target={"_blank"}
                          className='btn btn-primary btn-outline gap-1'> <FaCloudDownloadAlt/> Download Certificate</Link>
                    <h3 className='custom'>Provisioning Profile Certificate:</h3>
                    <Link href={iosBuildRequest.buildConfigs!.provisioningProfileCertificate!.fileUrl} target={"_blank"}
                          className='btn btn-primary btn-outline gap-1'> <FaCloudDownloadAlt/> Download Certificate</Link>
                </div>
            )
        } else {
            return (<></>)
        }

    }

    function renderCompleteAppGen() {
        let isCancelEnabled: boolean = iosBuildRequest.displayMessage !== undefined && iosBuildRequest.displayMessage.length > 3 && !isLoading;

        return (
            <div className='flex flex-wrap gap-2'>
                <button className={'btn btn-error btn-outline ' + (isLoading ? "loading" : "")}
                        disabled={!isCancelEnabled} onClick={onCancelBuildClicked}>Reject Build
                </button>
                <button className={"btn btn-success " + (isLoading ? "loading" : "")}
                        onClick={onCompleteBuildClicked}>Complete Build
                </button>
            </div>
        )
    }

    function handleToggle() {
        const collapsedContent = document.getElementById('collapsedContent');
        if (collapsedContent) {
            collapsedContent.classList.toggle('hidden');
        }
    }

    return (
        <div tabIndex={0} className="collapse collapse-arrow list-item-bg drop-shadow flex flex-col p-3 gap-3">
            <div className="collapse-title cursor-pointer" onClick={handleToggle}>
                <h2 className='custom'>{props.iosBuildRequestModel.projectSnapshot!.title!}</h2>
            </div>
            <div className="collapse-content flex flex-col hidden" id="collapsedContent">
                <div className='flex flex-col gap-3'>
                    <span className='custom'><span
                        className='font-bold'>Version: </span>{props.iosBuildRequestModel.buildConfigs.versionCode + " - " + props.iosBuildRequestModel.buildConfigs.versionName}</span>
                    <JSONTree data={props.iosBuildRequestModel}/>

                    {renderDownloadCertificates()}

                    {renderGenerationCommands()}
                    <InputTextComponent topLabel={"Build Message"} hint={"Message"} onChanged={(value) => {
                        iosBuildRequest.displayMessage = value
                        setIOSBuildRequest({
                            ...iosBuildRequest,
                        });
                    }}/>
                    {renderCompleteAppGen()}
                </div>
            </div>
        </div>
    )
}


/** FUNCTION TO DOWNLOAD THE IMAGE AND CONVERT IT TO A JPG */
const downloadAsJpeg = async (imageUrl: string, outputFilename: string) => {
    try {
        // Step 1: Fetch the image as a Blob
        const response = await fetch(imageUrl);
        const blob = await response.blob();

        // Step 2: Convert it to JPEG
        const canvas = document.createElement('canvas');
        const ctx = canvas.getContext('2d');
        const img = new Image();

        img.src = URL.createObjectURL(blob);
        await new Promise((resolve, reject) => {
            img.onload = () => {
                canvas.width = img.width;
                canvas.height = img.height;
                ctx?.drawImage(img, 0, 0, img.width, img.height);
                resolve(true);
            };
            img.onerror = reject;
        });

        // Step 3: Trigger the download
        canvas.toBlob((jpegBlob) => {
            const newImgUrl = URL.createObjectURL(jpegBlob!);
            const link = document.createElement('a');
            link.href = newImgUrl;
            link.download = outputFilename;
            link.click();

            // Cleanup: revoke the object URL to free up resources
            URL.revokeObjectURL(newImgUrl);
            URL.revokeObjectURL(img.src);
        }, 'image/jpeg');
    } catch (error) {
        console.error('Error downloading image:', error);
    }
};
