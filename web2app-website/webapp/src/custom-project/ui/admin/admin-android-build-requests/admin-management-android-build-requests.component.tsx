import {useEffect, useState} from "react";
import {ProjectAndroidBuildRequestModel} from "../../../data/model/project-android-build-request.model";
import {CustomBackendApi} from "../../../api/custombackend.api";
import {
    RootGlobalStateActions,
    useAlertMessageGlobalState
} from "../../../../components/react-shared-module/logic/global-hooks/root-global-state";
import SpinnerComponent from "../../../../components/react-shared-module/ui-components/shared/Spinner.component";
import {JSONTree} from "react-json-tree";
import AdminAndroidBuildRequestListItemComponent from "./admin-android-build-request-list-item.component";

export default function AdminManagementAndroidBuildRequestsComponent() {
    const [listBuildAndroidRequests, setListBuildAndroidRequests] = useState<ProjectAndroidBuildRequestModel[]>([]);
    const [isLoading, setIsLoading] = useState(true);
    const [alertMessage, setAlertMessage] = useAlertMessageGlobalState();

    useEffect(() => {
        fetchAndroidBuildRequests();
    }, []);

    function fetchAndroidBuildRequests() {
        setIsLoading(true);
        CustomBackendApi.adminGetAndroidBuildRequests({}).then((apiResponse) => {
            if (apiResponse.isSuccess) {
                setListBuildAndroidRequests(apiResponse.responseData as ProjectAndroidBuildRequestModel[]);
            } else {
                RootGlobalStateActions.displayAPIResponseError({
                    apiResponse: apiResponse,
                    setAlertMessageState: setAlertMessage,
                });
            }
            setIsLoading(false);
        });
    }

    function renderListBuildRequests() {
        return listBuildAndroidRequests.map((androidBuildRequest) => {
            return (<AdminAndroidBuildRequestListItemComponent key={androidBuildRequest.uid!} androidBuildRequest={androidBuildRequest} onRefreshList={onRefreshAndroidBuildList} />);
        });
    }

    function onRefreshAndroidBuildList() {
        fetchAndroidBuildRequests();
    }

    if (isLoading) {
        return (
            <div className='flex w-full h-full justify-center items-center'>
                <SpinnerComponent />
            </div>
        )
    } else {
        return (
            <div className='flex flex-col gap-2'>
                {renderListBuildRequests()}
            </div>
        )
    }

}