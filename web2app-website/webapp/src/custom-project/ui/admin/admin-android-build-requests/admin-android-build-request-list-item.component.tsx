import {ProjectAndroidBuildRequestModel} from "../../../data/model/project-android-build-request.model";
import {JSONTree} from "react-json-tree";
import {useState} from "react";
import InputTextComponent from "../../../../components/react-shared-module/ui-components/form/InputText.component";
import {ProjectDetailsModel, ProjectDetailsUtils} from "../../../data/model/project-details.model";
import Link from "next/link";
import {CustomBackendApi} from "../../../api/custombackend.api";
import CopyToClipboardButtonComponent
    from "../../../../components/react-shared-module/ui-components/shared/CopyToClipboardButton.component";
import AdminCommonBuildStepsComponent from "../shared/admin-common-build-steps.component";
import {ShouldExpandNodeInitially} from "react-json-tree/src/types";

export default function AdminAndroidBuildRequestListItemComponent(props: { androidBuildRequest: ProjectAndroidBuildRequestModel, onRefreshList: () => void}) {
    const [androidBuildRequest, setAndroidBuildRequest] = useState<ProjectAndroidBuildRequestModel>(props.androidBuildRequest);
    const [isLoading, setIsLoading] = useState(false);

    async function onCompleteBuildClicked() {
        setIsLoading(true);

        const updatedAndroidBuildRequest = await ProjectDetailsUtils.uploadAndroidBuildAppPackageFile(androidBuildRequest, androidBuildRequest.buildReleaseFile?.tempFile!);
        await CustomBackendApi.adminCompleteAndroidBuildRequest({
            buildRequestId: updatedAndroidBuildRequest.uid!,
            projectId: updatedAndroidBuildRequest.projectId,
            appPackage: updatedAndroidBuildRequest.buildReleaseFile!,
            message: androidBuildRequest.displayMessage!,
        });
        setAndroidBuildRequest(updatedAndroidBuildRequest);
        setIsLoading(false);
        props.onRefreshList();
    }

    async function onCancelBuildClicked() {
        setIsLoading(true);
        await CustomBackendApi.adminAndroidRejectBuild({
            buildRequestId: androidBuildRequest.uid!,
            message: androidBuildRequest.displayMessage!,
            projectId: androidBuildRequest.projectId,
        });
        setIsLoading(false);
        props.onRefreshList();

    }

    function handleAppPackageFile(event: any) {
        let uploadFile = event.target.files[0];
        setAndroidBuildRequest({
            ...androidBuildRequest,
            buildReleaseFile: {
                fileUrl: "",
                storagePath: "",
                tempFile: uploadFile,
            },
        });
    }

    async function handleKeystoreFile(event: any) {
        let uploadFile = event.target.files[0];
        setIsLoading(true);

        // Upload File
        const projectData: ProjectDetailsModel = await ProjectDetailsUtils.uploadKeystoreFile(androidBuildRequest.projectSnapshot, uploadFile);

        setAndroidBuildRequest({
            ...androidBuildRequest,
            buildConfigs: projectData.androidConfigs,
            projectSnapshot: projectData,
        });
        setIsLoading(false);
    }

    function renderUploadKeystoreFile() {
        if (androidBuildRequest.buildConfigs.keyStore && androidBuildRequest.buildConfigs.keyStore.fileUrl.startsWith("http")) {
            return (
                <div className='flex flex-col'>
                <h3 className='custom'>Keystore:</h3>
                <Link href={androidBuildRequest.buildConfigs.keyStore.fileUrl} target={"_blank"} className='btn btn-primary btn-outline'>Download
                    Keystore</Link>
            </div>);
        } else {
            const keystoreCommand: string = ProjectDetailsUtils.getKeytoolTerminalCommand(androidBuildRequest);
            return (
                <div className='flex flex-col gap-2'>
                    <h3 className='custom'>Keystore:</h3>
                    <input type="file" accept="\.jks$" className="file-input w-full max-w-xs"
                           onInput={handleKeystoreFile}/>
                    <div className="flex items-center overflow-x-auto rounded-lg bg-slate-200 p-2">
                        <CopyToClipboardButtonComponent text={keystoreCommand} />
                        <p className="whitespace-nowrap text-sm mx-2">
                            {keystoreCommand}
                        </p>
                    </div>
                </div>
            );
        }
    }

    function renderGenerationCommands() {
        return (
            <div className='flex flex-col gap-2'>
                <AdminCommonBuildStepsComponent projectDetails={props.androidBuildRequest.projectSnapshot} version={props.androidBuildRequest.buildConfigs.versionName!} versionCode={props.androidBuildRequest.buildConfigs.versionCode!} />
                <h3 className='custom'>Android Commands:</h3>
                <div className="flex items-center rounded-lg bg-slate-200 p-2">
                    <p className="text-sm mx-2">
                        key.properties:
                    </p>
                    <CopyToClipboardButtonComponent text={ProjectDetailsUtils.getAndroidKeyProperties(androidBuildRequest)} />
                </div>
                <div className="flex items-center rounded-lg bg-slate-200 p-2">
                    <p className="text-sm mx-2">
                        &quot;app.webtoapp&quot; to <b>{props.androidBuildRequest.buildConfigs.appId!}</b>
                    </p>
                    <CopyToClipboardButtonComponent text={props.androidBuildRequest.buildConfigs.appId!} />
                </div>
                <div className="flex flex-col gap-2 rounded-lg bg-slate-200 p-2">
                    <span>- Copy logo to <b>assets/launcher/ic_android_launcher.png</b></span>
                    <span>- Run: <b>flutter packages pub run flutter_launcher_icons:main</b> <CopyToClipboardButtonComponent text={"flutter packages pub run flutter_launcher_icons:main"} /></span>
                    <span>- Run: <b>flutter build appbundle</b> <CopyToClipboardButtonComponent text={"flutter build appbundle"} /></span>
                </div>
            </div>
        );
    }

    function renderUploadAppPackageFile() {
        return (
            <div className='flex flex-col'>
                <h3 className='custom'>App Package:</h3>
                <input type="file" className="file-input w-full max-w-xs" onInput={handleAppPackageFile}/>
            </div>
        );
    }

    function renderCompleteAppGen() {
        let isValid: boolean = androidBuildRequest.buildConfigs.keyStore !== undefined && androidBuildRequest.buildReleaseFile !== undefined && !isLoading;
        let isCancelEnabled: boolean = androidBuildRequest.displayMessage !== undefined && androidBuildRequest.displayMessage.length > 3 && !isLoading;

        return (
            <div className='flex flex-wrap gap-2'>
                <button className={'btn btn-error btn-outline ' + (isLoading ? "loading" : "")}
                        disabled={!isCancelEnabled} onClick={onCancelBuildClicked}>Reject Build
                </button>
                <button className={"btn btn-success " + (isLoading ? "loading" : "")} disabled={!isValid}
                        onClick={onCompleteBuildClicked}>Complete Build
                </button>
            </div>
        )
    }

    function handleToggle() {
        const collapsedContent = document.getElementById('collapsedContent');
        if (collapsedContent) {
            collapsedContent.classList.toggle('hidden');
        }
    }

    return (
        <div tabIndex={0} className="collapse collapse-arrow list-item-bg drop-shadow flex flex-col p-3 gap-3">
            <div className="collapse-title" onClick={handleToggle}>
                <div className='flex flex-wrap items-center gap-2 cursor-pointer'>
                    <img src={props.androidBuildRequest.projectSnapshot.logo?.fileUrl!} height={50} width={50}/>
                    <h2 className='custom'>{props.androidBuildRequest.projectSnapshot.title!}</h2>
                </div>
            </div>
            <div className="collapse-content flex flex-col hidden" id="collapsedContent">
                <div className='flex flex-col gap-3'>
                    <span className='custom'><span
                        className='font-bold'>Version: </span>{props.androidBuildRequest.buildConfigs.versionCode + " - " + props.androidBuildRequest.buildConfigs.versionName}</span>
                    <JSONTree data={props.androidBuildRequest} shouldExpandNodeInitially={() => false}/>

                    <div className='flex flex-col gap-2'>
                        {renderUploadKeystoreFile()}
                    </div>
                    {renderGenerationCommands()}

                    <div className='my-4 divider text-slate-400'>COMPLETE BUILD</div>

                    <InputTextComponent topLabel={"Build Message"} hint={"Message"} onChanged={(value) => {
                        androidBuildRequest.displayMessage = value
                        setAndroidBuildRequest({
                            ...androidBuildRequest,
                        });
                    }}/>

                    {renderUploadAppPackageFile()}
                    {renderCompleteAppGen()}
                </div>
            </div>
        </div>
    )
}