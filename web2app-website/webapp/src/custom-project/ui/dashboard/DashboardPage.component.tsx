import {AiFillPlusCircle} from "react-icons/ai";
import Link from "next/link";
import {ProjectDetailsModel} from "../../data/model/project-details.model";
import DashboardProjectListItemComponent from "./shared/DashboardProjectListItem.component";
import {useEffect, useState} from "react";
import {CustomBackendApi} from "../../api/custombackend.api";
import AuthManager from "../../../components/react-shared-module/logic/auth/auth.manager";
import {useAuthState} from "react-firebase-hooks/auth";
import {auth} from "../../../components/react-shared-module/logic/shared-data/firebase.utils";
import {FaRegListAlt} from "react-icons/fa";
import {CustomRoutesUtils} from "../../custom-routes.utils";

export default function DashboardPageComponent(props: {listProjects?: ProjectDetailsModel[]}) {
    const [listProjects, setListProjects] = useState(props.listProjects);
    const [currentUser, isAuthLoading] = useAuthState(auth);

    useEffect(() => {
        if ((!listProjects || listProjects.length === 0) && AuthManager.isUserLogged()) {
            CustomBackendApi.getUserListProjects().then((response) => {
                setListProjects(response);
            });
        }
    }, [currentUser]);



    function onProjectRemoved() {
    }

    function renderListProjects() {
        const renderListProjects = listProjects ? listProjects
            .map((projectDetails) => (<DashboardProjectListItemComponent key={projectDetails.uid!} projectDetails={projectDetails} onRemoved={onProjectRemoved} />)) : [<></>];

        return (
            <div className='flex flex-wrap gap-5 w-full'>
                {renderListProjects}
                {renderCreateProject()}
            </div>
        )
    }

    function renderCreateProject() {
        return (
            <Link href={CustomRoutesUtils.getCreateProjectPageUrl()}
                  className="rounded flex flex-col items-center justify-center bg-slate-300 hover:bg-slate-100 text-slate-700 text-lg gap-3 py-5 px-8 cursor-pointer"
            >
                <AiFillPlusCircle/>
                <span>Create a new project</span>
            </Link>
        )
    }

    return (
        <div className='flex flex-col gap-5 list-item-bg border drop-shadow p-5 mt-10 w-full'>
            <h1 className='custom flex gap-2 items-center'><FaRegListAlt /> My Projects</h1>
            {renderListProjects()}
        </div>
    )
}