import {ProjectDetailsModel} from "../../../data/model/project-details.model";
import Link from "next/link";
import {CustomRoutesUtils} from "../../../custom-routes.utils";

export default function DashboardProjectListItemComponent(props: {projectDetails: ProjectDetailsModel, onRemoved: (projectId: string) => void}) {

    return (
        <Link className='flex flex-row gap-4 items-start list-item-bg drop-shadow border p-5 cursor-pointer hover:bg-slate-100' href={CustomRoutesUtils.getProjectDetailsURL(props.projectDetails.uid!)}>
            <img className='rounded-lg border' src={props.projectDetails.logo?.fileUrl} style={{width: 75, height: 75}} />
            <div className='flex flex-col gap-1'>
                <h2 className='custom w-full text-start'>{props.projectDetails.title}</h2>
                <span className='text-xs text-slate-400 italic w-full text-start'>{props.projectDetails.url}</span>
            </div>
        </Link>
    )
}