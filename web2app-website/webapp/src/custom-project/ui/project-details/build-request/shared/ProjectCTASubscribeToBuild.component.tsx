import {
    ProjectDetailsModel,
    ProjectPlatformTypes,
    ProjectSubscriptionPlan
} from "../../../../data/model/project-details.model";
import SubscriptionPlanButtonComponent from "../../../pricing/shared/SubscriptionPlanButton.component";
import Link from "next/link";
import {CustomRoutesUtils} from "../../../../custom-routes.utils";
import {ProjectDetailsPageMenuOptions} from "../../../../../pages/studio/project/[projectId]";

export default function ProjectCTASubscribeToBuildComponent(props: { projectDetails: ProjectDetailsModel, platformBuildType: ProjectPlatformTypes }) {

    // Check if the user has already a plan to build this type
    if (props.projectDetails.subscription && props.projectDetails.subscription.planType) {
        switch (props.platformBuildType) {
            case ProjectPlatformTypes.ANDROID:
                if (props.projectDetails.subscription.planType === ProjectSubscriptionPlan.ANDROID_ONLY || props.projectDetails.subscription.planType === ProjectSubscriptionPlan.ANDROID_AND_IOS) {
                    return <></>;
                }
                break;
            case ProjectPlatformTypes.IOS:
                if (props.projectDetails.subscription.planType === ProjectSubscriptionPlan.IOS_ONLY || props.projectDetails.subscription.planType === ProjectSubscriptionPlan.ANDROID_AND_IOS) {
                    return <></>;
                }
                break;
        }
    }


    return (
        <div className='flex flex-col items-center'>
            <h2 className='custom text-center'>Subscribe to unlock our App Builder</h2>
            <Link className={"text-sm text-slate-400 text-center underline"} href={CustomRoutesUtils.getProjectDetailsURL(props.projectDetails.uid!, ProjectDetailsPageMenuOptions.SUBSCRIPTION)}>Choose from our flexible subscription plans to build your Mobile App. Check our available subscription plans</Link>
            <div className='flex w-full justify-evenly my-5'>
                {props.platformBuildType === ProjectPlatformTypes.ANDROID ? <SubscriptionPlanButtonComponent projectDetails={props.projectDetails}
                                                                                                             planType={ProjectSubscriptionPlan.ANDROID_ONLY} periodPayment={"yearlyPrice"} extraStyle={"btn-outline"}/> : <></>}
                {props.platformBuildType === ProjectPlatformTypes.IOS ? <SubscriptionPlanButtonComponent projectDetails={props.projectDetails}
                                                                                                             planType={ProjectSubscriptionPlan.IOS_ONLY} periodPayment={"yearlyPrice"} extraStyle={"btn-outline"}/> : <></>}

                <SubscriptionPlanButtonComponent projectDetails={props.projectDetails}
                                                 planType={ProjectSubscriptionPlan.ANDROID_AND_IOS} periodPayment={"yearlyPrice"}/>
            </div>
        </div>
    )

}