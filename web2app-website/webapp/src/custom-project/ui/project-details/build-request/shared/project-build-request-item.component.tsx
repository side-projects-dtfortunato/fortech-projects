import {ProjectBuildStatusType} from "../../../../data/model/project-details.model";
import {
    StorageFileModel
} from "../../../../../components/react-shared-module/logic/shared-data/datamodel/storage-file.model";
import {RxCrossCircled} from "react-icons/rx";
import {MdErrorOutline} from "react-icons/md";
import Link from "next/link";
import {FaAndroid, FaCheckCircle, FaCloudDownloadAlt} from "react-icons/fa";
import {
    StripePaymentStatus
} from "../../../../../components/react-shared-module/logic/shared-data/datamodel/custom-stripe.model";
import Linkify from "react-linkify";

export default function ProjectBuildRequestItemComponent(props: {
    versionName: string,
    versionCode: number,
    status: ProjectBuildStatusType,
    buildFile?: StorageFileModel,
    message?: string,
    childButtons?: any,
}) {

    let bgColor: string;
    let textColor: string;
    let icon: any;
    let statusLabel: string;

    switch (props.status) {
        case ProjectBuildStatusType.BUILD_FAILED:
            bgColor = "bg-red-300";
            textColor = "text-slate-700";
            icon = <MdErrorOutline/>
            statusLabel = "Build Failed!";
            break;

        case ProjectBuildStatusType.IN_PROGRESS:
            bgColor = "bg-amber-300";
            textColor = "text-amber-700";
            icon = <span className="loading loading-spinner loading-lg"></span>;
            statusLabel = "Your app is being generated...";
            break;
        case ProjectBuildStatusType.BUILD_COMPLETED:
            bgColor = "bg-green-300";
            textColor = "text-green-700";
            icon = <div className='text-xl'><FaCheckCircle/></div>;
            statusLabel = "Your app was built successfully.";
            break;
        case ProjectBuildStatusType.BUILD_CANCELLED:
        default:
            bgColor = "bg-slate-300";
            textColor = "text-slate-700";
            icon = <RxCrossCircled/>
            statusLabel = "Your built request was cancelled.";
            break;
    }

    function renderMessage() {
        if (props.message) {
            return (
                <Linkify>
                    <div className='flex flex-col text-slate-600 gap-2 bg-white bg-opacity-50 rounded-xl p-5'>
                        <span className='text-slate-700 text-xl font-bold'>Next Steps:</span>
                        <span className='italic'>{props.message}</span>
                    </div>
                </Linkify>
            )
        } else {
            return (<></>);
        }
    }

    function renderDownloadFile() {
        if (props.buildFile) {
            return (
                <Link href={props.buildFile.fileUrl} target={"_blank"} className='btn btn-success gap-2'>
                    <FaCloudDownloadAlt/> Download App Package
                </Link>
            )
        } else {
            return (<></>);
        }
    }


    return (
        <div className={"p-5 rounded-lg flex flex-col items-center gap-5 " + bgColor + " " + textColor}>
            {icon}
            <h2 className='custom'>{statusLabel}</h2>
            <div className='flex flex-wrap gap-2'>
                <h4 className='custom gap-2'>Version Name: {props.versionName}</h4>
                <h4 className='custom gap-2'>- Version Code: {props.versionCode}</h4>
            </div>
            {renderMessage()}
            {renderDownloadFile()}
            {props.childButtons}
        </div>
    )
}