import {
    ProjectDetailsModel,
    ProjectDetailsUtils,
    ProjectPlatformTypes
} from "../../../../data/model/project-details.model";
import ProjectBuildRequestItemComponent from "../shared/project-build-request-item.component";
import {useEffect, useState} from "react";
import {CustomBackendApi} from "../../../../api/custombackend.api";
import InputTextComponent from "../../../../../components/react-shared-module/ui-components/form/InputText.component";
import {
    RootGlobalStateActions,
    useAlertMessageGlobalState
} from "../../../../../components/react-shared-module/logic/global-hooks/root-global-state";
import {
    StripePaymentStatus
} from "../../../../../components/react-shared-module/logic/shared-data/datamodel/custom-stripe.model";
import {MdOutlinePayment} from "react-icons/md";
import Link from "next/link";
import ProjectCTASubscribeToBuildComponent from "../shared/ProjectCTASubscribeToBuild.component";

export default function ProjectAndroidBuildComponent(props: {projectDetails: ProjectDetailsModel, onProjectUpdated: (projectDetails: ProjectDetailsModel) => void}) {
    const inProgressBuildRequest = props.projectDetails.inProgressBuilds?.ANDROID;
    const lastBuildRequest = props.projectDetails.lastBuilds?.ANDROID;
    const [isLoading, setIsLoading] = useState(false);
    const [alertMessage, setAlertMessage] = useAlertMessageGlobalState();


    // Init Version
    if (!props.projectDetails.androidConfigs.versionName) {
        props.projectDetails.androidConfigs.versionName = "1.0.0";
    }
    if (!props.projectDetails.androidConfigs.versionCode) {
        props.projectDetails.androidConfigs.versionCode = 0;
    }

    function onCancelBuildRequest() {
        setIsLoading(true);
        // API request to cancel build request
        CustomBackendApi.web2appProjectCancelBuild({
            projectId: props.projectDetails.uid!,
            platformType: ProjectPlatformTypes.ANDROID,
        }).then((apiResponse) => {
            setIsLoading(false);
            if (apiResponse.isSuccess) {
                props.onProjectUpdated(apiResponse.responseData as ProjectDetailsModel);
            } else {
                RootGlobalStateActions.displayAPIResponseError({
                    apiResponse: apiResponse,
                    setAlertMessageState: setAlertMessage,
                });
            }

        }).catch((err) => {
            setIsLoading(false);
            RootGlobalStateActions.displayAlertMessage({
                alertType: "ERROR",
                message: err.message,
                setAlertMessageState: setAlertMessage,
            });
        });
    }

    async function onRequestBuild() {
        setIsLoading(true);
        props.projectDetails.androidConfigs.isEditable = false;
        props.onProjectUpdated(props.projectDetails);

        // Firstly publish changes
        if (!props.projectDetails.hasChangesPublished) {
            await CustomBackendApi.projectPublishChanges({projectId: props.projectDetails.uid!});
        }

        // API Request
        CustomBackendApi.web2appProjectAndroidBuildRequest({
            projectId: props.projectDetails.uid!,
            androidConfigs: props.projectDetails.androidConfigs,
        }).then((apiResponse) => {
            setIsLoading(false);
            if (apiResponse.isSuccess) {
                const updatedProjectDetails: ProjectDetailsModel = apiResponse.responseData as ProjectDetailsModel;
                props.onProjectUpdated(updatedProjectDetails);
                RootGlobalStateActions.displayAlertMessage({
                    alertType: "SUCCESS",
                    message: "You create a build request successfully...",
                    setAlertMessageState: setAlertMessage,
                });
            } else {
                RootGlobalStateActions.displayAPIResponseError({
                    apiResponse: apiResponse,
                    setAlertMessageState: setAlertMessage,
                });
            }

        }).catch((err) => {
            setIsLoading(false);
            RootGlobalStateActions.displayAlertMessage({
                alertType: "ERROR",
                message: err.message,
                setAlertMessageState: setAlertMessage,
            });
        });
    }

    function renderLastBuild() {
        if (lastBuildRequest) {
            return (
                <ProjectBuildRequestItemComponent versionName={lastBuildRequest.buildConfigs.versionName!} versionCode={lastBuildRequest.buildConfigs.versionCode!} status={lastBuildRequest.status} message={lastBuildRequest.displayMessage} buildFile={lastBuildRequest.buildReleaseFile} />
            )
        } else {
            return (<></>);
        }
    }

    function renderInProgressBuild() {

        let inProgressBtns: any = <></>;
        if (ProjectDetailsUtils.isInProgressAndroidBuild(props.projectDetails)) {
            inProgressBtns = (
                <div className='flex flex-wrap gap-2'>
                    <button className={'btn btn-sm btn-outline btn-error ' + (isLoading ? "loading" : "")} disabled={isLoading} onClick={onCancelBuildRequest}>Cancel Build Request</button>
                </div>
            )
        }

        if (inProgressBuildRequest) {
            return (
                <ProjectBuildRequestItemComponent childButtons={inProgressBtns}
                                                  versionName={inProgressBuildRequest.buildConfigs.versionName!}
                                                  versionCode={inProgressBuildRequest.buildConfigs.versionCode!}
                                                  status={inProgressBuildRequest.status} message={inProgressBuildRequest.displayMessage} />
            )
        } else {
            return (<></>);
        }
    }

    function renderVersionFields() {
        if (!props.projectDetails.inProgressBuilds?.ANDROID) {
            return (
                <div className='flex flex-wrap gap-4 justify-center items-end'>
                    <div className='w-12'>
                        <InputTextComponent topLabel={"Code"} hint={"Unique Version Number"} disabled={inProgressBuildRequest !== undefined} defaultValue={String(props.projectDetails.androidConfigs.versionCode! + 1)} onChanged={(value) => props.projectDetails.androidConfigs.versionCode = Number(value)} />
                    </div>
                    <div className='w-48'>
                        <InputTextComponent disabled={inProgressBuildRequest !== undefined} topLabel={"Version Name"} hint={"Version Name"} defaultValue={props.projectDetails.androidConfigs.versionName!} onChanged={(value) => props.projectDetails.androidConfigs.versionName = value} />
                    </div>
                    <div className='flex flex-col gap-1'>
                        <Link href={"https://webtoapp.app/pricing"} className={"text-xs text-slate-400"} target={"_blank"}>Pricing</Link>
                        <button className={'btn btn-success ' + (isLoading ? "loading" : "")} disabled={isLoading || !ProjectDetailsUtils.isProjectAndroidConfigsValid(props.projectDetails)} onClick={onRequestBuild}>🚀 Make My App</button>
                    </div>
                </div>
            )
        } else {
            return <></>
        }

    }


    // First Check if the project is subscribed
    if (ProjectDetailsUtils.isProjectWithSubscription(props.projectDetails, ProjectPlatformTypes.ANDROID)) {
        return (
            <div className='flex flex-col gap-3'>
                {renderLastBuild()}
                {renderInProgressBuild()}
                {renderVersionFields()}
            </div>
        )
    } else {
        return (
            <div className='flex w-full justify-center'>
                <ProjectCTASubscribeToBuildComponent projectDetails={props.projectDetails} platformBuildType={ProjectPlatformTypes.ANDROID}/>
            </div>
        )
    }

}