import {
    ProjectBuildStatusType,
    ProjectDetailsModel,
    ProjectDetailsUtils,
    ProjectPlatformTypes
} from "../../../../data/model/project-details.model";
import ProjectBuildRequestItemComponent from "../shared/project-build-request-item.component";
import {useEffect, useState} from "react";
import {CustomBackendApi} from "../../../../api/custombackend.api";
import InputTextComponent from "../../../../../components/react-shared-module/ui-components/form/InputText.component";
import {
    RootGlobalStateActions,
    useAlertMessageGlobalState
} from "../../../../../components/react-shared-module/logic/global-hooks/root-global-state";
import {
    StripePaymentStatus
} from "../../../../../components/react-shared-module/logic/shared-data/datamodel/custom-stripe.model";
import {MdOutlinePayment} from "react-icons/md";
import Link from "next/link";
import ProjectCTASubscribeToBuildComponent from "../shared/ProjectCTASubscribeToBuild.component";

export default function ProjectiOSBuildComponent(props: {projectDetails: ProjectDetailsModel, onProjectUpdated: (projectDetails: ProjectDetailsModel) => void}) {
    const inProgressBuildRequest = props.projectDetails.inProgressBuilds?.IOS;
    const lastBuildRequest = props.projectDetails.lastBuilds?.IOS;
    const [isLoading, setIsLoading] = useState(false);
    const [alertMessage, setAlertMessage] = useAlertMessageGlobalState();

    // Init Version
    if (!props.projectDetails.iosConfigs.versionName) {
        props.projectDetails.iosConfigs.versionName = "1.0.0";
    }
    if (!props.projectDetails.iosConfigs.versionCode) {
        props.projectDetails.iosConfigs.versionCode = 0;
    }

    function onCancelBuildRequest() {
        setIsLoading(true);
        // API request to cancel build request
        CustomBackendApi.web2appProjectCancelBuild({
            projectId: props.projectDetails.uid!,
            platformType: ProjectPlatformTypes.IOS,
        }).then((apiResponse) => {
            setIsLoading(false);
            if (apiResponse.isSuccess) {
                props.onProjectUpdated(apiResponse.responseData as ProjectDetailsModel);
            } else {
                RootGlobalStateActions.displayAPIResponseError({
                    apiResponse: apiResponse,
                    setAlertMessageState: setAlertMessage,
                });
            }

        }).catch((err) => {
            setIsLoading(false);
            RootGlobalStateActions.displayAlertMessage({
                alertType: "ERROR",
                message: err.message,
                setAlertMessageState: setAlertMessage,
            });
        });
    }

    async function onRequestBuild() {
        setIsLoading(true);
        props.projectDetails.iosConfigs.isEditable = false;
        props.onProjectUpdated(props.projectDetails);


        // Firstly publish changes
        if (!props.projectDetails.hasChangesPublished) {
            await CustomBackendApi.projectPublishChanges({projectId: props.projectDetails.uid!});
        }

        // API Request
        CustomBackendApi.web2appProjectIOSBuildRequest({
            projectId: props.projectDetails.uid!,
            iosConfigs: props.projectDetails.iosConfigs,
        }).then((apiResponse) => {
            setIsLoading(false);
            if (apiResponse.isSuccess) {
                const updatedProjectDetails: ProjectDetailsModel = apiResponse.responseData as ProjectDetailsModel;
                props.onProjectUpdated(updatedProjectDetails);
                RootGlobalStateActions.displayAlertMessage({
                    alertType: "SUCCESS",
                    message: "You create a build request successfully...",
                    setAlertMessageState: setAlertMessage,
                });

            } else {
                RootGlobalStateActions.displayAPIResponseError({
                    apiResponse: apiResponse,
                    setAlertMessageState: setAlertMessage,
                });
            }

        }).catch((err) => {
            setIsLoading(false);
            RootGlobalStateActions.displayAlertMessage({
                alertType: "ERROR",
                message: err.message,
                setAlertMessageState: setAlertMessage,
            });
        });
    }

    function renderLastBuild() {
        if (lastBuildRequest) {
            return (
                <ProjectBuildRequestItemComponent versionName={lastBuildRequest.buildConfigs.versionName!} versionCode={lastBuildRequest.buildConfigs.versionCode!} status={lastBuildRequest.status} message={lastBuildRequest.displayMessage} buildFile={lastBuildRequest.buildReleaseFile} />
            )
        } else {
            return (<></>);
        }
    }

    function renderInProgressBuild() {
        let inProgressBtns: any = <></>;
        if (ProjectDetailsUtils.isInProgressIOSBuild(props.projectDetails)) {
            inProgressBtns = (
                <div className='flex flex-wrap gap-2 w-full justify-center'>
                    <button className={'btn btn-sm btn-outline btn-error ' + (isLoading ? "loading" : "")} disabled={isLoading} onClick={onCancelBuildRequest}>Cancel Build Request</button>
                </div>
            )
        }

        if (inProgressBuildRequest) {
            return (
                <ProjectBuildRequestItemComponent childButtons={inProgressBtns} versionName={inProgressBuildRequest.buildConfigs.versionName!} versionCode={inProgressBuildRequest.buildConfigs.versionCode!} status={inProgressBuildRequest.status} message={inProgressBuildRequest.displayMessage} />
            )
        } else {
            return (<></>);
        }
    }

    function renderVersionFields() {
        if (!props.projectDetails.inProgressBuilds?.IOS) {
            return (
                <div className='flex flex-wrap gap-4 justify-center items-end'>
                    <div className='w-14'>
                        <InputTextComponent topLabel={"Code"} hint={"Unique Version Number"} disabled={inProgressBuildRequest !== undefined} defaultValue={String(props.projectDetails.iosConfigs.versionCode! + 1)} onChanged={(value) => props.projectDetails.iosConfigs.versionCode = Number(value)} />
                    </div>
                    <div className='w-48'>
                        <InputTextComponent disabled={inProgressBuildRequest !== undefined} topLabel={"Version Name"} hint={"Version Name"} defaultValue={props.projectDetails.iosConfigs.versionName!} onChanged={(value) => props.projectDetails.iosConfigs.versionName = value} />
                    </div>
                    <div className='flex flex-col gap-1'>
                        <button className={'btn btn-success ' + (isLoading ? "loading" : "")} disabled={isLoading || !ProjectDetailsUtils.isProjectIOSConfigsValid(props.projectDetails)} onClick={onRequestBuild}>🚀 Make My App</button>
                    </div>

                </div>
            )
        } else {
            return <></>
        }

    }

    // First Check if the project is subscribed
    if (ProjectDetailsUtils.isProjectWithSubscription(props.projectDetails, ProjectPlatformTypes.IOS)) {
        return (
            <div className='flex flex-col gap-3'>
                {renderLastBuild()}
                {renderInProgressBuild()}
                {renderVersionFields()}
            </div>
        )
    } else {
        return (
            <div className='flex w-full justify-center'>
                <ProjectCTASubscribeToBuildComponent projectDetails={props.projectDetails} platformBuildType={ProjectPlatformTypes.IOS}/>
            </div>
        )
    }
}