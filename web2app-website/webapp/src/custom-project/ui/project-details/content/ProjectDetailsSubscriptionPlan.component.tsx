import {ProjectDetailsModel, ProjectSubscriptionPlan} from "../../../data/model/project-details.model";
import {useState} from "react";
import {CustomBackendApi} from "../../../api/custombackend.api";
import RootConfigs from "../../../../configs";
import CommonUtils from "../../../../components/react-shared-module/logic/commonutils";
import PricingPlansTableComponent from "../../pricing/shared/PricingPlansTable.component";
import PageHeaderTitleComponent
    from "../../../../components/react-shared-module/ui-components/shared/PageHeaderTitle.component";
import {MdOutlinePayments} from "react-icons/md";
import PricingShortenWebToAppComponent from "../../pricing/shared/PricingShortenWebToApp.component";


export default function ProjectDetailsSubscriptionPlanComponent(props: { projectDetails: ProjectDetailsModel, onProjectUpdated: (projectDetails: ProjectDetailsModel) => void }) {
    const [isLoading, setIsLoading] = useState(false);

    function onCustomerPortalClicked() {
        setIsLoading(true);
        CustomBackendApi.stripeCreateCustomerPortalSession({
            env: RootConfigs.ENVIRONMENT_DEV ? "DEV" : "PROD",
            projectId: props.projectDetails.uid!,
        }).then((res) => {
            setIsLoading(false);
            if (res.isSuccess && res.responseData?.redirectUrl) {
                CommonUtils.openExternalUrl(res.responseData?.redirectUrl);
            }
        });
    }

    return (
        <div className='flex flex-col gap-4'>
            <PageHeaderTitleComponent title={"Pricing to build an Android or/and iOS App"} subtitle={"You can build the app for the stores that you prefer separately."}/>
            <PricingShortenWebToAppComponent projectDetails={props.projectDetails} hideTitle={true} />
            <div className='divider text-slate-400 my-5'>MANAGE YOUR SUBSCRIPTION</div>
            <div className='flex flex-row justify-center'>
                <button className={'btn btn-outline '  + (isLoading ? "loading" : "")} disabled={isLoading} onClick={onCustomerPortalClicked}><MdOutlinePayments/> MANAGE BILLING SETTINGS</button>
            </div>
        </div>
    )
}