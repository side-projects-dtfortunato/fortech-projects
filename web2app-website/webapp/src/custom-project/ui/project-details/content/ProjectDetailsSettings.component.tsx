import {ProjectDetailsModel} from "../../../data/model/project-details.model";
import InputTextComponent from "../../../../components/react-shared-module/ui-components/form/InputText.component";
import CommonUtils from "../../../../components/react-shared-module/logic/commonutils";
import {IoSettings} from "react-icons/io5";
import Link from "next/link";
import {CustomRoutesUtils} from "../../../custom-routes.utils";

export default function ProjectDetailsSettingsComponent(props: { projectDetails: ProjectDetailsModel, onProjectUpdated: (projectDetails: ProjectDetailsModel) => void }) {

    function onEmailChanged(email: string) {
        if (!CommonUtils.isValidEmail(email)) {
            return;
        }

        if (!props.projectDetails.settings) {
            props.projectDetails.settings = {
                contactEmail: "",
                receiveEmailAlerts: true,
            };
        }
        props.projectDetails.settings.contactEmail = email;

        props.onProjectUpdated({
            ...props.projectDetails,
        });
    }

    function onReceiveEmailsToggle() {
        if (!props.projectDetails.settings) {
            props.projectDetails.settings = {
                contactEmail: "",
                receiveEmailAlerts: true,
            };
        }
        props.projectDetails.settings.receiveEmailAlerts = !props.projectDetails.settings.receiveEmailAlerts;

        props.onProjectUpdated({
            ...props.projectDetails,
        });
    }

    function renderAskFilesPermissions() {
        return (
            <div className="form-control justify-start">
                <label className="label cursor-pointer form-control flex flex-row justify-start gap-5">
                    <input type="checkbox" className="checkbox" checked={props.projectDetails.appConfigs?.askFilesPermission === true} onClick={() => {
                        if (!props.projectDetails.appConfigs) {
                            props.projectDetails.appConfigs = {};
                        }
                        props.projectDetails.appConfigs.askFilesPermission = !(props.projectDetails.appConfigs.askFilesPermission === true);
                        props.onProjectUpdated(props.projectDetails);
                    }
                    }/>
                    <span className="label-text font-bold text-lg">Ask for Files Permissions at launch of the app?</span>
                </label>
            </div>
        )
    }

    function renderHasOAuthSignin() {
        return (
            <div className="form-control justify-start">
                <label className="label cursor-pointer form-control flex flex-row justify-start gap-5">
                    <input type="checkbox" className="checkbox" checked={props.projectDetails.appConfigs?.hasOAuthSignin === true} onClick={() => {
                        if (!props.projectDetails.appConfigs) {
                            props.projectDetails.appConfigs = {};
                        }
                        props.projectDetails.appConfigs.hasOAuthSignin = !(props.projectDetails.appConfigs.hasOAuthSignin === true);
                        props.onProjectUpdated(props.projectDetails);
                    }
                    }/>
                    <span className="label-text font-bold text-lg">The App have OAuth Authentication (Google Signin, Facebook Auth, etc...)?</span>
                </label>
            </div>
        )
    }

    function renderCheckbox() {
        return (
            <div className="form-control justify-start">
                <label className="label cursor-pointer form-control flex flex-row justify-start gap-5">
                    <input type="checkbox" checked={props.projectDetails.settings?.receiveEmailAlerts ? true : false}
                           className="checkbox" onClick={onReceiveEmailsToggle}/>
                    <span className="label-text font-bold text-lg">Want to receive emails from us?</span>
                </label>
            </div>
        )
    }

    return (
        <div className='flex flex-col gap-4'>
            <h1 className={"flex gap-2 custom items-center w-full text-center justify-center"}>
                <IoSettings/>Project Settings</h1>

            <div className='divider text-slate-400'>App Configs</div>
            {renderAskFilesPermissions()}
            {renderHasOAuthSignin()}

            <div className='divider text-slate-400'>Account Configs</div>
            <InputTextComponent topLabel={"Email"} hint={"Email to receive our emails notifications"}
                                defaultValue={props.projectDetails.settings?.contactEmail}
                                inputType={"email"} onChanged={onEmailChanged}/>
            {renderCheckbox()}
            <div className='flex w-full justify-center'>
                <Link href={CustomRoutesUtils.getDeleteProjectUrl(props.projectDetails.uid!)} className='btn btn-ghost text-red-500'>Delete project</Link>
            </div>
        </div>
    )
}