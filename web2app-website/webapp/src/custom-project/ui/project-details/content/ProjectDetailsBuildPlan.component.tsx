import {useState} from "react";
import {CustomBackendApi} from "../../../api/custombackend.api";
import RootConfigs from "../../../../configs";
import CommonUtils from "../../../../components/react-shared-module/logic/commonutils";
import PageHeaderTitleComponent
    from "../../../../components/react-shared-module/ui-components/shared/PageHeaderTitle.component";
import PricingShortenWebToAppComponent from "../../pricing/shared/PricingShortenWebToApp.component";
import {ProjectDetailsModel} from "../../../data/model/project-details.model";

export default function ProjectDetailsBuildPlanComponent(props: { projectDetails: ProjectDetailsModel, onProjectUpdated: (projectDetails: ProjectDetailsModel) => void }) {
    const [isLoading, setIsLoading] = useState(false);

    function onCustomerPortalClicked() {
        setIsLoading(true);
        CustomBackendApi.stripeCreateCustomerPortalSession({
            env: RootConfigs.ENVIRONMENT_DEV ? "DEV" : "PROD",
            projectId: props.projectDetails.uid!,
        }).then((res) => {
            setIsLoading(false);
            if (res.isSuccess && res.responseData?.redirectUrl) {
                CommonUtils.openExternalUrl(res.responseData?.redirectUrl);
            }
        });
    }

    return (
        <div className='flex flex-col gap-4'>
            <PageHeaderTitleComponent title={"Start the build of your App"} subtitle={"Choose the Mobile platform that you want to publish your app"}/>
            <PricingShortenWebToAppComponent projectDetails={props.projectDetails} hideTitle={true} />
        </div>
    )
}