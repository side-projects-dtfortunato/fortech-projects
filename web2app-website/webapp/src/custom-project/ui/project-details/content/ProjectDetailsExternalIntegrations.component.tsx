import {ProjectDetailsModel} from "../../../data/model/project-details.model";
import {getLanguageLabel} from "../../../../components/react-shared-module/logic/language/language.helper";
import {PiPlugsConnectedBold} from "react-icons/pi";
import Image from "next/image";
import Link from "next/link";
import InputTextComponent from "../../../../components/react-shared-module/ui-components/form/InputText.component";

export default function ProjectDetailsExternalIntegrationsComponent(props: { projectData: ProjectDetailsModel, onProjectUpdated: (projectData: ProjectDetailsModel) => void }) {

    function onOneSignalAppIdChanged(value: string) {
        if (!props.projectData.externalIntegrations) {
            props.projectData.externalIntegrations = {};
        }
        props.projectData.externalIntegrations.oneSignalAppId = value;
        
        props.onProjectUpdated(props.projectData);
    }
    
    function renderOneSignalField() {
        return (
            <div className='flex flex-col gap-2'>
                <Link href={"https://onesignal.com/"} target={"_blank"}>
                    <Image src={"/images/ic_onesignal_logo.png"} alt={"OneSignal Logo"} width={150} height={100}/></Link>
                <InputTextComponent topLabel={"OneSignal AppId"} hint={"Set here your app id. E.g: xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx"} onChanged={onOneSignalAppIdChanged} defaultValue={props.projectData.externalIntegrations?.oneSignalAppId}/>
                <Link href={"https://onesignal.com/"} target={"_blank"} className='text-slate-400 text-sm underline italic'>Setup a OneSignal account to let your mobile app receive push notifications</Link>
            </div>
        )
    }

    return (
        <div className='flex flex-col gap-5'>
            <h1 className={"flex gap-2 custom items-center w-full text-center justify-center"}>
                <PiPlugsConnectedBold/>{getLanguageLabel("WEB2APP_PAGE_EXTERNAL_INTEGRATIONS")}</h1>
            {renderOneSignalField()}
        </div>
    )
}