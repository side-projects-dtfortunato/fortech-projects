import {ProjectDetailsModel, ProjectDetailsUtils} from "../../../data/model/project-details.model";
import InputTextComponent from "../../../../components/react-shared-module/ui-components/form/InputText.component";
import {CustomUtils} from "../../../data/custom.utils";
import {FcAndroidOs} from "react-icons/fc";
import {getLanguageLabel} from "../../../../components/react-shared-module/logic/language/language.helper";
import ProjectAndroidBuildComponent from "../build-request/android-build/project-android-build.component";
import Link from "next/link";
import {FaCloudDownloadAlt} from "react-icons/fa";
import {useState} from "react";
import CustomUploadFileComponent
    from "../../../../components/react-shared-module/ui-components/shared/CustomUploadFile.component";

export default function ProjectDetailsAndroidBuildComponent(props: { projectData: ProjectDetailsModel, onProjectUpdated: (projectData: ProjectDetailsModel) => void }) {
    let isDisabled: boolean = (props.projectData.androidConfigs && props.projectData.androidConfigs.isEditable === false);
    const [isLoading, setIsLoading] = useState(false);

    if (!props.projectData.androidConfigs) {
        props.projectData.androidConfigs = {
            keyStorePassword: "",
            keyStoreAlias: "",
            appId: "",
        };
    }

    function onAppId(appId: string) {
        props.projectData.androidConfigs.appId = CustomUtils.convertToAndroidAppId(appId);
        props.onProjectUpdated(props.projectData);
    }

    function onKeystoreAlias(keystoreAlias: string) {
        props.projectData.androidConfigs.keyStoreAlias = keystoreAlias;
        props.onProjectUpdated(props.projectData);
    }

    function onKeystorePassword(keystorePassword: string) {
        props.projectData.androidConfigs.keyStorePassword = keystorePassword;
        props.onProjectUpdated(props.projectData);
    }

    async function handleFileUpload(uploadFile: File) {
        setIsLoading(true);
        const projectData: ProjectDetailsModel = await ProjectDetailsUtils.uploadKeystoreFile(props.projectData, uploadFile);
        props.projectData.androidConfigs.keyStore = projectData.androidConfigs.keyStore;
        props.onProjectUpdated(props.projectData);
        setIsLoading(false);
    }

    function renderUploadKeystoreFile() {
        if (!props.projectData.androidConfigs.isEditable && props.projectData.androidConfigs.keyStore && props.projectData.androidConfigs.keyStore.fileUrl.startsWith("http")) {
            return (
                <div className='flex flex-col items-start gap-1'>
                    <h3 className='custom'>Keystore File:</h3>
                    <Link href={props.projectData.androidConfigs.keyStore.fileUrl} target={"_blank"}
                          className='btn btn-primary btn-outline gap-1'> <FaCloudDownloadAlt/> Download
                        Keystore</Link>
                    <CustomUploadFileComponent onFileSelected={handleFileUpload}>
                        <div className={"btn btn-ghost btn-xs max-w-xs mt-1 " + (isLoading ? "loading" : "")}>Re-upload
                            Certificate
                        </div>
                    </CustomUploadFileComponent>
                </div>);
        } else {
            return (
                <>
                    <div className='flex flex-col'>
                        <h3 className='custom'>Keystore Upload (.jks file):</h3>
                        <span className="text-xs italic text-slate-400">If empty, we will generate a keystore to generate the app</span>
                    </div>
                    <div className='flex flex-col'>
                        <h3 className='custom'>Keystore:</h3>
                        {isLoading ? <progress className="progress w-56"></progress> : <></>}
                        <input type="file" accept="\.jks$" className="file-input w-full max-w-xs"
                               disabled={isDisabled || isLoading}
                               onInput={(event: any) => handleFileUpload(event.target.files[0])}/>
                    </div>
                </>
            );
        }
    }


    return (
        <div className='flex flex-col gap-4'>
            <h1 className={"flex gap-2 custom items-center w-full text-center justify-center"}>
                <FcAndroidOs/>{getLanguageLabel("WEB2APP_PAGE_ANDROID_BUILD")}</h1>

            <InputTextComponent disabled={isDisabled} topLabel={"App Id"} hint={"ex: app.webtoapp.MYAPPNAME"}
                                onChanged={onAppId} inputType={"text"}
                                value={props.projectData.androidConfigs.appId ? props.projectData.androidConfigs.appId : ""}/>
            <InputTextComponent disabled={isDisabled} topLabel={"Keystore Alias"} hint={"Alias of the keystore"}
                                onChanged={onKeystoreAlias} inputType={"text"}
                                value={props.projectData.androidConfigs.keyStoreAlias ? props.projectData.androidConfigs.keyStoreAlias : ""}/>
            <InputTextComponent disabled={isDisabled} topLabel={"Keystore Password"} hint={"Password of the keystore"}
                                onChanged={onKeystorePassword} inputType={"password"}
                                value={props.projectData.androidConfigs.keyStorePassword ? props.projectData.androidConfigs.keyStorePassword : ""}/>
            {renderUploadKeystoreFile()}

            <div className='divider text-slate-400'>APP BUILDS</div>
            <ProjectAndroidBuildComponent projectDetails={props.projectData} onProjectUpdated={props.onProjectUpdated}/>
        </div>
    )
}