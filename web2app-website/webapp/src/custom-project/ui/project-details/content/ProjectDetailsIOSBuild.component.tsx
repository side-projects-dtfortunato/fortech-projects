import {ProjectDetailsModel, ProjectDetailsUtils} from "../../../data/model/project-details.model";
import InputTextComponent from "../../../../components/react-shared-module/ui-components/form/InputText.component";
import {CustomUtils} from "../../../data/custom.utils";
import {FaAppStoreIos, FaCloudDownloadAlt} from "react-icons/fa";
import {useEffect, useState} from "react";
import {getLanguageLabel} from "../../../../components/react-shared-module/logic/language/language.helper";
import Link from "next/link";
import ProjectiOSBuildComponent from "../build-request/ios-build/project-ios-build.component";
import CustomUploadFileComponent
    from "../../../../components/react-shared-module/ui-components/shared/CustomUploadFile.component";
import {CustomRoutesUtils} from "../../../custom-routes.utils";
import {
    LottieMapIconsUtils
} from "../../../../components/react-shared-module/ui-components/lottie/lottie-map-icons.utils";
import Lottie from "lottie-react";


export default function ProjectDetailsIOSBuildComponent(props: { projectData: ProjectDetailsModel, onProjectUpdated: (projectData: ProjectDetailsModel) => void }) {
    let isDisabled: boolean = props.projectData.iosConfigs.isEditable === false;
    const [isLoading, setIsLoading] = useState(false);

    useEffect(() => {
        return () => {
            if (!props.projectData.iosConfigs.appId || props.projectData.iosConfigs.appId.length === 0) {
                props.projectData.iosConfigs.appId = props.projectData.androidConfigs.appId;
                props.onProjectUpdated(props.projectData);
            }
        };
    }, []);


    function onAppId(appId: string) {
        props.projectData.iosConfigs.appId = CustomUtils.convertToAndroidAppId(appId);
        props.onProjectUpdated(props.projectData);
    }

    async function handleProvisionProfileCertificateUpload(uploadFile: File) {
        setIsLoading(true);
        const projectData: ProjectDetailsModel = await ProjectDetailsUtils.uploadProvisioningProfileCertificate(props.projectData, uploadFile);
        props.projectData.iosConfigs.provisioningProfileCertificate = projectData.iosConfigs.provisioningProfileCertificate;
        props.onProjectUpdated(props.projectData);
        setIsLoading(false);
    }

    async function handleDistributionCertificateUpload(uploadFile: File) {
        setIsLoading(true);
        const projectData: ProjectDetailsModel = await ProjectDetailsUtils.updateiOSDistributionCert(props.projectData, uploadFile);
        props.projectData.iosConfigs.distributionCertificateFile = projectData.iosConfigs.distributionCertificateFile;
        props.onProjectUpdated(props.projectData);
        setIsLoading(false);
    }

    function renderProvisionProfileCertFile() {
        if (!props.projectData.iosConfigs.isEditable && props.projectData.iosConfigs.provisioningProfileCertificate && props.projectData.iosConfigs.provisioningProfileCertificate.fileUrl.startsWith("http")) {
            return (
                <div className='flex flex-col items-start gap-1'>
                    <h3 className='custom'>Provisioning Profile Certificate:</h3>
                    <Link href={props.projectData.iosConfigs.provisioningProfileCertificate.fileUrl} target={"_blank"}
                          className='btn btn-primary btn-outline gap-1'> <FaCloudDownloadAlt/> Download Certificate</Link>
                    <CustomUploadFileComponent onFileSelected={handleProvisionProfileCertificateUpload}>
                        <div className={"btn btn-ghost btn-xs max-w-xs mt-1 " + (isLoading ? "loading" : "")}>Re-upload Certificate</div>
                    </CustomUploadFileComponent>

                </div>);
        } else {
            return (
                <>
                    <div className='flex flex-col'>
                        <h3 className='custom'>Provisioning Profile Certificate:</h3>
                        {isLoading ? <progress className="progress w-56"></progress> : <></>}
                        <input type="file" className="file-input w-full max-w-xs mt-1" disabled={isDisabled || isLoading}
                               onInput={(event: any) => handleProvisionProfileCertificateUpload(event.target.files[0])}/>
                        <Link href={CustomRoutesUtils.getWikiDociOSProvisioningCertificate()} className="text-xs italic underline text-slate-400" target={"_blank"}>How to generate a Provisioning Profile Certificate</Link>

                    </div>
                </>
            );
        }
    }

    function renderStepByStepGuideLink() {
        if (props.projectData.lastBuilds?.IOS) {
            return <></>;
        }
        return (
            <Link href={CustomRoutesUtils.getWikiDocsiOSBuildGuide()} className='w-full list-item-bg drop-shadow p-4 flex flex-row justify-start items-center gap-4 border bg-slate-50 hover:bg-slate-200' target={"_blank"}>
                <div className={"w-16 h-16"}>
                    <Lottie animationData={LottieMapIconsUtils.lottieWikiGuideIcon} loop={true} width={10} height={10}  />
                </div>
                <span className={"text-lg font-bold text-start"} >Follow this Step-by-Step guide before you build your iOS app.</span>
            </Link>
        )
    }
    
    function renderDistributionCertificateFile() {
        if (!props.projectData.iosConfigs.isEditable && props.projectData.iosConfigs.distributionCertificateFile && props.projectData.iosConfigs.distributionCertificateFile.fileUrl.startsWith("http")) {
            return (
                <div className='flex flex-col items-start gap-1'>
                    <h3 className='custom'>Distribution Certificate:</h3>
                    <Link href={props.projectData.iosConfigs.distributionCertificateFile.fileUrl} target={"_blank"}
                          className='btn btn-primary btn-outline gap-1'> <FaCloudDownloadAlt/> Download Certificate</Link>
                    <CustomUploadFileComponent onFileSelected={handleDistributionCertificateUpload}>
                        <div className={"btn btn-ghost btn-xs max-w-xs mt-1 " + (isLoading ? "loading" : "")}>Re-upload Certificate</div>
                    </CustomUploadFileComponent>
                </div>);
        } else {
            return (
                <>
                    <div className='flex flex-col'>
                        <h3 className='custom'>Distribution Certificate:</h3>
                        {isLoading ? <progress className="progress w-56"></progress> : <></>}
                        <input type="file" className="file-input w-full max-w-xs mt-1" disabled={isDisabled}
                               onInput={(event: any) => handleDistributionCertificateUpload(event.target.files[0])}/>
                        <Link href={CustomRoutesUtils.getWikiDociOSDistributionCertificate()} className="text-xs italic underline text-slate-400" target={"_blank"}>How to Generate a Distribution Certificate for iOS App Development</Link>

                    </div>
                </>
            );
        }
    }

    return (
        <div className='flex flex-col gap-5'>
            <h1 className={"flex gap-2 custom items-center w-full text-center justify-center"}>
                <FaAppStoreIos/>{getLanguageLabel("WEB2APP_PAGE_IOS_BUILD")}</h1>
            {renderStepByStepGuideLink()}
            <InputTextComponent topLabel={"App Bundle Identifier"} hint={"ex: app.webtoapp.MYAPPNAME"} onChanged={onAppId} disabled={isDisabled}
                                inputType={"text"} value={props.projectData.iosConfigs.appId}/>
            {/*renderProvisionProfileCertFile()*/}
            {/*renderDistributionCertificateFile()*/}
            <div className='divider text-slate-400'>APP BUILDS</div>
            <ProjectiOSBuildComponent projectDetails={props.projectData} onProjectUpdated={props.onProjectUpdated} />
        </div>
    )
}