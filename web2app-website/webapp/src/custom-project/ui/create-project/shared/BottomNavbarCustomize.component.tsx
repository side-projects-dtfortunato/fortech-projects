import {ProjectBottomNavbarItem} from "../../../data/model/project-details.model";
import Link from "next/link";
import {FaTrash} from "react-icons/fa";
import AddBottomNavbarFormComponent from "./AddBottomNavbarForm.component";
import CommonUtils from "../../../../components/react-shared-module/logic/commonutils";

const MAX_TABS = 4;

export default function BottomNavbarCustomizeComponent(props: {listNavbarItems: ProjectBottomNavbarItem[], onBottomNavbarItemsChanged: (listNavbarItems: ProjectBottomNavbarItem[]) => void}) {


    function onAddedNavbarItem(navbarItem: ProjectBottomNavbarItem) {
        props.listNavbarItems.push(navbarItem);
        props.onBottomNavbarItemsChanged(props.listNavbarItems);
    }


    return (
        <div className='flex flex-col gap-3'>
            {props.listNavbarItems.length < MAX_TABS ?
                <AddBottomNavbarFormComponent onAddedNavbarItem={onAddedNavbarItem} /> : <></>}
        </div>
    )
}