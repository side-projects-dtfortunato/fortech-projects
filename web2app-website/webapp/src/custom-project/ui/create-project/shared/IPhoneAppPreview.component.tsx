export default function IPhoneAppPreviewComponent(props: {iframeSrc: string}) {
    return (
        <div style={{ position: 'relative', width: '375px', margin: 'auto', overflow: 'hidden' }}>
            {/* Iframe with correct aspect ratio */}
            <div
                style={{
                    position: 'relative',
                    paddingTop: '177.77%', // (9 / 16 * 100) for a 9:16 aspect ratio (iPhone X)
                }}
            >
                <iframe
                    title="Embedded Content"
                    src={props.iframeSrc}
                    frameBorder="0"
                    style={{
                        position: 'absolute',
                        top: '0',
                        left: '0',
                        width: '100%',
                        height: '100%',
                        zIndex: '1', // Increase the zIndex to make sure the iframe is above the overlay
                    }}
                    allowFullScreen
                ></iframe>
            </div>

            {/* iPhone Mockup Overlay */}
            <div
                style={{
                    position: 'absolute',
                    top: '0',
                    left: '0',
                    width: '100%',
                    height: '100%',
                    backgroundImage: 'url(iphone-mockup.png)', // Replace with your mockup image
                    backgroundSize: 'cover',
                    backgroundRepeat: 'no-repeat',
                    zIndex: '0', // Set zIndex to be behind the iframe
                }}
            ></div>
        </div>
    );
}