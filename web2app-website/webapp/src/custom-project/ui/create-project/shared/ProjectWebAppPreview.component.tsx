import {ProjectBottomNavbarItem, ProjectDetailsModel} from "../../../data/model/project-details.model";
import {PhoneLayout} from "react-device-layouts";
import ProjectBottomNavbarComponent from "./ProjectBottomNavbar.component";
import {useEffect, useState} from "react";

export default function ProjectWebAppPreviewComponent(props: {projectDetails: ProjectDetailsModel}) {
    const [loadUrl, setLoadUrl] = useState(props.projectDetails.url);


    useEffect(() => {
        setLoadUrl(props.projectDetails.url)
    }, [props.projectDetails]);

    function onNavbarClicked(navbarItem: ProjectBottomNavbarItem) {
        setLoadUrl(navbarItem.url!);
    }

    return (
        <div className="flex flex-col w-full drop-shadow rounded-lg border bg-slate-300 p-1" style={{height: 600, maxWidth: 300}}>
            <div className="flex flex-row justify-center my-1 mb-2">
                <div className='h-1 w-24 bg-white rounded-full'></div>
            </div>

            <iframe
                className='flex w-full h-full rounded-t-lg bg-white'
                title={props.projectDetails.title}
                src={loadUrl}
                width="100%"
                height="100%"
                frameBorder="0"
                allowFullScreen
            />
            <div className="flex w-full h-14 bg-white rounded-b-lg">
                <ProjectBottomNavbarComponent projectDetails={props.projectDetails} editable={false} onProjectDetailsUpdated={() => {}} onNavbarClicked={onNavbarClicked} />
            </div>
        </div>
    )

    /*return (
        <div className='w-full max-w-sm '>
            <PhoneLayout>
                <div className="relative w-full h-full">
                    <div className="absolute top-0 left-0 w-full h-full">
                        <iframe
                            title={props.projectDetails.title}
                            src={loadUrl}
                            width="100%"
                            height="100%"
                            frameBorder="0"
                            allowFullScreen
                        />
                    </div>
                    <div className="absolute bottom-0 left-0 w-full h-14 bg-white">
                        <ProjectBottomNavbarComponent projectDetails={props.projectDetails} editable={false} onProjectDetailsUpdated={() => {}} onNavbarClicked={onNavbarClicked} />
                    </div>
                </div>
            </PhoneLayout>
        </div>
    )*/
}