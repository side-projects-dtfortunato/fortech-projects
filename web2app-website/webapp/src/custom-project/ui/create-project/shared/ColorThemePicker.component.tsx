import {useState} from "react";
import {ColorResult, SwatchesPicker} from 'react-color'


export default function ColorThemePickerComponent(props: {selectedColor: string, label: string, onSelectedColorChanged: (selectedColor: string) => void}) {
    const [isPickerOpened, setIsPickerOpened] = useState(false);


    function onColorChanged(color: ColorResult) {
        props.onSelectedColorChanged(color.hex);
        setIsPickerOpened(false);
    }

    function renderSelectedColor() {
        return (
            <div className='flex flex-col gap-2 items-start'>
                <div className='rounded w-14 h-14 drop-shadow cursor-pointer hover:opacity-50'
                     style={{backgroundColor: props.selectedColor}} onClick={() => {setIsPickerOpened(!isPickerOpened)}} />
                <span className='text-md'>{props.label}</span>
            </div>
        )
    }

    function renderColorPicker() {
        if (isPickerOpened) {
            return (
                <div className='list-item-bg p-2'>
                    <SwatchesPicker color={props.selectedColor} onChange={onColorChanged} />
                </div>
            )
        } else {
            return (<></>);
        }
    }

    return (
        <div className='flex flex-col gap-2'>
            {renderSelectedColor()}
            {renderColorPicker()}
        </div>
    )
}