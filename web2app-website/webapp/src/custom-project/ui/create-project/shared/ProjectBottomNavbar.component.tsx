import {ProjectBottomNavbarItem, ProjectDetailsModel} from "../../../data/model/project-details.model";
import {useState} from "react";
import CommonUtils from "../../../../components/react-shared-module/logic/commonutils";
import {FaTrash} from "react-icons/fa";
import SVG from 'react-inlinesvg';

export default function ProjectBottomNavbarComponent(props: {projectDetails: ProjectDetailsModel, editable: boolean, onProjectDetailsUpdated: (projectDetails: ProjectDetailsModel) => void, onNavbarClicked?: (navbarItem: ProjectBottomNavbarItem) => void}) {
    const [selectedTabIndex, setSelectedTabIndex] = useState(0);

    if (props.projectDetails.bottomNavbar.length === 0) {
        return <></>
    }

    function onNavbarClicked(navbarItem: ProjectBottomNavbarItem, index: number) {
        if (props.onNavbarClicked) {
            props?.onNavbarClicked(navbarItem);
        }
        setSelectedTabIndex(index);
    }

    function onRemoveNavbarItem(index: number) {
        setSelectedTabIndex(0);
        props.projectDetails.bottomNavbar.splice(index, 1);
        props.onProjectDetailsUpdated(props.projectDetails);
    }

    function renderNavbarItem(navbarItem: ProjectBottomNavbarItem, index: number) {
        const isSelected: boolean = index === selectedTabIndex;

        return (
            <div className={'flex flex-col items-center justify-center p-2 gap-2 w-40 cursor-pointer ' + (isSelected ? "opacity-100" : "opacity-50")}
                 style={{ fill: props.projectDetails.uiStyle.iconColor, color: props.projectDetails.uiStyle.iconColor}}
                 onClick={() => onNavbarClicked(navbarItem, index)}>
                {
                    CommonUtils.renderStorageFileImage(navbarItem.icon)
                        ? <SVG src={CommonUtils.renderStorageFileImage(navbarItem.icon)} height={20} width={20} color={props.projectDetails.uiStyle.iconColor} fill={props.projectDetails.uiStyle.iconColor} />  : <></>
                }


                <span className='text-sm' style={{ color: props.projectDetails.uiStyle.iconColor }}>{navbarItem.label}</span>

                {props.editable ? <span className={"flex flex-row gap-1 items-center text-xs text-red-500 cursor-pointer"} onClick={() => onRemoveNavbarItem(index)}><FaTrash/> Remove</span> : <></>}
            </div>
        )
    }

    return (
        <div className='flex flex-row gap-2 justify-evenly w-full max-w-xl border' style={{backgroundColor: props.projectDetails.uiStyle.navbarBgColor}}>
            {props.projectDetails.bottomNavbar.map(renderNavbarItem)}
        </div>
    )
}