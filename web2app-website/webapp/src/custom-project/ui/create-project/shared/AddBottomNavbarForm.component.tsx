import {ProjectBottomNavbarItem} from "../../../data/model/project-details.model";
import {useState} from "react";
import InputTextComponent from "../../../../components/react-shared-module/ui-components/form/InputText.component";
import {LuImagePlus} from "react-icons/lu";
import UploadImageComponent from "../../../../components/react-shared-module/ui-components/form/UploadImage.component";
import {MdAdd} from "react-icons/md";
import CommonUtils from "../../../../components/react-shared-module/logic/commonutils";
import IconPickerButtonComponent
    from "../../../../components/react-shared-module/ui-components/shared/IconPickerButton.component";
import IconPickerComponent from "../../../../components/react-shared-module/ui-components/shared/IconPicker.component";

const ICON_PICKER_BTN_SIZE = 47;

export default function AddBottomNavbarFormComponent(props: {onAddedNavbarItem: (navbarItem: ProjectBottomNavbarItem) => void}) {
    const [keyId, setKeyId] = useState(Date.now);
    const [isIconPickerOpened, setIsIconPickerOpened] = useState(false);
    const [navbarItem, setNavbarItem] = useState<ProjectBottomNavbarItem>({
        label: "",
        url: "",
        icon: {
            fileUrl: "",
            storagePath: "",
        },
    });

    const handleImageUpload = (selectedImage: string) => {
        setIsIconPickerOpened(false);
        setNavbarItem({
            ...navbarItem,
            icon: {
                fileUrl: selectedImage,
                storagePath: "",
            },
        });
    };

    function onLabelChanged(text: string) {
        setNavbarItem({
            ...navbarItem,
            label: text,
        });
    }

    function onUrlChanged(text: string) {
        setNavbarItem({
            ...navbarItem,
            url: text,
        });
    }

    function onAddClicked() {
        props.onAddedNavbarItem(navbarItem);
        setNavbarItem({
            label: "",
            url: "",
            icon: {
                fileUrl: "",
                storagePath: "",
            },
        });
        setKeyId(Date.now);
    }

    function isValidToAdd() {
        return navbarItem.label
            && navbarItem.label.length > 2
            && navbarItem.url && CommonUtils.isValidHttpUrl(navbarItem.url);
    }

    function renderIconPickerButton() {
        return (
            <div
                style={{
                    width: ICON_PICKER_BTN_SIZE + 'px',
                    height: ICON_PICKER_BTN_SIZE + 'px',
                    border: '2px dashed #aaa',
                    borderRadius: '10px',
                    display: 'flex',
                    justifyContent: 'center',
                    alignItems: 'center',
                    cursor: 'pointer',
                }} onClick={() => setIsIconPickerOpened(true)}
            >
                {navbarItem.icon?.fileUrl ? (
                    <img
                        src={navbarItem.icon?.fileUrl}
                        alt="Selected"
                        style={{ maxWidth: '100%', maxHeight: '100%',
                            borderRadius: '10px', padding: "5px" }}
                    />
                ) : (
                    <LuImagePlus color={"#aaa"} size={(ICON_PICKER_BTN_SIZE/2) + "px"} />
                )}
            </div>
        )
    }

    function renderIconPicker() {
        if (isIconPickerOpened) {
            return (
                <div className='list-item-bg drop-shadow bg-slate-100'>
                    <IconPickerComponent onIconSelected={handleImageUpload} onCancel={() => setIsIconPickerOpened(false)}/>
                </div>
            )
        } else {
            return (<></>);
        }

    }

    return (
        <div className='flex flex-col gap-3'>
            <div className='flex flex-row gap-3 items-end'>
                <div >
                    {renderIconPickerButton()}
                </div>
                <InputTextComponent topLabel={""} hint={"Tab Label"} onChanged={onLabelChanged} inputType={"text"} value={navbarItem.label} />
                <InputTextComponent topLabel={""} hint={"Tab Link"} onChanged={onUrlChanged} inputType={"url"} value={navbarItem.url} />
                <button className="btn btn-square btn-outline btn-primary" onClick={onAddClicked} disabled={!isValidToAdd()}>
                    <MdAdd size={25} />
                </button>
            </div>
            {renderIconPicker()}
        </div>
    )
}