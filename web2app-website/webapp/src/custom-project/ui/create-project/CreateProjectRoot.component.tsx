import {useEffect, useState} from "react";
import {
    initProjectDetailsModel,
    ProjectDetailsModel,
    ProjectDetailsUtils
} from "../../data/model/project-details.model";
import CreateProjectStepInitialComponent from "./steps/CreateProjectStepInitial.component";
import CreateProjectStepPreviewAppComponent from "./steps/CreateProjectStepPreviewApp.component";
import {CustomBackendApi} from "../../api/custombackend.api";
import {
    RootGlobalStateActions,
    useAlertMessageGlobalState
} from "../../../components/react-shared-module/logic/global-hooks/root-global-state";
import CreateProjectStepSuccessfulCreatedComponent from "./steps/CreateProjectStepSuccessfulCreated.component";
import {useAuthState} from "react-firebase-hooks/auth";
import {auth, ClientRealtimeDbUtils} from "../../../components/react-shared-module/logic/shared-data/firebase.utils";
import CommonUtils from "../../../components/react-shared-module/logic/commonutils";
import {CustomRealtimeCollectionDB} from "../../data/custom-firestore-collection-names";
import {CustomRoutesUtils} from "../../custom-routes.utils";
import {useRouter} from "next/router";

export default function CreateProjectRootComponent(props: {url?: string, demoId?: string}) {
    const [projectDetails, setProjectDetails] = useState<ProjectDetailsModel>(initProjectDetailsModel({url: props.url}))
    const [currentStep, setCurrentStep] = useState(0);
    const [isLoading, setIsLoading] = useState(false);
    const [alertMessage, setAlertMessage] = useAlertMessageGlobalState();
    const [currentUser, isAuthLoading] = useAuthState(auth);
    const router = useRouter();

    useEffect(() => {
        // Load Demo Project if exists
        if (props.demoId) {
            setIsLoading(true);

            // Get Demo Project
            ClientRealtimeDbUtils.getData({
                collectionName: CustomRealtimeCollectionDB.DraftDemoProjects,
                docId: props.demoId,
            }).then((demoProjectData: ProjectDetailsModel | undefined) => {
                if (demoProjectData) {
                    setProjectDetails(demoProjectData)
                }
                setIsLoading(false);
            }).catch((err) => {
                setIsLoading(false);
            });
        }
    }, [props.demoId]);

    function onProjectUpdated(projectDetails: ProjectDetailsModel) {
        setProjectDetails({
            ...projectDetails
        });
    }

    function renderStepsIndicators() {
        return (
            <ul className="steps">
                <li className="step step-primary">New Project</li>
                <li className={"step " + (currentStep >= 1 ? "step-primary" : "")}>Preview App</li>
                <li className={"step " + (currentStep >= 2 ? "step-primary" : "")}>Project Created 🚀</li>
            </ul>
        )
    }

    function renderStepComponent() {
        switch (currentStep) {
            case 0:

                return (
                    <div className='flex list-item-bg drop-shadow p-5 flex-row sm:gap-5 justify-between'>
                        <CreateProjectStepInitialComponent projectData={projectDetails} onProjectUpdated={onProjectUpdated} autoImportFromUrl={props.demoId ? false : true}/>
                        {
                            CommonUtils.isValidHttpUrl(projectDetails.url) ? (<div className='hidden md:flex'>
                                <CreateProjectStepPreviewAppComponent projectData={projectDetails} onProjectUpdated={onProjectUpdated} />
                            </div>)  : <></>
                        }
                    </div>
                )
            case 1:
                return (
                    <div className='list-item-bg drop-shadow p-5'>
                        <CreateProjectStepPreviewAppComponent projectData={projectDetails} onProjectUpdated={onProjectUpdated} />
                    </div>
                )
            case 2:
                return (<CreateProjectStepSuccessfulCreatedComponent projectDetails={projectDetails} />)
        }

        return (<></>);
    }

    function onBuildItClicked() {
        // Api Request
        setIsLoading(true);
        CustomBackendApi
            .web2appCreateProject({projectData: projectDetails}).then((apiResponse) => {
                setIsLoading(false);
                if (apiResponse.isSuccess) {
                    // Redirect to Project Details
                    // setCurrentStep(2);
                    router.replace(CustomRoutesUtils.getProjectDetailsURL(projectDetails.uid!));

                } else {
                    RootGlobalStateActions.displayAPIResponseError({
                        apiResponse: apiResponse,
                        setAlertMessageState: setAlertMessage,
                    });
                }
        }).catch((err) => {
            setIsLoading(false);
            RootGlobalStateActions.displayAlertMessage({
                message: err.message,
                alertType: "ERROR",
                setAlertMessageState: setAlertMessage,
            });
        });
    }

    function renderBottomButtons() {
        let listButtons: any[] = [];

        if (currentStep > 0 && currentStep < 2) {
            listButtons.push(<button className={"btn btn-ghost btn-sm " + (isLoading ? "loading" : "")} disabled={isLoading} onClick={() => setCurrentStep(currentStep - 1)}>Back</button>);
        }

        switch (currentStep) {
            case 0:
                // listButtons.push(<button className='btn btn-primary' disabled={!ProjectDetailsUtils.isProjectStepValid(projectDetails, currentStep)} onClick={() => setCurrentStep(currentStep + 1)}>Preview</button>)
                listButtons.push(<button className={'btn btn-success ' + (isLoading ? "loading" : "")} disabled={!ProjectDetailsUtils.isProjectStepValid(projectDetails, currentStep) || isLoading} onClick={onBuildItClicked}>Create Project 🚀</button>)
                break;
            /*case 1:
                listButtons.push(<button className={'btn btn-success ' + (isLoading ? "loading" : "")} disabled={!ProjectDetailsUtils.isProjectStepValid(projectDetails, currentStep) || isLoading} onClick={onBuildItClicked}>Create Project 🚀</button>)
                break;*/
        }

        return listButtons;
    }


    return (
        <div className='flex flex-col gap-5 m-5 items-stretch justify-stretch w-full max-w-6xl min-h-screen mt-5'>
            {/*renderStepsIndicators()*/}
            <div className='flex flex-row gap-2 w-full justify-end'>
                {renderBottomButtons()}
            </div>
            {renderStepComponent()}
            <div className='flex flex-row gap-2 w-full justify-end'>
                {renderBottomButtons()}
            </div>
        </div>
    )
}