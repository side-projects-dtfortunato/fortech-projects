import {ProjectBottomNavbarItem, ProjectDetailsModel} from "../../../data/model/project-details.model";

import ColorThemePickerComponent from "../shared/ColorThemePicker.component";
import BottomNavbarCustomizeComponent from "../shared/BottomNavbarCustomize.component";
import {FaAppStoreIos} from "react-icons/fa";
import {HiRocketLaunch} from "react-icons/hi2";
import ProjectWebAppPreviewComponent from "../shared/ProjectWebAppPreview.component";
import IPhoneAppPreviewComponent from "../shared/IPhoneAppPreview.component";
import {getLanguageLabel} from "../../../../components/react-shared-module/logic/language/language.helper";
import {useAuthState} from "react-firebase-hooks/auth";
import {auth} from "../../../../components/react-shared-module/logic/shared-data/firebase.utils";
import AuthManager from "../../../../components/react-shared-module/logic/auth/auth.manager";
import SignupLoginCallToActionContainerComponent
    from "../../../../components/react-shared-module/ui-components/shared/SignupLoginCallToActionContainer.component";


export default function CreateProjectStepPreviewAppComponent(props: {projectData: ProjectDetailsModel, onProjectUpdated: (projectData: ProjectDetailsModel) => void}) {
    const [currentUser, isAuthLoading] = useAuthState(auth);

    function renderAuthenticationRequired() {
        if (AuthManager.isUserLogged()) {
            return <></>;
        } else {
            return (
                <div className='flex flex-col items-center gap-2'>
                    <div className='divider text-slate-400'>CREATE AN ACCOUNT</div>
                    <SignupLoginCallToActionContainerComponent customMessage={(<span><b>Create an account</b> before we convert your <b>Mobile App</b></span>)} />
                </div>
            )
        }
    }

    return (
        <div className='flex flex-col gap-2'>
            <h1 className={"flex gap-2 custom items-center w-full text-center justify-center"}><HiRocketLaunch />{getLanguageLabel("WEB2APP_PAGE_PREVIEW_APP")}</h1>
            <div className='flex w-full justify-center p-5'>
                <ProjectWebAppPreviewComponent projectDetails={props.projectData} />
            </div>
            {renderAuthenticationRequired()}
        </div>
    )
}