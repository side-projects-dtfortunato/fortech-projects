import {ProjectDetailsModel} from "../../../data/model/project-details.model";
import {AiFillAndroid, AiFillApple} from "react-icons/ai";
import Lottie from "lottie-react";
import {
    LottieMapIconsUtils
} from "../../../../components/react-shared-module/ui-components/lottie/lottie-map-icons.utils";
import Link from "next/link";
import PricingPlansTableComponent from "../../pricing/shared/PricingPlansTable.component";
import {CustomRoutesUtils} from "../../../custom-routes.utils";
import {ProjectDetailsPageMenuOptions} from "../../../../pages/studio/project/[projectId]";
import PricingShortenWebToAppComponent from "../../pricing/shared/PricingShortenWebToApp.component";

export default function CreateProjectStepSuccessfulCreatedComponent(props: {projectDetails: ProjectDetailsModel}) {

    function renderSuccessfulHeader() {
        return (
            <div className='flex flex-col items-center'>
                <div style={{maxWidth: 100, maxHeight: 100}}>
                    <Lottie animationData={LottieMapIconsUtils.lottie_success_icon} loop={false} width={50} height={50} />
                </div>
                <h1 className='custom text-green-500 font-bold text-center'>Congratulations, your project was created!</h1>
                <span className='text-base text-center text-slate-500'>Start by choosing one of our Subscription Plans that fits your needs</span>
            </div>
        )
    }

    function renderSubscriptionPlan() {
        return (
            <div className='flex flex-col gap-5 items-center'>
                <PricingShortenWebToAppComponent projectDetails={props.projectDetails} hideTitle={true}/>
                <div className='divider'>OR</div>
                <Link href={CustomRoutesUtils.getProjectDetailsURL(props.projectDetails.uid!)} className='text-base underline text-slate-500 text-center'>Skip it for now and open my project dashboard</Link>
            </div>
        )
    }

    function renderButtons() {
        return (
            <div className='flex flex-wrap w-full justify-evenly'>
                <Link className='btn btn-success gap-2' href={CustomRoutesUtils.getProjectDetailsURL(props.projectDetails.uid!, ProjectDetailsPageMenuOptions.ANDROID_BUILD)}><AiFillAndroid /> Build an Android App</Link>
                <Link className='btn btn-primary gap-2' href={CustomRoutesUtils.getProjectDetailsURL(props.projectDetails.uid!, ProjectDetailsPageMenuOptions.IOS_BUILD)}><AiFillApple /> Build an iOS App</Link>
                <Link className='btn btn-ghost' href={CustomRoutesUtils.getProjectDetailsURL(props.projectDetails.uid!)}>Go to project</Link>
            </div>
        )
    }

    return (
        <div className='flex flex-col gap-10 items-center'>
            {renderSuccessfulHeader()}
            {renderSubscriptionPlan()}
        </div>
    );
}