import {ProjectBottomNavbarItem, ProjectDetailsModel} from "../../../data/model/project-details.model";
import InputTextComponent from "../../../../components/react-shared-module/ui-components/form/InputText.component";
import CommonUtils from "../../../../components/react-shared-module/logic/commonutils";
import UploadImageComponent from "../../../../components/react-shared-module/ui-components/form/UploadImage.component";
import {IoCreateOutline} from "react-icons/io5";
import ColorThemePickerComponent from "../shared/ColorThemePicker.component";
import ProjectBottomNavbarComponent from "../shared/ProjectBottomNavbar.component";
import BottomNavbarCustomizeComponent from "../shared/BottomNavbarCustomize.component";
import {getLanguageLabel} from "../../../../components/react-shared-module/logic/language/language.helper";
import {useEffect, useState} from "react";
import Lottie from "lottie-react";
import {
    LottieMapIconsUtils
} from "../../../../components/react-shared-module/ui-components/lottie/lottie-map-icons.utils";
import {SharedBackendApi} from "../../../../components/react-shared-module/logic/shared-data/sharedbackend.api";
import {
    RootGlobalStateActions,
    useAlertMessageGlobalState
} from "../../../../components/react-shared-module/logic/global-hooks/root-global-state";
import {CustomUtils} from "../../../data/custom.utils";
import {CustomBackendApi} from "../../../api/custombackend.api";
import {ApiResponse} from "../../../../components/react-shared-module/logic/shared-data/datamodel/base.model";
import {CiImport} from "react-icons/ci";
import {LuImport} from "react-icons/lu";

const MAX_LOGO_SIZE_BYTES = 600000; // 600k bytes

export default function CreateProjectStepInitialComponent(props: {projectData: ProjectDetailsModel, onProjectUpdated: (projectData: ProjectDetailsModel) => void, autoImportFromUrl: boolean}) {
    const [isExportingUrlData, setIsExportingUrlData] = useState(false);
    const [hasExportedData, setHasExportedData] = useState(props.projectData.uid !== undefined ? true : false);
    const [alertMessage, setAlertMessage] = useAlertMessageGlobalState();

    useEffect(() => {
        if (!hasExportedData && props.autoImportFromUrl) {
            onConvertWebsite();
        }
    }, [hasExportedData])

    function renderIsExportingUrlData() {
        if (isExportingUrlData) {
            return (
                <div className='flex flex-col items-center justify-center'>
                    <div className='flex w-64 h-64'>
                        <Lottie animationData={LottieMapIconsUtils.lottieWebsiteAnalyzing} width={50} height={50} loop={true}/>
                    </div>
                    <h3 className='custom text-center'>Wait a moment, we are importing your Website style</h3>
                </div>
            )
        } else {
            return <></>
        }

    }

    async function onConvertWebsite() {
        if (!CommonUtils.isValidHttpUrl(props.projectData.url)) {
            return;
        }

        setIsExportingUrlData(true);


        let apiResponse: ApiResponse<ProjectDetailsModel> = await CustomBackendApi.prefetchUrlToConvert({projectData: props.projectData});

        if (!apiResponse.isSuccess) {
            RootGlobalStateActions.displayAPIResponseError({apiResponse: apiResponse, setAlertMessageState: setAlertMessage});
        } else {
            RootGlobalStateActions.displayAlertMessage({
                alertType: "SUCCESS",
                setAlertMessageState: setAlertMessage,
                message: "We imported some infos of your site successfully",
            });
        }

        if (apiResponse.responseData) {
            props.onProjectUpdated(apiResponse.responseData!);
        }

        setHasExportedData(true);
        setIsExportingUrlData(false);
    }

    function onTitleChanged(title: string) {
        props.projectData.title = title;
        props.onProjectUpdated(props.projectData);
    }

    function onUrlChanged(url: string) {
        if (!url.startsWith("http")) {
            url = "https://" + url;
        }

        props.projectData.url = url;
        props.onProjectUpdated(props.projectData);
    }

    function handleImageUpload(uploadFile: File) {

        if (uploadFile) {
            const reader = new FileReader();
            reader.onloadend = async () => {
                let imageBase64: string = reader.result as string;
                // Resize image
                imageBase64 = await CommonUtils.resizeBase64Image(imageBase64, MAX_LOGO_SIZE_BYTES);
                props.projectData.logo = {
                    fileUrl: imageBase64,
                    storagePath: "",
                    tempFile: uploadFile,
                };
                props.onProjectUpdated(props.projectData);
            };
            reader.readAsDataURL(uploadFile);
        }
    }

    function getDefaultLogoImage() {
        if (props.projectData.logo?.tempFile !== undefined) {
            return URL.createObjectURL(props.projectData.logo?.tempFile);
        } else {
            return props.projectData.logo?.fileUrl;
        }

    }

    function renderImageUpload() {
        return (
            <div className='flex flex-col gap-2 items-start'>
                <UploadImageComponent size={100} onImageSelected={handleImageUpload} defaultImage={getDefaultLogoImage()} />
            </div>
        )
    }

    function onBottomNavbarColorChanged(selectedColor: string) {
        props.projectData.uiStyle.navbarBgColor = selectedColor;
        props.onProjectUpdated(props.projectData);
    }
    function onIconColorChanged(selectedColor: string) {
        props.projectData.uiStyle.iconColor = selectedColor;
        props.onProjectUpdated(props.projectData);
    }

    function onBottomNavbarItemsChanged(listNavbarItems: ProjectBottomNavbarItem[]) {
        props.projectData.bottomNavbar = listNavbarItems;
        props.onProjectUpdated(props.projectData);
    }

    function renderColorThemeContainer() {
        return (
            <div className='flex flex-col gap-2'>
                <div className='flex flex-col'>
                    <h3 className='custom'>App Color Theme:</h3>
                    <span className='text-xs text-slate-400 italic'>Pick the color to be used in some UI components of the app</span>
                </div>

                <div className='flex flex-wrap gap-10'>
                    <ColorThemePickerComponent selectedColor={props.projectData.uiStyle.iconColor} onSelectedColorChanged={onIconColorChanged} label={"Primary Color"} />
                    <ColorThemePickerComponent selectedColor={props.projectData.uiStyle.navbarBgColor} onSelectedColorChanged={onBottomNavbarColorChanged} label={"Background Color"} />
                </div>
            </div>
        )
    }

    function renderUrlField() {
        return (
            <div className='flex flex-col sm:flex-row gap-5 items-end'>
                <InputTextComponent topLabel={"Website Url to Convert"} hint={"https://..."} onChanged={onUrlChanged} value={props.projectData.url} inputType={"url"} />
                <button className='btn btn-primary text-white' disabled={!CommonUtils.isValidHttpUrl(props.projectData.url) || isExportingUrlData} onClick={onConvertWebsite}><LuImport/>IMPORT WEBSITE STYLE</button>
            </div>
        )
    }

    function renderCustomizationFields() {
        if (!hasExportedData && !props.projectData.title) {
            return <></>
        }

        return (
            <>
                <InputTextComponent topLabel={"App name"} hint={"Set the name of your app"} onChanged={onTitleChanged} value={props.projectData.title} inputType={"text"} />


                <div className='flex flex-wrap gap-5'>
                    <div className='flex flex-col gap-1 mr-20'>
                        <h3 className='custom'>App Logo:</h3>
                        {renderImageUpload()}
                    </div>

                    {renderColorThemeContainer()}
                </div>


                <div className='flex flex-col gap-1'>
                    <div className='flex flex-col'>
                        <h3 className='custom'>Bottom Navbar Menu:</h3>
                        <span className='text-xs text-slate-400 italic'>Configure the navigation bottom navbar (max. 4 tabs)</span>
                    </div>
                    <div className='flex w-full justify-start drop-shadow'>
                        <ProjectBottomNavbarComponent projectDetails={props.projectData} editable={true} onProjectDetailsUpdated={props.onProjectUpdated} />
                    </div>
                </div>
                <BottomNavbarCustomizeComponent listNavbarItems={props.projectData.bottomNavbar} onBottomNavbarItemsChanged={onBottomNavbarItemsChanged} />
            </>
        )
    }

    return (
        <div className='flex flex-col gap-8'>
            <h1 className={"flex gap-2 custom items-center w-full text-center justify-center"}><IoCreateOutline />{getLanguageLabel("WEB2APP_PAGE_APP_STYLE")}</h1>

            {renderUrlField()}
            {renderIsExportingUrlData()}

            {renderCustomizationFields()}
        </div>
    )
}