import PricingShortenTableComponent, {
    PricingShortenPlansTableModel
} from "../../../../components/react-shared-module/ui-components/pricing-shorten/PricingShortenTable.component";
import {ProjectDetailsModel, ProjectSubscriptionPlan} from "../../../data/model/project-details.model";
import SubscriptionPlanButtonComponent from "./SubscriptionPlanButton.component";
import {
    PaymentPeriodType
} from "../../../../components/react-shared-module/ui-components/pricing/PricingTable.component";
import {CustomRoutesUtils} from "../../../custom-routes.utils";
import Link from "next/link";

export default function PricingShortenWebToAppComponent(props: {projectDetails?: ProjectDetailsModel, hideTitle?: boolean}) {

    function renderSubscribeButton(isRecommended: boolean, planType: ProjectSubscriptionPlan, periodPayment: PaymentPeriodType) {
        if (!props.projectDetails) {
            return (<Link href={CustomRoutesUtils.getCreateProjectPageUrl()} className={'btn btn-primary text-white ' + (isRecommended ? "" : "btn-outline")}>Build Your App Now</Link>)
        } else {
            return (<SubscriptionPlanButtonComponent extraStyle={isRecommended ? "" : "btn-outline"} projectDetails={props.projectDetails} planType={planType} periodPayment={periodPayment} />)
        }
    }

    function getPricingPlans(): PricingShortenPlansTableModel[] {
        return [
            {
                title: "Android App",
                isRecommended: false,
                id: ProjectSubscriptionPlan.ANDROID_ONLY,
                description: "Convert your Website to an Android App only and publish it to the Play Store.",
                monthlyPrice: {
                    label: "$15/month",
                    btnComponent: renderSubscribeButton(false, ProjectSubscriptionPlan.ANDROID_ONLY, "monthlyPrice"),
                },
                yearlyPrice: {
                    label: "$45/year",
                    btnComponent: renderSubscribeButton(false, ProjectSubscriptionPlan.ANDROID_ONLY, "yearlyPrice"),
                },
                icon: (
                    <img className='object-contain' src={"/images/ic_android_app.png"}/>
                )
            },
            {
                title: "iOS & Android App",
                isRecommended: true,
                id: ProjectSubscriptionPlan.ANDROID_AND_IOS,
                description: "Convert your Website to an iOS and Android App and publish it to the respective App Stores.",
                monthlyPrice: {
                    label: "$30/month",
                    btnComponent: renderSubscribeButton(true, ProjectSubscriptionPlan.ANDROID_AND_IOS, "monthlyPrice"),
                },
                yearlyPrice: {
                    label: "$99/year",
                    btnComponent: renderSubscribeButton(true, ProjectSubscriptionPlan.ANDROID_AND_IOS, "yearlyPrice"),
                },
                icon: (
                    <div className='flex flex-row gap-2 w-full justify-center'>
                        <img className='object-contain' src={"/images/ic_android_app.png"}/>
                        <img className='object-contain' src={"/images/ic_app_store.png"}/>
                    </div>
                )
            },
            {
                title: "iOS App",
                isRecommended: false,
                id: ProjectSubscriptionPlan.IOS_ONLY,
                description: "Convert your Website to an iOS App only and publish it to the Apple App Store.",
                monthlyPrice: {
                    label: "$20/month",
                    btnComponent: renderSubscribeButton(false, ProjectSubscriptionPlan.IOS_ONLY, "monthlyPrice"),
                },
                yearlyPrice: {
                    label: "$75/year",
                    btnComponent: renderSubscribeButton(false, ProjectSubscriptionPlan.IOS_ONLY, "yearlyPrice"),
                },
                icon: (
                    <img className='object-contain' src={"/images/ic_app_store.png"}/>
                )
            },
        ]
    }

    return (
        <PricingShortenTableComponent pricePlans={getPricingPlans()} yearlySave={"Save 73% annually"} hideTitle={props.hideTitle}/>
    )
}