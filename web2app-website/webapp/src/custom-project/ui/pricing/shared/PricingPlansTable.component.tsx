import PricingTableComponent, {
    PaymentPeriodType,
    PricingTablePlanContentModel
} from "../../../../components/react-shared-module/ui-components/pricing/PricingTable.component";
import {ProjectDetailsModel, ProjectSubscriptionPlan} from "../../../data/model/project-details.model";
import {useAuthState} from "react-firebase-hooks/auth";
import {auth} from "../../../../components/react-shared-module/logic/shared-data/firebase.utils";
import {useState} from "react";
import {
    useAlertMessageGlobalState,
    useCentralUserDataState
} from "../../../../components/react-shared-module/logic/global-hooks/root-global-state";
import {useRouter} from "next/router";
import SubscriptionPlanButtonComponent from "./SubscriptionPlanButton.component";
import Link from "next/link";
import {CustomRoutesUtils} from "../../../custom-routes.utils";

/**
 * @deprecated
 * @param props
 * @constructor
 */
export default function PricingPlansTableComponent(props: {projectDetails?: ProjectDetailsModel}) {
    const [currentUser, isAuthLoading] = useAuthState(auth);
    const [centralUserData, setCentralUserData] = useCentralUserDataState();
    const [billingPeriod, setBillingPeriod] = useState<PaymentPeriodType>("yearlyPrice");
    const [alertMessage, setAlertMessage] = useAlertMessageGlobalState();

    const route = useRouter();

    let subscribedPlanType: ProjectSubscriptionPlan | undefined = props.projectDetails?.subscription?.planType;


    function renderTable(planContent: PricingTablePlanContentModel, tablePlanType: ProjectSubscriptionPlan, isRecommended: boolean) {
        let displayTable: boolean = true;
        let isCurrentPlan: boolean = false;

        if (subscribedPlanType) {
            if (subscribedPlanType === tablePlanType) {
                isCurrentPlan = true;
            } else {
                displayTable = false;
            }
        }
        displayTable = true;

        let pricePlanButton: any = <Link href={CustomRoutesUtils.getCreateProjectPageUrl()} className={'btn btn-primary ' + (isRecommended ? "" : "btn-outline")}>Get Started</Link>;

        if (props.projectDetails) {
            pricePlanButton = <SubscriptionPlanButtonComponent extraStyle={isRecommended ? "" : "btn-outline"} planType={tablePlanType} projectDetails={props.projectDetails!} periodPayment={billingPeriod}/>;
        }


        if (displayTable) {
            return (
                <PricingTableComponent planTableContent={planContent}
                                       isCurrentPlan={isCurrentPlan}
                                       billingPeriod={billingPeriod}
                                       isRecommended={isRecommended}
                                       buttonComponent={pricePlanButton}/>
            );
        } else {
            return (<></>);
        }
    }


    return (
        <div className='flex flex-wrap sm:flex-grow justify-evenly gap-2 w-full'>
            {/*renderTable(StaticPricingPlansContent.FREE_PLAN, ProjectSubscriptionPlan.FREE_PLAN)*/}
            {renderTable(StaticPricingPlansContent.ANDROID_ONLY, ProjectSubscriptionPlan.ANDROID_ONLY, false)}
            {renderTable(StaticPricingPlansContent.IOS_ONLY, ProjectSubscriptionPlan.IOS_ONLY, false)}
            {renderTable(StaticPricingPlansContent.ANDROID_AND_IOS, ProjectSubscriptionPlan.ANDROID_AND_IOS, true)}
        </div>
    )
}


export const StaticPricingPlansContent = {
    FREE_PLAN: {
        planType: ProjectSubscriptionPlan.FREE_PLAN,
        planName: "Preview App",
        planDescription: "Start your Mobile App project for free and preview how it will look like your app.",
        priceLabel: "$0",
        listFeatures: [
            {
                label: "Customize your bottom navbar",
                type: "INCLUDED",
            },
            {
                label: "Test and preview your app",
                type: "INCLUDED",
            },
            {
                label: "Customize before you pay",
                type: "INCLUDED",
            },

        ]
    } as PricingTablePlanContentModel,
    ANDROID_ONLY: {
        planType: ProjectSubscriptionPlan.ANDROID_ONLY,
        planName: "Android App (only)",
        planDescription: "Build your Android App and publish it on Play Store.",
        priceLabel: "$25",
        listFeatures: [
            {
                label: "Customize your bottom navbar",
                type: "INCLUDED",
            },
            {
                label: "Test and preview your app",
                type: "INCLUDED",
            },
            {
                label: "Customize before you pay",
                type: "INCLUDED",
            },
            {
                label: "Build an Android App",
                type: "INCLUDED",
            },
            {
                label: "Our support on publishing your app to Play Store",
                type: "INCLUDED",
            },
            {
                label: "App Package (.aab file)",
                type: "INCLUDED",
            },
        ]
    } as PricingTablePlanContentModel,
    IOS_ONLY: {
        planType: ProjectSubscriptionPlan.IOS_ONLY,
        planName: "iOS App (only)",
        planDescription: "Build and publish an app on App Store",
        priceLabel: "$40",
        listFeatures: [
            {
                label: "Customize your bottom navbar",
                type: "INCLUDED",
            },
            {
                label: "Test and preview your app",
                type: "INCLUDED",
            },
            {
                label: "Customize before you pay",
                type: "INCLUDED",
            },
            {
                label: "Build an iOS App",
                type: "INCLUDED",
            },
            {
                label: "Our support on publishing your app to Play Store",
                type: "INCLUDED",
            },
        ]
    } as PricingTablePlanContentModel,
    ANDROID_AND_IOS: {
        planType: ProjectSubscriptionPlan.ANDROID_AND_IOS,
        planName: "iOS and Android App",
        planDescription: "Build and publish an iOS and Android app.",
        priceLabel: "$50",
        listFeatures: [
            {
                label: "Everything included in the Android Plan",
                type: "INCLUDED",
            },
            {
                label: "Everything included in the iOS Plan",
                type: "INCLUDED",
            },
        ]
    } as PricingTablePlanContentModel,
}