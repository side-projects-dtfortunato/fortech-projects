import {
    ProjectDetailsModel, ProjectDetailsUtils,
    ProjectSubscriptionPlan
} from "../../../data/model/project-details.model";
import {useAuthState} from "react-firebase-hooks/auth";
import {auth} from "../../../../components/react-shared-module/logic/shared-data/firebase.utils";
import {
    RootGlobalStateActions,
    useAlertMessageGlobalState,
    useCentralUserDataState
} from "../../../../components/react-shared-module/logic/global-hooks/root-global-state";
import {useState} from "react";
import {useRouter} from "next/router";
import RootConfigs from "../../../../configs";
import AuthManager from "../../../../components/react-shared-module/logic/auth/auth.manager";
import {getLoginPageLink} from "../../../../pages/login";
import {CustomBackendApi} from "../../../api/custombackend.api";
import {getPaymentSuccessPageUrl} from "../../../../pages/payment/success";
import {
    PaymentPeriodType
} from "../../../../components/react-shared-module/ui-components/pricing/PricingTable.component";


export default function SubscriptionPlanButtonComponent(props: {projectDetails: ProjectDetailsModel,
    planType: ProjectSubscriptionPlan,
    periodPayment?: PaymentPeriodType,
    priceId?: string,
    extraStyle?: string}) {
    const [currentUser, isAuthLoading] = useAuthState(auth);
    const [centralUserData, setCentralUserData] = useCentralUserDataState();
    const [isLoading, setIsLoading] = useState(false);
    const route = useRouter();
    const [alertMessage, setAlertMessage] = useAlertMessageGlobalState();

    const isPaymentsDisabled: boolean = RootConfigs.DISABLE_PAYMENTS;

    async function onSubscribeClicked() {
        setIsLoading(true);

        if (!AuthManager.isUserLogged()) {
            // Redirect user to Login page
            route.push(getLoginPageLink());
        } else {
            // Check if user already have a subscription
            if (props.projectDetails.subscription && props.projectDetails.subscription.planType !== ProjectSubscriptionPlan.FREE_PLAN) {
                // Already have a subscription, so, can use the upgrade function
                const apiResponse = await CustomBackendApi.stripeUpdateSubscriptionPlan({projectId: props.projectDetails.uid!, planType: props.planType, periodPayment: props.periodPayment, priceId: props.priceId});

                if (apiResponse.isSuccess) {
                    RootGlobalStateActions.displayAlertMessage({alertType: "SUCCESS", message: "Congratulations. You updated successfully your subscription", setAlertMessageState: setAlertMessage});
                    await route.push(getPaymentSuccessPageUrl(props.projectDetails.uid!));
                } else {
                    RootGlobalStateActions
                        .displayAPIResponseError({apiResponse: apiResponse, setAlertMessageState: setAlertMessage});
                }
            } else {

                // NEW SUBSCRIBER
                const apiResponse = await CustomBackendApi.stripeCreateCheckoutSession({
                    planType: props.planType,
                    periodPayment: props.periodPayment,
                    projectId: props.projectDetails.uid!,
                    priceId: props.priceId,
                });
                if (apiResponse.isSuccess && apiResponse.responseData && apiResponse.responseData.redirectUrl) {
                    document.location.href = apiResponse.responseData.redirectUrl;
                } else {
                    RootGlobalStateActions
                        .displayAPIResponseError({apiResponse: apiResponse, setAlertMessageState: setAlertMessage});
                }
            }

            // Update Central User Data
            await AuthManager.getCentralUserData(true);
        }


        setIsLoading(false);
    }

    function getSubscriptionLabel() {
        const hasSubscription: boolean = ProjectDetailsUtils.hasProjectAnySubscription(props.projectDetails);
        const freeTrialLabel: string = "";
        switch (props.planType) {
            case ProjectSubscriptionPlan.FREE_PLAN:
                return "Try for Free";
            case ProjectSubscriptionPlan.ANDROID_ONLY:
                return "Select Android Plan" + freeTrialLabel;
            case ProjectSubscriptionPlan.IOS_ONLY:
                return "Select iOS Plan" + freeTrialLabel;
            case ProjectSubscriptionPlan.ANDROID_AND_IOS:
                return "Select Android & iOS Plan" + freeTrialLabel;
        }
    }

    function isSubscribed() {
        return props.planType === props.projectDetails.subscription?.planType;
    }

    return (
        <button className={'btn btn-primary text-white ' + (isLoading ? "loading" : "") + " " + props.extraStyle} disabled={isPaymentsDisabled || isAuthLoading || isSubscribed() || isLoading} onClick={onSubscribeClicked}>{getSubscriptionLabel()}</button>
    )
}