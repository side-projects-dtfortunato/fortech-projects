import {ShowcaseProjectModel} from "../../../data/model/showcase-project.model";
import Link from "next/link";
import {useEffect, useState} from "react";

export default function ShowcaseProjectItemComponent(props: {showcaseItemData: ShowcaseProjectModel}) {
    const [imageIndex, setImageIndex] = useState(0);

    useEffect(() => {
        const timer = setInterval(() => {
            let updatedImageIndex = imageIndex + 1;
            if (updatedImageIndex >= props.showcaseItemData.images.length) {
                updatedImageIndex = 0;
            }
            setImageIndex(updatedImageIndex);
        }, 5000);

        return () => {
            clearInterval(timer);
        };
    }, []);

    function renderLeftContent() {
        const downloadPlayStore = props.showcaseItemData.links.playstore
            ? (<Link href={props.showcaseItemData.links.playstore} target={"_blank"}><img className='w-24' src={"/images/ic_download_google_play.png"} alt='Download it on Play Store' /></Link>)
            : <></>;
        const downloadAppStore = props.showcaseItemData.links.appstore
            ? (<Link href={props.showcaseItemData.links.appstore} target={"_blank"}><img className='w-24' src={"/images/ic_download_app_store.png"} alt='Download it on App Store' /></Link>)
            : <></>;

        return (
            <div className='flex flex-col gap-4 max-w-60'>
                <div className='flex flex-row gap-2 items-center'>
                    <img className='h-16 w-16 rounded-lg' src={props.showcaseItemData.logo} alt={props.showcaseItemData.appName}/>
                    <Link className='text-black font-bold text-lg' href={props.showcaseItemData.links.website} target={"_blank"}>{props.showcaseItemData.appName}</Link>
                </div>
                <p className='text-slate-700 text-sm'>{props.showcaseItemData.description}</p>
                <div className='flex flex-wrap gap-2'>
                    {downloadPlayStore}
                    {downloadAppStore}
                </div>
            </div>
        )
    }

    function renderScreenshot() {
        return (
            <div className='flex justify-start'>
                <div className="rounded-xl bg-white p-2">
                    <img className='rounded-xl h-56' src={props.showcaseItemData.images[imageIndex]}/>
                </div>
            </div>
        )
    }

    return (
        <div className='flex flex-row p-5 gap-4 bg-slate-100 rounded-xl drop-shadow-xl '>
            {renderLeftContent()}
            {renderScreenshot()}
        </div>
    );
}