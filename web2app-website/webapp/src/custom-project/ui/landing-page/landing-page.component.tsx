import CustomRootLayoutComponent from "../../../components/app-components/root/CustomRootLayout.component";
import SaasLandingPageTopComponent
    from "../../../components/react-shared-module/ui-components/saas-landing-page/shared/saas-landing-top.component";
import LandingPageHowItWorksComponent from "./shared/landing-page-how-it-works.component";
import LandingPagePricingComponent from "./shared/landing-page-pricing.component";
import Link from "next/link";
import {CustomRoutesUtils} from "../../custom-routes.utils";
import AuthManager from "../../../components/react-shared-module/logic/auth/auth.manager";
import BlogLatestPostsContainerComponent
    from "../../../components/react-shared-module/ui-components/blog/shared/blog-latest-posts-container.component";
import {
    BasePostDataModel
} from "../../../components/react-shared-module/logic/shared-data/datamodel/base-post-data.model";
import {
    PostFormArticleBlocksDetails
} from "../../../components/react-shared-module/logic/shared-data/datamodel/posts-model/post-article-blocks";
import LandingPageConvertWebsiteCtaComponent from "./shared/landing-page-convert-website-cta.component";
import LandingFeaturesListComponent, {
    LandingFeatureListItemModel
} from "../../../components/react-shared-module/ui-components/landing-page-content/shared/LandingFeaturesList.component";
import {SiConvertio} from "react-icons/si";
import {DiAndroid} from "react-icons/di";
import {IoLogoAppleAppstore, IoLogoGooglePlaystore} from "react-icons/io5";
import {FaApple, FaAppStoreIos, FaGooglePlay, FaMobile, FaPaintBrush} from "react-icons/fa";
import {IoIosCreate, IoMdNotifications} from "react-icons/io";
import {MdOutlineAccountCircle, MdOutlineSendToMobile, MdOutlineSupportAgent} from "react-icons/md";
import LandingHowItWorksMinimalistComponent
    , {
    LandingHowItWorksStepModel
} from "../../../components/react-shared-module/ui-components/landing-page-content/shared/LandingHowItWorksMinimalist.component";
import {CgWebsite} from "react-icons/cg";
import CustomFaqComponent from "./shared/custom-faq.component";
import ShowcaseProjectItemComponent from "../showcase/shared/showcase-project-item.component";
import {CustomShowcaseListData} from "../../data/model/showcase-project.model";
import LandingPageShowcaseComponent from "./shared/landing-page-showcase.component";

export default function LandingPageComponent(props: {initialListPosts: BasePostDataModel<any, PostFormArticleBlocksDetails>[]}) {

    function renderLoggedInCta() {
        return (
            <div className='flex flex-col gap-2 items-center'>
                <h3 className="custom">Welcome back.</h3>
                <span className="subtitle">What do you want to do now?</span>
                <div className='flex flex-wrap gap-5 justify-center'>
                    <Link className='btn btn-primary btn-outline' href={CustomRoutesUtils.getStudioDashboardUrl()}>My Converted Apps</Link>
                </div>
            </div>
        )
    }

    return (
        <CustomRootLayoutComponent customBody={true} backgroundColor={"bg-white"}>
            <div className='flex flex-col items-center w-full py-10 px-5'>
                <div className='flex flex-col max-w-6xl gap-10'>
                    <SaasLandingPageTopComponent heroImgUrl={"/images/landing-page-convert-to-app.png"} loggedInComponent={renderLoggedInCta()}
                                                 customComponent={(<LandingPageConvertWebsiteCtaComponent />)}/>
                    <LandingPageShowcaseComponent />
                    {renderHowItWorks()}
                    <BlogLatestPostsContainerComponent listPosts={props.initialListPosts} seeMoreArticle={true} />
                    {renderListFeatures()}
                    <LandingPagePricingComponent />
                    <CustomFaqComponent isLandingPage={true} />
                    {
                        /*
                          <CoMakerLandingPagePopularToolsComponent />
                          <CoMakerLandingPageHowItWorksComponent />
                          <CoMakerLandingPagePricingComponent />
                         */
                    }
                </div>
            </div>
        </CustomRootLayoutComponent>
    )
}

function renderListFeatures() {
    let listFeatures: LandingFeatureListItemModel[] = [
        {
            icon: <SiConvertio />,
            title: "Convert your Website to App",
            description: "With our easy to use web to app converter, you can easily convert your Website to an Android and iOS native app",
            color: "red",
        },
        {
            icon: <DiAndroid />,
            title: "Make an Android App",
            description: "Our platform supports any type of Website to convert it to an Android Native App.",
            color: "green",
        },
        {
            icon: <FaApple />,
            title: "Make an iOS App",
            description: "Our platform supports any type of Website to convert it to an iOS Native App.",
            color: "slate",
        },
        {
            icon: <IoLogoGooglePlaystore />,
            title: "Publishable to the Play Store",
            description: "After you make your App through our platform you can publish your App to Google Play Store (the Android App Store)",
            color: "emerald",
        },
        {
            icon: <FaAppStoreIos />,
            title: "Publishable to the App Store",
            description: "After you make your App through our platform you can publish your iOS App to the App Store (the iOS App Store)",
            color: "sky",
        },
        {
            icon: <IoMdNotifications />,
            title: "Push Notifications",
            description: (<>With our partnership with OneSignal.com you can easily integrate and manage push notifications directly to your Mobile App Users. Take a look to <a className='underline' href='https://OneSignal.com'>OneSignal.com</a> for more information</>),
            color: "blue",
        },
        {
            icon: <FaPaintBrush />,
            title: "Custom Style",
            description: "Customize the style of your app such as the App Logo, Colors, Bottom Navbar, icons and much more.",
            color: "orange",
        },
        {
            icon: <MdOutlineSupportAgent />,
            title: "Customer Support",
            description: "We are here to help you, you can contact us at any moment and we will answer asap to help you to make your beautiful Mobile App.",
            color: "lime",
        },
    ];

    return (
        <LandingFeaturesListComponent listFeatures={listFeatures} />
    )
}

function renderHowItWorks() {
    const listSteps: LandingHowItWorksStepModel[] = [
        {
            title: "Create a new project",
            description: "Create a new project in your User Account.",
            icon: <IoIosCreate />,
        },
        {
            title: "Website URL",
            description: "In the initial project form, copy & paste your Website URL and we will automatically import your Website infos.",
            icon: <CgWebsite />
        },
        {
            title: "Customize your App Style",
            description: "Edit freely the style of your app, such as your App name, logo, colors, bottom navbar, navigation links and much more.",
            icon: <FaPaintBrush />
        },
        {
            title: "Preview and Build it",
            description: "Preview your app, do your last adjustments and after you do the last required adjustments you can request to we build your Android or iOS App.",
            icon: <MdOutlineSendToMobile />
        },
        {
            title: "Publish it to the App Stores",
            description: "After we build properly your app you will be able to publish it directly in the App Stores (Play Store and App Store)",
            icon: (
                <div className='flex gap-1 text-sm'>
                    <FaGooglePlay />
                    <FaAppStoreIos />
                </div>
            )
        },

    ]
    
    return (
        <LandingHowItWorksMinimalistComponent title={"How Web To App Works?"} subtitle={"Convert your Website into an iOS and/or Android app in just a few steps."} steps={listSteps} videoUrl={"<iframe width=\"100%\" height=\"315\" src=\"https://www.youtube.com/embed/DCASQ5xGtno?si=rr1LDSBekVY0sf4l\" title=\"YouTube video player\" frameborder=\"0\" allow=\"accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share\" allowfullscreen></iframe>"} />
    )
}