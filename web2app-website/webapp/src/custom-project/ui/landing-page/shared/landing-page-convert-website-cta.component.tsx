import InputTextComponent from "../../../../components/react-shared-module/ui-components/form/InputText.component";
import {useState} from "react";
import {useRouter} from "next/router";
import {CustomRoutesUtils} from "../../../custom-routes.utils";

export default function LandingPageConvertWebsiteCtaComponent() {
    const [linkForm, setLinkForm] = useState("");
    const router = useRouter();

    function onConvertWebClicked()
    {
        router.push(CustomRoutesUtils.getCreateProjectPageUrl({defaultUrl: linkForm}));
    }

    return (
        <div className='flex flex-col gap-2'>
            <div className='divider text-slate-400'>OR START CONVERTING NOW</div>
            <div className='flex flex-wrap gap-2 justify-center items-center w-full'>
                <div className='flex w-max-xl'>
                    <InputTextComponent  topLabel={""} hint={"Paste your Website URL"} onChanged={(value) => setLinkForm(value)} defaultValue={linkForm} value={linkForm} inputType={"url"} />
                </div>
                <button className='btn btn-primary text-white' onClick={onConvertWebClicked}>Convert Web To App</button>
            </div>
        </div>
    )
}