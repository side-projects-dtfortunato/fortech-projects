import FaqComponent, {FAQItemModel} from "../../../../components/react-shared-module/ui-components/faq/faq.component";

const LIST_FAQ_LIST: FAQItemModel[] = [
    {
      title: "1. What is webtoapp.app?",
      message: "Webtoapp.app is a platform that allows you to turn your website into a dedicated app for Android and iOS.",
    },
    {
      title: "2. How do I convert my website to an app?",
      message: "You can convert your website to an iOS and Android app by input your Website URL, customize the app style, subscribe one of our plans and request the build of your app to publish to the App Store.",
    },
    {
      title: "3. What features does webtoapp.app offer?",
      message: "Webtoapp.app offers features like Push Notifications with OneSignal integration, Detect Installs, monetize your app, customize the App Style and much more. We offer our complete support to help you publish the app in the respective App Store.",
    },
    {
      title: "4. How much does it cost to use webtoapp.app?",
      message: "The service starts from $2/month billed annually to publish an Android app or $4/month billed annually to publish an Android and iOS App.",
    },
    {
      title: "5. What happens when my website changes? Does my app show the changes too?",
      message: "Yes, immediately, the app should reflect the changes made to your website.",
    },
    {
      title: "6. How long does it take to develop my app?",
      message: "The exact time can vary, but the platform is designed to turn your website into an app within 5 minutes, and we take up to 24 hours to make your App Build to publish it to the App Stores.",
    },
]

export default function CustomFaqComponent(props: {isLandingPage: boolean}) {
    return (
        <FaqComponent listFAQItems={LIST_FAQ_LIST} isLandingPage={props.isLandingPage}/>
    )
}