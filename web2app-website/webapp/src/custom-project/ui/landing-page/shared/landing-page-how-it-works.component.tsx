import LandingPageSecondaryTitleComponent
    from "../../../../components/react-shared-module/ui-components/saas-landing-page/shared/landing-page-secondary-title.component";

export default function LandingPageHowItWorksComponent() {


    function renderSteps() {
        let renderListSteps: any[] = [];

        // Step 1
        renderListSteps.push(renderStep({
            step: 1,
            title: "Create an account",
            subtitle: "Start by create an account on our studio",
            imgSrc: "/images/landing-page/landing_page_how_works_step_1.png",
        }));

        // Step 2
        renderListSteps.push(renderStep({
            step: 2,
            title: "Register your Website URL",
            subtitle: "Tell us what is the name of your app and the URL of the website you want to convert to a Mobile App.",
            imgSrc: "/images/landing-page/landing_page_how_works_step_2.png",
        }));

        // Step 3
        renderListSteps.push(renderStep({
            step: 3,
            title: "Customize the style of your app",
            subtitle: "Customize the style of your app such as the Logo and the bottom navigation tabs.",
            imgSrc: "/images/landing-page/landing_page_how_works_step_3.png",
        }));

        // Step 4
        renderListSteps.push(renderStep({
            step: 4,
            title: "Preview it and it's ready to Launch 🚀",
            subtitle: "Check the preview of your app and it's ready to build. You just have to share with some App Store certificates and it's ready to publish, only in just a few steps.",
            imgSrc: "/images/landing-page/landing_page_how_works_step_4.png",
            isCentered: true,
        }));

        return renderListSteps.map((item) => {
            return (
                <div key={item.step} className='my-8 w-full'>
                    {item}
                </div>
            )
        });
    }

    return (
        <div className='flex flex-col w-full items-stretch gap-5'>
            <LandingPageSecondaryTitleComponent subtitle={"Convert your Website into an iOS and/or Android app in just a few steps."}>How it works?</LandingPageSecondaryTitleComponent>
            {renderSteps()}
        </div>
    )
}


function renderStep(props: {
    step: number,
    title: string,
    subtitle: string,
    imgSrc?: string,
    customImage?: any,
    isCentered?: boolean
}) {
    let renderText: any = (
        <div className='flex flex-row items-start gap-2'>
            <div className='rounded-full w-10 h-10 bg-red-400 bg-opacity-25 justify-center items-center'>
                <h2 className='custom text-center'>{props.step}</h2>
            </div>
            <div className='flex flex-col'>
                <h2 className='custom'>{props.title}</h2>
                <span className='custom'>{props.subtitle}</span>
            </div>
        </div>
    )

    let renderImage: any = props.imgSrc ? (
        <img className='shadow-lg shadow-rose-400/50 rounded' src={props.imgSrc} alt={props.title} height={500} width={400}/>
    ) : (<></>);
    if (props.customImage) {
        renderImage = props.customImage
    }

    if (props.isCentered) {
        return (
            <div className='flex flex-col justify-between items-center gap-3'>
                {renderText}
                {renderImage}
            </div>
        )
    } else {
        return (
            <div className='flex flex-col sm:flex-row justify-between items-center sm:items-start gap-4'>
                {renderText}
                {renderImage}
            </div>
        )
    }

}