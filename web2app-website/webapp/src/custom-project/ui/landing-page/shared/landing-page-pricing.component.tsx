import LandingPageSecondaryTitleComponent
    from "../../../../components/react-shared-module/ui-components/saas-landing-page/shared/landing-page-secondary-title.component";
import PricingPlansTableComponent from "../../pricing/shared/PricingPlansTable.component";
import PricingShortenWebToAppComponent from "../../pricing/shared/PricingShortenWebToApp.component";

export default function LandingPagePricingComponent() {

    return (
        <div className='flex flex-col w-full items-stretch gap-5'>
            <LandingPageSecondaryTitleComponent subtitle={"You can build the app for the stores that you prefer separately."}>Pricing</LandingPageSecondaryTitleComponent>
            <PricingShortenWebToAppComponent/>
        </div>
    )
}