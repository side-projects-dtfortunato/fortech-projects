import {CustomShowcaseListData, ShowcaseProjectModel} from "../../../data/model/showcase-project.model";
import ShowcaseProjectItemComponent from "../../showcase/shared/showcase-project-item.component";
import LandingPageSecondaryTitleComponent
    from "../../../../components/react-shared-module/ui-components/saas-landing-page/shared/landing-page-secondary-title.component";

export default function LandingPageShowcaseComponent() {

    function renderShowcaseItem(showcaseItem: ShowcaseProjectModel) {
        return (
            <div className="carousel-item pl-4 sm:pl-0">
                <ShowcaseProjectItemComponent showcaseItemData={showcaseItem} />
            </div>
        )
    }

    return (
        <div className='flex flex-col gap-2 max-w-max'>
            <LandingPageSecondaryTitleComponent subtitle={"Some of the apps that were created by our happy customers. Will your app be the next on this list?"}>Showcase</LandingPageSecondaryTitleComponent>
            <div className="carousel carousel-center p-4 space-x-4 bg-neutral rounded-box w-screen sm:w-full">
                {CustomShowcaseListData.map((item) => renderShowcaseItem(item))}
            </div>
        </div>
    )
}