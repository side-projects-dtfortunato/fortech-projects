import SideMenuItemComponent from "./shared/side-menu-item.component";
import {FiEdit} from "react-icons/fi";
import {IoLogoAndroid} from "react-icons/io";
import {FaAppStoreIos} from "react-icons/fa";
import {HiOutlineDevicePhoneMobile} from "react-icons/hi2";
import {IoSettings} from "react-icons/io5";
import {MdPayments} from "react-icons/md";
import {PiPlugsConnectedBold} from "react-icons/pi";
import {getLanguageLabel} from "../../../components/react-shared-module/logic/language/language.helper";
import {CustomRoutesUtils} from "../../custom-routes.utils";
import {ProjectDetailsPageMenuOptions} from "../../../pages/studio/project/[projectId]";

export default function ProjectDetailsSideMenuComponent(props: {projectId: string, selectedMenuOption: ProjectDetailsPageMenuOptions, publishButton?: any}) {

    return (
        <div className='flex flex-col gap-3'>
            <SideMenuItemComponent path={CustomRoutesUtils.getProjectDetailsURL(props.projectId, ProjectDetailsPageMenuOptions.APP_THEME)}
                                   label={"App Theme"} icon={<FiEdit />}
                                   isSelected={props.selectedMenuOption === ProjectDetailsPageMenuOptions.APP_THEME}/>

            <SideMenuItemComponent path={CustomRoutesUtils.getProjectDetailsURL(props.projectId, ProjectDetailsPageMenuOptions.ANDROID_BUILD)}
                                   label={"Android Build"} icon={<IoLogoAndroid />}
                                   isSelected={props.selectedMenuOption === ProjectDetailsPageMenuOptions.ANDROID_BUILD}/>

            <SideMenuItemComponent path={CustomRoutesUtils.getProjectDetailsURL(props.projectId, ProjectDetailsPageMenuOptions.IOS_BUILD)}
                                   label={"iOS Build"} icon={<FaAppStoreIos />}
                                   isSelected={props.selectedMenuOption === ProjectDetailsPageMenuOptions.IOS_BUILD}/>

            <SideMenuItemComponent path={CustomRoutesUtils.getProjectDetailsURL(props.projectId, ProjectDetailsPageMenuOptions.SETTINGS)}
                                   label={"Project Settings"} icon={<IoSettings />}
                                   isSelected={props.selectedMenuOption === ProjectDetailsPageMenuOptions.SETTINGS}/>

            <SideMenuItemComponent path={CustomRoutesUtils.getProjectDetailsURL(props.projectId, ProjectDetailsPageMenuOptions.EXTERNAL_INTEGRATIONS)}
                                   label={getLanguageLabel("WEB2APP_PAGE_EXTERNAL_INTEGRATIONS")!} icon={<PiPlugsConnectedBold />}
                                   isSelected={props.selectedMenuOption === ProjectDetailsPageMenuOptions.EXTERNAL_INTEGRATIONS}/>

            <SideMenuItemComponent path={CustomRoutesUtils.getProjectDetailsURL(props.projectId, ProjectDetailsPageMenuOptions.APP_PREVIEW)}
                                   label={"App Preview"} icon={<HiOutlineDevicePhoneMobile />}
                                   isSelected={props.selectedMenuOption === ProjectDetailsPageMenuOptions.APP_PREVIEW}/>

            <SideMenuItemComponent path={CustomRoutesUtils.getProjectDetailsURL(props.projectId, ProjectDetailsPageMenuOptions.SUBSCRIPTION)}
                                   label={"Subscription Plan"} icon={<MdPayments />}
                                   isSelected={props.selectedMenuOption === ProjectDetailsPageMenuOptions.SUBSCRIPTION}/>
            {props.publishButton ? props.publishButton : <></>}
        </div>
    )
}