import Link from "next/link";
import {useRouter} from "next/router";
import {useEffect, useState} from "react";

export interface SideMenuItemParams {path: string, label: string, icon?: any, isSelected?: boolean}

export default function SideMenuItemComponent(props: SideMenuItemParams) {

    let classSuffix: string = "bg-transparent text-slate-500 hover:bg-red-50 hover:text-red-400";
    if (props.isSelected) {
        classSuffix = " bg-red-100 text-red-700"
    }



    return (
        <Link href={props.path} className={'flex flex-row px-3 py-2 gap-3 items-center rounded-full text-base font-bold ' + classSuffix}>
            {props.icon}
            {props.label}
        </Link>
    );
}