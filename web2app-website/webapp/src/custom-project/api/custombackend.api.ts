import 'firebase/compat/firestore';
import {
    callFunction,
    db,
    getAPIDocument,
    uploadFileStoragePromise
} from "../../components/react-shared-module/logic/shared-data/firebase.utils";
import {ApiResponse} from "../../components/react-shared-module/logic/shared-data/datamodel/base.model";
import {
    ProjectAndroidConfigsModel,
    ProjectDetailsModel,
    ProjectDetailsUtils,
    ProjectIOSConfigsModel,
    ProjectPlatformTypes, ProjectSubscriptionPlan
} from "../data/model/project-details.model";
import AuthManager from "../../components/react-shared-module/logic/auth/auth.manager";
import CommonUtils from "../../components/react-shared-module/logic/commonutils";
import {CustomFirestoreCollectionDB} from "../data/custom-firestore-collection-names";
import {collection, getDocsFromServer, query, where} from "@firebase/firestore";
import {StorageFileModel} from "../../components/react-shared-module/logic/shared-data/datamodel/storage-file.model";
import {
    StripePaymentStatus
} from "../../components/react-shared-module/logic/shared-data/datamodel/custom-stripe.model";
import {PaymentPeriodType} from "../../components/react-shared-module/ui-components/pricing/PricingTable.component";


export class CustomBackendApi {

    static async web2appCreateProject(data: {
        projectData: ProjectDetailsModel
    }): Promise<ApiResponse<any>> {
        let projectData: ProjectDetailsModel = data.projectData;

        // Generate ProjectId
        if (!projectData.uid) {
            projectData.uid = CommonUtils.string_to_slug(projectData.title!) + "-" + Date.now();
        }
        projectData.userId = AuthManager.getUserId();


        if (projectData.logo?.tempFile) {
            // Upload Logo image first
            let projectPath: string = ProjectDetailsUtils.getProjectStorageFolderPath(projectData);
            const fileUrl: string = await uploadFileStoragePromise(projectData.logo?.tempFile, projectPath + "logo");

            if (fileUrl) {
                projectData.logo = {
                    fileUrl: fileUrl,
                    storagePath: projectPath + "logo",
                };
            }

            // Upload Tab Icons
            /*for (let i = 0; i < projectData.bottomNavbar.length; i++) {
                let tab: ProjectBottomNavbarItem = projectData.bottomNavbar[i];

                if (tab.icon && tab.icon.fileUrl) {

                }
            }*/
        }


        return await callFunction("web2appCreateProject", {
            projectData,
        });
    }

    static async getDemoProject(data: {demoId: string}) {
        return await callFunction("getDemoProject", data);
    }
    static async updatedAndroidKeystoreProject(data: {projectId: string, fileData: StorageFileModel}) {
        return await callFunction("updatedAndroidKeystoreProject", data);
    }

    static async updateiOSProvisioningProfileCert(data: {projectId: string, fileData: StorageFileModel}) {
        return await callFunction("updateiOSProvisioningProfileCert", data);
    }

    static async updateiOSDistributionCert(data: {projectId: string, fileData: StorageFileModel}) {
        return await callFunction("updateiOSDistributionCert", data);
    }

    static async getProjectDetailsData(projectId: string): Promise<ProjectDetailsModel | undefined> {
        return await getAPIDocument<ProjectDetailsModel>(CustomFirestoreCollectionDB.Web2AppProjects, projectId);
    }

    static async getUserListProjects(): Promise<ProjectDetailsModel[]> {
        let listProjects: ProjectDetailsModel[] = [];

        if (AuthManager.getUserId()) {
            listProjects = await this.getProjectsByUserId(AuthManager.getUserId()!);
        }
        return listProjects;
    }

    static async getProjectsByUserId(userId: string): Promise<ProjectDetailsModel[]> {
        const projectsRef = collection(db, CustomFirestoreCollectionDB.Web2AppProjects);
        const q = query(projectsRef, where("userId", "==", userId));
        const querySnapshot = await getDocsFromServer(q);

        const projects: ProjectDetailsModel[] = [];
        querySnapshot.forEach((doc) => {
            const projectData = doc.data() as ProjectDetailsModel;
            projectData.uid = doc.id;
            projects.push(projectData);
        });

        return projects;
    }

    static async web2appSaveProjectData(data: {
        projectData: ProjectDetailsModel
    }): Promise<ApiResponse<any>> {
        if (data.projectData.logo?.tempFile) {
            delete data.projectData.logo?.tempFile;
        }
        return await callFunction("web2appSaveProjectData", data);
    }

    static async web2appProjectAndroidBuildRequest(data: {
        projectId: string, androidConfigs?: ProjectAndroidConfigsModel
    }): Promise<ApiResponse<any>> {
        return await callFunction("web2appProjectAndroidBuildRequest", data);
    }

    static async web2appProjectIOSBuildRequest(data: {
        projectId: string, iosConfigs?: ProjectIOSConfigsModel
    }): Promise<ApiResponse<any>> {
        return await callFunction("web2appProjectIOSBuildRequest", data);
    }

    static async web2appProjectCancelBuild(data: { projectId: string, platformType: ProjectPlatformTypes }): Promise<ApiResponse<any>> {
        return await callFunction("web2appProjectCancelBuild", data);
    }

    static async projectPublishChanges(data: { projectId: string }): Promise<ApiResponse<any>> {
        return await callFunction("projectPublishChanges", data);
    }

    static async projectBuildCheckPayment(data: { buildId: string, projectId: string, platformType: ProjectPlatformTypes}): Promise<ApiResponse<StripePaymentStatus>> {
        return await callFunction("projectBuildCheckPayment", data);
    }

    static async prefetchUrlToConvert(data: { projectData: ProjectDetailsModel}): Promise<ApiResponse<ProjectDetailsModel>> {
        return await callFunction("prefetchUrlToConvert", data);
    }

    static async deleteProject(data: { projectId: string}): Promise<ApiResponse<StripePaymentStatus>> {
        return await callFunction("deleteProject", data);
    }
    static async adminGetAndroidBuildRequests(data: {}) {
        return await callFunction("adminGetAndroidBuildRequests", data);
    }
    static async adminGetiOSBuildRequests(data: {}) {
        return await callFunction("adminGetiOSBuildRequests", data);
    }

    static async adminCompleteAndroidBuildRequest(data: {
        projectId: string,
        buildRequestId: string,
        appPackage: StorageFileModel,
        message: string,
    }) {
        return await callFunction("adminCompleteAndroidBuildRequest", data);
    }

    static async adminCompleteIOSBuildRequest(data: {
        projectId: string,
        buildRequestId: string,
        message: string,
    }) {
        return await callFunction("adminCompleteIOSBuildRequest", data);
    }

    static async adminAndroidRejectBuild(data: {projectId: string, buildRequestId: string, message: string}) {
        return await callFunction("adminAndroidRejectBuild", data);
    }

    static async adminIOSRejectBuild(data: {projectId: string, buildRequestId: string, message: string}) {
        return await callFunction("adminIOSRejectBuild", data);
    }
    static async adminDraftDemoProject(data: {projectData: ProjectDetailsModel, contactEmail?: string}) {
        let projectData: ProjectDetailsModel = data.projectData;

        // Generate ProjectId
        if (!projectData.uid) {
            projectData.uid = CommonUtils.string_to_slug(projectData.title!) + "-" + Date.now();
        }


        if (projectData.logo?.tempFile) {
            // Upload Logo image first
            let projectPath: string = ProjectDetailsUtils.getProjectStorageFolderPath(projectData);
            const fileUrl: string = await uploadFileStoragePromise(projectData.logo?.tempFile, projectPath + "logo");

            if (fileUrl) {
                projectData.logo = {
                    fileUrl: fileUrl,
                    storagePath: projectPath + "logo",
                };
            }

            // Upload Tab Icons
            /*for (let i = 0; i < projectData.bottomNavbar.length; i++) {
                let tab: ProjectBottomNavbarItem = projectData.bottomNavbar[i];

                if (tab.icon && tab.icon.fileUrl) {

                }
            }*/
        }

        return await callFunction("adminDraftDemoProject", {
            projectData,
            contactEmail: data.contactEmail,
        });
    }
    static async adminDeleteDraftDemoProject(data: {projectUid: string}) {
        return await callFunction("adminDeleteDraftDemoProject", data);
    }
    
    static async stripeCreateCheckoutSession(data: {planType: ProjectSubscriptionPlan, periodPayment?: PaymentPeriodType, projectId: string, priceId?: string }): Promise<ApiResponse<{redirectUrl: string} | undefined>> {
        return await callFunction("stripeCreateCheckoutSession", data);
    }
    static async stripeCreateCustomerPortalSession(data: { env: "PROD" | "DEV", projectId: string}): Promise<ApiResponse<{redirectUrl: string} | undefined>> {
        return await callFunction("stripeCreateCustomerPortalSession", data);
    }
    static async stripeUpdateSubscriptionPlan(data: { projectId: string, planType: ProjectSubscriptionPlan,periodPayment?: PaymentPeriodType, priceId?: string}): Promise<ApiResponse<{redirectUrl: string} | undefined>> {
        return await callFunction("stripeUpdateSubscriptionPlan", data);
    }
}