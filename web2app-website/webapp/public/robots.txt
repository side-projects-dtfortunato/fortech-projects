# *
User-agent: *
Allow: /

# Host
Host: https://webtoapp.app

# Sitemaps
Sitemap: https://webtoapp.app/sitemap.xml
