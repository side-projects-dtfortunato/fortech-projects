import {BaseModel} from "../../../shareable-firebase-backend/model-data/base.model";
import {ProjectAndroidConfigsModel, ProjectBuildStatusType, ProjectDetailsModel} from "./project-details.model";
import {StorageFileModel} from "../../../shareable-firebase-backend/model-data/storage-file.model";

export interface ProjectAndroidBuildRequestModel extends BaseModel {
    projectId: string;
    buildConfigs: ProjectAndroidConfigsModel;
    buildReleaseFile?: StorageFileModel;
    status: ProjectBuildStatusType;
    displayMessage?: string;
    projectSnapshot: ProjectDetailsModel;
}