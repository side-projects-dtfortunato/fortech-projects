import {StorageFileModel} from "../../../shareable-firebase-backend/model-data/storage-file.model";
import {BaseModel} from "../../../shareable-firebase-backend/model-data/base.model";
import {ProjectAndroidBuildRequestModel} from "./project-android-build-request.model";
import {ProjectIOSBuildRequestModel} from "./project-ios-build-request.model";
import {ProjectPublishedAppStyleModel} from "./project-published-app-style.model";


export enum ProjectBuildStatusType {
    IN_PROGRESS = "IN_PROGRESS", BUILD_COMPLETED = "BUILD_COMPLETED",
    BUILD_FAILED = "BUILD_FAILED", BUILD_CANCELLED = "BUILD_CANCELLED"
}

export enum ProjectPlatformTypes {ANDROID = "ANDROID", IOS = "IOS"}

export enum ProjectSubscriptionPlan {
    FREE_PLAN = "FREE_PLAN",
    ANDROID_ONLY = "ANDROID_ONLY",
    IOS_ONLY = "IOS_ONLY",
    ANDROID_AND_IOS = "ANDROID_AND_IOS",
}

export interface ProjectDetailsModel extends BaseModel {
    title?: string;
    url?: string;
    logo?: StorageFileModel;
    androidConfigs: ProjectAndroidConfigsModel;
    iosConfigs: ProjectIOSConfigsModel;
    bottomNavbar: ProjectBottomNavbarItem[];
    uiStyle: {
        iconColor: string,
        navbarBgColor: string,
    };
    appConfigs?: {
        askFilesPermission?: boolean,
        hasOAuthSignin?: boolean,
    }
    userId?: string;
    lastBuilds?: {
        IOS?: ProjectIOSBuildRequestModel;
        ANDROID?: ProjectAndroidBuildRequestModel;
    };
    inProgressBuilds?: {
        IOS?: ProjectIOSBuildRequestModel;
        ANDROID?: ProjectAndroidBuildRequestModel;
    };
    settings: {
        contactEmail?: string;
        receiveEmailAlerts: boolean;
    };
    subscription?: {
        planType?: ProjectSubscriptionPlan,
        updatedAt?: number,
        subscriptionId?: string,
    };
    publishedData?: ProjectPublishedAppStyleModel;
    hasChangesPublished?: boolean;
    externalIntegrations?: ProjectExternalIntegrations;
    emailMarketing?: {
        notFinishedNotif?: boolean; // Email to notify the  user that his project wasn't finished
        expiringProjectNotif?: boolean // Email to notify when the Project is expiring (more than 30 days)
    },
    hasBuilt?: boolean;
}

export interface ProjectBottomNavbarItem {
    label?: string;
    url?: string;
    icon?: StorageFileModel;
}

export interface ProjectAndroidConfigsModel {
    appId?: string;
    keyStore?: StorageFileModel;
    keyStorePassword?: string;
    keyStoreAlias?: string;
    isEditable?: boolean;
    versionCode?: number;
    versionName?: string;
}

export interface ProjectIOSConfigsModel {
    appId?: string;
    provisioningProfileCertificate?: StorageFileModel;
    distributionCertificateFile?: StorageFileModel;
    isEditable?: boolean;
    versionCode?: number;
    versionName?: string;
}

export interface ProjectExternalIntegrations {
    oneSignalAppId?: string;
}

export class ProjectDetailsUtils {

    static getProjectPublishAppStyle(projectDetails: ProjectDetailsModel): ProjectPublishedAppStyleModel {
        let publishedData: ProjectPublishedAppStyleModel = {
            title: projectDetails.title,
            uid: projectDetails.uid,
            url: projectDetails.url,
            updatedAt: Date.now(),
            createdAt: projectDetails.createdAt,
            logo: projectDetails.logo,
            bottomNavbar: projectDetails.bottomNavbar,
            navbarBgColor: projectDetails.uiStyle?.navbarBgColor,
            iconColor: projectDetails.uiStyle?.iconColor,
            subscriptionPlan: projectDetails.subscription?.planType,
        };

        if (projectDetails.appConfigs) {
            if (projectDetails.appConfigs.askFilesPermission) {
                publishedData.askFilesPermission = projectDetails.appConfigs.askFilesPermission;
            }
            if (projectDetails.appConfigs.hasOAuthSignin) {
                publishedData.hasOAuthSignin = projectDetails.appConfigs.hasOAuthSignin;
            }
        }
        if (projectDetails.externalIntegrations) {
            publishedData.externalIntegrations = projectDetails.externalIntegrations;
        }

        return publishedData;
    }

}