import {BaseModel} from "../../../shareable-firebase-backend/model-data/base.model";
import {ProjectBuildStatusType, ProjectDetailsModel, ProjectIOSConfigsModel} from "./project-details.model";
import {StorageFileModel} from "../../../shareable-firebase-backend/model-data/storage-file.model";

export interface ProjectIOSBuildRequestModel extends BaseModel {
    projectId: string;
    buildConfigs: ProjectIOSConfigsModel;
    buildReleaseFile?: StorageFileModel;
    projectSnapshot?: ProjectDetailsModel;
    status: ProjectBuildStatusType;
    displayMessage?: string;
}