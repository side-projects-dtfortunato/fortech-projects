import {BaseModel} from "../../../shareable-firebase-backend/model-data/base.model";
import {ProjectBottomNavbarItem, ProjectExternalIntegrations, ProjectSubscriptionPlan} from "./project-details.model";
import {StorageFileModel} from "../../../shareable-firebase-backend/model-data/storage-file.model";


export interface ProjectPublishedAppStyleModel extends BaseModel {
    title?: string;
    url?: string;
    bottomNavbar: ProjectBottomNavbarItem[];
    iconColor?: string;
    navbarBgColor?: string;
    logo?: StorageFileModel;
    subscriptionPlan?: ProjectSubscriptionPlan;
    externalIntegrations?: ProjectExternalIntegrations;
    askFilesPermission?: boolean;
    hasOAuthSignin?: boolean;
}