import {
    SharedCentralUserDataModel
} from "../../../shareable-firebase-backend/model-data/frontend-model-data/shared-central-user-data.model";

export interface CustomCentralUserDataModel extends SharedCentralUserDataModel {
    stripeCustomerId?: string;
}