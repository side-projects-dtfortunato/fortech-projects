export const CustomFirestoreCollectionDB = {
    Web2AppProjects: "Web2AppProjects",
    UserProjectsCreated: "UserProjectsCreated",
    ProjectAndroidBuildRequests: "ProjectAndroidBuildRequests",
    ProjectiOSBuildRequests: "ProjectiOSBuildRequests",
}

export const CustomRealtimeCollectionDB = {
    PublishedAppStyle: "PublishedAppStyle",
    DraftDemoProjects: "DraftDemoProjects",
}

export const CustomFirestoreDocsDB = {
}