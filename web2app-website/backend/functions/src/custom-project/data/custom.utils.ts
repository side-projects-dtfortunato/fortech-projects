const crypto = require('crypto');

export class CustomUtils {
   static ENCRYPTION_KEY = "BiT9KXZPFTJqiPFt3vhZA5bbM0TtU1dFIuP2ak8V47ORLMN74YT2ib36X1EEK2fA";
   static IV_LENGTH = 16;

   static encryptText(text: string) {
      const iv = crypto.randomBytes(CustomUtils.IV_LENGTH);
      const cipher = crypto.createCipheriv('aes-256-cbc', Buffer.from(CustomUtils.ENCRYPTION_KEY), iv);
      let encrypted = cipher.update(text);
      encrypted = Buffer.concat([encrypted, cipher.final()]);
      return iv.toString('hex') + ':' + encrypted.toString('hex');
   }

   static decryptText(text: string): string {
      const textParts = text.split(':');
      const iv = Buffer.from(textParts[0], 'hex');
      const encryptedText = Buffer.from(textParts[1], 'hex');
      const decipher = crypto.createDecipheriv('aes-256-cbc', Buffer.from(CustomUtils.ENCRYPTION_KEY), iv);
      let decrypted = decipher.update(encryptedText);
      decrypted = Buffer.concat([decrypted, decipher.final()]);
      return decrypted.toString();
   }
}