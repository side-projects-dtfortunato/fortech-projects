import * as functions from "firebase-functions";
import * as admin from "firebase-admin";
import {ProjectDetailsModel, ProjectSubscriptionPlan} from "../../data/model/project-details.model";
import {getQuickResponse, getResponse} from "../../../shareable-firebase-backend/model-data/api-response.model";
import {PaymentPeriodType, StripeConfigs, stripeDev, stripeProd} from "../../utils/stripe-utils";
import {BackendSharedUtils} from "../../../shareable-firebase-backend/utils/backend-shared-utils";
import {getDocumentData} from "../../../shareable-firebase-backend/utils/utils";
import {CustomFirestoreCollectionDB} from "../../data/custom-firestore-collection-names";
import {CustomCentralUserDataModel} from "../../data/model/custom-central-user-data.model";
import {SharedFirestoreCollectionDB} from "../../../shareable-firebase-backend/model-data/shared-firestore-collections";
import {Log} from "../../../shareable-firebase-backend/utils/log";

export const stripeUpdateSubscriptionPlan = functions.https.onCall(
    (data: { projectId: string, planType: ProjectSubscriptionPlan, periodPayment?: PaymentPeriodType, priceId?: string },
     context) => {
        if (!context.auth || !context.auth.uid) return getResponse(1001, undefined, "User must be logged in to execute this action");
        const userId: string = context && context.auth && context.auth.uid ? context.auth.uid : "";
        Log.onCall(data, userId);

        return new Promise(async (resolve, reject) => {
            const env = BackendSharedUtils.isProjectProduction() ? "PROD" : "DEV";
            let priceId: string | undefined = data.priceId ? data.priceId : StripeConfigs[env].plans[data.planType][data.periodPayment!];

            if (!priceId) {
                resolve(getQuickResponse(false, null, "Plan not found"));
                return;
            }

            // Check if the project has already this plan subscribed
            const projectDetails: ProjectDetailsModel = await getDocumentData(CustomFirestoreCollectionDB.Web2AppProjects, data.projectId);

            if (projectDetails.subscription && projectDetails.subscription.planType === data.planType) {
                resolve(getQuickResponse(false, null, "You already have subscribed this plan"));
                return;
            }

            const stripeApi = env === "PROD" ? stripeProd : stripeDev;

            // First check if the user already have a customerId on stripe
            const centralUserDataModel: CustomCentralUserDataModel = await getDocumentData(SharedFirestoreCollectionDB.CentralUserData, userId);
            let customerId: string = "";
            if (!centralUserDataModel?.stripeCustomerId) {
                // We should create the customer first
                let customerObj = await stripeApi.customers.create({
                    email: centralUserDataModel.email,
                    name: centralUserDataModel.publicProfile?.name,
                    metadata: {
                        userId: centralUserDataModel.uid!,
                    },
                });
                if (customerObj && customerObj.id) {
                    customerId = customerObj.id;

                    // Update UserSubscriptionPlan
                    await admin.firestore()
                        .collection(SharedFirestoreCollectionDB.CentralUserData)
                        .doc(userId).set({
                            stripeCustomerId: customerId,
                        }, {merge: true});
                }
            } else {
                customerId = centralUserDataModel.stripeCustomerId;
            }

            if (!customerId) {
                resolve(getQuickResponse(false, null, "Some error occurred while creating your subscription profile. Please, try again our contact us: contact@comaker.ai"));
                return;
            }

            if (!projectDetails.subscription || !projectDetails.subscription.subscriptionId) {
                resolve(getQuickResponse(false, null, "We didn't found any subscription to upgrade"));
                return;
            }


            // Retrieve the existing subscription
            const subscription = await stripeApi.subscriptions.retrieve(projectDetails.subscription.subscriptionId);

            // Assuming the subscription has only one item. Adjust if your case is different
            const currentItemId = subscription.items.data[0].id;

            const stripeResponse: any = await stripeApi.subscriptions.update(projectDetails.subscription.subscriptionId, {
                items: [{
                    id: currentItemId,
                    price: priceId,
                }],
                metadata: {
                    planType: data.planType,
                    projectId: projectDetails.uid,
                },
            });

            if (stripeResponse && stripeResponse.status === "active") {

                // Update user plan -> It's now being handled by webhooks
                /*await admin.firestore()
                    .collection(CustomFirestoreCollectionDB.Web2AppProjects)
                    .doc(userId)
                    .set({

                    }, {merge: true});*/

                resolve(getQuickResponse(true, stripeResponse));
            } else {
                resolve(getQuickResponse(false, stripeResponse));
            }
        });
    });