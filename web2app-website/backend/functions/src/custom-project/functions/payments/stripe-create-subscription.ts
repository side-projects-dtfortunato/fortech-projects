import * as functions from "firebase-functions";
import * as admin from "firebase-admin";
import {ProjectDetailsModel, ProjectSubscriptionPlan} from "../../data/model/project-details.model";
import {PaymentPeriodType, StripeConfigs, stripeDev, stripeProd} from "../../utils/stripe-utils";
import {getDocumentData} from "../../../shareable-firebase-backend/utils/utils";
import {CustomFirestoreCollectionDB} from "../../data/custom-firestore-collection-names";
import {getQuickResponse, getResponse} from "../../../shareable-firebase-backend/model-data/api-response.model";
import {SharedFirestoreCollectionDB} from "../../../shareable-firebase-backend/model-data/shared-firestore-collections";
import {CustomCentralUserDataModel} from "../../data/model/custom-central-user-data.model";
import {WEBAPP_CONFIGS} from "../../../configs/webapp.configs";
import {Log} from "../../../shareable-firebase-backend/utils/log";
import {BackendSharedUtils} from "../../../shareable-firebase-backend/utils/backend-shared-utils";

export const stripeCreateCheckoutSession = functions.https.onCall(
    (data: {planType: ProjectSubscriptionPlan, periodPayment?: PaymentPeriodType, projectId: string, priceId?: string, },
     context) => {
        if (!context.auth || !context.auth.uid) return getResponse(1001, undefined, "User must be logged in to execute this action");
        const userId: string = context && context.auth && context.auth.uid ? context.auth.uid : "";
        Log.onCall(data, userId);

        return new Promise(async (resolve, reject) => {
            const env = BackendSharedUtils.isProjectProduction() ? "PROD" : "DEV";
            let priceId: string | undefined = data.priceId ? data.priceId : StripeConfigs[env].plans[data.planType][data.periodPayment!];
            let checkoutMode: "payment" | "subscription" = "subscription";

            console.log("PriceId: ", priceId);

            // Check if the project has already this plan subscribed
            const projectDetails: ProjectDetailsModel = await getDocumentData(CustomFirestoreCollectionDB.Web2AppProjects, data.projectId);

            if (projectDetails.subscription && projectDetails.subscription.planType === data.planType) {
                resolve(getQuickResponse(false, null, "You already have subscribed this plan"));
                return;
            }


            if (priceId) {
                const stripeApi = env === "PROD" ? stripeProd : stripeDev;

                // First check if the user already have a customerId on stripe
                const centralUserDataModel: CustomCentralUserDataModel = await getDocumentData(SharedFirestoreCollectionDB.CentralUserData, userId);
                let customerId: string = "";
                if (!centralUserDataModel?.stripeCustomerId) {
                    // We should create the customer first
                    let customerObj = await stripeApi.customers.create({
                        email: centralUserDataModel.email,
                        name: centralUserDataModel.publicProfile?.name,
                        metadata: {
                            userId: centralUserDataModel.uid!,
                        },
                    });
                    if (customerObj && customerObj.id) {
                        customerId = customerObj.id;

                        // Update UserSubscriptionPlan
                        await admin.firestore()
                            .collection(SharedFirestoreCollectionDB.CentralUserData)
                            .doc(userId).set({
                                stripeCustomerId: customerId,
                            }, {merge: true});
                    }
                } else {
                    customerId = centralUserDataModel.stripeCustomerId;
                }

                if (!customerId) {
                    resolve(getQuickResponse(false, null, "Some error occurred while creating your subscription profile. Please, try again our contact us: contact@comaker.ai"));
                    return;
                }

                const redirectDomain: string = WEBAPP_CONFIGS.WEBSITE_URL;

                const session = await stripeApi.checkout.sessions.create({
                    mode: checkoutMode,
                    customer: customerId,
                    line_items: [
                        {
                            price: priceId,
                            quantity: 1,
                        },
                    ],
                    subscription_data: {
                        metadata: {
                            planType: data.planType,
                            projectId: projectDetails.uid,
                        }
                    },
                    // {CHECKOUT_SESSION_ID} is a string literal; do not change it!
                    // the actual Session ID is returned in the query parameter when your customer
                    // is redirected to the success page.
                    success_url: redirectDomain + '/payment/success?session_id={CHECKOUT_SESSION_ID}&projectId=' + projectDetails.uid!,
                    cancel_url: redirectDomain + '/payment/cancel?projectId=' + projectDetails.uid!,
                    allow_promotion_codes: true,
                });


                resolve(getQuickResponse(true, {redirectUrl: session.url}));
            } else {
                resolve(getQuickResponse(false, null, "Something failed while subscribing. Try again later or contact contact@comaker.ai"));
            }
        });
    });