import * as functions from "firebase-functions";
import * as admin from "firebase-admin";
import {stripeDev, stripeProd} from "../../utils/stripe-utils";
import {CustomCentralUserDataModel} from "../../data/model/custom-central-user-data.model";
import {getDocumentData} from "../../../shareable-firebase-backend/utils/utils";
import {SharedFirestoreCollectionDB} from "../../../shareable-firebase-backend/model-data/shared-firestore-collections";
import {getQuickResponse, getResponse} from "../../../shareable-firebase-backend/model-data/api-response.model";
import {FrontendRouteUtils} from "../../utils/frontend-route.utils";
import {Log} from "../../../shareable-firebase-backend/utils/log";

export const stripeCreateCustomerPortalSession = functions.https.onCall(
    (data: { env: "PROD" | "DEV", projectId: string},
     context) => {
        if (!context.auth || !context.auth.uid) return getResponse(1001, undefined, "User must be logged in to execute this action");
        const userId: string = context && context.auth && context.auth.uid ? context.auth.uid : "";
        Log.onCall(data, userId);

        return new Promise(async (resolve, reject) => {
            const stripeApi = data.env === "PROD" ? stripeProd : stripeDev;

            // First check if the user already have a customerId on stripe
            let customerId: string = "";
            let centralUserDataModel: CustomCentralUserDataModel = await getDocumentData(SharedFirestoreCollectionDB.CentralUserData, userId);
            if (!centralUserDataModel.stripeCustomerId) {
                // We should create the customer first
                let customerObj = await stripeApi.customers.create({
                    email: centralUserDataModel.email,
                    metadata: {
                        userId: userId,
                    },
                });
                if (customerObj && customerObj.id) {
                    customerId = customerObj.id;

                    // Update UserSubscriptionPlan
                    await admin.firestore()
                        .collection(SharedFirestoreCollectionDB.CentralUserData)
                        .doc(userId).set({
                            stripeCustomerId: customerId,
                        }, {merge: true});
                }

            } else {
                customerId = centralUserDataModel.stripeCustomerId;
            }

            if (!customerId) {
                resolve(getQuickResponse(false, null, "Some error occurred while creating your subscription profile. Please, try again our contact us: contact@comaker.ai"));
                return;
            }

            const returnUrl = FrontendRouteUtils.getProjectDetailsURL(data.projectId, "SUBSCRIPTION");

            const portalSession = await stripeApi.billingPortal.sessions.create({
                customer: customerId,
                return_url: returnUrl,
            });

            resolve(getQuickResponse(true, {redirectUrl: portalSession.url}));
        });
    });