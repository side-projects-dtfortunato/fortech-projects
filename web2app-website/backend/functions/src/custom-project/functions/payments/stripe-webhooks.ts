import * as functions from "firebase-functions";
import * as admin from "firebase-admin";
import {StripeConfigs, stripeDev, stripeProd, StripeUtils} from "../../utils/stripe-utils";
import {ProjectSubscriptionPlan} from "../../data/model/project-details.model";
import {CustomFirestoreCollectionDB, CustomRealtimeCollectionDB} from "../../data/custom-firestore-collection-names";
import {BackendSharedUtils} from "../../../shareable-firebase-backend/utils/backend-shared-utils";
import {RealtimeDbUtils} from "../../../shareable-firebase-backend/utils/realtime-db.utils";

export const stripeWebhook = functions.https.onRequest(async (request,
                                                              response) => {
    const sig = request.headers['stripe-signature'];

    let event;
    const stripeApi = BackendSharedUtils.isProjectProduction() ? stripeProd : stripeDev;
    const webhookKey: string = BackendSharedUtils.isProjectProduction() ? StripeConfigs.PROD.webHookKey : StripeConfigs.DEV.webHookKey;

    try {
        event = stripeApi.webhooks.constructEvent(request.rawBody.toString(), sig, webhookKey);
    } catch (err: any) {
        response.status(400).send(`Webhook Error: ${err.message}`);
        return;
    }

    // Handle the event
    const eventData: any = event.data.object;

    let planType: ProjectSubscriptionPlan;
    let projectId: string;
    let responseData: any = {}
    const env: string = BackendSharedUtils.isProjectProduction() ? "PROD" : "DEV";
    switch (event.type) {
        case 'customer.subscription.created':
        case "customer.subscription.updated":

            // Access the metadata
            let metadata: any = eventData.metadata;
            projectId = metadata.projectId;

            // Get Plan type
            const priceId: string = eventData.plan.id;
            planType = StripeUtils.getPlanTypeByPriceId(env, priceId);

            if (!planType || !projectId) {
                response.status(400).send("Didn't found the planType or projectId");
                return;
            }

            responseData = await updateProjectSubscriptionPlan(projectId, planType!, eventData.id);

            break;

        case "customer.subscription.deleted":
            planType = eventData.metadata.planType as ProjectSubscriptionPlan;
            projectId = eventData.metadata.projectId;
            if (!planType || !projectId) {
                response.status(400).send("Didn't found the planType or projectId");
                return;
            }

            responseData = await updateProjectSubscriptionPlan(projectId, ProjectSubscriptionPlan.FREE_PLAN);
            break;

        case "invoice.voided":
            planType = eventData.subscription_details.metadata.planType as ProjectSubscriptionPlan;
            projectId = eventData.subscription_details.metadata.projectId;
            if (!planType || !projectId) {
                response.status(400).send("Didn't found the planType or projectId");
                return;
            }

            responseData = await updateProjectSubscriptionPlan(projectId, ProjectSubscriptionPlan.FREE_PLAN);
            break;

        case "invoice.payment_failed":
            planType = eventData.subscription_details.metadata.planType as ProjectSubscriptionPlan;
            projectId = eventData.subscription_details.metadata.projectId;
            if (!planType || !projectId) {
                response.status(400).send("Didn't found the planType or projectId");
                return;
            }

            responseData = await updateProjectSubscriptionPlan(projectId, ProjectSubscriptionPlan.FREE_PLAN);
            break;

        default:
        // Unhandled event type
    }

    // Return a 200 response to acknowledge receipt of the event
    response.send(JSON.stringify(responseData));
});


async function updateProjectSubscriptionPlan(projectId: string, planType: ProjectSubscriptionPlan, subscriptionId?: string) {

    await RealtimeDbUtils.writeData({
        collectionName: CustomRealtimeCollectionDB.PublishedAppStyle,
        docId: projectId,
        data: {
            subscriptionPlan: planType,
        },
        merge: true,
    });

    return await admin.firestore()
        .collection(CustomFirestoreCollectionDB.Web2AppProjects)
        .doc(projectId)
        .set({
            subscription: {
                planType: planType,
                updatedAt: Date.now(),
                subscriptionId: subscriptionId ? subscriptionId : admin.firestore.FieldValue.delete(),
            },
            updatedAt: Date.now(),
        }, {merge: true});
}


