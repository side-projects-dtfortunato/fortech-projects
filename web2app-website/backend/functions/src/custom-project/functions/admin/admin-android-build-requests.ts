import * as functions from "firebase-functions";
import * as admin from "firebase-admin";
import {getQuickResponse} from "../../../shareable-firebase-backend/model-data/api-response.model";
import {Log} from "../../../shareable-firebase-backend/utils/log";
import {
    SharedCentralUserDataModel,
    UserRole
} from "../../../shareable-firebase-backend/model-data/frontend-model-data/shared-central-user-data.model";
import {getDocumentData} from "../../../shareable-firebase-backend/utils/utils";
import {SharedFirestoreCollectionDB} from "../../../shareable-firebase-backend/model-data/shared-firestore-collections";
import {CustomFirestoreCollectionDB} from "../../data/custom-firestore-collection-names";
import {ProjectAndroidBuildRequestModel} from "../../data/model/project-android-build-request.model";
import {StorageFileModel} from "../../../shareable-firebase-backend/model-data/storage-file.model";
import {ProjectBuildStatusType, ProjectPlatformTypes} from "../../data/model/project-details.model";
import {AdminUtils} from "../../utils/admin.utils";

export const adminGetAndroidBuildRequests = functions.https.onCall(
    async (data: {},
           context) => {
        if (!context.auth || !context.auth.uid) return getQuickResponse(false, undefined, "User not authorized");
        const userId = context && context.auth && context.auth.uid ? context.auth.uid : "";
        Log.onCall(data, userId);

        // Get User Profile
        let userProfile: SharedCentralUserDataModel = await getDocumentData<SharedCentralUserDataModel>(SharedFirestoreCollectionDB.CentralUserData, userId);
        if (userProfile.userRole !== UserRole.ADMIN) {
            return getQuickResponse(false, null, "You're not authorized");
        }

        // Get Build Android requests
        let listAndroidBuildRequests: ProjectAndroidBuildRequestModel[] = await getAndroidBuildRequests();

        return getQuickResponse(true, listAndroidBuildRequests);
    });


export const adminCompleteAndroidBuildRequest = functions.https.onCall(
    async (data: { projectId: string, buildRequestId: string, appPackage: StorageFileModel, message: string},
           context) => {
        if (!context.auth || !context.auth.uid) return getQuickResponse(false, undefined, "User not authorized");
        const userId = context && context.auth && context.auth.uid ? context.auth.uid : "";
        Log.onCall(data, userId);

        // Get User Profile
        let userProfile: SharedCentralUserDataModel = await getDocumentData<SharedCentralUserDataModel>(SharedFirestoreCollectionDB.CentralUserData, userId);
        if (userProfile.userRole !== UserRole.ADMIN) {
            return getQuickResponse(false, null, "You're not authorized");
        }

        const batch = admin.firestore().batch();

        // Update Build Request
        const androidBuildRequest: ProjectAndroidBuildRequestModel = await getDocumentData<ProjectAndroidBuildRequestModel>(CustomFirestoreCollectionDB.ProjectAndroidBuildRequests, data.buildRequestId);
        androidBuildRequest.buildReleaseFile = data.appPackage;
        androidBuildRequest.status = ProjectBuildStatusType.BUILD_COMPLETED;
        androidBuildRequest.updatedAt = Date.now();
        androidBuildRequest.displayMessage = data.message;

        batch.set(admin.firestore().collection(CustomFirestoreCollectionDB.ProjectAndroidBuildRequests).doc(data.buildRequestId),
            androidBuildRequest, {merge: true});

        let projectUpdate: any = {};
        projectUpdate = {
            ...projectUpdate,
            lastBuilds: {
                ANDROID: androidBuildRequest,
            },
            inProgressBuilds: {
                ANDROID: admin.firestore.FieldValue.delete(),
            },
        };

        batch.set(admin.firestore().collection(CustomFirestoreCollectionDB.Web2AppProjects).doc(data.projectId),
            projectUpdate, {merge: true});


        await batch.commit();


        // Send email
        await AdminUtils.sendAdminEmailBuildResponse({
            status: ProjectBuildStatusType.BUILD_COMPLETED,
            projectDetails: androidBuildRequest.projectSnapshot,
            platform: ProjectPlatformTypes.ANDROID,
        });

        return getQuickResponse(true);
    });

export const adminAndroidRejectBuild = functions.https.onCall(
    async (data: { projectId: string, buildRequestId: string, message: string },
           context) => {
        if (!context.auth || !context.auth.uid) return getQuickResponse(false, undefined, "User not authorized");
        const userId = context && context.auth && context.auth.uid ? context.auth.uid : "";
        Log.onCall(data, userId);

        // Get User Profile
        let userProfile: SharedCentralUserDataModel = await getDocumentData<SharedCentralUserDataModel>(SharedFirestoreCollectionDB.CentralUserData, userId);
        if (userProfile.userRole !== UserRole.ADMIN) {
            return getQuickResponse(false, null, "You're not authorized");
        }

        const batch = admin.firestore().batch();

        // Update Build Request
        const androidBuildRequest: ProjectAndroidBuildRequestModel = await getDocumentData<ProjectAndroidBuildRequestModel>(CustomFirestoreCollectionDB.ProjectAndroidBuildRequests, data.buildRequestId);
        androidBuildRequest.status = ProjectBuildStatusType.BUILD_FAILED;
        androidBuildRequest.displayMessage = data.message;
        androidBuildRequest.updatedAt = Date.now();

        batch.set(admin.firestore().collection(CustomFirestoreCollectionDB.ProjectAndroidBuildRequests).doc(data.buildRequestId),
            androidBuildRequest, {merge: true});

        let projectUpdate: any = {
            inProgressBuilds: {
                ANDROID: androidBuildRequest,
            },
        };

        batch.set(admin.firestore().collection(CustomFirestoreCollectionDB.Web2AppProjects).doc(data.projectId),
            projectUpdate, {merge: true});


        await batch.commit();

        // Send email
        await AdminUtils.sendAdminEmailBuildResponse({
            displayMessage: data.message,
            status: ProjectBuildStatusType.BUILD_FAILED,
            projectDetails: androidBuildRequest.projectSnapshot,
            platform: ProjectPlatformTypes.ANDROID,
        });

        return getQuickResponse(true);
    });


async function getAndroidBuildRequests() {
    const collectionRef = admin.firestore().collection(CustomFirestoreCollectionDB.ProjectAndroidBuildRequests);

    // Query documents where the "status" field is equal to "IN_PROGRESS"
    const querySnapshot = await collectionRef
        .where('status', '==', 'IN_PROGRESS')
        .get();

    const inProgressDocuments: ProjectAndroidBuildRequestModel[] = [];

    querySnapshot.forEach((doc) => {
        // Add each document's data to the array
        inProgressDocuments.push(doc.data() as ProjectAndroidBuildRequestModel);
    });

    return inProgressDocuments;
}

