import * as functions from "firebase-functions";
import {getQuickResponse} from "../../../shareable-firebase-backend/model-data/api-response.model";
import {Log} from "../../../shareable-firebase-backend/utils/log";
import {
    SharedCentralUserDataModel,
    UserRole
} from "../../../shareable-firebase-backend/model-data/frontend-model-data/shared-central-user-data.model";
import {getDocumentData} from "../../../shareable-firebase-backend/utils/utils";
import {SharedFirestoreCollectionDB} from "../../../shareable-firebase-backend/model-data/shared-firestore-collections";
import * as admin from "firebase-admin";
import {CustomFirestoreCollectionDB} from "../../data/custom-firestore-collection-names";
import {ProjectIOSBuildRequestModel} from "../../data/model/project-ios-build-request.model";
import {ProjectBuildStatusType, ProjectPlatformTypes} from "../../data/model/project-details.model";
import {AdminUtils} from "../../utils/admin.utils";

export const adminGetiOSBuildRequests = functions.https.onCall(
    async (data: {},
           context) => {
        if (!context.auth || !context.auth.uid) return getQuickResponse(false, undefined, "User not authorized");
        const userId = context && context.auth && context.auth.uid ? context.auth.uid : "";
        Log.onCall(data, userId);

        // Get User Profile
        let userProfile: SharedCentralUserDataModel = await getDocumentData<SharedCentralUserDataModel>(SharedFirestoreCollectionDB.CentralUserData, userId);
        if (userProfile.userRole !== UserRole.ADMIN) {
            return getQuickResponse(false, null, "You're not authorized");
        }

        // Get Build iOS requests
        let listIOSBuildRequests: ProjectIOSBuildRequestModel[] = await getIOSBuildRequests();

        return getQuickResponse(true, listIOSBuildRequests);
    });





export const adminCompleteIOSBuildRequest = functions.https.onCall(
    async (data: {projectId: string, buildRequestId: string, message: string},
           context) => {
        if (!context.auth || !context.auth.uid) return getQuickResponse(false, undefined, "User not authorized");
        const userId = context && context.auth && context.auth.uid ? context.auth.uid : "";
        Log.onCall(data, userId);

        // Get User Profile
        let userProfile: SharedCentralUserDataModel = await getDocumentData<SharedCentralUserDataModel>(SharedFirestoreCollectionDB.CentralUserData, userId);
        if (userProfile.userRole !== UserRole.ADMIN) {
            return getQuickResponse(false, null, "You're not authorized");
        }

        const batch = admin.firestore().batch();

        // Update Build Request
        const iosBuildRequestModel: ProjectIOSBuildRequestModel = await getDocumentData<ProjectIOSBuildRequestModel>(CustomFirestoreCollectionDB.ProjectiOSBuildRequests, data.buildRequestId);
        iosBuildRequestModel.status = ProjectBuildStatusType.BUILD_COMPLETED;
        iosBuildRequestModel.updatedAt = Date.now();
        iosBuildRequestModel.displayMessage = data.message;

        batch.set(admin.firestore().collection(CustomFirestoreCollectionDB.ProjectiOSBuildRequests).doc(data.buildRequestId),
            iosBuildRequestModel, {merge: true});

        let projectUpdate: any = {};
        projectUpdate = {
            ...projectUpdate,
            lastBuilds: {
                IOS: iosBuildRequestModel,
            },
            inProgressBuilds: {
                IOS: admin.firestore.FieldValue.delete(),
            },
        };

        batch.set(admin.firestore().collection(CustomFirestoreCollectionDB.Web2AppProjects).doc(data.projectId),
            projectUpdate, {merge: true});


        await batch.commit();

        // Send email
        await AdminUtils.sendAdminEmailBuildResponse({
            status: ProjectBuildStatusType.BUILD_COMPLETED,
            projectDetails: iosBuildRequestModel.projectSnapshot!,
            platform: ProjectPlatformTypes.IOS,
        });

        return getQuickResponse(true);
    });

export const adminIOSRejectBuild = functions.https.onCall(
    async (data: {projectId: string, buildRequestId: string, message: string},
           context) => {
        if (!context.auth || !context.auth.uid) return getQuickResponse(false, undefined, "User not authorized");
        const userId = context && context.auth && context.auth.uid ? context.auth.uid : "";
        Log.onCall(data, userId);

        // Get User Profile
        let userProfile: SharedCentralUserDataModel = await getDocumentData<SharedCentralUserDataModel>(SharedFirestoreCollectionDB.CentralUserData, userId);
        if (userProfile.userRole !== UserRole.ADMIN) {
            return getQuickResponse(false, null, "You're not authorized");
        }

        const batch = admin.firestore().batch();

        // Update Build Request
        const iosBuildRequestModel: ProjectIOSBuildRequestModel = await getDocumentData<ProjectIOSBuildRequestModel>(CustomFirestoreCollectionDB.ProjectiOSBuildRequests, data.buildRequestId);
        iosBuildRequestModel.status = ProjectBuildStatusType.BUILD_FAILED;
        iosBuildRequestModel.displayMessage = data.message;
        iosBuildRequestModel.updatedAt = Date.now();

        batch.set(admin.firestore().collection(CustomFirestoreCollectionDB.ProjectiOSBuildRequests).doc(data.buildRequestId),
            iosBuildRequestModel, {merge: true});

        let projectUpdate: any = {
            inProgressBuilds: {
                IOS: iosBuildRequestModel,
            },
        };

        batch.set(admin.firestore().collection(CustomFirestoreCollectionDB.Web2AppProjects).doc(data.projectId),
            projectUpdate, {merge: true});


        await batch.commit();

        // Send email
        await AdminUtils.sendAdminEmailBuildResponse({
            displayMessage: data.message,
            status: ProjectBuildStatusType.BUILD_FAILED,
            projectDetails: iosBuildRequestModel.projectSnapshot!,
            platform: ProjectPlatformTypes.IOS,
        });

        return getQuickResponse(true);
    });


async function getIOSBuildRequests() {
    const collectionRef = admin.firestore().collection(CustomFirestoreCollectionDB.ProjectiOSBuildRequests);

    // Query documents where the "status" field is equal to "IN_PROGRESS"
    const querySnapshot = await collectionRef
        .where('status', '==', 'IN_PROGRESS').get();

    const inProgressDocuments: ProjectIOSBuildRequestModel[] = [];

    querySnapshot.forEach((doc) => {
        // Add each document's data to the array
        inProgressDocuments.push(doc.data() as ProjectIOSBuildRequestModel);
    });

    return inProgressDocuments;
}