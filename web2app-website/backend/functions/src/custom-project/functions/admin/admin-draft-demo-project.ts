import * as functions from "firebase-functions";
import {ProjectDetailsModel} from "../../data/model/project-details.model";
import {getQuickResponse} from "../../../shareable-firebase-backend/model-data/api-response.model";
import {Log} from "../../../shareable-firebase-backend/utils/log";
import {
    SharedCentralUserDataModel, UserRole
} from "../../../shareable-firebase-backend/model-data/frontend-model-data/shared-central-user-data.model";
import {getDocumentData} from "../../../shareable-firebase-backend/utils/utils";
import {SharedFirestoreCollectionDB} from "../../../shareable-firebase-backend/model-data/shared-firestore-collections";
import {CustomRealtimeCollectionDB} from "../../data/custom-firestore-collection-names";
import {RealtimeDbUtils} from "../../../shareable-firebase-backend/utils/realtime-db.utils";

export const adminDraftDemoProject = functions.https.onCall(
    async (data: {projectData: ProjectDetailsModel, contactEmail?: string},
     context) => {
        if (!context.auth || !context.auth.uid) return getQuickResponse(false, undefined, "User not authorized");
        const userId = context && context.auth && context.auth.uid ? context.auth.uid : "";
        Log.onCall(data, userId);

        // Check if is Admin
        let userProfile: SharedCentralUserDataModel = await getDocumentData<SharedCentralUserDataModel>(SharedFirestoreCollectionDB.CentralUserData, userId);
        if (userProfile.userRole !== UserRole.ADMIN) {
            return getQuickResponse(false, null, "You're not authorized");
        }

        let projectDetails: ProjectDetailsModel = {
            ...data.projectData,
            userId: userId,
            authorization: {
                allRead: true,
                allWrite: true,
            },
            createdAt: Date.now(),
            updatedAt: Date.now(),
        };


        await RealtimeDbUtils.writeData({
            collectionName: CustomRealtimeCollectionDB.DraftDemoProjects,
            docId: projectDetails.uid!,
            data: projectDetails,
            merge: false,
        });
        return getQuickResponse(true, projectDetails);
    });


export const adminDeleteDraftDemoProject = functions.https.onCall(
    async (data: {projectUid: string},
     context) => {
        if (!context.auth || !context.auth.uid) return getQuickResponse(false, undefined, "User not authorized");
        const userId = context && context.auth && context.auth.uid ? context.auth.uid : "";
        Log.onCall(data, userId);

        // Check if is Admin
        let userProfile: SharedCentralUserDataModel = await getDocumentData<SharedCentralUserDataModel>(SharedFirestoreCollectionDB.CentralUserData, userId);
        if (userProfile.userRole !== UserRole.ADMIN) {
            return getQuickResponse(false, null, "You're not authorized");
        }

        await RealtimeDbUtils.deleteData({
            collectionName: CustomRealtimeCollectionDB.DraftDemoProjects,
            docId: data.projectUid,
        });
        return;
    });


