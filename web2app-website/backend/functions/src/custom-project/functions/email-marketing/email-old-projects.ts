import * as functions from "firebase-functions";
import * as admin from "firebase-admin";
import {CustomFirestoreCollectionDB} from "../../data/custom-firestore-collection-names";
import {ProjectDetailsModel} from "../../data/model/project-details.model";
import {EmailMarketingUtils} from "../../utils/email-marketing.utils";
import {firestore} from "firebase-admin";
import WriteBatch = firestore.WriteBatch;
import {getQuickResponse} from "../../../shareable-firebase-backend/model-data/api-response.model";
import {BackendSharedUtils} from "../../../shareable-firebase-backend/utils/backend-shared-utils";


export const updateProjectsWithHasBuiltField = functions.https.onRequest(async (request,
                                                                              response) => {
    const querySnapshot = await admin.firestore()
        .collection(CustomFirestoreCollectionDB.Web2AppProjects).get();

    let listProjects: ProjectDetailsModel[] = [];

    if (!querySnapshot.empty) {
        querySnapshot.docs.forEach((doc) => {
            listProjects.push({
                ...doc.data() as ProjectDetailsModel,
                uid: doc.id,
            });
        });

        const batch = admin.firestore().batch();

        listProjects.forEach((projectData) => {
            batch.set(admin.firestore()
                .collection(CustomFirestoreCollectionDB.Web2AppProjects).doc(projectData.uid!), {
                hasBuilt: (projectData.lastBuilds ? true : false),
                emailMarketing: projectData.emailMarketing ? projectData.emailMarketing : {
                    expiringProjectNotif: false,
                    notFinishedNotif: false,
                },
            }, {merge: true});
        });
        await batch.commit();
    }
    response.send(getQuickResponse(true));
});

export const scanOldProjectsEmailMarketing = functions.https.onRequest(async (request,
                                                                response) => {
    const batch = admin.firestore().batch();

    await sendProjectNotFinished(batch);

    await sendProjectIsExpiring(batch);

    await removeExpiredProjects(batch);

    const dbResponse = await batch.commit();

    response.send(getQuickResponse(true, dbResponse));
});

async function sendProjectNotFinished(batch: WriteBatch) {

    // Get the timestamp for 3 days ago
    const oldDate = Date.now() - 3 * 24 * 60 * 60 * 1000;

    const querySnapshot = await admin.firestore()
        .collection(CustomFirestoreCollectionDB.Web2AppProjects)
        .where('updatedAt', '<', oldDate)
        .where('hasBuilt', '==', false)
        .get();

    let listProjects: ProjectDetailsModel[] = [];

    if (!querySnapshot.empty) {
        querySnapshot.docs.filter((doc) => {
            const data = doc.data();
            return !data.emailMarketing || !data.emailMarketing.notFinishedNotif;
        }).forEach((doc) => {
            listProjects.push(doc.data() as ProjectDetailsModel);
        });
    }

    if (listProjects.length > 0) {

        for (let i = 0; i < listProjects.length; i++) {
            const projectData: ProjectDetailsModel = listProjects[i];

            await EmailMarketingUtils.sendProjectNotFinished(projectData);

            batch
                .set(admin.firestore().collection(CustomFirestoreCollectionDB.Web2AppProjects).doc(projectData.uid!), {
                    emailMarketing: {
                        notFinishedNotif: true,
                    },
                }, {merge: true});
        }

    }

}


async function sendProjectIsExpiring(batch: WriteBatch) {

    // Get the timestamp for 25 days ago
    const oldDate = Date.now() - 25 * 24 * 60 * 60 * 1000;

    const querySnapshot = await admin.firestore()
        .collection(CustomFirestoreCollectionDB.Web2AppProjects)
        .where('updatedAt', '<', oldDate)
        .where('hasBuilt', '==', false)
        .get();

    let listProjects: ProjectDetailsModel[] = [];

    if (!querySnapshot.empty) {
        querySnapshot.docs.filter((doc) => {
            const data = doc.data();
            return !data.emailMarketing || !data.emailMarketing.expiringProjectNotif;
        }).forEach((doc) => {
            listProjects.push(doc.data() as ProjectDetailsModel);
        });
    }

    if (listProjects.length > 0) {
        for (let i = 0; i < listProjects.length; i++) {
            const projectData: ProjectDetailsModel = listProjects[i];

            await EmailMarketingUtils.sendProjectIsExpiring(projectData);

            batch
                .set(admin.firestore().collection(CustomFirestoreCollectionDB.Web2AppProjects).doc(projectData.uid!), {
                    emailMarketing: {
                        expiringProjectNotif: true,
                    },
                }, {merge: true});
        }
    }
}


async function removeExpiredProjects(batch: WriteBatch) {

    // Get the timestamp for 30 days ago
    const oldDate = Date.now() - 30 * 24 * 60 * 60 * 1000;

    const querySnapshot = await admin.firestore()
        .collection(CustomFirestoreCollectionDB.Web2AppProjects)
        .where('updatedAt', '<', oldDate)
        .where('hasBuilt', '==', false)
        .get();

    let listProjects: ProjectDetailsModel[] = [];

    if (!querySnapshot.empty) {
        querySnapshot.docs.filter((doc) => {
            const data = doc.data();
            return data.emailMarketing && data.emailMarketing.expiringProjectNotif;
        }).forEach((doc) => {
            listProjects.push(doc.data() as ProjectDetailsModel);
        });
    }

    if (listProjects.length > 0) {

        for (let i = 0; i < listProjects.length; i++) {
            const projectData: ProjectDetailsModel = listProjects[i];

            await EmailMarketingUtils.sendProjectExpiredRemoved(projectData);

            // Android
            const androidBuildSnapshots = await admin.firestore().collection(CustomFirestoreCollectionDB.ProjectAndroidBuildRequests)
                .where('projectId', '==', projectData.uid!).get();
            androidBuildSnapshots.forEach(doc => {
                batch.delete(doc.ref);
            });

            // iOS
            const iosBuildSnapshots = await admin.firestore().collection(CustomFirestoreCollectionDB.ProjectiOSBuildRequests)
                .where('projectId', '==', projectData.uid!).get();
            iosBuildSnapshots.forEach(doc => {
                batch.delete(doc.ref);
            });

            // Delete storage folder
            await BackendSharedUtils.deleteFolderInStorage("projects/" + projectData.uid! + "/");

            // Delete Project
            batch.delete(admin.firestore().collection(CustomFirestoreCollectionDB.Web2AppProjects).doc(projectData.uid!));
        }
    }
}