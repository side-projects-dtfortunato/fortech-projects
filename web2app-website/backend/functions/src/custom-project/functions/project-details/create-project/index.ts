import * as functions from "firebase-functions";
import * as admin from "firebase-admin";
import {getQuickResponse} from "../../../../shareable-firebase-backend/model-data/api-response.model";
import {Log} from "../../../../shareable-firebase-backend/utils/log";
import {ProjectDetailsModel} from "../../../data/model/project-details.model";
import {CustomFirestoreCollectionDB, CustomRealtimeCollectionDB} from "../../../data/custom-firestore-collection-names";
import {
    SharedCentralUserDataModel
} from "../../../../shareable-firebase-backend/model-data/frontend-model-data/shared-central-user-data.model";
import {getDocumentData} from "../../../../shareable-firebase-backend/utils/utils";
import {
    SharedFirestoreCollectionDB
} from "../../../../shareable-firebase-backend/model-data/shared-firestore-collections";
import {RealtimeDbUtils} from "../../../../shareable-firebase-backend/utils/realtime-db.utils";
import {BackendSharedUtils} from "../../../../shareable-firebase-backend/utils/backend-shared-utils";



export const web2appCreateProject = functions.https.onCall(
    (data: {projectData: ProjectDetailsModel, createdFromDraft?: boolean},
     context) => {
        if (!context.auth || !context.auth.uid) return getQuickResponse(false, undefined, "User not authorized");
        const userId = context && context.auth && context.auth.uid ? context.auth.uid : "";
        Log.onCall(data, userId);

        return new Promise(async (resolve, reject) => {

            // appPackagedId
            let appPackagedId: string = BackendSharedUtils.extractDomain(data.projectData.url!);

            // Get User details
            const centralUserData: SharedCentralUserDataModel = await getDocumentData(SharedFirestoreCollectionDB.CentralUserData, userId);

            let projectDetails: ProjectDetailsModel = {
                ...data.projectData,
                androidConfigs: {
                    appId: appPackagedId,
                    isEditable: false,
                    versionCode: 1,
                    versionName: "1.0.0",
                    keyStoreAlias: "webtoapp",
                    keyStorePassword: "webtoapp",
                },
                iosConfigs: {
                    appId: appPackagedId,
                    isEditable: false,
                    versionName: "1.0.0",
                    versionCode: 1,
                },
                userId: userId,
                authorization: {
                    readUIDs: [userId],
                    writeUIDs: [userId],
                },
                createdAt: Date.now(),
                updatedAt: Date.now(),
                settings: {
                    contactEmail: centralUserData.email!,
                    receiveEmailAlerts: true,
                },
                emailMarketing: {
                    expiringProjectNotif: false,
                    notFinishedNotif: false,
                },
            };


            const batch = admin.firestore().batch();

            batch.set(admin.firestore()
                .collection(CustomFirestoreCollectionDB.Web2AppProjects).doc(projectDetails.uid!), projectDetails);

            batch.commit().then(async (res) => {
                // Remove from draft
                await RealtimeDbUtils.deleteData({
                    collectionName: CustomRealtimeCollectionDB.DraftDemoProjects,
                    docId: projectDetails.uid!,
                });

                resolve(getQuickResponse(true, projectDetails));
            }).catch((err) => {
                resolve(getQuickResponse(false, err));
            });
            return;
        });
    });

export const getDemoProject = functions.https.onCall(
    async (data: {demoId: string},
     context) => {
        const userId = context && context.auth && context.auth.uid ? context.auth.uid : "";
        Log.onCall(data, userId);

        let projectData = await RealtimeDbUtils.getData({
            collectionName: CustomRealtimeCollectionDB.DraftDemoProjects,
            docId: data.demoId,
        });

        if (projectData) {
            return getQuickResponse(true, projectData);
        } else {
            return getQuickResponse(false);
        }
    });