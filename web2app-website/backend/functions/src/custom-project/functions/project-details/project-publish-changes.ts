import * as functions from "firebase-functions";
import * as admin from "firebase-admin";
import {ProjectDetailsModel, ProjectDetailsUtils} from "../../data/model/project-details.model";
import {getQuickResponse} from "../../../shareable-firebase-backend/model-data/api-response.model";
import {Log} from "../../../shareable-firebase-backend/utils/log";
import {getDocumentData} from "../../../shareable-firebase-backend/utils/utils";
import {CustomFirestoreCollectionDB, CustomRealtimeCollectionDB} from "../../data/custom-firestore-collection-names";
import {ProjectPublishedAppStyleModel} from "../../data/model/project-published-app-style.model";
import {RealtimeDbUtils} from "../../../shareable-firebase-backend/utils/realtime-db.utils";

export const projectPublishChanges = functions.https.onCall(
    async (data: { projectId: string },
           context) => {
        if (!context.auth || !context.auth.uid) return getQuickResponse(false, undefined, "User not authorized");
        const userId = context && context.auth && context.auth.uid ? context.auth.uid : "";
        Log.onCall(data, userId);

        // Get project
        const projectData: ProjectDetailsModel = await getDocumentData(CustomFirestoreCollectionDB.Web2AppProjects, data.projectId);

        if (projectData.userId !== userId) {
            return getQuickResponse(false, null, "Not authorized");
        }
        if (!projectData.subscription || !projectData.subscription.planType) {
            return getQuickResponse(false, null, "Subscription plan not found");
        }

        const publishData: ProjectPublishedAppStyleModel = ProjectDetailsUtils.getProjectPublishAppStyle(projectData);

        await RealtimeDbUtils.writeData({
            collectionName: CustomRealtimeCollectionDB.PublishedAppStyle,
            docId: data.projectId,
            data: publishData,
            merge: false,
        });

        await admin.firestore().collection(CustomFirestoreCollectionDB.Web2AppProjects)
            .doc(data.projectId)
            .set({
                hasChangesPublished: true,
                publishedData: publishData,
            }, {merge: true});

        return getQuickResponse(true, publishData);
    });