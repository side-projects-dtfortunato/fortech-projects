import * as functions from "firebase-functions";
import * as admin from "firebase-admin";
import {getQuickResponse} from "../../../shareable-firebase-backend/model-data/api-response.model";
import {Log} from "../../../shareable-firebase-backend/utils/log";
import {ProjectDetailsModel, ProjectPlatformTypes} from "../../data/model/project-details.model";
import {getDocumentData} from "../../../shareable-firebase-backend/utils/utils";
import {CustomFirestoreCollectionDB} from "../../data/custom-firestore-collection-names";
import {ProjectIOSBuildRequestModel} from "../../data/model/project-ios-build-request.model";
import {ProjectAndroidBuildRequestModel} from "../../data/model/project-android-build-request.model";

export const web2appProjectCancelBuild = functions.https.onCall(
    (data: { projectId: string, platformType: ProjectPlatformTypes },
     context) => {
        if (!context.auth || !context.auth.uid) return getQuickResponse(false, undefined, "User not authorized");
        const userId = context && context.auth && context.auth.uid ? context.auth.uid : "";
        Log.onCall(data, userId);

        return new Promise(async (resolve, reject) => {
            let projectDetails: ProjectDetailsModel = await getDocumentData<ProjectDetailsModel>(CustomFirestoreCollectionDB.Web2AppProjects, data.projectId);

            if (!projectDetails.inProgressBuilds || !projectDetails.inProgressBuilds[data.platformType]) {
                resolve(getQuickResponse(false, null, "Project build is not in progress..."));
                return;
            }


            const batch = admin.firestore().batch();

            batch.set(admin.firestore()
                .collection(CustomFirestoreCollectionDB.Web2AppProjects).doc(data.projectId), {
                inProgressBuilds: {
                    [data.platformType]: admin.firestore.FieldValue.delete(),
                },
            }, {merge: true});

            if (data.platformType === ProjectPlatformTypes.IOS) {
                    const inProgressBuildRequest: ProjectIOSBuildRequestModel = projectDetails.inProgressBuilds![data.platformType] as ProjectIOSBuildRequestModel;

                    batch.delete(admin.firestore()
                        .collection(CustomFirestoreCollectionDB.ProjectiOSBuildRequests).doc(inProgressBuildRequest.uid!));
            } else if (data.platformType === ProjectPlatformTypes.ANDROID) {
                    const inProgressBuildRequest: ProjectAndroidBuildRequestModel = projectDetails.inProgressBuilds![data.platformType] as ProjectAndroidBuildRequestModel;

                    batch.delete(admin.firestore()
                        .collection(CustomFirestoreCollectionDB.ProjectAndroidBuildRequests).doc(inProgressBuildRequest.uid!));
            }

            await batch.commit();

            projectDetails = await getDocumentData(CustomFirestoreCollectionDB.Web2AppProjects, data.projectId);

            resolve(getQuickResponse(true, projectDetails));
        });
    });