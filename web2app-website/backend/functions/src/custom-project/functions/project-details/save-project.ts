import * as functions from "firebase-functions";
import {ProjectDetailsModel} from "../../data/model/project-details.model";
import {getQuickResponse} from "../../../shareable-firebase-backend/model-data/api-response.model";
import {Log} from "../../../shareable-firebase-backend/utils/log";
import * as admin from "firebase-admin";
import {CustomFirestoreCollectionDB} from "../../data/custom-firestore-collection-names";
import {StorageFileModel} from "../../../shareable-firebase-backend/model-data/storage-file.model";
import {getDocumentData} from "../../../shareable-firebase-backend/utils/utils";

export const web2appSaveProjectData = functions.https.onCall(
    (data: { projectData: ProjectDetailsModel },
     context) => {
        if (!context.auth || !context.auth.uid) return getQuickResponse(false, undefined, "User not authorized");
        const userId = context && context.auth && context.auth.uid ? context.auth.uid : "";
        Log.onCall(data, userId);

        return new Promise(async (resolve, reject) => {
            const batch = admin.firestore().batch();

            data.projectData.updatedAt = Date.now();
            data.projectData.hasChangesPublished = false;

            batch.set(admin.firestore()
                .collection(CustomFirestoreCollectionDB.Web2AppProjects).doc(data.projectData.uid!), data.projectData);

            batch.commit().then((res) => {
                resolve(getQuickResponse(true, data.projectData));
            }).catch((err) => {
                resolve(getQuickResponse(false, err));
            });
        });
    });

export const updatedAndroidKeystoreProject = functions.https.onCall(
    async (data: { projectId: string, fileData: StorageFileModel },
           context) => {
        if (!context.auth || !context.auth.uid) return getQuickResponse(false, undefined, "User not authorized");
        const userId = context && context.auth && context.auth.uid ? context.auth.uid : "";
        Log.onCall(data, userId);

        const projectData: ProjectDetailsModel = await getDocumentData<ProjectDetailsModel>(CustomFirestoreCollectionDB.Web2AppProjects, data.projectId);

        projectData.androidConfigs.keyStore = data.fileData;

        const batch = admin.firestore().batch();

        batch.set(admin.firestore().collection(CustomFirestoreCollectionDB.Web2AppProjects).doc(data.projectId), projectData);

        // Check if should update a build in progress
        if (projectData.inProgressBuilds && projectData.inProgressBuilds.ANDROID) {
            batch.set(admin.firestore().collection(CustomFirestoreCollectionDB.ProjectAndroidBuildRequests)
                .doc(projectData.inProgressBuilds.ANDROID.uid!), {
                projectSnapshot: {
                    ...projectData,
                    inProgressBuilds: {},
                    lastBuilds: {},
                },
                buildConfigs: projectData.androidConfigs,
            }, {merge: true});
        }

        await batch.commit();

        return getQuickResponse(true, projectData);
    });

export const updateiOSProvisioningProfileCert = functions.https.onCall(
    async (data: { projectId: string, fileData: StorageFileModel },
           context) => {
        if (!context.auth || !context.auth.uid) return getQuickResponse(false, undefined, "User not authorized");
        const userId = context && context.auth && context.auth.uid ? context.auth.uid : "";
        Log.onCall(data, userId);

        const projectData: ProjectDetailsModel = await getDocumentData<ProjectDetailsModel>(CustomFirestoreCollectionDB.Web2AppProjects, data.projectId);

        projectData.iosConfigs.provisioningProfileCertificate = data.fileData;

        const batch = admin.firestore().batch();

        batch.set(admin.firestore().collection(CustomFirestoreCollectionDB.Web2AppProjects).doc(data.projectId), projectData);

        // Check if should update a build in progress
        if (projectData.inProgressBuilds && projectData.inProgressBuilds.IOS) {
            batch.set(admin.firestore().collection(CustomFirestoreCollectionDB.ProjectiOSBuildRequests)
                .doc(projectData.inProgressBuilds.IOS.uid!), {
                projectSnapshot: {
                    ...projectData,
                    inProgressBuilds: {},
                    lastBuilds: {},
                },
                buildConfigs: projectData.androidConfigs,
            }, {merge: true});
        }

        await batch.commit();

        return getQuickResponse(true, projectData);
    });


export const updateiOSDistributionCert = functions.https.onCall(
    async (data: { projectId: string, fileData: StorageFileModel },
           context) => {
        if (!context.auth || !context.auth.uid) return getQuickResponse(false, undefined, "User not authorized");
        const userId = context && context.auth && context.auth.uid ? context.auth.uid : "";
        Log.onCall(data, userId);

        const projectData: ProjectDetailsModel = await getDocumentData<ProjectDetailsModel>(CustomFirestoreCollectionDB.Web2AppProjects, data.projectId);

        projectData.iosConfigs.distributionCertificateFile = data.fileData;

        const batch = admin.firestore().batch();

        batch.set(admin.firestore().collection(CustomFirestoreCollectionDB.Web2AppProjects).doc(data.projectId), projectData);

        // Check if should update a build in progress
        if (projectData.inProgressBuilds && projectData.inProgressBuilds.IOS) {
            batch.set(admin.firestore().collection(CustomFirestoreCollectionDB.ProjectiOSBuildRequests)
                .doc(projectData.inProgressBuilds.IOS.uid!), {
                projectSnapshot: {
                    ...projectData,
                    inProgressBuilds: {},
                    lastBuilds: {},
                },
                buildConfigs: projectData.androidConfigs,
            }, {merge: true});
        }

        await batch.commit();

        return getQuickResponse(true, projectData);
    });