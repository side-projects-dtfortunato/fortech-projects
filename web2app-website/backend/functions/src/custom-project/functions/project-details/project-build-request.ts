import * as functions from "firebase-functions";
import * as admin from "firebase-admin";
import {getQuickResponse} from "../../../shareable-firebase-backend/model-data/api-response.model";
import {Log} from "../../../shareable-firebase-backend/utils/log";
import {
    ProjectAndroidConfigsModel,
    ProjectBuildStatusType,
    ProjectDetailsModel,
    ProjectIOSConfigsModel,
    ProjectPlatformTypes
} from "../../data/model/project-details.model";
import {getDocumentData} from "../../../shareable-firebase-backend/utils/utils";
import {CustomFirestoreCollectionDB} from "../../data/custom-firestore-collection-names";
import {ProjectAndroidBuildRequestModel} from "../../data/model/project-android-build-request.model";
import {BaseModelUtils} from "../../../shareable-firebase-backend/model-data/base.model";
import {ProjectIOSBuildRequestModel} from "../../data/model/project-ios-build-request.model";
import {
    createEmailTemplateSimpleMessage
} from "../../../shareable-firebase-backend/email-templates/email-templates/email-template-simple-message";
import {FrontendRouteUtils} from "../../utils/frontend-route.utils";
import {WEBAPP_CONFIGS} from "../../../configs/webapp.configs";
import {MailerSendUtils} from "../../../shareable-firebase-backend/utils/email-utils/email-sender-utils";

export const web2appProjectAndroidBuildRequest = functions.https.onCall(
    (data: { projectId: string, androidConfigs?: ProjectAndroidConfigsModel},
     context) => {
        if (!context.auth || !context.auth.uid) return getQuickResponse(false, undefined, "User not authorized");
        const userId = context && context.auth && context.auth.uid ? context.auth.uid : "";
        Log.onCall(data, userId);

        return new Promise(async (resolve, reject) => {

            let projectDetails: ProjectDetailsModel | undefined = await getDocumentData<ProjectDetailsModel>(CustomFirestoreCollectionDB.Web2AppProjects, data.projectId);
            if (!projectDetails) {
                resolve(getQuickResponse(false, null, "Project not found."));
                return;
            }

            let batch = admin.firestore().batch();
            if (projectDetails.inProgressBuilds
                && projectDetails.inProgressBuilds.ANDROID) {
                batch.delete(admin.firestore().collection(CustomFirestoreCollectionDB.ProjectAndroidBuildRequests).doc(projectDetails.inProgressBuilds.ANDROID.uid!));
            }

            // Update the version name
            if (data.androidConfigs) {
                projectDetails.androidConfigs = {
                    ...projectDetails.androidConfigs,
                    ...data.androidConfigs,
                };
            }

            projectDetails.androidConfigs.isEditable = false;

            // Generate the new object
            const buildId: string = projectDetails.uid!.toLowerCase() + "-android-" + Date.now();
            let androidBuildRequest: ProjectAndroidBuildRequestModel = {
                ...BaseModelUtils.initBaseModel({
                    collectionName: CustomFirestoreCollectionDB.ProjectAndroidBuildRequests,
                    userId: projectDetails.userId,
                    uid: buildId,
                }),
                projectId: data.projectId,
                status: ProjectBuildStatusType.IN_PROGRESS,
                buildConfigs: projectDetails.androidConfigs,
                projectSnapshot: {
                    ...projectDetails,
                    lastBuilds: {},
                    inProgressBuilds: {},
                }
            };

            // Create Project Build Request
            batch.create(admin.firestore()
                .collection(CustomFirestoreCollectionDB.ProjectAndroidBuildRequests)
                .doc(androidBuildRequest.uid!), androidBuildRequest);

            // Update Project Details with the new Build Request
            batch.set(admin.firestore().collection(CustomFirestoreCollectionDB.Web2AppProjects)
                .doc(data.projectId), {
                androidConfigs: projectDetails.androidConfigs,
                inProgressBuilds: {
                    ANDROID: androidBuildRequest,
                },
                hasBuilt: true,
            }, {merge: true});


            await batch.commit();

            projectDetails = await getDocumentData<ProjectDetailsModel>(CustomFirestoreCollectionDB.Web2AppProjects, data.projectId);
            resolve(getQuickResponse(true, projectDetails));

            // Send Alert Email
            await sendNotifEmail({
                projectDetails: projectDetails,
                platformType: ProjectPlatformTypes.ANDROID,
            });
        });
    });


export const web2appProjectIOSBuildRequest = functions.https.onCall(
    (data: { projectId: string, iosConfigs?: ProjectIOSConfigsModel},
     context) => {
        if (!context.auth || !context.auth.uid) return getQuickResponse(false, undefined, "User not authorized");
        const userId = context && context.auth && context.auth.uid ? context.auth.uid : "";
        Log.onCall(data, userId);

        return new Promise(async (resolve, reject) => {

            let projectDetails: ProjectDetailsModel | undefined = await getDocumentData<ProjectDetailsModel>(CustomFirestoreCollectionDB.Web2AppProjects, data.projectId);
            if (!projectDetails) {
                resolve(getQuickResponse(false, null, "Project not found."));
                return;
            }

            let batch = admin.firestore().batch();
            if (projectDetails.inProgressBuilds
                && projectDetails.inProgressBuilds.IOS) {
                batch.delete(admin.firestore().collection(CustomFirestoreCollectionDB.ProjectiOSBuildRequests).doc(projectDetails.inProgressBuilds.IOS.uid!));
            }

            // Update the version name
            if (data.iosConfigs) {
                projectDetails.iosConfigs = {
                    ...projectDetails.iosConfigs,
                    ...data.iosConfigs,
                };
            }
            projectDetails.iosConfigs.isEditable = false;

            // Generate the new object
            const buildId: string = projectDetails.uid!.toLowerCase() + "-ios-" + Date.now();
            let iosBuildRequestModel: ProjectIOSBuildRequestModel = {
                ...BaseModelUtils.initBaseModel({
                    collectionName: CustomFirestoreCollectionDB.ProjectiOSBuildRequests,
                    userId: projectDetails.userId,
                    uid: buildId,
                }),
                projectId: data.projectId,
                status: ProjectBuildStatusType.IN_PROGRESS,
                buildConfigs: projectDetails.iosConfigs,
                projectSnapshot: {
                    ...projectDetails,
                    lastBuilds: {},
                    inProgressBuilds: {},
                },
            };


            // Create Project Build Request
            batch.create(admin.firestore()
                .collection(CustomFirestoreCollectionDB.ProjectiOSBuildRequests)
                .doc(iosBuildRequestModel.uid!), iosBuildRequestModel);

            // Update Project Details with the new Build Request
            batch.set(admin.firestore().collection(CustomFirestoreCollectionDB.Web2AppProjects)
                .doc(data.projectId), {
                iosConfigs: projectDetails.iosConfigs,
                inProgressBuilds: {
                    IOS: iosBuildRequestModel,
                },
                hasBuilt: true,
            }, {merge: true});


            await batch.commit();

            projectDetails = await getDocumentData<ProjectDetailsModel>(CustomFirestoreCollectionDB.Web2AppProjects, data.projectId);
            resolve(getQuickResponse(true, projectDetails));


            // Send Alert Email
            await sendNotifEmail({
                projectDetails: projectDetails,
                platformType: ProjectPlatformTypes.IOS,
            });
        });
    });

async function sendNotifEmail(props: { projectDetails: ProjectDetailsModel, platformType: ProjectPlatformTypes }) {
    if (!props.projectDetails.settings || !props.projectDetails.settings.contactEmail || !props.projectDetails.settings.receiveEmailAlerts) {
        return;
    }

    const navParam: string = props.platformType === ProjectPlatformTypes.IOS ? "IOS_BUILD" : "ANDROID_BUILD";
    const bodyEmailHtml: string = createEmailTemplateSimpleMessage({
        title: "We are building your " + props.platformType + " App.",
        subtitle: "We will send you a notification as soon as we have an update (Expect 24 hours minimum to get an update)",
        message: "After we generate your app, you will be able to publish it on your App Store, feel free to contact us to support you on that process",
        ctaLabel: "Open Project",
        ctaLink: FrontendRouteUtils.getProjectDetailsURL(props.projectDetails.uid!, navParam),
        logoSrc: WEBAPP_CONFIGS.LOGO_URL,
        companyAddress: WEBAPP_CONFIGS.COMPANY_ADDRESS,
        companyName: WEBAPP_CONFIGS.SITE_NAME,
        unsubscribeLink: WEBAPP_CONFIGS.WEBSITE_URL + "/project/" + props.projectDetails.uid! + "?nav=SETTINGS",
    });


    // Send an email to confirm the Payment was validated
    await MailerSendUtils.sendEmail({
        sentFrom: {email: "no-reply@webtoapp.app", name: WEBAPP_CONFIGS.SITE_NAME},
        subject: "Great. The App Build is now in progress...",
        destinationsEmail: [
            {
                uid: props.projectDetails.userId!,
                email: props.projectDetails.settings.contactEmail!,
                name: props.projectDetails.title,
            },
        ],
        bodyHtml: bodyEmailHtml,
    });
}