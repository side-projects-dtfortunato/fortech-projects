import * as functions from "firebase-functions";
const getMetaData = require('metadata-scraper')
import {ProjectDetailsModel} from "../../data/model/project-details.model";
import {BackendSharedUtils} from "../../../shareable-firebase-backend/utils/backend-shared-utils";
import {getQuickResponse} from "../../../shareable-firebase-backend/model-data/api-response.model";
import {MetaData} from "metadata-scraper";

export const prefetchUrlToConvert = functions.https.onCall(
    async (data: {projectData: ProjectDetailsModel},
     context) => {

        if (!data.projectData.url || !BackendSharedUtils.isValidHttpUrl(data.projectData.url!)) {
            return getQuickResponse(false, null, "Url is invalid");
        }

        try {
            const metaData: MetaData = await getMetaData(data.projectData.url!);

            console.debug(metaData);

            if (metaData) {
                // Extract data
                data.projectData.title = metaData.title;
                if (metaData.icon) {
                    data.projectData.logo = {
                        fileUrl: metaData.icon,
                        storagePath: "",
                    };
                } else if (metaData.image) {
                    data.projectData.logo = {
                        fileUrl: metaData.image,
                        storagePath: "",
                    };
                }

                if (data.projectData.logo?.fileUrl) {
                    let colors: { primaryColor: string, backgroundColor: string } | undefined = await BackendSharedUtils.extractColorsFromImage(data.projectData.logo?.fileUrl);

                    if (colors) {
                        data.projectData.uiStyle.iconColor = colors.primaryColor;
                        data.projectData.uiStyle.navbarBgColor = colors.backgroundColor;
                    }
                }

            } else {
                return getQuickResponse(false, data.projectData, "Metadata of the link wasn't found");
            }
        } catch (e: any) {

            console.log("catch.error", e);
            return getQuickResponse(false, data.projectData, e.message);
        }

        return getQuickResponse(true, data.projectData);
    });