import * as functions from "firebase-functions";
import * as admin from "firebase-admin";
import {getQuickResponse} from "../../../shareable-firebase-backend/model-data/api-response.model";
import {Log} from "../../../shareable-firebase-backend/utils/log";
import {ProjectDetailsModel} from "../../data/model/project-details.model";
import {getDocumentData} from "../../../shareable-firebase-backend/utils/utils";
import {CustomFirestoreCollectionDB} from "../../data/custom-firestore-collection-names";
import {BackendSharedUtils} from "../../../shareable-firebase-backend/utils/backend-shared-utils";


export const deleteProject = functions.https.onCall(
    async (data: { projectId: string },
           context) => {
        if (!context.auth || !context.auth.uid) return getQuickResponse(false, undefined, "User not authorized");
        const userId = context && context.auth && context.auth.uid ? context.auth.uid : "";
        Log.onCall(data, userId);

        // Get Project Details
        const projectDetails: ProjectDetailsModel = await getDocumentData(CustomFirestoreCollectionDB.Web2AppProjects, data.projectId);

        if (!projectDetails || projectDetails.userId !== userId) {
            return getQuickResponse(false, null, "Not Authorized");
        }

        // Delete all android and iOS builds
        const batch = admin.firestore().batch();

        // Android
        const androidBuildSnapshots = await admin.firestore().collection(CustomFirestoreCollectionDB.ProjectAndroidBuildRequests)
            .where('projectId', '==', data.projectId).get();
        androidBuildSnapshots.forEach(doc => {
            batch.delete(doc.ref);
        });

        // iOS
        const iosBuildSnapshots = await admin.firestore().collection(CustomFirestoreCollectionDB.ProjectiOSBuildRequests)
            .where('projectId', '==', data.projectId).get();
        iosBuildSnapshots.forEach(doc => {
            batch.delete(doc.ref);
        });

        // Delete storage folder
        await BackendSharedUtils.deleteFolderInStorage(projectDetails.userId + "/" + projectDetails.uid! + "/");

        // Delete Project
        batch.delete(admin.firestore().collection(CustomFirestoreCollectionDB.Web2AppProjects).doc(data.projectId));

        await batch.commit();

        return getQuickResponse(true);
    });