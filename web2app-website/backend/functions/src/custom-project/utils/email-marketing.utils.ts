import {ProjectDetailsModel} from "../data/model/project-details.model";
import {
    createEmailTemplateSimpleMessage
} from "../../shareable-firebase-backend/email-templates/email-templates/email-template-simple-message";
import {WEBAPP_CONFIGS} from "../../configs/webapp.configs";
import {FrontendRouteUtils} from "./frontend-route.utils";
import {MailerSendUtils} from "../../shareable-firebase-backend/utils/email-utils/email-sender-utils";

export class EmailMarketingUtils {


    static async sendProjectNotFinished(projectData: ProjectDetailsModel) {
        const title: string = projectData.title
            ? projectData.title! + " is awaiting to be converted into a Mobile App" : "Your Website is waiting to convert into a Mobile App";

        const bodyEmailHtml: string = createEmailTemplateSimpleMessage({
            title: title,
            subtitle: "Finish the conversion of your Website.",
            message: "We noticed that you started a project on WebToApp.app but didn't finish. Finish now converting your website. Contact our Support team in case you need some help to convert your Website.",
            ctaLabel: "Open Project",
            ctaLink: FrontendRouteUtils.getStudioUrl(),
            logoSrc: WEBAPP_CONFIGS.LOGO_URL,
            companyAddress: WEBAPP_CONFIGS.COMPANY_ADDRESS,
            companyName: WEBAPP_CONFIGS.SITE_NAME,
            unsubscribeLink: FrontendRouteUtils.getProjectDetailsURL(projectData.uid!, "SETTINGS"),
        });

        // Send an email to confirm the Payment was validated
        await MailerSendUtils.sendEmail({
            sentFrom: {email: "no-reply@webtoapp.app", name: WEBAPP_CONFIGS.SITE_NAME},
            subject: title,
            destinationsEmail: [
                {
                    uid: projectData.userId!,
                    email: projectData.settings.contactEmail!,
                    name: projectData.title,
                },
            ],
            bodyHtml: bodyEmailHtml,
        });
    }

    static async sendProjectIsExpiring(projectData: ProjectDetailsModel) {
        const title: string = projectData.title
            ? projectData.title! + " conversion project is expiring" : "Your Project is expiring";

        const bodyEmailHtml: string = createEmailTemplateSimpleMessage({
            title: title,
            subtitle: "Finish converting your website before we remove it.",
            message: "We noticed that you started a project on WebToApp.app but didn't finish. Because your project was created more than 30 days ago, we will remove it from our Data Base. Click in the button below to continue converting your Website into a Mobile App and avoid the removal of your project. Contact our Support team in case you need some help to convert your Website.",
            ctaLabel: "Open Project",
            ctaLink: FrontendRouteUtils.getProjectDetailsURL(projectData.uid!),
            logoSrc: WEBAPP_CONFIGS.LOGO_URL,
            companyAddress: WEBAPP_CONFIGS.COMPANY_ADDRESS,
            companyName: WEBAPP_CONFIGS.SITE_NAME,
            unsubscribeLink: FrontendRouteUtils.getProjectDetailsURL(projectData.uid!, "SETTINGS"),
        });

        // Send an email to confirm the Payment was validated
        await MailerSendUtils.sendEmail({
            sentFrom: {email: "no-reply@webtoapp.app", name: WEBAPP_CONFIGS.SITE_NAME},
            subject: title,
            destinationsEmail: [
                {
                    uid: projectData.userId!,
                    email: projectData.settings.contactEmail!,
                    name: projectData.title,
                },
            ],
            bodyHtml: bodyEmailHtml,
        });
    }

    static async sendProjectExpiredRemoved(projectData: ProjectDetailsModel) {
        const title: string = "Your Project was removed from our platform";

        const bodyEmailHtml: string = createEmailTemplateSimpleMessage({
            title: title,
            subtitle: "Your project \"" + projectData.title! + "\" was removed due to inactivity.",
            message: "We noticed that you started a project on WebToApp.app but didn't finish. Because your project was created more than 30 days ago, we removed it from our DataBase. Click in the button below to convert another Website into a Mobile App.",
            ctaLabel: "Convert Your Website Again",
            ctaLink: FrontendRouteUtils.getCreateProjectPageUrl({defaultUrl: projectData.url}),
            logoSrc: WEBAPP_CONFIGS.LOGO_URL,
            companyAddress: WEBAPP_CONFIGS.COMPANY_ADDRESS,
            companyName: WEBAPP_CONFIGS.SITE_NAME,
            unsubscribeLink: FrontendRouteUtils.getProjectDetailsURL(projectData.uid!, "SETTINGS"),
        });

        // Send an email to confirm the Payment was validated
        await MailerSendUtils.sendEmail({
            sentFrom: {email: "no-reply@webtoapp.app", name: WEBAPP_CONFIGS.SITE_NAME},
            subject: title,
            destinationsEmail: [
                {
                    uid: projectData.userId!,
                    email: projectData.settings.contactEmail!,
                    name: projectData.title,
                },
            ],
            bodyHtml: bodyEmailHtml,
        });
    }
}