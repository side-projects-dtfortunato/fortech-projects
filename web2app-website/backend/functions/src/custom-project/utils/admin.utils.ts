import {ProjectBuildStatusType, ProjectDetailsModel, ProjectPlatformTypes} from "../data/model/project-details.model";
import {WEBAPP_CONFIGS} from "../../configs/webapp.configs";
import {FrontendRouteUtils} from "./frontend-route.utils";
import {
    createEmailTemplateSimpleMessage
} from "../../shareable-firebase-backend/email-templates/email-templates/email-template-simple-message";
import {MailerSendUtils} from "../../shareable-firebase-backend/utils/email-utils/email-sender-utils";

export class AdminUtils {
    static async sendAdminEmailBuildResponse(props: { projectDetails: ProjectDetailsModel, platform: ProjectPlatformTypes, displayMessage?: string, status: ProjectBuildStatusType }) {
        if (!props.projectDetails.settings || !props.projectDetails.settings.contactEmail || !props.projectDetails.settings.receiveEmailAlerts) {
            return;
        }

        let subject, title, subtitle, message, ctaLink;

        switch (props.status) {
            case ProjectBuildStatusType.BUILD_FAILED:
                subject = "Something failed on your build.";
                title = WEBAPP_CONFIGS.SITE_NAME;
                subtitle = "Something failed on your build.";
                message = "We detected some problems while generating your mobile app, here's the message from our Support Team: \"" + props.displayMessage! + "\"";
                break;
            case ProjectBuildStatusType.BUILD_COMPLETED:
                subject = "Your app is ready to publish!";
                title = WEBAPP_CONFIGS.SITE_NAME;
                subtitle = "We just completed the build of your app.";
                message = "You can start publishing the app on the app stores. Contact us if you need any support to publish it.";
                break;
            default:
                return;
        }

        if (props.platform === ProjectPlatformTypes.IOS) {
            ctaLink = FrontendRouteUtils.getProjectDetailsURL(props.projectDetails.uid!, "IOS_BUILD");
        } else {
            ctaLink = FrontendRouteUtils.getProjectDetailsURL(props.projectDetails.uid!, "ANDROID_BUILD");
        }
        const bodyEmailHtml: string = createEmailTemplateSimpleMessage({
            title: title,
            subtitle: subtitle,
            message: message,
            ctaLabel: "Open Project",
            ctaLink: ctaLink,
            logoSrc: WEBAPP_CONFIGS.LOGO_URL,
            companyAddress: WEBAPP_CONFIGS.COMPANY_ADDRESS,
            companyName: WEBAPP_CONFIGS.SITE_NAME,
            unsubscribeLink: FrontendRouteUtils.getProjectDetailsURL(props.projectDetails.uid!, "SETTINGS"),
        });


        // Send an email to confirm the Payment was validated
        await MailerSendUtils.sendEmail({
            sentFrom: {email: "no-reply@webtoapp.app", name: WEBAPP_CONFIGS.SITE_NAME},
            subject: subject,
            destinationsEmail: [
                {
                    uid: props.projectDetails.userId!,
                    email: props.projectDetails.settings.contactEmail!,
                    name: props.projectDetails.title,
                },
            ],
            bodyHtml: bodyEmailHtml,
        });
    }
}