import {WEBAPP_CONFIGS} from "../../configs/webapp.configs";
import {ProjectSubscriptionPlan} from "../data/model/project-details.model";

export const stripeDev = require('stripe')(WEBAPP_CONFIGS.STRIPE_API.dev);
export const stripeProd = require('stripe')(WEBAPP_CONFIGS.STRIPE_API.prod);

export type PaymentPeriodType = "monthlyPrice" | "yearlyPrice";
export type Environment = "PROD" | "DEV";

export const StripeConfigs: {
    [env: string]: {
        webHookKey: string,
        plans: {
            [planType: string]: {
                monthlyPrice: string,
                yearlyPrice: string,
            },
        },
    },
} = {
    PROD: {
        webHookKey: "whsec_MveGrZUecqKwe0C5M32lMtQWHeAt9bf8",
        plans: {
            [ProjectSubscriptionPlan.ANDROID_ONLY]: {
                monthlyPrice: "price_1PsVMWJzE1KrIdc6YIRsHHrS",
                yearlyPrice: "price_1PsVLtJzE1KrIdc6rIgIF1iS",
            },
            [ProjectSubscriptionPlan.IOS_ONLY]: {
                monthlyPrice: "price_1PsVKjJzE1KrIdc66QtZAYZD",
                yearlyPrice: "price_1PsVJeJzE1KrIdc6Nnf4V004",
            },
            [ProjectSubscriptionPlan.ANDROID_AND_IOS]: {
                monthlyPrice: "price_1PsVIUJzE1KrIdc6gWUc2ZHU",
                yearlyPrice: "price_1PsVHcJzE1KrIdc6A1NwwGQt",
            },
        },
    },
    DEV: {
        webHookKey: "whsec_cgHzn1IsKJXZUIpfyLsA31pL1t9WU749",
        plans: {
            [ProjectSubscriptionPlan.ANDROID_ONLY]: {
                monthlyPrice: "price_1OWPlDJzE1KrIdc6yJpIovb0",
                yearlyPrice: "price_1OWPlDJzE1KrIdc6oYrBpHL7",
            },
            [ProjectSubscriptionPlan.ANDROID_AND_IOS]: {
                monthlyPrice: "price_1OXtGkJzE1KrIdc6CwWKQnVD",
                yearlyPrice: "price_1OXtGkJzE1KrIdc6CwWKQnVD",
            },
        },

    },
}

export class StripeUtils {

    static getPlanTypeByPriceId(env: string, priceId: string): ProjectSubscriptionPlan {
        let envConfigs: any = env === "PROD" ? StripeConfigs.PROD : StripeConfigs.DEV;
        let planType: string = "";

        Object.keys(envConfigs.plans)
            .forEach((itPlanType) => {
            if (Object.values(envConfigs.plans[itPlanType]).includes(priceId)) {
                planType = itPlanType;
            }
        });

        return planType as ProjectSubscriptionPlan;
    }

    static getSubscribedPriceIdFromInvoice (event: any): string | null {
        const invoice = event.data.object;

        // Assuming the first subscription item corresponds to the current subscription
        const subscriptionItem = invoice.lines.data.find((line: any) => line.price && line.price.type === 'recurring');

        if (!subscriptionItem) {
            console.log('No subscription items found in invoice');
            return null;
        }

        return subscriptionItem.price.id;
    };

}