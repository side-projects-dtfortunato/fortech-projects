import {WEBAPP_CONFIGS} from "../../configs/webapp.configs";

export class FrontendRouteUtils {

    static getStudioUrl() {
        return WEBAPP_CONFIGS.WEBSITE_URL + "/studio";
    }

    static getProjectDetailsURL(projectId: string, menuSelectedOption?: string) {
        let qparams = "";

        if (menuSelectedOption) {
            qparams = "?" + "nav" + "=" + menuSelectedOption;
        }

        return WEBAPP_CONFIGS.WEBSITE_URL + "/studio/project/" + projectId + qparams;
    }


    static getCreateProjectPageUrl(props?: {defaultUrl?: string, demoId?: string}) {
        let url = "/studio/create-project";

        if (props?.defaultUrl) {
            url += "?url=" + props.defaultUrl;
        } else if  (props?.demoId) {
            url += "?demoId=" + props.demoId;
        }

        return url;
    }
}