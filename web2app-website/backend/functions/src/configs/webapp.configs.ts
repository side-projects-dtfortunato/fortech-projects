import {StripeProductItemModel} from "../shareable-firebase-backend/model-data/custom-stripe.model";
import {BackendSharedUtils} from "../shareable-firebase-backend/utils/backend-shared-utils";

export const PRODUCTION_GCLOUD_PROJECT = "web2app-maker";

export let WEBAPP_CONFIGS = {

    LOGO_URL: "https://webtoapp.app/images/logo-white-bg.jpg",
    SITE_TAGLINE: "Convert your Website into a Mobile App",
    SITE_NAME: "WebToApp.app",
    WEBSITE_URL: BackendSharedUtils.isProjectProduction() ? "https://webtoapp.app/" : "http://localhost:3000",
    COMPANY_ADDRESS: "3030 Coimbra (Portugal)",
    COMPANY_EMAIL: "contact@webtoapp.app",
    POST_PREFIX_URL: "https://webtoapp.app/post/",
    PROFILE_PREFIX_URL: "https://www.webtoapp.app/public-profile/",
    UNSUBSCRIBE_URL: "https://us-central1-web2app-maker.cloudfunctions.net/unsubscribeEmail?uid=",

    MAILERSEND_API_KEY: "mlsn.d251f532c6048fe7726b635f88f317833ff09d409c642d0ef4cb659895102af6",

    CUSTOM_CONFIGS: {
        openPushInApp: true,
    } as any,

    // Account: contact.webtoapp@gmail.com - Di.....oc7
    IMAGE_KIT_API: {
        publicKey:"public_pi/TTr2Kyoqlfd9HUol6GO3ohNY=",
        privateKey:"private_P/RtYhyLuWFIgUGbFPEzuc+xK+c=",
        urlEndpoint:"https://ik.imagekit.io/WebToApp"
    },

    STRIPE_API: {
        prod: "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",
        dev: "sk_test_51OQ5RpJzE1KrIdc6qHXH9R5q4mUYnYBUmIMKoo102paE15kwVVr8ahBlfQoB4sJsTkHC8C3CUJ8cKEWse1wv5WQp00vmU5WjvE",
    },


    ONESIGNAL_CONFIGS: {
        apiKey: "",
        appId: ""
    },

    STRIPE_PRODUCTS: {
        androidBuild: {
            productName: "Android Build",
            currency: "usd",
            price: 2500, // $25
            productDescription: "Convert your website into a native Android App",
        } as StripeProductItemModel,
        iOSBuild: {
            productName: "iOS Build",
            currency: "usd",
            price: 4000, // $40
            productDescription: "Convert your website into a native iOS App",
        } as StripeProductItemModel,
    },

    SOCIAL_NETWORK_API_KEY: {
        metaAccessToken: "",
        facebookPageId: "",
        instagramId: "",
        threadsAPI: {
            clientId: "",
            clientSecret: "",
            redirectUri: `https://us-central1-${PRODUCTION_GCLOUD_PROJECT}.cloudfunctions.net/threadsAPIHandleAuthCallback`,
        },
        redditAPI: {
            clientId: "",
            clientSecret: "",
            username: "",
            password: "",
            subReddit: "",
        }
    }
}