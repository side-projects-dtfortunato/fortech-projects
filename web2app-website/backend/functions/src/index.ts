import * as functions from 'firebase-functions';
import * as admin from 'firebase-admin';

import * as AuthService from './shareable-firebase-backend/functions/auth'
import * as PushNotifsService from './shareable-firebase-backend/functions/push-notifs';
import * as DocumentChangeService from './shareable-firebase-backend/functions/document-change';
import * as UserProfileService from './shareable-firebase-backend/functions/user-profile';
import * as PostsDataService from './shareable-firebase-backend/functions/posts-data';

// Custom Functions
import {web2appCreateProject, getDemoProject} from './custom-project/functions/project-details/create-project';
import {deleteProject} from './custom-project/functions/project-details/delete-project';
import {web2appSaveProjectData, updatedAndroidKeystoreProject, updateiOSProvisioningProfileCert, updateiOSDistributionCert} from './custom-project/functions/project-details/save-project';
import {
    web2appProjectAndroidBuildRequest,
    web2appProjectIOSBuildRequest
} from './custom-project/functions/project-details/project-build-request';
import {web2appProjectCancelBuild} from './custom-project/functions/project-details/project-cancel-build-request';
import {projectPublishChanges} from './custom-project/functions/project-details/project-publish-changes';
import {
    adminCompleteAndroidBuildRequest,
    adminGetAndroidBuildRequests,
    adminAndroidRejectBuild,
} from './custom-project/functions/admin/admin-android-build-requests'
import {
    adminIOSRejectBuild, adminCompleteIOSBuildRequest, adminGetiOSBuildRequests
} from './custom-project/functions/admin/admin-ios-build-requests'
import {
    adminDraftDemoProject, adminDeleteDraftDemoProject
} from './custom-project/functions/admin/admin-draft-demo-project'
import {stripeCheckPaymentStatus} from './shareable-firebase-backend/functions/payments-stripe/stripe-check-payment-status';
import {stripeCreateCheckoutSession} from './custom-project/functions/payments/stripe-create-subscription'
import {stripeWebhook} from './custom-project/functions/payments/stripe-webhooks'
import {stripeCreateCustomerPortalSession} from './custom-project/functions/payments/stripe-customer-portal'
import {stripeUpdateSubscriptionPlan} from './custom-project/functions/payments/stripe-update-subscription-plan'
import {prefetchUrlToConvert} from './custom-project/functions/project-details/prefetch-url'
import {scanOldProjectsEmailMarketing, updateProjectsWithHasBuiltField} from './custom-project/functions/email-marketing/email-old-projects'

// Init App
admin.initializeApp(functions.config().firebase);
admin.firestore().settings({ignoreUndefinedProperties: true});


// Push
export const updateFCMTokens = PushNotifsService.updateFCMTokens;


// Auth
export const onCreateUser = AuthService.onCreateUser;

// User profile
export const updateUserProfile = UserProfileService.updateUserProfile;
export const onUpdatePublicUserProfile = UserProfileService.onUpdatePublicUserProfile;
export const checkUsernameValid = UserProfileService.checkUsernameValid;

// Document Change Service
export const requestDocumentChanges = DocumentChangeService.requestDocumentChanges;


// Posts Data Management
export const pushPostData = PostsDataService.pushPostData;
export const onPostDataUpdated = PostsDataService.onPostDataUpdated;
export const pushPostComment = PostsDataService.pushPostComment;
export const pushPostReaction = PostsDataService.pushPostReaction;
export const openViewPost = PostsDataService.openViewPost;
export const exportUrlMetadata = PostsDataService.exportUrlMetadata;


// Export functions
export {
    web2appCreateProject, getDemoProject,
    web2appSaveProjectData, updatedAndroidKeystoreProject, updateiOSProvisioningProfileCert, updateiOSDistributionCert,
    web2appProjectIOSBuildRequest, web2appProjectAndroidBuildRequest,
    web2appProjectCancelBuild, deleteProject, projectPublishChanges, prefetchUrlToConvert,

    // Admin Functions
    adminGetAndroidBuildRequests, adminCompleteAndroidBuildRequest, adminAndroidRejectBuild,
    adminIOSRejectBuild, adminCompleteIOSBuildRequest, adminGetiOSBuildRequests,
    adminDraftDemoProject, adminDeleteDraftDemoProject,

    stripeCheckPaymentStatus,
    stripeCreateCheckoutSession, stripeWebhook, stripeCreateCustomerPortalSession, stripeUpdateSubscriptionPlan,

    scanOldProjectsEmailMarketing, updateProjectsWithHasBuiltField
}